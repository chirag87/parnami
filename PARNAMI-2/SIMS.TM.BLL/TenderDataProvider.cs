﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SIMS.TM.BLL
{
    using DAL;
    using Model;
    public class TenderDataProvider:ITenderDataProvider
    {
        TMDBDataContext db = new TMDBDataContext();
        public IPaginated<Model.MTender> GetTenderHeaders(string username, int? pageindex, int? pagesize, string Key, int? sideid, bool showall = true)
        {
            var data = db.Tenders.OrderByDescending(x => x.ID)                
                        .ToPaginate(pageindex, pagesize)
                        .Select(x => new MTender()
                        {
                            ID=x.ID,
                            ProjectID = x.ProjectID,
                            CurrentOwner = x.CurrentOwner,
                            Title = x.Title,
                            ClientName = x.ClientName,

                            FirstSubmittedOn = x.FirstSubmittedOn,
                            DueOn = x.DueOn,
                            ExpEndDate = x.ExpEndDate,
                            ExpStartDate = x.ExpStartDate,                            
                        });
            return data;
        }

        public MTender GetTenderByID(int TenderID)
        {
            var tender= db.Tenders.Single(x => x.ID == TenderID);
            return ConvertTOMTender(tender);
        }

        private MTender ConvertTOMTender(Tender tender)
        {
            MTender _mtender = new MTender()
            {
                ID = tender.ID,
                Title = tender.Title,
                ClientName = tender.ClientName,
                //ClientAddress = tender.ClientAddress,
                CurrentOwner = tender.CurrentOwner,
                DueOn = tender.DueOn,
                //ExpectedTotal = tender.ExpectedTotal,
                ExpEndDate = tender.ExpEndDate,
                ExpStartDate = tender.ExpStartDate,
                FirstSubmittedOn = tender.FirstSubmittedOn,
                //IsWon = tender.IsWon,
                //ProjectID = tender.ProjectID,
                //SiteAddress = tender.SiteAddress,
                //SiteCity = tender.SiteCity,
                //SiteLocation = tender.SiteLocation,
                //SitePincode = tender.SitePincode,
                //SiteState = tender.SiteState,
                //Taxes = tender.Taxes,
                //TotalAfterTax = tender.TotalAfterTax
            };
            _mtender.TenderLines = new System.Collections.ObjectModel.ObservableCollection<MTenederLine>();
            tender.TenderLineItems.Select(x => ConvertTOTenderLInes(x)).ToList()
                .ForEach(x => _mtender.TenderLines.Add(x));
            return _mtender;

        }

        private MTenederLine ConvertTOTenderLInes(TenderLineItem x)
        {
            MTenederLine line = new MTenederLine()
            {
                LineID=x.LineID,
                Title=x.Title,
                ParentLineID=x.ParentLineID,
                TenderID=x.TenderID,
                UM=x.UnitOfMeasurement,
                ExpBillingQty=x.ExpBillingQty,
                ExpLineTotal=x.ExpLineTotal,
                ForUnits=x.ForUnits,
                IsBillable=x.IsBillable,
                PerUnitPricing=x.PerUnitPricing,
                Pricing=x.Pricing                
            };
            return line;
        }
    }
}
