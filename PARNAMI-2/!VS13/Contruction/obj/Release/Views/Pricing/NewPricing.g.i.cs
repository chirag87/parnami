﻿#pragma checksum "D:\OFFICE-WORK\INNOSOLS\WORK\SILVERLIGHT\PARNAMI\PARNAMI-2\Contruction\Views\Pricing\NewPricing.xaml" "{406ea660-64cf-4c82-b6f0-42d48172a799}" "E85EC51C641FC85E26060613F25BD62C"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.296
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using SyedMehrozAlam.CustomControls;
using System;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Automation.Peers;
using System.Windows.Automation.Provider;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Interop;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Resources;
using System.Windows.Shapes;
using System.Windows.Threading;


namespace Contruction {
    
    
    public partial class NewPricing : System.Windows.Controls.UserControl {
        
        internal System.Windows.Controls.Grid LayoutRoot;
        
        internal SyedMehrozAlam.CustomControls.AutoCompleteComboBox SiteCombo_Copy;
        
        internal SyedMehrozAlam.CustomControls.AutoCompleteComboBox ItemCombo_Copy;
        
        internal SyedMehrozAlam.CustomControls.AutoCompleteComboBox VendorCombo_Copy;
        
        internal System.Windows.Controls.TextBox tbxMeasurementUnit;
        
        internal System.Windows.Controls.Button btnAddNewLine;
        
        internal System.Windows.Controls.Button btnClear;
        
        internal System.Windows.Controls.DataGrid dgd;
        
        internal System.Windows.Controls.Button btnResetForm;
        
        internal System.Windows.Controls.Button btnSave;
        
        internal System.Windows.Controls.TextBox tbxItem;
        
        internal System.Windows.Controls.AutoCompleteBox autoItem;
        
        internal SyedMehrozAlam.CustomControls.AutoCompleteComboBox ItemCombo;
        
        internal System.Windows.Controls.ListBox ItemsList;
        
        internal System.Windows.Controls.TextBox tbxVendor;
        
        internal System.Windows.Controls.AutoCompleteBox autoVendor;
        
        internal SyedMehrozAlam.CustomControls.AutoCompleteComboBox VendorCombo;
        
        internal System.Windows.Controls.ListBox VendorsList;
        
        internal System.Windows.Controls.TextBox tbxSite;
        
        internal System.Windows.Controls.AutoCompleteBox autoSite;
        
        internal SyedMehrozAlam.CustomControls.AutoCompleteComboBox SiteCombo;
        
        internal System.Windows.Controls.ListBox SitesList;
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Windows.Application.LoadComponent(this, new System.Uri("/Contruction;component/Views/Pricing/NewPricing.xaml", System.UriKind.Relative));
            this.LayoutRoot = ((System.Windows.Controls.Grid)(this.FindName("LayoutRoot")));
            this.SiteCombo_Copy = ((SyedMehrozAlam.CustomControls.AutoCompleteComboBox)(this.FindName("SiteCombo_Copy")));
            this.ItemCombo_Copy = ((SyedMehrozAlam.CustomControls.AutoCompleteComboBox)(this.FindName("ItemCombo_Copy")));
            this.VendorCombo_Copy = ((SyedMehrozAlam.CustomControls.AutoCompleteComboBox)(this.FindName("VendorCombo_Copy")));
            this.tbxMeasurementUnit = ((System.Windows.Controls.TextBox)(this.FindName("tbxMeasurementUnit")));
            this.btnAddNewLine = ((System.Windows.Controls.Button)(this.FindName("btnAddNewLine")));
            this.btnClear = ((System.Windows.Controls.Button)(this.FindName("btnClear")));
            this.dgd = ((System.Windows.Controls.DataGrid)(this.FindName("dgd")));
            this.btnResetForm = ((System.Windows.Controls.Button)(this.FindName("btnResetForm")));
            this.btnSave = ((System.Windows.Controls.Button)(this.FindName("btnSave")));
            this.tbxItem = ((System.Windows.Controls.TextBox)(this.FindName("tbxItem")));
            this.autoItem = ((System.Windows.Controls.AutoCompleteBox)(this.FindName("autoItem")));
            this.ItemCombo = ((SyedMehrozAlam.CustomControls.AutoCompleteComboBox)(this.FindName("ItemCombo")));
            this.ItemsList = ((System.Windows.Controls.ListBox)(this.FindName("ItemsList")));
            this.tbxVendor = ((System.Windows.Controls.TextBox)(this.FindName("tbxVendor")));
            this.autoVendor = ((System.Windows.Controls.AutoCompleteBox)(this.FindName("autoVendor")));
            this.VendorCombo = ((SyedMehrozAlam.CustomControls.AutoCompleteComboBox)(this.FindName("VendorCombo")));
            this.VendorsList = ((System.Windows.Controls.ListBox)(this.FindName("VendorsList")));
            this.tbxSite = ((System.Windows.Controls.TextBox)(this.FindName("tbxSite")));
            this.autoSite = ((System.Windows.Controls.AutoCompleteBox)(this.FindName("autoSite")));
            this.SiteCombo = ((SyedMehrozAlam.CustomControls.AutoCompleteComboBox)(this.FindName("SiteCombo")));
            this.SitesList = ((System.Windows.Controls.ListBox)(this.FindName("SitesList")));
        }
    }
}

