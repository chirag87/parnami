﻿#pragma checksum "D:\Softwares\Office Works\PARNAMI\PARNAMI-2\Contruction\Views\Employee\NewEmployee.xaml" "{406ea660-64cf-4c82-b6f0-42d48172a799}" "2EAAC5C464BFA178F9F13E24EBA80734"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.18408
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using SyedMehrozAlam.CustomControls;
using System;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Automation.Peers;
using System.Windows.Automation.Provider;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Interop;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Resources;
using System.Windows.Shapes;
using System.Windows.Threading;


namespace Contruction.Views.Employee {
    
    
    public partial class NewEmployee : System.Windows.Controls.UserControl {
        
        internal System.Windows.Controls.Grid LayoutRoot;
        
        internal System.Windows.Controls.TextBlock tbsStatus;
        
        internal SyedMehrozAlam.CustomControls.AutoCompleteComboBox SiteCombo;
        
        internal System.Windows.Controls.ValidationSummary valSum;
        
        internal System.Windows.Controls.TextBlock tbsStatus_Copy;
        
        internal System.Windows.Controls.Button btnSaveChanges;
        
        internal System.Windows.Controls.Button btnCancel;
        
        internal System.Windows.Controls.Button btnSelectImage;
        
        internal System.Windows.Controls.Image imgsrc;
        
        internal System.Windows.Controls.TextBlock tbxImageName;
        
        internal System.Windows.Shapes.Rectangle rectVideo;
        
        internal System.Windows.Controls.Button btnSelectImage_Copy;
        
        internal System.Windows.Controls.TextBox testbox;
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Windows.Application.LoadComponent(this, new System.Uri("/Contruction;component/Views/Employee/NewEmployee.xaml", System.UriKind.Relative));
            this.LayoutRoot = ((System.Windows.Controls.Grid)(this.FindName("LayoutRoot")));
            this.tbsStatus = ((System.Windows.Controls.TextBlock)(this.FindName("tbsStatus")));
            this.SiteCombo = ((SyedMehrozAlam.CustomControls.AutoCompleteComboBox)(this.FindName("SiteCombo")));
            this.valSum = ((System.Windows.Controls.ValidationSummary)(this.FindName("valSum")));
            this.tbsStatus_Copy = ((System.Windows.Controls.TextBlock)(this.FindName("tbsStatus_Copy")));
            this.btnSaveChanges = ((System.Windows.Controls.Button)(this.FindName("btnSaveChanges")));
            this.btnCancel = ((System.Windows.Controls.Button)(this.FindName("btnCancel")));
            this.btnSelectImage = ((System.Windows.Controls.Button)(this.FindName("btnSelectImage")));
            this.imgsrc = ((System.Windows.Controls.Image)(this.FindName("imgsrc")));
            this.tbxImageName = ((System.Windows.Controls.TextBlock)(this.FindName("tbxImageName")));
            this.rectVideo = ((System.Windows.Shapes.Rectangle)(this.FindName("rectVideo")));
            this.btnSelectImage_Copy = ((System.Windows.Controls.Button)(this.FindName("btnSelectImage_Copy")));
            this.testbox = ((System.Windows.Controls.TextBox)(this.FindName("testbox")));
        }
    }
}

