﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Contruction.Models.Employee.SalaryStructure;

namespace Contruction.Views.Employee.SalaryStructure
{
    public partial class EmployeeSS : UserControl
    {
        public NewSSVM Model
        {
            get { return (NewSSVM)this.DataContext; }
        }
				
        public EmployeeSS()
        {
            InitializeComponent();
        }

        public EmployeeSS(int EmpID)
            : this()
        {
            this.EmpID = EmpID;
            Model.GetSSForEmployee(EmpID);
        }
        public EmployeeSS(int EmpID,bool IsfromIndex,bool IsfromIndex2)
            : this()
        {
            this.EmpID = EmpID;
            //Model.GetSSForEmployee(EmpID);
        }

        public EmployeeSS(int EmpID, bool IsFromDetailed)
            : this()
        {
            this.EmpID = EmpID;
            this.IsFromDetailedPage = IsFromDetailed;
            Model.GetSSForEmployee(EmpID);
        }

        private void btnNewSS_Click(object sender, RoutedEventArgs e)
        {
            Model.NewSS.EmpID = this.EmpID;
            Model.SubmitToDb(this.EmpID);
            
            Base.EmployeeEvents.SSUpdated += new EventHandler<SSUpdatedArgs>(EmployeeEvents_SSUpdated);
        }

        void EmployeeEvents_SSUpdated(object sender, SSUpdatedArgs e)
        {
            if (!IsFromDetailedPage)
            {
                Base.EmployeeEvents.SS.Title = "Salary Structure , (ID: " + e.EmpID + ")";
                Base.EmployeeEvents.SS.Content = new EmployeeSSDetail(e.EmpID,false);
                Base.EmployeeEvents.SS.Show();
            }
            else
            {
                Base.EmployeeEvents.RaiseSSShifted(e.EmpID, true); 
            }
        }

        public int EmpID { get; set; }

        public bool IsFromDetailedPage { get; set; }
    }
}
