﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
//using SIMS.MM.MODEL;

namespace Contruction
{
    public partial class NewPR : UserControl
    {
        public NewPRModel PRModel
        {
            get { return (NewPRModel)this.DataContext; }
        }
				
        public NewPR()
        {
            InitializeComponent();
            //this.KeyDown += new KeyEventHandler(NewPR_KeyDown);
        }

        //void NewPR_KeyDown(object sender, KeyEventArgs e)
        //{
        //    if (e.Key == Key.L && Keyboard.Modifiers == ModifierKeys.Alt)
        //    {
        //        PRModel.AddNewLine();
        //    }
        //}

        private void textBlock_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            var textblock = sender as TextBlock;
            var line = (MPRLine)textblock.DataContext;
            PRModel.DeleteLine(line.LineID);
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            form.Focus();
        }

        private void InfoIcon_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            var img = sender as Image;
            var data = (MPRLine)img.DataContext;
            if (data.ItemID == 0) return;
            var popup = new ItemSummary(data.ItemID, data.SizeID, data.BrandID);
            popup.CreatePopup();
        }

        //private void DataGrid_KeyDown(object sender, KeyEventArgs e)
        //{
        //    if (e.Key == Key.L && Keyboard.Modifiers == ModifierKeys.Alt)
        //    {
        //        PRModel.AddNewLine();
        //    }
        //}
    }
}
