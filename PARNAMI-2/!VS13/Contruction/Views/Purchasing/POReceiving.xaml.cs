﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
//using SIMS.MM.MODEL;

namespace Contruction
{
    public partial class POReceiving : ChildWindow
    {
        public POReceivingViewModel Model
        {
            get { return (POReceivingViewModel)this.DataContext; }
        }

        public POReceiving()
        {
            InitializeComponent();
        }

        public POReceiving(int poid)
            :this()
        {
            Model.GetConvertedGRN(poid);
        }

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            Model.CreateGRN();
            this.DialogResult = true;
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

        private void textBlock_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {

        }

        private void ChildWindow_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Escape)
            {
                this.DialogResult = false;
            }
        }

        private void ChildWindow_Loaded(object sender, RoutedEventArgs e)
        {
            dgd.Focus();
        }

        private void InfoIcon_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            var img = sender as Image;
            var data = (MGRNLine)img.DataContext;
            if (data.ItemID == 0) return;
            var popup = new ItemSummary(data.ItemID, data.SizeID, data.BrandID);
            popup.CreatePopup();
        }
    }
}