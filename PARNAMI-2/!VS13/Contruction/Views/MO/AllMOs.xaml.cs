﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
//using SIMS.MM.MODEL;
using SIMS.Core;

namespace Contruction
{
    public partial class AllMOs : UserControl
    {
        public MOViewModel Model
        {
            get { return (MOViewModel)this.DataContext; }
        }
				
        public AllMOs()
        {
            InitializeComponent();
            Model.MOChanged += new EventHandler(Model_MOChanged);
        }

        void Model_MOChanged(object sender, EventArgs e)
        {
            myPager.PagedData = (IPagination)Model.MOs;
        }

        private void myPager_MoveToPage(object sender, Controls.MyPageChangedEventArgs e)
        {
            Model.PageIndex = e.RequestedPage;
        }

        private void DataGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var SelectedRow = (MMOHeader)(sender as DataGrid).SelectedItem;
            if (SelectedRow == null) return;
            Base.AppEvents.RaiseMORequested(SelectedRow.ID);
        }

        private void Image_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            var img = (Image)sender;
            var moheader = (MMOHeader)img.DataContext;
            if (moheader == null) return;
            //Base.AppEvents.RaiseMORequested(moheader.ID);
            MODetailedView mod = new MODetailedView(moheader.ID);
            mod.Show();
        }

        private void btnRelease_Click(object sender, RoutedEventArgs e)
        {
            var btn = sender as HyperlinkButton;
            var line = (MMOHeader)btn.DataContext;
            if (line == null) return;
            MORelease mor = new MORelease(line.ID);
            mor.Show();
        }

        private void btnReceive_Click(object sender, RoutedEventArgs e)
        {
            var btn = sender as HyperlinkButton;
            var line = (MMOHeader)btn.DataContext;
            if (line == null) return;
            MOReceive mor = new MOReceive(line.ID, null, false);
            mor.Show();
        }

        private void SearchKey_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                Model.SearchKey = SearchKey.Text;
                Model.LoadMOs();
            }
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            SearchKey.Focus();
        }

        private void btnClear_Click(object sender, RoutedEventArgs e)
        {
            this.ReqSiteCombo.Text = "";
            this.TransSiteCombo.Text = "";
            this.StatusCombo.SelectedItem = null;
            this.ConsumableType.SelectedItem = null;
        }
    }
}
