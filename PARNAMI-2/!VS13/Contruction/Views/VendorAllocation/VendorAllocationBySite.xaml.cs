﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
//using SIMS.MM.MODEL;

namespace Contruction
{
    public partial class VendorAllocationBySite : UserControl
    {
        public VendorAllocationBySite()
        {
            InitializeComponent();
        }

        private void btnAddNew_Click(object sender, RoutedEventArgs e)
        {
            NewVendorAllocation all = new NewVendorAllocation();
            all.Show();
        }

        private void tbxItem_TextChanged(object sender, TextChangedEventArgs e)
        {
            var box = sender as TextBox;
            Base.Current.SearchItem = box.Text;
        }

        private void ItemsList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var list = sender as ListBox;
            var data = (MItem)list.SelectedItem;
            tbxItem.Text = data.Name;
            //Model.SelectedPrice.ItemID = data.ID;
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            tbxItem.Focus();
        }
    }
}
