﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Contruction.Models.Stock;
//using SIMS.MM.MODEL;

namespace Contruction
{
    public partial class StockAdjustments : UserControl
    {
        public StockAdjustmentVM Model
        {
            get { return (StockAdjustmentVM)this.DataContext; }
        }
				
        public StockAdjustments()
        {
            InitializeComponent();
            Model.SAAdded += new EventHandler(Model_SAAdded);
        }

        void Model_SAAdded(object sender, EventArgs e)
        {
            ////List<MSite> sitelist = new List<MSite>();
            ////SiteCombo1.ItemsSource = sitelist;
            //SiteCombo1.SelectedValue = "";
            //SiteCombo1.ValueMemberPath = "";
        }

        private void textBlock_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)//For Delete Line
        {
            var tbx = sender as TextBlock;
            var data=(MReceivingLine)tbx.DataContext;
            Model.DeleteLine(data.LineID);
        }

        private void hbtnAddNewLine_Click(object sender, RoutedEventArgs e)
        {
            Model.AddNewLine();
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            SiteCombo1.Focus();
        }
    }
}
