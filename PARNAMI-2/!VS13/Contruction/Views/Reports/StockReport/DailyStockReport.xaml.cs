﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace Contruction
{
    public partial class DailyStockReport : UserControl
    {
        public DailyStockReportsModel Model
        {
            get { return (DailyStockReportsModel)this.DataContext; }
        }
		
        public DailyStockReport()
        {
            InitializeComponent();
        }

        public DailyStockReport(DateTime sdate, DateTime edate, int siteid, int? itemCatID, int itemid, int? sizeid, int? brandid, double? OpeningStock)
            :this()
        {
            Model.Load(sdate, edate, siteid, itemCatID, itemid, sizeid, brandid);

            //Set Site and Item ID for Displaying on Daily Stock Report Screen
            Model.SiteID = siteid;
            Model.ItemCatID = itemCatID;
            Model.ItemID = itemid;
            Model.SizeID = sizeid;
            Model.BrandID = brandid;
            Model.OpeningStock = OpeningStock;
        }

        private void btnExport_Click(object sender, RoutedEventArgs e)
        {
            dgdDailyStock.Export();
        }
    }
}
