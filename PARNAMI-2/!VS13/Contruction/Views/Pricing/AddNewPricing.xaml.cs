﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
//using SIMS.MM.MODEL;

namespace Contruction
{
    public partial class AddNewPricing : ChildWindow
    {
        public NewPricingModel Model
        {
            get { return (NewPricingModel)this.DataContext; }
        }
				
        public AddNewPricing()
        {
            InitializeComponent();
        }

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

        private void textBlock_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            //var textblock = sender as TextBlock;
            //var line = (MPriceMaster)textblock.DataContext;
            //Model.DeleteLine(line.LineID);
        }

        private void ChildWindow_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Escape)
            {
                this.DialogResult = false;
            }
        }

        private void addnew_Click(object sender, RoutedEventArgs e)
        {
            //Model.AddNewLine();
        }
    }
}