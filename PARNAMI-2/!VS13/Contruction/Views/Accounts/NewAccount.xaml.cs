﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace Contruction
{
    public partial class NewAccount : ChildWindow
    {
        public AccountsWindowViewModel Model
        {
            get { return (AccountsWindowViewModel)this.DataContext; }
        }
				
        public NewAccount()
        {
            InitializeComponent();
        }

        public NewAccount(int id)
            : this()
        {
            Model.SelectedAccount.AccountFor = "Vendor";
            Model.SelectedAccount.AccountForID = id;
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            Model.CreateAccount();
            this.DialogResult = true;
        }

        private void ChildWindow_Loaded(object sender, RoutedEventArgs e)
        {
            tbxChequePayableTo.Focus();
        }
    }
}

