﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
//using SIMS.MM.MODEL;

namespace Contruction
{
    public partial class ShowVendors : UserControl
    {
        public ShowVendors()
        {
            InitializeComponent();
        }

        private void btnNew_Click(object sender, RoutedEventArgs e)
        {
            VendorDetailView view = new VendorDetailView(null);
            Base.Redirect(view);
        }

        private void btnExport_Click(object sender, RoutedEventArgs e)
        {
            dgd.Export();
        }

        private void tbxKey_TextChanged(object sender, TextChangedEventArgs e)
        {
            var box = sender as TextBox;
            Base.Current.SearchVendor = box.Text;
        }

        private void Image_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            var img = sender as Image;
            var data = (MVendor)img.DataContext;
            if (data == null) return;
            VendorDetailView view = new VendorDetailView(data.ID);
            view.id = data.ID;
            Base.Redirect(view);
        }

        private void HyperlinkButton_Click(object sender, RoutedEventArgs e)
        {
            var img = sender as HyperlinkButton;
            var data = (MVendor)img.DataContext;
            if (data == null) return;
            VendorDetailView view = new VendorDetailView(data.ID);
            view.id = data.ID;
            Base.Redirect(view);
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            tbxKey.Focus();
        }
    }
}