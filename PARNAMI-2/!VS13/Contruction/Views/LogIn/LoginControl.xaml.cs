﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Runtime.InteropServices;
using System.Security;
using System.Security.Permissions;

namespace Contruction
{
    public partial class LoginControl : UserControl
    {
        /*Code for Capturing CAPS LOCK Keys ON/OFF State*/
        //[DllImport("user32.dll", CharSet = CharSet.Auto, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //[SecurityCritical]
        //public static extern short GetKeyState(int keyCode);

        //bool CapsLock = (((ushort)GetKeyState(0x14)) & 0xffff) != 0;
        //bool NumLock = (((ushort)GetKeyState(0x90)) & 0xffff) != 0;
        //bool ScrollLock = (((ushort)GetKeyState(0x91)) & 0xffff) != 0;

        [DllImport("user32.dll")]
        static extern void keybd_event(byte bVk, byte bScan, uint dwFlags, UIntPtr dwExtraInfo);
        const int KEYEVENTF_EXTENDEDKEY = 0x1;
        const int KEYEVENTF_KEYUP = 0x2;

        public LoginControl()
        {
            InitializeComponent();
            Base.Current.LoginStatusChanged += new EventHandler(Current_LoginStatusChanged);
        }

        void Current_LoginStatusChanged(object sender, EventArgs e)
        {
            Status = Base.Current.LoginStatus;
            busy.IsBusy = false;
            if (Base.Current.IsLoggedIn)
                Clear();
        }

        public string Status
        {
            get { return txtStatus.Text; }
            set { txtStatus.Text = value; }
        }

        public string Username
        {
            get { return txtUsername.Text; }
            set { txtUsername.Text = value; }
        }

        public string Password
        {
            get { return txtPassword.Password; }
            set { txtPassword.Password = value; }
        }

        private void btn_Login_Click(object sender, RoutedEventArgs e)
        {
            Login();
        }

        public void Login()
        {
            busy.IsBusy = true;
            Status = "Logging ...";
            if (String.IsNullOrWhiteSpace(Username))
            { Status = "Invaid Username"; return; }
            if (String.IsNullOrWhiteSpace(Password))
            {
                Status = "Please Provide Password";
                return;
            }
            //            busy.IsBusy = true;
            Base.Current.Login(Username, Password);
            busy.IsBusy = false;
        }

        public void Clear()
        {
            Username = "";
            Password = "";
            Status = "";
        }

        private void txtPassword_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                Login();
            }
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            txtUsername.Focus();
            this.Caps.Visibility = System.Windows.Visibility.Collapsed;
        }

        private void UserControl_KeyDown(object sender, KeyEventArgs e)
        {
            //if (e.Key == Key.CapsLock && GetCapsLoackKeyState() == true)
            //{
            //    this.Caps.Visibility = System.Windows.Visibility.Visible;
            //}
            //else
            //{
            //    this.Caps.Visibility = System.Windows.Visibility.Collapsed;
            //}
        }
    }
}