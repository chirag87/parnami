﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.IO;
using System.ComponentModel;
using System.Windows.Browser;

namespace Contruction
{
    public partial class DocumentWindow : ChildWindow, INotifyPropertyChanged
    {
        public DocumentsModel Model
        {
            get { return (DocumentsModel)this.DataContext; }
        }
		
        public string Item1 = "No File Selected...";

        public DocumentWindow()
        {
            InitializeComponent();
            //doclist.Items.Add(Item1);
            Loaded += new RoutedEventHandler(DocumentWindow_Loaded);
        }

        public DocumentWindow(int id, string docfor)
            :this()
        {
            this.ForID = id;
            this.For = docfor;
        }

        void DocumentWindow_Loaded(object sender, RoutedEventArgs e)
        {
            Uri uri = new Uri(HtmlPage.Document.DocumentUri, "FileUpload/FileReceiver.ashx");
            uploadControl.UploadUrl = uri;
            uploadControl.UploadChunkSize = 25000000;
            //uploadControl.ResizeImage = true;
            //uploadControl.ImageSize = 1024;
            uploadControl.UploadCompleted += new EventHandler(uploadControl_UploadCompleted);
        }

        void uploadControl_UploadCompleted(object sender, EventArgs e)
        {
            Model.FileName = "";
            Model.FileName = uploadControl.FileName;
            Model.For = this.For;
            Model.ForID = this.ForID;
            if (comboDocumentType.SelectedItem != null)
            {
                Model.DocType = comboDocumentType.SelectedItem.ToString();
            }
            Model.SaveDoc();
            this.DialogResult = true;
            Base.Current.GetDocumentsByForID(ForID.Value, For);
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            Model.SaveDoc();
            this.DialogResult = true;
        }

        private void ChildWindow_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Escape)
            {
                this.DialogResult = false;
            }
        }

        string _For;
        public string For
        {
            get { return _For; }
            set
            {
                _For = value;
                Notify("For");
            }
        }

        int? _ForID;
        public int? ForID
        {
            get { return _ForID; }
            set
            {
                _ForID = value;
                Notify("ForID");
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void Notify(string propertyName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }

        private void ChildWindow_Loaded(object sender, RoutedEventArgs e)
        {
            comboDocumentType.Focus();
        }
    }
}

