﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.ComponentModel;
//using SIMS.MM.MODEL;

namespace Contruction
{
    public partial class AddressWindow : UserControl, INotifyPropertyChanged
    {
        public NewAddressWindowVM Model
        {
            get { return (NewAddressWindowVM)this.DataContext; }
        }
		
        public AddressWindow()
        {
            InitializeComponent();
        }

        public int ForID
        {
            get { return Model.ForID; }
            set
            {
                Model.ForID = value;
                Notify("ForID");
            }
        }

        public string For
        {
            get { return Model.For; }
            set
            {
                Model.For = value;
                Notify("For");
            }
        }

        private void tbxAddress_GotFocus(object sender, RoutedEventArgs e)
        {
            var btn = sender as TextBox;
            var data = btn.DataContext;
            if (data == null) return;
            Address address1 = new Address();
            address1.IsNew = false;
            address1.DataContext = data;
            address1.Show();
        }

        private void btnAddNew_Click(object sender, RoutedEventArgs e)
        {
            Address address = new Address(ForID);
            address.IsNew = true;
            address.Show();
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void Notify(string propertyName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            comboAddress.Focus();
        }
    }
}
