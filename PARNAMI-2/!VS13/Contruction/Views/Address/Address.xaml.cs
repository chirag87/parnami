﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.ComponentModel;
//using SIMS.MM.MODEL;
using Contruction.MasterDBService;

namespace Contruction
{
    public partial class Address : ChildWindow, INotifyPropertyChanged
    {
        public NewAddressWindowVM Model
        {
            get { return (NewAddressWindowVM)this.DataContext; }
        }

        public Address()
        {
            InitializeComponent();
        }

        public Address(int id)
            :this()
        {
            Model.SelectedAddress.AddressFor = "Vendor";
            Model.SelectedAddress.AddressForID = id;
        }

        void service_CreateNewAddressCompleted(object sender, CreateNewAddressCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                Base.ShowError(e.Error);
                Base.Current.IsBusy = false;
            }
            else
            {
                MessageBox.Show("New Address Created Successful !");
                Base.Current.IsBusy = false;
            }
        }

        bool _IsNew = false;
        public bool IsNew
        {
            get { return _IsNew; }
            set
            {
                _IsNew = value;
                Notify("IsNew");
            }
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            if (IsNew == true)
            {
                Model.CreateNewAddress();
            }
            else
            {
                Model.UpdateAddress();
            }
            this.DialogResult = true;
        }

        private void ChildWindow_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Escape)
            {
                this.DialogResult = false;
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void Notify(string propertyName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }

        private void ChildWindow_Loaded(object sender, RoutedEventArgs e)
        {
            tbxAddressName.Focus();
        }
    }
}
