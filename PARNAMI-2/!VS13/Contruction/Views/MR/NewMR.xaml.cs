﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Contruction.MRService;
using SIMS.MM.MODEL;

namespace Contruction
{
    public partial class NewMR : UserControl
    {
        public NewMRModel Model
        {
            get { return (NewMRModel)this.DataContext; }
        }
				
        public NewMR()
        {
            InitializeComponent();
        }

        private void textBlock_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            var textblock = sender as TextBlock;
            var line = (MMRLine)textblock.DataContext;
            Model.DeleteLine(line.LineID);
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            Model.Status = "";
            Model.Reset();
            this.ResetForm();
        }

        public void ResetForm()
        {
            this.status = null;
            this.requestingSiteCombo.SelectedItem = null;
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            requestingSiteCombo.Focus();
        }

        private void InfoIcon_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            var img = sender as Image;
            var data = (MMRLine)img.DataContext;
            if (data.ItemID == 0) return;
            var popup = new ItemSummary(data.ItemID, data.SizeID, data.BrandID);
            popup.CreatePopup();
        }
    }
}
