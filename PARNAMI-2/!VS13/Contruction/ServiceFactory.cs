﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.ServiceModel.Channels;
using System.ServiceModel;

namespace Contruction
{
    public class ServiceFactory
    {
        /// <summary>
        /// Creates the binding.
        /// </summary>
        /// <returns></returns>
        private static Binding CreateBinding()
        {
            BasicHttpBinding binding = new BasicHttpBinding();
            binding.Name = "SilverlightBindingGoodTimes";
            binding.MaxBufferSize = int.MaxValue;
            binding.MaxReceivedMessageSize = int.MaxValue;
            binding.Security.Mode = BasicHttpSecurityMode.None;
            return binding;
        }

        /// <summary>
        /// Creates the aand C service client.
        /// </summary>
        /// <returns></returns>
        public static T CreateMyServiceClient<T>(string location,string contractType)
        {
            var binding = CreateBinding();
            var url = new Uri(Application.Current.Host.Source, "../Services/" + location);
            var edpadd = new EndpointAddress(url);
            var type = typeof(T);

            var obj = (T)type.GetConstructor(new Type[] { typeof(Binding), typeof(EndpointAddress) })
                .Invoke(new object[] { binding, edpadd });
            return obj;
        }

        public static EndpointAddress GetAddress(string location)
        {
            var url = new Uri(Application.Current.Host.Source, "../Services/" + location);
            return new EndpointAddress(url);
        }
    }

    

    public class MyServiceFactory : ServiceFactory
    {
        public static MasterDBService.MasterDataServiceClient GetMasterDBService()
        {
            var svc = new MasterDBService.MasterDataServiceClient();
            svc.InnerChannel.OperationTimeout = new TimeSpan(0, 20, 0);
            //svc.Endpoint.Address = GetAddress("MasterDBService.svc");
            return svc;
        }
    }

    public class MyMasterDBService : MasterDBService.MasterDataServiceClient
    {
        public MyMasterDBService()
        {
            ((System.ServiceModel.IContextChannel)base.Channel).OperationTimeout = new TimeSpan(0,20,0);
        }
    }
}


