﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Collections;
using System.Collections.Generic;
using System.Windows.Printing;

namespace Contruction.Printing.Reporting
{
    public class PrintingEventArgs : EventArgs
    {
        public object DataContext { get; set; }
    }

    public class Report : Control
    {
        public DataTemplate PageHeaderTemplate
        {
            get { return (DataTemplate)GetValue(PageHeaderTemplateProperty); }
            set { SetValue(PageHeaderTemplateProperty, value); }
        }

        public static readonly DependencyProperty PageHeaderTemplateProperty =
            DependencyProperty.Register("PageHeaderTemplate", typeof(DataTemplate), typeof(Report), new PropertyMetadata(null));


        public DataTemplate PageFooterTemplate
        {
            get { return (DataTemplate)GetValue(PageFooterTemplateProperty); }
            set { SetValue(PageFooterTemplateProperty, value); }
        }

        public static readonly DependencyProperty PageFooterTemplateProperty =
            DependencyProperty.Register("PageFooterTemplate", typeof(DataTemplate), typeof(Report), new PropertyMetadata(null));


        public DataTemplate ReportHeaderTemplate
        {
            get { return (DataTemplate)GetValue(ReportHeaderTemplateProperty); }
            set { SetValue(ReportHeaderTemplateProperty, value); }
        }

        public static readonly DependencyProperty ReportHeaderTemplateProperty =
            DependencyProperty.Register("ReportHeaderTemplate", typeof(DataTemplate), typeof(Report), new PropertyMetadata(null));


        public DataTemplate ItemsHeaderTemplate
        {
            get { return (DataTemplate)GetValue(ItemsHeaderProperty); }
            set { SetValue(ItemsHeaderProperty, value); }
        }

        public static readonly DependencyProperty ItemsHeaderProperty =
            DependencyProperty.Register("ItemsHeader", typeof(DataTemplate), typeof(Report), new PropertyMetadata(null));


        public DataTemplate ItemTemplate
        {
            get { return (DataTemplate)GetValue(ItemTemplateProperty); }
            set { SetValue(ItemTemplateProperty, value); }
        }

        public static readonly DependencyProperty ItemTemplateProperty =
            DependencyProperty.Register("ItemTemplate", typeof(DataTemplate), typeof(Report), new PropertyMetadata(null));


        public DataTemplate ReportFooterTemplate
        {
            get { return (DataTemplate)GetValue(ReportFooterTemplateProperty); }
            set { SetValue(ReportFooterTemplateProperty, value); }
        }
        public static readonly DependencyProperty ReportFooterTemplateProperty =
            DependencyProperty.Register("ReportFooterTemplate", typeof(DataTemplate), typeof(Report), new PropertyMetadata(null));

        public DataTemplate ReportFooterTemplate2
        {
            get { return (DataTemplate)GetValue(ReportFooterTemplate2Property); }
            set { SetValue(ReportFooterTemplate2Property, value); }
        }
        public static readonly DependencyProperty ReportFooterTemplate2Property =
            DependencyProperty.Register("ReportFooterTemplate2", typeof(DataTemplate), typeof(Report), new PropertyMetadata(null));

        public DataTemplate ReportFooterTemplate3
        {
            get { return (DataTemplate)GetValue(ReportFooterTemplate3Property); }
            set { SetValue(ReportFooterTemplate3Property, value); }
        }
        public static readonly DependencyProperty ReportFooterTemplate3Property =
            DependencyProperty.Register("ReportFooterTemplate3", typeof(DataTemplate), typeof(Report), new PropertyMetadata(null));

        public DataTemplate ReportFooterTemplate4
        {
            get { return (DataTemplate)GetValue(ReportFooterTemplate4Property); }
            set { SetValue(ReportFooterTemplate4Property, value); }
        }
        public static readonly DependencyProperty ReportFooterTemplate4Property =
            DependencyProperty.Register("ReportFooterTemplate4", typeof(DataTemplate), typeof(Report), new PropertyMetadata(null));



        public IEnumerable ItemsSource
        {
            get { return (IEnumerable)GetValue(ItemsSourceProperty); }
            set { SetValue(ItemsSourceProperty, value); }
        }

        public static readonly DependencyProperty ItemsSourceProperty =
            DependencyProperty.Register("ItemsSource", typeof(IEnumerable), typeof(Report), new PropertyMetadata(null));


        //public ItemCollection ItemsOnCurrentPage
        //{
        //    get { return (ItemCollection)GetValue(ItemsOnCurrentPageProperty); }
        //    private set { SetValue(ItemsOnCurrentPageProperty, value); }
        //}

        //public static readonly DependencyProperty ItemsOnCurrentPageProperty =
        //    DependencyProperty.Register("ItemsOnCurrentPage", typeof(ItemCollection), typeof(Report), new PropertyMetadata(null));


        public string Title
        {
            get { return (string)GetValue(TitleProperty); }
            set { SetValue(TitleProperty, value); }
        }

        public static readonly DependencyProperty TitleProperty =
            DependencyProperty.Register("Title", typeof(string), typeof(Report), new PropertyMetadata("Report"));


        public int CurrentPageNumber
        {
            get { return (int)GetValue(CurrentPageNumberProperty); }
            private set { SetValue(CurrentPageNumberProperty, value); }
        }

        public static readonly DependencyProperty CurrentPageNumberProperty =
            DependencyProperty.Register("CurrentPageNumber", typeof(int), typeof(Report), new PropertyMetadata(0));


        public int TotalPageCount
        {
            get { return (int)GetValue(TotalPageCountProperty); }
            private set { SetValue(TotalPageCountProperty, value); }
        }

        public static readonly DependencyProperty TotalPageCountProperty =
            DependencyProperty.Register("TotalPageCount", typeof(int), typeof(Report), new PropertyMetadata(0));


        private PrintDocument _printDocument = new PrintDocument();
        public PrintDocument PrintDocument
        {
            get { return _printDocument; }
        }


        public event EventHandler EndPrint;
        protected void OnEndPrint(EventArgs args)
        {
            if (EndPrint != null)
                EndPrint(this, args);
        }

        public event EventHandler BeginPrint;
        protected void OnBeginPrint(EventArgs args)
        {
            if (BeginPrint != null)
                BeginPrint(this, args);
        }

        public event EventHandler EndBuildReport;
        protected void OnEndBuildReport(EventArgs args)
        {
            if (EndBuildReport != null)
                EndBuildReport(this, args);
        }

        public event EventHandler BeginBuildReport;
        protected void OnBeginBuildReport(EventArgs args)
        {
            if (BeginBuildReport != null)
                BeginBuildReport(this, args);
        }

        public event EventHandler EndBuildReportItem;
        protected void OnEndBuildReportItem(EventArgs args)
        {
            if (EndBuildReportItem != null)
                EndBuildReportItem(this, args);
        }

        public event EventHandler<PrintingEventArgs> BeginBuildReportItem;
        protected void OnBeginBuildReportItem(PrintingEventArgs args)
        {
            if (BeginBuildReportItem != null)
                BeginBuildReportItem(this, args);
        }

        public event EventHandler<PrintingEventArgs> BeginBuildReportFooter;
        protected void OnBeginBuildReportFooter(PrintingEventArgs args)
        {
            if (BeginBuildReportFooter != null)
                BeginBuildReportFooter(this, args);
        }



        private int _pageTreeIndex = 0;
        private void PrintDocumentPrintPageHandler(object sender, PrintPageEventArgs e)
        {

            if (_pageTreeIndex == 0)
            {
                BuildReport(e.PrintableArea);
                TotalPageCount = _pageTrees.Count;
                CurrentPageNumber = 0;
            }

            if (_pageTrees.Count > 0)
            {
                CurrentPageNumber++;
                e.PageVisual = _pageTrees[_pageTreeIndex];
            }

            e.HasMorePages = _pageTreeIndex < _pageTrees.Count - 1;

            _pageTreeIndex++;
        }

        private void PrintDocumentBeginPrintHandler(object sender, BeginPrintEventArgs e)
        {
            OnBeginPrint(EventArgs.Empty);
        }

        private void PrintDocumentEndPrintHandler(object sender, EndPrintEventArgs e)
        {
            OnEndPrint(EventArgs.Empty);

            // remove event handlers so we don't have any references hanging around
            _printDocument.PrintPage -= PrintDocumentPrintPageHandler;
            _printDocument.BeginPrint -= PrintDocumentBeginPrintHandler;
            _printDocument.EndPrint -= PrintDocumentEndPrintHandler;
        }

        //public void Print()
        //{
        //    System.Windows.Printing.
        //    Print(null);
        //}

        public void Print(Object data)
        {
            this.DataContext = data;
            Print();
        }

        public void Print()
        {
            CurrentPageNumber = 0;
            _pageTreeIndex = 0;

            _printDocument.PrintPage += PrintDocumentPrintPageHandler;
            _printDocument.BeginPrint += PrintDocumentBeginPrintHandler;
            _printDocument.EndPrint += PrintDocumentEndPrintHandler;

            _printDocument.Print(Title);
        }


        private List<UIElement> _pageTrees = new List<UIElement>();

        // Pre-calculates pages. This allows for total page count
        // as well as better handling of page breaks, and eventual
        // inclusion of groups and whatnot. If this takes too long, though
        // it'll exceed the timeout and make the printing fail in a 
        // sandboxed silverlight application. Alternative would be to make
        // printing a two-step operation: First, build report, Second, user 
        // clicks button to do the actual printing. That approach 
        // introduces issues with data changing in-between the steps.
        private void BuildReport(Size printableArea)
        {
            _pageTrees.Clear();
            CurrentPageNumber = 0;

            OnBeginBuildReport(EventArgs.Empty);

            if (ItemsSource == null)
                return;

            IEnumerable reportItems = ItemsSource;
            IEnumerator reportItemsEnumerator = reportItems.GetEnumerator();

            reportItemsEnumerator.Reset();

            StackPanel itemsPanel = null;
            Grid pagePanel = null;
            Size itemsPanelMaxSize = new Size();

            // create first page
            pagePanel = GetNewPage(printableArea, out itemsPanel, out itemsPanelMaxSize, true);

            // add the items
            while (reportItemsEnumerator.MoveNext())
            {
                object currentItem = reportItemsEnumerator.Current;

                PrintingEventArgs args = new PrintingEventArgs();
                args.DataContext = currentItem;
                OnBeginBuildReportItem(args);

                // create row. Set data context.
                FrameworkElement row = ItemTemplate.LoadContent() as FrameworkElement;
                row.DataContext = args.DataContext;

                // ContentPresenter approach was tried to see if it helped resolve Run
                // inlines with binding. No go.
                //ContentPresenter row = new ContentPresenter();
                //row.ContentTemplate = ItemTemplate;
                //row.Content = currentItem;

                //// create a new page if we're out of room here
                //if (row.DesiredSize.Height + itemsPanel.DesiredSize.Height > itemsPanelMaxSize.Height)
                //{
                //    pagePanel = GetNewPage(printableArea, out itemsPanel, out itemsPanelMaxSize);
                //}

                row.Measure(printableArea);
                itemsPanel.Children.Add(row);
                itemsPanel.Measure(printableArea);

                // create a new page if we're out of room here
                if (itemsPanel.DesiredSize.Height > itemsPanelMaxSize.Height)
                {
                    itemsPanel.Children.Remove(row);
                    itemsPanel.Measure(printableArea);

                    // On A New Page
                    pagePanel = GetNewPage(printableArea, out itemsPanel, out itemsPanelMaxSize);
                    itemsPanel.Children.Add(row);
                    itemsPanel.Measure(printableArea);
                }
            }

            // create report footer. This goes at the very end of the report and typically
            // includes totals and other summary information
            TryAddFooter(ReportFooterTemplate, printableArea,itemsPanel,itemsPanelMaxSize);
            TryAddFooter(ReportFooterTemplate2, printableArea, itemsPanel, itemsPanelMaxSize);
            TryAddFooter(ReportFooterTemplate3, printableArea, itemsPanel, itemsPanelMaxSize);
            TryAddFooter(ReportFooterTemplate4, printableArea, itemsPanel, itemsPanelMaxSize);

            OnEndBuildReport(EventArgs.Empty);
        }

        void TryAddFooter(DataTemplate _footerTemplate, Size printableArea, StackPanel itemsPanel, Size itemsPanelMaxSize)
        {
            if (_footerTemplate != null)
            {
                PrintingEventArgs reportFooterEventArgs = new PrintingEventArgs();
                reportFooterEventArgs.DataContext = this;
                OnBeginBuildReportFooter(reportFooterEventArgs);

                FrameworkElement reportFooter = _footerTemplate.LoadContent() as FrameworkElement;
                if (reportFooter != null)
                {
                    reportFooter.DataContext = this.DataContext;
                    itemsPanel.Children.Add(reportFooter);
                    itemsPanel.Measure(printableArea);
                    // fit the footer into the report
                    if (itemsPanel.DesiredSize.Height > itemsPanelMaxSize.Height)
                    {
                        itemsPanel.Children.Remove(reportFooter);
                        // On New Page
                        GetNewPage(printableArea, out itemsPanel, out itemsPanelMaxSize);
                        itemsPanel.Children.Add(reportFooter);
                        itemsPanel.Measure(printableArea);
                    }
                }
            }
        }


        private Grid GetNewPage(Size printableArea, out StackPanel itemsPanel, out Size itemsPanelMaxSize, bool isFirstPage=false)
        {
            const int HeaderGridRowNumber = 0;
            const int BodyGridRowNumber = 1;
            const int FooterGridRowNumber = 2;

            CurrentPageNumber++;

            Grid pagePanel = new Grid();
            RowDefinition headerRow = new RowDefinition();
            headerRow.Height = GridLength.Auto;

            RowDefinition itemsRow = new RowDefinition();
            itemsRow.Height = new GridLength(1, GridUnitType.Star);

            RowDefinition footerRow = new RowDefinition();
            footerRow.Height = GridLength.Auto;

            pagePanel.RowDefinitions.Add(headerRow);
            pagePanel.RowDefinitions.Add(itemsRow);
            pagePanel.RowDefinitions.Add(footerRow);

            // page header. Typically includes column headers and whatnot
            double headerDesiredHeight = 0;
            if (PageHeaderTemplate != null)
            {
                FrameworkElement header = PageHeaderTemplate.LoadContent() as FrameworkElement;
                header.DataContext = this.DataContext;
                Grid.SetRow(header, HeaderGridRowNumber);
                pagePanel.Children.Add(header);
                header.Measure(new Size(printableArea.Width, printableArea.Height));

                headerDesiredHeight = header.DesiredSize.Height;
            }

            // create body to fit in between
            itemsPanel = new StackPanel();
            itemsPanel.Orientation = Orientation.Vertical;
            itemsPanel.HorizontalAlignment = HorizontalAlignment.Stretch;
            itemsPanel.VerticalAlignment = VerticalAlignment.Top;

            // Add Report Header
            if (this.ReportHeaderTemplate != null && isFirstPage)
            {
                var reportheader = this.ReportHeaderTemplate.LoadContent() as FrameworkElement;
                reportheader.DataContext = this.DataContext;
                itemsPanel.Children.Add(reportheader);
                itemsPanel.Measure(printableArea);
            }

            // Add Items Header
            if (this.ItemsHeaderTemplate != null)
            {
                var itemheaderrow = this.ItemsHeaderTemplate.LoadContent() as FrameworkElement;
                itemheaderrow.DataContext = this.DataContext;
                itemsPanel.Children.Add(itemheaderrow);
                itemsPanel.Measure(printableArea);
            }

            Grid.SetRow(itemsPanel, BodyGridRowNumber);
            pagePanel.Children.Add(itemsPanel);


            // create page footer. Typically includes the page number
            double footerDesiredHeight = 0;

            if (PageFooterTemplate != null)
            {
                FrameworkElement footer = PageFooterTemplate.LoadContent() as FrameworkElement;
                footer.DataContext = this.DataContext;
                Grid.SetRow(footer, FooterGridRowNumber);
                pagePanel.Children.Add(footer);
                footer.Measure(new Size(printableArea.Width, printableArea.Height));

                footerDesiredHeight = footer.DesiredSize.Height;
            }

            itemsPanelMaxSize = new Size(printableArea.Width, printableArea.Height - footerDesiredHeight - headerDesiredHeight);

            _pageTrees.Add(pagePanel);

            return pagePanel;
        }
    }
}
