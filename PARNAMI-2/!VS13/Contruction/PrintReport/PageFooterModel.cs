﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace Contruction
{
    public class PageFooterModel : ViewModel
    {
        string _ReportType;
        public string ReportType
        {
            get { return _ReportType; }
            set
            {
                _ReportType = value;
                Notify("ReportType");
            }
        }

        string _ContactPerson;
        public string ContactPerson
        {
            get { return _ContactPerson; }
            set
            {
                _ContactPerson = value;
                Notify("ContactPerson");
            }
        }

        string _ContactNo;
        public string ContactNo
        {
            get { return _ContactNo; }
            set
            {
                _ContactNo = value;
                Notify("ContactNo");
            }
        }
    }
}
