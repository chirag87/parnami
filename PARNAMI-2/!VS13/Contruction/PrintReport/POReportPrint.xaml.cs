﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
//using SIMS.MM.MODEL;
using System.Windows.Printing;

namespace Contruction
{
    public partial class POReportPrint : UserControl
    {   
        public POReportPrint()
        {
            InitializeComponent();
        }

        public void PrintPO(MPO po)
        {
            POReportsViewModel data = new POReportsViewModel();
            data.SelectedPO = po;
            POreport.DataContext = data;
            POreport.ItemsSource = data.SelectedPO.PRLines;
            POreport.Print();
        }
    }
}
