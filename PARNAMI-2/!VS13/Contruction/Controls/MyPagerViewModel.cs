﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
//using SIMS.MM.MODEL;
using SIMS.Core;

namespace Contruction.Controls
{
    public class MyPagerViewModel : ViewModel
    {
        IPagination _PagedData = new PaginatedData<object>();
        public IPagination PagedData
        {
            get { return _PagedData; }
            set
            {
                _PagedData = value;
                Notify("PagedData");
                NotifyOnSet();
            }
        }

        public void NotifyOnSet()
        {
            Notify("CurrentPage");
            Notify("CanNext");
            Notify("CanPrevious");
            Notify("CurrentPageToShow");
        }

        public bool CanNext { get { return PagedData.CurrentPage < PagedData.TotalPages - 1; } }
        public bool CanPrevious { get { return PagedData.CurrentPage > 0; } }
        public int CurrentPage
        {
            get { return PagedData.CurrentPage; }
            set { PagedData.CurrentPage = value; }
        }
        public int CurrentPageToShow
        {
            get { return PagedData.CurrentPage + 1; }
            set { PagedData.CurrentPage = value - 1; }
        }
    }
}
