﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace Contruction
{
    public class ExtAutoCompleteBoxItem : ContentControl
    {
        ExtAutoCompleteBox parentControl = null;
        public ExtAutoCompleteBoxItem()
        {
            DefaultStyleKey = typeof(ExtAutoCompleteBoxItem);
        }
        public ExtAutoCompleteBoxItem(string s, ExtAutoCompleteBox parent)
            : this()
        {
            Content = s;
            parentControl = parent;
        }

        Button btn = null;
        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();
            btn = GetTemplateChild("RemoveItemButton") as Button;
            btn.Click += new RoutedEventHandler(btn_Click);
        }

        void btn_Click(object sender, RoutedEventArgs e)
        {
            parentControl.RemoveItem(Content.ToString());
        }
    }
}
