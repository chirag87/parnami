﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace Contruction
{
    public partial class PopUp : ChildWindow
    {

        public event EventHandler Submitted;
        public event EventHandler Cancelled;

        //public partial void OnSubmitted();
        //public partial void OnCancelled();

        internal void Submit()
        {
            //OnSubmitted();
            if (Submitted != null)
                Submitted(this, new EventArgs());
            this.DialogResult = true;
        }

        internal void Cancel()
        {
            //OnCancelled();
            if (Cancelled != null)
                Cancelled(this, new EventArgs());
            this.DialogResult = false;
        }

        public UIElement Content
        {
            get { return ContentBorder.Child; }
            set { ContentBorder.Child = value; }
        }

        public PopUp()
        {
            InitializeComponent();
            ShowOkCancel = true;
            this.KeyDown += new KeyEventHandler(PopUp_KeyDown);
            Show();
        }

        void PopUp_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Escape)
                Cancel();
        }

        public void SetAutoLayout()
        {
            this.Height = Double.NaN;
            this.Width = Double.NaN;
        }

        public static PopUp New(UIElement _content)
        {
            return New(_content,true);
        }

        public static PopUp New(UIElement _content, bool showOkCancel)
        {
            return new PopUp()
            {
                Content = _content,
                ShowOkCancel = showOkCancel
            };
        }

        public static PopUp New()
        {
            return new PopUp()
            {

            };
        }


        bool _ShowOkCancel;
        public bool ShowOkCancel
        {
            get
            {
                return _ShowOkCancel;
            }
            set
            {
                _ShowOkCancel = value;
                OKButton.Visibility = value ? Visibility.Visible : Visibility.Collapsed;
                CancelButton.Visibility = value ? Visibility.Visible : Visibility.Collapsed;
            }
        }

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            Submit();
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            Cancel();
        }
    }
}

