﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Windows.Printing;
//using SIMS.MM.MODEL;

namespace Contruction
{
    public partial class POPrintReport : UserControl
    {
        public POPrintReport()
        {
            InitializeComponent();
        }

        public POReportsViewModel Model
        {
            get { return (POReportsViewModel)this.DataContext; }
        }
				

        public void Print()
        {
            if (Model.SelectedPO == null) return;
            PrintDocument doc = new PrintDocument();
            doc.PrintPage += new EventHandler<PrintPageEventArgs>(doc_PrintPage);
            doc.Print("PO " + Model.SelectedPO.POID);
        }

        void doc_PrintPage(object sender, PrintPageEventArgs e)
        {
            VisualStateManager.GoToState(this as Control, "Print", true);
            this.Width = e.PrintableArea.Width;
            this.Height = e.PrintableArea.Height;
            e.PageVisual = this;
        }

        public void PrintPO(MPO po)
        {
            Model.SelectedPO = po;
            Print();
        }
    }

    public partial class PrintFactory
    {
        public static void PrintPO(MPO mpo)
        {
            POPrintReport report = new POPrintReport();
            //POReportPrint report = new POReportPrint();
            report.PrintPO(mpo);
        }

        public static void PrintDP(MReceiving doc)
        {
            DirectPurchasePrint report = new DirectPurchasePrint();
            report.SetDP(doc);
        }

        public static void PrintReceiving(MReceiving doc)
        {
            ReceivingPrintReport report = new ReceivingPrintReport();
            //GRNReportPrint report = new GRNReportPrint();
            report.SetReceiving(doc);
        }

        public static void PrintBill(MBilling bill)
        {
            PrintBill report = new PrintBill();
            //POReportPrint report = new POReportPrint();
            report.PrintingBill(bill);
        }
    }
}