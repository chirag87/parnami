﻿using System;
using System.Net;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Contruction.PurchasingService;
using System.Collections.ObjectModel;
//using SIMS.MM.MODEL;
using System.ComponentModel;

namespace Contruction
{
    public class ViewPODetailsModel : ViewModel
    {
        PurchasingServiceClient service = new PurchasingServiceClient();

        public ViewPODetailsModel()
        {
            if (!DesignerProperties.GetIsInDesignMode(Application.Current.RootVisual))
            {
                OnInit();
            }
        }

        public void OnInit()
        {
            service.GetPObyIDCompleted += new EventHandler<GetPObyIDCompletedEventArgs>(service_GetPObyIDCompleted);
            service.EmailPRCompleted += new EventHandler<AsyncCompletedEventArgs>(service_EmailPRCompleted);
            service.ConfirmVendorEmailCompleted += new EventHandler<ConfirmVendorEmailCompletedEventArgs>(service_ConfirmVendorEmailCompleted);
            service.EmailtoVendorCompleted += new EventHandler<AsyncCompletedEventArgs>(service_EmailtoVendorCompleted);
        }

        void service_ConfirmVendorEmailCompleted(object sender, ConfirmVendorEmailCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                Base.ShowError(e.Error);
                IsBusy = false;
            }
            else
            {
                VendorEmail = e.Result;
                IsBusy = false;
                if (VendorEmail == null)
                {
                    MessageBox.Show("Not Successful. Please check if Email Address is configured for the Vendor!");
                }
                else
                {
                    EmailtoVendor();
                }
            }
        }

        void service_EmailtoVendorCompleted(object sender, AsyncCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                Base.ShowError(e.Error);
                IsBusy = false;
            }
            else
            {
                IsBusy = false;
                MessageBox.Show("Email Sent Successfully to " + ConvertedPO.Vendor);
            }
        }

        void service_EmailPRCompleted(object sender, AsyncCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                Base.ShowError(e.Error);
                IsBusy = false;
            }
            else
            {
                IsBusy = false;
                MessageBox.Show("Email Sent Successfully!");
            }
        }

        void service_GetPObyIDCompleted(object sender, GetPObyIDCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                Base.ShowError(e.Error);
                IsBusy = false;
            }
            else
            {
                ConvertedPO = e.Result;
                ConvertedPO.RaisedBy = Base.Current.UserName;
                IsBusy = false;
            }
        }

        public void UpdateTotal()
        {
            if (ConvertedPO == null) return;
            ConvertedPO.UpdateTotalValue();
        }

        MPO _ConvertedPO = new MPO();
        public MPO ConvertedPO
        {
            get { return _ConvertedPO; }
            set
            {
                _ConvertedPO = value;
                PRLines = value.PRLines;
                Notify("ConvertedPO");
            }
        }

        string _VendorEmail;
        public string VendorEmail
        {
            get { return _VendorEmail; }
            set
            {
                _VendorEmail = value;
                Notify("VendorEmail");
            }
        }

        ObservableCollection<MPRLine> _PRLines = new ObservableCollection<MPRLine>();
        public ObservableCollection<MPRLine> PRLines
        {
            get { return _PRLines; }
            set
            {
                _PRLines = value;
                Notify("PRLines");
            }
        }

        public bool CanEmailPO { get { return Base.Current.Rights.PO_CanEmailPO; } }

        public void GetPO(int poid)
        {
            service.GetPObyIDAsync(poid);
            IsBusy = true;
        }

        internal void SetID(int p)
        {
            GetPO(p);
        }

        public void Email()
        {
            if (ConvertedPO.IsApproved)
            {
                service.EmailPRAsync(ConvertedPO.PRNumber);
                IsBusy = true;
            }
            else
            {
                MessageBox.Show("PR is not Approved! Operation not Allowed!");
            }
        }

        public void ConfirmEmail()
        {
            service.ConfirmVendorEmailAsync(ConvertedPO.POID);
            IsBusy = true;
        }

        public void EmailtoVendor()
        {
            service.EmailtoVendorAsync(ConvertedPO.POID);
            IsBusy = true;
        }
    }
}
