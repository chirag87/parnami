﻿using System;
using System.Net;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.ComponentModel;
//using SIMS.MM.MODEL;
using Contruction.TransactionService;

namespace Contruction
{
    public class POReceivingViewModel : ViewModel
    {
        TransactionServiceClient newservice = new TransactionServiceClient();

        public POReceivingViewModel()
        {
            if (!DesignerProperties.GetIsInDesignMode(Application.Current.RootVisual))
            {
                OnInit();
            }
        }

        #region Methods...

        public void OnInit()
        {
            //service.ConvertfromPOCompleted += new EventHandler<ConvertfromPOCompletedEventArgs>(service_ConvertfromPOCompleted);
            //service.CreateNewReceivingCompleted += new EventHandler<CreateNewReceivingCompletedEventArgs>(service_CreateNewReceivingCompleted);
            newservice.ConvertfromPOCompleted += new EventHandler<TransactionService.ConvertfromPOCompletedEventArgs>(newservice_ConvertfromPOCompleted);
            newservice.CreateNewGRNCompleted += new EventHandler<CreateNewGRNCompletedEventArgs>(newservice_CreateNewGRNCompleted);
        }        

        public void GetConvertedGRN(int id)
        {
            //service.ConvertfromPOAsync(id);
            newservice.ConvertfromPOAsync(id);
            IsBusy = true;
        }

        public void CreateGRN()
        {
            //service.CreateNewReceivingAsync(NewGRN, Base.Current.UserName);            
            if (NewGRN.GRNLines.All(x => x.Quantity == 0)) { MessageBox.Show("Enter quantity to receive"); }
            //if (NewGRN.GRNLines.All(x => x.Quantity == 0)) { MessageBox.Show("Enter quantity to receive"); }
            else
            {                
                newservice.CreateNewGRNAsync(NewGRN, Base.Current.UserName);
                IsBusy = true;
            }
        }

        #endregion

        #region Completed Events...

        void newservice_CreateNewGRNCompleted(object sender, CreateNewGRNCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                Base.ShowError(e.Error);
                IsBusy = false;
            }
            else
            {
                IsBusy = false;
                CreatedGRN = e.Result;
                MessageBox.Show("Reference No: " + CreatedGRN.ReferenceNo + "\n\nNew GRN Added Successfully !");
                //Reset();
                //Base.AppEvents.RaiseNewReceivingAdded();
                Base.AppEvents.RaisePODataChanged();
            }
        }

        void newservice_ConvertfromPOCompleted(object sender, TransactionService.ConvertfromPOCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                Base.ShowError(e.Error);
                IsBusy = false;
            }
            else
            {
                NewGRN = e.Result;
                NewGRN.Requester = Base.Current.UserName;
                NewGRN.CreatedBy = Base.Current.UserName;
                NewGRN.UpdateLineNumbers();
                IsBusy = false;
            }
        }

        #endregion

        #region Properties...

        //MReceiving _CreatedGRN = new MReceiving();
        //public MReceiving CreatedGRN
        //{
        //    get { return _CreatedGRN; }
        //    set
        //    {
        //        _CreatedGRN = value;
        //        Notify("CreatedGRN");
        //    }
        //}

        //MReceiving _NewGRN = new MReceiving();
        //public MReceiving NewGRN
        //{
        //    get { return _NewGRN; }
        //    set
        //    {
        //        _NewGRN = value;
        //        Notify("NewGRN");
        //    }
        //}

        MGRN _CreatedGRN = new MGRN();
        public MGRN CreatedGRN
        {
            get { return _CreatedGRN; }
            set
            {
                _CreatedGRN = value;
                Notify("CreatedGRN");
            }
        }

        MGRN _NewGRN = new MGRN();
        public MGRN NewGRN
        {
            get { return _NewGRN; }
            set
            {
                _NewGRN = value;
                Notify("NewGRN");
            }
        }

        #endregion
    }
}