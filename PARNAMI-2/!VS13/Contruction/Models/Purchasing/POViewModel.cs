﻿using System;
using System.Net;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Collections.Generic;
using Contruction.PurchasingService;
using System.Collections.ObjectModel;
//using SIMS.MM.MODEL;
using System.ComponentModel;
using SIMS.Core;

namespace Contruction
{
    public class POViewModel : ViewModel
    {
        PurchasingServiceClient service = new PurchasingServiceClient();

        public POViewModel()
        {
            if (!DesignerProperties.GetIsInDesignMode(Application.Current.RootVisual))
            {
                OnInit();
            }
        }

        #region Events...

        public event EventHandler POChanged;
        public void RaisePOChanged()
        {
            if (POChanged != null)
                POChanged(this, new EventArgs());
        }

        #endregion

        public void OnInit()
        {
            service.GetAllPOsCompleted += new EventHandler<GetAllPOsCompletedEventArgs>(service_GetAllPOsCompleted);
            Base.AppEvents.PODataChanged += new EventHandler(AppEvents_PODataChanged);
        }

        #region Completed Events...

        void AppEvents_PODataChanged(object sender, EventArgs e)
        {
            LoadPOs();
        }

        void service_GetAllPOsCompleted(object sender, GetAllPOsCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                Base.ShowError(e.Error);
                IsBusy = false;
            }
            else
            {
                POs = e.Result;
                IsBusy = false;
            }
        }

        #endregion

        #region Properties...

        IPaginated<MPOHeader> _POs = new PaginatedData<MPOHeader>();
        public IPaginated<MPOHeader> POs
        {
            get { return _POs; }
            set
            {
                _POs = value;
                Notify("POs");
                RaisePOChanged();
            }
        }

        public bool CanReceivePO { get { return Base.Current.Rights.MRN_CanRaise; } }

        int _PageIndex = 0;
        public int PageIndex
        {
            get { return _PageIndex; }
            set
            {
                _PageIndex = value;
                Notify("PageIndex");
                LoadPOs();
            }
        }

        string _SearchKey;
        public string SearchKey
        {
            get { return _SearchKey; }
            set
            {
                _SearchKey = value;
                Notify("SearchKey");
            }
        }

        int? _VendorID;
        public int? VendorID
        {
            get { return _VendorID; }
            set
            {
                _VendorID = value;
                Notify("VendorID");
            }
        }

        int? _SiteID;
        public int? SiteID
        {
            get { return _SiteID; }
            set
            {
                _SiteID = value;
                Notify("SiteID");
            }
        }

        int? _DefaultVendorID = null;
        public int? DefaultVendorID
        {
            get { return _DefaultVendorID; }
            set
            {
                _DefaultVendorID = value;
                Notify("DefaultVendorID");
            }
        }

        int? _DefaultSiteID = null;
        public int? DefaultSiteID
        {
            get { return _DefaultSiteID; }
            set
            {
                _DefaultSiteID = value;
                Notify("DefaultSiteID");
            }
        }

        #endregion

        #region Methods...

        public void Clear()
        {
            SearchKey = null;
            VendorID = DefaultVendorID;
            SiteID = DefaultSiteID;
            LoadPOs();
        }

        public void LoadPOs()
        {
            service.GetAllPOsAsync(Base.Current.UserName, PageIndex, null, SearchKey, VendorID, SiteID);
            IsBusy = true;
        }

        internal void MoveToPage(int p)
        {
            PageIndex = p;
        }

        #endregion
    }
}