﻿using System;
using System.Net;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Contruction.MRService;
using System.ComponentModel;
using SIMS.MM.MODEL;

namespace Contruction
{
    public class MRReturnModel : ViewModel
    {
        MRServiceClient service = new MRServiceClient();

        public event EventHandler Return;
        private void RaiseReturn()
        {
            if (Return!= null)
                Return(this,new EventArgs());
        }

        public MRReturnModel()
        {
            if (!DesignerProperties.GetIsInDesignMode(Application.Current.RootVisual))
            {
                OnInit();
            }
        }

        public void OnInit()
        {
            //service.GetMReceivingforMRReturnCompleted += new EventHandler<GetMReceivingforMRReturnCompletedEventArgs>(service_GetMReceivingforMRReturnCompleted);
            //service.ReturnMRCompleted += new EventHandler<ReturnMRCompletedEventArgs>(service_ReturnMRCompleted);
            service.GetMRReturnTransactionByMRIDCompleted += new EventHandler<GetMRReturnTransactionByMRIDCompletedEventArgs>(service_GetMRReturnTransactionByMRIDCompleted);
            service.MRReturnCompleted += new EventHandler<MRReturnCompletedEventArgs>(service_MRReturnCompleted);
        }

        void service_MRReturnCompleted(object sender, MRReturnCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                Base.ShowError(e.Error);
                IsBusy = false;
            }
            else
            {
                IsBusy = false;
                IsReturned = true;
                TransactionID = e.Result;
                MessageBox.Show("Material Received Successfully!");
                Base.AppEvents.RaiseNewMRAdded();
                RaiseReturn();
            }
        }

        void service_GetMRReturnTransactionByMRIDCompleted(object sender, GetMRReturnTransactionByMRIDCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                Base.ShowError(e.Error);
                IsBusy = false;
            }
            else
            {
                ReceivingforMRReturn = e.Result;
                ReceivingforMRReturn.Requester = Base.Current.UserName;
                ReceivingforMRReturn.ReturnedBy = Base.Current.UserName;
                Notify("ReceivingforMRReturn");
                IsBusy = false;
            }
        }

        void service_ReturnMRCompleted(object sender, ReturnMRCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                Base.ShowError(e.Error);
                IsBusy = false;
            }
            else
            {
                IsBusy = false;
                IsReturned = true;
                TransactionID = e.Result;
                MessageBox.Show("Material Received Successfully!");
                Base.AppEvents.RaiseNewMRAdded();
                RaiseReturn();
            }
        }

        //void service_GetMReceivingforMRReturnCompleted(object sender, GetMReceivingforMRReturnCompletedEventArgs e)
        //{
        //    if (e.Error != null)
        //    {
        //        Base.ShowError(e.Error);
        //        IsBusy = false;
        //    }
        //    else
        //    {
        //        ReceivingforMRReturn = e.Result;
        //        ReceivingforMRReturn.Purchaser = Base.Current.UserName;
        //        Notify("ReceivingforMRReturn");
        //        IsBusy = false;
        //    }
        //}

        //MReceiving _ReceivingforMRReturn = new MReceiving();
        //public MReceiving ReceivingforMRReturn
        //{
        //    get { return _ReceivingforMRReturn; }
        //    set 
        //    {
        //        _ReceivingforMRReturn = value;
        //        Notify("ReceivingforMRReturn");
        //    }
        //}

        MMaterialReturn _ReceivingforMRReturn = new MMaterialReturn();
        public MMaterialReturn ReceivingforMRReturn
        {
            get { return _ReceivingforMRReturn; }
            set
            {
                _ReceivingforMRReturn = value;
                Notify("ReceivingforMRReturn");
            }
        }

        int _TransactionID;
        public int TransactionID
        {
            get { return _TransactionID; }
            set
            {
                _TransactionID = value;
                Notify("TransactionID");
            }
        }

        bool _IsReturned;
        public bool IsReturned
        {
            get { return _IsReturned; }
            set
            {
                _IsReturned = value;
                Notify("IsReturned");
            }
        }

        string _Status;
        public string Status
        {
            get { return _Status; }
            set
            {
                _Status = value;
                Notify("Status");
            }
        }

        public void GetMReceivingforMRReturn(int mrid)
        {
            //service.GetMReceivingforMRReturnAsync(mrid);
            service.GetMRReturnTransactionByMRIDAsync(mrid);
            IsBusy = true;
        }

        public void ReturnMR()
        {
            if (!Validate())
            {
                IsReturned = false;
                return;
            }
            //service.ReturnMRAsync(ReceivingforMRReturn);
            service.MRReturnAsync(ReceivingforMRReturn, Base.Current.UserName);
            IsBusy = true;
        }

        public bool Validate()
        {
            Status = "";
            //if (ReceivingforMRReturn.ReceivingLines.All(x => x.Quantity == 0.0)) { Status = "MR has some invaid/unselected Quantity!"; return false; }
            if (ReceivingforMRReturn.MaterialReturnLines.All(x => x.Quantity == 0.0)) { Status = "MR has some invaid/unselected Quantity!"; return false; }
            return true;
        }
    }
}
