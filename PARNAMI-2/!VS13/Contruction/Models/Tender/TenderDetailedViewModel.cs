﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Contruction.SrvTenders;
using System.ComponentModel;
using SIMS.TM.Model;

namespace Contruction
{
    public class TenderDetailedViewModel:ViewModel
    {
        TenderServiceClient proxy = new TenderServiceClient();

        public TenderDetailedViewModel()
        {
            if (!DesignerProperties.GetIsInDesignMode(Application.Current.RootVisual))
            {
                OnInit();
                
            }
        }

        private void OnInit()
        {
            proxy.GetTenderByIDCompleted += new EventHandler<GetTenderByIDCompletedEventArgs>(proxy_GetTenderByIDCompleted);
        }

        void proxy_GetTenderByIDCompleted(object sender, GetTenderByIDCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                Base.ShowError(e.Error);
                IsBusy = false;
            }
            else
            {
                Tender = e.Result;
                IsBusy = false;
            }
        }

            public MTender _Tender;
			public MTender Tender
			{
				get{return _Tender;}
				set{_Tender = value;
				Notify("Tender");
				}
			}
        public void GetTender(int TenderID)
        {
            proxy.GetTenderByIDAsync(TenderID);
            IsBusy = true;
        }
    }
}
