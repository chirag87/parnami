﻿using System;
using System.Net;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Collections.ObjectModel;
using Contruction.PurchasingService;
//using SIMS.MM.MODEL;
using System.Collections.Generic;
using System.ComponentModel;
using Contruction.MasterDBService;

namespace Contruction
{
    public class NewItemModel : ViewModel
    {
        MasterDataServiceClient service = new MasterDataServiceClient();

        public NewItemModel()
        {
            if (!DesignerProperties.GetIsInDesignMode(Application.Current.RootVisual))
            {
                Reset();
                OnInit();
            }
        }

        #region Events...

        public event EventHandler ItemCategoriesLoaded;
        public void RaiseItemCategoriesLoaded()
        {
            if (ItemCategoriesLoaded != null)
                ItemCategoriesLoaded(this, new EventArgs());
        }

        public event EventHandler LeafAdded;
        public void RaiseLeafAdded()
        {
            if (LeafAdded != null)
                LeafAdded(this, new EventArgs());
        }

        #endregion

        #region Completed Events...

        void service_CheckforDuplicateItemCodeCompleted(object sender, CheckforDuplicateItemCodeCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                Base.ShowError(e.Error);
                IsBusy = false;
            }
            else
            {
                IsCodeDuplicate = e.Result;
                if (IsCodeDuplicate == true)
                    MessageBox.Show("Please Select Unique Item Code !");
                IsBusy = false;
            }
        }

        void service_GetItemCategoryLeavesCompleted(object sender, GetItemCategoryLeavesCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                Base.ShowError(e.Error);
                IsBusy = false;
            }
            else
            {
                LeafCategories = new ObservableCollection<MItemCategory>();
                SelectedLeafCategory = new MItemCategory();
                if (e.Result.Count() > 0)
                {
                    SelectedLeafCategory = e.Result.First();
                    LeafCategories = e.Result;
                }
                else
                {
                    SelectedLeafCategory = new MItemCategory(); 
                    ItemsByCategory = new ObservableCollection<MItem>();
                }
                IsBusy = false;
                Notify("LeafCategories");
            }
        }

        void service_GetItemsByCategoryIDCompleted(object sender, GetItemsByCategoryIDCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                Base.ShowError(e.Error);
                IsBusy = false;
            }
            else
            {
                ItemsByCategory = new ObservableCollection<MItem>();
                ItemsByCategory = e.Result;
                IsBusy = false;
                Notify("ItemsByCategory");
            }
        }

        void service_GetAllItemNamesCompleted(object sender, GetAllItemNamesCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                Base.ShowError(e.Error);
                IsBusy = false;
            }
            else
            {
                ItemNames = null;
                ItemNames = e.Result;
                IsBusy = false;
            }
        }

        void service_GetAllItemCategoriesCompleted(object sender, GetAllItemCategoriesCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                Base.ShowError(e.Error);
                IsBusy = false;
            }
            else
            {
                AllItemCategories = new ObservableCollection<MItemCategory>();
                AllItemCategories = e.Result;
                Notify("AllItemCategories");
                IsBusy = false;
            }
        }

        void service_GetAllMUCompleted(object sender, GetAllMUCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                Base.ShowError(e.Error);
                IsBusy = false;
            }
            else
            {
                GetAllMUs = e.Result;
                IsBusy = false;
            }
        }

        void service_CreateNewItemCompleted(object sender, System.ComponentModel.AsyncCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                MessageBox.Show(e.Error.ToString());
            }
            else
            {
                IsBusy = false;
                MessageBox.Show("New Item Added Successfully !");
                Base.AppEvents.RaiseNewItemAdded();
            }
        }

        void service_CreateItemCompleted(object sender, CreateItemCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                MessageBox.Show(e.Error.ToString());
            }
            else
            {
                IsBusy = false;
                MessageBox.Show("New Item Added Successfully !");
                var item = e.Result;
                LoadItemsForCategory(item.ItemCategoryID);
                Notify("ItemsByCategory");
            }
        }

        #endregion

        #region Properties...

        public bool CanEditItems { get { return Base.Current.Rights.Items_CanEdit; } }

        MItem _NewItem = new MItem();
        public MItem NewItem
        {
            get { return _NewItem; }
            set
            {
                _NewItem = value;
                Notify("NewItem");
            }
        }

        MItem _SelectedItem = new MItem();
        public MItem SelectedItem
        {
            get { return _SelectedItem; }
            set
            {
                _SelectedItem = value;
                Notify("SelectedItem");
            }
        }

        bool _IsCodeDuplicate = false;
        public bool IsCodeDuplicate
        {
            get { return _IsCodeDuplicate; }
            set
            {
                _IsCodeDuplicate = value;
                Notify("IsCodeDuplicate");
            }
        }

        string _ItemCategory;
        public string ItemCategory
        {
            get { return _ItemCategory; }
            set
            {
                _ItemCategory = value;
                Notify("ItemCategory");
            }
        }

        MItemCategory _ItemCategory2 = new MItemCategory();
        public MItemCategory ItemCategory2
        {
            get { return _ItemCategory2; }
            set
            {
                _ItemCategory2 = value;
                Notify("ItemCategory2");
            }
        }

        MItemCategory _ParentItemCategory = new MItemCategory();
        public MItemCategory ParentItemCategory
        {
            get { return _ParentItemCategory; }
            set
            {
                _ParentItemCategory = value;
                Notify("ParentItemCategory");
            }
        }

        MItemCategory _NewLeafCategory = new MItemCategory();
        public MItemCategory NewLeafCategory
        {
            get { return _NewLeafCategory; }
            set
            {
                _NewLeafCategory = value;
                Notify("NewLeafCategory");
            }
        }
        
        IEnumerable<MMeasurementUnit> _GetAllMUs;
        public IEnumerable<MMeasurementUnit> GetAllMUs
        {
            get { return _GetAllMUs; }
            set
            {
                _GetAllMUs = value;
                Notify("GetAllMUs");
            }
        }

        IEnumerable<string> _ItemNames;
        public IEnumerable<string> ItemNames
        {
            get { return _ItemNames; }
            set
            {
                _ItemNames = value;
                Notify("ItemNames");
            }
        }

        ObservableCollection<MItemCategory> _AllItemCategories = new ObservableCollection<MItemCategory>();
        public ObservableCollection<MItemCategory> AllItemCategories
        {
            get { return _AllItemCategories; }
            set
            {
                _AllItemCategories = value;
                Notify("AllItemCategories");
                RaiseItemCategoriesLoaded();
            }
        }

        ObservableCollection<MItem> _ItemsByCategory = new ObservableCollection<MItem>();
        public ObservableCollection<MItem> ItemsByCategory
        {
            get { return _ItemsByCategory; }
            set
            {
                _ItemsByCategory = value;
                Notify("ItemsByCategory");
            }
        }

        ObservableCollection<MItemCategory> _LeafCategories = new ObservableCollection<MItemCategory>();
        public ObservableCollection<MItemCategory> LeafCategories
        {
            get { return _LeafCategories; }
            set
            {
                _LeafCategories = value;
                Notify("LeafCategories");
            }
        }

        bool _IsVisibleAddNewItem = false;
        public bool IsVisibleAddNewItem
        {
            get { return _IsVisibleAddNewItem; }
            set
            {
                _IsVisibleAddNewItem = value;
                Notify("IsVisibleAddNewItem");
            }
        }

        bool _IsDetailViewVisible = false;
        public bool IsDetailViewVisible
        {
            get { return _IsDetailViewVisible; }
            set
            {
                _IsDetailViewVisible = value;
                Notify("IsDetailViewVisible");
            }
        }

        bool _IsNew = false;
        public bool IsNew
        {
            get { return _IsNew; }
            set
            {
                _IsNew = value;
                Notify("IsNew");
            }
        }

        string _SingleLeafName;
        public string SingleLeafName
        {
            get { return _SingleLeafName; }
            set
            {
                _SingleLeafName = value;
                Notify("SingleLeafName");
            }
        }

        string _SingleCategoryName;
        public string SingleCategoryName//Category for Which CanHaveItem is true
        {
            get { return _SingleCategoryName; }
            set
            {
                _SingleCategoryName = value;
                Notify("SingleCategoryName");
            }
        }

        int _SingleLeafID;
        public int SingleLeafID
        {
            get { return _SingleLeafID; }
            set
            {
                _SingleLeafID = value;
                Notify("SingleLeafID");
            }
        }

        int _ParentCategoryID;
        public int ParentCategoryID
        {
            get { return _ParentCategoryID; }
            set
            {
                _ParentCategoryID = value;
                Notify("ParentCategoryID");
            }
        }

        MItemCategory _SelectedLeafCategory = new MItemCategory();
        public MItemCategory SelectedLeafCategory
        {
            get { return _SelectedLeafCategory; }
            set
            {
                _SelectedLeafCategory = value;
                OnSelectedLeafCategoryChanged();
                Notify("SelectedLeafCategory");
            }
        }

        #endregion

        //public void AddNewLeafCategory(MItemCategory leaf)
        //{
        //    if (leaf.ID != 0)
        //        NewLeafCategory.ParentID = leaf.ID;
        //    NewLeafCategory.IsItem = true;
        //    AddNewItemCartegoryVM d=new AddNewItemCartegoryVM();
        //    d.AddNewLeafCategory(NewLeafCategory);

        //}

        #region Methods...

        public void OnSelectedLeafCategoryChanged()
        {
            var data = SelectedLeafCategory;
            if (data == null) return;
            this.SingleLeafName = data.Name;
            this.IsVisibleAddNewItem = true;
            this.ParentCategoryID = default(int);
            this.ParentCategoryID = data.ID;
            this.LoadItemsForCategory(data.ID);
        }

        public void OnInit()
        {
            service.CheckforDuplicateItemCodeCompleted += new EventHandler<CheckforDuplicateItemCodeCompletedEventArgs>(service_CheckforDuplicateItemCodeCompleted);
            service.CreateNewItemCompleted += new EventHandler<System.ComponentModel.AsyncCompletedEventArgs>(service_CreateNewItemCompleted);
            service.GetAllMUCompleted += new EventHandler<GetAllMUCompletedEventArgs>(service_GetAllMUCompleted);
            service.GetAllItemCategoriesCompleted += new EventHandler<GetAllItemCategoriesCompletedEventArgs>(service_GetAllItemCategoriesCompleted);
            service.GetAllItemNamesCompleted += new EventHandler<GetAllItemNamesCompletedEventArgs>(service_GetAllItemNamesCompleted);
            service.GetItemsByCategoryIDCompleted += new EventHandler<GetItemsByCategoryIDCompletedEventArgs>(service_GetItemsByCategoryIDCompleted);
            service.GetItemCategoryLeavesCompleted += new EventHandler<GetItemCategoryLeavesCompletedEventArgs>(service_GetItemCategoryLeavesCompleted);
            service.CreateItemCompleted += new EventHandler<CreateItemCompletedEventArgs>(service_CreateItemCompleted);
            LoadMUs();
            LoadItemNames();
            LoadItemCategories();
            Base.Current.NewItemAdded += new EventHandler(Current_NewItemAdded);
            Base.Current.SelectedItemLoaded += new EventHandler<Base.NewEventArgs>(Current_SelectedItemLoaded);
            Base.AppEvents.NewMURaised += new EventHandler(AppEvents_NewMURaised);
        }

        void Current_NewItemAdded(object sender, EventArgs e)
        {
            //service.CreateNewItemAsync(Base.Current.SelectedItem);
            //IsBusy = true;
        }

        void AppEvents_NewMURaised(object sender, EventArgs e)
        {
            LoadMUs();
        }

        void Current_SelectedItemLoaded(object sender, Base.NewEventArgs e)
        {
            SelectedItem = e.SelectedItem;
        }

        public void AddLeaf()
        {

        }

        public void CheckDuplicateCode(string code)
        {
            service.CheckforDuplicateItemCodeAsync(code);
            IsBusy = true;
        }

        public void SubmitToDB()
        {
            if (NewItem.MeasurementUnitID == null)
            {
                MessageBox.Show("Select Proper Measurement Unit"); 
                return; 
            }
            if (IsCodeDuplicate == true)
            {
                MessageBox.Show("Please Select Unique Item Code!");
                return;
            }
            //NewItem = SelectedItem;
            service.CreateNewItemAsync(NewItem, ItemCategory);
            IsBusy = true;
        }

        public void AddNewItem()
        {
            if (SelectedItem.MeasurementUnitID == null)
            {
                MessageBox.Show("Select Proper Measurement Unit");
                return;
            }
            SelectedItem.ItemCategoryID = ParentCategoryID;
            SelectedItem.Name = SelectedItem.DisplayName;
            SelectedItem.DisplayName = null;

            var checkforDuplicate = Base.Current.Items.SingleOrDefault(x => x.Name.ToLower() == SelectedItem.Name.ToLower() && x.ItemCategoryID == SelectedItem.ItemCategoryID);

            if(checkforDuplicate!=null)
            {
                MessageBox.Show("Item Already Exists");
                return;
            }

            service.CreateItemAsync(SelectedItem);
            IsBusy = true;
        }

        public void UpdateItemDetails()
        {
            SelectedItem.Name = SelectedItem.DisplayName;
            SelectedItem.DisplayName = null;
            Base.Current.UpdateItem(SelectedItem);
            IsBusy = true;
            Base.Current.ItemUpdated += new EventHandler(Current_ItemUpdated);
        }

        void Current_ItemUpdated(object sender, EventArgs e)
        {
            ReloadItemsForLeafCategory();
        }

        public void ReloadItemsForLeafCategory()
        {
            if (ParentCategoryID != 0)
            {
                LoadItemsForCategory(ParentCategoryID);
                Notify("ItemsByCategory");
                IsBusy = true;
            }
            else
            {
                MessageBox.Show("No Category Seleted!");
            }
        }

        public void AddNewLeafCategory()
        {
            if (ParentItemCategory.ID != 0)
            {
                NewLeafCategory.ParentID = ParentItemCategory.ID;
                NewLeafCategory.IsItem = true;
                AddNewItemCartegoryVM vm = new AddNewItemCartegoryVM();
                vm.AddNewLeafCategory(NewLeafCategory);
                RaiseLeafAdded();
            }
            else 
            {
                MessageBox.Show("No Parent Selected ! Try Again !");
            }
        }

        public void LoadMUs()
        {
            service.GetAllMUAsync();
            IsBusy = true;
        }

        public void Reset()
        {
            NewItem = new MItem();
            ItemCategory = "";
        }

        public void LoadItemNames()
        {
            service.GetAllItemNamesAsync();
            IsBusy = true;
        }

        public void LoadItemCategories()
        {
            service.GetAllItemCategoriesAsync();
            IsBusy = true;
        }

        public void LoadItemsForCategory(int categoryid)
        {
            if (categoryid != 0)
                service.GetItemsByCategoryIDAsync(categoryid);
            else
                ItemsByCategory = new ObservableCollection<MItem>();
            IsBusy = true;
        }

        public void LoadLeafCategories(int categoryid)
        {
            if (categoryid != 0)
            { 
                service.GetItemCategoryLeavesAsync(categoryid);
                IsBusy = true;
            }
            else
                LeafCategories = new ObservableCollection<MItemCategory>();
        }

        #endregion
    }
}
