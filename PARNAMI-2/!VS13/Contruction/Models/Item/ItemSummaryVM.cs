﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.ComponentModel;
using Contruction.ItemSummaryService;
using System.Collections.ObjectModel;

namespace Contruction
{
    public class ItemSummaryVM : ViewModel
    {
        ItemSummaryServiceClient service = new ItemSummaryServiceClient();

        public ItemSummaryVM()
        {
            if (!DesignerProperties.GetIsInDesignMode(Application.Current.RootVisual))
            {
                OnInit();
            }
        }

        #region Methods...

        public void OnInit()
        {
            service.GetItemQtyInIndentsCompleted += new EventHandler<GetItemQtyInIndentsCompletedEventArgs>(service_GetItemQtyInIndentsCompleted);
            service.GetItemQtyInPOsCompleted += new EventHandler<GetItemQtyInPOsCompletedEventArgs>(service_GetItemQtyInPOsCompleted);
            service.GetItemQtyOnSitesCompleted += new EventHandler<GetItemQtyOnSitesCompletedEventArgs>(service_GetItemQtyOnSitesCompleted);
            service.GetRateHistoryOfItemCompleted += new EventHandler<GetRateHistoryOfItemCompletedEventArgs>(service_GetRateHistoryOfItemCompleted);
        }

        public void Load()
        {
            service.GetItemQtyInIndentsAsync(itemid, sizeid, brandid);
            service.GetItemQtyInPOsAsync(itemid, sizeid, brandid);
            service.GetItemQtyOnSitesAsync(itemid, sizeid, brandid);
            service.GetRateHistoryOfItemAsync(itemid, sizeid, brandid);

            IsBusy = true;
        }

        #endregion

        #region CompletedEvents...

        void service_GetRateHistoryOfItemCompleted(object sender, GetRateHistoryOfItemCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                Base.ShowError(e.Error);
                IsBusy = false;
            }
            else
            {
                IsBusy = false;
                RateHistory = e.Result;
            }
        }

        void service_GetItemQtyOnSitesCompleted(object sender, GetItemQtyOnSitesCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                Base.ShowError(e.Error);
                IsBusy = false;
            }
            else
            {
                IsBusy = false;
                StockOnSites = e.Result;
            }
        }

        void service_GetItemQtyInPOsCompleted(object sender, GetItemQtyInPOsCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                Base.ShowError(e.Error);
                IsBusy = false;
            }
            else
            {
                IsBusy = false;
                StockInPO = e.Result;
            }
        }

        void service_GetItemQtyInIndentsCompleted(object sender, GetItemQtyInIndentsCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                Base.ShowError(e.Error);
                IsBusy = false;
            }
            else
            {
                IsBusy = false;
                StockInIndent = e.Result;
            }
        }

        #endregion

        #region Properties...

        int? _itemid;
        public int? itemid
        {
            get { return _itemid; }
            set
            {
                _itemid = value;
                Notify("itemid");
            }
        }

        int? _sizeid;
        public int? sizeid
        {
            get { return _sizeid; }
            set
            {
                _sizeid = value;
                Notify("sizeid");
            }
        }

        int? _brandid;
        public int? brandid
        {
            get { return _brandid; }
            set
            {
                _brandid = value;
                Load();
                Notify("brandid");
            }
        }

        ObservableCollection<MStockInINDENT> _StockInIndent = new ObservableCollection<MStockInINDENT>();
        public ObservableCollection<MStockInINDENT> StockInIndent
        {
            get { return _StockInIndent; }
            set
            {
                _StockInIndent = value;
                Notify("StockInIndent");
            }
        }

        ObservableCollection<MStockInPO> _StockInPO = new ObservableCollection<MStockInPO>();
        public ObservableCollection<MStockInPO> StockInPO
        {
            get { return _StockInPO; }
            set
            {
                _StockInPO = value;
                Notify("StockInPO");
            }
        }

        ObservableCollection<MStockOnSites> _StockOnSites = new ObservableCollection<MStockOnSites>();
        public ObservableCollection<MStockOnSites> StockOnSites
        {
            get { return _StockOnSites; }
            set
            {
                _StockOnSites = value;
                Notify("StockOnSites");
            }
        }

        ObservableCollection<MRateHistory> _RateHistory = new ObservableCollection<MRateHistory>();
        public ObservableCollection<MRateHistory> RateHistory
        {
            get { return _RateHistory; }
            set
            {
                _RateHistory = value;
                Notify("RateHistory");
            }
        }

        #endregion
    }
}
