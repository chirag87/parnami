﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Contruction.MasterDBService;
//using SIMS.MM.MODEL;
using System.ComponentModel;
namespace Contruction
{
    public class EditItemCategoryVM:ViewModel
    {
        public event EventHandler<UpdateItemCategoryArgs> Updated;
        public void RaiseUpdated(MItemCategory _ItemCategory)
        {
            if (Updated != null)
                Updated(this, new UpdateItemCategoryArgs(_ItemCategory));
        }

        MasterDataServiceClient proxy = new MasterDataServiceClient();

        public EditItemCategoryVM()
        {
            if (!DesignerProperties.GetIsInDesignMode(Application.Current.RootVisual))
            {

                OnInit();
            }
        }

        private void OnInit()
        {
            proxy.UpdateItemCategoryCompleted += new EventHandler<UpdateItemCategoryCompletedEventArgs>(proxy_UpdateItemCategoryCompleted);
        }

        void proxy_UpdateItemCategoryCompleted(object sender, UpdateItemCategoryCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                Base.ShowError(e.Error);
                IsBusy = false;
            }
            else
            {
                MessageBox.Show(" ItemCategory Updated Successfully !");
                RaiseUpdated(e.Result);
                IsBusy = false;
            }
        }

        public void SubmitToDB()
        {
            UpdatedItemCategory.ID = ItemCategory.ID;
            UpdatedItemCategory.Name = ItemCategory.Name;
            UpdatedItemCategory.ParentID = ItemCategory.ParentID;
            proxy.UpdateItemCategoryAsync(UpdatedItemCategory);            
        }

        MItemCategory _UpdatedItemCategory = new MItemCategory();
        public MItemCategory UpdatedItemCategory
        {
            get { return _UpdatedItemCategory; }
            set
            {
                _UpdatedItemCategory = value;
                Notify("NewItemCategory");
            }
        }

        MItemCategory _ItemCategory = new MItemCategory();
        public MItemCategory ItemCategory
        {
            get { return _ItemCategory; }
            set
            {
                _ItemCategory = value;
                Notify("ItemCategory");
            }
        }
    }
}
