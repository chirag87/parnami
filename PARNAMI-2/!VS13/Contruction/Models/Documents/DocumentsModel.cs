﻿using System;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.ComponentModel;
using Contruction.MasterDBService;
using System.Collections.Generic;
//using SIMS.MM.MODEL;
using System.Collections.ObjectModel;

namespace Contruction
{
    public class DocumentsModel : ViewModel
    {
        MasterDataServiceClient service = new MasterDataServiceClient();

        public DocumentsModel()
        {
            if (!DesignerProperties.GetIsInDesignMode(Application.Current.RootVisual))
            {
                OnInit();
            }
        }

        public void OnInit()
        {
            service.SaveDocumentsCompleted += new EventHandler<SaveDocumentsCompletedEventArgs>(service_SaveDocumentsCompleted);
        }

        void service_SaveDocumentsCompleted(object sender, SaveDocumentsCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                Base.ShowError(e.Error);
                IsBusy = false;
            }
            else
            {
                SelectedDocument = null;
                SelectedDocument = e.Result;
                MessageBox.Show("Document Saved !");
                Base.Current.GetDocumentsByForID(ForID.Value, For);
                IsBusy = false;
            }
        }

        public void SaveDoc()
        {
            service.SaveDocumentsAsync(FileName, For, ForID, DocType);
            IsBusy = true;
        }

        #region Model

        string _For = "";
        public string For
        {
            get { return _For; }
            set
            {
                _For = value;
                Notify("For");
            }
        }

        int? _ForID;
        public int? ForID
        {
            get { return _ForID; }
            set
            {
                _ForID = value;
                Notify("ForID");
            }
        }

        string _FileName = "";
        public string FileName
        {
            get { return "~/Upload/" + _FileName; }
            set
            {
                _FileName = value;
                Notify("FileName");
            }
        }

        public string DocType
        {
            get { return SelectedDocument.DocumentType; }
            set
            {
                SelectedDocument.DocumentType = value;
                Notify("DocType");
            }
        }

        MDocument _SelectedDocument = new MDocument();
        public MDocument SelectedDocument
        {
            get { return _SelectedDocument; }
            set
            {
                _SelectedDocument = value;
                Notify("SelectedDocument");
            }
        }

        #endregion
    }
}