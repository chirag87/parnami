﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.ComponentModel;
using System.Collections.ObjectModel;
using Contruction.MasterReportingService;
//using SIMS.MM.MODEL;

namespace Contruction
{
    public class MonthlyStockReportsModel : ViewModel
    {
        MasterReportingServiceClient service = new MasterReportingServiceClient();

        public MonthlyStockReportsModel()
        {
            if (!DesignerProperties.GetIsInDesignMode(Application.Current.RootVisual))
            {
                OnInit();
            }
        }

        #region Methods...

        public void OnInit()
        {
            service.GetMonthlyStockReportCompleted += new EventHandler<GetMonthlyStockReportCompletedEventArgs>(service_GetMonthlyStockReportCompleted);
        }

        public void Load(int siteid, int? itemCatID, int itemid, int? sizeid, int? brandid)
        {
            service.GetMonthlyStockReportAsync(sdate, edate, siteid, itemCatID, itemid, sizeid, brandid);
            IsBusy = true;
        }

        #endregion

        #region Completed Events...

        void service_GetMonthlyStockReportCompleted(object sender, GetMonthlyStockReportCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                Base.ShowError(e.Error);
                IsBusy = false;
            }
            else
            {
                Data = e.Result;
                IsBusy = false;
            }
        }

        #endregion

        #region Properties...

        ObservableCollection<MMonthlyStockReport> _Data = new ObservableCollection<MMonthlyStockReport>();
        public ObservableCollection<MMonthlyStockReport> Data
        {
            get { return _Data; }
            set
            {
                _Data = value;
                Notify("Data");
            }
        }

        DateTime? _sdate;
        public DateTime? sdate
        {
            get { return _sdate; }
            set
            {
                _sdate = value;
                Notify("sdate");
            }
        }

        DateTime? _edate;
        public DateTime? edate
        {
            get { return _edate; }
            set
            {
                _edate = value;
                Notify("edate");
            }
        }

        int? _SiteID;
        public int? SiteID
        {
            get { return _SiteID; }
            set
            {
                _SiteID = value;
                Notify("SiteID");
            }
        }

        int? _ItemCatID;
        public int? ItemCatID
        {
            get { return _ItemCatID; }
            set
            {
                _ItemCatID = value;
                Notify("ItemCatID");
            }
        }

        int? _ItemID;
        public int? ItemID
        {
            get { return _ItemID; }
            set
            {
                _ItemID = value;
                Notify("ItemID");
            }
        }

        int? _SizeID;
        public int? SizeID
        {
            get { return _SizeID; }
            set
            {
                _SizeID = value;
                Notify("SizeID");
            }
        }

        int? _BrandID;
        public int? BrandID
        {
            get { return _BrandID; }
            set
            {
                _BrandID = value;
                Notify("BrandID");
            }
        }

        int? _catid;
        public int? catid
        {
            get { return _catid; }
            set
            {
                _catid = value;
                Notify("catid");
            }
        }

        #endregion
    }
}
