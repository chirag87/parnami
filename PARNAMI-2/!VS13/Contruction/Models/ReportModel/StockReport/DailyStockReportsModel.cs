﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.ComponentModel;
using System.Collections.ObjectModel;
using Contruction.MasterReportingService;
//using SIMS.MM.MODEL;

namespace Contruction
{
    public class DailyStockReportsModel : ViewModel
    {
        MasterReportingServiceClient service = new MasterReportingServiceClient();

        public DailyStockReportsModel()
        {
            if (!DesignerProperties.GetIsInDesignMode(Application.Current.RootVisual))
            {
                OnInit();
            }
        }

        #region Methods...

        public void OnInit()
        {
            service.GetDailyStockReportCompleted += new EventHandler<GetDailyStockReportCompletedEventArgs>(service_GetDailyStockReportCompleted);
        }

        public void Load(DateTime sdate, DateTime edate, int siteid, int? itemCatID, int itemid, int? sizeid, int? brandid)
        {
            service.GetDailyStockReportAsync(sdate, edate, siteid, itemCatID, itemid, sizeid, brandid);
            IsBusy = true;
        }

        #endregion

        #region Completed Events...

        void service_GetDailyStockReportCompleted(object sender, GetDailyStockReportCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                Base.ShowError(e.Error);
                IsBusy = false;
            }
            else
            {
                Data = e.Result;
                IsBusy = false;
            }
        }

        #endregion

        #region Properties...

        double? _OpeningStock;
        public double? OpeningStock
        {
            get { return _OpeningStock; }
            set
            {
                _OpeningStock = value;
                Notify("OpeningStock");
            }
        }

        ObservableCollection<MDailyStockReport> _Data = new ObservableCollection<MDailyStockReport>();
        public ObservableCollection<MDailyStockReport> Data
        {
            get { return _Data; }
            set
            {
                _Data = value;
                Notify("Data");
            }
        }

        int? _SiteID;
        public int? SiteID
        {
            get { return _SiteID; }
            set
            {
                _SiteID = value;
                Notify("SiteID");
            }
        }

        int? _ItemCatID;
        public int? ItemCatID
        {
            get { return _ItemCatID; }
            set
            {
                _ItemCatID = value;
                Notify("ItemCatID");
            }
        }

        int? _ItemID;
        public int? ItemID
        {
            get { return _ItemID; }
            set
            {
                _ItemID = value;
                Notify("ItemID");
            }
        }

        int? _SizeID;
        public int? SizeID
        {
            get { return _SizeID; }
            set
            {
                _SizeID = value;
                Notify("SizeID");
            }
        }

        int? _BrandID;
        public int? BrandID
        {
            get { return _BrandID; }
            set
            {
                _BrandID = value;
                Notify("BrandID");
            }
        }

        #endregion
    }
}
