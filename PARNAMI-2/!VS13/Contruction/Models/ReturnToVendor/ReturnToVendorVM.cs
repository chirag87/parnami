﻿using System;
using System.Net;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.ComponentModel;
using System.Collections.ObjectModel;
//using SIMS.MM.MODEL;
using SIMS.Core;
using Contruction.ReturnToVendorService;

namespace Contruction
{
    public class ReturnToVendorVM : ViewModel
    {
        ReturnToVendorServiceClient service = new ReturnToVendorServiceClient();

        public ReturnToVendorVM()
        {
            if (!DesignerProperties.GetIsInDesignMode(Application.Current.RootVisual))
            {
                OnInit();
            }
        }

        #region Methods...

        public void OnInit()
        {
            service.GetAllReturnToVendorsCompleted += new EventHandler<GetAllReturnToVendorsCompletedEventArgs>(service_GetAllReturnToVendorsCompleted);
            service.GetRTVByIDCompleted += new EventHandler<GetRTVByIDCompletedEventArgs>(service_GetRTVByIDCompleted);
            service.CreateRTVCompleted += new EventHandler<CreateRTVCompletedEventArgs>(service_CreateRTVCompleted);
            service.UpdateRTVReceivingInfoCompleted += new EventHandler<UpdateRTVReceivingInfoCompletedEventArgs>(service_UpdateRTVReceivingInfoCompleted);
            service.DeleteRTVCompleted += new EventHandler<AsyncCompletedEventArgs>(service_DeleteRTVCompleted);
            service.ChangeConfirmationStatusCompleted += new EventHandler<ChangeConfirmationStatusCompletedEventArgs>(service_ChangeConfirmationStatusCompleted);
            Initiate();
            Load();
        }

        public void Initiate()
        {
            RTV.CreatedBy = Base.Current.UserName;
            RTV.LastUpdatedBy = Base.Current.UserName;
            RTV.ReturnedBy = Base.Current.UserName;
        }

        public void Load()
        {
            service.GetAllReturnToVendorsAsync(Base.Current.UserName, PageIndex, null, Key, VendorID, SiteID);
            IsBusy = true;
        }

        public void GetRTVByID(int id)
        {
            service.GetRTVByIDAsync(id);
            IsBusy = true;
        }

        public void SubmitToDB()
        {
            if (Validate())
            {
                service.CreateRTVAsync(RTV, Base.Current.UserName);
                Status = "";
                IsBusy = true;
            }
        }

        public void UpdateRTV()
        {
            if (ValidateUpdate())
            {
                service.UpdateRTVReceivingInfoAsync(RTV);
                Status = "";
                IsBusy = true;
            }
        }

        public void Delete(MReturnToVendor rtv)
        {
            service.DeleteRTVAsync(rtv);
            IsBusy = true;
        }

        public void DeleteLine(int lineid)
        {
            var rtvline = RTV.RTVLines.Single(x => x.LineID == lineid);
            RTV.RTVLines.Remove(rtvline);
            RTV.UpdateLineNumbers();
        }

        public void AddNewLine()
        {
            RTV.AddNewLine();
            Notify("RTV");
        }

        public void ChangeConfirmStatus(int id, bool status)
        {
            service.ChangeConfirmationStatusAsync(id, status);
            IsBusy = true;
        }

        public void Clear()
        {
            Key = "";
            VendorID = null;
            SiteID = null;
        }

        public void Reset()
        {
            RTV = new MReturnToVendor();
        }

        public bool Validate()
        {
            if (RTV.SiteID == 0) { Status = "Please Select a Site. Site can't go NULL !"; return false; }
            if (RTV.VendorID == 0) { Status = "Please Select a Vendor. Vendor can't go NULL !"; return false; }
            if (RTV.RTVLines.Any(x=>x.ItemID == 0)) { Status = "Please Select a valid Item !"; return false; }
            if (RTV.RTVLines.Any(x=>x.ConsumableType == null)) { Status = "Please Select Consumable Type !"; return false; }
            else return true;
        }

        public bool ValidateUpdate()
        {
            if (RTV.IsReceived == false) { Status = "Please Complete Receiving Details !"; return false; }
            if (RTV.ReceivingDate == null) { Status = "Receiving Date Can not be Null !"; return false; }
            if (String.IsNullOrWhiteSpace(RTV.VendorChallanNo)) { Status = "Please Fill Vendor Challan No. !"; return false; }
            else return true;
        }

        #endregion

        #region Completed Events...

        void service_ChangeConfirmationStatusCompleted(object sender, ChangeConfirmationStatusCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                Base.ShowError(e.Error);
                IsBusy = false;
            }
            else
            {
                RTV = e.Result;
                IsConfirmed = e.Result.IsReceived;
                Notify("CanRevert");
                Notify("CanConfirm");
                RaiseRTVUpdateCompleted();
                IsBusy = false;
            }
        }

        void service_DeleteRTVCompleted(object sender, AsyncCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                Base.ShowError(e.Error);
                IsBusy = false;
            }
            else
            {
                MessageBox.Show("Deletion Successful !");
                Load();
                IsBusy = false;
            }
        }

        void service_UpdateRTVReceivingInfoCompleted(object sender, UpdateRTVReceivingInfoCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                Base.ShowError(e.Error);
                IsBusy = false;
            }
            else
            {
                RTV = e.Result;
                MessageBox.Show("Updating Complete !");
                RaiseRTVUpdateCompleted();
                IsBusy = false;
            }
        }

        void service_CreateRTVCompleted(object sender, CreateRTVCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                Base.ShowError(e.Error);
                IsBusy = false;
            }
            else
            {
                RTV = e.Result;
                MessageBox.Show("Created Successfully !");
                RaiseRTVCreated();
                IsBusy = false;
            }
        }

        void service_GetRTVByIDCompleted(object sender, GetRTVByIDCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                Base.ShowError(e.Error);
                IsBusy = false;
            }
            else
            {
                RTV = e.Result;
                IsConfirmed = e.Result.IsReceived;
                Notify("CanRevert");
                Notify("CanConfirm");
                IsBusy = false;
            }
        }

        void service_GetAllReturnToVendorsCompleted(object sender, GetAllReturnToVendorsCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                Base.ShowError(e.Error);
                IsBusy = false;
            }
            else
            {
                AllRTVs = e.Result;
                IsBusy = false;
            }
        }

        #endregion

        #region Properties...

        IPaginated<MReturnToVendor> _AllRTVs = new PaginatedData<MReturnToVendor>();
        public IPaginated<MReturnToVendor> AllRTVs
        {
            get { return _AllRTVs; }
            set
            {
                _AllRTVs = value;
                Notify("AllRTVs");
                RaiseRTVDataChanged();
            }
        }

        MReturnToVendor _RTV = new MReturnToVendor();
        public MReturnToVendor RTV
        {
            get { return _RTV; }
            set
            {
                _RTV = value;
                Notify("RTV");
            }
        }

        int _PageIndex = 0;
        public int PageIndex
        {
            get { return _PageIndex; }
            set
            {
                _PageIndex = value;
                Notify("PageIndex");
                Load();
            }
        }

        int? _VendorID;
        public int? VendorID
        {
            get { return _VendorID; }
            set
            {
                _VendorID = value;
                Notify("VendorID");
            }
        }

        int? _SiteID;
        public int? SiteID
        {
            get { return _SiteID; }
            set
            {
                _SiteID = value;
                Notify("SiteID");
            }
        }

        string _Key;
        public string Key
        {
            get { return _Key; }
            set
            {
                _Key = value;
                Notify("Key");
            }
        }

        string _Status;
        public string Status
        {
            get { return _Status; }
            set
            {
                _Status = value;
                Notify("Status");
            }
        }

        bool _IsConfirmed = false;
        public bool IsConfirmed
        {
            get { return _IsConfirmed; }
            set
            {
                _IsConfirmed = value;
                Notify("IsConfirmed");
            }
        }

        public bool CanRevert
        {
            get { return (Base.Current.CanEdit && IsConfirmed); }
        }

        public bool CanConfirm
        {
            get { return (!IsConfirmed); }
        }

        #endregion

        #region Events...

        public event EventHandler RTVCreated;
        public void RaiseRTVCreated()
        {
            if (RTVCreated != null)
                RTVCreated(this, new EventArgs());
        }

        public event EventHandler RTVUpdateCompleted;
        public void RaiseRTVUpdateCompleted()
        {
            if (RTVUpdateCompleted != null)
                RTVUpdateCompleted(this, new EventArgs());
        }

        public event EventHandler RTVDataChanged;
        public void RaiseRTVDataChanged()
        {
            if (RTVDataChanged != null)
                RTVDataChanged(this, new EventArgs());
        }

        #endregion
    }
}
