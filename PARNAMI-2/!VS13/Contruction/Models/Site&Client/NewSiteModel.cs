﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Collections.ObjectModel;
using Contruction.PurchasingService;
//using SIMS.MM.MODEL;
using System.Collections.Generic;
using Contruction.MasterDBService;
using System.ComponentModel;

namespace Contruction
{
    public class NewSiteModel : ViewModel
    {
        MasterDataServiceClient service = new MasterDataServiceClient();

        public NewSiteModel()
        {
            if (!DesignerProperties.GetIsInDesignMode(Application.Current.RootVisual))
            {
                Reset();
                OnInit();
            }
        }

        //MSite _NewSite;
        //public MSite NewSite
        //{
        //    get { return _NewSite; }
        //    set
        //    {
        //        _NewSite = value;
        //        Notify("NewSite");
        //    }
        //}

        bool _IsAlreadyExist = false;
        public bool IsAlreadyExist
        {
            get { return _IsAlreadyExist; }
            set
            {
                _IsAlreadyExist = value;
                Notify("IsAlreadyExist");
            }
        }

        public void OnInit()
        {
            service.ValidateSiteNameCompleted += new EventHandler<ValidateSiteNameCompletedEventArgs>(service_ValidateSiteNameCompleted);
            service.ValidateSiteCodeCompleted += new EventHandler<ValidateSiteCodeCompletedEventArgs>(service_ValidateSiteCodeCompleted);
            service.CreateNewSiteCompleted += new EventHandler<System.ComponentModel.AsyncCompletedEventArgs>(service_CreateNewSiteCompleted);
            Base.Current.NewSiteAdded += new EventHandler(Current_NewSiteAdded);
        }

        public void ValidateSiteCode()
        {
            if (Base.Current.SelectedSite.SiteCode != null)
            {
                service.ValidateSiteCodeAsync(Base.Current.SelectedSite.SiteCode);
                IsBusy = true;
            }
        }

        public void ValidateSiteName()
        {
            if (Base.Current.SelectedSite.Name != null)
            {
                service.ValidateSiteNameAsync(Base.Current.SelectedSite.Name);
                IsBusy = true;
            }
        }

        void service_ValidateSiteCodeCompleted(object sender, ValidateSiteCodeCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                Base.ShowError(e.Error);
                IsBusy = false;
            }
            else
            {
                IsAlreadyExist = false;
                IsAlreadyExist = e.Result;
                IsBusy = false;
                if (IsAlreadyExist == true)
                    MessageBox.Show("Site Code Alredy Used !");
            }
        }

        void service_ValidateSiteNameCompleted(object sender, ValidateSiteNameCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                Base.ShowError(e.Error);
                IsBusy = false;
            }
            else
            {
                IsAlreadyExist = false;
                IsAlreadyExist = e.Result;
                IsBusy = false;
                if (IsAlreadyExist == true)
                    MessageBox.Show("Name Alredy Exists !");
            }
        }

        void Current_NewSiteAdded(object sender, EventArgs e)
        {
            if (Base.Current.SelectedSite.IsBlocked == false)
                service.CreateNewSiteAsync(Base.Current.SelectedSite);
            IsBusy = true;
            Base.Current.SelectedSite.IsBlocked = true;
        }

        void service_CreateNewSiteCompleted(object sender, System.ComponentModel.AsyncCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                Base.ShowError(e.Error);
                IsBusy = false;
                Base.Current.SelectedSite.IsBlocked = false;
            }
            else
            {
                Base.Current.SelectedSite.IsBlocked = true;
                IsBusy = false;
                MessageBox.Show("New Site Added Successfully !");
                Base.Current.LoadSites();
                Base.AppEvents.RaiseNewSiteAdded();
            }
        }

        public void SubmitToDB()
        {
            //service.CreateNewSiteAsync(NewSite);
            //IsBusy = true;
        }

        public void Reset()
        {
            //NewSite = new MSite();
        }
    }
}
