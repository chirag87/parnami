﻿using System;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Collections.ObjectModel;
//using SIMS.MM.MODEL;
using System.ComponentModel;
using Contruction.TransactionService;

namespace Contruction
{
    public class NewConsumptionModel : ViewModel
    {
        TransactionServiceClient newservice = new TransactionServiceClient();

        public NewConsumptionModel()
        {
            if (!DesignerProperties.GetIsInDesignMode(Application.Current.RootVisual))
            {
                Reset();
                OnInit();
            }
        }

        public void OnInit()
        {
            //service.CreateNewConsumptionCompleted += new EventHandler<CreateNewConsumptionCompletedEventArgs>(service_CreateNewConsumptionCompleted);
            newservice.CreateNewConsumptionCompleted += new EventHandler<TransactionService.CreateNewConsumptionCompletedEventArgs>(newservice_CreateNewConsumptionCompleted);
            NewConsumption.CreatedBy = Base.Current.UserName;
        }

        void newservice_CreateNewConsumptionCompleted(object sender, TransactionService.CreateNewConsumptionCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                Base.ShowError(e.Error);
                IsBusy = false;
            }
            else
            {
                IsBusy = false;
                CreatedConsumption = e.Result;
                MessageBox.Show("Reference No: " + CreatedConsumption.ReferenceNo + "\n\nNew Consumption Added Successfully !");
                Reset();
                Base.AppEvents.RaiseNewConsumptionAdded();
            }
        }

        //void service_CreateNewConsumptionCompleted(object sender, CreateNewConsumptionCompletedEventArgs e)
        //{
        //    if (e.Error != null)
        //    {
        //        Base.ShowError(e.Error);
        //        IsBusy = false;
        //    }
        //    else
        //    {
        //        IsBusy = false;
        //        CreatedConsumption = e.Result;
        //        MessageBox.Show("Reference No: " + CreatedConsumption.ReferenceNo+ "\n\nNew Consumption Added Successfully !");
        //        Reset();
        //        Base.AppEvents.RaiseNewConsumptionAdded();
        //    }
        //}

        //MReceiving _NewConsumption = new MReceiving();
        //public MReceiving NewConsumption
        //{
        //    get { return _NewConsumption; }
        //    set
        //    {
        //        _NewConsumption = value;
        //        Notify("NewConsumption");
        //    }
        //}

        //MReceiving _CreatedConsumption = new MReceiving();
        //public MReceiving CreatedConsumption
        //{
        //    get { return _CreatedConsumption; }
        //    set
        //    {
        //        _CreatedConsumption = value;
        //        Notify("CreatedConsumption");
        //    }
        //}

        MConsumption _NewConsumption = new MConsumption();
        public MConsumption NewConsumption
        {
            get { return _NewConsumption; }
            set
            {
                _NewConsumption = value;
                Notify("NewConsumption");
            }
        }

        MConsumption _CreatedConsumption = new MConsumption();
        public MConsumption CreatedConsumption
        {
            get { return _CreatedConsumption; }
            set
            {
                _CreatedConsumption = value;
                Notify("CreatedConsumption");
            }
        }

        public void AddNewLine()
        {
            NewConsumption.AddNewLine();
            Notify("NewConsumption");
        }

        public void SubmitToDB()
        {
            if (!Validate()) return;
            //service.CreateNewConsumptionAsync(NewConsumption, Base.Current.UserName);
            newservice.CreateNewConsumptionAsync(NewConsumption, Base.Current.UserName);
            IsBusy = true;
        }

        public void Reset()
        {
            //NewConsumption = new MReceiving();
            //NewConsumption.Purchaser = Base.Current.UserName;
            //NewConsumption.CreatedBy= Base.Current.UserName;
            //NewConsumption.PurchaseDate = DateTime.UtcNow.AddHours(5.5);
            NewConsumption = new MConsumption();
            NewConsumption.ConsumedBy= Base.Current.UserName;
            NewConsumption.CreatedBy = Base.Current.UserName;
            NewConsumption.ForDate = DateTime.UtcNow.AddHours(5.5);
            Notify("NewConsumption");
        }

        public void DeleteLine(int lid)
        {
            try
            {
                //var line = NewConsumption.ReceivingLines.Single(x => x.LineID == lid);
                //NewConsumption.ReceivingLines.Remove(line);
                var line = NewConsumption.ConsumptionLines.Single(x => x.LineID == lid);
                NewConsumption.ConsumptionLines.Remove(line);
                NewConsumption.UpdateLineNumbers();
            }
            catch { }
        }

        string _Status;
        public string Status
        {
            get { return _Status; }
            set
            {
                _Status = value;
                Notify("Status");
            }
        }

        public bool Validate()
        {
            //Status = "";
            //if (NewConsumption.SiteID == 0) { Status = "Please Select Site"; return false; }
            //if (String.IsNullOrWhiteSpace(NewConsumption.Purchaser)) { Status = "Requester Cannot be Empty!"; return false; }
            //if (NewConsumption.ReceivingLines == null) { Status = "Consumption should have alteast one item."; return false; }
            //if (NewConsumption.ReceivingLines.Count == 0) { Status = "Consumption should have alteast one item."; return false; }
            //if (NewConsumption.ReceivingLines.Any(x => x.ItemID == 0)) { Status = "Consumption Lines has some invaid/unselected Items!"; return false; }
            Status = "";
            if (NewConsumption.SiteID == 0) { Status = "Please Select Site"; return false; }
            if (String.IsNullOrWhiteSpace(NewConsumption.ConsumedBy)) { Status = "Requester Cannot be Empty!"; return false; }
            if (NewConsumption.ConsumptionLines == null) { Status = "Consumption should have alteast one item."; return false; }
            if (NewConsumption.ConsumptionLines.Count == 0) { Status = "Consumption should have alteast one item."; return false; }
            if (NewConsumption.ConsumptionLines.Any(x => x.ItemID == 0)) { Status = "Consumption Lines has some invaid/unselected Items!"; return false; }
            if (NewConsumption.ConsumptionLines.Any(x => x.Quantity == 0)) { Status = "Consumption Lines has some invaid/unfilled Quantity!"; return false; }
            return true;
        }
    }
}
