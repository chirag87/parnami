﻿using System;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Collections.ObjectModel;
//using SIMS.MM.MODEL;
using System.ComponentModel;
using Contruction.TransactionService;

namespace Contruction
{
    public class NewCashPurchaseModel : ViewModel
    {        
        TransactionServiceClient newservice = new TransactionServiceClient();

        public NewCashPurchaseModel()
        {
            if (!DesignerProperties.GetIsInDesignMode(Application.Current.RootVisual))
            {
                Reset();
                OnInit();
            }
        }

        public void OnInit()
        {
            //service.CreateNewCashPurchaseCompleted += new EventHandler<CreateNewCashPurchaseCompletedEventArgs>(service_CreateNewCashPurchaseCompleted);
            newservice.CreateNewCashPurchaseCompleted += new EventHandler<TransactionService.CreateNewCashPurchaseCompletedEventArgs>(newservice_CreateNewCashPurchaseCompleted);
        }

        void newservice_CreateNewCashPurchaseCompleted(object sender, TransactionService.CreateNewCashPurchaseCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                Base.ShowError(e.Error);
                IsBusy = false;
            }
            else
            {
                IsBusy = false;
                CreatedTransaction = e.Result;
                MessageBox.Show("Reference No: " + CreatedTransaction.ReferenceNo + "\n\nNew Cash Purchase Added Successfully !");
                Reset();
                Base.AppEvents.RaiseNewCashPurchaseAdded();
            }
        }

        //void service_CreateNewCashPurchaseCompleted(object sender, CreateNewCashPurchaseCompletedEventArgs e)
        //{
        //    if (e.Error != null)
        //    {
        //        Base.ShowError(e.Error);
        //        IsBusy = false;
        //    }
        //    else
        //    {
        //        IsBusy = false;
        //        CreatedTransaction = e.Result;
        //        MessageBox.Show("Reference No: " + CreatedTransaction.ReferenceNo + "\n\nNew Cash Purchase Added Successfully !");
        //        Reset();
        //        Base.AppEvents.RaiseNewCashPurchaseAdded();
        //    }
        //}

        //MReceiving _NewTransaction;
        //public MReceiving NewTransaction
        //{
        //    get { return _NewTransaction; }
        //    set
        //    {
        //        _NewTransaction = value;
        //        Notify("NewTransaction");
        //    }
        //}

        //MReceiving _CreatedTransaction;
        //public MReceiving CreatedTransaction
        //{
        //    get { return _CreatedTransaction; }
        //    set
        //    {
        //        _CreatedTransaction = value;
        //        Notify("CreatedTransaction");
        //    }
        //}

        MCashPurchase _NewTransaction = new MCashPurchase();
        public MCashPurchase NewTransaction
        {
            get { return _NewTransaction; }
            set
            {
                _NewTransaction = value;
                Notify("NewTransaction");
            }
        }

        MCashPurchase _CreatedTransaction = new MCashPurchase();
        public MCashPurchase CreatedTransaction
        {
            get { return _CreatedTransaction; }
            set
            {
                _CreatedTransaction = value;
                Notify("CreatedTransaction");
            }
        }

        public void AddNewLine()
        {
            //MessageBox.Show("VendorID:" + NewTransaction.VendorID);
            NewTransaction.AddNewLine();
            Notify("NewTransaction");
        }

        public void SubmitToDB()
        {
            if (!Validate()) return;
            //service.CreateNewCashPurchaseAsync(NewTransaction, Base.Current.UserName);
            newservice.CreateNewCashPurchaseAsync(NewTransaction, Base.Current.UserName);
            IsBusy = true;
        }

        public void Reset()
        {
            //NewTransaction = new MReceiving();
            //NewTransaction.Purchaser = Base.Current.UserName;
            //NewTransaction.PurchaseDate = DateTime.UtcNow.AddHours(5.5);
            NewTransaction = new MCashPurchase();
            NewTransaction.Requester = Base.Current.UserName;
            NewTransaction.ForDate = DateTime.UtcNow.AddHours(5.5);
            Notify("NewTransaction");
        }

        public void DeleteLine(int lid)
        {
            try
            {
                //var line = NewTransaction.ReceivingLines.Single(x => x.LineID == lid);
                //NewTransaction.ReceivingLines.Remove(line);
                var line = NewTransaction.CashPurchaseLines.Single(x => x.LineID == lid);
                NewTransaction.CashPurchaseLines.Remove(line);
                NewTransaction.UpdateLineNumbers();
            }
            catch { }
        }

        public void UpdateGrossLinePrice()
        {
            NewTransaction.UpdateGrossLinePrice();
            Notify("NewTransaction");
        }

        public void UpdateTotalLinePrice()
        {
            NewTransaction.UpdateTotalLinePrice();
            Notify("NewTransaction");
        }

        public void UpdateTaxAmount()
        {
            NewTransaction.UpdateTaxAmount();
            Notify("NewTransaction");
        }

        string _Status;
        public string Status
        {
            get { return _Status; }
            set
            {
                _Status = value;
                Notify("Status");
            }
        }

        public bool Validate()
        {
            Status = "";
            if (!NewTransaction.VendorID.HasValue) { Status = "Please Select Vendor"; return false; }
            if (NewTransaction.SiteID == 0) { Status = "Please Select Site"; return false; }
            //if (String.IsNullOrWhiteSpace(NewTransaction.Purchaser)) { Status = "Requester Cannot be Empty!"; return false; }
            //if (NewTransaction.ReceivingLines == null) { Status = "Cash Purchase should have alteast one item."; return false; }
            //if (NewTransaction.ReceivingLines.Count == 0) { Status = "Cash Purchase should have alteast one item."; return false; }
            //if (NewTransaction.ReceivingLines.Any(x => x.ItemID == 0)) { Status = "Cash Purchase Lines has some invaid/unselected Items!"; return false; }
            if (String.IsNullOrWhiteSpace(NewTransaction.Requester)) { Status = "Requester Cannot be Empty!"; return false; }
            if (NewTransaction.CashPurchaseLines == null) { Status = "Cash Purchase should have alteast one item."; return false; }
            if (NewTransaction.CashPurchaseLines.Count == 0) { Status = "Cash Purchase should have alteast one item."; return false; }
            if (NewTransaction.CashPurchaseLines.Any(x => x.ItemID == 0)) { Status = "Cash Purchase Lines has some invaid/unselected Items!"; return false; }
            if (NewTransaction.CashPurchaseLines.Any(x => x.Quantity == 0)) { Status = "Cash Purchase Lines has some invaid/unfilled Quantity!"; return false; }
            return true;
        }
    }
}
