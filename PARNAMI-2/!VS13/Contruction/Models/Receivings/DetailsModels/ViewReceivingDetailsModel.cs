﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Collections.ObjectModel;
//using SIMS.MM.MODEL;
using System.ComponentModel;
using Contruction.TransactionService;

namespace Contruction
{
    public class ViewReceivingDetailsModel : ViewModel
    {
        TransactionServiceClient newservice = new TransactionServiceClient();

        public ViewReceivingDetailsModel()
        {
            if (!DesignerProperties.GetIsInDesignMode(Application.Current.RootVisual))
            {
                OnInit();
            }
        }

        public void OnInit()
        {
            //service.GetGRNByIDCompleted += new EventHandler<GetGRNByIDCompletedEventArgs>(service_GetGRNByIDCompleted);
            newservice.GetGRNByIDCompleted += new EventHandler<TransactionService.GetGRNByIDCompletedEventArgs>(newservice_GetGRNByIDCompleted);
        }

        void newservice_GetGRNByIDCompleted(object sender, TransactionService.GetGRNByIDCompletedEventArgs e)
        {
            if (e.Error == null)
            {
                NewGRN = e.Result;
                IsBusy = false;
                IsOld = true;
            }
            else
            {
                Base.ShowError(e.Error);
                IsBusy = false;
            }
        }

        //void service_GetGRNByIDCompleted(object sender, GetGRNByIDCompletedEventArgs e)
        //{
        //    if (e.Error == null)
        //    {
        //        NewGRN = e.Result;
        //        IsBusy = false;
        //        IsOld = true;
        //    }
        //    else
        //    {
        //        Base.ShowError(e.Error);
        //        IsBusy = false;
        //    }
        //}

        void Current_GRNRequested(object sender, ReceivingRequestedArgs e)
        {
            int recevingid = e.GRNID;
            SetID(recevingid);
        }

        //MReceiving _NewGRN;
        //public MReceiving NewGRN
        //{
        //    get { return _NewGRN; }
        //    set
        //    {
        //        _NewGRN = value;
        //        Notify("NewGRN");
        //    }
        //}

        MGRN _NewGRN = new MGRN();
        public MGRN NewGRN
        {
            get { return _NewGRN; }
            set
            {
                _NewGRN = value;
                Notify("NewGRN");
            }
        }

        bool _IsOld = false;
        public bool IsOld
        {
            get { return _IsOld; }
            set
            {
                _IsOld = value;
                Notify("IsOld");
                Notify("CanEdit");
                Notify("IsLocked");
            }
        }

        public override bool CanEdit
        {
            get
            {
                return !IsOld;
            }
        }

        public bool CanPrint
        {
            get
            {
                return true;
            }
        }

        internal void SetID(int p)
        {
            //service.GetGRNByIDAsync(p);
            newservice.GetGRNByIDAsync(p);
            IsBusy = true;
        }
    }
}
