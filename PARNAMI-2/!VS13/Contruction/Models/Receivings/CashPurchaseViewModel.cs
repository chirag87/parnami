﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Collections.ObjectModel;
//using SIMS.MM.MODEL;
using System.ComponentModel;
using SIMS.Core;
using Contruction.TransactionService;

namespace Contruction
{
    public class CashPurchaseViewModel : ViewModel
    {
        TransactionServiceClient newservice = new TransactionServiceClient();

        public CashPurchaseViewModel()
        {
            if (!DesignerProperties.GetIsInDesignMode(Application.Current.RootVisual))
            {
                OnInit();
            }
        }

        public event EventHandler CPChanged;
        public void RaiseCPChanged()
        {
            if (CPChanged != null)
                CPChanged(this, new EventArgs());
        }

        int _PageIndex = 0;
        public int PageIndex
        {
            get { return _PageIndex; }
            set
            {
                _PageIndex = value;
                Notify("PageIndex");
                LoadCashPurchases();
            }
        }

        public void OnInit()
        {
            //service.GetAllCashPurchasesCompleted += new EventHandler<GetAllCashPurchasesCompletedEventArgs>(service_GetAllCashPurchasesCompleted);
            newservice.GetAllCashPurchasesCompleted += new EventHandler<TransactionService.GetAllCashPurchasesCompletedEventArgs>(newservice_GetAllCashPurchasesCompleted);
            LoadCashPurchases();
            Base.AppEvents.CashPurchseDataChanged += new EventHandler(AppEvents_CashPurchseDataChanged);
        }

        void newservice_GetAllCashPurchasesCompleted(object sender, TransactionService.GetAllCashPurchasesCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                Base.ShowError(e.Error);
                IsBusy = false;
            }
            else
            {
                CashPurchases = e.Result;
                IsBusy = false;
            }
        }

        void AppEvents_CashPurchseDataChanged(object sender, EventArgs e)
        {
            LoadCashPurchases();
        }

        //void service_GetAllCashPurchasesCompleted(object sender, GetAllCashPurchasesCompletedEventArgs e)
        //{
        //    if (e.Error != null)
        //    {
        //        Base.ShowError(e.Error);
        //        IsBusy = false;
        //    }
        //    else
        //    {
        //        CashPurchases = e.Result;
        //        IsBusy = false;
        //    }
        //}

        //IPaginated<MReceivingHeader> _CashPurchases;
        //public IPaginated<MReceivingHeader> CashPurchases
        //{
        //    get { return _CashPurchases; }
        //    set
        //    {
        //        _CashPurchases = value;
        //        Notify("CashPurchases");
        //        RaiseCPChanged();
        //    }
        //}

        IPaginated<MCashPurchase> _CashPurchases;
        public IPaginated<MCashPurchase> CashPurchases
        {
            get { return _CashPurchases; }
            set
            {
                _CashPurchases = value;
                Notify("CashPurchases");
                RaiseCPChanged();
            }
        }

        string _SearchKey;
        public string SearchKey
        {
            get { return _SearchKey; }
            set
            {
                _SearchKey = value;
                Notify("SearchKey");
            }
        }

        int? _VendorID;
        public int? VendorID
        {
            get { return _VendorID; }
            set
            {
                _VendorID = value;
                Notify("VendorID");
            }
        }

        int? _SiteID;
        public int? SiteID
        {
            get { return _SiteID; }
            set
            {
                _SiteID = value;
                Notify("SiteID");
            }
        }

        public void Clear()
        {
            SearchKey = null;
            VendorID = null;
            SiteID = null;
            LoadCashPurchases();
        }

        public void LoadCashPurchases()
        {
            //service.GetAllCashPurchasesAsync(Base.Current.UserName, PageIndex, null, SearchKey, VendorID, SiteID);\
            newservice.GetAllCashPurchasesAsync(Base.Current.UserName, PageIndex, null, SearchKey, VendorID, SiteID);
            IsBusy = true;
        }

        internal void MoveToPage(int p)
        {
            PageIndex = p;
        }
    }
}
