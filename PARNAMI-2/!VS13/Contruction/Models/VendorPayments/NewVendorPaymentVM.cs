﻿using System;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using SIMS.MM.MODEL.Headers;
using Contruction.SrvVendorPayments;
using System.ComponentModel;
using System.Collections.ObjectModel;

namespace Contruction
{
    public class NewVendorPaymentVM:ViewModel
    {
        VendorPaymentsServiceClient service = new VendorPaymentsServiceClient();


        #region Core
        public NewVendorPaymentVM()
        {
            if (!DesignerProperties.GetIsInDesignMode(Application.Current.RootVisual))
            {
                OnInit();
            }
        }

        public void OnInit()
        {
            service.CreateNewVendorPaymentCompleted += new EventHandler<CreateNewVendorPaymentCompletedEventArgs>(service_CreateNewVendorPaymentCompleted);
        }

        void service_CreateNewVendorPaymentCompleted(object sender, CreateNewVendorPaymentCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                Base.ShowError(e.Error);
                IsBusy = false;
            }
            else
            {
                MessageBox.Show("New Payment Generated !");
                IsBusy = false;
                Reset();
                Base.AppEvents.RaiseNewVendorPaymentAdded();
            }
        }
        #endregion

        #region Properties

        MVendorPayment _VendorPayment = new MVendorPayment();
        public MVendorPayment VendorPayment
        {
            get { return _VendorPayment; }
            set
            {
                _VendorPayment = value;
                Notify("VendorPayment");
                Notify("CanEditHeader");
                Notify("CanEditDetails");
            }
        }

        public void NotifyCans()
        {
            Notify("CanEditHeader");
            Notify("CanEditDetails");
        }

        public bool CanEditHeader
        {
            get { return (VendorID == default(int)) || SiteID == default(int); }
        }

        public bool CanEditDetails
        {
            get { return !CanEditHeader; }
        }

        #endregion

        public void SubmitToDB()
        {
            if (!Validate()) return;
            service.CreateNewVendorPaymentAsync(VendorPayment);
            IsBusy = true;
        }

        public int VendorID 
        { 
            get 
            { 
                return VendorPayment.VendorID; 
            }
            set 
            { 
                VendorPayment.VendorID = value;
                NotifyCans(); 
            }
        }

        public int SiteID 
        { 
            get 
            { 
                return VendorPayment.SiteID; 
            } 
            set 
            { 
                VendorPayment.SiteID = value; 
                NotifyCans(); 
            } 
        }

        string _Status;
        public string Status
        {
            get { return _Status; }
            set
            {
                _Status = value;
                Notify("Status");
            }
        }
        
        public void AddNewLine()
        {
            PendingBills BillsPopup = new PendingBills();
            BillsPopup.Model.VendorID = VendorID;
            BillsPopup.Model.SiteID = SiteID;
            BillsPopup.CreatePopup();
            BillsPopup.Selected += (s, e) =>
                {
                    foreach (var item in BillsPopup.PaymentDetailList)
                    {
                        if (VendorPayment.Details.Any(x => x.BillID == item.BillID)) continue;
                        VendorPayment.Details.Add(item);
                        UpdateNetAmount();
                        UpdateTotal();
                    }
                    NotifyCans();
                };
        }

        public void UpdateTotal()
        {
            VendorPayment.UpdateTotal();
            Notify("VendorPayment");
        }

        public void UpdateNetAmount()
        {
            VendorPayment.UpdateNetAmount();
            Notify("VendorPayment");
        }

        public void UpdateTDS()
        {
            VendorPayment.UpdateTDS();
            Notify("VendorPayment");
        }

        public void DeleteLine(int id)
        {
            VendorPayment.DeleteLine(id);
            Notify("VendorPayment");
        }

        public void loaddataInGrid(ObservableCollection<MVendorPaymentDetail> details)
        {
            VendorPayment.Details = details;
            Notify("VendorPayment");
        }

        public void Reset()
        {
            VendorPayment = new MVendorPayment();
        }

        public bool Validate()
        {
            Status = "";
            if (VendorPayment.SiteID == 0) { Status = "Please Select Site"; return false; }
            if (String.IsNullOrWhiteSpace(VendorPayment.PayedBy)) { Status = "Payed By Cannot be Empty!"; return false; }
            if (String.IsNullOrWhiteSpace(VendorPayment.Mode)) { Status = "Payment Mode Cannot be Empty!"; return false; }
            if (String.IsNullOrWhiteSpace(VendorPayment.PayeeName)) { Status = "Payee Name Cannot be Empty!"; return false; }
            if (String.IsNullOrWhiteSpace(VendorPayment.GivenTo)) { Status = "GivenTo Cannot be Empty!"; return false; }
            if (VendorPayment.Details == null) { Status = "Payment should have alteast one item."; return false; }
            if (VendorPayment.Details.Count == 0) { Status = "Payment should have alteast one item."; return false; }
            if (VendorPayment.Details.Any(x => x.Payment == 0)) { Status = "Items has some invaid/unselected Amount!"; return false; }
            return true;
        }
    }
}