﻿using System;
using System.Net;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Contruction.MOService;
using System.ComponentModel;
//using SIMS.MM.MODEL;
using System.Collections.ObjectModel;

namespace Contruction
{
    public class NewMOfromSplittedPRModel : ViewModel
    {
         MOServiceClient service = new MOServiceClient();

         public NewMOfromSplittedPRModel()
        {
            if (!DesignerProperties.GetIsInDesignMode(Application.Current.RootVisual))
            {
                OnInit();
            }
        }

        public class SplittedMORequestedArgs : EventArgs
        {
            public MMO mo { get; set; }
            public SplittedMORequestedArgs(MMO _mo) { mo = _mo; }
        }

        public event EventHandler<SplittedMORequestedArgs> SaveSplittedMO;
        public void RaiseSaveSplittedMO(MMO newMO)
        {
            if (SaveSplittedMO != null)
                SaveSplittedMO(this, new SplittedMORequestedArgs(newMO));
        }

        public event EventHandler CloseMOScreen;
        public void RaiseCloseMOScreen()
        {
            if (CloseMOScreen != null)
                CloseMOScreen(this, new EventArgs());
        }

        MMO _NewMO;
        public MMO NewMO
        {
            get { return _NewMO; }
            set
            {
                _NewMO = value;
                Notify("NewMO");
            }
        }

        string _Status;
        public string Status
        {
            get { return _Status; }
            set
            {
                _Status = value;
                Notify("Status");
            }
        }

        public void OnInit()
        {
            //service.SplitToMOCompleted += new EventHandler<SplitToMOCompletedEventArgs>(service_SplitToMOCompleted);
            service.SplitToMOwithPartialQtyCompleted += new EventHandler<SplitToMOwithPartialQtyCompletedEventArgs>(service_SplitToMOwithPartialQtyCompleted);
        }

        void service_SplitToMOwithPartialQtyCompleted(object sender, SplitToMOwithPartialQtyCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                Base.ShowError(e.Error);
                IsBusy = false;
            }
            else
            {
                IsBusy = false;
                CreatedMO = e.Result;
                MessageBox.Show("Created / Updated TO with Reference No: " + CreatedMO.ReferenceNo);
                Base.AppEvents.RaisePRDataChanged();
                Base.AppEvents.RaiseNewMOAdded();
                RaiseCloseMOScreen();
            }
        }

        //void service_SplitToMOCompleted(object sender, SplitToMOCompletedEventArgs e)
        //{
        //    if (e.Error != null)
        //    {
        //        Base.ShowError(e.Error);
        //        IsBusy = false;
        //    }
        //    else
        //    {
        //        IsBusy = false;
        //        CreatedMO = e.Result;
        //        MessageBox.Show("Reference No: " + CreatedMO.ReferenceNo + "\n\nNew Transfer Order created successfully!");
        //        //Base.AppEvents.RaiseNewMOAdded();
        //    }
        //}

        //public void CreateNewSplittedMO(int OldPRID, ObservableCollection<int> lineIDs)
        //{
        //    if (!Validate()) return;
        //    service.SplitToMOAsync(NewMO, OldPRID, lineIDs);
        //    IsBusy = true;
        //}

        //public void SplitttoMOwithPartialQty(int OldPRID, ObservableCollection<MSplittedLines> lines)
        //{
        //    if (!Validate()) return;
        //    service.SplitToMOwithPartialQtyAsync(NewMO, OldPRID, lines);
        //    IsBusy = true;
        //}

        public void SplitttoMOwithPartialQty(MMO newMO, int OldPRID, ObservableCollection<MMOSplittedLine> lines)
        {
            if (!Validate(newMO)) return;
            IsBusy = true;
            service.SplitToMOwithPartialQtyAsync(newMO, OldPRID, lines);
        }

        MMO _CreatedMO;
        public MMO CreatedMO
        {
            get { return _CreatedMO; }
            set
            {
                _CreatedMO = value;
                Notify("CreatedMO");
            }
        }

        public bool Validate(MMO NewMO)
        {
            Status = "";
            if (NewMO.RequestingSiteID == 0) { Status = "Please Select Requesting Site"; return false; }
            if (NewMO.TransferringSiteID == 0) { Status = "Please Select Transferring Site"; return false; }
            if (NewMO.TransferringSiteID == NewMO.RequestingSiteID) { Status = "Both Sites cann not be same"; return false; }
            if (String.IsNullOrWhiteSpace(NewMO.Requester)) { Status = "Requester Cannot be Empty!"; return false; }
            if (NewMO.MOLines == null) { Status = "Transfer Order should have alteast one item."; return false; }
            //if (NewMO.MOLines.Count == 0) { Status = "Transfer Order should have alteast one item."; return false; }
            //if (NewMO.MOLines.Any(x => x.ItemID == 0)) { Status = "Transfer Order Lines has some invaid/unselected Items!"; return false; }
            //if (NewMO.MOLines.Any(X => X.Quantity == 0)) { Status = "Transfer Order Lines must have some Quantity of Item!"; return false; }
            return true;
        }
    }
}
