﻿using System;
using System.Net;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Contruction.MOService;
//using SIMS.MM.MODEL;
using System.ComponentModel;

namespace Contruction
{
    public class MOReceiveModel : ViewModel
    {
        MOServiceClient service = new MOServiceClient();

        public event EventHandler Received;
        private void RaiseReceive()
        {
            if (Received != null)
                Received(this, new EventArgs());
        }

        public MOReceiveModel()
        {
            if (!DesignerProperties.GetIsInDesignMode(Application.Current.RootVisual))
            {
                OnInit();
            }
        }

        public void OnInit()
        {
            //service.GetMReceivingforMOReceiveCompleted += new EventHandler<GetMReceivingforMOReceiveCompletedEventArgs>(service_GetMReceivingforMOReceiveCompleted);
            //service.ReceiveMOCompleted += new EventHandler<ReceiveMOCompletedEventArgs>(service_ReceiveMOCompleted);
            service.GetMOReceiveTransactionByMOIDCompleted += new EventHandler<GetMOReceiveTransactionByMOIDCompletedEventArgs>(service_GetMOReceiveTransactionByMOIDCompleted);
            service.GetMOReceivingAgainstMOReleaseCompleted += new EventHandler<GetMOReceivingAgainstMOReleaseCompletedEventArgs>(service_GetMOReceivingAgainstMOReleaseCompleted);
            service.MOReceiveCompleted += new EventHandler<MOReceiveCompletedEventArgs>(service_MOReceiveCompleted);
            ReceivingforReceive.ReceivedBy = Base.Current.UserName;
        }

        void service_MOReceiveCompleted(object sender, MOReceiveCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                Base.ShowError(e.Error);
                IsBusy = false;
            }
            else
            {
                IsBusy = false;
                IsReceived = true;
                TransactionID = e.Result;
                MessageBox.Show("TO Received Successfully from " + ReceivingforReceive.TransferringSite);
                Base.AppEvents.RaiseNewMOAdded();
                RaiseReceive();
            }
        }

        void service_GetMOReceivingAgainstMOReleaseCompleted(object sender, GetMOReceivingAgainstMOReleaseCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                Base.ShowError(e.Error);
                IsBusy = false;
            }
            else
            {
                ReceivingforReceive = e.Result;
                ReceivingforReceive.ReceivedBy = Base.Current.UserName;
                this.TransporterID = e.Result.TranporterID;
                var lines = ReceivingforReceive.MOReceivingLines.ToList();
                int i = 1;
                foreach (var line in lines)
                {
                    line.Quantity = (line.AlreadyReleasedQty - line.EarlierReceivedQty);
                    line.LineID = i;
                    i++;
                }
                IsBusy = false;
            }
        }

        void service_GetMOReceiveTransactionByMOIDCompleted(object sender, GetMOReceiveTransactionByMOIDCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                Base.ShowError(e.Error);
                IsBusy = false;
            }
            else
            {
                ReceivingforReceive = e.Result;
                ReceivingforReceive.ReceivedBy = Base.Current.UserName;
                this.TransporterID = e.Result.TranporterID;
                var lines = ReceivingforReceive.MOReceivingLines.ToList();
                int i = 1;
                foreach (var line in lines)
                {
                    line.Quantity = (line.AlreadyReleasedQty - line.EarlierReceivedQty);
                    line.LineID = i;
                    i++;
                }
                IsBusy = false;
            }
        }

        //void service_ReceiveMOCompleted(object sender, ReceiveMOCompletedEventArgs e)
        //{
        //    if (e.Error != null)
        //    {
        //        Base.ShowError(e.Error);
        //        IsBusy = false;
        //    }
        //    else
        //    {
        //        IsBusy = false;
        //        IsReceived = true;
        //        TransactionID = e.Result;
        //        MessageBox.Show("MO Received Successfully from " + ReceivingforRelease.TransferSite);
        //        Base.AppEvents.RaiseNewMOAdded();
        //        RaiseReceive();
        //    }
        //}

        //void service_GetMReceivingforMOReceiveCompleted(object sender, GetMReceivingforMOReceiveCompletedEventArgs e)
        //{
        //    if (e.Error != null)
        //    {
        //        Base.ShowError(e.Error);
        //        IsBusy = false;
        //    }
        //    else
        //    {
        //        ReceivingforRelease = e.Result;
        //        ReceivingforRelease.Purchaser = Base.Current.UserName;
        //        IsBusy = false;
        //    }
        //}

        //MReceiving _ReceivingforRelease = new MReceiving();
        //public MReceiving ReceivingforRelease
        //{
        //    get { return _ReceivingforRelease; }
        //    set
        //    {
        //        _ReceivingforRelease = value;
        //        Notify("ReceivingforRelease");
        //    }
        //}

        MMOReceiving _ReceivingforReceive = new MMOReceiving();
        public MMOReceiving ReceivingforReceive
        {
            get { return _ReceivingforReceive; }
            set
            {
                _ReceivingforReceive = value;
                Notify("ReceivingforReceive");
            }
        }

        int _TransactionID;
        public int TransactionID
        {
            get { return _TransactionID; }
            set
            {
                _TransactionID = value;
                Notify("TransactionID");
            }
        }

        public void GetReceivingforMOReceive(int moid, int? relid, bool ReceiveforaRelease)
        {
            //service.GetMReceivingforMOReceiveAsync(moid);
            if (ReceiveforaRelease && relid != null)
                service.GetMOReceivingAgainstMOReleaseAsync(moid, relid.Value);
            else
                service.GetMOReceiveTransactionByMOIDAsync(moid);
            IsBusy = true;
        }

        public void MOReceive()
        {
            if (!Validate())
            {
                IsReceived = false;
                return;
            }
            //service.ReceiveMOAsync(ReceivingforRelease);
            ReceivingforReceive.TranporterID = this.TransporterID;
            service.MOReceiveAsync(ReceivingforReceive, Base.Current.UserName);
            IsBusy = true;
        }

        string _Status;
        public string Status
        {
            get { return _Status; }
            set
            {
                _Status = value;
                Notify("Status");
            }
        }

        bool _IsReceived = false;
        public bool IsReceived
        {
            get { return _IsReceived; }
            set
            {
                _IsReceived = value;
                Notify("IsReceived");
            }
        }

        int? _TransporterID;
        public int? TransporterID
        {
            get { return _TransporterID; }
            set
            {
                _TransporterID = value;
                Notify("TransporterID");
            }
        }

        public bool Validate()
        {
            Status = "";
            if (ReceivingforReceive.MOReceivingLines.Any(x => x.Quantity == 0.0)) { Status = "Transfer Order has some invaid/unselected Quantity!"; return false; }
            return true;
        }
    }
}
