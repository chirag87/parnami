﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using SIMS.MM.MODEL;
using System.Collections.Generic;
using Contruction.MTRService;
using System.Collections.ObjectModel;
using System.ComponentModel;

namespace Contruction
{
    public class MTRViewModel : ViewModel
    {
        MTRServiceClient service = new MTRServiceClient();

        public MTRViewModel()
        {
            if (!DesignerProperties.GetIsInDesignMode(Application.Current.RootVisual))
            {
                OnInit();
            }
        }

        public event EventHandler MTRChanged;
        public void RaiseMTRChanged()
        {
            if (MTRChanged != null)
                MTRChanged(this, new EventArgs());
        }

        IPaginated<MMTRHeader> _MTRs;
        public IPaginated<MMTRHeader> MTRs
        {
            get { return _MTRs; }
            set
            {
                _MTRs = value;
                Notify("MTRs");
                RaiseMTRChanged();
            }
        }

        int _PageIndex = 0;
        public int PageIndex
        {
            get { return _PageIndex; }
            set
            {
                _PageIndex = value;
                Notify("PageIndex");
                LoadMTRs();
            }
        }

        public void OnInit()
        {
            service.GetAllMTRsCompleted += new EventHandler<GetAllMTRsCompletedEventArgs>(service_GetAllMTRsCompleted);
            LoadMTRs();
            Base.AppEvents.MTRDataChanged += new EventHandler(AppEvents_MTRDataChanged);
        }

        void AppEvents_MTRDataChanged(object sender, EventArgs e)
        {
            LoadMTRs();
        }

        void service_GetAllMTRsCompleted(object sender, GetAllMTRsCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                Base.ShowError(e.Error);
            }
            else
            {
                MTRs = e.Result;
            }
        }

        string _SearchKey;
        public string SearchKey
        {
            get { return _SearchKey; }
            set
            {
                _SearchKey = value;
                Notify("SearchKey");
            }
        }

        int? _SiteID;
        public int? SiteID
        {
            get { return _SiteID; }
            set
            {
                _SiteID = value;
                Notify("SiteID");
            }
        }

        public void Clear()
        {
            SearchKey = null;
            SiteID = null;
            LoadMTRs();
        }

        public void LoadMTRs()
        {
            service.GetAllMTRsAsync(PageIndex, null, SearchKey, SiteID);
        }

        internal void MoveToPage(int p)
        {
            PageIndex = p;
        }
    }
}
