﻿using System;
using System.Net;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using SIMS.MM.MODEL;
using System.Collections.Generic;
using Contruction.MTRService;
using System.Collections.ObjectModel;
using System.ComponentModel;

namespace Contruction
{
    public class NewMTRModel : ViewModel
    {
        MTRServiceClient service = new MTRServiceClient();

        public NewMTRModel()
        {
            if (!DesignerProperties.GetIsInDesignMode(Application.Current.RootVisual))
            {
                Reset();
                OnInit();
            }
        }

        MMTR _NewMTR;
        public MMTR NewMTR
        {
            get { return _NewMTR; }
            set
            {
                _NewMTR = value;
                Notify("NewMTR");
            }
        }

        public void OnInit()
        {
            service.CreateNewMTRCompleted += new EventHandler<System.ComponentModel.AsyncCompletedEventArgs>(service_CreateNewMTRCompleted);
        }

        void service_CreateNewMTRCompleted(object sender, System.ComponentModel.AsyncCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                Base.ShowError(e.Error);
            }
            else
            {
                MessageBox.Show("New MTR Added Successfully !");
                Base.AppEvents.RaiseNewMTRAdded();
            }
        }

        public void AddNewLine()
        {
            //MessageBox.Show("SiteID:" + NewMTR.SiteID);
            NewMTR.AddNewLine();
            Notify("NewMTR");
        }

        public void SubmitToDB()
        {
            if (!Validate()) return;
            service.CreateNewMTRAsync(NewMTR);
        }

        public void Reset()
        {
            NewMTR = new MMTR();
            NewMTR.Requester = Base.Current.UserName;
            NewMTR.TransferDate = DateTime.Now;
            Notify("NewMTR");
        }

        public void DeleteLine(int lid)
        {
            try
            {
                var line = NewMTR.MTRLines.Single(x => x.LineID == lid);
                NewMTR.MTRLines.Remove(line);
            }
            catch { }
        }

        string _Status;
        public string Status
        {
            get { return _Status; }
            set
            {
                _Status = value;
                Notify("Status");
            }
        }

        public bool Validate()
        {
            Status = "";
            if (NewMTR.RequestingSiteID == 0) { Status = "Please Select Requesting Site"; return false; }
            if (NewMTR.TransferringSiteID == 0) { Status = "Please Select Transferring Site"; return false; }
            if (String.IsNullOrWhiteSpace(NewMTR.Requester)) { Status = "Requester Cannot be Empty!"; return false; }
            if (NewMTR.MTRLines == null) { Status = "Material Transfer Request should have alteast one item."; return false; }
            if (NewMTR.MTRLines.Count == 0) { Status = "Direch Material Transfer Request should have alteast one item."; return false; }
            if (NewMTR.MTRLines.Any(x => x.ItemID == 0)) { Status = "Material Transfer Request Lines has some invaid/unselected Items!"; return false; }
            return true;
        }
    }
}
