﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Collections.Generic;
using Innosols.Web.Services;
using Contruction;
using System.Collections.ObjectModel;
using Contruction.MasterDBService;

namespace Contruction
{
    public partial class Base : ViewModel
    {
        ItemsContext _ItemsDB;
        public ItemsContext ItemsDB
        {
            get { return _ItemsDB; }
        }

        void OnCreatedForItems()
        {
            _ItemsDB = new ItemsContext();
            //ItemsDB.PropertyChanged += new System.ComponentModel.PropertyChangedEventHandler(ItemsDB_PropertyChanged);
            InitForItems();
        }

        void ItemsDB_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "IsLoading" || e.PropertyName == "IsSubmitting")
                Notify("IsItemsDBBusy");
        }

        public bool IsItemsDBBusy
        {
            //get { return ItemsDB.IsLoading || ItemsDB.IsSubmitting; }
            get { return false; }
        }

        public event EventHandler ItemsLoaded;
        public void RaiseItemsLoaded()
        {
            if (ItemsLoaded != null)
                ItemsLoaded(this, new EventArgs());
        }

        public void InitForItems()
        {
        }

        public void LoadMeasurementUnits()
        {
        }

        public void LoadDealTypes()
        { }

        public void LoadItemsCategories()
        {
        }
    }
}

namespace Innosols.Web.Services
{
    public partial class ItemsContext
    {
    }
}
