﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace Contruction
{
    public partial class Base : ViewModel
    {
        public void OnCreatedForPO()
        {
        }

        void POService_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {

            if (e.PropertyName == "IsLoading" || e.PropertyName == "IsSubmitting")
                Notify("IsPOServiceBusy");
        }
        public bool IsPOServiceBusy
        {
            get { return false; }
        }

        public void InitForPOs()
        {
            LoadCostCenters();
            LoadPurchasingTypes();
        }

        public void LoadCostCenters()
        {
        }

        public void LoadPurchasingTypes()
        {
        }

        public void EmailPR(int prid)
        {
        }
    }
}
