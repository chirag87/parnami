﻿using System;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
//using SIMS.MM.MODEL;

namespace Contruction
{
    using SrvLogin;
    using System.Collections.Generic;

    public partial class Base
    {
        LoginServiceClient loginService = new LoginServiceClient();

        string _LoginStatus;
        public string LoginStatus
        {
            get { return _LoginStatus; }
            set
            {
                _LoginStatus = value;
                Notify("LoginStatus");
            }
        }

        public event EventHandler LoginStatusChanged;
        public void RaiseLoginStatusChanged()
        {
            if (LoginStatusChanged != null)
                LoginStatusChanged(this, new EventArgs());
        }

        void AfterLogin()
        {
            IsLoggedIn = true;
            this.AtLoginChanged();
            this.NotifyOnLogin();
        }

        public void Login(string username, string password)
        {
            loginService = new LoginServiceClient();
            loginService.LoginCompleted += new EventHandler<LoginCompletedEventArgs>(loginService_LoginCompleted);
            loginService.LoginAsync(username, password);
        }

        public void LogOut()
        {
            Rights = new UserRights();
            Rights.SetAllCans(false);
            LoadAll();
        }

        public void Lock()
        {
            IsLoggedIn = false;
            IsLocked = true;
        }

        bool _IsLocked;
        public bool IsLocked
        {
            get { return _IsLocked; }
            set
            {
                _IsLocked = value;
                Notify("IsLocked");
            }
        }

        void loginService_LoginCompleted(object sender, LoginCompletedEventArgs e)
        {
            if (e.Error == null)
            {
                Rights = e.Result;
                //LoadAllMessages();
                RaiseLoginStatusChanged();
            }
            else
            {
                LoginStatus = e.Error.Message;
                RaiseLoginStatusChanged();
            }
        }

        UserRights _Rights = new UserRights();
        public UserRights Rights
        {
            get { return _Rights; }
            set
            {
                _Rights = value;
                SetRights();
                Notify("Rights");
            }
        }

        public void SetRights()
        {
            IsLoggedIn = Rights.IsValid;
            UserName = Rights.UserName;
            LoginStatus = Rights.Status;

            if (Rights.Sites_CanViewAllDisplaySites)
            {
                MySiteIDs = Sites.Select(x => x.ID).ToList();
            }
            else
            {
                if (Rights.AllowedSiteIDs == null)
                    Rights.AllowedSiteIDs = new List<int>().ToArray();
                MySiteIDs = Rights.AllowedSiteIDs.ToList();
            }
            this.AtLoginChanged();
            this.NotifyOnLogin();
        }

        public void ChangePassword(string olspsswrd, string newpsswrd)
        {
            loginService.ChangePasswordCompleted += new EventHandler<ChangePasswordCompletedEventArgs>(loginService_ChangePasswordCompleted);
            loginService.ChangePasswordAsync(Base.Current.UserName, olspsswrd, newpsswrd);
            IsBusy = true;
        }

        bool _IsPasswordChanged = false;
        public bool IsPasswordChanged
        {
            get { return _IsPasswordChanged; }
            set
            {
                _IsPasswordChanged = value;
                Notify("IsPasswordChanged");
            }
        }

        void loginService_ChangePasswordCompleted(object sender, ChangePasswordCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                Base.ShowError(e.Error);
                IsPasswordChanged = false;
                IsBusy = false;
            }
            else
            {
                IsPasswordChanged = e.Result;
                MessageBox.Show("Password Updated Successfully !");
                IsBusy = false;
            }
        }
    }
}
