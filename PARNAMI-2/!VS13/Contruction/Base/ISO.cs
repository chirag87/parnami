﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.IO.IsolatedStorage;
using System.IO;
using System.Net.NetworkInformation;

namespace Contruction
{
    public class ISO
    {
        public static int QuotaSize = 104857600;
        #region FileStorageMethods
        public static void SaveData(string data, string fileName)
        {
            using (IsolatedStorageFile isf = IsolatedStorageFile.GetUserStoreForApplication())
            {
                if (isf.Quota != QuotaSize)
                {
                    isf.IncreaseQuotaTo(QuotaSize);
                }
                using (IsolatedStorageFileStream isfs = new IsolatedStorageFileStream(fileName, FileMode.Create, isf))
                {
                    using (StreamWriter sw = new StreamWriter(isfs))
                    {
                        sw.Write(data);
                        sw.Close();
                    }
                }
            }
        }

        public static string LoadData(string fileName)
        {
            string data = String.Empty;
            using (IsolatedStorageFile isf = IsolatedStorageFile.GetUserStoreForApplication())
            {
                if (!isf.FileExists(fileName))
                {
                    return "";
                }
                using (IsolatedStorageFileStream isfs = new IsolatedStorageFileStream(fileName, FileMode.Open, isf))
                {
                    using (StreamReader sr = new StreamReader(isfs))
                    {
                        string lineOfData = String.Empty;
                        while ((lineOfData = sr.ReadLine()) != null)
                            data += lineOfData;
                    }
                }
            }
            return data;
        }
        #endregion

        public static IsolatedStorageSettings KeyStore 
        {
            get
            {
                return IsolatedStorageSettings.ApplicationSettings;
            }
        }

        public static void Save()
        {
            KeyStore.Save();
        }

        public static Boolean IsOffilineMode
        {
            get
            {
                return !NetworkInterface.GetIsNetworkAvailable();
            }
        }
    }
}
