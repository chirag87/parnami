﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Contruction.ReceivingService;
using System.ComponentModel;

namespace Contruction
{
    public class ReturnToVendorVM:ViewModel
    {
        ReceivingServiceClient service = new ReceivingServiceClient();

        public ReturnToVendorVM()
        {
            if (!DesignerProperties.GetIsInDesignMode(Application.Current.RootVisual))
            {
                OnInit();
            }
        }

        private void OnInit()
        {
            service.CreateNewRTVCompleted += new EventHandler<CreateNewRTVCompletedEventArgs>(service_CreateNewRTVCompleted);
        }

        void service_CreateNewRTVCompleted(object sender, CreateNewRTVCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                Base.ShowError(e.Error);
                IsBusy = false;
            }
            else
            {
                IsBusy = false;
                //CreatedTransaction = e.Result;
                //MessageBox.Show("Reference No: " + CreatedTransaction.ReferenceNo + "\n\nNew Direct Purchase Added Successfully !");
                //Reset();
                //Base.AppEvents.RaiseNewDirectPurchaseAdded();
            }
        }
    }
}
