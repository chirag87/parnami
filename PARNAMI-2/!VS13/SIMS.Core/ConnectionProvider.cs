﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SIMS
{
    public class ConnectionProvider
    {
        public static string ForLINQ2SQL()
        {
            var conn = System.Web.Configuration.WebConfigurationManager.ConnectionStrings["dbConnection"].ConnectionString;
            return conn;
        }

        public static string ForEntities()
        {
            var conn = System.Web.Configuration.WebConfigurationManager.ConnectionStrings["entityConnection"].ConnectionString;
            return conn;
        }
    }
}
