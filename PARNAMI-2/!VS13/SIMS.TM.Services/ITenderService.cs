﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using SIMS.TM.Model;

namespace SIMS.TM.Services
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "ITenderService" in both code and config file together.
    [ServiceContract]
    public interface ITenderService
    {
        [OperationContract]
        MTender GetTenderDatails(int id);

        [OperationContract]
        PaginatedData<MTender> GetAllTenders(string username, int? pageindex, int? pagesize, string Key, int? sideid, bool showall = true);

        [OperationContract]
        MTender GetTenderByID(int TenderID);

        [OperationContract]
        MTender CreateNewTender(MTender _NewTender);

        [OperationContract]
        MTender UpdateTender(MTender _UpdatedTender);
    }
}
