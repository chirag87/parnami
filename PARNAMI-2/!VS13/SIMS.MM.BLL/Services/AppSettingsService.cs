﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SIMS.MM.BLL.Services
{
    using DAL;
    using MODEL;
    public class AppSettingsService : IAppsettings
    {

        public MAppSettings GetAppSettings()
        {
            return GetLatestAppSettings();
        }

        public static MAppSettings GetLatestAppSettings()
        {
            parnamidb1Entities db = new parnamidb1Entities();
            return db.AppSettings.ToList().Select(x => new MAppSettings()
              {
                  ID = x.ID,
                  Address1 = x.Address1,
                  Address2 = x.Address2,
                  Address3 = x.Address3,
                  Address4 = x.Address4,
                  CompanyName = x.CompanyName,
                  Email = x.Email,
                  LogoPath = x.LogoPath,
                  mailfrom = x.mailfrom,
                  mailreplyto = x.mailreplyto,
                  mailserver = x.mailserver,
                  mailserverpassword = x.mailserverpassword,
                  mailserverusername = x.mailserverusername,
                  Mobile = x.Mobile,
                  Phone = x.Phone,
                  ShowLogo = x.ShowLogo,
                  City = x.City,
                  CompanyTitle1 = x.CompanyTitle1,
                  CompanyTitle2 = x.CompanyTitle2,
                  Country = x.Country,
                  FaxNo = x.Fax,
                  State = x.State,
                  WebSite = x.Website,
                  HasAddress4 = x.HasAddress4,
                  HasAddress3 = x.HasAddress3,
                  HasCompanyTitle1 = x.HasCompanyTitle1,
                  HasCompanyTitle2 = x.HasCompanyTitle2,
                  HasEmail = x.HasEmail,
                  HasFax = x.HasFax,
                  HasMobile = x.HasMobile,
                  HasPhone = x.HasPhone,
                  HasTinNo = x.HasTinNo,
                  HasServiceTaxNo = x.HasServiceTaxNo,
                  ServiceTaxNo = x.ServiceTaxNo,
                  HasWebsite = x.HasWebsite,
                  PinCode = x.Pincode,
                  TinNo = x.TinNo,
                  FontColor = x.FontColor,
                  FontStyle = x.FontStyle
              }).Last();
        }
    }
}
