﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Entity;
using System.Collections.ObjectModel;

namespace SIMS.MM.BLL
{
    using DAL;
    using MODEL;
    using System.Collections.ObjectModel;
    using SIMS.Core;
    public class MTRDataProvider : IMTRDataProvider
    {
        parnamidb1Entities db = new parnamidb1Entities();

        //public IQueryable<MODEL.MMTRHeader> GetMTRHeaders(int? pageindex, int? pagesize, string Key, int? vendorid, int? sideid, int? sideid2)
        //{
        //  return  db.MTRs
        //      .OrderBy(x=>x.ID)
        //      .ToList()
        //      .Select(x => new MMTRHeader()
        //        {
        //            ID=x.ID,
        //            IsApproved=x.IsApproved,
        //            Date=x.TransferBefore,
        //            Remarks=x.Remarks,
        //            RequestedBy=x.RequestedBy,
        //            RequestedBySite=x.BUs.Name,
        //            TransferringSite=x.BUs1.Name,
        //            ApprovalStatus=x.ApprovalStatus,
        //            ReceiveStatus=x.ReceivedStatus,
        //            ReleaseStatus=x.ReleasedStatus
        //        }).AsQueryable();            
                
        //}

        public IPaginated<MMTRHeader> GetMTRHeaders(int? pageindex, int? pagesize, string Key, int? vendorid, int? sideid, int? sideid2)
        {
            return db.MTRs
               .OrderBy(x => x.ID)
               .ToList()
               .ToPaginate(pageindex, pagesize)
               .Select(x => new MMTRHeader()
               {
                   ID = x.ID,
                   IsApproved = x.IsApproved,
                   Date = x.TransferBefore,
                   Remarks = x.Remarks,
                   RequestedBy = x.RequestedBy,
                   RequestedBySite = x.BUs.Name,
                   TransferringSite = x.BUs1.Name,
                   ApprovalStatus = x.ApprovalStatus,
                   //ReceiveStatus = x.ReceivedStatus,
                   //ReleaseStatus = x.ReleasedStatus
               });   
        }

        public MMTR GetMTRbyId(int mtrid)
        {
            var mtr = db.MTRs.Single(x => x.ID == mtrid);
            return ConvertToMMTR(mtr);
        }

        public MMTR ConvertToMMTR(MTR _mtr)
        {
            MMTR mmtr = new MMTR()
            {
                 MTRNo=_mtr.ID,
                 MTRRemarks=_mtr.Remarks,
                 Requester=_mtr.RequestedBy,
                 RequestingSiteID=_mtr.RequestedSiteID,
                 TransferringSiteID=_mtr.TransferringSiteID,
                 TransferDate=_mtr.TransferBefore                    
            };
            mmtr.MTRLines = new ObservableCollection<MMTRLine>();
            _mtr.MTRLines.Select(x => ConvertTOMTR_Line(x)).ToList()
                .ForEach(x => mmtr.MTRLines.Add(x));
            return mmtr;
        }

        public MMTRLine ConvertTOMTR_Line(MTRLine _mtrline)
        {
            MMTRLine mmtrline = new MMTRLine()
            {
                LineID = _mtrline.LineID,
                ApprovedQty = _mtrline.ApprovedQty,
                InTransitQty = _mtrline.InTransitQty,
                Item = _mtrline.Item.DisplayName,
                ItemID = _mtrline.ItemID,
                SizeID = _mtrline.SizeID,
                BrandID = _mtrline.BrandID,
                LineRemarks = _mtrline.Remarks,
                MTRID = _mtrline.MTRID,
                Quantity = _mtrline.RequiredQty,
                ReleasedQty = _mtrline.ReleasedQty,
                TransferredQty = _mtrline.TransferredQty
            };
            if (_mtrline.ItemBrand != null)
                mmtrline.Brand = _mtrline.ItemBrand.Value;
            if (_mtrline.ItemSize != null)
                mmtrline.Size = _mtrline.ItemSize.Value;
            return mmtrline;
        }

    }
}
