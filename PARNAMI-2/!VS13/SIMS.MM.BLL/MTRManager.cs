﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SIMS.MM.BLL
{
    using DAL;
    using MODEL;
    public class MTRManager:IMTRManager
    {
        parnamidb1Entities db = new parnamidb1Entities();
        public MMTR CreateNewMTR(MMTR newMTR)
        {
            var qry=ConvertToMTR(newMTR);
            qry.ID = db.GetMAX_MTR_ID() + 1;
            qry.RaisedBy = "dba";
            qry.RaisedOn = DateTime.UtcNow.AddHours(5.5);
            qry.CurrentOwnerID = "dba";
            db.MTRs.AddObject(qry);
            db.SaveChanges();
            return newMTR;
        }

        //public void DeleteMTR(int MTRId)
        //{
        //    var data=db.MTRs.Single(x=>x.ID==MTRId);
        //    db.MTRs.DeleteObject(data);
        //    db.SaveChanges();
        //}

        MTR ConvertToMTR(MMTR _mmtr)
        {
            MTR mtr = new MTR()
            {
                ID=_mmtr.MTRNo,
                Remarks=_mmtr.MTRRemarks,
                RequestedBy=_mmtr.Requester,
                RequestedSiteID=_mmtr.RequestingSiteID,
                TransferringSiteID=_mmtr.TransferringSiteID,
                TransferBefore=_mmtr.TransferDate

            };
            if (_mmtr.MTRLines != null) _mmtr.MTRLines.ToList()
                   .ForEach(x => mtr.MTRLines.Add(ConvertToMTRLine(x)));
            //mtr.MTRLines.Add(ConvertToMTRLine(x));
            return mtr;
        }

        MTRLine ConvertToMTRLine(MMTRLine _mmtrline)
        {
            MTRLine line = new MTRLine()
            {
                LineID = _mmtrline.LineID,
                ItemID = _mmtrline.ItemID,
                Remarks = _mmtrline.LineRemarks,
                MTRID = _mmtrline.MTRID,// ?
                RequiredQty = _mmtrline.Quantity,
                SizeID = _mmtrline.SizeID,
                BrandID = _mmtrline.BrandID,
            };
            return line;
        }

        public void ApproveMTR(int MTRId)
        {
           var obj= db.MTRs.Single(x => x.ID == MTRId);
           obj.IsApproved = true;
           obj.ApprovedBy = "dba";
           obj.ApprovedOn = DateTime.UtcNow.AddHours(5.5);
           db.SaveChanges();          
        }

        public void ReleaseMTR(int MTRId)
        {
            var obj = db.MTRs.Single(x => x.ID == MTRId);           
            if (obj.IsApproved != true)
                throw new Exception("MTR Not Approved");
            //obj.CanRelease = true;
            obj.ReleasedOn = DateTime.UtcNow.AddHours(5.5);
            db.SaveChanges(); 
        }

        public void ReceiveMTR(int MTRId)
        {
            var obj = db.MTRs.Single(x => x.ID == MTRId);
            if (obj.IsApproved != true)
                throw new Exception("MTR Not Approved");
            //obj.CanReceive = true;
            obj.ReceivedOn = DateTime.UtcNow.AddHours(5.5);
            db.SaveChanges(); 
        }
    }
}
