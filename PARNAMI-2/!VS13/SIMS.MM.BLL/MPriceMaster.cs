﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SIMS.MM.BLL
{
    public class MPriceMaster
    {
        public int ID { get; set; }

        public int VendorID { get; set; }

        public int ItemID { get; set; }

        public int? SiteID { get; set; }

        public double Price { get; set; }

        public double QtyFor { get; set; }

        public string UM { get; set; }

        public double UnitPrice { get; set; }

        public DateTime EffectiveFrom { get; set; }

        public DateTime? EffectiveTill { get; set; }

        public DateTime CreatedOn { get; set; }

        public string CreatedBy { get; set; }

        public DateTime UpdatedOn { get; set; }

        public string UpdatedBy { get; set; }
    }
}
