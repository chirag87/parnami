﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace SIMS.MM.BLL.Emailer
{
    using DAL;
    using System.IO;
    using System.Configuration;

    public class Emailer
    {
        public static string from = "dkbuildcon@gmail.com";

        public static void SMSPO(string mobile, PR pr, bool isForApproval)
        {
            string EmailTxtFormat = "" +
@"
<header>
PO/PR: <poid>
Site: <site>
Vendor: <vendor>
Amount: <amount>
Raised by: <raisedby> on <raisedon>

Line Details:
<lines>

Remarks: <remarks>
";
            // Line Format
            string LineFormat =
                "Line: {0}\nItem: {1}\nQty: {2}\nUnit Price: {3}\nLine Price: {4}\nDelivery Date: {5}\nRemarks: {6}" + "\n\n";
            string LineHeader = String.Format(LineFormat,
                "Line", "Item", "Qty", "Unit Price", "LinePrice", "Delivery Date", "Remarks");
            string Lines = "";
            foreach (var line in pr.PRDetails)
            {
                Lines += String.Format(LineFormat,
                line.ID, String.Format("{0} ({1})", line.Item.ItemCategory.Name, line.Item.Name), line.Qty + " " + line.Item.MeasurementID, line.UnitPrice, line.LinePrice, line.DeliveryDate.Value.ToShortDateString(), line.Remarks);
            }

            string headerTxt = "";

            if (isForApproval)
            {
                headerTxt = "PR For Approval";
            }
            else
            {
                headerTxt = "PO Raised as";
            }

            Replacements r1 = new Replacements();
            r1.AddNew(new object[]{
                "poid", isForApproval?pr.ID:pr.POID,
                "vendor",pr.Vendor.Name,
                "site",pr.BUs.Name + " " +pr.BUs.Address + " "+pr.BUs.City,
                "amount",pr.TotalPrice,
                "raisedby",pr.RaisedBy,
                "raisedon",pr.RaisedOn,
                //"lineheader",LineHeader,
                "lines",Lines,
                "header",headerTxt,
                "remarks",pr.Remarks
            });

            string EmailTxt = r1.Execute(EmailTxtFormat);
            //SendSMS(Web.Services.AppSettings.Current.ClientMobile, EmailTxt);
        }

        public static void SendPRRejectionEmail(string emailid, PR pr, string comments)
        {
            string EmailTxtFormat = "" +
            @"
Dear Sir/Ma'am,

    PR No. <PRRef> has been Reject with following comments:

    <comments>";

            Replacements r1 = new Replacements();
            r1.AddNew(new object[]{
                "PRRef", pr.ReferenceNo,
                "comments", comments
            });

            string EmailTxt = r1.Execute(EmailTxtFormat);
            //SendSMS(Web.Services.AppSettings.Current.ClientMobile, EmailTxt);

            //EmailSendor.SendTextEmail(emailid, from, "PR Rejected !", null, EmailTxt);
            EmailSendor.SendTextEmail(emailid, from, "PR Rejected !", EmailTxt, false, null); ;
        }

        public static void SendSMS(string mobile, string message)
        {
            //SMSModule.MVayooSMS sms = new SMSModule.MVayooSMS("INNOSOLS");
            //sms.sendSMS(mobile, message, 0, 0);
        }

        public static void EmailDraftPR(string mailid, PR pr)
        {
            string EmailTxtFormat = "" +
@"
__________________________________________

<header>
__________________________________________

MI: <prid>
Site: <site>
Vendor: <vendor>
Raised by: <raisedby> on <raisedon>
            
Line Details:
_____________

<lines>

MI Remarks:
<remarks>
__________________________________________";
            string LineFormat =
                "\nLineID: {0}\nItem: {1}\nSize: {2}\nMake: {3}\nQty: {4}\nDelivery Date: {5}\nRemarks: {6}" + "\n\n";
            string LineHeader = String.Format(LineFormat,
                "LineID", "Item", "Size", "Make", "Qty", "Required Date", "Remarks");
            string Lines = "";
            foreach (var line in pr.PRDetails)
            {
                var size = "";
                var brand = "";
                if (line.ItemSize != null)
                    size = line.ItemSize.Value;
                if (line.ItemBrand != null)
                    brand = line.ItemBrand.Value;
                Lines += String.Format(LineFormat,
                line.ID, String.Format("{0} ({1})", line.Item.ItemCategory.Name, line.Item.Name), size, brand, line.Qty + " " + line.Item.MeasurementID, line.DeliveryDate.Value.ToShortDateString(), line.Remarks);
            }

            string headerTxt = "";
            headerTxt = "Dear Sir\n\nNew MI Raised. Kindly Review the MI !";

            string terms = pr.PaymentTerms;

            Replacements r1 = new Replacements();
            r1.AddNew(new object[]{
                "prid", pr.ID,
                "vendor", pr.Vendor.Name,
                "site", pr.BUs.Name + " " +pr.BUs.Address + " "+pr.BUs.City,
                "raisedby", pr.RaisedBy,
                "raisedon", pr.RaisedOn,
                "lineheader", LineHeader,
                "lines", Lines,
                "header", headerTxt,
                "remarks", pr.Remarks,
                "terms", terms,
               // "sendor",Web.Services.AppSettings.Current.Name
            });

            string EmailTxt = r1.Execute(EmailTxtFormat);
            var title = "PR";
            if (pr.POs == null) title = pr.ReferenceNo;
            else title = pr.POs.Display;

            string attachment = "";

            EmailSendor.SendTextEmail(mailid, from, title, attachment, EmailTxt);
        }

        public static void EmailPO(string mailid, PR pr, bool isForPOApproval)
        {
            string EmailTxtFormat = "" +
@"
__________________________________________

<header>
__________________________________________
<approve>
<approvelink>
";
                      
            string LineFormat =
                "\nLineID: {0}\nItem: {1}\nSize: {2}\nMake: {3}\nQty: {4}\nUnit Price: {5}\nAmount: Rs.{6}/-\nDiscount: {7} {8}\nNet Amount: Rs.{9}/-\nDelivery Date: {10}\nRemarks: {11}" + "\n\n";
            string LineHeader = String.Format(LineFormat,
                "LineID", "Item", "Size", "Make", "Qty", "Unit Price", "Amount", "Discount", "DiscountType", "NetAmount", "Delivery Date", "Remarks");
            string Lines = "";
            foreach (var line in pr.PRDetails)
            {
                Lines += String.Format(LineFormat,
                line.ID, String.Format("{0} ({1})", line.Item.ItemCategory.Name, line.Item.Name), line.ItemSize, line.ItemBrand, line.Qty + " " + line.Item.MeasurementID, line.UnitPrice, line.LinePrice, line.Discount, line.DiscountType, line.TotalLinePrice, line.DeliveryDate.Value.ToShortDateString(), line.Remarks);
            }

            string headerTxt = "";
            string ApprovalText = "";
            string Approvallink = "";

            if (!pr.IsVerified && !pr.IsReqApproved && !pr.IsApproved && isForPOApproval)
            {
                headerTxt = "Dear Sir\nNew MI Raised; Kindly Review the MI";
                ApprovalText = "Click the link below to View the MI :";
                //Approvallink = "http://" + ConfigurationManager.AppSettings["mobilepagepath"];// + "/ApprovePR.aspx?ID=" + pr.ID;
                Approvallink = "http://" + "m.parnami.innosols.co.in/PR/Details/" + pr.ID + "?For=MI-Draft";
            }
            if (!pr.IsVerified && !pr.IsReqApproved && !pr.IsApproved && isForPOApproval)
            {
                headerTxt = "Dear Sir\nKindly Review and Verify the MI";
                ApprovalText = "Click the link below to Verify MI :";
                //Approvallink = "http://" + ConfigurationManager.AppSettings["mobilepagepath"];// + "/ApprovePR.aspx?ID=" + pr.ID;
                Approvallink = "http://" + "m.parnami.innosols.co.in/PR/Details/" + pr.ID + "?For=MI-Verify";
            }
            else if (pr.IsVerified && !pr.IsReqApproved && !pr.IsApproved && isForPOApproval)
            {
                headerTxt = "Dear Sir\nKindly Review and Approve the MI";
                ApprovalText = "Click the link below to Approve MI :";
                //Approvallink = "http://" + ConfigurationManager.AppSettings["mobilepagepath"];// + "/ApprovePR.aspx?ID=" + pr.ID;
                Approvallink = "http://" + "m.parnami.innosols.co.in/PR/eDetails/" + pr.ID + "?For=MI-Approve&key=927249E9-4489-49C6-B667-C76BBD6CEEFC&username=parnami";
            }
            else if (pr.IsVerified && pr.IsReqApproved && !pr.IsApproved && isForPOApproval)
            {
                headerTxt = "Dear Sir\nKindly Review and Approve the PO";
                ApprovalText = "Click the link below to Approve PO :";
                //Approvallink = "http://" + ConfigurationManager.AppSettings["mobilepagepath"];// + "/ApprovePR.aspx?ID=" + pr.ID;
                //Approvallink = "http://" + "m.parnami.innosols.co.in/PR/eDetails/" + pr.ID + "?For=PO-Approve&key=927249E9-4489-49C6-B667-C76BBD6CEEFC&username=parnami";
                Approvallink = "<a href=\"http://" + "m.parnami.innosols.co.in/PR/eDetails/" + pr.ID + "?For=PO-Approve&key=927249E9-4489-49C6-B667-C76BBD6CEEFC&username=parnami\"></a>";
            }
            else
            {
                headerTxt = "Dear Sir\nThis is for your kind information";
                ApprovalText = "";
                Approvallink = "";
            }

            string terms = pr.PaymentTerms;

            Replacements r1 = new Replacements();
            r1.AddNew(new object[]{
                "poid", isForPOApproval?pr.ID:pr.POID,
                "vendor", pr.Vendor.Name,
                "site", pr.BUs.Name + " " +pr.BUs.Address + " "+pr.BUs.City,
                "amount", pr.TotalPrice,
                "vat", pr.PVat,
                "cst", pr.PCST,
                "frieght", pr.Frieght,
                "netamount", pr.NetAmount,
                "raisedby", pr.RaisedBy,
                "raisedon", pr.RaisedOn,
                "lineheader", LineHeader,
                "lines", Lines,
                "header", headerTxt,
                "remarks", pr.Remarks,
                "terms", terms,
                "approve", ApprovalText,
                "approvelink", Approvallink,
               // "sendor",Web.Services.AppSettings.Current.Name
            });

            string EmailTxt = r1.Execute(EmailTxtFormat);
            var title = "PR/PO";
            if (pr.POs == null) title = pr.ReferenceNo;
            else title = pr.POs.Display;

            string attachment = "";

            //if (pr.POID.HasValue)
            //    attachment = GetAttachableFilePath(pr.POID.Value);

            EmailSendor.SendTextEmail(mailid, from, title, attachment, EmailTxt);
        }

        //        public static void EmailPO(string mailid, PR pr, bool isForApproval)
        //        {
        //            string EmailTxtFormat = "" +
        //@"
        //<header>
        //__________
        //PO/PR: <poid>
        //Site: <site>
        //Vendor: <vendor>
        //Amount: <amount>
        //Raised by: <raisedby> on <raisedon>
        //
        //Line Details:
        //<lines>
        //
        //PO/PR Remarks:
        //<remarks>
        //
        //Terms and Conditions:
        //<terms>
        //
        //<sendor>
        //";
        //            // Line Format
        //            string LineFormat =
        //                "Line: {0}\nItem: {1}\nQty: {2}\nUnit Price: {3}\nLine Price: {4}\nDelivery Date: {5}\nRemarks: {6}" + "\n\n";
        //            string LineHeader = String.Format(LineFormat,
        //                "Line", "Item", "Qty", "Unit Price", "LinePrice", "Delivery Date", "Remarks");
        //            string Lines = "";
        //            foreach (var line in pr.PRDetails)
        //            {
        //                Lines += String.Format(LineFormat,
        //                line.ID, String.Format("{0} ({1})", line.Item.ItemCategory.Name, line.Item.Name), line.Qty + " " + line.Item.MeasurementID, line.UnitPrice, line.LinePrice, line.DeliveryDate.Value.ToShortDateString(), line.Remarks);
        //            }

        //            string headerTxt = "";

        //            if (isForApproval)
        //            {
        //                headerTxt = "Dear Sir\nKindly Review and approve the PO";
        //            }
        //            else
        //            {
        //                headerTxt = "Dear Sir\nThis is for your kind information";
        //            }

        //            string terms = pr.PaymentTerms;

        //            Replacements r1 = new Replacements();
        //            r1.AddNew(new object[]{
        //                "poid", isForApproval?pr.ID:pr.POID,
        //                "vendor",pr.Vendor.Name,
        //                "site",pr.BUs.Name + " " +pr.BUs.Address + " "+pr.BUs.City,
        //                "amount",pr.TotalPrice,
        //                "raisedby",pr.RaisedBy,
        //                "raisedon",pr.RaisedOn,
        //                "lineheader",LineHeader,
        //                "lines",Lines,
        //                "header",headerTxt,
        //                "remarks",pr.Remarks,
        //                "terms",terms
        //               // "sendor",Web.Services.AppSettings.Current.Name
        //            });

        //            string EmailTxt = r1.Execute(EmailTxtFormat);
        //            var title = "PR/PO";
        //            if (pr.POs == null) title = pr.ReferenceNo;
        //            else title = pr.POs.Display;
        //            EmailSendor.SendTextEmail(mailid, from, title, EmailTxt);
        //        }

        //public static string GetAttachableFilePath(int poid)
        //{
        //    var basePath = ConfigurationManager.AppSettings["serverabspath"];
        //    var name = "file_" + Guid.NewGuid() + ".pdf";
        //    var abspath = basePath + @"\TempFiles\" + name;



        //    IReportLoaderProvider provider = new ReportLoaderProviders();
        //    IReportLoader loader = provider.GetReportLoader("PO");
        //    var dic = new ReportParameterDictionary();
        //    dic.Add("id", poid);
        //    // Call RDLC SAver
        //    System.Collections.Generic.List<Microsoft.Reporting.WebForms.ReportDataSource> List = loader.GetReportData(dic).DataSources;

        //    var reportabspath = basePath + loader.ReportPath.Substring(1).Replace("/", @"\");

        //    RDLCSaveHandler.SaveAsPDF(reportabspath, List, abspath);

        //    return abspath;
        //}
    }

    public class Replacement
    {
        public string oldvalue { get; set; }
        public object newvalue { get; set; }
        public Replacement(string _old, object _new)
        {
            oldvalue = _old;
            newvalue = _new;
        }
    }

    public class Replacements : List<Replacement>
    {
        public void AddNew(string _old, object _new)
        {
            this.Add(new Replacement(_old, _new));
        }

        public void AddNew(object[] list)
        {
            if (list.Count() % 2 != 0)
                return;
            for (int i = 0; i < list.Count(); i = i + 2)
            {
                this.AddNew(list[i].ToString(), list[i + 1]);
            }
        }

        public string Execute(string text)
        {
            foreach (var item in this)
            {
                string val = "";
                if (item.newvalue != null)
                    val = item.newvalue.ToString();
                text = text.Replace("<" + item.oldvalue + ">", val);
            }
            return text;
        }
    }
}
