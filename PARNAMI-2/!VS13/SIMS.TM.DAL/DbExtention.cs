﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SIMS.TM.DAL
{
    public partial class TMDBDataContext
    {
        public int GetMaxTenderID()
        { 
            if(Tenders.Count()==0) return 0;
            return Tenders.Max(x => x.ID);
        }
  
    }

    public static class staticDbExtention
    {
        public static IQueryable<MeasurementBook> FilterBySite(this IQueryable<MeasurementBook> qry, int? siteid)
        {
            if (siteid.HasValue)
                return qry.Where(x => x.SiteID == siteid);
            return qry;
        }
    }
}
