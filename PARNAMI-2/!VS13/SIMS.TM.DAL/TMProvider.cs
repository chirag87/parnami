﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Linq;

namespace SIMS.TM.DAL
{
    public interface IDBProvider<T> where T:DataContext
    {
        T NewDB();
    }

    public class TMDBProvider:IDBProvider<TMDBDataContext>
    {
        public static string DefaultConnectionString { get; set; }

        public TMDBDataContext NewDB()
        {
            if (String.IsNullOrWhiteSpace(DefaultConnectionString))
            {
                return new TMDBDataContext();
            }
            else
            {
                return new TMDBDataContext(DefaultConnectionString);
            }
        }
    }
}
