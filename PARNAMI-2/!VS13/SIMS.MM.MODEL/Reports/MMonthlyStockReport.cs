﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;

namespace SIMS.MM.MODEL
{
    [DataContract]
    public class MMonthlyStockReport : EntityModel
    {
        [DataMember]
        public int? Year { get; set; }

        [DataMember]
        public int? month { get; set; }

        [DataMember]
        public string MonthName { get; set; }
        
        [DataMember]
        public string DisplayMonthName
        {
            get { return GetMonthName(month, Year); }
            set { }
        }

        [DataMember]
        public string DisplayDateFormat 
        {
            get { return DisplayMonthName + ", " + Year; }
            set { }
        }

        [DataMember]
        public int LinesCount { get; set; }

        [DataMember]
        public double? QtyIn { get; set; }

        [DataMember]
        public double? QtyOut { get; set; }

        [DataMember]
        public double? NetQtyIN { get; set; }

        [DataMember]
        public double? TotalConsumedQty { get; set; }

        [DataMember]
        public double? OpeningBalance { get; set; }

        [DataMember]
        public double? ClosingBalance { get; set; }

        public string GetMonthName(int? Month, int? year)
        {
            DateTime date = DateTime.UtcNow.AddHours(5.5); ;
            if (Month.HasValue && Year.HasValue)
                date = new DateTime(year.Value, Month.Value, 1);
            return date.ToString("MMM");
        }
    }
}
