﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;

namespace SIMS.MM.MODEL
{
    [DataContract]
    public class MStockReport : EntityModel
    {
        [DataMember]
        [Display(AutoGenerateField = false)]
        public int ID { get; set; }

        [DataMember]
        [Display(AutoGenerateField = false)]
        public int SiteID { get; set; }

        [DataMember]
        [Display(AutoGenerateField = false)]
        public string RefNo { get; set; }

        [DataMember]
        [Display(Name = "Site")]
        public string DisplaySite { get; set; }

        [DataMember]
        [Display(AutoGenerateField = false)]
        public string DisplayItemName { get; set; }

        [DataMember]
        [Display(AutoGenerateField = false)]
        public string Display 
        {
            get { return ItemCategory + "( " + Item + " )"; }
            set { }
        }

        [DataMember]
        [Display(AutoGenerateField = false)]
        public double? TotalAvailableQty 
        {
            get
            {
                return NetQtyIN - TotalConsumedQty;
            }
            set { }
        }

        [DataMember]
        [Display(AutoGenerateField = false)]
        public double? NetQtyIN 
        {
            get
            {
                return QtyIn - QtyOut;
            }
            set { }
        }

        [DataMember]
        [Display(AutoGenerateField = false)]
        public double? TotalConsumedQty { get; set; }

        [DataMember]
        public int? ItemCatID { get; set; }

        [DataMember]
        public int ItemID { get; set; }

        [DataMember]
        public string Item { get; set; }

        [DataMember]
        public int? SizeID { get; set; }

        [DataMember]
        public string Size { get; set; }

        [DataMember]
        public int? BrandID { get; set; }

        [DataMember]
        public string Brand { get; set; }

        [DataMember]
        public int CategoryID { get; set; }

        [DataMember]
        public string ItemCategory { get; set; }

        [DataMember]
        public string MU { get; set; }

        [DataMember]
        public double? OpeningBalance { get; set; }

        [DataMember]
        public double? QtyIn  { get; set; }

        [DataMember]
        public double? QtyOut { get; set; }

        [DataMember]
        public double? ClosingBalance { get; set; }

        [DataMember]
        [Display(AutoGenerateField = false)]
        public string Remarks { get; set; }

        [DataMember]
        [Display(AutoGenerateField = false)]
        public int LinesCount { get; set; }
    }
}
