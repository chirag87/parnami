﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace SIMS.MM.MODEL
{
    [DataContract]
    public class MMIReportSummaryLine : EntityModel
    {
        [DataMember]
        public string Status { get; set; }

        [DataMember]
        public string PRRefNo { get; set; }

        [DataMember]
        public int PRID { get; set; }

        [DataMember]
        public int PRLineID { get; set; }

        [DataMember]
        public string MORefNo { get; set; }

        [DataMember]
        public int? MOID { get; set; }

        [DataMember]
        public int? MOLineID { get; set; }

        [DataMember]
        public DateTime RaisedOn { get; set; }

        [DataMember]
        public int SiteID { get; set; }

        [DataMember]
        public string Site { get; set; }

        [DataMember]
        public bool ConvertedToPO { get; set; }

        [DataMember]
        public bool IsSplittedToTO { get; set; }

        [DataMember]
        public bool IsDeleted { get; set; }

        [DataMember]
        public int ItemID { get; set; }

        [DataMember]
        public string Item { get; set; }

        [DataMember]
        public string DisplayItem { get; set; }

        [DataMember]
        public int? SizeID { get; set; }

        [DataMember]
        public string Size { get; set; }

        [DataMember]
        public int? BrandID { get; set; }

        [DataMember]
        public string Brand { get; set; }

        [DataMember]
        public string ConsumableType { get; set; }

        [DataMember]
        public double TotalMIQty { get; set; }

        [DataMember]
        public double TobePOQty { get; set; }

        [DataMember]
        public double TOQty { get; set; }

        [DataMember]
        public double TODisplatchQty { get; set; }

        [DataMember]
        public double POQty { get; set; }

        [DataMember]
        public double MRNQty { get; set; }

        [DataMember]
        public double QtyInCloseMIs { get; set; }

        [DataMember]
        public double PendingQtyAfterClose
        {
            get { return TobePOQty - QtyInCloseMIs; }
            set { }
        }

        [DataMember]
        public int RecordsCount { get; set; }
    }
}
