﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;

namespace SIMS.MM.MODEL
{
    [DataContract]
    public class MDailyStockReport : EntityModel
    {
        [DataMember]
        public string Type { get; set; }

        [DataMember]
        public int? TypeID { get; set; }

        [DataMember]
        public int? TypeHeaderID { get; set; }

        [DataMember]
        public DateTime? ForDate { get; set; }

        [DataMember]
        public string DisplayDate
        {
            get { return ForDate.Value.ToString("MMM-dd-yyyy"); }
            set { }
        }

        [DataMember]
        public int? TransactionID { get; set; }

        [DataMember]
        public int? TransactionHeaderID { get; set; }

        [DataMember]
        public int? ItemID { get; set; }

        [DataMember]
        public int? SizeID { get; set; }

        [DataMember]
        public int? BrandID { get; set; }

        [DataMember]
        public string AccFor { get; set; }

        [DataMember]
        public int? AccForID { get; set; }

        [DataMember]
        public string AccForName { get; set; }

        [DataMember]
        public int LinesCount { get; set; }

        [DataMember]
        public double? StockIn { get; set; }

        [DataMember]
        public double? StockOut { get; set; }

        [DataMember]
        public double? OpeningBalance { get; set; }

        [DataMember]
        public double? ClosingBalance { get; set; }
    }
}
