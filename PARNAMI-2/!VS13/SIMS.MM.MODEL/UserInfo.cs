﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace SIMS.MM.MODEL
{
    [DataContract]
    public partial class UserInfo : EntityModel
    {
        [DataMember]
        public string Username { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public string Password { get; set; }
        [DataMember]
        public string Email { get; set; }
        [DataMember]
        public string[] Roles { get; set; }
        [DataMember]
        public bool IsOnline { get; set; }
        [DataMember]
        public bool IsApproved { get; set; }
        [DataMember]
        public string ApprovalStatus
        {
            get { return IsApproved ? "Approved" : "Blocked"; }
            set { }
        }
        [DataMember]
        public string ApprovalAction
        {
            get { return IsApproved ? "Block" : "Approve"; }
            set { }
        }
        [DataMember]
        public string OnlineStatus
        {
            get { return IsOnline ? "Online" : "Offline"; }
            set { }
        }
        [DataMember]
        public string AllRoles
        {
            get
            {
                string str = "";
                bool isFirst = true;
                foreach (var role in Roles)
                {
                    if (isFirst)
                    {
                        str += role;
                        isFirst = false;
                    }
                    else
                        str += ", " + role;
                }
                return str;
            }
            set { }
        }

        [DataMember]
        public List<int> AllowedSiteIDs { get; set; }

        [DataMember]
        public string AllowedSites { get; set; }
    }
}
