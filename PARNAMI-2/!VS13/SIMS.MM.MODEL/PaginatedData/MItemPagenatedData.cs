﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace SIMS.MM.MODEL
{
    [DataContract]
    public class MItemPagenatedData : EntityModel
    {
        public MItemPagenatedData()
        {
            Data = new List<MItem>();
        }

        [DataMember]
        public int CurrentPage { get; set; }

        [DataMember]
        public int TotalPages { get; set; }

        [DataMember]
        public List<MItem> Data { get; set; }

        [DataMember]
        public int PageSize { get; set; }

    }
}
