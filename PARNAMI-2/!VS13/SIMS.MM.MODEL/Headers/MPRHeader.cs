﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;

namespace SIMS.MM.MODEL
{
    [DataContract]
    public class MPRHeader : EntityModel
    {
        [DataMember]
        public int PRNo { get; set; }

        [DataMember]
        public string ReferenceNo { get; set; }

        [DataMember]
        public DateTime Date { get; set; }

        [DataMember]
        [Display(AutoGenerateField = false)]
        public string Vendor { get; set; }

        [DataMember]
        [Display(Name = "Vendor", Order = 3)]
        public string DisplayVendor { get; set; }

        [DataMember]
        [Display(AutoGenerateField = false)]
        public string Site { get; set; }

        [DataMember]
        [Display(Name = "Site", Order = 4)]
        public string DisplaySite { get; set; }

        [DataMember]
        public double Price { get; set; }

        [DataMember]
        public string VerificationStatus { get; set; }

        [DataMember]
        public string RequestApprovalStatus { get; set; }

        [DataMember]
        public string ApprovalStatus { get; set; }

        [DataMember]
        public bool IsVerified { get; set; }

        [DataMember]
        public bool IsReqApproved { get; set; }

        [DataMember]
        public bool IsApproved { get; set; }

        [DataMember]
        public string STATUS
        {
            get
            {
                if (IsVerified && !IsReqApproved && !IsApproved)
                    return "Verified !";
                else if (IsVerified && IsReqApproved && !IsApproved)
                    return "Initially Approved !";
                else if (IsVerified && IsReqApproved && IsApproved)
                    return "Approved !";
                else
                    return "Not Verified !";
            }
            set { }
        }

        [DataMember]
        public string PRState { get; set; }

        [DataMember]
        public bool IsClosed { get; set; }
    }
}
