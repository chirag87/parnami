﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace SIMS.MM.MODEL
{
    [DataContract]
    public class MDocument : EntityModel
    {
        [DataMember]
        public int ID { get; set; }

        [DataMember]
        public string DocumentFor { get; set; }

        [DataMember]
        public int? DocumentForID { get; set; }

        [DataMember]
        public string DocumentType { get; set; }

        [DataMember]
        public string DocPath { get; set; }

        [DataMember]
        public string DocName 
        {
            get
            {
                var data = DocPath.Substring(9);
                return data;
            }
            set { }
        }
    }
}