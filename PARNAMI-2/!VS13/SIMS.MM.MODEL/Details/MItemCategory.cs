﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.ComponentModel;

namespace SIMS.MM.MODEL
{
    [DataContract]
    public class MItemCategory : EntityModel
    {
        public event EventHandler<CategoryEventArgs> CategoryIDChanged;
        public void RaiseCategoryIDChanged(int categoryid)
        {
            if (CategoryIDChanged != null)
                CategoryIDChanged(this, new CategoryEventArgs(categoryid));
        }

        public int _ID = new int();
        [DataMember]
        public int ID
        {
            get { return _ID; }
            set
            {
                _ID = value;
                Notify("ID");
                RaiseCategoryIDChanged(value);
            }
        }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public string Description { get; set; }

        [DataMember]
        public int? ParentID { get; set; }

        [DataMember]
        public bool CanHaveItem { get; set; }

        [DataMember]
        public bool IsItem { get; set; }

        [DataMember]
        public bool HasVariations { get; set; }

        [DataMember]
        public string ItemType { get; set; }

        [DataMember]
        public bool IsStockable { get; set; }

        [DataMember]
        public bool IsSystemDefined { get; set; }

        [DataMember]
        public bool IsConsumable { get; set; }
    }

    public class CategoryEventArgs : EventArgs
    {
        public int CategoryID { get; set; }
        public CategoryEventArgs(int _CategoryID) { CategoryID = _CategoryID; }
    }
}
