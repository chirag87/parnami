﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.ComponentModel;
using System.Collections.ObjectModel;

namespace SIMS.MM.MODEL
{
    [DataContract]
    public class MPriceMaster : EntityModel
    {
        public MPriceMaster()
        {
            EffectiveFrom = DateTime.UtcNow.AddHours(5.5);
            EffectiveTill = DateTime.UtcNow.AddHours(5.5);
            CreatedOn = DateTime.UtcNow.AddHours(5.5);
            UpdatedOn = DateTime.UtcNow.AddHours(5.5);
        }

        [DataMember]
        public int ID { get; set; }

        int _LineID;
        [DataMember] 
        public int LineID
        {
            get { return _LineID; }
            set
            {
                _LineID = value;
                Notify("LineID");
            }
        }

        [DataMember]
        public int VendorID { get; set; }

        [DataMember]
        public string Vendor { get; set; }

        [DataMember]
        public int ItemID { get; set; }

        [DataMember]
        public string Item { get; set; }

        [DataMember]
        public int? SiteID { get; set; }

        [DataMember]
        public string Site { get; set; }

        double _QtyFor;
        [DataMember]
        public double QtyFor
        {
            get { return _QtyFor; }
            set
            {
                _QtyFor = value;
                UpdateUnitPrice();
                Notify("QtyFor");
            }
        }

        double _TotalPrice;
        [DataMember]
        public double TotalPrice
        {
            get { return _TotalPrice; }
            set
            {
                _TotalPrice = value;
                UpdateUnitPrice();
                Notify("TotalPrice");
            }
        }

        [DataMember]
        public string UM { get; set; }

        [DataMember]
        public double UnitPrice { get { return TotalPrice / QtyFor; } set { } }

        [DataMember]
        public DateTime EffectiveFrom { get; set; }

        [DataMember]
        public DateTime? EffectiveTill { get; set; }

        //[DataMember]
        //public DateTime? CreatedOn { get; set; }
        DateTime? _CreatedOn;
        [DataMember]
        public DateTime? CreatedOn
        {
            get
            {
                if (_CreatedOn < DateTime.UtcNow.AddHours(5.5))
                    return _CreatedOn;
                else
                    return DateTime.UtcNow.AddHours(5.5);
            }
            set
            {
                _CreatedOn = value;
                Notify("CreatedOn");
            }
        }

        [DataMember]
        public string CreatedBy { get; set; }

        //[DataMember]
        //public DateTime? UpdatedOn { get; set; }
        DateTime? _UpdatedOn;
        [DataMember]
        public DateTime? UpdatedOn
        {
            get
            {
                if (_UpdatedOn < DateTime.UtcNow.AddHours(5.5))
                    return _UpdatedOn;
                else
                    return DateTime.UtcNow.AddHours(5.5);
            }
            set
            {
                _UpdatedOn = value;
                Notify("UpdatedOn");
            }
        }


        [DataMember]
        public string UpdatedBy { get; set; }

        [DataMember]
        public string CreationDetails
        {
            get 
            {
                if (CreatedOn != null)
                {
                    return " BY: " + CreatedBy + "   ON: " + CreatedOn;
                }
                else { return ""; }
            }
            set { }
        }

        [DataMember]
        public string UpdationDetails
        {
            get 
            {
                if (UpdatedOn != null)
                {
                    return " BY: " + UpdatedBy + "   ON: " + UpdatedOn;
                }
                else { return ""; }
            }
            set { }
        }

        public void UpdateUnitPrice()
        {
            UnitPrice = TotalPrice / QtyFor;
            Notify("UnitPrice");
        }
    }
}
