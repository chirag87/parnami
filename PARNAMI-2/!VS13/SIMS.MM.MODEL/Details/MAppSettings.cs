﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.ComponentModel;

namespace SIMS.MM.MODEL
{ 
    [DataContract]
    public class MAppSettings : EntityModel
    {
        [DataMember]
        public int ID { get; set; }

        string _CompanyName;
        [DataMember]
        public string CompanyName
        {
            get { return _CompanyName; }
            set
            {
                _CompanyName = value;
                Notify("CompanyName");
            }
        }

        bool _ShowLogo = true;
        [DataMember]
        public bool ShowLogo
        {
            get { return _ShowLogo; }
            set
            {
                _ShowLogo = value && !String.IsNullOrWhiteSpace(LogoPath);
                Notify("ShowLogo");
            }
        }

        string _LogoPath = "http://localhost:3385/Images/Logo.png";
        [DataMember]
        public string LogoPath
        {
            get { return _LogoPath; }
            set
            {
                _LogoPath = value;
                Notify("LogoPath");
            }
        }

        string _FontStyle;
        [DataMember]
        public string FontStyle
        {
            get { return _FontStyle; }
            set
            {
                _FontStyle = value;
                Notify("FontStyle");
            }
        }

        string _FontColor;
        [DataMember]
        public string FontColor
        {
            get { return _FontColor; }
            set
            {
                _FontColor = value;
                Notify("FontColor");
            }
        }

        string _CompanyTitle1;
        [DataMember]
        public string CompanyTitle1
        {
            get { return _CompanyTitle1; }
            set
            {
                _CompanyTitle1 = value;
                Notify("CompanyTitle1");
            }
        }

        bool _HasCompanyTitle1 = false;
        [DataMember]
        public bool HasCompanyTitle1
        {
            get { return _HasCompanyTitle1; }
            set
            {
                _HasCompanyTitle1 = value;
                Notify("HasCompanyTitle1");
            }
        }

        string _CompanyTitle2;
        [DataMember]
        public string CompanyTitle2
        {
            get { return _CompanyTitle2; }
            set
            {
                _CompanyTitle2 = value;
                Notify("CompanyTitle2");
            }
        }

        
        bool _HasCompanyTitle2 = false;
        [DataMember]
        public bool HasCompanyTitle2
        {
            get { return _HasCompanyTitle2; }
            set
            {
                _HasCompanyTitle2 = value;
                Notify("HasCompanyTitle2");
            }
        }

        
        string _Address1;
        [DataMember]
        public string Address1
        {
            get { return _Address1; }
            set
            {
                _Address1 = value;
                Notify("Address1");
            }
        }

        string _Address2;
        [DataMember]
        public string Address2
        {
            get { return _Address2; }
            set
            {
                _Address2 = value;
                Notify("Address2");
            }
        }

        string _Address3;
        [DataMember]
        public string Address3
        {
            get { return _Address3; }
            set
            {
                _Address3 = value;
                Notify("Address3");
            }
        }

        bool _HasAddress3 = false;
        [DataMember]
        public bool HasAddress3
        {
            get { return _HasAddress3; }
            set
            {
                _HasAddress3 = value;
                Notify("HasAddress3");
            }
        }

        string _Address4;
        [DataMember]
        public string Address4
        {
            get { return _Address4; }
            set
            {
                _Address4 = value;
                Notify("Address4");
            }
        }

        bool _HasAddress4 = false;
        [DataMember]
        public bool HasAddress4
        {
            get { return _HasAddress4; }
            set
            {
                _HasAddress4 = value;
                Notify("HasAddress4");
            }
        }

        string _City;
        [DataMember]
        public string City
        {
            get { return _City; }
            set
            {
                _City = value;
                Notify("City");
            }
        }

        string _State;
        [DataMember]
        public string State
        {
            get { return _State; }
            set
            {
                _State = value;
                Notify("State");
            }
        }

        string _Country = "India";
        [DataMember]
        public string Country
        {
            get { return _Country; }
            set
            {
                _Country = value;
                Notify("Country");
            }
        }

        string _PinCode;
        [DataMember]
        public string PinCode
        {
            get { return _PinCode; }
            set
            {
                _PinCode = value;
                Notify("PinCode");
            }
        }

        string _Phone;
        [DataMember]
        public string Phone
        {
            get { return _Phone; }
            set
            {
                _Phone = value;
                Notify("Phone");
            }
        }

        public string DisplayPhone 
        {
            get { return "Phone: " + Phone; }
            set { }
        }

        bool _HasPhone = false;
        [DataMember]
        public bool HasPhone
        {
            get { return _HasPhone; }
            set
            {
                _HasPhone = value;
                Notify("HasPhone");
            }
        }

        string _Mobile;
        [DataMember]
        public string Mobile
        {
            get { return _Mobile; }
            set
            {
                _Mobile = value;
                Notify("Mobile");
            }
        }

        public string DisplayMobile 
        {
            get { return "Mobile: " + Mobile; }
            set { }
        }

        bool _HasMobile = false;
        [DataMember]
        public bool HasMobile
        {
            get { return _HasMobile; }
            set
            {
                _HasMobile = value;
                Notify("HasMobile");
            }
        }

        string _FaxNo;
        [DataMember]
        public string FaxNo
        {
            get { return _FaxNo; }
            set
            {
                _FaxNo = value;
                Notify("FaxNo");
            }
        }

        public string DisplayFax 
        {
            get { return "Fax: " + FaxNo; }
            set { }
        }

        bool _HasFax = false;
        [DataMember]
        public bool HasFax
        {
            get { return _HasFax; }
            set
            {
                _HasFax = value;
                Notify("HasFax");
            }
        }

        string _WebSite;
        [DataMember]
        public string WebSite
        {
            get { return _WebSite; }
            set
            {
                _WebSite = value;
                Notify("WebSite");
            }
        }

        public string DisplayWebSite 
        {
            get { return "WebSite: " + WebSite; }
            set { }
        }

        bool _HasWebsite = false;
        [DataMember]
        public bool HasWebsite
        {
            get { return _HasWebsite; }
            set
            {
                _HasWebsite = value;
                Notify("HasWebsite");
            }
        }

        string _Email;
        [DataMember]
        public string Email
        {
            get { return _Email; }
            set
            {
                _Email = value;
                Notify("Email");
            }
        }

        public string DisplayEmail 
        {
            get { return "Email: " + Email; }
            set { }
        }

        bool _HasEmail = false;
        [DataMember]
        public bool HasEmail
        {
            get { return _HasEmail; }
            set
            {
                _HasEmail = value;
                Notify("HasEmail");
            }
        }

        string _TinNo;
        [DataMember]
        public string TinNo
        {
            get { return _TinNo; }
            set
            {
                _TinNo = value;
                Notify("TinNo");
            }
        }

        string _ServerPath = "localhost:3385";
        [DataMember]
        public string ServerPath
        {
            get { return _ServerPath; }
            set
            {
                _ServerPath = value;
                Notify("ServerPath");
            }
        }

        public string DisplayTinNo 
        {
            get { return "TIN - " + TinNo; }
            set { }
        }

        bool _HasTinNo = false;
        [DataMember]
        public bool HasTinNo
        {
            get { return _HasTinNo; }
            set
            {
                _HasTinNo = value;
                Notify("HasTinNo");
            }
        }

        string _ServiceTaxNo;
        public string ServiceTaxNo
        {
            get { return _ServiceTaxNo; }
            set
            {
                _ServiceTaxNo = value;
                Notify("ServiceTaxNo");
            }
        }

        public string DisplayServiceTaxNo
        {
            get { return "Service Tax No. - " + ServiceTaxNo; }
            set { }
        }

        bool _HasServiceTaxNo = false;
        public bool HasServiceTaxNo
        {
            get { return _HasServiceTaxNo; }
            set
            {
                _HasServiceTaxNo = value;
                Notify("HasServiceTaxNo");
            }
        }

        string _mailserver;
        [DataMember]
        public string mailserver
        {
            get { return _mailserver; }
            set
            {
                _mailserver = value;
                Notify("mailserver");
            }
        }

        string _mailserverusername;
        [DataMember]
        public string mailserverusername
        {
            get { return _mailserverusername; }
            set
            {
                _mailserverusername = value;
                Notify("mailserverusername");
            }
        }

        string _mailserverpassword;
        [DataMember]
        public string mailserverpassword
        {
            get { return _mailserverpassword; }
            set
            {
                _mailserverpassword = value;
                Notify("mailserverpassword");
            }
        }

        string _mailfrom;
        [DataMember]
        public string mailfrom
        {
            get { return _mailfrom; }
            set
            {
                _mailfrom = value;
                Notify("mailfrom");
            }
        }

        string _mailreplyto;
        [DataMember]
        public string mailreplyto
        {
            get { return _mailreplyto; }
            set
            {
                _mailreplyto = value;
                Notify("mailreplyto");
            }
        }
    }
}

