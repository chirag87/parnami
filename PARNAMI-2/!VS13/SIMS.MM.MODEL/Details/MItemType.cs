﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace SIMS.MM.MODEL
{
    [DataContract]
    public class MItemType : EntityModel
    {
        [DataMember]
        public string ItemType { get; set; }
    }
}