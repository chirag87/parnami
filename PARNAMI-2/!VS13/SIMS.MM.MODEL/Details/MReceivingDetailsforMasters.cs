﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace SIMS.MM.MODEL
{
    [DataContract]
    public class MReceivingDetailsforMasters : EntityModel
    {
        [DataMember]
        public int ID { get; set; }

        [DataMember]
        public string Type { get; set; }

        [DataMember]
        public string ReferenceNo { get; set; }

        [DataMember]
        public DateTime ForDate { get; set; }

        [DataMember]
        public int? VendorID { get; set; }

        [DataMember]
        public string Vendor { get; set; }

        [DataMember]
        public int SiteID { get; set; }

        [DataMember]
        public string Site { get; set; }

        [DataMember]
        public string Remarks { get; set; }

        [DataMember]
        public string Requester { get; set; }

        [DataMember]
        public DateTime CreatedOn { get; set; }

        [DataMember]
        public string InvoiceNo { get; set; }

        [DataMember]
        public string ChallanNo { get; set; }

        [DataMember]
        public string VehicleDetails { get; set; }

        [DataMember]
        public int LineID { get; set; }

        [DataMember]
        public int itemID { get; set; }

        [DataMember]
        public string Item { get; set; }

        [DataMember]
        public string LineRemarks { get; set; }

        [DataMember]
        public string LineChallanNo { get; set; }

        [DataMember]
        public double Quantity { get; set; }

        [DataMember]
        public double? UnitPrice { get; set; }

        [DataMember]
        public double? LinePrice { get; set; }

        [DataMember]
        public string TruckNo { get; set; }
    }
}
