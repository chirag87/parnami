﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.ComponentModel;

namespace SIMS.MM.MODEL
{
    [DataContract]
    public class MMTRLine : EntityModel
    {
        [DataMember]
        public int MTRID { get; set; }

        int _LineID;
        [DataMember]
        public int LineID
        {
            get { return _LineID; }
            set
            {
                _LineID = value;
                Notify("LineID");
            }
        }

        [DataMember]
        public double Quantity { get; set; }

        [DataMember]
        public double ApprovedQty { get; set; }

        [DataMember]
        public double? ReleasedQty { get; set; }

        [DataMember]
        public double? TransferredQty { get; set; }

        [DataMember]
        public double? InTransitQty { get; set; }

        [DataMember]
        public double? PendingQty { get; set; }

        [DataMember]
        public int ItemID { get; set; }

        [DataMember]
        public string Item { get; set; }

        [DataMember]
        public int? SizeID { get; set; }

        [DataMember]
        public string Size { get; set; }

        [DataMember]
        public int? BrandID { get; set; }

        [DataMember]
        public string Brand { get; set; }

        [DataMember]
        public string LineRemarks { get; set; }
    }
}
