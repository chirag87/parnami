﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace SIMS.MM.MODEL
{
    [DataContract]
    public class MItem : EntityModel
    {
        public MItem()
        {
            ConsumableType = "Consumable";
            DealTypeID = "Fixed";
        }

        [DataMember]
        public int ID { get; set; }

        [DataMember]
        public int ItemCategoryID { get; set; }

        [DataMember]
        public int? ParentID { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public string DisplayName { get; set; }

        [DataMember]
        public string ItemCode { get; set; }

        Uri _PictureName;
        [DataMember]
        public Uri PictureName                  //to Get Photo from database
        {
            get { return _PictureName; }
            set
            {
                _PictureName = value;
                Notify("PictureName");
            }
        }

        [DataMember]
        public string Photo { get; set; }           //to Save Photo in database

        [DataMember]
        public byte[] PictureStream { get; set; }

        [DataMember]
        public string Description { get; set; }

        [DataMember]
        public string ItemCategory { get; set; }

        [DataMember]
        public string CategoryParent { get; set; }

        [DataMember]
        public int? VendorID { get; set; }

        [DataMember]
        public int? DefaultVendorID { get; set; }

        [DataMember]
        public string DefaultVendor { get; set; }

        [DataMember]
        public bool IsDeleted { get; set; }

        string _MeasurementUnitID;
        [DataMember]
        public string MeasurementUnitID
        {
            get { return _MeasurementUnitID; }
            set
            {
                _MeasurementUnitID = value;
                Notify("MeasurementUnitID");
            }
        }

        string _DealTypeID;
        [DataMember]
        public string DealTypeID
        {
            get { return _DealTypeID; }
            set
            {
                _DealTypeID = value;
                Notify("DealTypeID");
            }
        }

        [DataMember]
        public string Remarks { get; set; }

        [DataMember]
        public double? InventoryCount { get; set; }

        [DataMember]
        public bool IsStockable { get; set; }

        [DataMember]
        public string ConsumableType { get; set; }

        [DataMember]
        public string Type { get; set; }
    }


    [DataContract]
    public class MItemSize : EntityModel
    {
        [DataMember]
        public int ID { get; set; }

        string _Size;
        [DataMember]
        public string Size
        {
            get { return _Size; }
            set
            {
                _Size = value;
                RaiseSizeChanged();
                Notify("Size");
            }
        }

        public event EventHandler SizeChanged;
        public void RaiseSizeChanged()
        {
            if (SizeChanged != null)
                SizeChanged(this, new EventArgs());
        }
    }

    [DataContract]
    public class MItemBrand : EntityModel
    {
        [DataMember]
        public int ID { get; set; }

        string _BrandName;
        [DataMember]
        public string BrandName
        {
            get { return _BrandName; }
            set
            {
                _BrandName = value;
                Notify("BrandName");
                RaiseBrandChanged();
            }
        }

        public event EventHandler BrandChanged;
        public void RaiseBrandChanged()
        {
            if (BrandChanged != null)
                BrandChanged(this, new EventArgs());
        }
    }
}
