﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace SIMS.MM.MODEL
{
    [DataContract]
    public class MAccounts : EntityModel
    {
        [DataMember]
        public int ID { get; set; }

        public string Title
        {
            get { return BankName + "  ( " + BranchAddress + " , " + BranchCity + " )"; }
            set { }
        }

        [DataMember]
        public string AccountFor { get; set; }

        [DataMember]
        public int AccountForID { get; set; }

        [DataMember]
        public string BankName { get; set; }

        [DataMember]
        public string BranchAddress { get; set; }

        [DataMember]
        public string BranchCity { get; set; }

        [DataMember]
        public string BranchState { get; set; }

        [DataMember]
        public string BranchCountry { get; set; }

        [DataMember]
        public string BranchPinCode { get; set; }

        [DataMember]
        public string BankPhone { get; set; }

        [DataMember]
        public string BankWebSite { get; set; }

        [DataMember]
        public string BankEmail { get; set; }

        [DataMember]
        public string ChequePayableTo { get; set; }

        [DataMember]
        public string AccountNo { get; set; }

        [DataMember]
        public string IFSCCode { get; set; }
    }
}
