﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SIMS.MM.DAL.PrintReports
{
    using SIMS.ReportLoader;

    public class VendorBillPrintLoader : ReportLoader
    {
        public override RDLCReportData GetReportData(ReportParameterDictionary Dic)
        {
            RDLCReportData data = new RDLCReportData();

            parnamidb1Entities db = new parnamidb1Entities();
            MMReportsDBDataContext rdb = new MMReportsDBDataContext();
            rdb.CommandTimeout = 5000;
            int billid = (int)Dic["id"];

            var header = rdb.GetBillingHeaders(null, null, null, null, null).Where(x => x.ID == billid).ToList();
            var detail = rdb.GetBillingDetailsByUniqueID(billid);
            var vendor = rdb.GetVendorDetailsbyVendorID(header.First().VendorID);
            var site = rdb.GetSiteDetailsbySiteID(header.First().SiteID);
            var app = db.AppSettings;

            data.Add("Header", header);
            data.Add("Details", detail);
            data.Add("VendorDetails", vendor);
            data.Add("SiteDetails", site);
            data.Add("AppSettings", app);

            return data;
        }
    }
}
