﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SIMS.MM.DAL
{
    public class BillDataProvider
    {
        parnamidb1Entities db = new parnamidb1Entities();
        MMReportsDBDataContext rdb = new MMReportsDBDataContext();

        public IQueryable<Billing> GetBillings()
        {
            return db.Billings;
        }

        public IQueryable<BillingDetail> GetBillingDetails() { return db.BillingDetails; }

        public IQueryable<AppSetting> GetAppSettings() { return null; }
    }
}
