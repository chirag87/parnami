﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SIMS.MM.DAL
{
    using SIMS.ReportLoader;

    public class StockBreakUpReportLoader : ReportLoader
    {
        public override RDLCReportData GetReportData(ReportParameterDictionary Dic)
        {
            RDLCReportData data = new RDLCReportData();

            parnamidb1Entities db = new parnamidb1Entities();
            MMReportsDBDataContext rdb = new MMReportsDBDataContext();
            rdb.CommandTimeout = 5000;
            DateTime? sdate = null;
            DateTime? edate = null;
            int? itemid = null;
            int? itemCatID = null;
            int? sizeid = null;
            int? brandid = null;
            int? siteid = null;
            string contype = null;

            if (Dic.Count != 0)
            {
                /*Checking if Dictionary Contains the Parameter name as well as there Values*/

                if (Dic.Any(x=>x.Key == "sdate") && !String.IsNullOrWhiteSpace(Dic["sdate"].ToString()))
                    sdate = (DateTime)Dic["sdate"];

                if (Dic.Any(x => x.Key == "edate") && !String.IsNullOrWhiteSpace(Dic["edate"].ToString()))
                    edate = (DateTime)Dic["edate"];

                if (Dic.Any(x => x.Key == "itemCatID") && !String.IsNullOrWhiteSpace(Dic["itemCatID"].ToString()))
                    itemid = (int)Dic["itemCatID"];

                if (Dic.Any(x => x.Key == "itemid") && !String.IsNullOrWhiteSpace(Dic["itemid"].ToString()))
                    itemid = (int)Dic["itemid"];

                if (Dic.Any(x => x.Key == "sizeid") && !String.IsNullOrWhiteSpace(Dic["sizeid"].ToString()))
                    sizeid = (int?)Dic["sizeid"];

                if (Dic.Any(x => x.Key == "brandid") && !String.IsNullOrWhiteSpace(Dic["brandid"].ToString()))
                    brandid = (int?)Dic["brandid"];

                if (Dic.Any(x => x.Key == "siteid") && !String.IsNullOrWhiteSpace(Dic["siteid"].ToString()))
                    siteid = (int?)Dic["siteid"];

                if (Dic.Any(x => x.Key == "contype") && !String.IsNullOrWhiteSpace(Dic["contype"].ToString()))
                    contype = (string)Dic["contype"];
            }

            var stockbreakup = rdb.GetStockFurcation(sdate, edate, siteid, itemid, itemCatID, sizeid, brandid, contype).OrderBy(x => x.Item);

            data.Add("StockBreakUp", stockbreakup);

            return data;
        }
    }
}
