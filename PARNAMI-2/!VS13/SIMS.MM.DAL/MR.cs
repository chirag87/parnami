﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SIMS.MM.DAL
{
    public partial class MR
    {
        public string ApprovalStatus
        { 
            get
            {
                if (IsApproved)
                    return "Approved.";
                else
                    return "Pending Approval...";
            }
            
        }
    }
}
