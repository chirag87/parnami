﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using SIMS.MM.MODEL;
using SIMS.MM.BLL;
namespace Contruction.Web.Services
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Tender" in code, svc and config file together.
    public class Tender : ITender
    {
        ITenderDataProvider provider = new TenderDataProvider();
        ITenderManager manager = new TenderManager();
        public PaginatedData<MTender> GetAllTenders(string username, int? pageindex, int? pagesize, string Key, int? sideid, bool showall = true)
        {
            var showAll = LoginService.CanShowAllSites(username);
            var data = (PaginatedData<MTender>)provider.GetTenderHeader(username, pageindex, pagesize, Key, sideid, showall);
            return data;
        }

        public MTender CreateNewTender(MTender _NewTender)
        {
            return manager.CreateNewTender(_NewTender);
        }

        public MTender UpdateTender(MTender _UpdatedTender)
        {
            return manager.UpdateTender(_UpdatedTender);
        }

        public MTender GetTenderByID(int TenderID)
        {
            return provider.GetTenderByID(TenderID);
        }
    }
}
