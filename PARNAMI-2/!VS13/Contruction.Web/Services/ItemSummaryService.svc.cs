﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using SIMS.MM.BLL;
using SIMS.MM.MODEL;

namespace Contruction.Web.Services
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "ItemSummaryService" in code, svc and config file together.
    public class ItemSummaryService : IItemSummaryService
    {
        IItemSummaryManager manager = new ItemSummaryManager();

        public IEnumerable<MStockInINDENT> GetItemQtyInIndents(int? itemid, int? sizeid, int? brandid)
        {
            var data = manager.GetItemQtyInIndents(itemid, sizeid, brandid);
            return data;
        }

        public IEnumerable<MStockInPO> GetItemQtyInPOs(int? itemid, int? sizeid, int? brandid)
        {
            var data = manager.GetItemQtyInPOs(itemid, sizeid, brandid);
            return data;
        }

        public IEnumerable<MStockOnSites> GetItemQtyOnSites(int? itemid, int? sizeid, int? brandid)
        {
            var data = manager.GetItemQtyOnSites(itemid, sizeid, brandid);
            return data;
        }

        public IEnumerable<MRateHistory> GetRateHistoryOfItem(int? itemid, int? sizeid, int? brandid)
        {
            var data = manager.GetRateHistoryOfItem(itemid, sizeid, brandid);
            return data;
        }
    }
}
