﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using Contruction.Web.Data;
using SIMS.MM.BLL;
using SIMS.MM.MODEL;
using System.Collections.ObjectModel;
using SIMS.Core;

namespace Contruction.Web.Services
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IPurchasingService" in both code and config file together.
    [ServiceContract]
    //[ServiceBehavior(
    public interface IPurchasingService
    {
        [OperationContract]
        PaginatedData<MPRHeader> GetAllPRs(string username, int? pageindex, int? pagesize, string key, int? vendorid, int? siteid, string status);

        [OperationContract]
        PaginatedData<MPOHeader> GetAllPOs(string username, int? pageindex, int? pagesize, string key, int? vendorid, int? siteid);

        [OperationContract]
        MPR GetPRbyID(int PRID);

        [OperationContract]
        MPO GetPObyID(int POID);

        [OperationContract]
        MTODispatchInfo GetTotalTODispacthQty(int? ID, int LineID, int SiteID, int ItemID, int? SizeID, int? BrandID, string ConType);

        [OperationContract]
        MPR CreateNewPR(MPR NewPR);

        [OperationContract]
        MPR SplitPR(MPR NewPR, int OldPRID, ObservableCollection<int> OldPRLines);

        [OperationContract]
        MPR SplitPRwithPartialQty(int OldPRID, ObservableCollection<MSplittedLines> NewPRLines);

        [OperationContract]
        void DeletePR(int PRID);

        [OperationContract]
        MPR UpdatePR(MPR UpdatedPR);

        [OperationContract]
        void EmailPR(int PRID);

        [OperationContract]
        void EmailDraftPR(int PRID);

        [OperationContract]
        string ConfirmVendorEmail(int POID);

        [OperationContract]
        void EmailtoVendor(int PRID);

        [OperationContract]
        void AcknowledgePO(int POID, string Remarks);

        [OperationContract]
        MPR ApprovePR(int PRID, string ApprovedBy, DateTime? ApprovedOn, string ApprovalRemarks);

        [OperationContract]
        MPR VerifyPR(int PRID, string VerifiedBy, DateTime? VerifiedOn, string VerificationRemarks);

        [OperationContract]
        MPR InitialApprovePR(int PRID, string ReqApprovedBy, DateTime? ReqApprovedOn, string ReqApprovalRemarks);

        [OperationContract]
        MPR ChangesStateofPR(int PRID, string State);

        [OperationContract]
        MPR RejectPR(int PRID, string comments, string State);

        [OperationContract]
        int ConvertToPO(MPR PR);

        [OperationContract]
        MPR SaveandApprove(MPR mpr, string User);

        [OperationContract]
        MPR SaveandInitialApprove(MPR mpr, string User);
        
        [OperationContract]
        MPR SaveandVerify(MPR mpr, string User);

        [OperationContract]
        MPR SaveandChangesStateofPR(MPR mpr, string State);

        [OperationContract]
        MPR CloseMI(string UserName, int PRID);

        [OperationContract]
        MPO CopyPOfromMI(string UserName, MPR OldPR);

        [OperationContract]
        MPR SubmitToHOPurchase(string UserName, MPR OldPR);
    }
}
