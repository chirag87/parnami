﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using SIMS.MM.BLL;
using SIMS.MM.MODEL;

namespace Contruction.Web.Services
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "MTRService" in code, svc and config file together.
    public class MTRService : IMTRService
    {
        //IMTRDataProvider dp = new TestMTRDataProvider();
        IMTRManager pm = new MTRManager();
        IMTRDataProvider dp = new MTRDataProvider();

        public PaginatedData<MMTRHeader> GetAllMTRs(int? pageindex, int? pagesize, string key, int? siteid)
        {
            var data = (PaginatedData<MMTRHeader>)dp.GetMTRHeaders(pageindex, pagesize, key, null, siteid, null);
            return data;
        }

        public void CreateNewMTR(MMTR NewMTR)
        {
            pm.CreateNewMTR(NewMTR);
        }

        public MMTR GetMTRbyID(int MTRID)
        {
            return dp.GetMTRbyId(MTRID);
        }

        public void ApproveMTR(int MTRID)
        {
            pm.ApproveMTR(MTRID);
        }

        public void ReleaseMTR(int MTRID)
        {
            pm.ReleaseMTR(MTRID);
        }

        public void ReceiveMTR(int MTRID)
        {
            pm.ReceiveMTR(MTRID);
        }
    }

    //public class TestMTRDataProvider : IMTRDataProvider
    //{
    //    public IQueryable<MMTRHeader> GetMTRHeaders(int? pageindex, int? pagesize, string Key, int? vendorid, int? sideid, int? sideid2)
    //    {
    //        List<MMTRHeader> MTRList = new List<MMTRHeader>();
    //        MTRList.Add(new MMTRHeader()
    //        {
    //            ID = 111,
    //            Date = (new DateTime(2012, 01, 11)),
    //            RequestedBy = "A",
    //            RequestedBySite = "Site-A",
    //            TransferringSite = "Site-B",
    //            ApprovalStatus = "Approved",
    //            Remarks = "Testing"
    //        });
    //        MTRList.Add(new MMTRHeader()
    //        {
    //            ID = 112,
    //            Date = (new DateTime(2012, 01, 11)),
    //            RequestedBy = "B",
    //            RequestedBySite = "Site-B",
    //            TransferringSite = "Site-C",
    //            ApprovalStatus = "Not Approved",
    //            Remarks = "Testing"
    //        });
    //        MTRList.Add(new MMTRHeader()
    //        {
    //            ID = 113,
    //            Date = (new DateTime(2012, 01, 11)),
    //            RequestedBy = "C",
    //            RequestedBySite = "Site-C",
    //            TransferringSite = "Site-A",
    //            ApprovalStatus = "Approved",
    //            Remarks = "Testing"
    //        });

    //        return MTRList.AsQueryable();
    //    }

    //    public MMTR GetMTRbyId(int mtrid)
    //    {
    //        throw new NotImplementedException();
    //    }

    //    IPaginated<MMTRHeader> IMTRDataProvider.GetMTRHeaders(int? pageindex, int? pagesize, string Key, int? vendorid, int? sideid, int? sideid2)
    //    {
    //        throw new NotImplementedException();
    //    }
    //}
}
