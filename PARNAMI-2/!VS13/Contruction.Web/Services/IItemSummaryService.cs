﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using SIMS.MM.MODEL;

namespace Contruction.Web.Services
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IItemSummaryService" in both code and config file together.
    [ServiceContract]
    public interface IItemSummaryService
    {
        [OperationContract]
        IEnumerable<MStockInINDENT> GetItemQtyInIndents(int? itemid, int? sizeid, int? brandid);

        [OperationContract]
        IEnumerable<MStockInPO> GetItemQtyInPOs(int? itemid, int? sizeid, int? brandid);

        [OperationContract]
        IEnumerable<MStockOnSites> GetItemQtyOnSites(int? itemid, int? sizeid, int? brandid);

        [OperationContract]
        IEnumerable<MRateHistory> GetRateHistoryOfItem(int? itemid, int? sizeid, int? brandid);
    }
}
