﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using SIMS.MM.BLL;
using SIMS.MM.MODEL;
using SIMS.Core;

namespace Contruction.Web.Services
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IReceivingService" in both code and config file together.
    [ServiceContract]
    public interface IReceivingService
    {
        [OperationContract]
        PaginatedData<MReceivingHeader> GetAllReceivings(string username, int? pageindex, int? pagesize, string key, int? vendorid, int? siteid);
        
        [OperationContract]
        PaginatedData<MReceivingHeader> GetAllCashPurchases(string username, int? pageindex, int? pagesize, string key, int? vendorid, int? siteid);
        
        [OperationContract]
        PaginatedData<MReceivingHeader> GetAllDirectPurchases(string username, int? pageindex, int? pagesize, string key, int? vendorid, int? siteid);

        [OperationContract]
        PaginatedData<MReceivingHeader> GetAllDirectGrns(string username, int? pageindex, int? pagesize, string key, int? vendorid, int? siteid, string type);

        [OperationContract]
        PaginatedData<MReceivingHeader> GetAllConsumptions(string username, int? pageindex, int? pagesize, string key, int? siteid);

        [OperationContract]
        MReceiving CreateNewReceiving(MReceiving NewReceiving, string username);

        [OperationContract]
        MReceiving CreateNewCashPurchase(MReceiving NewCashPurchase, string username);

        [OperationContract]
        MReceiving CreateNewDirectPurchase(MReceiving NewDirectPurchase, string username);

        [OperationContract]
        MReceiving CreateNewConsumption(MReceiving NewConsumption, string username);

        [OperationContract]
        MReceiving CreateNewRTV(MReceiving _newRTV, string username);

        [OperationContract]
        MReceiving CreateNewSA(MReceiving _newSA, string username);

        [OperationContract]
        MReceiving GetGRNByID(int receivingID);

        [OperationContract]
        MReceiving GetCashPurchaseByID(int receivingID);

        [OperationContract]
        MReceiving GetDirectPurchaseByID(int receivingID);

        [OperationContract]
        MReceiving GetConsumptionByID(int receivingID);

        [OperationContract]
        IEnumerable<int> GetAllowedItemIDs(int vendorid, int siteid);

        [OperationContract]
        IEnumerable<PendingPOInfo> GetPendingPOInfos(int vendorid, int siteid, int itemid, int? sizeid, int? brandid);

        [OperationContract]
        MReceiving ConvertfromPO(int poid);
    }
}
