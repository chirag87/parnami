﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using SIMS.MM.BLL.Reports;
using SIMS.MM.DAL;
using SIMS.MM.MODEL;

namespace Contruction.Web.Services
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "MasterReportingService" in code, svc and config file together.
    [ServiceBehavior(MaxItemsInObjectGraph = 2147483647)]
    public class MasterReportingService : IMasterReportingService
    {
        ReportProvider pr = new ReportProvider();

        public List<MPOReport> GetPOReport(DateTime? startdate, DateTime? enddate, int? siteid, int? vendorid, int? itemid, int? itemcatid, int? sizeid, int? brandid)
        {
            var data = pr.GetPODetailsByLine(startdate, enddate, siteid, vendorid, itemid, itemcatid, sizeid, brandid).ToList();
            return data;
        }

        public List<MGRNReport> GetGRNReport(DateTime? sdate, DateTime? edate, int? siteid, int? vendorid, int? itemid, int? sizeid, int? brandid)
        {
            var data = pr.GetGRNDetails(sdate, edate, siteid, vendorid, itemid, sizeid, brandid).ToList();
            return data;
        }

        public List<MCPReport> GetCPReport(DateTime? sdate, DateTime? edate, int? siteid, int? vendorid, int? itemid, int? sizeid, int? brandid)
        {
            var data = pr.GetCPdetails(sdate, edate, siteid, vendorid, itemid, sizeid, brandid).ToList();
            return data;
        }

        public List<MDPReport> GetDPReport(DateTime? sdate, DateTime? edate, int? siteid, int? vendorid, int? itemid, int? sizeid, int? brandid)
        {
            var data = pr.GetDPDetails(sdate, edate, siteid, vendorid, itemid, sizeid, brandid).ToList();
            return data;
        }

        public List<MDirectGRNReport> GetDirectGRNReport(DateTime? sdate, DateTime? edate, int? siteid, int? vendorid, int? itemid, int? sizeid, int? brandid)
        {
            var data = pr.GetDirectGRNReport(sdate, edate, siteid, vendorid, itemid, sizeid, brandid).ToList();
            return data;
        }

        public List<MConsumptionReport> GetConsumptionReport(DateTime? sdate, DateTime? edate, int? siteid, int? vendorid, int? itemid, int? sizeid, int? brandid)
        {
            var data = pr.GetConsumptionDetails(sdate, edate, siteid, vendorid, itemid, sizeid, brandid).ToList();
            return data;
        }

        public List<MMOReport> GetMMOReport(DateTime? sdate, DateTime? edate, int? siteid, int? vendorid, int? itemid, int? sizeid, int? brandid)
        {
            var data = pr.GetMOdetails(sdate, edate, siteid, itemid, sizeid, brandid).ToList();
            return data;
        }

        public List<MBilledReceivings> GetBilledReceivings(DateTime? sdate, DateTime? edate, int? siteid, int? vendorid, int? itemid, int? sizeid, int? brandid)
        {
            var data = pr.GetBilledReceivings(sdate, edate, siteid, vendorid, itemid, sizeid, brandid).ToList();
            return data;
        }

        public List<MDirectGRNReport> GetUnBilledReceivings(DateTime? sdate, DateTime? edate, int? siteid, int? vendorid, int? itemid, int? sizeid, int? brandid)
        {
            var data = pr.GetUnBilledReceivings(sdate, edate, siteid, vendorid, itemid, sizeid, brandid).ToList();
            return data;
        }

        public List<MStockReport> GetStockReport(DateTime? sdate, DateTime? edate, int? siteid, int? itemCatID, int? itemid, int? sizeid, int? brandid)
        {
            var data = pr.GetDetailedStockReport(sdate, edate, siteid, itemCatID, itemid, sizeid, brandid).ToList();
            return data;
        }

        public List<MMonthlyStockReport> GetMonthlyStockReport(DateTime? sdate, DateTime? edate, int? siteid, int? itemCatID, int? itemid, int? sizeid, int? brandid)
        {
            var data = pr.GetMonthlyStockReport(sdate, edate, siteid, itemCatID, itemid, sizeid, brandid).ToList();
            return data;
        }

        public List<MDailyStockReport> GetDailyStockReport(DateTime? sdate, DateTime? edate, int? siteid, int? itemCatID, int? itemid, int? sizeid, int? brandid)
        {
            var data = pr.GetDailyStockReport(sdate, edate, siteid, itemCatID, itemid, sizeid, brandid).ToList();
            return data;
        }

        public List<MMIReportSummary> GetMIReportSummary(DateTime? sdate, DateTime? edate, int? itemCatID, int? itemid, int? sizeid, int? brandid, int? siteid, string Contype, string status, bool showzero)
        {
            var data = pr.GetMIReportSummary(sdate, edate, itemCatID, itemid, sizeid, brandid, siteid, Contype, status, showzero).ToList();
            return data;
        }

        public List<MMIReportSummaryLine> GetMIReportSummaryDetails(DateTime? sdate, DateTime? edate, int? itemCatID, int? itemid, int? sizeid, int? brandid, int? siteid, string Contype, string status)
        {
            var data = pr.GetMIReportSummaryDetails(sdate, edate, itemCatID, itemid, sizeid, brandid, siteid, Contype, status).ToList();
            return data;
        }

        public List<MMIReportSiteWiseSummary> GetMIReportSiteWiseSummary(DateTime? sdate, DateTime? edate, int? itemCatID, int? itemid, int? sizeid, int? brandid, int? siteid, string Contype, string status, bool showzero)
        {
            var data = pr.GetMIReportSiteWiseSummary(sdate, edate, itemCatID, itemid, sizeid, brandid, siteid, Contype, status, showzero).ToList();
            return data;
        }

        public IEnumerable<MMIReportSummary> GetMIDashBoard(DateTime? sdate, DateTime? edate, int? itemid, int? sizeid, int? brandid, int? siteid, string Contype)
        {
            var data = pr.GetMIDashBoard(sdate, edate, itemid, sizeid, brandid, siteid, Contype).ToList();
            return data;
        }

        public List<MStockBreakUp> GetStockBreakUpReport(DateTime? sdate, DateTime? edate, int? itemid, int? itemCatID, int? sizeid, int? brandid, int? siteid, string Contype)
        {
            var data = pr.GetStockBreakUpReport(sdate, edate, itemid, itemCatID, sizeid, brandid, siteid, Contype).ToList();
            return data;
        }

        public List<MDailyStockBreakUp> GetDailyStockBreakUpReport(DateTime? sdate, DateTime? edate, int? itemid, int? itemCatID, int? sizeid, int? brandid, int? siteid, string Contype)
        {
            var data = pr.GetDailyStockBreakUpReport(sdate, edate, itemid, itemCatID, sizeid, brandid, siteid, Contype).ToList();
            return data;
        }
    }
}
