﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using SIMS.MM.MODEL;
using SIMS.Core;

namespace Contruction.Web
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IBillingService" in both code and config file together.
    [ServiceContract]
    public interface IBillingService
    {
        [OperationContract]
        PaginatedData<MReceiving> GetAllReceivings(string username, int? pageindex, int? pagesize, string key, int? vendorid, int? siteid);

        [OperationContract]
        MReceiving GetReceivingforBillByID(int ID, string Type);

        [OperationContract]
        MBilling CreateNewBill(MBilling _NewBill);

        [OperationContract]
        MBilling GetBillByID(int Billid);

        [OperationContract]
        MBilling UpdateBill(MBilling _Bill);

        [OperationContract]
        PaginatedData<MBilling> GetAllBills(string username, int? pageindex, int? pagesize, string key, int? vendorid, int? siteid, string filter);

        [OperationContract]
        MBilling ApproveBill(MBilling _Bill);

        [OperationContract]
        MBilling VerifyBill(MBilling Billing, string VerifiedBy);

        [OperationContract]
        PaginatedData<MReceivingLine> GetReceivingLines(string username, int? pageindex, int? pagesize, string key, int? itemid, int? vendorid, int? siteid, DateTime? sdate, DateTime? edate, string challan);

        [OperationContract]
        PaginatedData<MGRNLine> GetGRNLines(string username, int? pageindex, int? pagesize, string key, int? itemid, int? vendorid, int? siteid, DateTime? sdate, DateTime? edate, string challan);
    }
}
