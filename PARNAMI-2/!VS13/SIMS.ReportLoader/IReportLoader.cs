﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SIMS.ReportLoader
{
    public class ReportParameterDictionary : Dictionary<string, object> { }

    public interface IReportLoader
    {
        string ReportPath { get; set; }
        IDictionaryConverter DictionaryConverter { get; set; }
        RDLCReportData GetReportData(object parameters);
        RDLCReportData GetReportData(ReportParameterDictionary Dic);
    }

    public interface IDictionaryConverter
    {
        ReportParameterDictionary GetDic(Object obj);
    }
}
