﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Reporting.WebForms;

namespace SIMS.ReportLoader
{
    public class DefaultDictionaryConverter : IDictionaryConverter
    {

        public virtual ReportParameterDictionary GetDic(object obj)
        {
            if (obj == null) return new ReportParameterDictionary();
            ReportParameterDictionary dic = new ReportParameterDictionary();

            foreach (var prop in obj.GetType().GetProperties())
            {
                dic.Add(prop.Name, prop.GetValue(obj, null));
            }
            return dic;
        }
    }

    public abstract class ReportLoader : IReportLoader
    {

        public virtual RDLCReportData GetReportData(object parameters)
        {
            return GetReportData(DictionaryConverter.GetDic(parameters));
        }

        public abstract RDLCReportData GetReportData(ReportParameterDictionary Dic);

        public IDictionaryConverter DictionaryConverter
        {
            get;
            set;
        }

        public virtual string ReportPath
        {
            get;
            set;
        }
    }

    public interface IReportLoaderProvider
    {
        IReportLoader GetReportLoader(string type);
    }
}
