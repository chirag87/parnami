﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace Contruction
{
    public partial class WaterMarkTextbox : UserControl
    {
        public WaterMarkTextbox()
        {
            InitializeComponent();
        }

        private void textBox1_Loaded(object sender, RoutedEventArgs e)
        {
            if (!string.IsNullOrEmpty(this.watermark))
            {
                textBox1.GotFocus += new RoutedEventHandler(textBox1_GotFocus);
                textBox1.LostFocus += new RoutedEventHandler(textBox1_LostFocus);
                SetWaterMark();
            }
        }

        private void textBox1_GotFocus(object sender, RoutedEventArgs e)
        {
            DisableWaterMark();
        }

        private void textBox1_LostFocus(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrEmpty(textBox1.Text))
            {
                SetWaterMark();
                textBox1.GotFocus += new RoutedEventHandler(textBox1_GotFocus);
            }
        }

        private string watermark;
        public string WaterMark
        {
            set
            {
                watermark = value;
            }

            get
            {
                return watermark;
            }
        }

        public string Text
        {
            get
            {
                if (textBox1.Text == this.watermark)
                {
                    return "";
                }
                else
                {
                    return textBox1.Text;
                }
            }
            set
            {
                DisableWaterMark();
                textBox1.Text = value;
            }
        }

        private void SetWaterMark()
        {
            textBox1.Foreground = new SolidColorBrush(Colors.LightGray);
            textBox1.Text = this.watermark;
        }

        private void DisableWaterMark()
        {
            textBox1.Text = "";
            textBox1.GotFocus -= textBox1_GotFocus;
            textBox1.Foreground = new SolidColorBrush(Colors.Black);
        }
    }
}
