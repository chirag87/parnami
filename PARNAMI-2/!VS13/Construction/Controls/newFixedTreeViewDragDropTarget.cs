﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Threading;
using Microsoft.Windows;

namespace Contruction
{
    public class newFixedTreeViewDragDropTarget : TreeViewDragDropTarget
    {
        private Timer dragDropDelayTimer;
        private bool isMouseDown = false;

        public newFixedTreeViewDragDropTarget()
        {
            AddHandler(MouseLeftButtonUpEvent, new MouseButtonEventHandler(HandleMouseLeftButtonUp), true);
            AddHandler(MouseLeftButtonDownEvent, new MouseButtonEventHandler(HandleMouseLeftButtonDown), true);
        }

        private void HandleMouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            lock (this)
            {
                isMouseDown = false;
                // Cancel the pending drag operation
                if (dragDropDelayTimer != null)
                    dragDropDelayTimer.Dispose();
                dragDropDelayTimer = null;
            }
        }

        private void HandleMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            lock (this)
            {
                isMouseDown = true;
                // Cancel the pending drag operation
                if (dragDropDelayTimer != null)
                    dragDropDelayTimer.Dispose();
                dragDropDelayTimer = null;
            }
        }


        protected override void OnItemDragStarting(ItemDragEventArgs eventArgs)
        {
            if (!isMouseDown)
            {
                eventArgs.Handled = true;
                return;
            }

            lock (this)
            {
                if (dragDropDelayTimer == null)
                {
                    // Temporization - the drag operation will start in 300ms
                    dragDropDelayTimer = new Timer
                    (
                        callback =>
                        {
                            lock (this)
                            {
                                if (!DragDrop.IsDragInProgress)
                                {
                                    Dispatcher.BeginInvoke(() =>
                                    {
                                        lock (this)
                                        {
                                            base.OnItemDragStarting(eventArgs);
                                        }
                                    });
                                }
                            }
                        },
                        null,
                        3,    // 300ms delay - adjust that to work for your own environment
                        Timeout.Infinite
                    );
                }
            }
        }
    }

    public class FixedListBoxDragDropTarget : ListBoxDragDropTarget
    {
        private Timer dragDropDelayTimer;
        private bool isMouseDown = false;

        public FixedListBoxDragDropTarget()
        {
            AddHandler(MouseLeftButtonUpEvent, new MouseButtonEventHandler(HandleMouseLeftButtonUp), true);
            AddHandler(MouseLeftButtonDownEvent, new MouseButtonEventHandler(HandleMouseLeftButtonDown), true);
        }

        private void HandleMouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            lock (this)
            {
                isMouseDown = false;
                // Cancel the pending drag operation
                if (dragDropDelayTimer != null)
                    dragDropDelayTimer.Dispose();
                dragDropDelayTimer = null;
            }
        }

        private void HandleMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            lock (this)
            {
                isMouseDown = true;
                // Cancel the pending drag operation
                if (dragDropDelayTimer != null)
                    dragDropDelayTimer.Dispose();
                dragDropDelayTimer = null;
            }
        }

        protected override void OnItemDragStarting(ItemDragEventArgs eventArgs)
        {
            if (isMouseDown)
            {
                eventArgs.Handled = true;
                return;
            }

            lock (this)
            {
                if (dragDropDelayTimer == null)
                {
                    // Temporization - the drag operation will start in 300ms
                    dragDropDelayTimer = new Timer
                    (
                        callback =>
                        {
                            lock (this)
                            {
                                if (!DragDrop.IsDragInProgress)
                                {
                                    Dispatcher.BeginInvoke(() =>
                                    {
                                        lock (this)
                                        {
                                            base.OnItemDragStarting(eventArgs);
                                        }
                                    });
                                }
                            }
                        },
                        null,
                        30,    // 300ms delay - adjust that to work for your own environment
                        Timeout.Infinite
                    );
                }
            }
        }
    }
}
