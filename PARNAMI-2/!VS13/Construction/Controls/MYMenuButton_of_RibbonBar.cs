﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using SIMS.RibbonBar;

namespace Contruction
{
    public class MyMenuButton : MenuButton
    {
        public MyMenuButton()
        {
            this.Click += new RoutedEventHandler(MyMenuButton_Click);
        }

        void MyMenuButton_Click(object sender, RoutedEventArgs e)
        {
            if (String.IsNullOrWhiteSpace(NavigationPath)) return;
            Base.Redirect(this.NavigationPath);
        }

        public string NavigationPath
        {
            get { return (string)GetValue(NavigationPathProperty); }
            set { SetValue(NavigationPathProperty, value); }
        }

        // Using a DependencyProperty as the backing store for NavigationPath.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty NavigationPathProperty =
            DependencyProperty.Register("NavigationPath", typeof(string), typeof(MyMenuButton), new PropertyMetadata(string.Empty));
    }
}
