﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Contruction.AlertService;
using System.Collections.ObjectModel;
using SIMS.MM.MODEL;
using System.ComponentModel;
using System.Threading;

namespace Contruction
{
    public partial class Base : ViewModel
    {
        AlertServiceClient AlertService = new AlertServiceClient();

        public void OnInitNotificationModel()
        {
            if (!DesignerProperties.GetIsInDesignMode(Application.Current.RootVisual))
            {
                OnInit();
            }
        }

        #region Event

        public event EventHandler AlertLoaded;
        public void RaiseAlertLoaded()
        {
            if (AlertLoaded != null)
                AlertLoaded(this, new EventArgs());
        }

        #endregion

        #region CompletedEvent...

        void service_GetAlertsCompleted(object sender, GetAlertsCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                Base.ShowError(e.Error);
            }
            else
            {
                Alerts = null;
                Alerts = e.Result;
                if (Alerts != null)
                {
                    IsAlert = true;
                    IsOffline = false;
                    RaiseAlertLoaded();
                }
            }
        }

        #endregion

        #region Properties...

        ObservableCollection<MAlerts> _Alerts = new ObservableCollection<MAlerts>();
        public ObservableCollection<MAlerts> Alerts
        {
            get { return _Alerts; }
            set
            {
                _Alerts = value;
                Notify("Alerts");
            }
        }

        public bool _IsAlert = false;
        public bool IsAlert
        {
            get { return _IsAlert; }
            set
            {
                _IsAlert = value;
                Notify("IsAlert");
            }
        }

        public bool _IsOffline = true;
        public bool IsOffline
        {
            get { return _IsOffline; }
            set
            {
                _IsOffline = value;
                Notify("IsOffline");
            }
        }

        #endregion

        #region Methods...

        public void OnInit()
        {
            AlertService.GetAlertsCompleted += new EventHandler<GetAlertsCompletedEventArgs>(service_GetAlertsCompleted);
            On_Load();
        }

        public void Call()
        {
            AlertService.GetAlertsAsync(Base.Current.UserName);
        }
                
        #endregion

        #region Timer

        public TimerCallback tmr;
        public Timer timer;

        public void On_Load()
        {
            tmr = new TimerCallback(MyTimerCallBack);
            timer = new Timer(tmr, null, 0, 1000 * 60);
        }

        public void MyTimerCallBack(object state)
        {
            System.Windows.Deployment.Current.Dispatcher.BeginInvoke(Call);
            //Call();
        }

        #endregion
    }
}
