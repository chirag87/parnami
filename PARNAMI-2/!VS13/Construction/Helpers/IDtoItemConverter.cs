﻿using System;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Windows.Data;
//using SIMS.MM.MODEL;

namespace Contruction
{
    public class IDtoItemConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            string defval = "NA";
            if (parameter != null)
                defval = parameter.ToString();
            int itemID = 0;
            if (value == null) return defval;
            if (value is int)
            {
                itemID = (int)value;
            } else if (value is string)
            {
                if(!Int32.TryParse(value.ToString(), out itemID))
                {
                    return defval;
                }
            }
            else { return defval;}

            try
            {
                MItem item = Base.Current.Items.Single(x => x.ID == itemID);
                return item.Name;
            }
            catch { }
            return defval ;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
