﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Contruction.MTRService;
using System.Collections.ObjectModel;
using SIMS.MM.MODEL;
using System.ComponentModel;

namespace Contruction
{
    public class ViewMTRDetailsModel : ViewModel
    {
        MTRServiceClient service = new MTRServiceClient();

        public ViewMTRDetailsModel()
        {
            if (!DesignerProperties.GetIsInDesignMode(Application.Current.RootVisual))
            {
                OnInit();
            }
        }

        public void OnInit()
        {
            AppEvents.Current.MTRRequested += new EventHandler<MTRRequestedArgs>(Current_MTRRequested);
            service.GetMTRbyIDCompleted += new EventHandler<GetMTRbyIDCompletedEventArgs>(service_GetMTRbyIDCompleted);
            service.ApproveMTRCompleted += new EventHandler<AsyncCompletedEventArgs>(service_ApproveMTRCompleted);
            service.ReceiveMTRCompleted += new EventHandler<AsyncCompletedEventArgs>(service_ReceiveMTRCompleted);
            service.ReleaseMTRCompleted += new EventHandler<AsyncCompletedEventArgs>(service_ReleaseMTRCompleted);          
        }

        void service_ApproveMTRCompleted(object sender, AsyncCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                Base.ShowError(e.Error);
            }
            else
            {
                MessageBox.Show("Material Transfer Request Approved Successfully !");
            }
        }

        void service_ReleaseMTRCompleted(object sender, AsyncCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                Base.ShowError(e.Error);
            }
            else
            {
                MessageBox.Show("Material Transfer Request Released Successfully !");
            }
        }

        void service_ReceiveMTRCompleted(object sender, AsyncCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                Base.ShowError(e.Error);
            }
            else
            {
                MessageBox.Show("Material Transfer Request Received Successfully !");
            }
        }

        void Current_MTRRequested(object sender, MTRRequestedArgs e)
        {
            int mtrid = e.MTRID;
            GetMTR(mtrid);
        }

        void service_GetMTRbyIDCompleted(object sender, GetMTRbyIDCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                Base.ShowError(e.Error);
            }
            else
            {
                ConvertedMTR = e.Result;
            }
        }

        MMTR _ConvertedMTR;
        public MMTR ConvertedMTR
        {
            get { return _ConvertedMTR; }
            set
            {
                _ConvertedMTR = value;
                Notify("ConvertedMTR");
            }
        }

        public void GetMTR(int mtrid)
        {
            service.GetMTRbyIDAsync(mtrid);
        }

        public void ApproveMTR()
        {
            service.ApproveMTRAsync(ConvertedMTR.MTRNo);
        }

        public void ReleaseMTR()
        {
            service.ApproveMTRAsync(ConvertedMTR.MTRNo);
        }

        public void ReceiveMTR()
        {
            service.ApproveMTRAsync(ConvertedMTR.MTRNo);
        }
    }
}
