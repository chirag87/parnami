﻿using System;
using System.Net;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Contruction.MasterReportingService;
//using SIMS.MM.MODEL;

namespace Contruction
{
    public class StockBreakUpReportVM : ViewModel
    {
        MasterReportingServiceClient service = new MasterReportingServiceClient();

        public StockBreakUpReportVM()
        {
            if (!DesignerProperties.GetIsInDesignMode(Application.Current.RootVisual))
            {
                OnInit();
                AddRTypes();
            }
        }

        #region Methods...

        public void OnInit()
        {
            service.GetStockBreakUpReportCompleted += new EventHandler<GetStockBreakUpReportCompletedEventArgs>(service_GetStockBreakUpReportCompleted);
            SetConsumableTypes();
        }

        public void AddRTypes()
        {
            RTypes.Add("pdf");
            RTypes.Add("Excel");
        }

        void SetConsumableTypes()
        {
            ConsumableTypes = (new string[] { "All", "Consumable", "Returnable" });
        }

        public void Load()
        {
            if (SiteID != null)
            {
                if (ConsumableType == "All") ConsumableType = null;
                service.GetStockBreakUpReportAsync(sdate, edate, ItemID, ItemCatID, SizeID, BrandID, SiteID, ConsumableType);
                IsBusy = true;
            }
            else
            {
                MessageBox.Show("Please Select any Site !");
            }
        }

        public void Clear()
        {
            sdate = null;
            edate = null;
            ItemID = null;
            SizeID = null;
            BrandID = null;
            ConsumableType = null;
            Load();
        }

        #endregion

        #region Completed Events...

        void service_GetStockBreakUpReportCompleted(object sender, GetStockBreakUpReportCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                Base.ShowError(e.Error);
                IsBusy = false;
            }
            else
            {
                Data = e.Result;
                TotalRecords = Data.Count;

                /*Calculate Sum of Different Quantities*/
                SumNetIN = String.Format("{0:0.00}", Data.Sum(x => x.NetIn));
                SumTotalIN = String.Format("{0:0.00}", Data.Sum(x => x.TotalIn));
                SumTotalOUT = String.Format("{0:0.00}", Data.Sum(x => x.TotalOut));
                TotalCPQty = String.Format("{0:0.00}", Data.Sum(x => x.CPQty));
                TotalDispatchQty = String.Format("{0:0.00}", Data.Sum(x => x.ToBeReceivedQty));
                TotalMRNQty = String.Format("{0:0.00}", Data.Sum(x => x.MRNQty));
                TotalReceivedQty = String.Format("{0:0.00}", Data.Sum(x => x.ReceivedQty));
                TotalReleasedQty = String.Format("{0:0.00}", Data.Sum(x => x.ReleasedQty));
                SumTotalDispatch = String.Format("{0:0.00}", (Data.Sum(x => x.ToBeReceivedQty) + Data.Sum(x => x.MRNQty) + Data.Sum(x => x.CPQty)));
                IsBusy = false;
            }
        }

        #endregion

        #region Properties...

        ObservableCollection<MStockBreakUp> _Data = new ObservableCollection<MStockBreakUp>();
        public ObservableCollection<MStockBreakUp> Data
        {
            get { return _Data; }
            set
            {
                _Data = value;
                Notify("Data");
            }
        }

        DateTime? _sdate;
        public DateTime? sdate
        {
            get { return _sdate; }
            set
            {
                _sdate = value;
                Notify("sdate");
            }
        }

        DateTime? _edate;
        public DateTime? edate
        {
            get { return _edate; }
            set
            {
                _edate = value;
                Notify("edate");
            }
        }

        int? _SiteID;
        public int? SiteID
        {
            get { return _SiteID; }
            set
            {
                _SiteID = value;
                Notify("SiteID");
            }
        }

        int? _ItemID;
        public int? ItemID
        {
            get { return _ItemID; }
            set
            {
                _ItemID = value;
                Notify("ItemID");
            }
        }

        int? _ItemCatID;
        public int? ItemCatID
        {
            get { return _ItemCatID; }
            set
            {
                _ItemCatID = value;
                Notify("ItemCatID");
            }
        }

        int? _SizeID;
        public int? SizeID
        {
            get { return _SizeID; }
            set
            {
                _SizeID = value;
                Notify("SizeID");
            }
        }

        int? _BrandID;
        public int? BrandID
        {
            get { return _BrandID; }
            set
            {
                _BrandID = value;
                Notify("BrandID");
            }
        }

        string _ConsumableType;
        public string ConsumableType
        {
            get { return _ConsumableType; }
            set
            {
                _ConsumableType = value;
                Notify("ConsumableType");
            }
        }

        IEnumerable<string> _ConsumableTypes;
        public IEnumerable<string> ConsumableTypes
        {
            get { return _ConsumableTypes; }
            set
            {
                _ConsumableTypes = value;
                Notify("ConsumableTypes");
            }
        }

        string _RType;
        public string RType
        {
            get { return _RType; }
            set
            {
                _RType = value;
                Notify("RType");
            }
        }

        List<string> _RTypes = new List<string>();
        public List<string> RTypes
        {
            get { return _RTypes; }
            set
            {
                _RTypes = value;
                Notify("RTypes");
            }
        }

        int _TotalRecords = 0;
        public int TotalRecords
        {
            get { return _TotalRecords; }
            set
            {
                _TotalRecords = value;
                Notify("TotalRecords");
            }
        }

        string _TotalDispatchQty;
        public string TotalDispatchQty
        {
            get { return _TotalDispatchQty; }
            set
            {
                _TotalDispatchQty = value;
                Notify("TotalDispatchQty");
            }
        }

        string _TotalReceivedQty;
        public string TotalReceivedQty
        {
            get { return _TotalReceivedQty; }
            set
            {
                _TotalReceivedQty = value;
                Notify("TotalReceivedQty");
            }
        }

        string _TotalReleasedQty;
        public string TotalReleasedQty
        {
            get { return _TotalReleasedQty; }
            set
            {
                _TotalReleasedQty = value;
                Notify("TotalReleasedQty");
            }
        }

        string _TotalMRNQty;
        public string TotalMRNQty
        {
            get { return _TotalMRNQty; }
            set
            {
                _TotalMRNQty = value;
                Notify("TotalMRNQty");
            }
        }

        string _TotalCPQty;
        public string TotalCPQty
        {
            get { return _TotalCPQty; }
            set
            {
                _TotalCPQty = value;
                Notify("TotalCPQty");
            }
        }

        string _SumTotalDispatch;               // SUMTotalDispatch = Dispatch + MRN + CP
        public string SumTotalDispatch
        {
            get { return _SumTotalDispatch; }
            set
            {
                _SumTotalDispatch = value;
                Notify("SumTotalDispatch");
            }
        }

        string _SumTotalIN;
        public string SumTotalIN
        {
            get { return _SumTotalIN; }
            set
            {
                _SumTotalIN = value;
                Notify("SumTotalIN");
            }
        }

        string _SumTotalOUT;
        public string SumTotalOUT
        {
            get { return _SumTotalOUT; }
            set
            {
                _SumTotalOUT = value;
                Notify("SumTotalOUT");
            }
        }

        string _SumNetIN;
        public string SumNetIN
        {
            get { return _SumNetIN; }
            set
            {
                _SumNetIN = value;
                Notify("SumNetIN");
            }
        }

        //double _TotalDispatchQty = 0;
        //public double TotalDispatchQty
        //{
        //    get { return _TotalDispatchQty; }
        //    set
        //    {
        //        _TotalDispatchQty = value;
        //        Notify("TotalDispatchQty");
        //    }
        //}

        //double _TotalReceivedQty = 0;
        //public double TotalReceivedQty
        //{
        //    get { return _TotalReceivedQty; }
        //    set
        //    {
        //        _TotalReceivedQty = value;
        //        Notify("TotalReceivedQty");
        //    }
        //}

        //double _TotalReleasedQty = 0;
        //public double TotalReleasedQty
        //{
        //    get { return _TotalReleasedQty; }
        //    set
        //    {
        //        _TotalReleasedQty = value;
        //        Notify("TotalReleasedQty");
        //    }
        //}

        //double _TotalMRNQty = 0;
        //public double TotalMRNQty
        //{
        //    get { return _TotalMRNQty; }
        //    set
        //    {
        //        _TotalMRNQty = value;
        //        Notify("TotalMRNQty");
        //    }
        //}

        //double _TotalCPQty = 0;
        //public double TotalCPQty
        //{
        //    get { return _TotalCPQty; }
        //    set
        //    {
        //        _TotalCPQty = value;
        //        Notify("TotalCPQty");
        //    }
        //}

        //double _SumTotalIN = 0;
        //public double SumTotalIN
        //{
        //    get { return _SumTotalIN; }
        //    set
        //    {
        //        _SumTotalIN = value;
        //        Notify("SumTotalIN");
        //    }
        //}

        //double _SumTotalOUT = 0;
        //public double SumTotalOUT
        //{
        //    get { return _SumTotalOUT; }
        //    set
        //    {
        //        _SumTotalOUT = value;
        //        Notify("SumTotalOUT");
        //    }
        //}

        //double _SumNetIN = 0;
        //public double SumNetIN
        //{
        //    get { return _SumNetIN; }
        //    set
        //    {
        //        _SumNetIN = value;
        //        Notify("SumNetIN");
        //    }
        //}

        #endregion
    }
}