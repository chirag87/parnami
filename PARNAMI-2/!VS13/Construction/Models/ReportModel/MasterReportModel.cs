﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Collections;
using Contruction.MasterReportingService;
using System.ComponentModel;

namespace Contruction
{
	public class MasterReportModel:ViewModel
	{
        MasterReportingServiceClient proxy = new MasterReportingServiceClient();

		public MasterReportModel()
		{
            OnInit();
		}

        private void OnInit()
        {
            proxy.GetCPReportCompleted += new EventHandler<GetCPReportCompletedEventArgs>(proxy_GetCPReportCompleted);
            proxy.GetGRNReportCompleted += new EventHandler<GetGRNReportCompletedEventArgs>(proxy_GetGRNReportCompleted);
            proxy.GetConsumptionReportCompleted += new EventHandler<GetConsumptionReportCompletedEventArgs>(proxy_GetConsumptionReportCompleted);
            proxy.GetMMOReportCompleted += new EventHandler<GetMMOReportCompletedEventArgs>(proxy_GetMMOReportCompleted);
            proxy.GetPOReportCompleted += new EventHandler<GetPOReportCompletedEventArgs>(proxy_GetPOReportCompleted);
            RTypes = (new string[] { "Excel", "PDF" });
            //proxy.GetDPReportCompleted += new EventHandler<GetDPReportCompletedEventArgs>(proxy_GetDPReportCompleted);
            //proxy.GetDirectGRNReportCompleted += new EventHandler<GetDirectGRNReportCompletedEventArgs>(proxy_GetDirectGRNReportCompleted);
            //proxy.GetBilledReceivingsCompleted += new EventHandler<GetBilledReceivingsCompletedEventArgs>(proxy_GetBilledReceivingsCompleted);
            //proxy.GetUnBilledReceivingsCompleted += new EventHandler<GetUnBilledReceivingsCompletedEventArgs>(proxy_GetUnBilledReceivingsCompleted);
        }

        //void proxy_GetUnBilledReceivingsCompleted(object sender, GetUnBilledReceivingsCompletedEventArgs e)
        //{
        //    if (e.Error != null)
        //    {
        //        Base.ShowError(e.Error);
        //        IsBusy = false;
        //    }
        //    else
        //    {
        //        Data = null;
        //        Data = e.Result.ToList();
        //        IsBusy = false;
        //    }
        //    IsLoading = false;
        //}

        //void proxy_GetBilledReceivingsCompleted(object sender, GetBilledReceivingsCompletedEventArgs e)
        //{
        //    if (e.Error != null)
        //    {
        //        Base.ShowError(e.Error);
        //        IsBusy = false;
        //    }
        //    else
        //    {
        //        Data = null;
        //        Data = e.Result.ToList();
        //        IsBusy = false;
        //    }
        //    IsLoading = false;
        //}

        //void proxy_GetDirectGRNReportCompleted(object sender, GetDirectGRNReportCompletedEventArgs e)
        //{
        //    if (e.Error != null)
        //    {
        //        Base.ShowError(e.Error);
        //        IsBusy = false;
        //    }
        //    else
        //    {
        //        Data = null;
        //        Data = e.Result.ToList();
        //        IsBusy = false;
        //    }
        //    IsLoading = false;
        //}

        //void proxy_GetDPReportCompleted(object sender, GetDPReportCompletedEventArgs e)
        //{
        //    if (e.Error != null)
        //    {
        //        Base.ShowError(e.Error);
        //        IsBusy = false;
        //    }
        //    else
        //    {
        //        Data = null;
        //        Data = e.Result.ToList();
        //        IsBusy = false;
        //    }
        //    IsLoading = false;
        //}

        void proxy_GetPOReportCompleted(object sender, GetPOReportCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                Base.ShowError(e.Error);
                IsBusy = false;
            }
            else
            {
                Data = null;
                Data = e.Result.ToList();
                IsBusy = false;
            }
            IsLoading = false;
        }

        void proxy_GetMMOReportCompleted(object sender, GetMMOReportCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                Base.ShowError(e.Error);
                IsBusy = false;
            }
            else
            {
                Data = null;
                Data = e.Result.ToList();
                IsBusy = false;
            }
            IsLoading = false;
        }

        void proxy_GetConsumptionReportCompleted(object sender, GetConsumptionReportCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                Base.ShowError(e.Error);
                IsBusy = false;
            }
            else
            {
                Data = null;
                Data = e.Result.ToList();
                IsBusy = false;
            }
            IsLoading = false;
        }

        void proxy_GetGRNReportCompleted(object sender, GetGRNReportCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                Base.ShowError(e.Error);
                IsBusy = false;
            }
            else
            {
                Data = null;
                Data = e.Result.ToList();
                IsBusy = false;
            }
            IsLoading = false;
        }

        void proxy_GetCPReportCompleted(object sender, GetCPReportCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                Base.ShowError(e.Error);
                IsBusy = false;
            }
            else
            {
                Data = null;
                Data = e.Result.ToList();
                IsBusy = false;
            }
            IsLoading = false;
        }

        public string[] AllReportTypes
        {
            get
            {
                return new string[]
                { 
                    "Cash Purchases",
                    "Consumptions",
                    "Transfer Orders",
                    "Purchase Orders",
                    "MRNs",
                }; 
            }
        }
        
        public string[] RangeTypes
        {
            get
            {
                return new string[]
                {
                    "Last 7 Days",
                    "Last Month",
                    "Last Quater",
                    "Last Year",
                    //"Monthly",
                    //"Quaterly",
                    //"Yearly"
                };
            }
        }

        DateTime? _StartDate;
        public  DateTime? StartDate
        {
            get { return _StartDate; }
            set
            {
                _StartDate = value;
                Notify("StartDate");
            }
        }

        DateTime? _EndDate;
        public DateTime? EndDate
        {
            get { return _EndDate; }
            set
            {
                _EndDate = value;
                Notify("EndDate");
            }
        }

        string _RType;
        public string RType
        {
            get { return _RType; }
            set
            {
                _RType = value;
                Notify("RType");
            }
        }

        IEnumerable<string> _RTypes = new List<string>();
        public IEnumerable<string> RTypes
        {
            get { return _RTypes; }
            set
            {
                _RTypes = value;
                Notify("RTypes");
            }
        }

        public void ComputeDate()
        {
            switch (SelectedRangeType)
            {
                case "Last 7 Days":
                    EndDate = DateTime.Today;
                    StartDate = EndDate.Value.AddDays(-7);
                    break;

                case "Last Month":
                    EndDate = DateTime.Today;
                    StartDate = EndDate.Value.AddMonths(-1);
                    break;

                case "Last Quater":
                    EndDate = DateTime.Today;
                    StartDate = EndDate.Value.AddMonths(-3);
                    break;

                case "Last Year":
                    EndDate = DateTime.Today;
                    StartDate = EndDate.Value.AddYears(-1);
                    break;

                default:
                    EndDate = DateTime.Today;
                    StartDate = null;
                    break;
            }
        }

        bool _IsLoading = false;
        public bool IsLoading
        {
            get { return _IsLoading; }
            set
            {
                _IsLoading = value;
                if (value) { ButtonText = "Loading ..."; }
                else { ButtonText = "Load";}
                if (value) { Status = "Loading from Service, Please Wait!"; }
                else { Status = ""; }
                Notify("IsLoading");
            }
        }

        bool _CanExport;
        public bool CanExport
        {
            get { return _CanExport; }
            set
            {
                _CanExport = value;
                Notify("CanExport");
            }
        }

        string _Status;
        public string Status
        {
            get { return _Status; }
            set
            {
                _Status = value;
                Notify("Status");
            }
        } 

        string _ButtonText = "Load";
        public string ButtonText
        {
            get { return _ButtonText; }
            set
            {
                _ButtonText = value;
                Notify("ButtonText");
            }
        }

        string _SelectedReportType;
        public string SelectedReportType
        {
            get { return _SelectedReportType; }
            set
            {
                _SelectedReportType = value;
                Notify("SelectedReportType");
            }
        }

        string _SelectedRangeType;
        public string SelectedRangeType
        {
            get { return _SelectedRangeType; }
            set
            {
                _SelectedRangeType = value;
                ComputeDate();
                Notify("SelectedRangeType");
            }
        }

        int? _VendorID;
        public int? VendorID
        {
            get { return _VendorID; }
            set
            {
                _VendorID = value;
                Notify("VendorID");
            }
        }

        int? _SiteID;
        public int? SiteID
        {
            get { return _SiteID; }
            set
            {
                _SiteID = value;
                Notify("SiteID");
            }
        }

        int? _ItemID;
        public int? ItemID
        {
            get { return _ItemID; }
            set
            {
                _ItemID = value;
                Notify("ItemID");
            }
        }

        int? _SizeID;
        public int? SizeID
        {
            get { return _SizeID; }
            set
            {
                _SizeID = value;
                Notify("SizeID");
            }
        }

        int? _BrandID;
        public int? BrandID
        {
            get { return _BrandID; }
            set
            {
                _BrandID = value;
                Notify("BrandID");
            }
        }

        public void Clear()
        {
            ItemID = null;
            VendorID = null;
            SiteID = null;
            SizeID = null;
            BrandID = null;
        }

        IEnumerable _Data = new List<object>();
        public IEnumerable Data
        {
            get { return _Data; }
            set
            {
                _Data = value;
                OnDataSet();
                Notify("Data");
            }
        }

        private void OnDataSet()
        {
            if (Data == null)
            {
                CanExport = false;
                return;
            }
            if (Data.Cast<object>().Count() == 0)
            {
                CanExport = false;
                return;
            }
            if (Data.Cast<object>().Count() > 0)
            {
                CanExport = true;
                return;
            }
        }

        public void Load()
        {
            Status = "Loading Data from Service ... Please Wait!";
            //IsLoading = true;
            //proxy.GetPOReportAsync();

            switch (SelectedReportType)
            {
                case "Purchase Orders":
                    IsLoading = true;
                    proxy.GetPOReportAsync(StartDate, EndDate, SiteID, VendorID, ItemID, null, SizeID, BrandID);
                    IsBusy = true;
                    break;

                case "MRNs":
                    proxy.GetGRNReportAsync(StartDate, EndDate, SiteID, VendorID, ItemID, SizeID, BrandID);
                    IsBusy = true;
                    IsLoading = true;                    
                    break;

                case "Cash Purchases":
                    proxy.GetCPReportAsync(StartDate, EndDate, SiteID, VendorID, ItemID, SizeID, BrandID);
                    IsBusy = true;
                    IsLoading = true;                                    
                    break;

                case "Consumptions":
                    proxy.GetConsumptionReportAsync(StartDate, EndDate, SiteID, VendorID, ItemID, SizeID, BrandID);
                    IsBusy = true;
                    IsLoading = true;
                    break;

                case "Transfer Orders":
                    proxy.GetMMOReportAsync(StartDate, EndDate, SiteID, VendorID, ItemID, SizeID, BrandID);
                    IsBusy = true;
                    IsLoading = true;
                    break;

                default:
                    MessageBox.Show("Please Select Report Type !");
                    IsBusy = false;
                    IsLoading = false;
                    break;
            }
        }
	}

    public enum ReprtingTypes
    {
        PurchaseOrders,
        Receiving,
        CashPurchase,
        Consumption,
        TransferOrder,
    }
}