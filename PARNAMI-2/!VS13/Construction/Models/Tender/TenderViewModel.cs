﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Contruction.SrvTenders;
using System.ComponentModel;
using SIMS.TM.Model;
using System.Collections.ObjectModel;

namespace Contruction
{
    public class TenderViewModel:ViewModel
    {
        TenderServiceClient service = new TenderServiceClient();

        public TenderViewModel()
        {
            if (!DesignerProperties.GetIsInDesignMode(Application.Current.RootVisual))
            {
                OnInit();
            }
        }

        private void OnInit()
        {
            service.GetAllTendersCompleted += new EventHandler<GetAllTendersCompletedEventArgs>(service_GetAllTendersCompleted);
            LoadTenders();

        }

        void service_GetAllTendersCompleted(object sender, GetAllTendersCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                Status = e.Error.Message;
                IsBusy = false;
            }
            else
            {
                Tenders = e.Result;
                IsBusy = false;
            }
        }

        IPaginated<MTender> _Tenders;
        public IPaginated<MTender> Tenders
        {
            get { return _Tenders; }
            set
            {
                _Tenders = value;                
                Notify("Tenders");
            }
        }

        ObservableCollection<MTenderLineGrp> _TenderLineGrps = new ObservableCollection<MTenderLineGrp>();
        public ObservableCollection<MTenderLineGrp> TenderLineGrps
        {
            get { return _TenderLineGrps; }
            set
            {
                _TenderLineGrps = value;
                Notify("TenderLineGrps");
            }
        }

        string _Status;
        public string Status
        {
            get { return _Status; }
            set
            {
                _Status = value;
                Notify("Status");
            }
        }

        public void LoadTenders()
        {
            service.GetAllTendersAsync(null,null, null, SearchKey, SiteID, true);
            IsBusy = true;
        }

        public void LoadTenderForID(int id)
        {
            Status = "Loading";
            service.GetTenderDatailsAsync(id);
        }

        int _PageIndex = 0;
        public int PageIndex
        {
            get { return _PageIndex; }
            set
            {
                _PageIndex = value;
                Notify("PageIndex");
                LoadTenders();
            }
        }

        string _SearchKey;
        public string SearchKey
        {
            get { return _SearchKey; }
            set
            {
                _SearchKey = value;
                Notify("SearchKey");
            }
        }

        int? _SiteID;
        public int? SiteID
        {
            get { return _SiteID; }
            set
            {
                _SiteID = value;
                Notify("SiteID");
            }
        }

        int? _VendorID;
        public int? VendorID
        {
            get { return _VendorID; }
            set
            {
                _VendorID = value;
                Notify("VendorID");
            }
        }

        public void Clear()
        {
            SearchKey = null;
            SiteID = null;
            VendorID = null;
            LoadTenders();
        }
    }
}
