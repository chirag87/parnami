﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Contruction.Srv_Employee;
using System.ComponentModel;
using SIMS.Core;
//using SIMS.EM.Model;

namespace Contruction.Models.Employee
{
    public class EmployeeMasterVM:ViewModel
    {
        public event EventHandler EmployeeChanged;
        public void RaiseEmployeeChanged()
        {
            if (EmployeeChanged != null)
                EmployeeChanged(this, new EventArgs());
        }

        EmployeeServiceClient proxy = new EmployeeServiceClient();        

        public  EmployeeMasterVM()
        {
            if (!DesignerProperties.GetIsInDesignMode(Application.Current.RootVisual))
            {
                OnInit();
            }
        }

        private void OnInit()
        {
            proxy.GetAllEmployeesCompleted += new EventHandler<GetAllEmployeesCompletedEventArgs>(proxy_GetAllEmployeesCompleted);
            LoadALlEmoloyees();
            proxy.CreateNewEmployeeAttendanceCompleted += new EventHandler<CreateNewEmployeeAttendanceCompletedEventArgs>(proxy_CreateNewEmployeeAttendanceCompleted);
            Base.EmployeeEvents.SSUpdated += new EventHandler<SSUpdatedArgs>(EmployeeEvents_SSUpdated);
        }

        void EmployeeEvents_SSUpdated(object sender, SSUpdatedArgs e)
        {
            LoadALlEmoloyees();
        }

        void proxy_CreateNewEmployeeAttendanceCompleted(object sender, CreateNewEmployeeAttendanceCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                IsBusy = false;
                Base.ShowError(e.Error);
            }
            else
            {
                IsBusy = false;
                //MessageBox.Show("Att Marked");
            }
        }

        void proxy_GetAllEmployeesCompleted(object sender, GetAllEmployeesCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                IsBusy = false;
                Base.ShowError(e.Error);
            }
            else
            {
                IsBusy = false;
                AllEmployees = e.Result;
            }
        }

        public void LoadALlEmoloyees()
        {
            IsBusy = true;
            if (AttDate == null)
                AttDate = DateTime.UtcNow.AddHours(5.5).Date;
            proxy.GetAllEmployeesAsync(null, PageIndex, PageSize, SearchKey, SiteID, AttDate, true);
        }

        public void  CreateNewAttendance(int ID,int EmpID,DateTime? ckInTime,DateTime? ckOutTime)
        {
            if (AttDate == null)
            {
                MessageBox.Show("Select Date For Attendance");
                return;
            }
            //IsBusy=true;
            NewEmpAttendance = new MEmployeeAttendance()
            {
                ID=ID,
                EmpID=EmpID,
                CheckINTime=ckInTime,
                CheckOutTime=ckOutTime,
                Date=AttDate.Value,
            };
            proxy.CreateNewEmployeeAttendanceAsync(NewEmpAttendance);
        }

        public void AttachForAllEmployees()
        {
            if (AllEmployees != null)
            {
                foreach (var item in AllEmployees.Data)
                {
                    item.EmployeeAttendacne.EmpID = item.ID;
                    item.EmployeeAttendacne.AttMark += new EventHandler(EmployeeAttendacne_AttMark);
                }
            }
        }

        void EmployeeAttendacne_AttMark(object sender, EventArgs e)
        {
            var empatt = sender as MEmployeeAttendance;
            CreateNewAttendance(empatt.ID,empatt.EmpID,empatt.CheckINTime,empatt.CheckOutTime);
        }

        PaginatedData<MEmployee> _AllEmployees;
        public PaginatedData<MEmployee> AllEmployees
        {
            get { return _AllEmployees; }
            set
            {
                _AllEmployees = value;
                Notify("AllEmployees");
                AttachForAllEmployees();
                RaiseEmployeeChanged();
            }
        }

        string _SearchKey;
        public string SearchKey
        {
            get { return _SearchKey; }
            set
            {
                _SearchKey = value;
                Notify("SearchKey");
            }
        }

        int? _SiteID;
        public int? SiteID
        {
            get { return _SiteID; }
            set
            {
                _SiteID = value;
                Notify("SiteID");
                LoadALlEmoloyees();
            }
        }

        int _PageIndex = 0;
        public int PageIndex
        {
            get { return _PageIndex; }
            set
            {
                _PageIndex = value;
                Notify("PageIndex");
                LoadALlEmoloyees();
            }
        }

        int _PageSize = 20;
        public int PageSize
        {
            get { return _PageSize; }
            set
            {
                _PageSize = value;
                Notify("PageSize");
                LoadALlEmoloyees();
            }
        }

        bool _IsVisibleSearchBoxDbtn = false;
        public bool IsVisibleSearchBoxDbtn
        {
            get { return _IsVisibleSearchBoxDbtn; }
            set
            {
                _IsVisibleSearchBoxDbtn = value;
                Notify("IsVisibleSearchBoxDbtn");
            }
        }

        DateTime? _AttDate;
        public DateTime? AttDate
        {
            get { return _AttDate; }
            set
            {
                _AttDate = value;
                Notify("AttDate");
                LoadALlEmoloyees();
            }
        }

        MEmployeeAttendance _NewEmpAttendance=new MEmployeeAttendance();
		public MEmployeeAttendance NewEmpAttendance
		{
			get{return _NewEmpAttendance;}
			set{_NewEmpAttendance = value;
			Notify("NewEmpAttendance");
			}
		}
    }
}
