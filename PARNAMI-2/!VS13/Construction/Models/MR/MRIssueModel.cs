﻿using System;
using System.Net;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Contruction.MRService;
using System.ComponentModel;
using SIMS.MM.MODEL;

namespace Contruction
{
    public class MRIssueModel : ViewModel
    {
        MRServiceClient service = new MRServiceClient();

        public event EventHandler Issue;
        private void RaiseIssue()
        {
            if (Issue != null)
                Issue(this, new EventArgs());
        }

        public MRIssueModel()
        {
            if (!DesignerProperties.GetIsInDesignMode(Application.Current.RootVisual))
            {
                OnInit();
            }
        }

        public void OnInit()
        {
            //service.GetMReceivingforMRIssueCompleted += new EventHandler<GetMReceivingforMRIssueCompletedEventArgs>(service_GetMReceivingforMRIssueCompleted);
            //service.IssueMRCompleted += new EventHandler<IssueMRCompletedEventArgs>(service_IssueMRCompleted);
            service.GetMRIssueTransactionByMRIDCompleted += new EventHandler<GetMRIssueTransactionByMRIDCompletedEventArgs>(service_GetMRIssueTransactionByMRIDCompleted);
            service.MRIssueCompleted += new EventHandler<MRIssueCompletedEventArgs>(service_MRIssueCompleted);
        }

        void service_MRIssueCompleted(object sender, MRIssueCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                Base.ShowError(e.Error);
                IsBusy = false;
            }
            else
            {
                IsBusy = false;
                IsIssued = true;
                TransactionID = e.Result;
                MessageBox.Show("Material Issued Successfully!");
                Base.AppEvents.RaiseNewMRAdded();
                RaiseIssue();
            }
        }

        void service_GetMRIssueTransactionByMRIDCompleted(object sender, GetMRIssueTransactionByMRIDCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                Base.ShowError(e.Error);
                IsBusy = false;
            }
            else
            {
                ReceivingforMRIssue = e.Result;
                ReceivingforMRIssue.IssuedBy = Base.Current.UserName;
                ReceivingforMRIssue.Requester = Base.Current.UserName;
                IsBusy = false;
            }
        }

        //void service_IssueMRCompleted(object sender, IssueMRCompletedEventArgs e)
        //{
        //    if (e.Error != null)
        //    {
        //        Base.ShowError(e.Error);
        //        IsBusy = false;
        //    }
        //    else
        //    {
        //        IsBusy = false;
        //        IsIssued = true;
        //        TransactionID = e.Result;
        //        MessageBox.Show("Material Issued Successfully!");
        //        Base.AppEvents.RaiseNewMRAdded();
        //        RaiseIssue();
        //    }
        //}

        //void service_GetMReceivingforMRIssueCompleted(object sender, GetMReceivingforMRIssueCompletedEventArgs e)
        //{
        //    if (e.Error != null)
        //    {
        //        Base.ShowError(e.Error);
        //        IsBusy = false;
        //    }
        //    else
        //    {
        //        ReceivingforMRIssue = e.Result;
        //        ReceivingforMRIssue.Purchaser = Base.Current.UserName;
        //        IsBusy = false;
        //    }
        //}

        //MReceiving _ReceivingforMRIssue = new MReceiving();
        //public MReceiving ReceivingforMRIssue
        //{
        //    get { return _ReceivingforMRIssue; }
        //    set
        //    {
        //        _ReceivingforMRIssue = value;
        //        Notify("ReceivingforMRIssue");
        //    }
        //}

        MMaterialIssue _ReceivingforMRIssue = new MMaterialIssue();
        public MMaterialIssue ReceivingforMRIssue
        {
            get { return _ReceivingforMRIssue; }
            set
            {
                _ReceivingforMRIssue = value;
                Notify("ReceivingforMRIssue");
            }
        }

        int _TransactionID;
        public int TransactionID
        {
            get { return _TransactionID; }
            set
            {
                _TransactionID = value;
                Notify("TransactionID");
            }
        }

        bool _IsIssued = false;
        public bool IsIssued
        {
            get { return _IsIssued; }
            set
            {
                _IsIssued = value;
                Notify("IsIssued");
            }
        }

        string _Status;
        public string Status
        {
            get { return _Status; }
            set
            {
                _Status = value;
                Notify("Status");
            }
        }

        public void GetMReceivingforMRIssue(int mrid)
        {
            //service.GetMReceivingforMRIssueAsync(mrid);
            service.GetMRIssueTransactionByMRIDAsync(mrid);
            IsBusy = true;
        }

        public void IssueMR()
        {
            if (!Validate())
            {
                IsIssued = false;
                return;
            }
            //service.IssueMRAsync(ReceivingforMRIssue);
            service.MRIssueAsync(ReceivingforMRIssue, Base.Current.UserName);
            IsBusy = true;
        }

        public bool Validate()
        {
            Status = "";
            //if (ReceivingforMRIssue.ReceivingLines.All(x => x.Quantity == 0.0)) { Status = "MR has some invaid/unselected Quantity!"; return false; }
            if (ReceivingforMRIssue.MaterialIssueLines.All(x => x.Quantity == 0.0)) { Status = "MR has some invaid/unselected Quantity!"; return false; }
            return true;
        }
    }
}
