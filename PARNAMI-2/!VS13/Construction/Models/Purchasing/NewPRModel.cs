﻿using System;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Collections.ObjectModel;
using Contruction.PurchasingService;
using SIMS.MM.MODEL;
using System.Collections.Generic;
using System.ComponentModel;

namespace Contruction
{
    public class NewPRModel : ViewModel
    {
        PurchasingServiceClient service = new PurchasingServiceClient();

        public NewPRModel()
        {
            if (!DesignerProperties.GetIsInDesignMode(Application.Current.RootVisual))
            {
                Reset();
                OnInit();
            }
        }

        #region Events...

        public class SplittedPRRequestedArgs : EventArgs
        {
            public MPR mpr { get; set; }
            public SplittedPRRequestedArgs(MPR _mpr) { mpr = _mpr; }
        }

        public event EventHandler<SplittedPRRequestedArgs> SaveSplitPR;
        public void RaiseSaveSplitPR(MPR newpr)
        {
            if (SaveSplitPR != null)
                SaveSplitPR(this, new SplittedPRRequestedArgs(newpr));
        }

        #endregion

        #region Properties...

        MPR _NewPR = new MPR();
        public MPR NewPR
        {
            get { return _NewPR; }
            set
            {
                _NewPR = value;
                Notify("NewPR");
            }
        }

        MPR _CreatedPR = new MPR();
        public MPR CreatedPR
        {
            get { return _CreatedPR; }
            set
            {
                _CreatedPR = value;
                Notify("CreatedPR");
            }
        }

        string _Status;
        public string Status
        {
            get { return _Status; }
            set
            {
                _Status = value;
                Notify("Status");
            }
        }

        #endregion

        #region Methods...

        public void OnInit()
        {
            service.CreateNewPRCompleted += new EventHandler<CreateNewPRCompletedEventArgs>(service_CreateNewPRCompleted);
            //service.SplitPRCompleted += new EventHandler<SplitPRCompletedEventArgs>(service_SplitPRCompleted);
            service.SplitPRwithPartialQtyCompleted += new EventHandler<SplitPRwithPartialQtyCompletedEventArgs>(service_SplitPRwithPartialQtyCompleted);
            service.GetTotalTODispacthQtyCompleted += new EventHandler<GetTotalTODispacthQtyCompletedEventArgs>(service_GetTotalTODispacthQtyCompleted);
            service.EmailDraftPRCompleted += new EventHandler<AsyncCompletedEventArgs>(service_EmailDraftPRCompleted);
        }

        public void AddNewLine()
        {
            var line = NewPR.AddNewLine();
            NewPR.SiteIDChanged += new EventHandler(NewPR_SiteIDChanged);
            line.CalculateTODispQty += new EventHandler(line_CalculateTODispQty);
            Notify("NewPR");
        }

        void NewPR_SiteIDChanged(object sender, EventArgs e)
        {
            foreach (var line in NewPR.PRLines)
            {
                line.RaiseCalculateTODispQty();
            }
        }

        void line_CalculateTODispQty(object sender, EventArgs e)
        {
            var line = sender as MPRLine;
            if (NewPR.SiteID != null && line.ItemID != null)
            {
                GetTODispatchQty(line.LineID, NewPR.SiteID, line.ItemID, line.SizeID, line.BrandID, line.ConsumableType);
            }
        }

        public void GetTODispatchQty(int Lineid, int siteid, int itemid, int? sizeid, int? brandid, string Contype)
        {
            service.GetTotalTODispacthQtyAsync(null, Lineid, siteid, itemid, sizeid, brandid, Contype);
            //IsBusy = true;
        }

        public void SubmitToDB()
        {
            if (!Validate()) return;
            foreach (var line in NewPR.PRLines) /*Copying Quantity in Original Qty*/
            {
                line.OriginalQty = line.Quantity;
            }
            service.CreateNewPRAsync(NewPR);
            IsBusy = true;
        }

        public void SplitPRwithPartialQty(int OldPRID, ObservableCollection<MSplittedLines> lines)
        {
            IsBusy = true;
            service.SplitPRwithPartialQtyAsync(OldPRID, lines);
        }

        public void Reset()
        {
            NewPR = new MPR();
            NewPR.RaisedBy = Base.Current.UserName;
            NewPR.Purchaser = Base.Current.UserName;
            Notify("NewPR");
        }

        public void UpdateTotal()
        {
            NewPR.UpdateTotalValue();
            Notify("NewPR");
        }

        public bool Validate()
        {
            Status = "";
            if (NewPR.VendorID == 0) { Status = "Please Select Vendor"; return false; }
            if (NewPR.SiteID == 0) { Status = "Please Select Site"; return false; }
            if (String.IsNullOrWhiteSpace(NewPR.Purchaser)) { Status = "Purchaser Cannot be Empty!"; return false; }
            if (String.IsNullOrWhiteSpace(NewPR.PurchaseType)) { Status = "Please Select Purchase Type"; return false; }
            if (NewPR.PRLines == null) { Status = "Material Indent should have alteast one item."; return false; }
            if (NewPR.PRLines.Count == 0) { Status = "Material Indent should have alteast one item."; return false; }
            if (NewPR.PRLines.Any(x => x.ItemID == 0)) { Status = "Indent Lines has some invaid/unselected Item/s!"; return false; }
            if (NewPR.PRLines.Any(x => x.SizeID == 0)) { Status = "Indent Lines has some invaid/unselected Size/s!"; return false; }
            if (NewPR.PRLines.Any(x => x.BrandID == 0)) { Status = "Indent Lines has some invaid/unselected Brand/s!"; return false; }
            return true;
        }

        public void DeleteLine(int lid)
        {
            try
            {
                var line = NewPR.PRLines.Single(x => x.LineID == lid);
                NewPR.PRLines.Remove(line);
                UpdateTotal();
                NewPR.UpdateLineNumbers();
            }
            catch { }
        }

        public void Email(int PRID)
        {
            service.EmailDraftPRAsync(PRID);
            IsBusy = true;
        }

        #endregion

        #region Completed Events...

        void service_GetTotalTODispacthQtyCompleted(object sender, GetTotalTODispacthQtyCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                Base.ShowError(e.Error);
                IsBusy = false;
            }
            else
            {
                var result = e.Result;
                NewPR.PRLines.Single(x => x.LineID == result.LineID
                    && x.ItemID == result.ItemID
                    && x.SizeID == result.SizeID
                    && x.BrandID == result.BrandID).TODispInfo.TODispQty = result.TODispQty;
                IsBusy = false;
            }
        }

        void service_SplitPRwithPartialQtyCompleted(object sender, SplitPRwithPartialQtyCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                Base.ShowError(e.Error);
                IsBusy = false;
            }
            else
            {
                IsBusy = false;
                CreatedPR = e.Result;
                RaiseSaveSplitPR(CreatedPR);
                MessageBox.Show("Reference No: " + CreatedPR.ReferenceNo + "\n\nNew Material Indent Added Successfully !");
                Reset();
                Base.AppEvents.RaiseNewPRRaised();
            }
        }

        //void service_SplitPRCompleted(object sender, SplitPRCompletedEventArgs e)
        //{
        //    if (e.Error != null)
        //    {
        //        Base.ShowError(e.Error);
        //        IsBusy = false;
        //    }
        //    else
        //    {
        //        IsBusy = false;
        //        CreatedPR = e.Result;
        //        RaiseSaveSplitPR(CreatedPR);
        //        MessageBox.Show("Reference No: " + CreatedPR.ReferenceNo + "\n\nNew Material Indent Added Successfully !");
        //        Reset();
        //        Base.AppEvents.RaiseNewPRRaised();
        //    }
        //}

        //public void CreateNewSplittedPR(MPR newpr, int OldPRID, ObservableCollection<int> lineIDs)
        //{
        //    IsBusy = true;
        //    //if (!Validate()) return;
        //    service.SplitPRAsync(newpr, OldPRID, lineIDs);
        //}

        void service_CreateNewPRCompleted(object sender, CreateNewPRCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                Base.ShowError(e.Error);
                IsBusy = false;
            }
            else
            {
                IsBusy = false;
                CreatedPR = e.Result;
                MessageBox.Show("Reference No: " + CreatedPR.ReferenceNo + "\n\nNew Material Indent Added Successfully !");
                Reset();
                Base.AppEvents.RaiseNewPRRaised();
                Email(CreatedPR.PRNumber);
            }
        }

        void service_EmailDraftPRCompleted(object sender, AsyncCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                Base.ShowError(e.Error);
                IsBusy = false;
            }
            else
            {
                IsBusy = false;
                MessageBox.Show("Email Sent Successfully!");
            }
        }

        #endregion
    }
}