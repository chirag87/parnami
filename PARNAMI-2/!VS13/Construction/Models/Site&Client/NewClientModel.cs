﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Contruction.MasterDBService;
using System.ComponentModel;
//using SIMS.MM.MODEL;

namespace Contruction
{
    public class NewClientModel : ViewModel
    {
        MasterDataServiceClient service = new MasterDataServiceClient();

        public NewClientModel()
        {
            if (!DesignerProperties.GetIsInDesignMode(Application.Current.RootVisual))
            {
                Reset();
                OnInit();
            }
        }

        MClient _NewClient = new MClient();
        public MClient NewClient
        {
            get { return _NewClient; }
            set
            {
                _NewClient = value;
                Notify("NewClient");
            }
        }

        public void OnInit()
        {
            service.CreateNewClientCompleted += new EventHandler<CreateNewClientCompletedEventArgs>(service_CreateNewClientCompleted);
            service.UpdateClientCompleted += new EventHandler<UpdateClientCompletedEventArgs>(service_UpdateClientCompleted);
        }

        void service_UpdateClientCompleted(object sender, UpdateClientCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                Base.ShowError(e.Error);
                IsBusy = false;
            }
            else
            {
                NewClient = e.Result;
                IsBusy = false;
                MessageBox.Show("Updation Successfully !");
                Base.Current.LoadClients();
                Base.Current.IsNew = new bool();
            }
        }

        void service_CreateNewClientCompleted(object sender, CreateNewClientCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                Base.ShowError(e.Error);
                IsBusy = false;
            }
            else
            {
                IsBusy = false;
                MessageBox.Show("New Client Added Successfully !");
                Base.Current.LoadClients();
                Base.Current.IsNew = new bool();
            }
        }

        public void SubmitToDB()
        {
            service.CreateNewClientAsync(NewClient);
            IsBusy = true;
        }

        public void UpdateClient()
        {
            service.UpdateClientAsync(NewClient);
            IsBusy = true;
        }

        public void Reset()
        {
            NewClient = new MClient();
        }
    }
}
