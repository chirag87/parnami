﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using SIMS.MM.MODEL.Headers;
using System.Collections.ObjectModel;
using System.Collections.Generic;
using System.ComponentModel;

namespace Contruction
{
    public class PendingBillsVM : ViewModel
    {
        public override void OnStateChanged()
        {
            base.OnStateChanged();
        }

        #region Core
        SrvVendorPayments.VendorPaymentsServiceClient service = new SrvVendorPayments.VendorPaymentsServiceClient();

        public PendingBillsVM()
        {
            if (!DesignerProperties.GetIsInDesignMode(Application.Current.RootVisual))
            {
                OnInit();
            }
        }

        public void OnInit()
        {
            service.GetPendingBillDetailsCompleted += new EventHandler<SrvVendorPayments.GetPendingBillDetailsCompletedEventArgs>(service_GetPendingBillDetailsCompleted);
            Load();
        }

        void service_GetPendingBillDetailsCompleted(object sender, SrvVendorPayments.GetPendingBillDetailsCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                Base.ShowError(e.Error);
                IsBusy = false;
            }
            else
            {
                //PendingBillDetails = null;
                PendingBillDetails = e.Result;
                Status = "";
                IsBusy = false;
            }
        }

        string _Status;
        public string Status
        {
            get { return _Status; }
            set
            {
                _Status = value;
                Notify("Status");
            }
        }

        #endregion
        
        #region Properties

        int? _VendorID;
        public int? VendorID
        {
            get { return _VendorID; }
            set
            {
                _VendorID = value;
                OnBaseDataChanged();
                Notify("VendorID");
            }
        }

        int? _SiteID;
        public int? SiteID
        {
            get { return _SiteID; }
            set
            {
                _SiteID = value;
                OnBaseDataChanged();
                Notify("SiteID");
            }
        }

        ObservableCollection<MPendingBillDetail> _PendingBillDetails = new ObservableCollection<MPendingBillDetail>();
        public ObservableCollection<MPendingBillDetail> PendingBillDetails
        {
            get { return _PendingBillDetails; }
            set
            {
                _PendingBillDetails = value;
                Notify("PendingBillDetails");
            }
        }


        IEnumerable<MPendingBillDetail> _SelectedBillDetails = new List<MPendingBillDetail>();
        public IEnumerable<MPendingBillDetail> SelectedBillDetails
        {
            get { return _SelectedBillDetails; }
            set
            {
                _SelectedBillDetails = value;
                Notify("SelectedBillDetails");
            }
        }

        List<MPendingBillDetail> _BillingDetailList = new List<MPendingBillDetail>();
        public List<MPendingBillDetail> BillingDetailList
        {
            get { return _BillingDetailList; }
            set
            {
                _BillingDetailList = value;
                Notify("BillingDetailList");
            }
        }

        #endregion

        void OnBaseDataChanged()
        {
            Load();
        }

        public void Load()
        {
            Status = "Loading Data ...";
            service.GetPendingBillDetailsAsync(VendorID, SiteID, null, null);
            IsBusy = true;
        }
    }
}