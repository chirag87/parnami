﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Contruction.MasterDBService;
using System.ComponentModel;
//using SIMS.MM.MODEL;
namespace Contruction
{
    public class AddNewItemCartegoryVM:ViewModel
    {
        public event EventHandler<UpdateItemCategoryArgs> Added;
        public void RaiseAdded(MItemCategory _category)
        {
            if (Added != null)
                Added(this, new UpdateItemCategoryArgs(_category));
        }

        MasterDataServiceClient proxy = new MasterDataServiceClient();
        		
        public AddNewItemCartegoryVM()
        {
            if (!DesignerProperties.GetIsInDesignMode(Application.Current.RootVisual))
            {
                
                OnInit();
            }
        }

        private void OnInit()
        {
            proxy.CreateNewItemCategoryCompleted += new EventHandler<CreateNewItemCategoryCompletedEventArgs>(proxy_CreateNewItemCategoryCompleted);
        }

        void proxy_CreateNewItemCategoryCompleted(object sender, CreateNewItemCategoryCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                Base.ShowError(e.Error);
                IsBusy = false;
            }
            else
            {
                MessageBox.Show("New ItemCategory Added Successfully !");
                RaiseAdded(e.Result);
                IsBusy = false;
                NewItemCategory = e.Result;
                Notify("NewItemCategory");
            }
        }

        public void SubmitToDB()
        {
            if(ParentItemCategory.ID != 0)
                NewItemCategory.ParentID = ParentItemCategory.ID;
            else
                NewItemCategory.IsSystemDefined = true;
            proxy.CreateNewItemCategoryAsync(NewItemCategory);
            IsBusy = true;
        }

        public void AddNewLeafCategory(MItemCategory leafCategory)
        {
            proxy.CreateNewItemCategoryAsync(leafCategory);
            IsBusy = true;
        }

        bool _CanHaveItemsVisibility = false;
        public bool CanHaveItemsVisibility
        {
            get { return _CanHaveItemsVisibility; }
            set
            {
                _CanHaveItemsVisibility = value;
                Notify("CanHaveItemsVisibility");
            }
        }

        MItemCategory _NewItemCategory = new MItemCategory();
        public MItemCategory NewItemCategory
        {
            get { return _NewItemCategory; }
            set
            {
                _NewItemCategory = value;
                Notify("NewItemCategory");
            }
        }

        MItemCategory _ParentItemCategory = new MItemCategory();
        public MItemCategory ParentItemCategory
        {
            get { return _ParentItemCategory; }
            set
            {
                _ParentItemCategory = value;
                Notify("ParentItemCategory");
            }
        }
    }
}
