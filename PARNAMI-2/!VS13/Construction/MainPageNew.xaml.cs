﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace Contruction
{
    public partial class MainPageNew : UserControl
    {
        public MainPageNew()
        {
            InitializeComponent();
            Base.Current.InitForNavigation(this.ContentBorder);
            Base.Current.MainTitleBlock = this.tblkMainHeader;
            //LoginPage p = new LoginPage();
            //p.Visibility = Visibility.Visible;
            ContentBorder.Child = new LoginPage();
            Marquee.Begin();
        }

        private void btnChangePassword_Click(object sender, RoutedEventArgs e)
        {
            ChangePassword cp = new ChangePassword();
            cp.Show();
        }
    }
}