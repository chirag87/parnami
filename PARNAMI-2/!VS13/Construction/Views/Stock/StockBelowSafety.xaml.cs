﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.ComponentModel;
using Contruction.MasterDBService;
using System.Collections.ObjectModel;
//using SIMS.MM.MODEL;

namespace Contruction
{
    public partial class StockBelowSafety : UserControl, INotifyPropertyChanged
    {
        public StockBelowSafetyModel Model
        {
            get { return (StockBelowSafetyModel)this.DataContext; }
        }
		
        public StockBelowSafety()
        {
            InitializeComponent();
        }

        public int? siteid
        {
            get { return Model.siteid; }
            set
            {
                Model.siteid = value;
                Notify("siteid");
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void Notify(string propertyName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
