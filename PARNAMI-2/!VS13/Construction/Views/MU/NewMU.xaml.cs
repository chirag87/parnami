﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace Contruction
{
    public partial class NewMU : ChildWindow
    {
        public NewMUModel Model
        {
            get { return (NewMUModel)this.DataContext; }
        }

        public NewMU()
        {
            InitializeComponent();
        }

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            var btn  =sender as Button;
            var data = (NewMUModel)btn.DataContext;
            if (String.IsNullOrWhiteSpace(data.NewMU.MU))
            {
                MessageBox.Show("Please Select a Proper Unit Name!");
            }
            if (String.IsNullOrWhiteSpace(data.NewMU.FormalName))
            {
                MessageBox.Show("Please Select a Proper Full Unit Name!");
            }
            if (String.IsNullOrWhiteSpace(data.NewMU.Type))
            {
                MessageBox.Show("Please Select a Proper Unit Type!");
            }
            else
            {
                Model.SubmitToDB();
                this.DialogResult = true;
            }
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

        private void ResetButton_Click(object sender, RoutedEventArgs e)
        {
            Model.Reset();
        }

        private void ChildWindow_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Escape)
            {
                this.DialogResult = false;
            }
        }

        private void ChildWindow_Loaded(object sender, RoutedEventArgs e)
        {
            tbxMU.Focus();
        }
    }
}

