﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace Contruction
{
    public partial class AllMUs : UserControl
    {
        public AllMUs()
        {
            InitializeComponent();
        }

        private void btnNew_Click(object sender, RoutedEventArgs e)
        {
            NewMU mu = new NewMU();
            mu.Show();
        }

        private void btnExport_Click(object sender, RoutedEventArgs e)
        {
            dgdMUs.Export();
        }

        private void tbxKey_TextChanged(object sender, TextChangedEventArgs e)
        {
            var box = sender as TextBox;
            Base.Current.SearchMU = box.Text;
        }
    }
}