﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace Contruction
{
    public partial class TenderDetailView : UserControl
    {
        public TenderDetailedViewModel Model
        {
            get { return (TenderDetailedViewModel)this.DataContext; }
        }
        public TenderDetailView( int id)
        {
            InitializeComponent();
            Model.GetTender(id);
            Model.IsBusy = true;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Base.Redirect(new AllTenders());
        }
    }
}
