﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace Contruction
{
    public partial class MRIssue : ChildWindow
    {
        public MRIssueModel Model
        {
            get { return (MRIssueModel)this.DataContext; }
        }

        public MRIssue(int id)
        {
            InitializeComponent();
            Model.GetMReceivingforMRIssue(id);
            Model.Issue += new EventHandler(Model_Issue);
        }

        void Model_Issue(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnIssue_Click(object sender, RoutedEventArgs e)
        {

        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

        private void ChildWindow_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Escape)
                this.DialogResult = false;
        }

        private void ChildWindow_Loaded(object sender, RoutedEventArgs e)
        {
            tbxRequester.Focus();
        }
    }
}

