﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using SIMS.MM.MODEL;
using SIMS.Core;

namespace Contruction
{
    public partial class AllMRs : UserControl
    {
        public ViewMRModel Model
        {
            get { return (ViewMRModel)this.DataContext; }
        }
				
        public AllMRs()
        {
            InitializeComponent();
            Model.MRChanged += new EventHandler(Model_MRChanged);
        }

        void Model_MRChanged(object sender, EventArgs e)
        {
            myPager.PagedData = (IPagination)Model.MRs;
        }

        private void myPager_MoveToPage(object sender, Controls.MyPageChangedEventArgs e)
        {
            Model.PageIndex = e.RequestedPage;
        }

        private void Image_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            var img = sender as Image;
            var mrheader = (MMRHeader)img.DataContext;
            if (mrheader == null) return;
            MRDetailedView mrd = new MRDetailedView(mrheader.ID);
            mrd.Show();
        }

        private void dataGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var SelectedRow = (MMRHeader)(sender as DataGrid).SelectedItem;
            if (SelectedRow == null) return;
            Base.AppEvents.RaiseMRRequested(SelectedRow.ID);
        }

        private void btnIssue_Click(object sender, RoutedEventArgs e)
        {
            var btn = sender as HyperlinkButton;
            var line = (MMRHeader)btn.DataContext;
            if (line == null) return;
            MRIssue mri = new MRIssue(line.ID);
            mri.Show();
        }

        private void btnReturn_Click(object sender, RoutedEventArgs e)
        {
            var btn = sender as HyperlinkButton;
            var line = (MMRHeader)btn.DataContext;
            if (line == null) return;
            MRReturn mrr = new MRReturn(line.ID); // Passing MRID
            mrr.Show();
        }

        private void SearchKey_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                Model.SearchKey = SearchKey.Text;
                Model.LoadMRs();
            }
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            SearchKey.Focus();
        }
    }
}
