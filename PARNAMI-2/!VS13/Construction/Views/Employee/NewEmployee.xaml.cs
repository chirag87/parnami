﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Contruction.Models.Employee;
using System.Windows.Media.Imaging;
using System.IO;
using System.IO.IsolatedStorage;

namespace Contruction.Views.Employee
{
    public partial class NewEmployee : UserControl
    {
        private CaptureSource _capture;
        //private MainPageViewModel _viewModel;
        private IsolatedStorageFile _isf;
        private SaveFileDialog _saveFileDlg;

        public NewEmployeeVM Model
        {
            get { return (NewEmployeeVM)this.DataContext; }
        }
				
        public NewEmployee()
        {
            InitializeComponent();
            //OnLoad();
        }

        private void OnLoad()
        {
            //instantiate our CaptureSource
            _capture = new CaptureSource();

            //get access to issolated storage so we can store a copy of the capture
            _isf = IsolatedStorageFile.GetUserStoreForApplication();

            //when a user right clicks on an image in the wrap panel we are going to allow them to
            //store it to the file system.
            _saveFileDlg = new SaveFileDialog
            {
                DefaultExt = ".jpg",
                Filter = "JPEG Images (*jpeg *.jpg)|*.jpeg;*.jpg",
            };

            //since we declared our ViewModel in xaml i will pull  reference from the controls resources
            //_viewModel = Resources["MainViewModel"] as MainPageViewModel;


            //load up the list from issolated storage
            string[] names = _isf.GetFileNames("*.jpg");
            foreach (string name in names)
            {
                using (IsolatedStorageFileStream isfs = new IsolatedStorageFileStream(name, FileMode.Open, _isf))
                {
                    BitmapImage image = new BitmapImage();
                    image.SetSource(isfs);
                    WriteableBitmap writeableBitmap = new WriteableBitmap(image);
                    //_viewModel.Captures.Add(new Capture() { Name = name, Bitmap = writeableBitmap });
                }
            }
        }

        public NewEmployee(int EmpID):this()
        {
            Model.IsFromAddNewForm = false;
            Model.GetEmployeeByID(EmpID);
        }

        public NewEmployee(bool flag)
            : this()
        {
            Model.IsFromAddNewForm = flag;
        }

        private void btnSaveChanges_Click(object sender, RoutedEventArgs e)
        {
            if (valSum.Errors.Count == 0)
                Model.SubmitToDb();
        }

        private void btnSelectImage_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog objFileDialog = new OpenFileDialog();
            objFileDialog.Filter = "JPEG Images|*.jpg|PNG Images|*.png";
            bool? IsSelected = objFileDialog.ShowDialog();
            try
            {
                if (IsSelected == true)
                {
                    BitmapImage bitImage = new BitmapImage();
                    bitImage.SetSource(objFileDialog.File.OpenRead());

                    imgsrc.Source = bitImage;
                    string fileName = objFileDialog.File.Name;
                    tbxImageName.Text = fileName;
                    //var photopath= new Uri("EmployeePics/" + fileName);
                    testbox.Text = fileName;
                    //filesToUpload = objFileDialog.Files.ToList();
                    var file = objFileDialog.File;

                    //foreach (FileInfo file in filesToUpload)
                    //{
                        //Define the Url object for the Handler
                        UriBuilder handlerUrl = new UriBuilder("http://localhost:3385/UploadFileHandler.ashx");
                        //Set the QueryString
                        handlerUrl.Query = "InputFile=" + file.Name;
                        FileStream FsInputFile = file.OpenRead();
                        //Define the WebClient for Uploading the Data
                        WebClient webClient = new WebClient();
                        //Now make an async class for writing the file to the server
                        //Here I am using Lambda Expression

                        webClient.OpenWriteCompleted += (s, evt) =>
                        {
                            UploadFileData(FsInputFile, evt.Result);
                            evt.Result.Close();
                            FsInputFile.Close();
                        };
                        webClient.OpenWriteAsync(handlerUrl.Uri);
                    //}


                }
                else
                {
                    //TextBlock1.Text = "Please select an image to upload ....";

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        private void UploadFileData(Stream inputFile, Stream resultFile)
        {
            byte[] fileData = new byte[4096];
            int fileDataToRead;

            while ((fileDataToRead = inputFile.Read(fileData, 0, fileData.Length)) != 0)
            {
                resultFile.Write(fileData, 0, fileDataToRead);
            }
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            Base.Redirect(new EmployeeMaster(), "Employees");
        }

        private void imgsrc_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            //textClickMsg.Visibility = System.Windows.Visibility.Collapsed;
            if (_capture != null)
            {
                _capture.Stop();

                _capture.VideoCaptureDevice = CaptureDeviceConfiguration.GetDefaultVideoCaptureDevice();

                VideoBrush videoBrush = new VideoBrush();
                videoBrush.Stretch = Stretch.Uniform;
                videoBrush.SetSource(_capture);
                rectVideo.Fill = videoBrush;

                //foreach (Rectangle element in this.effects.Children)
                //{
                //    element.Fill = videoBrush;
                //}

                if (CaptureDeviceConfiguration.AllowedDeviceAccess || CaptureDeviceConfiguration.RequestDeviceAccess())
                {
                    _capture.Start();
                }
            }
        }

        private void rectVideo_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            //textClickMsg.Visibility = System.Windows.Visibility.Collapsed;
            if (_capture != null)
            {
                _capture.Stop();

                _capture.VideoCaptureDevice = CaptureDeviceConfiguration.GetDefaultVideoCaptureDevice();

                VideoBrush videoBrush = new VideoBrush();
                videoBrush.Stretch = Stretch.Uniform;
                videoBrush.SetSource(_capture);
                rectVideo.Fill = videoBrush;

                //foreach (Rectangle element in this.effects.Children)
                //{
                //    element.Fill = videoBrush;
                //}

                if (CaptureDeviceConfiguration.AllowedDeviceAccess || CaptureDeviceConfiguration.RequestDeviceAccess())
                {
                    _capture.Start();
                }
            }
        }

        private void btnSelectImage_Copy_Click(object sender, RoutedEventArgs e)
        {
            if (_capture != null)
            {
                //This approach is effective if you are not applying an effect to the image, but
                //if you are we will need to capture a writableBitmap directly from the destination 
                //of our video brush.

                //_capture.AsyncCaptureImage((capturedImage) => _viewModel.Captures.Add(capturedImage));

                try
                {
                    WriteableBitmap writeableBitmap = new WriteableBitmap(rectVideo, null);
                    string name = Guid.NewGuid().ToString() + ".jpg";

                    //store the image in a collection in my viewmodel
                    //_viewModel.Captures.Add(new Capture() { Name = name, Bitmap = writeableBitmap });

                    using (IsolatedStorageFileStream isfs = new IsolatedStorageFileStream(name, FileMode.CreateNew, _isf))
                    {
                        MemoryStream stream = new MemoryStream();
                        //writeableBitmap.EncodeJpeg(stream);
                        stream.CopyTo(isfs);
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "Error saving snapshot", MessageBoxButton.OK);
                }
            }
        }
    }
}
