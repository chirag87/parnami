﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
//using SIMS.MM.MODEL;

namespace Contruction
{
    public partial class PRDetailedView : ChildWindow
    {
        public ViewPRDetailsModel Model
        {
            get { return (ViewPRDetailsModel)this.DataContext; }
        }

        public PRDetailedView()
        {
            InitializeComponent();
            Base.AppEvents.POConverted += new EventHandler<PORequestedArgs>(AppEvents_POConverted);
        }

        void AppEvents_POConverted(object sender, PORequestedArgs e)
        {
            this.DialogResult = false;
            var win = new PODetailView();
            win.SetID(e.POID);
            win.CreatePopup();
        }			

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }
    }
}

