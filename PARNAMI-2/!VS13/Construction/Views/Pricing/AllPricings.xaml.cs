﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.ComponentModel;

namespace Contruction
{
    public partial class AllPricings : UserControl, INotifyPropertyChanged
    {
        public AllPricings()
        {
            InitializeComponent();
        }

        private void btnNew_Click(object sender, RoutedEventArgs e)
        {
            NewPricing an = new NewPricing();
            Base.Redirect(an);
        }

        private void btnExport_Click(object sender, RoutedEventArgs e)
        {
            dgd.Export();
        }

        private void btnSearch_Click(object sender, RoutedEventArgs e)
        {
            Base.Current.LoadPricing();
        }

        private void tbxSearchBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            var box = sender as TextBox;
            Base.Current.SearchPricing = box.Text;
        }

        
        public event PropertyChangedEventHandler PropertyChanged;
        public void Notify(string propertyName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }

        private void tbxSearchBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                Base.Current.SearchPricing = tbxSearchBox.Text;
                Base.Current.LoadPricing();
            }
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            tbxSearchBox.Focus();
        }
    }
}