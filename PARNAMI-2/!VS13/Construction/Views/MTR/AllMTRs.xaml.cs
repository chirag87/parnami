﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using SIMS.MM.MODEL;

namespace Contruction
{
    public partial class AllMTRs : UserControl
    {
        public MTRViewModel Model
        {
            get { return (MTRViewModel)this.DataContext; }
        }
				

        public AllMTRs()
        {
            InitializeComponent();
            Model.MTRChanged += new EventHandler(Model_MTRChanged);
        }

        void Model_MTRChanged(object sender, EventArgs e)
        {
            myPager.PagedData = (IPagination)Model.MTRs;
        }

        private void MTRRelease_Click(object sender, RoutedEventArgs e)
        {

        }

        private void MTRReceive_Click(object sender, RoutedEventArgs e)
        {

        }

        private void MTRApprove_Click(object sender, RoutedEventArgs e)
        {

        }

        private void Image_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            var img = sender as Image;
            var mtrheader = (MMTRHeader)img.DataContext;
            if (mtrheader == null) return;
            Base.AppEvents.RaiseMTRRequested(mtrheader.ID);
            MTRDetailedView mtrdv = new MTRDetailedView();
            mtrdv.Show();
        }

        private void DataGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var SelectedRow = (MMTRHeader)(sender as DataGrid).SelectedItem;
            if (SelectedRow == null) return;
            Base.AppEvents.RaiseMTRRequested(SelectedRow.ID);
        }

        private void myPager_MoveToPage(object sender, Controls.MyPageChangedEventArgs e)
        {
            Model.PageIndex = e.RequestedPage;
        }
    }
}
