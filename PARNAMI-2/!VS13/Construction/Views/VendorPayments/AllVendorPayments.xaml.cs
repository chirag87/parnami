﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using SIMS.MM.MODEL;
using SIMS.Core;

namespace Contruction
{
	public partial class AllVendorPayments : UserControl
	{
        public AllVendorPaymentVM Model
        {
            get { return (AllVendorPaymentVM)this.DataContext; }
        }
		
		public AllVendorPayments()
		{
			InitializeComponent();
            Model.VPDataChanged += new EventHandler(Model_VPDataChanged);
		}

        void Model_VPDataChanged(object sender, EventArgs e)
        {
            myPager.PagedData = (IPagination)Model.AllPayments;
        }

        private void myPager_MoveToPage(object sender, Controls.MyPageChangedEventArgs e)
        {
            Model.pageindex = e.RequestedPage;
        }

        private void SearchBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                Model.Load();
            }
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            SearchBox.Focus();
        }
	}
}