﻿using System;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using SIMS.MM.MODEL.Headers;
using System.Collections.ObjectModel;
using System.Collections.Generic;
using SIMS.MM.MODEL;
using System.ComponentModel;

namespace Contruction
{
	public partial class PendingBills : UserControl, INotifyPropertyChanged
	{
        public PendingBillsVM Model
        {
            get { return (PendingBillsVM)this.DataContext; }
        }
		
		public PendingBills()
		{
			// Required to initialize variables
			InitializeComponent();
		}

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            this.MyPopUP.Close();
        }

        public IEnumerable<MPendingBillDetail> SelectedBillDetails { get; set; }

        private void btnSelect_Click(object sender, RoutedEventArgs e)
        {
            var btn = sender as Button;
            var data = dgd.SelectedItems;
            if (data == null) return;
            List<MPendingBillDetail> _list = new List<MPendingBillDetail>();
            foreach (var a in data)
            {
                _list.Add((MPendingBillDetail)a);
            }
            PaymentDetailList = ConverttoPaymentLines(_list);
            if(IsPopUpMode)
                this.MyPopUP.Close();
            RaiseSelected();
        }

        public ObservableCollection<MVendorPaymentDetail> ConverttoPaymentLines(List<MPendingBillDetail> _Pendingbills)
        {
            ObservableCollection<MVendorPaymentDetail> Paymentlist = new ObservableCollection<MVendorPaymentDetail>();
            int i = 0;

            foreach (var bill in _Pendingbills)
            {
                i++;
                MVendorPaymentDetail payment = new MVendorPaymentDetail()
                {
                    LineID = i,
                    Paid = (bill.Total - bill.Pending),
                    BillDate = bill.BillDate,
                    BillID = bill.ID,
                    BillNo = bill.BillNo,
                    Total = bill.Total,
                    Remarks = bill.Remarks,
                };
                Paymentlist.Add(payment);
            }
            return Paymentlist;
        }

        public event EventHandler Selected;
        public void RaiseSelected()
        {
            if (Selected != null)
                Selected(this, new EventArgs());
        }

        ObservableCollection<MVendorPaymentDetail> _PaymentDetailList = new ObservableCollection<MVendorPaymentDetail>();
        public ObservableCollection<MVendorPaymentDetail> PaymentDetailList
        {
            get { return _PaymentDetailList; }
            set
            {
                _PaymentDetailList = value;
                Notify("PaymentDetailList");
            }
        }
        
        #region CreatePopUP Members
        public bool IsPopUpMode
        {
            get { return MyPopUP != null; }
        }

        public PopUp MyPopUP { get; set; }

        public PopUp CreatePopup()
        {
            MyPopUP = this.ToPopup(false);
            MyPopUP.SetAutoLayout();
            MyPopUP.Title = "PENDING BILLS";
            return MyPopUP;
        }
        #endregion

        public event PropertyChangedEventHandler PropertyChanged;
        public void Notify(string propertyName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            VendorCombo.Focus();
        }
	}
}