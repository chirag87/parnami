﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.ComponentModel;

namespace Contruction
{
    public partial class AccountsWindow : UserControl, INotifyPropertyChanged
    {
        public AccountsWindowViewModel Model
        {
            get { return (AccountsWindowViewModel)this.DataContext; }
        }
		
        public AccountsWindow()
        {
            InitializeComponent();
        }

        public int ForID
        {
            get { return Model.ForID; }
            set
            {
                Model.ForID = value;
                Notify("ForID");
            }
        }

        public string For
        {
            get { return Model.For; }
            set
            {
                Model.For = value;
                Notify("For");
            }
        }

        private void btnAddNew_Click(object sender, RoutedEventArgs e)
        {
            NewAccount acc = new NewAccount(ForID);
            acc.Show();
        }

        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            Model.UpdateAccount();
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void Notify(string propertyName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            accCombo.Focus();
        }
    }
}
