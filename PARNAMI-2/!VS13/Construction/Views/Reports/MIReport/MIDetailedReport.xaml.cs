﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace Contruction
{
    public partial class MIDetailedReport : UserControl
    {
        public MIDetailedReportVM Model
        {
            get { return (MIDetailedReportVM)this.DataContext; }
        }

        public MIDetailedReport()
        {
            InitializeComponent();
        }

        private void btnLoad_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            Model.Load();
        }

        private void ReportType_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            // TODO: Add event handler implementation here.
        }

        private void btnClearSet1_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            // TODO: Add event handler implementation here.
        }

        private void btnClearSet2_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            // TODO: Add event handler implementation here.
        }
    }
}