﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.ComponentModel;
//using SIMS.MM.MODEL;

namespace Contruction
{
    public partial class MonthlyStockReport : UserControl, INotifyPropertyChanged
    {
        public MonthlyStockReportsModel Model
        {
            get { return (MonthlyStockReportsModel)this.DataContext; }
        }
				
        public MonthlyStockReport()
        {
            InitializeComponent();
        }

        public MonthlyStockReport(int siteid, int? itemCatID, int itemid, int? sizeid, int? brandid)
            :this()
        {
            Model.Load(siteid, itemCatID, itemid, sizeid, brandid);

            //Set Site and Item ID for Displaying on Monthly Stock Report Screen
            Model.SiteID = siteid;
            Model.ItemCatID = itemCatID;
            Model.ItemID = itemid;
            Model.SizeID = sizeid;
            Model.BrandID = brandid;

            //Set Site and ItemID for New Daily Stock Report Tab Control Purpose
            SiteID = siteid;
            ItemCatID = itemCatID;
            ItemID = itemid;
            SizeID = sizeid;
            BrandID = brandid;
        }

        private void btnMonth_Click(object sender, RoutedEventArgs e)
        {
            var btn = sender as HyperlinkButton;
            var data = (MMonthlyStockReport)btn.DataContext;
            SDate = new DateTime(data.Year.Value, data.month.Value, 1); // Getting First date of Month
            EDate = SDate.AddMonths(1).AddDays(-1);                     // Getting Last date of Month
            OpeningStock = data.OpeningBalance;                         // Getting Initial Opening Stock for the Month
            OpenNewTabItem(data);
        }

        public void OpenNewTabItem(MMonthlyStockReport data)
        {
            if (data != null)
            {
                var tab = new CloseableTabItem();

                tab.Header = data.DisplayDateFormat;
                tab.Content = new DailyStockReport(SDate, EDate, SiteID, ItemCatID, ItemID, SizeID, BrandID, OpeningStock);
                tab.Name = data.DisplayDateFormat + ItemID + SizeID + BrandID + SiteID + " Details";
                string name = data.DisplayDateFormat + ItemID + SizeID + BrandID + SiteID + " Details";

                var tabitem = TabControl.Items.OfType<TabItem>().SingleOrDefault(x => x.Name == name);
                if (TabControl.Items.Contains(tabitem))
                {
                    TabControl.SelectedItem = tabitem;
                }
                else
                {
                    try
                    {
                        TabControl.Items.Add(tab);
                        TabControl.SelectedItem = tab;
                    }
                    catch
                    {

                    }
                }
            }
        }

        //private void imgDeleteTab_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        //{
        //    var img = sender as Image;
        //    var data = (MMonthlyStockReport)img.DataContext;
        //    var tabitem = TabControl.Items.OfType<TabItem>().SingleOrDefault(x => x.Name == data.DisplayDateFormat + ItemID + SiteID + " Details");
        //    if (tabitem != null)
        //        TabControl.Items.Remove(tabitem);
        //}

        private void btnExport_Click(object sender, RoutedEventArgs e)
        {
            dgdMonthlyStock.Export();
        }

        #region Properties

        TabControl _TabControl = new TabControl();
        public TabControl TabControl
        {
            get { return _TabControl; }
            set
            {
                _TabControl = value;
                Notify("TabControl");
            }
        }

        DataTemplate _HeaderTemp = new DataTemplate();
        public DataTemplate HeaderTemp
        {
            get { return _HeaderTemp; }
            set
            {
                _HeaderTemp = value;
                Notify("HeaderTemp");
            }
        }

        double? _OpeningStock;
        public double? OpeningStock
        {
            get { return _OpeningStock; }
            set
            {
                _OpeningStock = value;
                Notify("OpeningStock");
            }
        }

        int _SiteID;
        public int SiteID
        {
            get { return _SiteID; }
            set
            {
                _SiteID = value;
                Notify("SiteID");
            }
        }

        int? _ItemCatID;
        public int? ItemCatID
        {
            get { return _ItemCatID; }
            set
            {
                _ItemCatID = value;
                Notify("ItemCatID");
            }
        }

        int _ItemID;
        public int ItemID
        {
            get { return _ItemID; }
            set
            {
                _ItemID = value;
                Notify("ItemID");
            }
        }

        int? _SizeID;
        public int? SizeID
        {
            get { return _SizeID; }
            set
            {
                _SizeID = value;
                Notify("SizeID");
            }
        }

        int? _BrandID;
        public int? BrandID
        {
            get { return _BrandID; }
            set
            {
                _BrandID = value;
                Notify("BrandID");
            }
        }

        DateTime _SDate;
        public DateTime SDate
        {
            get { return _SDate; }
            set
            {
                _SDate = value;
                Notify("SDate");
            }
        }

        DateTime _EDate;
        public DateTime EDate
        {
            get { return _EDate; }
            set
            {
                _EDate = value;
                Notify("EDate");
            }
        }

        #endregion 

        #region INotifyPropertyChanged
        public event PropertyChangedEventHandler PropertyChanged;
        public void Notify(string propertyName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion
    }
}