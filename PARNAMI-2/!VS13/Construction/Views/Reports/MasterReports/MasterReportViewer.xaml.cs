﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Collections.Generic;
using System.Net;
using System.IO;

namespace Contruction
{
    public partial class MasterReportViewer : UserControl
    {
        public MasterReportModel Model
        {
            get { return (MasterReportModel)this.DataContext; }
        }

        public MasterReportViewer()
        {
            // Required to initialize variables
            InitializeComponent();
        }

        private void btnExport_Click(object sender, RoutedEventArgs e)
        {
            datagrid.Export();
        }

        private void hbtnClear_Click(object sender, RoutedEventArgs e)
        {
            reportTypeCombo.SelectedItem = null;
            rangeCombo.SelectedItem = null;
            dpStart.Text = null;
            dpEnd.Text = null;
            Model.Data = new List<object>();
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            reportTypeCombo.Focus();
        }

        SaveFileDialog sfd;

        private void download_Click(object sender, RoutedEventArgs e)
        {
            Model.IsBusy = true;
            sfd = new SaveFileDialog();
            if (Model.RType != null)
            {
                if (Model.RType == "Excel")
                    sfd.Filter = "Excel files (*.xls)|*.xls";
                else if (Model.RType == "PDF")
                    sfd.Filter = "PDF file format|*.pdf";
            }
            else
            {
                sfd.Filter = "PDF file format|*.pdf";
            }
            if ((bool)sfd.ShowDialog())
            {
                var hp = sender as HyperlinkButton;
                var data = (MasterReportModel)hp.DataContext;

                WebClient webClient = new WebClient();
                webClient.OpenReadCompleted += (s, args) =>
                {
                    StreamReader sr = new StreamReader(args.Result);
                    byte[] bytes = new byte[args.Result.Length];
                    // revisit Here
                    args.Result.Read(bytes, 0, (int)args.Result.Length);
                    Stream st = sfd.OpenFile();
                    st.Write(bytes, 0, (int)args.Result.Length);
                    st.Close();
                    MessageBox.Show("DOWNLOAD COMPLETE!" + "\nFile Name: " + sfd.SafeFileName);
                    Model.IsBusy = false;
                };

                if (!String.IsNullOrWhiteSpace(data.SelectedReportType))
                {
                    Uri uri = new Uri("http://" + Base.Current.AppSettings.ServerPath);
                    if (data.SelectedReportType == "MRNs")
                        uri = new Uri("http://" + Base.Current.AppSettings.ServerPath + "/GetReport.ashx?rtype=pdf&type=GRNData&" + "&sdate=" + Model.StartDate.ToString() + "&edate=" + Model.EndDate.ToString() + "&siteid=" + Model.SiteID.ToString() + "&vendorid=" + Model.VendorID.ToString() + "&itemid=" + Model.ItemID.ToString() + "&sizeid=" + Model.SizeID.ToString() + "&brandid=" + Model.BrandID.ToString(), UriKind.Absolute);
                    webClient.OpenReadAsync(uri);
                }
                else
                    MessageBox.Show("Please Select adequate Report Type!");
            }
        }
    }
}