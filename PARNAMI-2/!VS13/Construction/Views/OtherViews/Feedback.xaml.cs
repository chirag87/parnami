﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.IO;

namespace Contruction
{
    public partial class Feedback : UserControl
    {
        public FeedbackModel Model
        {
            get { return (FeedbackModel)this.DataContext; }
        }
				
        public Feedback()
        {
            InitializeComponent();
            tbxUsername.Text = Base.Current.UserName;
        }

        public void btnBrowse_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog dlg = new OpenFileDialog();
            dlg.Multiselect = false;
            dlg.Filter = "All files|*.*";

            if ((bool)dlg.ShowDialog())
            {
                WMtbxUpload.Text = dlg.File.Name;
                UploadFile(dlg.File.Name, dlg.File.OpenRead());
            }
            else
            {
                WMtbxUpload.Text = "No File Selected...";
            }

        }

        private void UploadFile(string fileName, FileStream data)
        {
            UriBuilder ub = new UriBuilder("http://locathost:3385/FileUpload/FileReceiver.ashx");
            ub.Query = string.Format("File Name: {0}", fileName);

            WebClient c = new WebClient();
            c.OpenWriteCompleted += (sender, e) =>
            {
                PushData(data, e.Result);
                e.Result.Close();
                data.Close();
            };
            c.OpenWriteAsync(ub.Uri);
        }

        public void PushData(FileStream input, Stream output)
        {
            byte[] buffer = new byte[4096];
            int bytesread;

            while ((bytesread = input.Read(buffer, 0, buffer.Length)) !=0)
            {
                output.Write(buffer, 0, bytesread);
            }
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            Model.Reset();
        }
    }
}
