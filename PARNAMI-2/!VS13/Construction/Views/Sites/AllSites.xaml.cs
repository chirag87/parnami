﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
//using SIMS.MM.MODEL;

namespace Contruction
{
    public partial class AllSites : UserControl
    {
        public AllSites()
        {
            InitializeComponent();
        }

        private void btnNew_Click(object sender, RoutedEventArgs e)
        {
            SiteDetailedView view = new SiteDetailedView(null);
            Base.Redirect(view);
        }

        private void btnExport_Click(object sender, RoutedEventArgs e)
        {
            dgdAllSites.Export();
        }

        private void tbxKey_TextChanged(object sender, TextChangedEventArgs e)
        {
            var box = sender as TextBox;
            Base.Current.SearchSite = box.Text;
        }

        private void tbxKey_KeyUp(object sender, KeyEventArgs e)
        {
            var box = sender as TextBox;
            Base.Current.SearchSite = box.Text;
        }

        private void Image_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            var img = sender as Image;
            var data = (MSite)img.DataContext;
            if (data == null) return;
            SiteDetailedView view = new SiteDetailedView(data.ID);
            view.id = data.ID;
            Base.Redirect(view);
        }

        private void HyperlinkButton_Click(object sender, RoutedEventArgs e)
        {
            var img = sender as HyperlinkButton;
            var data = (MSite)img.DataContext;
            if (data == null) return;
            SiteDetailedView view = new SiteDetailedView(data.ID);
            view.id = data.ID;
            Base.Redirect(view);
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            tbxKey.Focus();
        }
    }
}