﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using SIMS.Core;
//using SIMS.MM.MODEL;

namespace Contruction
{
    public partial class MOReleaseIndex : UserControl
    {
        public MOReleaseIndexVM Model
        {
            get { return (MOReleaseIndexVM)this.DataContext; }
        }
				
        public MOReleaseIndex()
        {
            InitializeComponent();
            Model.MOReleaseChanged += new EventHandler(Model_MOReleaseChanged);
        }

        void Model_MOReleaseChanged(object sender, EventArgs e)
        {
            myPager.PagedData = (IPagination)Model.MOReleases;
        }

        private void myPager_MoveToPage(object sender, Controls.MyPageChangedEventArgs e)
        {
            Model.PageIndex = e.RequestedPage;
        }

        private void btnReceive_Click(object sender, RoutedEventArgs e)
        {
            var btn = sender as HyperlinkButton;
            var line = (MMOReleaseHeader)btn.DataContext;
            if (line == null) return;

            MOReceive mor = new MOReceive(line.MOID, line.ID, true);
            mor.Show();
        }

        private void Image_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            var img = (Image)sender;
            var morheader = (MMOReleaseHeader)img.DataContext;
            if (morheader == null) return;

            MOReleaseDetailedView mor = new MOReleaseDetailedView(morheader.MOID, morheader.ID);
            mor.Show();
        }

        private void btnClear_Click(object sender, RoutedEventArgs e)
        {
            Model.Clear();
        }
    }
}
