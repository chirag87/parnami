﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace Contruction
{
    public partial class MORelease : ChildWindow
    {
        public MOReleaseModel Model
        {
            get { return (MOReleaseModel)this.DataContext; }
        }

        public MORelease(int moid)
        {
            InitializeComponent();
            Model.GetReceivingforMORelease(moid);
            Model.Released += new EventHandler(Model_Released);
        }

        void Model_Released(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

        private void textBlock_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {

        }

        private void btnRelease_Click(object sender, RoutedEventArgs e)
        {

        }

        private void ChildWindow_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Escape)
            {
                this.DialogResult = false;
            }
        }
		
		private void AutoFillReleasingQty()
		{
			
		}

        private void btnAddNewTransporter_Click(object sender, RoutedEventArgs e)
        {
            NewTransporter trans = new NewTransporter();
            trans.Show();
        }

        private void ChildWindow_Loaded(object sender, RoutedEventArgs e)
        {
            
        }
    }
}

