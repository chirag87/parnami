﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using SIMS.MM.MODEL;
using SIMS.Core;

namespace Contruction
{
    public partial class Inbox : UserControl
    {
        public Inbox()
        {
            InitializeComponent();
            Base.Current.LoadAllMessages();
            Base.Current.MessegesDataChanged += new EventHandler(Current_MessegesDataChanged);
        }

        void Current_MessegesDataChanged(object sender, EventArgs e)
        {
            myPager.PagedData = (IPagination)Base.Current.AllMessages;
        }

        private void btnNewMail_Click(object sender, RoutedEventArgs e)
        {
            Base.Redirect("NewMessage");
        }

        private void messageList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var list = sender as ListBox;
            var data = (MMessaging)list.SelectedItem;

            if (data != null)
            {
                Base.Current.SelectedMessage = data;
                data.IsRead = true;
                Base.Current.IsReadMsg = true;
                Base.Current.SetIsRead(data.ID);
            }
        }

        private void myPager_MoveToPage(object sender, Controls.MyPageChangedEventArgs e)
        {
            Base.Current.PageIndex = e.RequestedPage;
        }

        private void hpbtnClear_Click(object sender, RoutedEventArgs e)
        {
            searchBox.Text = "";
            Base.Current.LoadAllMessages();
        }

        private void Stars_Checked(object sender, RoutedEventArgs e)
        {
            var chk = sender as CheckBox;
            var data = (MMessaging)chk.DataContext;
            Base.Current.IsStar = true;
            Base.Current.SetIsStar(data.ID);
        }

        private void Stars_Unchecked(object sender, RoutedEventArgs e)
        {
            var chk = sender as CheckBox;
            var data = (MMessaging)chk.DataContext;
            Base.Current.IsStar = false;
            Base.Current.SetIsStar(data.ID);
        }

        private void Flags_Checked(object sender, RoutedEventArgs e)
        {
            var chk = sender as CheckBox;
            var data = (MMessaging)chk.DataContext;
            Base.Current.IsFlag = true;
            Base.Current.SetIsFlag(data.ID);
        }

        private void Flags_Unchecked(object sender, RoutedEventArgs e)
        {
            var chk = sender as CheckBox;
            var data = (MMessaging)chk.DataContext;
            Base.Current.IsFlag = false;
            Base.Current.SetIsFlag(data.ID);
        }

        private void deleteImage_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            var chk = sender as Image;
            var data = (MMessaging)chk.DataContext;
            Base.Current.DeleteMail(data.ID);
        }



        private void starImageBW_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            var img = sender as Image;
            var data = (MMessaging)img.DataContext;
            Base.Current.IsStar = true;
            Base.Current.SetIsStar(data.ID);
        }
        private void starImageColor_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            var img = sender as Image;
            var data = (MMessaging)img.DataContext;
            Base.Current.IsStar = false;
            Base.Current.SetIsStar(data.ID);
        }
        private void flagImageBW_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            var img = sender as Image;
            var data = (MMessaging)img.DataContext;
            Base.Current.IsFlag = true;
            Base.Current.SetIsFlag(data.ID);
        }
        private void flagImageColor_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            var img = sender as Image;
            var data = (MMessaging)img.DataContext;
            Base.Current.IsFlag = false;
            Base.Current.SetIsFlag(data.ID);
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            searchBox.Focus();
        }
    }
}