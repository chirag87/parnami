﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace Contruction
{
    public partial class UserNames : ChildWindow
    {
        public UserNames()
        {
            InitializeComponent();
        }

        public void SetUsersList(IEnumerable<string> UsersList)
        {
            usersList.ItemsSource = UsersList;
        }

        public IEnumerable<string> SelectedUsers
        {
            get { return usersList.SelectedItems.Cast<string>(); }
        }

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
            RaiseSubmitted();
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
            RaiseCancelled();
        }

        #region ISubmitActions Members

        public event EventHandler Submitted;

        public event EventHandler Cancelled;

        public void RaiseSubmitted()
        {
            if (Submitted != null)
                Submitted(this, new EventArgs());
        }

        public void RaiseCancelled()
        {
            if (Cancelled != null)
                Cancelled(this, new EventArgs());
        }

        #endregion
    }
}

