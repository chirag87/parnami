﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
//using SIMS.MM.MODEL;
using SIMS.Core;

namespace Contruction
{
    public partial class AllRTVs : UserControl
    {
        public ReturnToVendorVM  Model
        {
            get { return (ReturnToVendorVM )this.DataContext; }
        }
				
        public AllRTVs()
        {
            InitializeComponent();
            Model.RTVDataChanged += new EventHandler(Model_RTVDataChanged);
            Model.RTVUpdateCompleted += new EventHandler(Model_RTVUpdateCompleted);
            Model.RTVCreated += new EventHandler(Model_RTVCreated);
        }

        void Model_RTVCreated(object sender, EventArgs e)
        {
            Model.Load();
        }

        void Model_RTVUpdateCompleted(object sender, EventArgs e)
        {
            Model.Load();
        }

        void Model_RTVDataChanged(object sender, EventArgs e)
        {
            myPager.PagedData = (IPagination)Model.AllRTVs;
        }

        private void hpBtnClear_Click(object sender, RoutedEventArgs e)
        {
            Model.Clear();
            Model.Load();
        }

        private void btnNew_Click(object sender, RoutedEventArgs e)
        {
            Base.Redirect("ReturnToVendor");
        }

        private void dataGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var dg = sender as DataGrid;
            var data = (MReturnToVendor)dg.SelectedItem;
        }

        private void Img_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            var img = sender as Image;
            var line = (MReturnToVendor)img.DataContext;
            if (line == null) return;
            RTVDetailedView rtview = new RTVDetailedView(line.ID);
            rtview.Show();
        }

        private void myPager_MoveToPage(object sender, Controls.MyPageChangedEventArgs e)
        {
            Model.PageIndex = e.RequestedPage;
        }
    }
}
