﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
//using SIMS.MM.MODEL;

namespace Contruction
{
    public partial class ReturnToVendor : UserControl
    {
        public ReturnToVendorVM Model
        {
            get { return (ReturnToVendorVM)this.DataContext; }
        }

        public ReturnToVendor()
        {
            InitializeComponent();
            Model.RTVCreated += new EventHandler(Model_RTVCreated);
        }

        void Model_RTVCreated(object sender, EventArgs e)
        {
            Model.Reset();
            Reset();
            Model.Load();
            Base.Redirect("AllRTVs");
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            SiteCombo.Focus();
        }

        private void btnBack_Click(object sender, RoutedEventArgs e)
        {
            Base.Redirect("AllRTVs");
        }

        private void deleteLine_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            var btn = sender as TextBlock;
            var data = (MReturnToVendorLine)btn.DataContext;
            Model.DeleteLine(data.LineID);
        }

        private void btnReset_Click(object sender, RoutedEventArgs e)
        {
            Model.Reset();
            this.Reset();
        }

        private void btnConfirm_Click(object sender, RoutedEventArgs e)
        {
            Model.SubmitToDB();
        }

        public void Reset()
        {
            this.SiteCombo.SelectedItem = null;
            this.VendorCombo.SelectedItem = null;
        }

        private void InfoIcon_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            var img = sender as Image;
            var data = (MReturnToVendorLine)img.DataContext;
            if (data.ItemID == 0) return;
            var popup = new ItemSummary(data.ItemID, data.SizeID, data.BrandID);
            popup.CreatePopup();
        }

        #region CreatePopUP Members
        public bool IsPopUpMode
        {
            get { return MyPopUP != null; }
        }

        public PopUp MyPopUP { get; set; }

        public PopUp CreatePopup()
        {
            MyPopUP = this.ToPopup(false);
            MyPopUP.SetAutoLayout();
            MyPopUP.Title = "Return To Vendor Details";
            return MyPopUP;
        }
        #endregion
    }
}
