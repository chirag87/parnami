﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
//using SIMS.MM.MODEL;
using System.ComponentModel;
using System.IO;
using System.Windows.Browser;
using Contruction.MasterReportingService;

namespace Contruction
{
    public partial class ItemDetails : UserControl, INotifyPropertyChanged
    {
        MasterReportingServiceClient service = new MasterReportingServiceClient();

        public ItemDetails()
        {
            InitializeComponent();
        }

        public ItemDetails(int id)
            :this()
        {
            Base.Current.GetItemByID(id);
            Base.Current.GetDocumentsByForID(id, "Item");
            this.id = id;
            ForID = id;
            stocksummary.ItemID = id;
            ShowParents.itemid = id;
        }

        int _ForID;
        public int ForID
        {
            get { return _ForID; }
            set
            {
                _ForID = value;
                Notify("ForID");
            }
        }

        int _id = 0;
        public int id
        {
            get { return _id; }
            set
            {
                _id = value;
                Notify("id");
            }
        }

        MItem _SelectedItem = new MItem();
        public MItem SelectedItem
        {
            get { return _SelectedItem; }
            set
            {
                _SelectedItem = value;
                Notify("SelectedItem");
            }
        }

        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            Base.Current.UpdateItem();
        }

        private void btnBack_Click(object sender, RoutedEventArgs e)
        {
            Base.Redirect(new ViewItems());
        }

        private void btnAddNewDoc_Click(object sender, RoutedEventArgs e)
        {
            DocumentWindow doc = new DocumentWindow(id, "Item");
            doc.Show();
        }

        private void btnUploadFile_Click(object sender, RoutedEventArgs e)
        {
            FileUploader upload = new FileUploader(ForID);
            upload.Show();
        }

        private void btnClear_Click(object sender, RoutedEventArgs e)
        {
            Photo.Source = null;
        }

        private void delete_Click(object sender, RoutedEventArgs e)
        {
            var btn = sender as HyperlinkButton;
            var data = (MDocument)btn.DataContext;
            Base.Current.DeleteDocument(data);
            Base.Current.DocumentDeleted += new EventHandler(Current_DocumentDeleted);
        }

        void Current_DocumentDeleted(object sender, EventArgs e)
        {
            Base.Current.GetDocumentsByForID(id, "Item");
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void Notify(string propertyName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            tbxItemName.Focus();
        }
    }
}
