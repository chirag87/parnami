﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace Contruction
{
    public partial class ItemSummary : UserControl
    {
        public ItemSummaryVM Model
        {
            get { return (ItemSummaryVM)this.DataContext; }
        }

        public ItemSummary()
        {
            InitializeComponent();
        }

        public ItemSummary(int? itemid, int? sizeid, int? brandid)
            : this()
        {
            Model.itemid = itemid;
            Model.sizeid = sizeid;
            Model.brandid = brandid;
            if (itemid.HasValue && itemid != 0)
                item = Base.Current.Items.Single(x => x.ID == itemid).DisplayName;
            if (sizeid.HasValue && sizeid != 0)
                Size = Base.Current.ItemSizes.Single(x => x.ID == sizeid).Size;
            Model.Load();
        }

        string _item;
        public string item
        {
            get { return _item; }
            set
            {
                _item = value;
                Model.Notify("item");
            }
        }

        string _Size;
        public string Size
        {
            get { return _Size; }
            set
            {
                _Size = value;
                Model.Notify("Size");
            }
        }

        #region CreatePopUP Members
        public bool IsPopUpMode
        {
            get { return MyPopUP != null; }
        }

        public PopUp MyPopUP { get; set; }

        public PopUp CreatePopup()
        {
            MyPopUP = this.ToPopup(false);
            MyPopUP.SetAutoLayout();
            MyPopUP.Title = "Summary of " + item + " Size: " + Size;
            return MyPopUP;
        }
        #endregion
    }
}
