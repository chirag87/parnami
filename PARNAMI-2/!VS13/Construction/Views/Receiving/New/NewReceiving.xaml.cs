﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
//using SIMS.MM.MODEL;

namespace Contruction
{
    public partial class NewReceiving : UserControl
    {
        public NewReceivingModel GRNModel
        {
            get { return (NewReceivingModel)this.DataContext; }
        }
				
        public NewReceiving()
        {
            InitializeComponent();
        }

        private void textBlock_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            var textblock = sender as TextBlock;
            var line = (MReceivingLineView)textblock.DataContext;
            GRNModel.DeleteLine(line.LineID);
        }

        private void btnPrint_Click(object sender, RoutedEventArgs e)
        {
            var btn = sender as Button;
            var data = (MReceiving)btn.DataContext;
            PrintFactory.PrintReceiving(data);
        }

        private void hpbtnAddNewLine_Click(object sender, RoutedEventArgs e)
        {
            GRNModel.AddNewLine();
        }

        //private void CFTCalculator_ValueChanged(object sender, EventArgs e)
        //{
            //var data = sender as CFTCalculator;
            //var line = (MReceivingLineView)data.DataContext;
            //if (data.SetThroughMeasurement)
            //{
            //    line.Quantity = data.Value;
            //    line.TruckParameters = data.Measurements;
            //}
            //else
            //{
            //    line.Quantity = data.Value;
            //    line.TruckParameters = null;
            //}
        //}

        //private void cft_Loaded(object sender, RoutedEventArgs e)
        //{
        //    var data = sender as CFTCalculator;
        //    var line = (MReceivingLineView)data.DataContext;
        //    data.Value = line.Quantity;
        //}

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            VendorCombo.Focus();
        }

        private void InfoIcon_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            var img = sender as Image;
            var data = (MGRNLine)img.DataContext;
            if (data.ItemID == 0) return;
            var popup = new ItemSummary(data.ItemID, data.SizeID, data.BrandID);
            popup.CreatePopup();
        }

        private void btnReset_Click(object sender, RoutedEventArgs e)
        {
            GRNModel.Reset();
            this.ResetForm();
        }

        public void ResetForm()
        {
            this.SiteCombo.SelectedItem = null;
            this.VendorCombo.SelectedItem = null;
            this.status.Content = "";
            GRNModel.Status = "";
        }

        #region CreatePopUP Members
        public bool IsPopUpMode
        {
            get { return MyPopUP != null; }
        }

        public PopUp MyPopUP { get; set; }

        public PopUp CreatePopup()
        {
            MyPopUP = this.ToPopup(false);
            MyPopUP.SetAutoLayout();
            MyPopUP.Title = "New Receiving";
            return MyPopUP;
        }
        #endregion
    }
}
