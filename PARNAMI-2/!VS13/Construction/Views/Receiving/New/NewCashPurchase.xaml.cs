﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
//using SIMS.MM.MODEL;

namespace Contruction
{
    public partial class NewCashPurchase : UserControl
    {
        public NewCashPurchaseModel Model
        {
            get { return (NewCashPurchaseModel)this.DataContext; }
        }
				
        public NewCashPurchase()
        {
            InitializeComponent();
        }

        private void btnReset_Click(object sender, RoutedEventArgs e)
        {
            Model.Reset();
            this.ResetForm();
        }

        public void ResetForm()
        {
            this.status.Content = "";
            Model.Status = "";
            this.VendorCombo.SelectedItem = null;
            this.SiteCombo.SelectedItem = null;
        }

        private void textBlock_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            var textblock = sender as TextBlock;
            var line = (MCashPurchaseLine)textblock.DataContext;
            var id = line.LineID;
            Model.DeleteLine(id);
        }

        private void InfoIcon_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            var img = sender as Image;
            var data = (MCashPurchaseLine)img.DataContext;
            if (data.ItemID == 0) return;
            var popup = new ItemSummary(data.ItemID, data.SizeID, data.BrandID);
            popup.CreatePopup();
        }
    }
}
