﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
//using SIMS.MM.MODEL;
using Contruction.AppSettingService;

namespace Contruction
{
    public partial class Base
    {
        AppSettingServiceClient Appservice = new AppSettingServiceClient();

        MAppSettings _AppSettings;
		public MAppSettings AppSettings
		{
			get{return _AppSettings;}
			set{_AppSettings = value;
			Notify("AppSettings");
			}
		}

        public void OnInitForAppSettings()
        {
            Appservice.GetAllAppSettingsCompleted += new EventHandler<GetAllAppSettingsCompletedEventArgs>(Appservice_GetAllAppSettingsCompleted);
            Appservice.GetAllAppSettingsAsync();
        }

        void Appservice_GetAllAppSettingsCompleted(object sender, GetAllAppSettingsCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                Base.ShowError(e.Error);
                IsBusy = false;
            }
            else
            {
                AppSettings = e.Result;
                IsBusy = false;
            }
        }

        public void Load()
        {
            Appservice.GetAllAppSettingsAsync();
            IsBusy = true;
        }
    }
}
