﻿using System;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Collections.Generic;
using Contruction.Web;
using System.Collections.ObjectModel;
//using SIMS.MM.MODEL;

namespace Contruction
{
    public partial class Base : ViewModel
    {
        public string CurrentUserName { get; set; }

        public User User
        {
            get { return null; }
            //get { return WebContext.Current.User; }
        }

        public bool IsAuthenticated
        {
            get { return IsLoggedIn; }
        }

        public void NotifyOnLogin()
        {
            string[] keys = new string[] { "IsAuthenticated", "IsLoggedIn" };
            Notify("IsLoggedIn");
            Notify("IsAuthenticated");
        }

        //public bool CanEmailPO
        //{
        //    get
        //    {
        //        return CanApprovePO;
        //    }
        //}

        //public bool CanApprovePO
        //{
        //    get { return IsInAnyRole(new string[] { "dba", "PurchaseAdmin", "POApprove" }); }
        //}

        //public bool CanApproveMI
        //{
        //    get { return IsInAnyRole(new string[] { "dba", "PurchaseAdmin", "MIApprove" }); }
        //}

        //public bool CanVerifyMI
        //{
        //    get { return IsInAnyRole(new string[] { "dba", "PurchaseAdmin", "MIVerify" }); }
        //}

        //public bool CanRaisePO
        //{
        //    get { return IsInAnyRole(new string[] { "dba", "admin", "PurchaseAdmin" }); }
        //}

        //public bool CanRaiseTO
        //{
        //    get { return IsInAnyRole(new string[] { "dba", "admin", "Store" }); }
        //}

        //public bool CanManageTO
        //{
        //    get { return IsInAnyRole(new string[] { "dba", "admin", "StoreAdmin" }); }
        //}

        //public bool CanManageUsers
        //{
        //    get
        //    {
        //        return IsInAnyRole(new string[] { "dba", "HR" }) || UserName == "dba";
        //    }
        //}

        //public bool CanViewAllSites
        //{
        //    get
        //    {
        //        return IsInAnyRole(new string[] { "dba", "AllSites" });
        //    }
        //}

        //public bool CanAdmin
        //{
        //    get
        //    {
        //        return IsInAnyRole(new string[] { "dba", "admin" });
        //    }
        //}

        List<int> _MySiteIDs = new List<int>();
        public List<int> MySiteIDs
        {
            get { return _MySiteIDs; }
            set
            {
                _MySiteIDs = value;
                SetMySites();
                Notify("MySiteIDs");
                Notify("MySites");
            }
        }

        void SetMySites()
        {
            MySites = new ObservableCollection<MSite>();
            Sites.Where(x => MySiteIDs.Contains(x.ID)).ToList().ForEach(x => MySites.Add(x));
        }
    }
}
