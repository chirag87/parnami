﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace Contruction
{
    public partial class Base : ViewModel
    {
        public void OnCreatedForMTR()
        {
        }

        void MTRService_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "IsLoading" || e.PropertyName == "IsSubmitting")
                Notify("IsMTRServiceBusy");
        }
        public bool IsMTRServiceBusy
        {
            get { return false; }
        }

        public void InitForMTRs()
        {
        }

        public void UpdateMTR(int mtrid)
        {
        }

        public event EventHandler MTRApproved;
        public void RaiseMTRApproved()
        {
            if (MTRApproved != null)
                MTRApproved(this, new EventArgs());
        }
    }
}
