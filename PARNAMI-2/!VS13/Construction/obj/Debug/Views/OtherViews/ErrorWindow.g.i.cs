﻿#pragma checksum "D:\Softwares\Office Works\PARNAMI\PARNAMI-2\Construction\Views\OtherViews\ErrorWindow.xaml" "{406ea660-64cf-4c82-b6f0-42d48172a799}" "DCB11D8A3691F44FB69A622B50F2B0E1"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.18408
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Automation.Peers;
using System.Windows.Automation.Provider;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Interop;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Resources;
using System.Windows.Shapes;
using System.Windows.Threading;


namespace Contruction {
    
    
    public partial class ErrorWindow : System.Windows.Controls.ChildWindow {
        
        internal System.Windows.Controls.Grid LayoutRoot;
        
        internal System.Windows.VisualStateGroup ErrorDetails;
        
        internal System.Windows.VisualState ShowErrorDetails;
        
        internal System.Windows.VisualState HideErrorDetails;
        
        internal System.Windows.Controls.TextBlock tblkIntroductoryText;
        
        internal System.Windows.Controls.Button btnShowDetails;
        
        internal System.Windows.Controls.Button btnHideDetails;
        
        internal System.Windows.Controls.Border border;
        
        internal System.Windows.Controls.StackPanel stackPanel;
        
        internal System.Windows.Controls.TextBlock LabelText;
        
        internal System.Windows.Controls.TextBox ErrorTextBox;
        
        internal System.Windows.Controls.Button OKButton;
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Windows.Application.LoadComponent(this, new System.Uri("/Construction;component/Views/OtherViews/ErrorWindow.xaml", System.UriKind.Relative));
            this.LayoutRoot = ((System.Windows.Controls.Grid)(this.FindName("LayoutRoot")));
            this.ErrorDetails = ((System.Windows.VisualStateGroup)(this.FindName("ErrorDetails")));
            this.ShowErrorDetails = ((System.Windows.VisualState)(this.FindName("ShowErrorDetails")));
            this.HideErrorDetails = ((System.Windows.VisualState)(this.FindName("HideErrorDetails")));
            this.tblkIntroductoryText = ((System.Windows.Controls.TextBlock)(this.FindName("tblkIntroductoryText")));
            this.btnShowDetails = ((System.Windows.Controls.Button)(this.FindName("btnShowDetails")));
            this.btnHideDetails = ((System.Windows.Controls.Button)(this.FindName("btnHideDetails")));
            this.border = ((System.Windows.Controls.Border)(this.FindName("border")));
            this.stackPanel = ((System.Windows.Controls.StackPanel)(this.FindName("stackPanel")));
            this.LabelText = ((System.Windows.Controls.TextBlock)(this.FindName("LabelText")));
            this.ErrorTextBox = ((System.Windows.Controls.TextBox)(this.FindName("ErrorTextBox")));
            this.OKButton = ((System.Windows.Controls.Button)(this.FindName("OKButton")));
        }
    }
}

