﻿#pragma checksum "D:\Softwares\Office Works\PARNAMI\PARNAMI-2\Construction\Views\Document\DocumentWindow.xaml" "{406ea660-64cf-4c82-b6f0-42d48172a799}" "8E08C95297E0251F98CE1B4153AFE465"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.18408
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using DC.FileUpload;
using System;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Automation.Peers;
using System.Windows.Automation.Provider;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Interop;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Resources;
using System.Windows.Shapes;
using System.Windows.Threading;


namespace Contruction {
    
    
    public partial class DocumentWindow : System.Windows.Controls.ChildWindow {
        
        internal System.Windows.Controls.Grid LayoutRoot;
        
        internal System.Windows.Controls.ComboBox comboDocumentType;
        
        internal System.Windows.Controls.Button btnBrowse;
        
        internal System.Windows.Controls.ListBox doclist;
        
        internal DC.FileUpload.FileUploadControl uploadControl;
        
        internal System.Windows.Controls.Button btnSave;
        
        internal System.Windows.Controls.Button CancelButton1;
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Windows.Application.LoadComponent(this, new System.Uri("/Construction;component/Views/Document/DocumentWindow.xaml", System.UriKind.Relative));
            this.LayoutRoot = ((System.Windows.Controls.Grid)(this.FindName("LayoutRoot")));
            this.comboDocumentType = ((System.Windows.Controls.ComboBox)(this.FindName("comboDocumentType")));
            this.btnBrowse = ((System.Windows.Controls.Button)(this.FindName("btnBrowse")));
            this.doclist = ((System.Windows.Controls.ListBox)(this.FindName("doclist")));
            this.uploadControl = ((DC.FileUpload.FileUploadControl)(this.FindName("uploadControl")));
            this.btnSave = ((System.Windows.Controls.Button)(this.FindName("btnSave")));
            this.CancelButton1 = ((System.Windows.Controls.Button)(this.FindName("CancelButton1")));
        }
    }
}

