﻿#pragma checksum "D:\Softwares\Office Works\PARNAMI\PARNAMI-2\Construction\Views\Reports\Cash_Report.xaml" "{406ea660-64cf-4c82-b6f0-42d48172a799}" "01DE4CB55A2BC0816850D359B6BBFF85"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.18408
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Automation.Peers;
using System.Windows.Automation.Provider;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Interop;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Resources;
using System.Windows.Shapes;
using System.Windows.Threading;


namespace Contruction {
    
    
    public partial class Cash_Report : System.Windows.Controls.UserControl {
        
        internal System.Windows.Controls.Grid LayoutRoot;
        
        internal DatePicker dtpdate1;
        
        internal DatePicker dtpdate2;
        
        internal System.Windows.Controls.TextBlock txt_Status;
        
        internal System.Windows.Controls.TextBox txtkey;
        
        internal AutoCompleteBox cbx_Item;
        
        internal System.Windows.Controls.CheckBox ChkGrpSites;
        
        internal AutoCompleteBox cbx_Vendor;
        
        internal AutoCompleteBox cbxSites;
        
        internal DataGrid datagrid;
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Windows.Application.LoadComponent(this, new System.Uri("/Construction;component/Views/Reports/Cash_Report.xaml", System.UriKind.Relative));
            this.LayoutRoot = ((System.Windows.Controls.Grid)(this.FindName("LayoutRoot")));
            this.dtpdate1 = ((DatePicker)(this.FindName("dtpdate1")));
            this.dtpdate2 = ((DatePicker)(this.FindName("dtpdate2")));
            this.txt_Status = ((System.Windows.Controls.TextBlock)(this.FindName("txt_Status")));
            this.txtkey = ((System.Windows.Controls.TextBox)(this.FindName("txtkey")));
            this.cbx_Item = ((AutoCompleteBox)(this.FindName("cbx_Item")));
            this.ChkGrpSites = ((System.Windows.Controls.CheckBox)(this.FindName("ChkGrpSites")));
            this.cbx_Vendor = ((AutoCompleteBox)(this.FindName("cbx_Vendor")));
            this.cbxSites = ((AutoCompleteBox)(this.FindName("cbxSites")));
            this.datagrid = ((DataGrid)(this.FindName("datagrid")));
        }
    }
}

