﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace SIMS.MM.MODEL
{
    [DataContract]
    public class MSiteHeader
    {
        [DataMember]
        public int ID { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public string TypeID { get; set; }

        [DataMember]
        public string Address { get; set; }

        [DataMember]
        public string City { get; set; }

        [DataMember]
        public string State { get; set; }

        [DataMember]
        public string Country { get; set; }

        [DataMember]
        public string PinCode { get; set; }

        [DataMember]
        public string Phone { get; set; }

        [DataMember]
        public string ContactPerson { get; set; }

        [DataMember]
        public string KeyContactMobile1 { get; set; }

        [DataMember]
        public string KeyContactEmail { get; set; }
    }
}
