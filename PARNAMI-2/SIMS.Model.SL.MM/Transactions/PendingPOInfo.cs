﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Runtime.Serialization;

namespace SIMS.MM.MODEL
{
    [DataContract]
    public class PendingPOInfo : EntityModel
    {

        public PendingPOInfo()
        {
            IsBlank = false;
        }

        string _RefNo;
        [DataMember]
        public string RefNo
        {
            get { return _RefNo; }
            set
            {
                _RefNo = value;
                Notify("RefNo");
            }
        }

        int _POID;
        [DataMember]
        public int POID
        {
            get { return _POID; }
            set
            {
                _POID = value;
                Notify("POID");
            }
        }

        int _PRID;
        [DataMember]
        public int PRID
        {
            get { return _PRID; }
            set
            {
                _PRID = value;
                Notify("PRID");
            }
        }

        int _PRLineID;
        [DataMember]
        public int PRLineID
        {
            get { return _PRLineID; }
            set
            {
                _PRLineID = value;
                Notify("PRLineID");
            }
        }

        double _OrderedQty;
        [DataMember]
        public double OrderedQty
        {
            get { return _OrderedQty; }
            set
            {
                _OrderedQty = value;
                Notify("OrderedQty");
            }
        }

        double _EarlierReceivedQty;
        [DataMember]
        public double EarlierReceivedQty
        {
            get { return _EarlierReceivedQty; }
            set
            {
                _EarlierReceivedQty = value;
                Notify("EarlierReceivedQty");
            }
        }

        double _UnitPrice;
        [DataMember]
        public double UnitPrice
        {
            get { return _UnitPrice; }
            set
            {
                _UnitPrice = value;
                Notify("UnitPrice");
            }
        }

        double _LinePrice;
        [DataMember]
        public double LinePrice
        {
            get { return _LinePrice; }
            set
            {
                _LinePrice = value;
                Notify("LinePrice");
            }
        }

        int _ItemID;
        [DataMember]
        public int ItemID
        {
            get { return _ItemID; }
            set
            {
                _ItemID = value;
                Notify("ItemID");
            }
        }

        int? _SizeID;
        [DataMember]
        public int? SizeID
        {
            get { return _SizeID; }
            set
            {
                _SizeID = value;
                Notify("SizeID");
            }
        }

        int? _BrandID;
        [DataMember]
        public int? BrandID
        {
            get { return _BrandID; }
            set
            {
                _BrandID = value;
                Notify("BrandID");
            }
        }

        string _ConsumableType;
        [DataMember]
        public string ConsumableType
        {
            get { return _ConsumableType; }
            set
            {
                _ConsumableType = value;
                Notify("ConsumableType");
            }
        }

        [DataMember]
        public string Display
        {
            get
            {
                if (POID != 0 && PRLineID != 0)
                    return "PO: " + RefNo + " (ID: " + POID + ", LineID: " + PRLineID + ")";
                //return "PO: " + POID + ", LineID: " + PRLineID;
                else
                    return "";
            }
            set { }
        }

        [DataMember]
        public bool IsBlank { get; set; }
    }
}
