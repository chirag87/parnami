﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace SIMS.MM.MODEL
{
    [DataContract]
    public class MStockBreakUp : EntityModel
    {
        [DataMember]
        public int? ItemID { get; set; }

        [DataMember]
        public string Item { get; set; }

        [DataMember]
        public int? ItemCatID { get; set; }

        [DataMember]
        public int? SizeID { get; set; }

        [DataMember]
        public string Size { get; set; }

        [DataMember]
        public int? BrandID { get; set; }

        [DataMember]
        public string Brand { get; set; }

        [DataMember]
        public int? SiteID { get; set; }

        [DataMember]
        public string Site { get; set; }

        [DataMember]
        public string ConsumableType { get; set; }

        [DataMember]
        public double ToBeReceivedQty { get; set; }

        [DataMember]
        public double ReceivedQty { get; set; }

        [DataMember]
        public double ReleasedQty { get; set; }

        [DataMember]
        public double MRNQty { get; set; }

        [DataMember]
        public double CPQty { get; set; }

        [DataMember]
        public double ConsumedQty { get; set; }

        [DataMember]
        public double TotalIn { get { return ReceivedQty + MRNQty + CPQty; } set { } }

        [DataMember]
        public double TotalOut { get { return ReleasedQty + ConsumedQty; } set { } }

        [DataMember]
        public double NetIn { get { return TotalIn - TotalOut; } set { } }

        [DataMember]
        public int LinesCount { get; set; }
    }

    [DataContract]
    public class MDailyStockBreakUp : EntityModel
    {
        [DataMember]
        public int? ItemID { get; set; }

        [DataMember]
        public string Item { get; set; }

        [DataMember]
        public int? SizeID { get; set; }

        [DataMember]
        public string Size { get; set; }

        [DataMember]
        public int? BrandID { get; set; }

        [DataMember]
        public string Brand { get; set; }

        [DataMember]
        public int? SiteID { get; set; }

        [DataMember]
        public string Site { get; set; }

        [DataMember]
        public string ConsumableType { get; set; }

        [DataMember]
        public double ToBeReceivedQty { get; set; }

        [DataMember]
        public double ReceivedQty { get; set; }

        [DataMember]
        public double ReleasedQty { get; set; }

        [DataMember]
        public double MRNQty { get; set; }

        [DataMember]
        public double CPQty { get; set; }

        [DataMember]
        public int? MOID { get; set; }

        [DataMember]
        public int? MOLineID { get; set; }

        [DataMember]
        public string MORefNo { get; set; }

        [DataMember]
        public int? RecMOID { get; set; }

        [DataMember]
        public int? RecMOLineID { get; set; }

        [DataMember]
        public string RecMORefNo { get; set; }

        [DataMember]
        public int? RelMOID { get; set; }

        [DataMember]
        public int? RelMOLineID { get; set; }

        [DataMember]
        public string RelMORefNo { get; set; }

        [DataMember]
        public int? MRNID { get; set; }

        [DataMember]
        public int? MRNLineID { get; set; }

        [DataMember]
        public string MRNRefNo { get; set; }

        [DataMember]
        public int? CPID { get; set; }

        [DataMember]
        public int? CPLineID { get; set; }

        [DataMember]
        public string CPRefNo { get; set; }

        [DataMember]
        public string MOInfo
        {
            get
            {
                if (ToBeReceivedQty != null && ToBeReceivedQty > 0)
                    return "ReferenceNo : " + MORefNo + "\nMOID : " + MOID + "\nMOLineID : " + MOLineID + "\nQunatity : " + ToBeReceivedQty;
                else
                    return "N/A";
            }
            set { }
        }

        [DataMember]
        public string MORecInfo
        {
            get
            {
                if (ReceivedQty != null && ReceivedQty > 0)
                    return "ReferenceNo : " + RecMORefNo + "\nMOID : " + RecMOID + "\nMOLineID : " + RecMOLineID + "\nQunatity : " + ReceivedQty;
                else
                    return "N/A";
            }
            set { }
        }

        [DataMember]
        public string MORelInfo
        {
            get
            {
                if (ReleasedQty != null && ReleasedQty > 0)
                    return "ReferenceNo : " + RelMORefNo + "\nMOID : " + RelMOID + "\nMOLineID : " + RelMOLineID + "\nQunatity : " + ReleasedQty;
                else
                    return "N/A";
            }
            set { }
        }

        [DataMember]
        public string MRNInfo
        {
            get
            {
                if (MRNQty != null && MRNQty > 0)
                    return "ReferenceNo : " + MRNRefNo + "\nMRNID : " + MRNID + "\nMRNLineID : " + MRNLineID + "\nQunatity : " + MRNQty;
                else
                    return "N/A";
            }
            set { }
        }

        [DataMember]
        public string CPInfo
        {
            get
            {
                if (CPQty != null && CPQty > 0)
                    return "ReferenceNo : " + CPRefNo + "\nCPID : " + CPID + "\nCPLineID : " + CPLineID + "\nQunatity : " + CPQty;
                else
                    return "N/A";
            }
            set { }
        }
    }
}
