﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace SIMS.MM.MODEL
{
    [DataContract]
    public class MClient : EntityModel
    {
        [DataMember]
        public int ID { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public string FullAddress
        {
            get 
            {
                return Address1 + ", " + Address2 + ", " + City + " ( " + State + " )" + "  " + Country + " - " + Pincode;
            }
            set { }
        }

        [DataMember]
        public string Address1 { get; set; }

        [DataMember]
        public string Address2 { get; set; }

        [DataMember]
        public string City { get; set; }

        [DataMember]
        public string State { get; set; }

        [DataMember]
        public string Country { get; set; }

        [DataMember]
        public string Pincode { get; set; }

        [DataMember]
        public string TINNo { get; set; }

        [DataMember]
        public string ServiceTaxNo { get; set; }

        [DataMember]
        public string PANNo { get; set; }

        [DataMember]
        public string Phone { get; set; }

        [DataMember]
        public string Fax { get; set; }

        [DataMember]
        public string Email { get; set; }

        [DataMember]
        public string Website { get; set; }

        [DataMember]
        public string ContactPerson { get; set; }

        [DataMember]
        public string ContactMobile1 { get; set; }

        [DataMember]
        public string ContactMobile2 { get; set; }

        [DataMember]
        public string ContactEmail { get; set; }
    }
}
