﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace SIMS.MM.MODEL
{
    [DataContract]
    public class MBillingVerified : EntityModel
    {
        public MBillingVerified()
        {
            VerifiedOn = DateTime.UtcNow.AddHours(5.5);
        }

        [DataMember]
        public int SrNo { get; set; }

        [DataMember]
        public int ID { get; set; }

        [DataMember]
        public int BillingID { get; set; }

        [DataMember]
        public int VerificationCounter { get; set; }

        [DataMember]
        public string VerifiedBy { get; set; }

        //[DataMember]
        //public DateTime? VerifiedOn { get; set; }
        DateTime? _VerifiedOn;
        [DataMember]
        public DateTime? VerifiedOn
        {
            get
            {
                if (_VerifiedOn < DateTime.UtcNow.AddHours(5.5))
                    return _VerifiedOn;
                else
                    return DateTime.UtcNow.AddHours(5.5);
            }
            set
            {
                _VerifiedOn = value;
                Notify("VerifiedOn");
            }
        }
    }
}