﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace SIMS.MM.MODEL
{
    [DataContract]
    public class MPRtoMOConvertion
    {
        [DataMember]
        public int ID { get; set; }

        [DataMember]
        public int? MOID { get; set; }

        [DataMember]
        public int? MOLineID { get; set; }

        [DataMember]
        public int PRID { get; set; }

        [DataMember]
        public int PRLineID { get; set; }

        [DataMember]
        public double RaisedQty { get; set; }

        [DataMember]
        public double SplittedQty { get; set; }

        [DataMember]
        public string MORefNo { get; set; }

        [DataMember]
        public string PRRefNo { get; set; }

        [DataMember]
        public DateTime PRRaisedOn { get; set; }

        [DataMember]
        public DateTime MOSplittedOn { get; set; }
    }
}
