﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Collections.ObjectModel;

namespace SIMS.MM.MODEL
{
    [DataContract]
    public class MMR : EntityModel
    {
        public MMR()
        {
            RaisedOn = DateTime.UtcNow.AddHours(5.5);
            IssuedOn = DateTime.UtcNow.AddHours(5.5);
            ReturnedOn = DateTime.UtcNow.AddHours(5.5);
            ApprovedOn = DateTime.UtcNow.AddHours(5.5);
        }

        [DataMember]
        public int ID { get; set; }

        [DataMember]
        public string ReferenceNo { get; set; }

        DateTime _RaisedOn;
        [DataMember]
        public DateTime RaisedOn
        {
            get
            {
                if (_RaisedOn < DateTime.UtcNow.AddHours(5.5))
                    return _RaisedOn;
                else
                    return DateTime.UtcNow.AddHours(5.5);
            }
            set
            {
                _RaisedOn = value;
                Notify("RaisedOn");
            }
        }

        [DataMember]
        public string RaisedBy { get; set; }

        [DataMember]
        public int SiteID { get; set; }

        [DataMember]
        [Display(AutoGenerateField = false)]
        public string Site { get; set; }

        [DataMember]
        [Display(Name = "Site")]
        public string DisplaySite { get; set; }

        [DataMember]
        public string CurrentOwnerID
        {
            get { return "DBA"; }
            set { }
        }

        [DataMember]
        public string ContactPerson { get; set; }

        [DataMember]
        public string ContactPhone { get; set; }

        [DataMember]
        public string RequestedBy { get; set; }

        [DataMember]
        public string Remarks { get; set; }

        [DataMember]
        public bool IsApproved { get; set; }

        DateTime _ApprovedOn;
        [DataMember]
        public DateTime ApprovedOn
        {
            get
            {
                if (_ApprovedOn < DateTime.UtcNow.AddHours(5.5))
                    return _ApprovedOn;
                else
                    return DateTime.UtcNow.AddHours(5.5);
            }
            set
            {
                _ApprovedOn = value;
                Notify("ApprovedOn");
            }
        }

        [DataMember]
        public string ApprovedBy { get; set; }

        [DataMember]
        public string Status { get; set; }

        [DataMember]
        public string ApprovalStatus { get; set; }

        [DataMember]
        public bool IsIssued { get; set; }

        DateTime? _IssuedOn;
        [DataMember]
        public DateTime? IssuedOn
        {
            get
            {
                if (_IssuedOn < DateTime.UtcNow.AddHours(5.5))
                    return _IssuedOn;
                else
                    return DateTime.UtcNow.AddHours(5.5);
            }
            set
            {
                _IssuedOn = value;
                Notify("IssuedOn");
            }
        }

        [DataMember]
        public string IssuedBy { get; set; }

        [DataMember]
        public bool PendingIssue { get; set; }

        [DataMember]
        public bool IsReturned { get; set; }

        DateTime? _ReturnedOn;
        [DataMember]
        public DateTime? ReturnedOn
        {
            get
            {
                if (_ReturnedOn < DateTime.UtcNow.AddHours(5.5))
                    return _ReturnedOn;
                else
                    return DateTime.UtcNow.AddHours(5.5);
            }
            set
            {
                _ReturnedOn = value;
                Notify("ReturnedOn");
            }
        }

        [DataMember]
        public string ReturnReceivedBy { get; set; }
        
        [DataMember]
        public bool PendingReturn { get; set; }

        [DataMember]
        public ObservableCollection<MMRLine> MRLines { get; set;}

        public MMRLine AddNewLine()
        {
            if (this.MRLines == null)
                this.MRLines = new ObservableCollection<MMRLine>();
            var line = new MMRLine();
            this.MRLines.Add(line);
            this.UpdateLineNumbers();
            return line;
        }

        public int UpdateLineNumbers()
        {
            if (this.MRLines == null) return 0;
            int i = 1;
            foreach (var item in this.MRLines)
            {
                item.LineID = i;
                i++;
            }
            return i - 1;
        }
    }
}
