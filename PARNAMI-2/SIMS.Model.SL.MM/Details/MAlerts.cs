﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace SIMS.MM.MODEL 
{
    [DataContract]
    public class MAlerts : EntityModel
    {
        [DataMember]
        public string Title { get; set; }

        [DataMember]
        public string Description { get; set; }

        [DataMember]
        public string Icon { get; set; }

        [DataMember]
        public string AlertType { get; set; }
    }
}
