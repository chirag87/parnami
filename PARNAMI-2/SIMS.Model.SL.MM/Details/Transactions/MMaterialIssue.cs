﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;

namespace SIMS.MM.MODEL
{
    [DataContract]
    public class MMaterialIssue : EntityModel
    {
        public MMaterialIssue()
        {
            LastUpdatedOn = DateTime.UtcNow.AddHours(5.5);
            IssuedOn = DateTime.UtcNow.AddHours(5.5);
        }

        #region Events

        public event EventHandler SiteIDChanged;
        public void RaiseSiteIDChanged()
        {
            if (SiteIDChanged != null)
                SiteIDChanged(this, new EventArgs());
        }

        #endregion

        #region Model

        [DataMember]
        public int ID { get; set; }

        [DataMember]
        public string ReferenceNo { get; set; }

        public int _SiteID = new int();
        [DataMember]
        public int SiteID
        {
            get { return _SiteID; }
            set
            {
                _SiteID = value;
                Notify("SiteID");
                RaiseSiteIDChanged();
            }
        }

        [DataMember]
        public string Site { get; set; }

        [DataMember]
        public string Requester { get; set; }

        [DataMember]
        public string Remarks { get; set; }

        [DataMember]
        public int? TranporterID { get; set; }

        [DataMember]
        public string Transporter { get; set; }

        DateTime _IssuedOn;
        [DataMember]
        public DateTime IssuedOn
        {
            get
            {
                if (_IssuedOn < DateTime.UtcNow.AddHours(5.5))
                    return _IssuedOn;
                else
                    return DateTime.UtcNow.AddHours(5.5);
            }
            set
            {
                _IssuedOn = value;
                Notify("IssuedOn");
            }
        }

        [DataMember]
        public string IssuedBy { get; set; }

        DateTime _LastUpdatedOn;
        [DataMember]
        public DateTime LastUpdatedOn
        {
            get
            {
                if (_LastUpdatedOn < DateTime.UtcNow.AddHours(5.5))
                    return _LastUpdatedOn;
                else
                    return DateTime.UtcNow.AddHours(5.5);
            }
            set
            {
                _LastUpdatedOn = value;
                Notify("LastUpdatedOn");
            }
        }

        [DataMember]
        public string LastUpdatedBy { get; set; }

        ObservableCollection<MMaterialIssueLine> _MaterialIssueLines = new ObservableCollection<MMaterialIssueLine>();
        [DataMember]
        public ObservableCollection<MMaterialIssueLine> MaterialIssueLines
        {
            get { return _MaterialIssueLines; }
            set
            {
                _MaterialIssueLines = value;
                Notify("MaterialIssueLines");
            }
        }

        #endregion

        #region Methods

        public MMaterialIssueLine AddNewLine()
        {
            if (this.MaterialIssueLines == null)
                this.MaterialIssueLines = new ObservableCollection<MMaterialIssueLine>();
            var line = new MMaterialIssueLine();
            this.MaterialIssueLines.Add(line);
            this.UpdateLineNumbers();
            return line;
        }

        public int UpdateLineNumbers()
        {
            if (this.MaterialIssueLines == null) return 0;
            int i = 1;
            foreach (var item in this.MaterialIssueLines)
            {
                item.LineID = i;
                i++;
            }
            return i - 1;
        }

        #endregion
    }

    [DataContract]
    public class MMaterialIssueLine : EntityModel
    {
        #region Events...

        public event EventHandler ItemIDChanged;
        public void RaiseItemIDChanged()
        {
            if (ItemIDChanged != null)
                ItemIDChanged(this, new EventArgs());
        }

        #endregion

        #region Model

        [DataMember]
        public int HeaderID { get; set; }

        int _LineID;
        [DataMember]
        public int LineID
        {
            get { return _LineID; }
            set
            {
                _LineID = value;
                Notify("LineID");
            }
        }

        int _itemID { get; set; }
        [DataMember]
        public int ItemID
        {
            get { return _itemID; }
            set
            {
                _itemID = value;
                Notify("ItemID");
                RaiseItemIDChanged();
            }
        }

        [DataMember]
        public string Item { get; set; }

        int _SizeID;
        [DataMember]
        public int SizeID
        {
            get { return _SizeID; }
            set
            {
                _SizeID = value;
                Notify("SizeID");
            }
        }

        [DataMember]
        public string Size { get; set; }

        int _BrandID;
        [DataMember]
        public int BrandID
        {
            get { return _BrandID; }
            set
            {
                _BrandID = value;
                Notify("BrandID");
            }
        }

        [DataMember]
        public string Brand { get; set; }

        [DataMember]
        public string LineRemarks { get; set; }

        [DataMember]
        public string LineChallanNo { get; set; }

        double _Quantity;
        [DataMember]
        public double Quantity
        {
            get { return _Quantity; }
            set
            {
                if (value > (OrderedQty - EarlierIssuedQty))
                {
                    _Quantity = 0;
                }
                else
                {
                    _Quantity = value;
                }
                Notify("Quantity");
                UpdateLinePrice();
            }
        }

        [DataMember]
        public double OrderedQty { get; set; }

        [DataMember]
        public double EarlierIssuedQty { get; set; }

        [DataMember]
        public double PendingQty
        {
            get { return EarlierIssuedQty - Quantity; }
            set { }
        }

        double _UnitPrice;
        [DataMember]
        public double UnitPrice
        {
            get { return _UnitPrice; }
            set
            {
                _UnitPrice = value;
                Notify("UnitPrice");
                UpdateLinePrice();
            }
        }

        [DataMember]
        public double LinePrice { get { return Quantity * UnitPrice; } set { } }

        [DataMember]
        public string TruckNo { get; set; }

        [DataMember]
        public int MRID { get; set; }

        [DataMember]
        public int MRLineID { get; set; }

        #endregion

        #region Methods...

        public void UpdateLinePrice()
        {
            LinePrice = Quantity * UnitPrice;
            Notify("LinePrice");
        }

        #endregion
    }
}
