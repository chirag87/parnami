﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace SIMS.MM.MODEL
{
    [DataContract]
    public class MTender
    {
        [DataMember]
        public int ID { get; set; }
        
        [DataMember]
        public string CurrentOwner { get; set; }

        [DataMember]
        public string Title { get; set; }

        [DataMember]
        public string ClientName { get; set; }

        [DataMember]
        public string ClientAddress { get; set; }

        [DataMember]
        public DateTime? DueOn { get; set; }

        [DataMember]
        public DateTime? FirstSubmitOn { get; set; }

        [DataMember]
        public string SiteLocation { get; set; }

        [DataMember]
        public string SiteAddress { get; set; }

        [DataMember]
        public string SiteCity { get; set; }

        [DataMember]
        public string SiteState  { get; set; }

        [DataMember]
        public string SitePincode { get; set; }

        [DataMember]
        public bool IsWon { get; set; }

        [DataMember]
        public int? ProjectID { get; set; }

        [DataMember]
        public double? ExpectedTotal { get; set; }

        [DataMember]
        public DateTime? ExpStartDate { get; set; }

        [DataMember]
        public DateTime? ExpEndDate { get; set; }

        [DataMember]
        public double? Taxes { get; set; }

        [DataMember]
        public double? TotalAfterTax { get; set; }
    }
}
