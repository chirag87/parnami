﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;

namespace SIMS.MM.MODEL
{
    [DataContract]
    public class MReceivingHeader : EntityModel
    {
        [DataMember]
        public int ID { get; set; }

        [DataMember]
        public string ReferenceNo { get; set; }

        [DataMember]
        public string BillingStatus { get; set; }

        [DataMember]
        public DateTime ForDate { get; set; }

        [DataMember]
        public string Remarks { get; set; }

        [DataMember]
        [Display(AutoGenerateField = false)]
        public string Vendor { get; set; }

        [DataMember]
        [Display(Name = "Vendor", Order = 3)]
        public string DisplayVendor { get; set; }

        [DataMember]
        [Display(AutoGenerateField = false)]
        public string Site { get; set; }

        [DataMember]
        [Display(Name = "Site", Order = 4)]
        public string DisplaySite { get; set; }

        [DataMember]
        public DateTime UpdateOn { get; set; }

        [DataMember]
        public string ChallanNo { get; set; }

        [DataMember]
        public bool IsSelected { get; set; }

        [DataMember]
        [Display(AutoGenerateField = false)]
        public string Type { get; set; }

        [DataMember]
        [Display(AutoGenerateField = false)]
        public int? TotalTrnLines { get; set; }

        [DataMember]
        [Display(AutoGenerateField = false)]
        public int? BilliedTrnLines { get; set; }

        [DataMember]
        [Display(AutoGenerateField = false)]
        public int? UnBilliedTrnLines { get; set; }
    }
}
