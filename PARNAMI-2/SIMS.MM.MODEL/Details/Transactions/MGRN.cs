﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.Collections.ObjectModel;

namespace SIMS.MM.MODEL
{
    [DataContract]
    public class MGRN : EntityModel
    {
        public MGRN()
        {
            ForDate = DateTime.UtcNow.AddHours(5.5);
            LastUpdatedOn = DateTime.UtcNow.AddHours(5.5);
            CreatedOn = DateTime.UtcNow.AddHours(5.5);
        }

        #region Events

        public event EventHandler SiteIDChanged;
        public void RaiseSiteIDChanged()
        {
            if (SiteIDChanged != null)
                SiteIDChanged(this, new EventArgs());
        }
        
        #endregion

        #region Model

        [DataMember]
        public int ID { get; set; }

        [DataMember]
        public string ReferenceNo { get; set; }

        [DataMember]
        public int? VendorID { get; set; }

        [DataMember]
        public string Vendor { get; set; }

        public int _SiteID = new int();
        [DataMember]
        public int SiteID
        {
            get { return _SiteID; }
            set
            {
                _SiteID = value;
                Notify("SiteID");
                RaiseSiteIDChanged();
            }
        }

        [DataMember]
        public string Site { get; set; }

        [DataMember]
        public string Remarks { get; set; }

        [DataMember]
        public string Requester { get; set; }

        DateTime _ForDate;
        [DataMember]
        public DateTime ForDate
        {
            get
            {
                if (_ForDate < DateTime.UtcNow.AddHours(5.5))
                    return _ForDate;
                else
                    return DateTime.UtcNow.AddHours(5.5);
            }
            set
            {
                _ForDate = value;
                Notify("ForDate");
            }
        }

        DateTime _CreatedOn;
        [DataMember]
        public DateTime CreatedOn
        {
            get
            {
                if (_CreatedOn < DateTime.UtcNow.AddHours(5.5))
                    return _CreatedOn;
                else
                    return DateTime.UtcNow.AddHours(5.5);
            }
            set
            {
                _CreatedOn = value;
                Notify("CreatedOn");
            }
        }

        [DataMember]
        public string CreatedBy { get; set; }

        DateTime _LastUpdatedOn;
        [DataMember]
        public DateTime LastUpdatedOn
        {
            get
            {
                if (_LastUpdatedOn < DateTime.UtcNow.AddHours(5.5))
                    return _LastUpdatedOn;
                else
                    return DateTime.UtcNow.AddHours(5.5);
            }
            set
            {
                _LastUpdatedOn = value;
                Notify("LastUpdatedOn");
            }
        }

        [DataMember]
        public string LastUpdatedBy { get; set; }

        [DataMember]
        public string InvoiceNo { get; set; }

        [DataMember]
        public string ChallanNo { get; set; }

        [DataMember]
        public string VehicleDetails { get; set; }

        ObservableCollection<MGRNLine> _GRNLines = new ObservableCollection<MGRNLine>();
        [DataMember]
        public ObservableCollection<MGRNLine> GRNLines
        {
            get { return _GRNLines; }
            set
            {
                _GRNLines = value;
                Notify("GRNLines");
            }
        }

        #region For Billing

        [DataMember]
        public string BillingStatus { get; set; }

        [DataMember]
        public int? TotalTrnLines { get; set; }

        [DataMember]
        public int? BilliedTrnLines { get; set; }

        [DataMember]
        public int? UnBilliedTrnLines { get; set; }

        #endregion

        #endregion

        #region Methods

        public MGRNLine AddNewLine()
        {
            if (this.GRNLines == null)
                this.GRNLines = new ObservableCollection<MGRNLine>();
            var line = new MGRNLine();
            this.GRNLines.Add(line);
            this.UpdateLineNumbers();
            return line;
        }

        public int UpdateLineNumbers()
        {
            if (this.GRNLines == null) return 0;
            int i = 1;
            foreach (var item in this.GRNLines)
            {
                item.LineID = i;
                i++;
            }
            return i - 1;
        }

        #endregion
    }

    [DataContract]
    public class MGRNLine : EntityModel
    {
        public static bool AllowOverReceiving { get; set; }

        static MGRNLine()
        {
            AllowOverReceiving = false;
        }

        #region Events...

        public event EventHandler ItemIDChanged;
        public void RaiseItemIDChanged()
        {
            if (ItemIDChanged != null)
                ItemIDChanged(this, new EventArgs());
        }

        public event EventHandler SizeIDChanged;
        public void RaiseSizeIDChanged()
        {
            if (SizeIDChanged != null)
                SizeIDChanged(this, new EventArgs());
        }

        public event EventHandler BrandIDChanged;
        public void RaiseBrandIDChanged()
        {
            if (BrandIDChanged != null)
                BrandIDChanged(this, new EventArgs());
        }

        #endregion

        #region Model

        [DataMember]
        public int HeaderID { get; set; }

        int? _POID;
        [DataMember]
        public int? POID
        {
            get { return _POID; }
            set
            {
                _POID = value;
                Notify("POID");
            }
        }

        string _PORefNo;
        [DataMember]
        public string PORefNo
        {
            get { return _PORefNo; }
            set
            {
                _PORefNo = value;
                Notify("PORefNo");
            }
        }

        [DataMember]
        public int? PRID { get; set; }

        [DataMember]
        public int? PRLineID { get; set; }

        [DataMember]
        public string POInfo
        {
            get
            {
                if (POID != 0 && PRLineID != 0)
                    return "PO: " + PORefNo + " (ID: " + POID + ", LineID: " + PRLineID + ")";
                //return "PO: " + POID + ", LineID: " + PRLineID;
                else
                    return "";
            }
            set { }
        }

        int _LineID;
        [DataMember]
        public int LineID
        {
            get { return _LineID; }
            set
            {
                _LineID = value;
                Notify("LineID");
            }
        }

        [DataMember]
        public MTruckParameter TruckParameters { get; set; }

        int _itemID { get; set; }
        [DataMember]
        public int ItemID
        {
            get { return _itemID; }
            set
            {
                _itemID = value;
                Notify("ItemID");
                RaiseItemIDChanged();
            }
        }

        [DataMember]
        public string Item { get; set; }

        int? _SizeID;
        [DataMember]
        public int? SizeID
        {
            get { return _SizeID; }
            set
            {
                _SizeID = value;
                Notify("SizeID");
                RaiseSizeIDChanged();
            }
        }

        [DataMember]
        public string Size { get; set; }

        int? _BrandID;
        [DataMember]
        public int? BrandID
        {
            get { return _BrandID; }
            set
            {
                _BrandID = value;
                Notify("BrandID");
                RaiseBrandIDChanged();
            }
        }

        [DataMember]
        public string Brand { get; set; }

        [DataMember]
        public string LineRemarks { get; set; }

        [DataMember]
        public string LineChallanNo { get; set; }

        double _Quantity;
        [DataMember]
        public double Quantity
        {
            get { return _Quantity; }
            set
            {
                if (!AllowOverReceiving)
                {
                    if (POID != null && value > (OrderedQty - EarlierReceivedQty))
                    {
                        _Quantity = 0;
                    }
                    else
                    {
                        _Quantity = value;
                    }
                }
                else
                    _Quantity = value;

                Notify("Quantity");
                UpdateLinePrice();
            }
        }

        double _UnitPrice;
        [DataMember]
        public double UnitPrice
        {
            get { return _UnitPrice; }
            set
            {
                _UnitPrice = value;
                Notify("UnitPrice");
                UpdateLinePrice();
            }
        }

        [DataMember]
        public string ConsumableType { get; set; }

        [DataMember]
        public double LinePrice { get { return Quantity * UnitPrice; } set { } }

        [DataMember]
        public double Discount { get; set; }

        [DataMember]
        public string DiscountType { get; set; }

        [DataMember]
        public double DiscountAmount
        {
            get 
            {
                if (Discount > 0 || Discount != null)
                    return (LinePrice * Discount) / 100;
                else
                    return 0;
            }
            set { }
        }

        [DataMember]
        public double TotalLinePrice 
        {
            get
            {
                return LinePrice - DiscountAmount;
            }
            set { }
        }
        
        [DataMember]
        public double OrderedQty { get; set; }

        [DataMember]
        public double EarlierReceivedQty { get; set; }

        [DataMember]
        public double PendingQty 
        {
            get
            {
                return EarlierReceivedQty - Quantity;
            }
            set { }
        }

        [DataMember]
        public string TruckNo { get; set; }

        #endregion

        #region For BILLING AND RECEIVING STATUS...

        [DataMember]
        public int? BillingID { get; set; }

        [DataMember]
        public int? BillingLineID { get; set; }

        [DataMember]
        public string BillNo { get; set; }

        [DataMember]
        public DateTime? BillDate { get; set; }

        [DataMember]
        public double? TotalbilingAmnt { get; set; }

        [DataMember]
        public double? TotalPaidAmnt { get; set; }

        [DataMember]
        public bool? IsBillClosed { get; set; }

        [DataMember]
        public string BillingStatus
        {
            get
            {
                if (IsBillClosed == true)
                    return "Closed";
                else if (HasBill == false && IsBillClosed == false)
                    return "UnBilled";
                else
                    return "Open";
            }
            set
            {
                Notify("IsBillClosed");
                Notify("BillingStatus");
            }
        }

        bool _HasBill;
        [DataMember]
        public bool HasBill
        {
            get { return _HasBill; }
            set
            {
                _HasBill = value;
                Notify("HasBill");
            }
        }

        #endregion

        #region For Billing
        /*For Billing*/


        [DataMember]
        public string ReferenceNo { get; set; }

        [DataMember]
        public int? VendorID { get; set; }

        [DataMember]
        public string Vendor { get; set; }

        [DataMember]
        public string ReceivingChallanNo { get; set; }

        [DataMember]
        public DateTime? CreationDate { get; set; }

        [DataMember]
        public int? SiteID { get; set; }

        [DataMember]
        public string Site { get; set; }

        #endregion

        #region Methods...

        public void UpdateLinePrice()
        {
            LinePrice = Quantity * UnitPrice;
            Notify("LinePrice");
        }

        #endregion
    }
}
