﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.Collections.ObjectModel;

namespace SIMS.MM.MODEL
{
    [DataContract]
    public class MReturnToVendor : EntityModel
    {
        public MReturnToVendor()
        {
            SentDate = DateTime.UtcNow.AddHours(5.5);
            ReceivingDate = DateTime.UtcNow.AddHours(5.5);
            LastUpdatedOn = DateTime.UtcNow.AddHours(5.5);
            CreatedOn = DateTime.UtcNow.AddHours(5.5);
        }

        #region Events

        public event EventHandler SiteIDChanged;
        public void RaiseSiteIDChanged()
        {
            if (SiteIDChanged != null)
                SiteIDChanged(this, new EventArgs());
        }

        #endregion

        #region Model

        [DataMember]
        public int ID { get; set; }

        [DataMember]
        public string ReferenceNo { get; set; }

        [DataMember]
        public int VendorID { get; set; }

        [DataMember]
        public string Vendor { get; set; }

        public int _SiteID = new int();
        [DataMember]
        public int SiteID
        {
            get { return _SiteID; }
            set
            {
                _SiteID = value;
                Notify("SiteID");
                RaiseSiteIDChanged();
            }
        }

        [DataMember]
        public string Site { get; set; }

        [DataMember]
        public int? TransitID { get; set; }

        [DataMember]
        public string Transporter { get; set; }

        [DataMember]
        public string Remarks { get; set; }

        [DataMember]
        public string ReturnedBy { get; set; }

        DateTime _SentDate;
        [DataMember]
        public DateTime SentDate
        {
            get
            {
                if (_SentDate < DateTime.UtcNow.AddHours(5.5))
                    return _SentDate;
                else
                    return DateTime.UtcNow.AddHours(5.5);
            }
            set
            {
                _SentDate = value;
                Notify("SentDate");
            }
        }

        [DataMember]
        public bool IsReceived { get; set; }

        DateTime? _ReceivingDate;
        [DataMember]
        public DateTime? ReceivingDate
        {
            get { return _ReceivingDate; }
            set
            {
                _ReceivingDate = value;
                Notify("ReceivingDate");
            }
        }

        DateTime _CreatedOn;
        [DataMember]
        public DateTime CreatedOn
        {
            get
            {
                if (_CreatedOn < DateTime.UtcNow.AddHours(5.5))
                    return _CreatedOn;
                else
                    return DateTime.UtcNow.AddHours(5.5);
            }
            set
            {
                _CreatedOn = value;
                Notify("CreatedOn");
            }
        }

        [DataMember]
        public string CreatedBy { get; set; }

        DateTime _LastUpdatedOn;
        [DataMember]
        public DateTime LastUpdatedOn
        {
            get
            {
                if (_LastUpdatedOn < DateTime.UtcNow.AddHours(5.5))
                    return _LastUpdatedOn;
                else
                    return DateTime.UtcNow.AddHours(5.5);
            }
            set
            {
                _LastUpdatedOn = value;
                Notify("LastUpdatedOn");
            }
        }

        [DataMember]
        public string LastUpdatedBy { get; set; }

        [DataMember]
        public string SiteChallanNo { get; set; }

        [DataMember]
        public string VendorChallanNo { get; set; }

        [DataMember]
        public string Status
        {
            get 
            {
                if (IsReceived)
                    return "Closed !";
                else
                    return "";
            }
            set { }
        }

        ObservableCollection<MReturnToVendorLine> _RTVLines = new ObservableCollection<MReturnToVendorLine>();
        [DataMember]
        public ObservableCollection<MReturnToVendorLine> RTVLines
        {
            get { return _RTVLines; }
            set
            {
                _RTVLines = value;
                Notify("RTVLines");
            }
        }

        #endregion

        #region Methods

        public MReturnToVendorLine AddNewLine()
        {
            if (this.RTVLines == null)
                this.RTVLines = new ObservableCollection<MReturnToVendorLine>();
            var line = new MReturnToVendorLine();
            this.RTVLines.Add(line);
            this.UpdateLineNumbers();
            return line;
        }

        public int UpdateLineNumbers()
        {
            if (this.RTVLines == null) return 0;
            int i = 1;
            foreach (var item in this.RTVLines)
            {
                item.LineID = i;
                i++;
            }
            return i - 1;
        }

        #endregion
    }

    [DataContract]
    public class MReturnToVendorLine : EntityModel
    {
        public static bool AllowOverReceiving { get; set; }

        public MReturnToVendorLine()
        {
            ScrapQty = 0;
            ExtraQty = 0;
        }

        #region Events...

        public event EventHandler ItemIDChanged;
        public void RaiseItemIDChanged()
        {
            if (ItemIDChanged != null)
                ItemIDChanged(this, new EventArgs());
        }

        #endregion

        #region Model

        [DataMember]
        public int HeaderID { get; set; }

        int _LineID;
        [DataMember]
        public int LineID
        {
            get { return _LineID; }
            set
            {
                _LineID = value;
                Notify("LineID");
            }
        }

        int _itemID { get; set; }
        [DataMember]
        public int ItemID
        {
            get { return _itemID; }
            set
            {
                _itemID = value;
                Notify("ItemID");
                RaiseItemIDChanged();
            }
        }

        int? _SizeID;
        [DataMember]
        public int? SizeID
        {
            get { return _SizeID; }
            set
            {
                _SizeID = value;
                Notify("SizeID");
            }
        }

        [DataMember]
        public string Size { get; set; }

        int? _BrandID;
        [DataMember]
        public int? BrandID
        {
            get { return _BrandID; }
            set
            {
                _BrandID = value;
                Notify("BrandID");
            }
        }

        [DataMember]
        public string Brand { get; set; }

        [DataMember]
        public string ConsumableType { get; set; }

        [DataMember]
        public string Item { get; set; }

        [DataMember]
        public string LineRemarks { get; set; }

        double _Quantity;
        [DataMember]
        public double Quantity
        {
            get { return _Quantity; }
            set
            {
                _Quantity = value;
                Notify("Quantity");
                Notify("TotalReceivedQty");
                Notify("NetQty");
            }
        }

        double? _ScrapQty;
        [DataMember]
        public double? ScrapQty
        {
            get { return _ScrapQty; }
            set
            {
                _ScrapQty = value;
                Notify("ScrapQty");
                Notify("NetQty");
            }
        }

        double? _ExtraQty;
        [DataMember]
        public double? ExtraQty
        {
            get { return _ExtraQty; }
            set
            {
                _ExtraQty = value;
                Notify("ExtraQty");
                Notify("TotalReceivedQty");
                Notify("NetQty");
            }
        }

        [DataMember]
        public double TotalReceivedQty
        {
            get { return Quantity + ExtraQty.Value; }
            set { }
        }

        [DataMember]
        public double NetQty
        {
            get { return (Quantity - ScrapQty.Value) + ExtraQty.Value; }
            set { }
        }

        [DataMember]
        public string TruckNo { get; set; }

        #endregion
    }
}