﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.ComponentModel;

namespace SIMS.MM.MODEL 
{
    [DataContract]
    public class MTruckParameter : EntityModel
    {
        [DataMember]
        public int ID { get; set; }

        double _LFeet = new double();
        [DataMember]
        public double LFeet
        {
            get { return _LFeet; }
            set
            {
                _LFeet = value;
                Notify("LFeet");
            }
        }

        double _LInches = new double();
        [DataMember]
        public double LInches
        {
            get { return _LInches; }
            set
            {
                _LInches = value;
                Notify("LInches");
            }
        }

        [DataMember]
        public double Length
        {
            get 
            { 
                return LFeet + LInches/12; 
            }
            set { }
        }

        double _BFeet = new double();
        [DataMember]
        public double BFeet
        {
            get { return _BFeet; }
            set
            {
                _BFeet = value;
                Notify("BFeet");
            }
        }

        double _BInches = new double();
        [DataMember]
        public double BInches
        {
            get { return _BInches; }
            set
            {
                _BInches = value;
                Notify("BInches");
            }
        }

        [DataMember]
        public double Breadth
        {
            get
            {
                return BFeet + BInches / 12;
            }
            set { }
        }

        double _HFeet = new double();
        [DataMember]
        public double HFeet
        {
            get { return _HFeet; }
            set
            {
                _HFeet = value;
                Notify("HFeet");
            }
        }

        double _HInches = new double();
        [DataMember]
        public double HInches
        {
            get { return _HInches; }
            set
            {
                _HInches = value;
                Notify("HInches");
            }
        }

        [DataMember]
        public double Height 
        { 
            get
            {
                return HFeet + HInches / 12;
            }
            set { }
        }

        [DataMember]
        public double Volume
        {
            get
            {
                return (Length * Breadth * Height);
            }
            set { }
        }
    }
}
