﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.Collections.ObjectModel;
using System.ComponentModel;

namespace SIMS.MM.MODEL
{
    [DataContract]
    public class MReceiving : EntityModel
    {
        public MReceiving()
        {
            ReleaseOn = DateTime.UtcNow.AddHours(5.5);
            ReceivedOn = DateTime.UtcNow.AddHours(5.5);
            PurchaseDate = DateTime.UtcNow.AddHours(5.5);
            LastUpdaetdOn = DateTime.UtcNow.AddHours(5.5);
            CreatedOn = DateTime.UtcNow.AddHours(5.5);
            IssuedOn = DateTime.UtcNow.AddHours(5.5);
            ReturnedOn = DateTime.UtcNow.AddHours(5.5);
        }

        [DataMember]
        public string ReferenceNo { get; set; }

        [DataMember]
        public int TransactionID { get; set; }

        [DataMember]
        public string TransactionTypeID { get; set; }

        [DataMember]
        public int? TransporterID { get; set; }

        [DataMember]
        public string Transporter { get; set; }

        int? _VendorID;
        [DataMember]
        public int? VendorID
        {
            get { return _VendorID; }
            set
            {
                _VendorID = value;
                Notify("VendorID");
            }
        }

        [DataMember]
        public string Vendor { get; set; }

        public int _SiteID=new int();
        [DataMember]
        public int SiteID
        {
            get { return _SiteID; }
            set
            {
                _SiteID = value;
                Notify("SiteID");
                RaiseSiteIDChanged();
            }
        }

        public event EventHandler SiteIDChanged;
        public void RaiseSiteIDChanged()
        {
            if (SiteIDChanged != null)
                SiteIDChanged(this, new EventArgs());
        }

        [DataMember]
        public string Site { get; set; }

        [DataMember]
        public int TranferSiteID { get; set; }

        [DataMember]
        public string TransferSite { get; set; }

        string _ReleasedBy;
        [DataMember]
        public string ReleasedBy
        {
            get { return _ReleasedBy; }
            set
            {
                _ReleasedBy = value;
                Notify("ReleasedBy");
            }
        }

        DateTime _ReleaseOn;
        [DataMember]
        public DateTime ReleaseOn
        {
            get 
            {
                if (_ReleaseOn < DateTime.UtcNow.AddHours(5.5))
                    return _ReleaseOn;
                else
                    return DateTime.UtcNow.AddHours(5.5);
            }
            set
            {
                _ReleaseOn = value;
                Notify("ReleaseOn");
            }
        }

        string _ReceivedBy;
        [DataMember]
        public string ReceivedBy
        {
            get { return _ReceivedBy; }
            set
            {
                _ReceivedBy = value;
                Notify("ReceivedBy");
            }
        }

        DateTime _ReceivedOn;
        [DataMember]
        public DateTime ReceivedOn
        {
            get
            {
                if (_ReceivedOn < DateTime.UtcNow.AddHours(5.5))
                    return _ReceivedOn;
                else
                    return DateTime.UtcNow.AddHours(5.5);
            }
            set
            {
                _ReceivedOn = value;
                Notify("ReceivedOn");
            }
        }

        string _IssuedBy;
        [DataMember]
        public string IssuedBy
        {
            get { return _IssuedBy; }
            set
            {
                _IssuedBy = value;
                Notify("IssuedBy");
            }
        }

        DateTime _IssuedOn;
        [DataMember]
        public DateTime IssuedOn
        {
            get
            {
                if (_IssuedOn < DateTime.UtcNow.AddHours(5.5))
                    return _IssuedOn;
                else
                    return DateTime.UtcNow.AddHours(5.5);
            }
            set
            {
                _IssuedOn = value;
                Notify("IssuedOn");
            }
        }

        string _ReturnReceivedBy;
        [DataMember]
        public string ReturnReceivedBy
        {
            get { return _ReturnReceivedBy; }
            set
            {
                _ReturnReceivedBy = value;
                Notify("ReturnedBy");
            }
        }

        DateTime _ReturnedOn;
        [DataMember]
        public DateTime ReturnedOn
        {
            get
            {
                if (_ReturnedOn < DateTime.UtcNow.AddHours(5.5))
                    return _ReturnedOn;
                else
                    return DateTime.UtcNow.AddHours(5.5);
            }
            set
            {
                _ReturnedOn = value;
                Notify("ReturnedOn");
            }
        }

        [DataMember]
        public int TransitID { get; set; }

        [DataMember]
        public int MOID { get; set; }

        [DataMember]
        public int MRID { get; set; }

        [DataMember]
        public string Remarks { get; set; }

        [DataMember]
        public string Purchaser { get; set; }

        DateTime _PurchaseDate;
        [DataMember]
        public DateTime PurchaseDate
        {
            get
            {
                if (_PurchaseDate < DateTime.UtcNow.AddHours(5.5))
                    return _PurchaseDate;
                else
                    return DateTime.UtcNow.AddHours(5.5);
            }
            set
            {
                _PurchaseDate = value;
                Notify("PurchaseDate");
            }
        }

        DateTime _CreatedOn;
        [DataMember]
        public DateTime CreatedOn
        {
            get
            {
                if (_CreatedOn < DateTime.UtcNow.AddHours(5.5))
                    return _CreatedOn;
                else
                    return DateTime.UtcNow.AddHours(5.5);
            }
            set
            {
                _CreatedOn = value;
                Notify("CreatedOn");
            }
        }

        [DataMember]
        public string CreatedBy { get; set; }

        DateTime _LastUpdaetdOn;
        [DataMember]
        public DateTime LastUpdaetdOn
        {
            get
            {
                if (_LastUpdaetdOn < DateTime.UtcNow.AddHours(5.5))
                    return _LastUpdaetdOn;
                else
                    return DateTime.UtcNow.AddHours(5.5);
            }
            set
            {
                _LastUpdaetdOn = value;
                Notify("LastUpdaetdOn");
            }
        }

        [DataMember]
        public string LastUpdatedBy { get; set; }

        [DataMember]
        public string InvoiceNo { get; set; }

        [DataMember]
        public string ChallanNo { get; set; }

        [DataMember]
        public string ReceivingRemarks { get; set; }

        [DataMember]
        public string VehicleDetails { get; set; }

        ObservableCollection<MReceivingLine> _ReceivingLines = new ObservableCollection<MReceivingLine>();
        [DataMember]
        public ObservableCollection<MReceivingLine> ReceivingLines
        {
            get { return _ReceivingLines; }
            set
            {
                _ReceivingLines = value;
                Notify("ReceivingLines");
            }
        }

        public MReceivingLine AddNewLine()
        {
            if (this.ReceivingLines == null)
                this.ReceivingLines = new ObservableCollection<MReceivingLine>();
            var line = new MReceivingLine();
            this.ReceivingLines.Add(line);
            this.UpdateLineNumbers();
            return line;
        }

        public int UpdateLineNumbers()
        {
            if (this.ReceivingLines == null) return 0;
            int i = 1;
            foreach (var item in this.ReceivingLines)
            {
                item.LineID = i;
                i++;
            }
            return i - 1;
        }
    }
}
