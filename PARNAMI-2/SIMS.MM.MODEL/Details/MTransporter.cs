﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;

namespace SIMS.MM.MODEL
{
    [DataContract]
    public class MTransporter : EntityModel
    {
        [DataMember]
        [Display(AutoGenerateField = false)]
        public int ID { get; set; }

        [DataMember]
        [Required]
        public string Name { get; set; }

        [DataMember]
        public string Address { get; set; }

        [DataMember]
        public string City { get; set; }

        [DataMember]
        public string State { get; set; }

        [DataMember]
        public string PinCode { get; set; }

        [DataMember]
        [Display(AutoGenerateField = false)]
        public bool? IsActive { get; set; }

        [DataMember]
        public string Email { get; set; }

        [DataMember]
        public string ContactName { get; set; }

        [DataMember]
        public string ContactMobile1 { get; set; }

        [DataMember]
        public string ContactMobile2 { get; set; }

        [DataMember]
        public string ContactEmail { get; set; }

        [DataMember]
        public string TINNo { get; set; }

        [DataMember]
        public string CST { get; set; }

        [DataMember]
        public string PANNo { get; set; }

        [DataMember]
        public string ServiceTaxNo { get; set; }
    }
}
