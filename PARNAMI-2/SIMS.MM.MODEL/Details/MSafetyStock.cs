﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace SIMS.MM.MODEL
{
    [DataContract]
    public class MSafetyStock : EntityModel
    {
        [DataMember]
        public int ItemID { get; set; }

        [DataMember]
        public string ItemName { get; set; }

        [DataMember]
        public string Category { get; set; }

        [DataMember]
        public string MU { get; set; }

        [DataMember]
        public double? SafetyStock { get; set; }

        [DataMember]
        public double? ClosingBalance { get; set; }

        [DataMember]
        public double ReorderQty { get; set; }

        [DataMember]
        public string Remarks { get; set; }

        [DataMember]
        public string ItemDescription { get; set; }
    }
}
