﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace SIMS.MM.MODEL
{
    [DataContract]
    public class MDisplayItemCategories : EntityModel
    {
        [DataMember]
        public int ID { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public string ParentName { get; set; }

        [DataMember]
        public int? ParentID { get; set; }

        [DataMember]
        public string Display { get; set; }
    }
}
