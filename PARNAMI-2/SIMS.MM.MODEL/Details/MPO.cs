﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace SIMS.MM.MODEL
{
    [DataContract]
    public class MPO : MPR
    {
        [DataMember]
        public int POID { get; set; }

        [DataMember]
        public string POReferenceNo { get; set; }

        //[DataMember]
        //public DateTime CreatedOn { get; set; }
        DateTime _CreatedOn;
        [DataMember]
        public DateTime CreatedOn
        {
            get
            {
                if (_CreatedOn < DateTime.UtcNow.AddHours(5.5))
                    return _CreatedOn;
                else
                    return DateTime.UtcNow.AddHours(5.5);
            }
            set
            {
                _CreatedOn = value;
                Notify("CreatedOn");
            }
        }

        [DataMember]
        public string CreatedBy { get; set; }

        [DataMember]
        public bool IsAcknowledged { get; set; }

        [DataMember]
        public string AcknowledgedStatus
        {
            get { return IsAcknowledged?"Acknowledged":"Pending";}
            set{}
        }

        [DataMember]
        public DateTime AcknowledgedOn { get; set; }

        [DataMember]
        public string AckRemarks { get; set; }

        bool _IsClosed = false;
        [DataMember]
        public bool IsClosed
        {
            get { return _IsClosed; }
            set
            {
                _IsClosed = value;
                Notify("IsClosed");
            }
        }

        [DataMember]
        public string Status
        {
            get
            {
                if (IsClosed == true)
                    return "Closed !";
                return "";
            }
            set { }
        }
    }
}
