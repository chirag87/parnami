﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.ComponentModel;

namespace SIMS.MM.MODEL
{
    [DataContract]
    public partial class MPRLine : EntityModel
    {
        public MPRLine()
        {
            DeliveryDate = DateTime.UtcNow.AddHours(5.5);
            ConsumableType = "Consumable";
            Quantity = 0;
            AvailableQty = 0;
            SplittedQuantity = 0;
            TotalReceivedQty = 0;
        }

        #region Events...

        public event EventHandler CalculateTODispQty;
        public void RaiseCalculateTODispQty()
        {
            if (CalculateTODispQty != null)
                CalculateTODispQty(this, new EventArgs());
        }

        #endregion

        [DataMember]
        public int ID { get; set; }

        [DataMember]
        public int PRID { get; set; }

        [DataMember]
        public int? POID { get; set; }

        int _LineID;
        [DataMember]
        public int LineID
        {
            get { return _LineID; }
            set
            {
                _LineID = value;
                Notify("LineID");
            }
        }

        bool _IsSelected = false;
        [DataMember]
        public bool IsSelected
        {
            get { return _IsSelected; }
            set
            {
                _IsSelected = value;
                Notify("IsSelected");
            }
        }

        double _OriginalQty;
        [DataMember]
        public double OriginalQty
        {
            get { return _OriginalQty; }
            set
            {
                _OriginalQty = value;
                Quantity = value - SplittedQuantity;
                Notify("OriginalQty");
                Notify("Quantity");
            }
        }

        double _Quantity = 0;
        [DataMember]
        public double Quantity
        {
            get { return _Quantity; }
            set
            {
                _Quantity = value;
                UpdateLinePrice();
                Notify("Quantity");
                Notify("UnavailableQty");
            }
        }

        double _SplittedQuantity = 0;
        [DataMember]
        public double SplittedQuantity
        {
            get { return _SplittedQuantity; }
            set
            {
                _SplittedQuantity = value;
                Notify("SplittedQuantity");
                Notify("UnavailableQty");
            }
        }

        double _AvailableQty = 0;
        [DataMember]
        public double AvailableQty
        {
            get { return _AvailableQty; }
            set
            {
                _AvailableQty = value;
                Notify("AvailableQty");
                Notify("UnavailableQty");
            }
        }

        [DataMember]
        public double UnavailableQty
        {
            get
            {
                //var q = (OriginalQty - AvailableQty);
                //var q = ((Quantity + SplittedQuantity) - AvailableQty);
                var q = OriginalQty - (SplittedQuantity + AvailableQty);//TotalQty-(TOQty+StoreQty)
                if (q < 0)
                    return 0;
                else
                    return q;
            }
            set { }
        }

        double _UnitPrice = 0;
        [DataMember]
        public double UnitPrice
        {
            get { return _UnitPrice; }
            set
            {
                _UnitPrice = value;
                UpdateLinePrice();
            }
        }

        [DataMember]
        public double LastPrice { get; set; }

        int _ItemID;
        [DataMember]
        public int ItemID
        {
            get { return _ItemID; }
            set
            {
                _ItemID = value;
                RaiseCalculateTODispQty();
                Notify("ItemID");
            }
        }

        [DataMember]
        public string Item { get; set; }

        [DataMember]
        public string ItemName { get; set; }

        int? _SizeID;
        [DataMember]
        public int? SizeID
        {
            get { return _SizeID; }
            set
            {
                _SizeID = value;
                RaiseCalculateTODispQty();
                Notify("SizeID");
            }
        }

        [DataMember]
        public string Size { get; set; }

        int? _BrandID;
        [DataMember]
        public int? BrandID
        {
            get { return _BrandID; }
            set
            {
                _BrandID = value;
                RaiseCalculateTODispQty();
                Notify("BrandID");
            }
        }

        [DataMember]
        public string Brand { get; set; }

        string _ConsumableType;
        [DataMember]
        public string ConsumableType
        {
            get { return _ConsumableType; }
            set
            {
                _ConsumableType = value;
                RaiseCalculateTODispQty();
                Notify("ConsumableType");
            }
        }

        DateTime? _DeliveryDate;
        [DataMember]
        public DateTime? DeliveryDate
        {
            get
            {
                //if (_DeliveryDate < DateTime.UtcNow.AddHours(5.5))
                //    return _DeliveryDate;
                //else
                //    return DateTime.UtcNow.AddHours(5.5);
                return _DeliveryDate;
            }
            set
            {
                _DeliveryDate = value;
                Notify("DeliveryDate");
            }
        }


        double _Discount = 0;
        [DataMember]
        public double Discount
        {
            get { return _Discount; }
            set
            {
                if (DiscountType == "%")
                {
                    _Discount = value;
                    DiscountAmount = (LinePrice * value) / 100;
                    UpdateTotalLinePriceOnPercent();
                    Notify("Discount");
                }
                else
                {
                    _Discount = value;
                    DiscountAmount = value;
                    UpdateTotalLinePrice();
                    Notify("Discount");
                }
            }
        }

        double _DiscountAmount = 0;
        [DataMember]
        public double DiscountAmount
        {
            get { return _DiscountAmount; }
            set
            {
                _DiscountAmount = value;
                //LinePriceAfterDiscount = LinePrice - value;
                Notify("DiscountAmount");
            }
        }

        string _DiscountType = "Rs.";
        [DataMember]
        public string DiscountType
        {
            get { return _DiscountType; }
            set
            {
                if (value == "%")
                {
                    _DiscountType = value;
                    Notify("DiscountType");
                    DiscountAmount = (LinePrice * Discount) / 100;
                    UpdateTotalLinePriceOnPercent();
                }
                else
                {
                    DiscountAmount = Discount;
                    _DiscountType = value;
                    Notify("DiscountType");
                    UpdateTotalLinePrice();
                }
            }
        }

        string _ExciseIn = "Rs.";
        [DataMember]
        public string ExciseIn
        {
            get { return _ExciseIn; }
            set
            {
                _ExciseIn = value;
                CalculateExciseDuty();
                Notify("ExciseIn");
            }
        }

        double _Excise = 0;
        [DataMember]
        public double Excise
        {
            get { return _Excise; }
            set
            {
                _Excise = value;
                CalculateExciseDuty(); 
                Notify("Excise");
            }
        }

        private double CalculateExciseDuty()
        {
            if (ExciseIn == "%")
            {
                ExciseAmount = ((LinePrice - DiscountAmount) * Excise) / 100;
                TotalLinePrice = (LinePrice - DiscountAmount) + ExciseAmount;
                Notify("TotalLinePrice");
            }
            else
            {
                ExciseAmount = Excise;
                TotalLinePrice = (LinePrice - DiscountAmount) + ExciseAmount;
                Notify("TotalLinePrice");
            }
            return ExciseAmount;
        }

        double _ExciseAmount;
        [DataMember]
        public double ExciseAmount
        {
            get { return _ExciseAmount; }
            set
            {
                _ExciseAmount = value;
                Notify("ExciseAmount");
            }
        }

        double _EA2;
        public double EA2
        {
            get 
            {
                return CalculateExciseDuty();
            }
            set
            {
                Notify("EA2");
            }
        }

        [DataMember]
        public string LineRemarks { get; set; }

        [DataMember]
        public double LinePrice { get { return Quantity * UnitPrice; } set { } }

        //[DataMember]
        //public double LinePriceAfterDiscount { get { return LinePrice - DiscountAmount; } set { } }

        //double _LinePriceAfterDiscount = 0;
        //[DataMember]
        //public double LinePriceAfterDiscount { get { return _LinePriceAfterDiscount; } set { Math.Round(value, 0); Notify("LinePriceAfterDiscount"); } }

        //[DataMember]
        //public double TotalLinePrice { get { return Math.Round(LinePrice - DiscountAmount + ExciseAmount); } set { } }
        double _TotalLinePrice = 0;
        [DataMember]
        public double TotalLinePrice 
        { 
            get 
            { 
                return _TotalLinePrice; 
            } 
            set 
            { 
                _TotalLinePrice = Math.Round(value, 0);
                Notify("TotalLinePrice");
            } 
        }

        [DataMember]
        public string TruckNo { get; set; }

        [DataMember]
        public string MeasurementUnit { get; set; }

        [DataMember]
        public double? TotalReceivedQty { get; set; }

        MTODispatchInfo _TODispInfo = new MTODispatchInfo(); /*ToDisplay in NewMI Form*/
        [DataMember]
        public MTODispatchInfo TODispInfo
        {
            get { return _TODispInfo; }
            set
            {
                _TODispInfo = value;
                Notify("TODispInfo");
            }
        }

        public void UpdateLinePrice()
        {
            LinePrice = Quantity * UnitPrice;
            Notify("LinePrice");
            if (!(string.IsNullOrWhiteSpace(this.DiscountType)))
            {

                if (this.DiscountType == "%")
                {

                    DiscountAmount = (LinePrice * Discount) / 100;
                    UpdateTotalLinePriceOnPercent();

                }
                else
                {
                    DiscountAmount = Discount;
                    UpdateTotalLinePrice();

                }
            }
            else
            {
                TotalLinePrice = LinePrice + ExciseAmount;
                Notify("TotalLinePrice");
            }
        }

        public void UpdateTotalLinePrice()
        {
            //LinePriceAfterDiscount = LinePrice - DiscountAmount;            
            //Notify("LinePriceAfterDiscount");
            TotalLinePrice = LinePrice - DiscountAmount + ExciseAmount;
            Notify("TotalLinePrice");
        }

        public void UpdateTotalLinePriceOnPercent()
        {
            var discount = (LinePrice * Discount) / 100;
            //LinePriceAfterDiscount = LinePrice - DiscountAmount;
            //Notify("LinePriceAfterDiscount");
            TotalLinePrice = LinePrice - discount + ExciseAmount;            
            Notify("TotalLinePrice");
        }
    }

    public partial class MTODispatchInfo : EntityModel
    {
        int? _ID;   /*In case of PR Detailed View*/
        [DataMember]
        public int? ID
        {
            get { return _ID; }
            set
            {
                _ID = value;
                Notify("ID");
            }
        }

        int _LineID;    /*In case of New PR Screen*/
        [DataMember]
        public int LineID
        {
            get { return _LineID; }
            set
            {
                _LineID = value;
                Notify("LineID");
            }
        }

        int _SiteID;
        [DataMember]
        public int SiteID
        {
            get { return _SiteID; }
            set
            {
                _SiteID = value;
                Notify("SiteID");
            }
        }

        int _ItemID;
        [DataMember]
        public int ItemID
        {
            get { return _ItemID; }
            set
            {
                _ItemID = value;
                Notify("ItemID");
            }
        }

        int? _SizeID;
        [DataMember]
        public int? SizeID
        {
            get { return _SizeID; }
            set
            {
                _SizeID = value;
                Notify("SizeID");
            }
        }

        int? _BrandID;
        [DataMember]
        public int? BrandID
        {
            get { return _BrandID; }
            set
            {
                _BrandID = value;
                Notify("BrandID");
            }
        }

        double _TODispQty;
        [DataMember]
        public double TODispQty
        {
            get { return _TODispQty; }
            set
            {
                _TODispQty = value;
                Notify("TODispQty");
            }
        }
    }
}
