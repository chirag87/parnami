﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace SIMS.MM.MODEL
{
    public class MStockInINDENT : EntityModel
    {
        [DataMember]
        public int PRID { get; set; }

        [DataMember]
        public string ReferenceNo { get; set; }

        [DataMember]
        public DateTime Date { get; set; }

        [DataMember]
        public int SiteID { get; set; }

        [DataMember]
        public string Site { get; set; }

        [DataMember]
        public double Qty { get; set; }

        [DataMember]
        public bool IsApproved { get; set; }

        [DataMember]
        public string ApprovalStatus 
        {
            get
            {
                if (IsApproved == true)
                    return "Approved !";
                else
                    return "Pending Approval !";
            }
            set { }
        }
    }

    public class MStockInPO : EntityModel
    {
        [DataMember]
        public int POID { get; set; }

        [DataMember]
        public string ReferenceNo { get; set; }

        [DataMember]
        public DateTime Date { get; set; }

        [DataMember]
        public int SiteID { get; set; }

        [DataMember]
        public string Site { get; set; }

        [DataMember]
        public double Qty { get; set; }
    }

    public class MStockOnSites : EntityModel
    {
        [DataMember]
        public DateTime Date { get; set; }

        [DataMember]
        public int SiteID { get; set; }

        [DataMember]
        public string Site { get; set; }

        [DataMember]
        public double Qty { get; set; }
    }

    public class MRateHistory : EntityModel
    {
        [DataMember]
        public int POID { get; set; }

        [DataMember]
        public string ReferenceNo { get; set; }

        [DataMember]
        public DateTime Date { get; set; }

        [DataMember]
        public int VendorID { get; set; }

        [DataMember]
        public string Vendor { get; set; }

        [DataMember]
        public double Qty { get; set; }

        [DataMember]
        public double Rate { get; set; }
    }
}
