﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Windows.Printing;
using SIMS.MM.MODEL;

namespace Contruction
{
    public partial class GRNReportPrint : UserControl
    {
        public GRNReportPrint()
        {
            InitializeComponent();
        }

        public void SetReceiving(MReceiving _mreceiving)
        {
            GRNreport.DataContext = _mreceiving;
            GRNreport.ItemsSource = _mreceiving.ReceivingLines;
            GRNreport.Print();
        }
    }
}
