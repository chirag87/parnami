﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace Contruction
{
    public partial class CloseableTabItem : TabItem
    {
        public CloseableTabItem()
        {
            InitializeComponent();
        }

        private void ImgDelete_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            Close();
        }

        public void Close()
        {
            var parent = (TabControl)this.Parent;
            if (parent == null) return;
            parent.Items.Remove(this);
        }
    }
}
