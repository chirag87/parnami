﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using SIMS.MM.MODEL;
using System.ComponentModel;

namespace Contruction
{
    public partial class CFTCalculatorPopup : ChildWindow, INotifyPropertyChanged
    {
        public event EventHandler Submitted;
        public void RaiseSubmitted() 
        { 
            if (Submitted != null) Submitted(this, new EventArgs()); 
        }

        public CFTCalculatorPopup()
        {
            InitializeComponent();
        }

        MTruckParameter _TruckMeasurement;
        public MTruckParameter TruckMeasurement
        {
            get { return _TruckMeasurement; }
            set
            {
                _TruckMeasurement = value;
                Notify("TruckMeasurement");
            }
        }

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            RaiseSubmitted();
            this.DialogResult = true;
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void Notify(string propertyName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }

        private void ItextL_GotFocus(object sender, RoutedEventArgs e)
        {
            ItextL.Text = "";
        }

        private void ItextB_GotFocus(object sender, RoutedEventArgs e)
        {
            ItextB.Text = "";
        }

        private void ItextH_GotFocus(object sender, RoutedEventArgs e)
        {
            ItextH.Text = "";
        }

        private void FtextH_GotFocus(object sender, RoutedEventArgs e)
        {
            FtextH.Text = "";
        }

        private void FtextB_GotFocus(object sender, RoutedEventArgs e)
        {
            FtextB.Text = "";
        }

        private void FtextL_GotFocus(object sender, RoutedEventArgs e)
        {
            FtextL.Text = "";
        }

        private void FtextL_LostFocus(object sender, RoutedEventArgs e)
        {
            if(string.IsNullOrWhiteSpace(FtextL.Text))
                FtextL.Text = "0";
        }

        private void ItextL_LostFocus(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrWhiteSpace(ItextL.Text))
                ItextL.Text = "0";
        }

        private void FtextB_LostFocus(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrWhiteSpace(FtextB.Text))
                FtextB.Text = "0";
        }

        private void ItextB_LostFocus(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrWhiteSpace(ItextB.Text))
                ItextB.Text = "0";
        }

        private void FtextH_LostFocus(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrWhiteSpace(FtextH.Text))
                FtextH.Text = "0";
        }

        private void ItextH_LostFocus(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrWhiteSpace(ItextH.Text))
                ItextH.Text = "0";
        }
    }
}

