﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.ComponentModel;
using Contruction.MasterDBService;
using SIMS.MM.MODEL;

namespace Contruction
{
    public class AddNewItemSizeModel : ViewModel
    {
        MasterDataServiceClient service = new MasterDataServiceClient();

        public AddNewItemSizeModel()
        {
            if (!DesignerProperties.GetIsInDesignMode(Application.Current.RootVisual))
            {
                OnInit();
            }
        }

        public void OnInit()
        {
            service.CreateNewItemSizeCompleted += new EventHandler<CreateNewItemSizeCompletedEventArgs>(service_CreateNewItemSizeCompleted);
        }

        void service_CreateNewItemSizeCompleted(object sender, CreateNewItemSizeCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                Base.ShowError(e.Error);
                IsBusy = false;
            }
            else
            {
                NewSize = e.Result;
                MessageBox.Show("New Size Added Successfully !");
                Base.Current.LoadItemSizes();
                IsBusy = false;
            }
        }

        MItemSize _NewSize = new MItemSize();
        public MItemSize NewSize
        {
            get { return _NewSize; }
            set
            {
                _NewSize = value;
                Notify("NewSize");
            }
        }

        public void SubmitToDB()
        {
            service.CreateNewItemSizeAsync(NewSize);
            IsBusy = true;
        }
    }
}