﻿using System;
using System.Net;
using System.Linq;
using System.Windows;
using System.Windows.Data;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.ComponentModel;
using Contruction.MasterReportingService;
using System.Collections.ObjectModel;
using SIMS.MM.MODEL;
using System.Collections.Generic;

namespace Contruction
{
    public class MIDashBoardVM : ViewModel
    {
        MasterReportingServiceClient service = new MasterReportingServiceClient();

        public MIDashBoardVM()
        {
            if (!DesignerProperties.GetIsInDesignMode(Application.Current.RootVisual))
            {
                OnInit();
            }
        }

        #region Events...

        #endregion

        #region CompletedEvents...

        void service_GetMIDashBoardCompleted(object sender, GetMIDashBoardCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                Base.ShowError(e.Error);
                IsBusy = false;
            }
            else
            {
                MIReportSummary = e.Result;
                TotalRecords = e.Result.Count.ToString();

                /*Calculate Sum of Different Quantities*/
                //SumTotalCPQty = e.Result.Sum(x => x.CPQty);
                SumTotalMIQty = e.Result.Sum(x => x.TotalMIQty);
                SumTotalMRNQty = e.Result.Sum(x => x.MRNQty);
                SumTotalPendingQty = e.Result.Sum(x => x.TobePOQty);
                SumTotalTOQty = e.Result.Sum(x => x.TOQty);

                IsBusy = false;
            }
        }

        #endregion

        #region Properties...

        ObservableCollection<MMIReportSummary> _MIReportSummary = new ObservableCollection<MMIReportSummary>();
        public ObservableCollection<MMIReportSummary> MIReportSummary
        {
            get { return _MIReportSummary; }
            set
            {
                _MIReportSummary = value;
                Notify("MIReportSummary");
            }
        }

        PagedCollectionView _MIReportSiteWise;
        public PagedCollectionView MIReportSiteWise
        {
            get { return _MIReportSiteWise; }
            set
            {
                _MIReportSiteWise = value;
                Notify("MIReportSiteWise");
            }
        }

        DateTime? _SDate = DateTime.UtcNow.AddHours(5.5).AddMonths(-1);
        public DateTime? SDate
        {
            get { return _SDate; }
            set
            {
                _SDate = value;
                Notify("SDate");
            }
        }

        DateTime? _EDate = DateTime.UtcNow.AddHours(5.5);
        public DateTime? EDate
        {
            get { return _EDate; }
            set
            {
                _EDate = value;
                Notify("EDate");
            }
        }

        int? _ItemID;
        public int? ItemID
        {
            get { return _ItemID; }
            set
            {
                _ItemID = value;
                Notify("ItemID");
            }
        }

        int? _SizeID;
        public int? SizeID
        {
            get { return _SizeID; }
            set
            {
                _SizeID = value;
                Notify("SizeID");
            }
        }

        int? _BrandID;
        public int? BrandID
        {
            get { return _BrandID; }
            set
            {
                _BrandID = value;
                Notify("BrandID");
            }
        }

        int? _SiteID;
        public int? SiteID
        {
            get { return _SiteID; }
            set
            {
                _SiteID = value;
                Notify("SiteID");
            }
        }

        string _Status;
        public string Status
        {
            get { return _Status; }
            set
            {
                _Status = value;
                Notify("Status");
            }
        }

        string _ConsumableType;
        public string ConsumableType
        {
            get { return _ConsumableType; }
            set
            {
                _ConsumableType = value;
                Notify("ConsumableType");
            }
        }

        //bool _ShowZeroQty = true;
        //public bool ShowZeroQty
        //{
        //    get { return _ShowZeroQty; }
        //    set
        //    {
        //        _ShowZeroQty = value;
        //        Load();
        //        Notify("ShowZeroQty");
        //    }
        //}

        //string _RType;
        //public string RType
        //{
        //    get { return _RType; }
        //    set
        //    {
        //        _RType = value;
        //        Notify("RType");
        //    }
        //}

        string _TotalRecords;
        public string TotalRecords
        {
            get { return _TotalRecords; }
            set
            {
                _TotalRecords = value;
                Notify("TotalRecords");
            }
        }

        double _SumTotalMIQty = 0;
        public double SumTotalMIQty
        {
            get { return _SumTotalMIQty; }
            set
            {
                _SumTotalMIQty = value;
                Notify("SumTotalMIQty");
            }
        }

        double _SumTotalTOQty = 0;
        public double SumTotalTOQty
        {
            get { return _SumTotalTOQty; }
            set
            {
                _SumTotalTOQty = value;
                Notify("SumTotalTOQty");
            }
        }

        double _SumTotalPendingQty = 0;
        public double SumTotalPendingQty
        {
            get { return _SumTotalPendingQty; }
            set
            {
                _SumTotalPendingQty = value;
                Notify("SumTotalPendingQty");
            }
        }

        double _SumTotalMRNQty = 0;
        public double SumTotalMRNQty
        {
            get { return _SumTotalMRNQty; }
            set
            {
                _SumTotalMRNQty = value;
                Notify("SumTotalMRNQty");
            }
        }

        double _SumTotalCPQty = 0;
        public double SumTotalCPQty
        {
            get { return _SumTotalCPQty; }
            set
            {
                _SumTotalCPQty = value;
                Notify("SumTotalCPQty");
            }
        }

        string _ReportType;
        public string ReportType
        {
            get { return _ReportType; }
            set
            {
                _ReportType = value;
                Notify("ReportType");
            }
        }

        //IEnumerable<string> _StatusList = new List<string>();
        //public IEnumerable<string> StatusList
        //{
        //    get { return _StatusList; }
        //    set
        //    {
        //        _StatusList = value;
        //        Notify("StatusList");
        //    }
        //}

        //IEnumerable<string> _RTypes = new List<string>();
        //public IEnumerable<string> RTypes
        //{
        //    get { return _RTypes; }
        //    set
        //    {
        //        _RTypes = value;
        //        Notify("RTypes");
        //    }
        //}

        //IEnumerable<string> _ReportTypes;
        //public IEnumerable<string> ReportTypes
        //{
        //    get { return _ReportTypes; }
        //    set
        //    {
        //        _ReportTypes = value;
        //        Notify("ReportTypes");
        //    }
        //}

        //bool _IsSiteWiseReportSelected = false;
        //public bool IsSiteWiseReportSelected
        //{
        //    get { return _IsSiteWiseReportSelected; }
        //    set
        //    {
        //        _IsSiteWiseReportSelected = value;
        //        Notify("IsSiteWiseReportSelected");
        //    }
        //}

        #endregion

        #region Methods...

        public void OnInit()
        {
            service.GetMIDashBoardCompleted += new EventHandler<GetMIDashBoardCompletedEventArgs>(service_GetMIDashBoardCompleted);
            //StatusList = (new string[] { "Draft", "SubmittedForVerification", "SubmittedForMIApproval", "MIApproved", "SubmittedForPOApproval", "POApproved", "Closed", "All" });
            //RTypes = (new string[] { "Excel", "PDF" });
            //ReportTypes = (new string[] { "Site-Wise", "Cumulative" });
        }

        public void Load()
        {
            //if (Status == "" || Status == "All") Status = null;
            //if (String.IsNullOrWhiteSpace(ReportType))
            //{
            //    MessageBox.Show("Please Select Report Type !");
            //    return;
            //}
            //if (ReportType == "Site-Wise")
            //    service.GetMIReportSiteWiseSummaryAsync(SDate, EDate, ItemID, SizeID, BrandID, SiteID, ConsumableType, Status, ShowZeroQty);
            //else
            //    service.GetMIReportSummaryAsync(SDate, EDate, ItemID, SizeID, BrandID, SiteID, ConsumableType, Status, ShowZeroQty);
            service.GetMIDashBoardAsync(SDate, EDate, ItemID, SizeID, BrandID, SiteID, ConsumableType);
            IsBusy = true;
        }

        public void ClearLine1()
        {
            SDate = null;
            EDate = null;
            //Status = null;
            ConsumableType = null;
            Load();
        }

        public void ClearLine2()
        {
            ItemID = null;
            SizeID = null;
            BrandID = null;
            SiteID = null;
            Load();
        }

        #endregion
    }
}
