﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.ComponentModel;
using System.Collections.ObjectModel;
using Contruction.MasterReportingService;
using SIMS.MM.MODEL;

namespace Contruction
{
    public class DailyStockBreakUpReportVM : ViewModel
    {
        MasterReportingServiceClient service = new MasterReportingServiceClient();

        public DailyStockBreakUpReportVM()
        {
            if (!DesignerProperties.GetIsInDesignMode(Application.Current.RootVisual))
            {
                OnInit();
            }
        }

        #region Methods...

        public void OnInit()
        {
            service.GetDailyStockBreakUpReportCompleted += new EventHandler<GetDailyStockBreakUpReportCompletedEventArgs>(service_GetDailyStockBreakUpReportCompleted);
        }

        public void Load(DateTime? sdate, DateTime? edate, int siteid, int itemid, int? itemCatID, int? sizeid, int? brandid, string contype)
        {
            service.GetDailyStockBreakUpReportAsync(sdate, edate, itemid, itemCatID, sizeid, brandid, siteid, contype);
            IsBusy = true;
        }

        #endregion

        #region Completed Events...

        void service_GetDailyStockBreakUpReportCompleted(object sender, GetDailyStockBreakUpReportCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                Base.ShowError(e.Error);
                IsBusy = false;
            }
            else
            {
                Data = e.Result;
                IsBusy = false;
            }
        }

        #endregion

        #region Properties...

        ObservableCollection<MDailyStockBreakUp> _Data = new ObservableCollection<MDailyStockBreakUp>();
        public ObservableCollection<MDailyStockBreakUp> Data
        {
            get { return _Data; }
            set
            {
                _Data = value;
                Notify("Data");
            }
        }

        DateTime? _SDate;
        public DateTime? SDate
        {
            get { return _SDate; }
            set
            {
                _SDate = value;
                Notify("SDate");
            }
        }

        DateTime? _EDate;
        public DateTime? EDate
        {
            get { return _EDate; }
            set
            {
                _EDate = value;
                Notify("EDate");
            }
        }

        int? _SiteID;
        public int? SiteID
        {
            get { return _SiteID; }
            set
            {
                _SiteID = value;
                Notify("SiteID");
            }
        }

        int? _ItemID;
        public int? ItemID
        {
            get { return _ItemID; }
            set
            {
                _ItemID = value;
                Notify("ItemID");
            }
        }

        int? _itemCatID;
        public int? itemCatID
        {
            get { return _itemCatID; }
            set
            {
                _itemCatID = value;
                Notify("itemCatID");
            }
        }

        int? _SizeID;
        public int? SizeID
        {
            get { return _SizeID; }
            set
            {
                _SizeID = value;
                Notify("SizeID");
            }
        }

        int? _BrandID;
        public int? BrandID
        {
            get { return _BrandID; }
            set
            {
                _BrandID = value;
                Notify("BrandID");
            }
        }

        string _ConsumableType;
        public string ConsumableType
        {
            get { return _ConsumableType; }
            set
            {
                _ConsumableType = value;
                Notify("ConsumableType");
            }
        }

        #endregion
    }
}
