﻿using System;
using System.Net;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Contruction.PurchasingService;
using System.Collections.ObjectModel;
using SIMS.MM.MODEL;
using System.ComponentModel;
using Contruction.MOService;

namespace Contruction
{
    public class ViewPRDetailsModel : ViewModel
    {
        PurchasingServiceClient service = new PurchasingServiceClient();
        MOServiceClient moservice = new MOServiceClient();

        public ViewPRDetailsModel()
        {
            if (!DesignerProperties.GetIsInDesignMode(Application.Current.RootVisual))
            {
                OnInit();
            }
        }

        #region EVENTS...

        public event EventHandler POConverted;
        public void RaisePOConverted()
        {
            if (POConverted != null)
                POConverted(this, new EventArgs());
        }

        public class MICopyRequestedArgs : EventArgs
        {
            public MPR mpr { get; set; }
            public MICopyRequestedArgs(MPR _mpr) { mpr = _mpr; }
        }

        public event EventHandler<MICopyRequestedArgs> MICopied;
        public void RaiseMICopied(MPR newpr)
        {
            if (MICopied != null)
                MICopied(this, new MICopyRequestedArgs(newpr));
        }

        #endregion

        #region METHODS...

        public void OnInit()
        {
            service.GetPRbyIDCompleted += new EventHandler<GetPRbyIDCompletedEventArgs>(service_GetPRbyIDCompleted);
            service.ConvertToPOCompleted += new EventHandler<ConvertToPOCompletedEventArgs>(service_ConvertToPOCompleted);
            service.VerifyPRCompleted += new EventHandler<VerifyPRCompletedEventArgs>(service_VerifyPRCompleted);
            service.InitialApprovePRCompleted += new EventHandler<InitialApprovePRCompletedEventArgs>(service_InitialApprovePRCompleted);
            service.ApprovePRCompleted += new EventHandler<ApprovePRCompletedEventArgs>(service_ApprovePRCompleted);
            service.UpdatePRCompleted += new EventHandler<UpdatePRCompletedEventArgs>(service_UpdatePRCompleted);
            service.SaveandApproveCompleted += new EventHandler<SaveandApproveCompletedEventArgs>(service_SaveandApproveCompleted);
            service.SaveandInitialApproveCompleted += new EventHandler<SaveandInitialApproveCompletedEventArgs>(service_SaveandInitialApproveCompleted);
            service.SaveandVerifyCompleted += new EventHandler<SaveandVerifyCompletedEventArgs>(service_SaveandVerifyCompleted);
            service.EmailPRCompleted += new EventHandler<AsyncCompletedEventArgs>(service_EmailPRCompleted);
            service.ChangesStateofPRCompleted += new EventHandler<ChangesStateofPRCompletedEventArgs>(service_ChangesStateofPRCompleted);
            service.SaveandChangesStateofPRCompleted += new EventHandler<SaveandChangesStateofPRCompletedEventArgs>(service_SaveandChangesStateofPRCompleted);
            service.CloseMICompleted += new EventHandler<CloseMICompletedEventArgs>(service_CloseMICompleted);
            service.CopyPOfromMICompleted += new EventHandler<CopyPOfromMICompletedEventArgs>(service_CopyPOfromMICompleted);
            service.SubmitToHOPurchaseCompleted += new EventHandler<SubmitToHOPurchaseCompletedEventArgs>(service_SubmitToHOPurchaseCompleted);
            service.GetTotalTODispacthQtyCompleted += new EventHandler<GetTotalTODispacthQtyCompletedEventArgs>(service_GetTotalTODispacthQtyCompleted);
        }

        public void CalculateTODispatch(MPR pr)
        {
            foreach (var line in pr.PRLines)
            {
                GetTODispatchQty(line.ID, line.LineID, ConvertedPR.SiteID, line.ItemID, line.SizeID, line.BrandID, line.ConsumableType);
            }
        }

        public void GetTODispatchQty(int ID, int Lineid, int siteid, int itemid, int? sizeid, int? brandid, string Contype)
        {
            service.GetTotalTODispacthQtyAsync(ID, Lineid, siteid, itemid, sizeid, brandid, Contype);
            IsBusy = true;
        }

        public void SubmitToHOPurchase(MPR pr)
        {
            service.SubmitToHOPurchaseAsync(Base.Current.UserName, pr);
            IsBusy = true;
        }

        public void CopyPO(MPR mpr)
        {
            service.CopyPOfromMIAsync(Base.Current.UserName, mpr);
            IsBusy = true;
        }

        public void CloseMI(int PRID)
        {
            service.CloseMIAsync(Base.Current.UserName, PRID);
            IsBusy = true;
        }

        public void ChangeState(int prid, string state)
        {
            service.ChangesStateofPRAsync(prid, state);
            IsBusy = true;
        }

        public void SaveandChangeState(string state)
        {
            service.SaveandChangesStateofPRAsync(ConvertedPR, state);
            IsBusy = true;
        }

        public void GetPR(int prid)
        {
            //dynamic t;
            //t = 4;
            //t = "dsa";

            service.GetPRbyIDAsync(prid);
            IsBusy = true;
            IsLoading = true;
        }

        public void VerifyPR()
        {
            Status = "Verifying ...";
            if (!Validate()) return;
            //service.VerifyPRAsync(ConvertedPR.PRNumber, Base.Current.UserName, DateTime.UtcNow.AddHours(5.5), ConvertedPR.ApprovalRemarks);
            service.SaveandVerifyAsync(ConvertedPR, Base.Current.UserName);
            IsBusy = true;
            IsLoading = true;
        }

        public void InitiallyApprovePR()
        {
            Status = "Approving ...";
            if (!Validate()) return;
            //service.InitialApprovePRAsync(ConvertedPR.PRNumber, Base.Current.UserName, DateTime.UtcNow.AddHours(5.5), ConvertedPR.ApprovalRemarks);
            service.SaveandInitialApproveAsync(ConvertedPR, Base.Current.UserName);
            IsBusy = true;
            IsLoading = true;
        }

        public void ApprovePR()
        {
            Status = "Approving ...";
            if (!Validate()) return;
            //service.ApprovePRAsync(ConvertedPR.PRNumber, Base.Current.UserName, DateTime.UtcNow.AddHours(5.5), ConvertedPR.ApprovalRemarks);
            service.SaveandApproveAsync(ConvertedPR, Base.Current.UserName);
            IsBusy = true;
            IsLoading = true;
        }

        public void convertToPO()
        {
            service.ConvertToPOAsync(ConvertedPR);
            IsBusy = true;
            IsLoading = true;
        }

        public void Email()
        {
            service.EmailPRAsync(ConvertedPR.PRNumber);
            IsBusy = true;
        }

        public void UpdatePR()
        {
            Status = "Updating ...";
            if (!Validate()) return;
            service.UpdatePRAsync(ConvertedPR);
            IsBusy = true;
            IsLoading = true;
        }

        public void UpdatePRafterSplit(MPR convertedPR)
        {
            Status = "Updating ...";
            service.UpdatePRAsync(convertedPR);
            IsBusy = true;
            IsLoading = true;
        }

        public void NotifyAllCans()
        {
            foreach (var prop in this.GetType().GetProperties())
            {
                if (prop.Name.StartsWith("can", StringComparison.OrdinalIgnoreCase))
                    Notify(prop.Name);
            }
        }

        public void UpdateTotal()
        {
            ConvertedPR.UpdateTotalValue();
            Notify("ConvertedPR");
        }

        public void UpdateNetAmount()
        {
            ConvertedPR.UpdateNetAmount();
            Notify("ConvertedPR");
        }

        public void UpdateTotalDiscount()
        {
            ConvertedPR.UpdateTotalDiscount();
            Notify("ConvertedPR");
        }

        internal void SetID(int p)
        {
            GetPR(p);
            IsLoading = true;
        }

        public void AddNewLine()
        {
            ConvertedPR.AddNewLine();
            Notify("ConvertedPR");
        }

        public void DeleteLine(int lid)
        {
            try
            {
                var line = ConvertedPR.PRLines.Single(x => x.ID == lid);
                ConvertedPR.PRLines.Remove(line);
                UpdateTotal();
                ConvertedPR.UpdateLineNumbers();
            }
            catch { }
        }

        public bool Validate()
        {
            if (ConvertedPR.VendorID == 0) { Status = "Please Select Vendor"; return false; }
            if (ConvertedPR.SiteID == 0) { Status = "Please Select Site"; return false; }
            if (String.IsNullOrWhiteSpace(ConvertedPR.Purchaser)) { Status = "Purchaser Cannot be Empty!"; return false; }
            if (String.IsNullOrWhiteSpace(ConvertedPR.PurchaseType)) { Status = "Please Select Purchase Type"; return false; }
            if (ConvertedPR.PRLines == null) { Status = "Material Indent should have alteast one item."; return false; }
            if (ConvertedPR.PRLines.Count == 0) { Status = "Material Indent should have alteast one item."; return false; }
            if (ConvertedPR.PRLines.Any(x => x.ItemID == 0)) { Status = "Indent Lines has some invaid/unselected Items!"; return false; }
            return true;
        }

        #endregion

        #region COMPLETED EVENTS...

        void service_GetTotalTODispacthQtyCompleted(object sender, GetTotalTODispacthQtyCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                Base.ShowError(e.Error);
                IsBusy = false;
            }
            else
            {
                var result = e.Result;
                var to = ConvertedPR.PRLines.SingleOrDefault(x => x.ID == result.ID
                    && x.ItemID == result.ItemID
                    && x.SizeID == result.SizeID
                    && x.BrandID == result.BrandID);
                if (to != null)
                    to.TODispInfo.TODispQty = result.TODispQty;
                IsBusy = false;
            }
        }

        void service_SubmitToHOPurchaseCompleted(object sender, SubmitToHOPurchaseCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                Base.ShowError(e.Error);
                IsBusy = false;
            }
            else
            {
                CopiedMI = e.Result;
                MessageBox.Show("New PR Created Successfully !\n\nPR Reference No.: " + e.Result.ReferenceNo);
                RaiseMICopied(CopiedMI);
                IsBusy = false;
            }
        }

        void service_CopyPOfromMICompleted(object sender, CopyPOfromMICompletedEventArgs e)
        {
            if (e.Error != null)
            {
                Base.ShowError(e.Error);
                IsBusy = false;
            }
            else
            {
                CopiedPO = e.Result;
                MessageBox.Show("New PO Created Successfully !\n\nPO Reference No.: " + e.Result.POReferenceNo);
                IsBusy = false;
            }
        }

        void service_CloseMICompleted(object sender, CloseMICompletedEventArgs e)
        {
            if (e.Error != null)
            {
                Base.ShowError(e.Error);
                IsBusy = false;
            }
            else
            {
                ConvertedPR = e.Result;
                MessageBox.Show("MI Closed Successfully !");
                Status = ConvertedPR.PRStatus;
                Base.AppEvents.RaisePRDataChanged();
                IsBusy = false;
            }
        }

        void service_SaveandChangesStateofPRCompleted(object sender, SaveandChangesStateofPRCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                Base.ShowError(e.Error);
                IsBusy = false;
            }
            else
            {
                ConvertedPR = e.Result;
                MessageBox.Show("Material Indent State Changed to " + ConvertedPR.PRStatus + " !");
                Status = ConvertedPR.PRStatus;
                Base.AppEvents.RaisePRDataChanged();
                IsBusy = false;
            }
            IsLoading = false;
        }

        void service_ChangesStateofPRCompleted(object sender, ChangesStateofPRCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                Base.ShowError(e.Error);
                IsBusy = false;
            }
            else
            {
                ConvertedPR = e.Result;
                MessageBox.Show("Material Indent State Changed to " + ConvertedPR.PRStatus + " !");
                Status = ConvertedPR.PRStatus;
                Base.AppEvents.RaisePRDataChanged();
                IsBusy = false;
            }
            IsLoading = false;
        }

        void service_ApprovePRCompleted(object sender, ApprovePRCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                Base.ShowError(e.Error);
                IsBusy = false;
            }
            else
            {
                ConvertedPR = e.Result;
                MessageBox.Show("Material Indent Approved successfully !");
                Status = "PO Approved!";
                IsBusy = false;
            }
            IsLoading = false;
        }

        void service_InitialApprovePRCompleted(object sender, InitialApprovePRCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                Base.ShowError(e.Error);
                IsBusy = false;
            }
            else
            {
                ConvertedPR = e.Result;
                MessageBox.Show("Material Indent Approved successfully !");
                Base.AppEvents.RaisePRDataChanged();
                Status = "MI Approved!";
                IsBusy = false;
            }
            IsLoading = false;
        }

        void service_VerifyPRCompleted(object sender, VerifyPRCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                Base.ShowError(e.Error);
                IsBusy = false;
            }
            else
            {
                ConvertedPR = e.Result;
                MessageBox.Show("Material Indent Verified successfully !");
                Base.AppEvents.RaisePRDataChanged();
                Status = "MI Verified!";
                IsBusy = false;
            }
            IsLoading = false;
        }

        void service_ConvertToPOCompleted(object sender, ConvertToPOCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                Base.ShowError(e.Error);
                IsBusy = false;
            }
            else
            {
                IsBusy = false;
                MessageBox.Show("Successfully Converted to Purchase Order !");
                Base.AppEvents.RaisePOConverted(e.Result);
            }
            IsLoading = false;
        }

        void service_EmailPRCompleted(object sender, AsyncCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                Base.ShowError(e.Error);
                IsBusy = false;
            }
            else
            {
                IsBusy = false;
                MessageBox.Show("Email Sent Successfully!");
            }
        }

        void service_SaveandVerifyCompleted(object sender, SaveandVerifyCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                Base.ShowError(e.Error);
                IsBusy = false;
            }
            else
            {
                ConvertedPR = e.Result;
                Base.AppEvents.RaisePRDataChanged();
                Status = "MI Verified!";
                IsBusy = false;
            }
            IsLoading = false;
        }

        void service_SaveandInitialApproveCompleted(object sender, SaveandInitialApproveCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                Base.ShowError(e.Error);
                IsBusy = false;
            }
            else
            {
                ConvertedPR = e.Result;
                Base.AppEvents.RaisePRDataChanged();
                Status = "MI Approved!";
                IsBusy = false;
            }
            IsLoading = false;
        }

        void service_SaveandApproveCompleted(object sender, SaveandApproveCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                Base.ShowError(e.Error);
                IsBusy = false;
            }
            else
            {
                ConvertedPR = e.Result;
                Base.AppEvents.RaisePRDataChanged();
                Status = "PO Approved!";
                IsBusy = false;
            }
            IsLoading = false;
        }

        void service_UpdatePRCompleted(object sender, UpdatePRCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                Base.ShowError(e.Error);
                IsBusy = false;
            }
            else
            {
                if (!Validate()) return;
                ConvertedPR = e.Result;
                Base.AppEvents.RaisePRDataChanged();
                Status = "Saved!";
                IsBusy = false;
            }
            IsLoading = false;
        }

        void service_GetPRbyIDCompleted(object sender, GetPRbyIDCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                Base.ShowError(e.Error);
                IsBusy = false;
            }
            else
            {
                ConvertedPR = e.Result;
                if (ConvertedPR != null)
                    CalculateTODispatch(ConvertedPR);
                IsBusy = false;
            }
            IsLoading = false;
        }

        #endregion

        #region PROPERTIES...

        MPR _CopiedMI = new MPR();
        public MPR CopiedMI
        {
            get { return _CopiedMI; }
            set
            {
                _CopiedMI = value;
                Notify("CopiedMI");
            }
        }

        MPO _CopiedPO = new MPO();
        public MPO CopiedPO
        {
            get { return _CopiedPO; }
            set
            {
                _CopiedPO = value;
                Notify("CopiedPO");
            }
        }

        ObservableCollection<MSplittedLines> _SplittedLines = new ObservableCollection<MSplittedLines>();
        public ObservableCollection<MSplittedLines> SplittedLines
        {
            get { return _SplittedLines; }
            set
            {
                _SplittedLines = value;
                Notify("SplittedLines");
            }
        }

        MMOSplittedHeader _MOSplit = new MMOSplittedHeader();
        public MMOSplittedHeader MOSplit
        {
            get { return _MOSplit; }
            set
            {
                _MOSplit = value;
                Notify("MOSplit");
            }
        }

        MPR _ConvertedPR = new MPR();
        public MPR ConvertedPR
        {
            get { return _ConvertedPR; }
            set
            {
                _ConvertedPR = value;
                Notify("ConvertedPR");
                NotifyAllCans();
            }
        }

        bool _IsPR = false;
        public bool IsPR
        {
            get { return _IsPR; }
            set
            {
                _IsPR = value;
                Notify("IsPR");
            }
        }

        bool _IsPO = false;
        public bool IsPO
        {
            get { return _IsPO; }
            set
            {
                _IsPO = value;
                Notify("IsPO");
            }
        }

        string _Status;
        public string Status
        {
            get { return _Status; }
            set
            {
                _Status = value;
                Notify("Status");
            }
        }

        public override bool CanEdit
        {
            get { return !ConvertedPR.IsApproved && !ConvertedPR.IsClosed && (Base.Current.Rights.MI_AllRights || Base.Current.Rights.MI_CanEdit); }
        }

        public bool CanCloseMI
        {
            get
            {
                return !ConvertedPR.IsClosed;
            }
        }
        /*Base.Current.Rights.MI_CanCloseMI*/

        public bool CanEmailMI
        {
            get { return !ConvertedPR.IsClosed && Base.Current.Rights.MI_CanEmailMI; }
        }

        public bool CanSplittoMI
        {
            get { return Base.Current.Rights.MI_CanRaise; }
        }

        public bool CanEditItems
        {
            get { return !ConvertedPR.IsVerified && Base.Current.Rights.MI_CanEdit; }
        }

        public bool CanSubmitToMIVerify
        {
            get { return !ConvertedPR.IsVerified && (ConvertedPR.PRStatus == "Draft" || ConvertedPR.PRStatus == "MIRejected") && Base.Current.Rights.MI_CanSubmitForVerify; }
        }

        public bool CanMIVerify
        {
            get { return !ConvertedPR.IsVerified && ConvertedPR.PRStatus == "SubmittedForVerification" && Base.Current.Rights.MI_CanVerify; }
        }

        public bool CanSubmitToMIApprove
        {
            get { return ConvertedPR.IsVerified && (ConvertedPR.PRStatus == "MIVerified" || ConvertedPR.PRStatus == "MIRejected") && !ConvertedPR.IsReqApproved && Base.Current.Rights.MI_CanSubmitForMIApprove; }
        }

        public bool CanMIApprove
        {
            get { return ConvertedPR.IsVerified && ConvertedPR.PRStatus == "SubmittedForMIApproval" && !ConvertedPR.IsReqApproved && Base.Current.Rights.MI_CanApproveMI; }
        }

        public bool CanConvertToTO
        {
            get { return ConvertedPR.IsVerified && ConvertedPR.IsReqApproved && !ConvertedPR.IsClosed && (ConvertedPR.PRStatus == "MIApproved" || ConvertedPR.PRStatus == "SubmittedToPurchase" || ConvertedPR.PRStatus == "SubmittedToHOPurchase") && Base.Current.Rights.MI_CanConvertToTO; }
        }

        public bool CanSubmitToPurchase
        {
            get { return ConvertedPR.IsVerified && ConvertedPR.IsReqApproved && (ConvertedPR.PRStatus == "MIApproved") && Base.Current.Rights.MI_CanSubmitToPurchase; }
        }

        public bool CanEnterPrices
        {
            get { return ConvertedPR.IsVerified && ConvertedPR.IsReqApproved && ConvertedPR.PRStatus == "SubmittedToPurchase" && Base.Current.Rights.MI_CanSubmitForPOApprove; }
        }

        public bool CanViewPrices
        {
            get { return ConvertedPR.IsVerified && ConvertedPR.IsReqApproved && ConvertedPR.PRStatus != "SubmittedToPurchase" && Base.Current.Rights.MI_CanSubmitForPOApprove; }
        }

        public bool CanSubmitToPOApprove
        {
            get { return ConvertedPR.IsVerified && ConvertedPR.IsReqApproved && (ConvertedPR.PRStatus == "SubmittedToPurchase" || ConvertedPR.PRStatus == "PORejected") && Base.Current.Rights.MI_CanSubmitForPOApprove; }
        }

        public bool CanPOApprove
        {
            get { return ConvertedPR.IsVerified && ConvertedPR.IsReqApproved && ConvertedPR.PRStatus == "SubmittedForPOApproval" && !ConvertedPR.IsApproved && Base.Current.Rights.MI_CanApprovePO; }
        }

        public bool CanSave
        {
            get { return !ConvertedPR.IsApproved && !ConvertedPR.IsClosed && Base.Current.Rights.MI_CanEdit; }
        }

        public bool CanConvertToPO
        {
            get { return ConvertedPR.IsApproved && ConvertedPR.PRStatus == "POApproved" && !ConvertedPR.ConvertedToPO && Base.Current.Rights.MI_CanConvertToPO; }
        }

        public bool CanInsertApprovalRemarks
        {
            get
            {
                return (!ConvertedPR.IsVerified && ConvertedPR.PRStatus == "SubmittedForVerification" && Base.Current.Rights.MI_CanVerify)
                    || (!ConvertedPR.IsReqApproved && ConvertedPR.PRStatus == "SubmittedForMIApproval" && Base.Current.Rights.MI_CanApproveMI)
                    || (!ConvertedPR.IsApproved && ConvertedPR.PRStatus == "SubmittedForPOApproval" && Base.Current.Rights.MI_CanApprovePO)
                    && !ConvertedPR.ConvertedToPO && !ConvertedPR.IsClosed;
            }
        }

        bool _ApprovalStatus = false;
        public bool ApprovalStatus
        {
            get { return _ApprovalStatus; }
            set
            {
                _ApprovalStatus = value;
                Notify("ApprovalStatus");
            }
        }

        bool _IsLoading = false;
        public bool IsLoading
        {
            get { return _IsLoading; }
            set
            {
                _IsLoading = value;
                Notify("IsLoading");
            }
        }

        #endregion
    }

    
}