﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Collections.ObjectModel;
using SIMS.MM.MODEL;
using System.ComponentModel;
using Contruction.TransactionService;

namespace Contruction
{
    public class ViewCashPurchaseDetailsModel : ViewModel
    {
        TransactionServiceClient newservice = new TransactionServiceClient();

        public ViewCashPurchaseDetailsModel()
        {
            if (!DesignerProperties.GetIsInDesignMode(Application.Current.RootVisual))
            {
                OnInit();
            }
        }

        public void OnInit()
        {
            //service.GetCashPurchaseByIDCompleted += new EventHandler<GetCashPurchaseByIDCompletedEventArgs>(service_GetCashPurchaseByIDCompleted);
            newservice.GetCashPurchaseByIDCompleted += new EventHandler<TransactionService.GetCashPurchaseByIDCompletedEventArgs>(newservice_GetCashPurchaseByIDCompleted);
        }

        void newservice_GetCashPurchaseByIDCompleted(object sender, TransactionService.GetCashPurchaseByIDCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                Base.ShowError(e.Error);
                IsBusy = false;
            }
            else
            {
                NewTransaction = e.Result;
                IsBusy = false;
                IsOld = true;
            }
        }

        //void service_GetCashPurchaseByIDCompleted(object sender, GetCashPurchaseByIDCompletedEventArgs e)
        //{
        //    if (e.Error != null)
        //    {
        //        Base.ShowError(e.Error);
        //        IsBusy = false;
        //    }
        //    else
        //    {
        //        NewTransaction = e.Result;
        //        IsBusy = false;
        //        IsOld = true;
        //    }
        //}

        //MReceiving _NewTransaction = new MReceiving();
        //public MReceiving NewTransaction
        //{
        //    get { return _NewTransaction; }
        //    set
        //    {
        //        _NewTransaction = value;
        //        Notify("NewTransaction");
        //    }
        //}

        MCashPurchase _NewTransaction = new MCashPurchase();
        public MCashPurchase NewTransaction
        {
            get { return _NewTransaction; }
            set
            {
                _NewTransaction = value;
                Notify("NewTransaction");
            }
        }

        internal void SetID(int p)
        {
            //service.GetCashPurchaseByIDAsync(p);
            newservice.GetCashPurchaseByIDAsync(p);
            IsBusy = true;
        }

        bool _IsOld = false;
        public bool IsOld
        {
            get { return _IsOld; }
            set
            {
                _IsOld = value;
                Notify("IsOld");
                Notify("CanEdit");
            }
        }

        public override bool CanEdit
        {
            get
            {
                return !IsOld;
            }
        }

        public bool CanPrint
        {
            get
            {
                return true;
            }
        }
    }
}
