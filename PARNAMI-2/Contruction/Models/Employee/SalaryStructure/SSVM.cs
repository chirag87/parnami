﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.ComponentModel;
using Contruction.SSService;
using System.Collections.ObjectModel;
using SIMS.EM.Model;

namespace Contruction.Models.Employee.SalaryStructure
{
    public class SSVM:ViewModel
    {
        SalaryStructureServiceClient proxy = new SalaryStructureServiceClient(); 
        public SSVM()
        {
            if (!DesignerProperties.GetIsInDesignMode(Application.Current.RootVisual))
            {
                OnInit();
            }
        }

        private void OnInit()
        {
            LoadAllSS();
            proxy.GetAllSalaryStructuresCompleted += new EventHandler<GetAllSalaryStructuresCompletedEventArgs>(proxy_GetAllSalaryStructuresCompleted);
            proxy.GetSalaryStructureByIDCompleted += new EventHandler<GetSalaryStructureByIDCompletedEventArgs>(proxy_GetSalaryStructureByIDCompleted);
        }

        void proxy_GetSalaryStructureByIDCompleted(object sender, GetSalaryStructureByIDCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                IsBusy = false;
                Base.ShowError(e.Error);
            }
            else
            {
                IsBusy = false;
                SS = e.Result;
            }
        }

        void proxy_GetAllSalaryStructuresCompleted(object sender, GetAllSalaryStructuresCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                IsBusy = false;
                Base.ShowError(e.Error);
            }
            else
            {
                IsBusy = false;
                AllSalaryStructures = e.Result;
            }
        }

        public void LoadAllSS()
        {
            IsBusy=true;
            proxy.GetAllSalaryStructuresAsync();
        }

        ObservableCollection<MSalaryStructure> _AllSalaryStructures=new ObservableCollection<MSalaryStructure>();
        public ObservableCollection<MSalaryStructure> AllSalaryStructures
        {
            get { return _AllSalaryStructures; }
            set
            {
                _AllSalaryStructures = value;
                Notify("AllSalaryStructures");
            }
        }

        MSalaryStructure _SS=new MSalaryStructure();
        public MSalaryStructure SS
        {
            get { return _SS; }
            set
            {
                _SS = value;
                Notify("SS");
            }
        }

        internal void LoadEmployeeSS(int EmpID)
        {
            proxy.GetSalaryStructureByIDAsync(EmpID);
        }
    }
}
