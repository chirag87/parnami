﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.ComponentModel;
using Contruction.Srv_Employee;
using SIMS.EM.Model;
using Contruction.Views.Employee;

namespace Contruction.Models.Employee
{
    public class NewEmployeeVM:ViewModel
    {
        EmployeeServiceClient proxy = new EmployeeServiceClient();
        public NewEmployeeVM()
        {
            if (!DesignerProperties.GetIsInDesignMode(Application.Current.RootVisual))
            {
                OnInit();
            }
        }

        private void OnInit()
        {
            proxy.CreateNewEmployeeCompleted += new EventHandler<CreateNewEmployeeCompletedEventArgs>(proxy_CreateNewEmployeeCompleted);
            proxy.GetEmployeeByIDCompleted += new EventHandler<GetEmployeeByIDCompletedEventArgs>(proxy_GetEmployeeByIDCompleted);
            Base.EmployeeEvents.NewEmployeeAdded += new EventHandler(EmployeeEvents_NewEmployeeAdded);
        }

        void EmployeeEvents_NewEmployeeAdded(object sender, EventArgs e)
        {
            Base.Redirect(new EmployeeMaster());
        }

        void proxy_GetEmployeeByIDCompleted(object sender, GetEmployeeByIDCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                IsBusy = false;
                Base.ShowError(e.Error);
            }
            else
            {
                IsBusy = false;
                NewEmployee = e.Result;
            }
        }

        void proxy_CreateNewEmployeeCompleted(object sender, CreateNewEmployeeCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                IsBusy = false;
                Base.ShowError(e.Error);                
            }
            else
            {
                IsBusy = false;
                MessageBox.Show("                  ID:" + e.Result.ID + "\n" + "All Changes Successfullty Saved");
                if (IsFromAddNewForm)
                    Base.EmployeeEvents.RaiseNewEmployeeAdded();
            }
        }

        MEmployee _NewEmployee=new MEmployee();
        public MEmployee NewEmployee
        {
            get { return _NewEmployee; }
            set
            {
                _NewEmployee = value;
                Notify("NewEmployee");
            }
        }

        bool _IsFromAddNewForm = false;
        public bool IsFromAddNewForm
        {
            get { return _IsFromAddNewForm; }
            set
            {
                _IsFromAddNewForm = value;
                Notify("IsFromAddNewForm");
            }
        }

        string _Status;
        public string Status
        {
            get { return _Status; }
            set
            {
                _Status = value;
                Notify("Status");
            }
        }

        public enum Gender
        {
            Male,
            Female
        }

        public string[] Gnder
        {
            get
            {
                return new string[]
                { 
                    "Male",
                    "Female"
                };
            }
        }

        public void SubmitToDb()
        {
            if (!Validate()) return;
            Notify("NewLabour");
            IsBusy = true;
            proxy.CreateNewEmployeeAsync(NewEmployee);
        }

        internal void GetEmployeeByID(int _EmpID)
        {
            IsBusy = true;
            proxy.GetEmployeeByIDAsync(_EmpID);
        }

        private bool Validate()
        {
            Status = "";

            if (String.IsNullOrWhiteSpace(NewEmployee.FirstName)) { Status = "Enter the First Name"; return false; }
            //if (NewEmployee.TypeID == 0) { Status = "Please specify Type of Labour!"; return false; }
            if (NewEmployee.BUID== 0) { Status = "Please Select Site for Employee!"; return false; }
            //if (NewEmployee.CurrentRate == 0 || NewEmployee.CurrentRate == null) { Status = "Enter Current Rate For Labour"; return false; }
            //if (!Regex.IsMatch(NewLabour.LastName, @"^[a-zA-Z]+$", RegexOptions.IgnoreCase)) { Status = "Last Name should only contain alphabets!"; return false; }
            return true;
        }

    }
}
