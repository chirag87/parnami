﻿using System;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using SIMS.MM.MODEL;

namespace Contruction
{
    using SrvLogin;
    using System.Collections.ObjectModel;
    using System.Collections;
    using System.Collections.Generic;
    public class UserManagementModel : ViewModel
    {
        public LoginServiceClient service = new LoginServiceClient();
        public UserManagementModel()
        {
            service.GetAllUsersInfoCompleted += new EventHandler<GetAllUsersInfoCompletedEventArgs>(service_GetAllUsersInfoCompleted);
            service.AllowUserCompleted += new EventHandler<AllowUserCompletedEventArgs>(service_AllowUserCompleted);
            service.BlockUserCompleted += new EventHandler<BlockUserCompletedEventArgs>(service_BlockUserCompleted);
            service.AddRolesToUserCompleted += new EventHandler<AddRolesToUserCompletedEventArgs>(service_AddRolesToUserCompleted);
            service.RemoveRolesToUserCompleted += new EventHandler<RemoveRolesToUserCompletedEventArgs>(service_RemoveRolesToUserCompleted);
            service.GetAllRolesCompleted += new EventHandler<GetAllRolesCompletedEventArgs>(service_GetAllRolesCompleted);
            service.AddSitesToUserCompleted += new EventHandler<AddSitesToUserCompletedEventArgs>(service_AddSitesToUserCompleted);
            service.RemoveSitesToUserCompleted += new EventHandler<RemoveSitesToUserCompletedEventArgs>(service_RemoveSitesToUserCompleted);
            service.CreateNewUserCompleted += new EventHandler<System.ComponentModel.AsyncCompletedEventArgs>(service_CreateNewUserCompleted);
        }

        void service_CreateNewUserCompleted(object sender, System.ComponentModel.AsyncCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                Base.ShowError(e.Error);
                IsBusy = false;
            }
            else
            {
                IsBusy = false;
                MessageBox.Show("User Added Succesfully!");
            }
        }

        void service_RemoveSitesToUserCompleted(object sender, RemoveSitesToUserCompletedEventArgs e)
        {
            if (e.Error == null)
            {
                AllUsers = e.Result;
                IsBusy = false;
            }
            else
            {
                Base.ShowError(e.Error);
                IsBusy = false;
            }
        }

        void service_AddSitesToUserCompleted(object sender, AddSitesToUserCompletedEventArgs e)
        {
            if (e.Error == null)
            {
                AllUsers = e.Result;
                IsBusy = false;
            }
            else
            {
                Base.ShowError(e.Error);
                IsBusy = false;
            }
        }

        void service_GetAllRolesCompleted(object sender, GetAllRolesCompletedEventArgs e)
        {
            if (e.Error == null)
            {
                AllRoles = e.Result;
                IsBusy = false;
            }
            else
            {
                Base.ShowError(e.Error);
                IsBusy = false;
            }
        }

        void service_RemoveRolesToUserCompleted(object sender, RemoveRolesToUserCompletedEventArgs e)
        {
            if (e.Error == null)
            {
                AllUsers = e.Result;
                IsBusy = false;
            }
            else
            {
                Base.ShowError(e.Error);
                IsBusy = false;
            }
        }

        void service_AddRolesToUserCompleted(object sender, AddRolesToUserCompletedEventArgs e)
        {
            if (e.Error == null)
            {
                AllUsers = e.Result;
                IsBusy = false;
            }
            else
            {
                Base.ShowError(e.Error);
                IsBusy = false;
            }
        }

        void service_BlockUserCompleted(object sender, BlockUserCompletedEventArgs e)
        {
            if (e.Error == null)
            {
                AllUsers = e.Result;
                IsBusy = false;
            }
            else
            {
                Base.ShowError(e.Error);
                IsBusy = false;
            }
        }

        void service_AllowUserCompleted(object sender, AllowUserCompletedEventArgs e)
        {
            if (e.Error == null)
            {
                AllUsers = e.Result;
                IsBusy = false;
            }
            else
            {
                Base.ShowError(e.Error);
                IsBusy = false;
            }
        }

        void service_GetAllUsersInfoCompleted(object sender, GetAllUsersInfoCompletedEventArgs e)
        {
            if (e.Error == null)
            {
                AllUsers = e.Result;
                //foreach (var user in AllUsers)    //For ExtAutoCompleteBox as it accepts only String Type
                //{
                //    Base.Current.UserNames.Add(user.Username);
                //}

                IsBusy = false;
            }
            else
            {
                Base.ShowError(e.Error);
                IsBusy = false;
            }
        }

        ObservableCollection<UserInfo> _AllUsers = new ObservableCollection<UserInfo>();
        public ObservableCollection<UserInfo> AllUsers
        {
            get { return _AllUsers; }
            set
            {
                _AllUsers = value;
                Notify("AllUsers");
            }
        }

        ObservableCollection<string> _AllRoles;
        public ObservableCollection<string> AllRoles
        {
            get { return _AllRoles; }
            set
            {
                _AllRoles = value;
                Notify("AllRoles");
            }
        }

        public void GetAll()
        {
            service.GetAllUsersInfoAsync();
            service.GetAllRolesAsync();
            IsBusy = true;
        }

        public void AddRole(string role)
        {
            //
        }

        UserInfo selectedUser;
        public UserInfo SelectedUser
        {
            get { return selectedUser; }
            set
            {
                selectedUser = value;
                Notify("SelectedUser");
            }
        }

        UserInfo _NewUser = new UserInfo();
        public UserInfo NewUser
        {
            get { return _NewUser; }
            set
            {
                _NewUser = value;
                Notify("NewUser");
            }
        }

        public void ApproveSelected()
        {
            if (SelectedUser == null)
                return;
            service.AllowUserAsync(SelectedUser.Username);
            IsBusy = true;
        }

        public void BlockSelected()
        {
            if (SelectedUser == null)
                return;
            service.BlockUserAsync(SelectedUser.Username);
            IsBusy = true;
        }

        public void AddRolesToUser(IEnumerable<string> roles)
        {
            ObservableCollection<string> oRoles = new ObservableCollection<string>();
            roles.ToList().ForEach(x => oRoles.Add(x));
            service.AddRolesToUserAsync(selectedUser.Username, oRoles);
            IsBusy = true;
        }

        public void RemoveRolesToUser(IEnumerable<string> roles)
        {
            ObservableCollection<string> oRoles = new ObservableCollection<string>();
            roles.ToList().ForEach(x => oRoles.Add(x));
            service.RemoveRolesToUserAsync(selectedUser.Username, oRoles);
            IsBusy = true;
        }

        internal void AddSitesToUser(IEnumerable<int> list)
        {
            ObservableCollection<int> sites = new ObservableCollection<int>();
            list.ToList().ForEach(x => sites.Add(x));
            service.AddSitesToUserAsync(selectedUser.Username, sites);
            IsBusy = true;
        }

        internal void RemoveSitesToUser(IEnumerable<int> list)
        {
            ObservableCollection<int> sites = new ObservableCollection<int>();
            list.ToList().ForEach(x => sites.Add(x));
            service.RemoveSitesToUserAsync(selectedUser.Username, sites);
            IsBusy = true;
        }

        internal void SubmitToDB()
        {
            service.CreateNewUserAsync(NewUser.Username, NewUser.Password, NewUser.Email);
            IsBusy = true;
        }

        internal void Reset()
        {
            NewUser = new UserInfo();
        }

        internal bool ConfirmPassword(string str1, string str2)
        {
            if (str1 != str2)
            {
                MessageBoxResult result = MessageBox.Show("Passwords does not match!");
                return false;
            }
            return true;
        }
    }
}
