﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using SIMS.MM.MODEL;

namespace Contruction
{
    public partial class VendorAllocationByItem : UserControl
    {
        public VendorAllocationByItem()
        {
            InitializeComponent();
        }

        private void btnAddNew_Click(object sender, RoutedEventArgs e)
        {
            NewVendorAllocation all = new NewVendorAllocation();
            all.Show();
        }

        private void tbxVendor_TextChanged(object sender, TextChangedEventArgs e)
        {
            var box = sender as TextBox;
            Base.Current.SearchVendor = box.Text;
        }

        private void VendorsList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var list = sender as ListBox;
            var data = (MVendor)list.SelectedItem;
            tbxVendor.Text = data.Name;
            //Model.SelectedPrice.VendorID = data.ID;
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            tbxVendor.Focus();
        }
    }
}
