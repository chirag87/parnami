﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using SIMS.MM.MODEL;
using SIMS.Core;

namespace Contruction
{
    public partial class AllReceivings : UserControl
    {
        public ReceivingViewModel Model
        {
            get { return (ReceivingViewModel)this.DataContext; }
        }
				
        public AllReceivings()
        {
            InitializeComponent();
            Model.GRNChanged += new EventHandler(Model_GRNChanged);
        }

        void Model_GRNChanged(object sender, EventArgs e)
        {
            myPager.PagedData = (IPagination)Model.GRNs;
        }

        private void HyperlinkButton_Click(object sender, RoutedEventArgs e)
        {

        }

        private void Image_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            var img = sender as Image;
            var grnheader = (MGRN)img.DataContext;
            if (grnheader == null) return;
            Base.AppEvents.RaiseGRNRequested(grnheader.ID);
            var popup = new GRNsDetailedView();
            popup.SetID(grnheader.ID);
            popup.CreatePopup();
            //GRNsDetailedView gdv = new GRNsDetailedView();
            //gdv.Show();
        }

        private void dataGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var SelectedRow = (MGRN)(sender as DataGrid).SelectedItem;
            if (SelectedRow == null) return;
        }

        private void myPager_MoveToPage(object sender, Controls.MyPageChangedEventArgs e)
        {
            Model.PageIndex = e.RequestedPage;
        }

        private void SearchKey_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                Model.SearchKey = SearchKey.Text;
                Model.LoadGRNs();
            }
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            SearchKey.Focus();
        }
    }
}
