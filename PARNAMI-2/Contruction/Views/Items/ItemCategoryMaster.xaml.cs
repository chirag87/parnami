﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using SIMS.MM.MODEL;
using SyedMehrozAlam.CustomControls;
using System.ComponentModel;
using System.Collections.ObjectModel;

namespace Contruction
{
    				
    public partial class ItemCategory : UserControl, INotifyPropertyChanged
    {
        public NewItemModel Model
        {
            get { return (NewItemModel)this.DataContext; }
        }
				
        public ItemCategory()
        {
            InitializeComponent();
            this.Loaded += new RoutedEventHandler(ItemCategory_Loaded);
        }

        void ItemCategory_Loaded(object sender, RoutedEventArgs e)
        {
            this.Model.ItemCategoriesLoaded += new EventHandler(Model_ItemCategoriesLoaded);
            this.ItemCategoryViewer.ItemCategorySelected += new EventHandler(ItemCategoryViewer_ItemCategorySelected);
            this.Model.LeafAdded += new EventHandler(Model_LeafAdded);
            this.Model.LoadItemCategories();
        }

        void Model_LeafAdded(object sender, EventArgs e)
        {
            // Need to See What to Do???
        }

        void ItemCategoryViewer_ItemCategorySelected(object sender, EventArgs e)
        {
            var data = this.ItemCategoryViewer.SelectedItemCategory;
            if(data.CanHaveItem)
            {
                Model.ParentItemCategory = data;
                Model.SingleCategoryName = data.Name;
                this.Model.LoadLeafCategories(data.ID);
            }
        }

        void Model_ItemCategoriesLoaded(object sender, EventArgs e)
        {
            this.ItemCategoryViewer.ItemCategories = Model.AllItemCategories;
        }

        private void ItemsList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            //var list = sender as ListBox;
            //var data = (MItem)list.SelectedItem;
            //if (data == null) return;
            //itemdetail.SelectedItem = data;
        }

        private void btnAddNew_Click(object sender, RoutedEventArgs e)
        {
            var box = sender as Button;
            var data = box.DataContext;
            AddNewLeafCategory anic = new AddNewLeafCategory();            
            anic.DataContext = data;
            anic.Show();
        }

        MItemCategory _Leaf = new MItemCategory();
        public MItemCategory Leaf
        {
            get { return _Leaf; }
            set
            {
                _Leaf = value;
                Notify("Leaf");
            }
        }

        private void dgdItems_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            Model.IsNew = false;
            Model.IsDetailViewVisible = true;
            var grid = sender as DataGrid;
            var data = (MItem)grid.SelectedItem;

            Model.SelectedItem = new MItem();
            Model.SelectedItem = data;
        }

        private void hpbtnAdd_Click(object sender, RoutedEventArgs e)
        {
            Model.IsNew = true;
            var hbtn= sender as HyperlinkButton;
            var data=(NewItemModel)hbtn.DataContext;
            Model.IsDetailViewVisible = true;
            Model.SelectedItem = new MItem();
        }

        #region INotifyPropertyChanged...
        public event PropertyChangedEventHandler PropertyChanged;
        public void Notify(string propertyName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion
    }
}