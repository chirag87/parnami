﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.ComponentModel;

namespace Contruction
{
	public partial class ShowItemParents : UserControl
	{
        public ShowItemParentsVM Model
        {
            get { return (ShowItemParentsVM)this.DataContext; }
        }
				
		public ShowItemParents()
		{
			InitializeComponent();
            Model.DataLoaded += new EventHandler(Model_DataLoaded);
		}

        void Model_DataLoaded(object sender, EventArgs e)
        {
            ParentList.ItemsSource = Model.OrderedParents;
        }

        public int itemid
        {
            get { return Model.itemid; }
            set
            {
                Model.itemid = value;
            }
        }
    }
}