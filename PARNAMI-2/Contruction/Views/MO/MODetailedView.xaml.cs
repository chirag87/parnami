﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using SIMS.MM.MODEL;
using System.IO;

namespace Contruction
{
    public partial class MODetailedView : ChildWindow
    {
        public MODetailedViewModel Model
        {
            get { return (MODetailedViewModel)this.DataContext; }
        }

        public MODetailedView()
        {
            InitializeComponent();
        }

        public MODetailedView(int moid)
            : this()
        {
            Model.GetMO(moid);
            Model.GetMOReleasingsAgainstMO(moid);
            Model.GetMOReceivingsAgainstMO(moid);
        }

        private void btnConfirm_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

        private void btnClose_Click(object sender, RoutedEventArgs e)
        {
            var data = sender as Button;
            var mo = (MODetailedViewModel)data.DataContext;
            if (mo == null) return;
            Model.CloseMO(mo.ConvertedMO.ID);
            this.DialogResult = false;
        }

        private void ChildWindow_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Escape)
            {
                this.DialogResult = false;
            }
        }

        private void InfoIcon_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            var img = sender as Image;
            var data = (MMOLine)img.DataContext;
            if (data.ItemID == 0) return;
            var popup = new ItemSummary(data.ItemID, data.SizeID, data.BrandID);
            popup.CreatePopup();
        }

        private void btnCombine_Click(object sender, RoutedEventArgs e)
        {
            var data = sender as Button;
            var mo = (MODetailedViewModel)data.DataContext;
            if (mo == null) return;
            Model.CombineMO(mo.ConvertedMO.ID);
        }

        SaveFileDialog sfd;

        private void downloadTO_Click(object sender, RoutedEventArgs e)
        {
            Model.IsBusy = true;
            sfd = new SaveFileDialog();
            sfd.Filter = "PDF file format|*.pdf";
            if ((bool)sfd.ShowDialog())
            {
                var hp = sender as HyperlinkButton;
                var data = (MODetailedViewModel)hp.DataContext;

                WebClient webClient = new WebClient();
                webClient.OpenReadCompleted += (s, args) =>
                {
                    StreamReader sr = new StreamReader(args.Result);
                    byte[] bytes = new byte[args.Result.Length];
                    // revisit Here
                    args.Result.Read(bytes, 0, (int)args.Result.Length);
                    Stream st = sfd.OpenFile();
                    st.Write(bytes, 0, (int)args.Result.Length);
                    st.Close();
                    MessageBox.Show("DOWNLOAD COMPLETE!" + "\nFile Name: " + sfd.SafeFileName);
                    Model.IsBusy = false;
                };
                Uri uri = new Uri("http://" + Base.Current.AppSettings.ServerPath + "/GetReport.ashx?rtype=pdf&type=TO&id=" + data.ConvertedMO.ID.ToString(), UriKind.Absolute);
                webClient.OpenReadAsync(uri);
            }
        }

        private void downloadTO_to_EXCEL_Click(object sender, RoutedEventArgs e)
        {
            Model.IsBusy = true;
            sfd = new SaveFileDialog();
            sfd.Filter = "EXCEL file format|*.xls";
            if ((bool)sfd.ShowDialog())
            {
                var hp = sender as Button;
                var data = (MODetailedViewModel)hp.DataContext;

                WebClient webClient = new WebClient();
                webClient.OpenReadCompleted += (s, args) =>
                {
                    StreamReader sr = new StreamReader(args.Result);
                    byte[] bytes = new byte[args.Result.Length];
                    // revisit Here
                    args.Result.Read(bytes, 0, (int)args.Result.Length);
                    Stream st = sfd.OpenFile();
                    st.Write(bytes, 0, (int)args.Result.Length);
                    st.Close();
                    MessageBox.Show("DOWNLOAD COMPLETE!" + "\nFile Name: " + sfd.SafeFileName);
                    Model.IsBusy = false;
                };
                Uri uri = new Uri("http://" + Base.Current.AppSettings.ServerPath + "/GetReport.ashx?rtype=excel&type=TO&id=" + data.ConvertedMO.ID.ToString(), UriKind.Absolute);
                webClient.OpenReadAsync(uri);
            }
        }

        private void downloadReceive_Click(object sender, RoutedEventArgs e)
        {
            Model.IsBusy = true;
            sfd = new SaveFileDialog();
            sfd.Filter = "PDF file format|*.pdf";
            if ((bool)sfd.ShowDialog())
            {
                var hp = sender as HyperlinkButton;
                var data = (MMOReceiving)hp.DataContext;

                WebClient webClient = new WebClient();
                webClient.OpenReadCompleted += (s, args) =>
                {
                    StreamReader sr = new StreamReader(args.Result);
                    byte[] bytes = new byte[args.Result.Length];
                    // revisit Here
                    args.Result.Read(bytes, 0, (int)args.Result.Length);
                    Stream st = sfd.OpenFile();
                    st.Write(bytes, 0, (int)args.Result.Length);
                    st.Close();
                    MessageBox.Show("DOWNLOAD COMPLETE!" + "\nFile Name: " + sfd.SafeFileName);
                    Model.IsBusy = false;
                };
                if (data.ConsumableType == "Consumable")
                {
                    Uri uri = new Uri("http://" + Base.Current.AppSettings.ServerPath + "/GetReport.ashx?rtype=pdf&type=TOReceiveC&id=" + data.ID.ToString(), UriKind.Absolute);
                    webClient.OpenReadAsync(uri);
                }
                else
                {
                    Uri uri = new Uri("http://" + Base.Current.AppSettings.ServerPath + "/GetReport.ashx?rtype=pdf&type=TOReceiveR&id=" + data.ID.ToString(), UriKind.Absolute);
                    webClient.OpenReadAsync(uri);
                }
            }
        }

        private void TOReceive_to_EXCEL_Click(object sender, RoutedEventArgs e)
        {
            Model.IsBusy = true;
            sfd = new SaveFileDialog();
            sfd.Filter = "EXCEL file format|*.xls";
            if ((bool)sfd.ShowDialog())
            {
                var hp = sender as Button;
                var data = (MMOReceiving)hp.DataContext;

                WebClient webClient = new WebClient();
                webClient.OpenReadCompleted += (s, args) =>
                {
                    StreamReader sr = new StreamReader(args.Result);
                    byte[] bytes = new byte[args.Result.Length];
                    // revisit Here
                    args.Result.Read(bytes, 0, (int)args.Result.Length);
                    Stream st = sfd.OpenFile();
                    st.Write(bytes, 0, (int)args.Result.Length);
                    st.Close();
                    MessageBox.Show("DOWNLOAD COMPLETE!" + "\nFile Name: " + sfd.SafeFileName);
                    Model.IsBusy = false;
                };
                if (data.ConsumableType == "Consumable")
                {
                    Uri uri = new Uri("http://" + Base.Current.AppSettings.ServerPath + "/GetReport.ashx?rtype=excel&type=TOReceiveC&id=" + data.ID.ToString(), UriKind.Absolute);
                    webClient.OpenReadAsync(uri);
                }
                else
                {
                    Uri uri = new Uri("http://" + Base.Current.AppSettings.ServerPath + "/GetReport.ashx?rtype=excel&type=TOReceiveR&id=" + data.ID.ToString(), UriKind.Absolute);
                    webClient.OpenReadAsync(uri);
                }
            }
        }

        private void downloadDispatch_Click(object sender, RoutedEventArgs e)
        {
            Model.IsBusy = true;
            sfd = new SaveFileDialog();
            sfd.Filter = "PDF file format|*.pdf";
            if ((bool)sfd.ShowDialog())
            {
                var hp = sender as HyperlinkButton;
                var data = (MMOReleasing)hp.DataContext;

                WebClient webClient = new WebClient();
                webClient.OpenReadCompleted += (s, args) =>
                {
                    StreamReader sr = new StreamReader(args.Result);
                    byte[] bytes = new byte[args.Result.Length];
                    // revisit Here
                    args.Result.Read(bytes, 0, (int)args.Result.Length);
                    Stream st = sfd.OpenFile();
                    st.Write(bytes, 0, (int)args.Result.Length);
                    st.Close();
                    MessageBox.Show("DOWNLOAD COMPLETE!" + "\nFile Name: " + sfd.SafeFileName);
                    Model.IsBusy = false;
                };
                if (data.ConsumableType == "Consumable")
                {
                    Uri uri = new Uri("http://" + Base.Current.AppSettings.ServerPath + "/GetReport.ashx?rtype=pdf&type=TODispatchC&id=" + data.ID.ToString(), UriKind.Absolute);
                    webClient.OpenReadAsync(uri);
                }
                else
                {
                    Uri uri = new Uri("http://" + Base.Current.AppSettings.ServerPath + "/GetReport.ashx?rtype=pdf&type=TODispatchR&id=" + data.ID.ToString(), UriKind.Absolute);
                    webClient.OpenReadAsync(uri);
                }
            }
        }

        private void TODispatch_to_EXCEL_Click(object sender, RoutedEventArgs e)
        {
            Model.IsBusy = true;
            sfd = new SaveFileDialog();
            sfd.Filter = "EXCEL file format|*.xls";
            if ((bool)sfd.ShowDialog())
            {
                var hp = sender as Button;
                var data = (MMOReleasing)hp.DataContext;

                WebClient webClient = new WebClient();
                webClient.OpenReadCompleted += (s, args) =>
                {
                    StreamReader sr = new StreamReader(args.Result);
                    byte[] bytes = new byte[args.Result.Length];
                    // revisit Here
                    args.Result.Read(bytes, 0, (int)args.Result.Length);
                    Stream st = sfd.OpenFile();
                    st.Write(bytes, 0, (int)args.Result.Length);
                    st.Close();
                    MessageBox.Show("DOWNLOAD COMPLETE!" + "\nFile Name: " + sfd.SafeFileName);
                    Model.IsBusy = false;
                };
                if (data.ConsumableType == "Consumable")
                {
                    Uri uri = new Uri("http://" + Base.Current.AppSettings.ServerPath + "/GetReport.ashx?rtype=excel&type=TODispatchC&id=" + data.ID.ToString(), UriKind.Absolute);
                    webClient.OpenReadAsync(uri);
                }
                else
                {
                    Uri uri = new Uri("http://" + Base.Current.AppSettings.ServerPath + "/GetReport.ashx?rtype=excel&type=TODispatchR&id=" + data.ID.ToString(), UriKind.Absolute);
                    webClient.OpenReadAsync(uri);
                }
            }
        }
    }
}