﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Contruction.Views.Employee.SalaryStructure;
namespace Contruction.Views.Employee
{
    
    public partial class NewEmpTabView : UserControl
    {
        public NewEmpTabView()
        {
            InitializeComponent();
        }
        public NewEmpTabView(int EmpID,bool HasSS)
            : this()
        {
            tbiNewEmp.Content = new NewEmployee(EmpID);
            tbiSS.Content = new EmployeeSS(EmpID);
        }

        public NewEmpTabView(bool flag)
            : this()
        {
            tbiNewEmp.Content = new NewEmployee(flag);
            tbiSS.Content = new EmployeeSS();
        }

        public NewEmpTabView(int EmpID,bool HasSS, bool flag)
            : this()
        {
            tbiNewEmp.Content = new NewEmployee(true);
            tbiSS.Content = new EmployeeSS(EmpID);
        }
    }
}
