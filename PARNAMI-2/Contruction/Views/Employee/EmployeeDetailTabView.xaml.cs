﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Contruction.Views.Employee.SalaryStructure;

namespace Contruction.Views.Employee
{
    public partial class EmployeeDetailTabView : UserControl
    {
        public EmployeeDetailTabView()
        {
            InitializeComponent();
            AttachEventhandler();
        }
        public EmployeeDetailTabView(int EmpID, bool HasSS)
            : this()
        {
            AttachEventhandler();
            this.EmpID = EmpID;
            this.HasSS = HasSS;
            tbiEmployeeDetail.Content = new NewEmployee(EmpID);
            if (HasSS)
                tbiSS.Content = new EmployeeSSDetail(EmpID,true);
            else
                tbiSS.Content = new EmployeeSS(EmpID,true,true);
        }

        public EmployeeDetailTabView(bool flag)
            : this()
        {
            AttachEventhandler();
            this.flag = flag;
            tbiEmployeeDetail.Content = new NewEmployee(flag);
            //tbiSS.Content = new EmployeeSS();
        }

        public EmployeeDetailTabView(int EmpID, bool HasSS, bool flag)
            : this()
        {
            AttachEventhandler();
            this.EmpID = EmpID;
            this.HasSS = HasSS;
            this.flag = flag;
            tbiEmployeeDetail.Content = new NewEmployee(EmpID);
            if (HasSS)
                tbiSS.Content = new EmployeeSSDetail(EmpID,flag);
            //else
            //    tbiSS.Content = new EmployeeSS(EmpID,true,true);
        }

        internal void AttachEventhandler()
        {
            Base.EmployeeEvents.SSShifted+=new EventHandler<SSShiftArgs>(EmployeeEvents_SSShifted);
        }

        void  EmployeeEvents_SSShifted(object sender, SSShiftArgs e)
        {
 	        if(e.IsFromEditPage)
                tbiSS.Content = new EmployeeSSDetail(this.EmpID, this.flag);
            else
                tbiSS.Content = new EmployeeSS(this.EmpID, this.flag);
        }

        public int EmpID { get; set; }

        public bool HasSS { get; set; }

        public bool flag { get; set; }
    }
}
