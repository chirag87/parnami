﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Contruction.Models.Employee;
using SIMS.EM.Model;
using SIMS.Core;

namespace Contruction.Views.Employee
{
    public partial class EmployeeAttendance : UserControl
    {
        public EmployeeMasterVM Model
        {
            get { return (EmployeeMasterVM)this.DataContext; }
        }

        public EmployeeAttendance()
        {
            InitializeComponent();
        }

        private void myPager_MoveToPage(object sender, Controls.MyPageChangedEventArgs e)
        {
            myPager.PagedData = (IPagination)Model.AllEmployees;
        }

        private void tbxSearch_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                Model.SearchKey = tbxSearch.Text;
                Model.LoadALlEmoloyees();
            }
            tbxSearch.Focus();
        }

        private void tbxSearch_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (tbxSearch.Text != "")
            {
                Model.SearchKey = tbxSearch.Text;
                Model.IsVisibleSearchBoxDbtn = true;
            }
            else
            {
                Model.IsVisibleSearchBoxDbtn = false;
            }
        }

        private void imgSearchBox_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            Model.SearchKey = null;
        }

        private void btnNew_Click(object sender, RoutedEventArgs e)
        {
            Base.Redirect(new NewEmployee(true), "Employee");
        }

        private void hlbID_Click(object sender, RoutedEventArgs e)
        {
            var hbtn = sender as HyperlinkButton;
            var data = (MEmployee)hbtn.DataContext;
            FunctionToCallWhenDoubleClicked(data);
        }

        private void FunctionToCallWhenDoubleClicked(MEmployee data)
        {
            if (data != null)
            {
                var tab = new TabItem();
                //tab.DataContext = txtbx.DataContext;
                //Model.HeaderName = "Project Details(ID:" + data.ID + ")";

                DataTemplate template2 = this.Resources["TabTemplate"] as DataTemplate;
                tab.HeaderTemplate = template2;
                tab.Header = data;

                //tab.Content = new TenderDetailView(data.ID);
                tab.Content = new EmployeeDetailTabView(data.ID,data.HasSS);
                tab.Name = "tab" + data.ID;
                string name = "tab" + data.ID;
                var tabitem = tbcMain.Items.OfType<TabItem>().SingleOrDefault(x => x.Name == name);
                if (tbcMain.Items.Contains(tabitem))
                {
                    tbcMain.SelectedItem = tabitem;
                }
                else
                {
                    try
                    {
                        tbcMain.Items.Add(tab);
                        tbcMain.SelectedItem = tab;
                        //Model.Notify("HeaderName");
                    }
                    catch
                    {
                        //string name = "tab" + data.ID;
                        ////var tabitem=tcProjects.Items.OfType<TabItem>().Where(x => x.Name == name);
                        //tcProjects.SelectedItem = tabitem;
                    }
                }
            }
        }

        private void imgDeleteTab_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            var img = sender as Image;
            var data = (MEmployee)img.DataContext;
            var tabitem = tbcMain.Items.OfType<TabItem>().SingleOrDefault(x => x.Name == "tab" + data.ID);
            if (tabitem != null)
                tbcMain.Items.Remove(tabitem);
        }
    }
}
