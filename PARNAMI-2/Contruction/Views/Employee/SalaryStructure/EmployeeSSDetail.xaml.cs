﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Contruction.Models.Employee.SalaryStructure;
using SIMS.EM.Model;

namespace Contruction.Views.Employee.SalaryStructure
{
    public partial class EmployeeSSDetail : UserControl
    {
        public SSVM Model
        {
            get { return (SSVM)this.DataContext; }
        }
				
        public EmployeeSSDetail()
        {
            InitializeComponent();
        }

        //public EmployeeSSDetail(int EmpID)
        //    : this()
        //{
        //    Model.LoadEmployeeSS(EmpID);
        //}

        public EmployeeSSDetail(int EmpID,bool  IsFromDetailedPage)
            : this()
        {
            this.IsFromDetailedPage = IsFromDetailedPage;
            Model.LoadEmployeeSS(EmpID);
        }

        private void btnModifySS_Click(object sender, RoutedEventArgs e)
        {
            var hbtn = sender as Button;
            var empl = (SSVM)hbtn.DataContext;

            if (!IsFromDetailedPage)
            {
                Base.EmployeeEvents.SS.Title = "Salary Structure , (ID: " + empl.SS.EmpID + ")";
                Base.EmployeeEvents.SS.Content = new EmployeeSS(empl.SS.EmpID);
                Base.EmployeeEvents.SS.Show();
            }
            else { Base.EmployeeEvents.RaiseSSShifted(empl.SS.EmpID,false); }
        }

        public bool IsFromDetailedPage { get; set; }
    }
}
