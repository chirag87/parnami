﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.ComponentModel;
using System.IO;
using SIMS.MM.MODEL;

namespace Contruction
{
    public partial class MIReportDetails : UserControl, INotifyPropertyChanged
    {
        public MIReportDetailsVM Model
        {
            get { return (MIReportDetailsVM)this.DataContext; }
        }

        public MIReportDetails()
        {
            InitializeComponent();
        }

        public MIReportDetails(int? itemid, int? sizeid, int? brandid, DateTime? sdate, DateTime? edate, int? siteid, string contype, string status)
            : this()
        {
            Model.Load(itemid, sizeid, brandid, sdate, edate, siteid, contype, status);

            Model.BrandID = brandid;
            Model.ItemID = itemid;
            Model.SizeID = sizeid;
            Model.SDate = sdate;
            Model.EDate = edate;
            Model.SiteID = siteid;
            Model.ConsumableType = contype;
            Model.Status = status;
        }

        private void btnExport_Click(object sender, RoutedEventArgs e)
        {
            this.dgdMIReportDetail.Export();
        }

        private void hpbtnMI_Click(object sender, RoutedEventArgs e)
        {
            var btn = sender as HyperlinkButton;
            var data = (MMIReportSummaryLine)btn.DataContext;
            if (data == null) return;

            var popup = new PRView();
            popup.SetID(data.PRID);
            popup.CreatePopup();
        }

        private void hpbtnTO_Click(object sender, RoutedEventArgs e)
        {
            var btn = sender as HyperlinkButton;
            var data = (MMIReportSummaryLine)btn.DataContext;
            if (data == null) return;
            
            if (data.MOID == null) return;
            MODetailedView mod = new MODetailedView(data.MOID.Value);
            mod.Show();
        }

        SaveFileDialog sfd;

        private void download_Click(object sender, RoutedEventArgs e)
        {
            Model.IsBusy = true;
            sfd = new SaveFileDialog();
            if (Model.RType != null)
            {
                if (Model.RType == "Excel")
                    sfd.Filter = "Excel files (*.xls)|*.xls";
                else if (Model.RType == "PDF")
                    sfd.Filter = "PDF file format|*.pdf";
            }
            else
            {
                sfd.Filter = "PDF file format|*.pdf";
            }
            if ((bool)sfd.ShowDialog())
            {
                var hp = sender as HyperlinkButton;
                var data = (MIReportDetailsVM)hp.DataContext;

                WebClient webClient = new WebClient();
                webClient.OpenReadCompleted += (s, args) =>
                {
                    StreamReader sr = new StreamReader(args.Result);
                    byte[] bytes = new byte[args.Result.Length];
                    // revisit Here
                    args.Result.Read(bytes, 0, (int)args.Result.Length);
                    Stream st = sfd.OpenFile();
                    st.Write(bytes, 0, (int)args.Result.Length);
                    st.Close();
                    MessageBox.Show("DOWNLOAD COMPLETE!" + "\nFile Name: " + sfd.SafeFileName);
                    Model.IsBusy = false;
                };
                Uri uri = new Uri("http://" + Base.Current.AppSettings.ServerPath + "/GetReport.ashx?type=MIDetails&rtype=" + Model.RType + "&sdate=" + Model.SDate.ToString() + "&edate=" + Model.EDate.ToString() + "&itemid=" + Model.ItemID.ToString() + "&sizeid=" + Model.SizeID.ToString() + "&brandid=" + Model.BrandID.ToString() + "&siteid=" + Model.SiteID.ToString() + "&contype=" + Model.ConsumableType + "&status=" + Model.Status, UriKind.Absolute);
                webClient.OpenReadAsync(uri);
            }
        }

        #region Properties

        TabControl _TabControl = new TabControl();
        public TabControl TabControl
        {
            get { return _TabControl; }
            set
            {
                _TabControl = value;
                Notify("TabControl");
            }
        }

        //DataTemplate _HeaderTemp = new DataTemplate();
        //public DataTemplate HeaderTemp
        //{
        //    get { return _HeaderTemp; }
        //    set
        //    {
        //        _HeaderTemp = value;
        //        Notify("HeaderTemp");
        //    }
        //}

        //public int? SiteID
        //{
        //    get { return Model.SiteID; }
        //    set
        //    {
        //        Model.SiteID = value;
        //    }
        //}

        //public DateTime? SDate
        //{
        //    get { return Model.SDate; }
        //    set
        //    {
        //        Model.SDate = value;
        //    }
        //}

        //public DateTime? EDate
        //{
        //    get { return Model.EDate; }
        //    set
        //    {
        //        Model.EDate = value;
        //    }
        //}

        //public string Contype
        //{
        //    get { return Model.ConsumableType; }
        //    set
        //    {
        //        Model.ConsumableType = value;
        //    }
        //}

        //public string status
        //{
        //    get { return Model.Status; }
        //    set
        //    {
        //        Model.Status = value;
        //    }
        //}

        #endregion

        #region INotifyPropertyChanged
        public event PropertyChangedEventHandler PropertyChanged;
        public void Notify(string propertyName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion
    }
}
