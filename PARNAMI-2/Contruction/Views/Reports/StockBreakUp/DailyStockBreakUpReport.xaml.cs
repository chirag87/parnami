﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using SIMS.MM.MODEL;

namespace Contruction
{
    public partial class DailyStockBreakUpReport : UserControl
    {
        public DailyStockBreakUpReportVM Model
        {
            get { return (DailyStockBreakUpReportVM)this.DataContext; }
        }
		
        public DailyStockBreakUpReport()
        {
            InitializeComponent();
        }

        public DailyStockBreakUpReport(DateTime? sdate, DateTime? edate, int siteid, int itemid, int? itemCatID, int? sizeid, int? brandid, string ConType)
            :this()
        {
            Model.Load(sdate, edate, siteid, itemid, itemCatID, sizeid, brandid, ConType);

            //Set Site and Item ID for Displaying on Daily Stock Report Screen
            Model.SDate = sdate;
            Model.EDate = edate;
            Model.SiteID = siteid;
            Model.ItemID = itemid;
            Model.itemCatID = itemCatID;
            Model.SizeID = sizeid;
            Model.BrandID = brandid;
            Model.ConsumableType = ConType;
        }

        private void btnExport_Click(object sender, RoutedEventArgs e)
        {
            dgdDailyStock.Export();
        }

        private void hpbtnMOInfo_Click(object sender, RoutedEventArgs e)
        {
            var btn = sender as HyperlinkButton;
            var data = (MDailyStockBreakUp)btn.DataContext;
            if (data == null) return;

            if (data.ToBeReceivedQty <= 0 && data.MOID == null) return;

            MODetailedView view = new MODetailedView(data.MOID.Value);
            view.Show();
        }

        private void hobtnRecQtyInfo_Click(object sender, RoutedEventArgs e)
        {
            var btn = sender as HyperlinkButton;
            var data = (MDailyStockBreakUp)btn.DataContext;
            if (data == null) return;

            if (data.ReceivedQty <= 0 && data.RecMOID == null) return;

            MODetailedView view = new MODetailedView(data.RecMOID.Value);
            view.Show();
        }

        private void hpbtnRelQtyInfo_Click(object sender, RoutedEventArgs e)
        {
            var btn = sender as HyperlinkButton;
            var data = (MDailyStockBreakUp)btn.DataContext;
            if (data == null) return;

            if (data.ReleasedQty <= 0 && data.RelMOID == null) return;

            MODetailedView view = new MODetailedView(data.RelMOID.Value);
            view.Show();
        }

        private void hpbtnMRNQtyInfo_Click(object sender, RoutedEventArgs e)
        {
            var btn = sender as HyperlinkButton;
            var data = (MDailyStockBreakUp)btn.DataContext;
            if (data == null) return;

            if (data.MRNQty <= 0 && data.MRNID == null) return;

            var popup = new GRNsDetailedView();
            popup.SetID(data.MRNID.Value);
            popup.CreatePopup();
        }

        private void hpbtnCPQty_Click(object sender, RoutedEventArgs e)
        {
            var btn = sender as HyperlinkButton;
            var data = (MDailyStockBreakUp)btn.DataContext;
            if (data == null) return;

            if (data.CPQty <= 0 && data.CPID == null) return;

            var popup = new CashReceivingDetailedView();
            popup.setID(data.CPID.Value);
            popup.CreatePopup();
        }

        TabControl _TabControl = new TabControl();
        public TabControl TabControl
        {
            get { return _TabControl; }
            set
            {
                _TabControl = value;
                Model.Notify("TabControl");
            }
        }
    }
}
