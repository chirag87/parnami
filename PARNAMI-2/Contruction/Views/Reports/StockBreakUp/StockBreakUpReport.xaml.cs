﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.IO;
using SIMS.MM.MODEL;

namespace Contruction
{
    public partial class StockBreakUpReport : UserControl
    {
        public StockBreakUpReportVM Model
        {
            get { return (StockBreakUpReportVM)this.DataContext; }
        }

        public StockBreakUpReport()
        {
            InitializeComponent();
        }

        private void tbxSearch_KeyDown(object sender, KeyEventArgs e)
        {

        }

        private void tbxSearch_TextChanged(object sender, TextChangedEventArgs e)
        {

        }

        private void btnSearchBox_Click(object sender, RoutedEventArgs e)
        {

        }

        private void imgSearchBox1_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {

        }

        private void btnItem_Click(object sender, RoutedEventArgs e)
        {
            var btn = sender as Button;
            var data = (MStockBreakUp)btn.DataContext;
            OpenNewTabItem(data);
        }

        public void OpenNewTabItem(MStockBreakUp data)
        {
            if (data != null)
            {
                var tab = new CloseableTabItem();

                //DataTemplate headertemplate = this.Resources["TabHeaderTemplateForItems"] as DataTemplate;
                //tab.HeaderTemplate = headertemplate;

                tab.Header = data.Item;
                var uc = new DailyStockBreakUpReport(Model.sdate, Model.edate, data.SiteID.Value, data.ItemID.Value, data.ItemCatID, data.SizeID, data.BrandID, Model.ConsumableType);
                tab.Content = uc;
                uc.TabControl = tabStockReport;
                tab.Name = data.Item + data.Size + data.Brand + data.SiteID + " Details";

                string name = data.Item + data.Size + data.Brand + data.SiteID + " Details";
                var tabitem = tabStockReport.Items.OfType<TabItem>().SingleOrDefault(x => x.Name == name);
                if (tabStockReport.Items.Contains(tabitem))
                {
                    tabStockReport.SelectedItem = tabitem;
                }
                else
                {
                    try
                    {
                        tabStockReport.Items.Add(tab);
                        tabStockReport.SelectedItem = tab;
                    }
                    catch
                    {
                    }
                }
            }
        }

        private void imgDeleteTab2_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            throw new NotImplementedException();
        }

        private void hpClear_Click(object sender, RoutedEventArgs e)
        {
            Model.Clear();
        }

        private void btnLoad_Click(object sender, RoutedEventArgs e)
        {
            Model.Load();
        }

        SaveFileDialog sfd;

        private void download_Click(object sender, RoutedEventArgs e)
        {
            Model.IsBusy = true;
            sfd = new SaveFileDialog();
            if (Model.RType != null)
            {
                if (Model.RType == "Excel")
                    sfd.Filter = "Excel files (*.xls)|*.xls";
                else if (Model.RType == "pdf")
                    sfd.Filter = "PDF file format|*.pdf";
            }
            else
            {
                sfd.Filter = "PDF file format|*.pdf";
            }
            if ((bool)sfd.ShowDialog())
            {
                var hp = sender as HyperlinkButton;
                var data = (StockBreakUpReportVM)hp.DataContext;

                WebClient webClient = new WebClient();
                webClient.OpenReadCompleted += (s, args) =>
                {
                    StreamReader sr = new StreamReader(args.Result);
                    byte[] bytes = new byte[args.Result.Length];
                    // revisit Here
                    args.Result.Read(bytes, 0, (int)args.Result.Length);
                    Stream st = sfd.OpenFile();
                    st.Write(bytes, 0, (int)args.Result.Length);
                    st.Close();
                    MessageBox.Show("DOWNLOAD COMPLETE!" + "\nFile Name: " + sfd.SafeFileName);
                    Model.IsBusy = false;
                };
                Uri uri = new Uri("http://" + Base.Current.AppSettings.ServerPath + "/GetReport.ashx?type=StockBreakUp&rtype=" + Model.RType + "&sdate=" + Model.sdate.ToString() + "&edate=" + Model.edate.ToString() + "&siteid=" + Model.SiteID.ToString() + "&itemid=" + Model.ItemID.ToString() + "&itemCatID=" + Model.ItemCatID.ToString() + "&sizeid=" + Model.SizeID.ToString() + "&brandid=" + Model.BrandID.ToString() + "&contype=" + Model.ConsumableType, UriKind.Absolute);
                webClient.OpenReadAsync(uri);
            }
        }

        private void btnExport_Click(object sender, RoutedEventArgs e)
        {
            this.dgdStock.Export();
        }

        //#region Property

        //        public DateTime? sdate
        //        {
        //            get { return Model.sdate; }
        //            set
        //            {
        //                Model.sdate = value;
        //            }
        //        }

        //        public DateTime? edate
        //        {
        //            get { return Model.edate; }
        //            set
        //            {
        //                Model.edate = value;
        //            }
        //        }

        //        public int? SiteID
        //        {
        //            get { return Model.SiteID; }
        //            set
        //            {
        //                Model.SiteID = value;
        //            }
        //        }

        //        public int? ItemID
        //        {
        //            get { return Model.ItemID; }
        //            set
        //            {
        //                Model.ItemID = value;
        //            }
        //        }

        //        #endregion
    }
}