﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SIMS.MM.DAL.PrintReports
{
    using SIMS.ReportLoader;

    public class GRNDataPrintLoader : ReportLoader
    {
        public override RDLCReportData GetReportData(ReportParameterDictionary Dic)
        {
            RDLCReportData data = new RDLCReportData();

            parnamidb1Entities db = new parnamidb1Entities();
            MMReportsDBDataContext rdb = new MMReportsDBDataContext();
            rdb.CommandTimeout = 5000;
            DateTime? sdate = null;
            DateTime? edate = null;
            int? itemid = null;
            int? sizeid = null;
            int? brandid = null;
            int? siteid = null;
            int? vendorid = null;

            if (Dic.Count != 0)
            {
                /*Checking if Dictionary Contains the Parameter name as well as there Values*/

                if (Dic.Any(x => x.Key == "sdate") && !String.IsNullOrWhiteSpace(Dic["sdate"].ToString()))
                    sdate = (DateTime)Dic["sdate"];

                if (Dic.Any(x => x.Key == "edate") && !String.IsNullOrWhiteSpace(Dic["edate"].ToString()))
                    edate = (DateTime)Dic["edate"];

                if (Dic.Any(x => x.Key == "siteid") && !String.IsNullOrWhiteSpace(Dic["siteid"].ToString()))
                    siteid = (int?)Dic["siteid"];

                if (Dic.Any(x => x.Key == "vendorid") && !String.IsNullOrWhiteSpace(Dic["vendorid"].ToString()))
                    siteid = (int?)Dic["vendorid"];

                if (Dic.Any(x => x.Key == "itemid") && !String.IsNullOrWhiteSpace(Dic["itemid"].ToString()))
                    itemid = (int)Dic["itemid"];

                if (Dic.Any(x => x.Key == "sizeid") && !String.IsNullOrWhiteSpace(Dic["sizeid"].ToString()))
                    sizeid = (int?)Dic["sizeid"];

                if (Dic.Any(x => x.Key == "brandid") && !String.IsNullOrWhiteSpace(Dic["brandid"].ToString()))
                    brandid = (int?)Dic["brandid"];
            }

            var GRNData = rdb.GetGRNReport(sdate, edate, siteid, vendorid, itemid, sizeid, brandid).OrderByDescending(x => x.ForDate).ToArray();
            data.Add("Data", GRNData);
            
            return data;
        }
    }
}
