﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SIMS.MM.DAL.PrintReports
{
    using SIMS.ReportLoader;

    public class GRNPrintLoader : ReportLoader
    {
        public override RDLCReportData GetReportData(ReportParameterDictionary Dic)
        {
            RDLCReportData data = new RDLCReportData();

            parnamidb1Entities db = new parnamidb1Entities();
            MMReportsDBDataContext rdb = new MMReportsDBDataContext();
            rdb.CommandTimeout = 5000;
            int grnid = (int)Dic["id"];

            var header = rdb.GetGRNHeaders(null, null, null, null, null).Where(x => x.ID == grnid).ToList();
            //var detail = rdb.GetGRNDetailsByUniqueID(grnid);
            var detail = rdb.GetGRNDetailsforRDLC(grnid);
            var vendor = rdb.GetVendorDetailsbyVendorID(header.First().VendorID);
            var site = rdb.GetSiteDetailsbySiteID(header.First().BUID);
            var app = db.AppSettings;

            data.Add("Headers", header);
            data.Add("Details", detail);
            data.Add("VendorDetails", vendor);
            data.Add("SiteDetails", site);
            data.Add("AppSettings", app);

            return data;
        }
    }
}
