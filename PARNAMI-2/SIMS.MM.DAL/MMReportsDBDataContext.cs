﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SIMS.MM.DAL
{
    public partial class MMReportsDBDataContext
    {
        public MMReportsDBDataContext():this(ConnectionProvider.ForLINQ2SQL())
        {

        }

        public static MMReportsDBDataContext New()
        {
            return new MMReportsDBDataContext(ConnectionProvider.ForLINQ2SQL());
        }
    }
}
