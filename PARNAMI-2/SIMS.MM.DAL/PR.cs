﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SIMS.MM.DAL
{
    public partial class PR
    {
        public string ApprovalStatus
        {
            get {
                if (this.IsApproved)
                    return "Approved";
                else
                    return "Pending Approval";
            }
        }
    }
}
