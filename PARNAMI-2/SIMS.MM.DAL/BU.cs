﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SIMS.MM.DAL
{
    public partial class BUs
    {
        public string DisplaySite
        {
            get 
            {
                return Code + " (ID: " + ID + ", Name: " + Name + ", Address: " + Address + ", " + City + ")";
            }
        }
    }

}
