﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SIMS.MM.DAL
{
    partial class Billing
    {
        public string Status
        {
            get
            {
                if (IsApproved == true)
                {
                    return "Approved \n By: " + ApprovedBy + " on " + ApprovedOn;
                }

                else if(BillingVerifications.Select(x => x.BillingID == ID).Count() != 0)
                {
                    BillingVerification data = new BillingVerification();
                    data = BillingVerifications.Last(x => x.BillingID == ID);
                    return "Verified \n By: " + data.VerifiedBy + " on " + data.VerifiedOn;
                }

                else
                {
                    return "";
                }
            }
        }
    }
}
