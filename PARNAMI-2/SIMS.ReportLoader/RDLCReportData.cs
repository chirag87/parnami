﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Reporting.WebForms;

namespace SIMS.ReportLoader
{
    public class RDLCReportData
    {
        public string Path { get; set; }
        public List<ReportDataSource> DataSources;

        public RDLCReportData()
        {
            DataSources = new List<ReportDataSource>();
        }

        public void Add(string key, object DataSource)
        {
            DataSources.Add(new ReportDataSource(key, DataSource));
        }
    }
}
