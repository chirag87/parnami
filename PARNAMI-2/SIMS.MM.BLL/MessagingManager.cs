﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SIMS.MM.DAL;
using SIMS.MM.BLL;
using System.Collections.ObjectModel;
using SIMS.MM.MODEL;

namespace SIMS.MM.BLL
{
    public class MessagingManager : IMessagingManager
    {
        parnamidb1Entities db = new parnamidb1Entities();
        MessagingDataProvider provider = new MessagingDataProvider();

        public MMessaging SendNewMail(MMessaging NewEmail)
        {
            var dt = ConvertToMessaging(NewEmail);
            db.Messages.AddObject(dt);
            db.SaveChanges();
            return provider.ConvertToMMessaging(dt);
        }

        public Message ConvertToMessaging(MMessaging NewEmail)
        {
            Message msg = new Message()
            {
                HasAttachment = false, //NewEmail.HasAttachment,
                MainMessage = NewEmail.Message,
                SendingUser = NewEmail.Sender,
                SentDate = NewEmail.SentDate,
                Subject = NewEmail.Subject
            };
            
            if (NewEmail.Receivers != null)
            {
                NewEmail.ReceiversName.ToList()
                    .ForEach(x => msg.MessageReceivers.Add(new MessageReceiver()
                    {
                        ReceivingUser = x
                    }));
            }

            if (NewEmail.Emailers != null)
            {
                NewEmail.EmailersName.ToList()
                    .ForEach(x => msg.MessageEmailers.Add(new MessageEmailer()
                        {
                            EmailAddress = x
                        }));
            }
            return msg;
        }

        public MessageEmailer ConvertToMessageEmailers(MMessageEmailer x)
        {
            MessageEmailer msgEmail = new MessageEmailer()
            {
                MessageID = x.MessageID,
                EmailAddress = x.EmailAddress
            };
            return msgEmail;
        }

        public MessageReceiver ConvertToMessageReceiver(MMessageReceiver x)
        {
            MessageReceiver msgRec = new MessageReceiver()
            {
                IsDeleted = x.IsDeleted,
                IsFlag = x.IsFlag,
                IsRead = x.IsRead,
                IsStar = x.IsStar,
                ReadOn = x.ReadOn,
                ReceivingUser = x.Receiver
            };
            return msgRec;
        }

        public bool SetIsStar(int msgid, string user, bool IsStar)
        {
            var data = db.MessageReceivers.SingleOrDefault(x => x.MessageID == msgid && x.ReceivingUser == user);
            if (data != null)
            {
                data.IsStar = IsStar;
                db.SaveChanges();
                return db.MessageReceivers.SingleOrDefault(x => x.MessageID == msgid && x.ReceivingUser == user).IsStar;
            }
            return false;
            
        }

        public bool SetIsFlag(int msgid, string user, bool IsFlag)
        {
            var data = db.MessageReceivers.SingleOrDefault(x => x.MessageID == msgid && x.ReceivingUser == user);
            if (data != null)
            {
                data.IsFlag = IsFlag;
                db.SaveChanges();
                return db.MessageReceivers.SingleOrDefault(x => x.MessageID == msgid && x.ReceivingUser == user).IsFlag;
            }
            return false;
        }

        public bool SetIsRead(int msgid, string user, bool IsRead)
        {
            var data = db.MessageReceivers.SingleOrDefault(x => x.MessageID == msgid && x.ReceivingUser == user);
            if (data != null)
            {
                data.IsRead = IsRead;
                data.ReadOn = DateTime.UtcNow.AddHours(5.5);
                db.SaveChanges();
                return db.MessageReceivers.SingleOrDefault(x => x.MessageID == msgid && x.ReceivingUser == user).IsRead;
            }
            return false;
        }

        public void DeleteMail(int Msgid, string user)
        {
            var data = db.MessageReceivers.SingleOrDefault(x => x.MessageID == Msgid && x.ReceivingUser == user);
            if (data != null)
            {
                data.IsDeleted = true;
                db.SaveChanges();
            }
        }
    }
}