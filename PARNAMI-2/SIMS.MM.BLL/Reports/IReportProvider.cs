﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;

namespace SIMS.MM.BLL.Reports
{
    using DAL;
    using MODEL;
    using System.Collections;

    [ServiceContract]
    public interface IReport
    {
        [OperationContract]
        IEnumerable<MPOReport> GetPODetailsByLine(DateTime? startdate, DateTime? enddate, int? siteid, int? vendorid, int? itemid, int? itemcatid, int? sizeid, int? brandid);

        [OperationContract]
        IEnumerable<MGRNReport> GetGRNDetails(DateTime? sdate, DateTime? edate, int? siteid, int? vendorid, int? itemid, int? sizeid, int? brandid);

        [OperationContract]
        IEnumerable<MDPReport> GetDPDetails(DateTime? sdate, DateTime? edate, int? siteid, int? vendorid, int? itemid, int? sizeid, int? brandid);

        [OperationContract]
        IEnumerable<MDirectGRNReport> GetDirectGRNReport(DateTime? sdate, DateTime? edate, int? siteid, int? vendorid, int? itemid, int? sizeid, int? brandid);

        [OperationContract]
        IEnumerable<MCPReport> GetCPdetails(DateTime? sdate, DateTime? edate, int? siteid, int? vendorid, int? itemid, int? sizeid, int? brandid);

        [OperationContract]
        IEnumerable<MConsumptionReport> GetConsumptionDetails(DateTime? sdate, DateTime? edate, int? siteid, int? vendorid, int? itemid, int? sizeid, int? brandid);

        [OperationContract]
        IEnumerable<MConsumptionReport> TotalReceiveByDates(DateTime? date1, DateTime? date2);

        [OperationContract]
        IEnumerable<MMTRReport> TotalReceiveBySiteAndDates(int? siteid, DateTime? date1, DateTime? date2);

        [OperationContract]
        IEnumerable<MMOReport> GetMOdetails(DateTime? sdate, DateTime? edate, int? siteid, int? itemid, int? sizeid, int? brandid);

        [OperationContract]
        IEnumerable<MBilledReceivings> GetBilledReceivings(DateTime? sdate, DateTime? edate, int? siteid, int? vendorid, int? itemid, int? sizeid, int? brandid);

        [OperationContract]
        IEnumerable<MDirectGRNReport> GetUnBilledReceivings(DateTime? sdate, DateTime? edate, int? siteid, int? vendorid, int? itemid, int? sizeid, int? brandid);

        [OperationContract]
        IEnumerable<MStockReport> GetDetailedStockReport(DateTime? sdate, DateTime? edate, int? siteid, int? itemCatID, int? itemid, int? sizeid, int? brandid);

        [OperationContract]
        IEnumerable<MMonthlyStockReport> GetMonthlyStockReport(DateTime? sdate, DateTime? edate, int? siteid, int? itemCatID, int? itemid, int? sizeid, int? brandid);

        [OperationContract]
        IEnumerable<MDailyStockReport> GetDailyStockReport(DateTime? sdate, DateTime? edate, int? siteid, int? itemCatID, int? itemid, int? sizeid, int? brandid);

        [OperationContract]
        IEnumerable<MMIReportSummary> GetMIReportSummary(DateTime? sdate, DateTime? edate, int? itemCatID, int? itemid, int? sizeid, int? brandid, int? siteid, string Contype, string status, bool showzero);

        [OperationContract]
        IEnumerable<MMIReportSummaryLine> GetMIReportSummaryDetails(DateTime? sdate, DateTime? edate, int? itemCatID, int? itemid, int? sizeid, int? brandid, int? siteid, string Contype, string status);

        [OperationContract]
        IEnumerable<MMIReportSummary> GetMIDashBoard(DateTime? sdate, DateTime? edate, int? itemid, int? sizeid, int? brandid, int? siteid, string Contype);

        [OperationContract]
        IEnumerable<MMIReportSiteWiseSummary> GetMIReportSiteWiseSummary(DateTime? sdate, DateTime? edate, int? itemCatID, int? itemid, int? sizeid, int? brandid, int? siteid, string Contype, string status, bool showzero);

        [OperationContract]
        IEnumerable<MStockBreakUp> GetStockBreakUpReport(DateTime? sdate, DateTime? edate, int? itemid, int? itemCatID, int? sizeid, int? brandid, int? siteid, string Contype);

        [OperationContract]
        IEnumerable<MDailyStockBreakUp> GetDailyStockBreakUpReport(DateTime? sdate, DateTime? edate, int? itemid, int? itemCatID, int? sizeid, int? brandid, int? siteid, string Contype);
    }
}