﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SIMS.MM.BLL.Reports
{
    using DAL;
    using MODEL;
    using System.Collections;

    public class ReportProvider : IReport
    {

        public ReportProvider()
        {
            dc.CommandTimeout = 5000;
        }

        //estrodb2Entities1 db = new estrodb2Entities1();
        MMReportsDBDataContext dc = new MMReportsDBDataContext();

        public IEnumerable<MPOReport> GetPODetailsByLine(DateTime? startdate, DateTime? enddate, int? siteid, int? vendorid, int? itemid, int? itemcatid, int? sizeid, int? brandid)
        {
            return dc.GetPODetailsByLine(startdate, enddate, siteid, vendorid, itemid, itemcatid, sizeid, brandid)
                .OrderByDescending(x => x.CreatedOn)
                .Select(x => new MPOReport()
                    {
                        PRNO = x.PRID,
                        Site = x.Site,
                        OrderDate = x.CreatedOn,
                        MaterialCategory = x.ItemCategory,
                        Material = x.Item,
                        MaterialID = x.ItemID,
                        CategoryID = x.CategoryID,
                        MU = x.MU,
                        PerUnitPrice = (double)x.UnitPrice,
                        PONO = x.POID,
                        Qty = x.Qty,
                        SizeID = x.SizeID,
                        Size = x.Size,
                        BrandID = x.BrandID,
                        Brand = x.Brand,
                        TotalAmount = (double)x.LinePrice,
                        Vendor = x.Vendor,
                        DisplaySite = x.DisplaySite,
                        DisplayVendor = x.DisplayVendor
                    })
                .ToList();
        }

        public IEnumerable<MGRNReport> GetGRNDetails(DateTime? sdate, DateTime? edate, int? siteid, int? vendorid, int? itemid, int? sizeid, int? brandid)
        {
            return dc.GetGRNDetails(sdate, edate, siteid, vendorid, itemid, sizeid, brandid)
                .OrderByDescending(x => x.ForDate)
                .Select(x => new MGRNReport()
                {
                    ID = x.ID,
                    CategoryID = x.CategoryID,
                    RefNo = x.ReferenceNo,
                    DateofReceipt = x.ForDate,
                    ItemID = x.ItemID,
                    Item = x.ItemDetail,
                    ItemCategory = x.Item,
                    MU = x.Unit,
                    PerUnitPrice = x.UnitPrice.Value,
                    PONO = x.POID,
                    PRNO = x.PRID,
                    QtyReceived = x.Qty,
                    Remarks = x.Remarks,
                    Site = x.Site,
                    SizeID = x.SizeID,
                    Size = x.Size,
                    BrandID = x.BrandID,
                    Brand = x.Brand,
                    TotalAmount = x.Qty * x.UnitPrice.Value,
                    Vendor = x.Vendor,
                    InvoiceNo = x.InvoiceNo,
                    ChallanNo = x.ChallanNo,
                    DisplaySite = x.DisplaySite,
                    DisplayVendor = x.DisplayVendor,
                    Purchaser = x.CreatedBy
                })
                .ToList();
        }

        public IEnumerable<MDPReport> GetDPDetails(DateTime? sdate, DateTime? edate, int? siteid, int? vendorid, int? itemid, int? sizeid, int? brandid)
        {
            var data1 = dc.GetDirectPurchaseDetails(sdate, edate, siteid, vendorid, itemid, sizeid, brandid).ToList();
            var data = data1
                .Select(x => new MDPReport()
                {
                    ID = x.ID,
                    RefNo = x.ReferenceNo,
                    Date = x.ForDate,
                    InvoiceNo = x.InvoiceNo,
                    ChallanNo = x.challanNo,
                    LineChallanNo = x.LineChallanNo,
                    ItemID = x.ItemID,
                    Item = x.ItemDetail,
                    ItemCategory = x.Item,
                    CategoryID = x.CategoryID,
                    SizeID = x.SizeID,
                    Size = x.Size,
                    BrandID = x.BrandID,
                    Brand = x.Brand,
                    MU = x.Unit,
                    PerUnitPrice = x.UnitPrice,
                    Purchaser = x.CreatedBy,
                    Quantity = x.Qty,
                    Remarks = x.Remarks,
                    TotalAmount = x.Qty * x.UnitPrice,
                    Vendor = x.Vendor,
                    Site = x.Site,
                    DisplaySite = x.DisplaySite,
                    DisplayVendor = x.DisplayVendor,
                    LastUpdatedOn = x.LastUpdatedOn,
                }).ToList();
            return data;
        }

        public IEnumerable<MDirectGRNReport> GetDirectGRNReport(DateTime? sdate, DateTime? edate, int? siteid, int? vendorid, int? itemid, int? sizeid, int? brandid)
        {
            var data1 = dc.GetDirectGRNReport(sdate, edate, siteid, vendorid, itemid, sizeid, brandid).ToList();
            var data = data1
                .Select(x => new MDirectGRNReport()
                {
                    ID = x.ID,
                    RefNo = x.ReferenceNo,
                    InvoiceNo = x.InvoiceNo,
                    ChallanNo = x.ChallanNo,
                    LineChallanNo = x.LineChallanNo,
                    ItemID = x.ItemID,
                    Item = x.ItemDetail,
                    ItemCategory = x.Item,
                    CategoryID = x.CategoryID,
                    SizeID = x.SizeID,
                    Size = x.Size,
                    BrandID = x.BrandID,
                    Brand = x.Brand,
                    MU = x.Unit,
                    PerUnitPrice = x.UnitPrice.Value,
                    Purchaser = x.CreatedBy,
                    Remarks = x.Remarks,
                    TotalAmount = x.Qty * x.UnitPrice.Value,
                    Vendor = x.Vendor,
                    Site = x.Site,
                    DisplaySite = x.DisplaySite,
                    DisplayVendor = x.DisplayVendor,
                    LastUpdatedOn = x.LastUpdatedOn,
                    PONO = x.POID,
                    PRNO = x.PRID,
                    QtyReceived = x.Qty,
                    DateofReceipt = x.ForDate
                }).ToList();
            return data;
        }

        public IEnumerable<MCPReport> GetCPdetails(DateTime? sdate, DateTime? edate, int? siteid, int? vendorid, int? itemid, int? sizeid, int? brandid)
        {
            var data = dc.GetCashPurchaseDetails(sdate, edate, siteid, vendorid, itemid, sizeid, brandid)
                .OrderByDescending(x => x.ForDate)
                 .Select(x => new MCPReport()
                 {
                     Date = x.ForDate,
                     InvoiceNo = x.InvoiceNo,
                     ChallanNo = x.ChallanNo,
                     ItemID = x.ItemID,
                     ItemCategory = x.Item,
                     Item = x.ItemDetail,
                     SizeID = x.SizeID,
                     Size = x.Size,
                     BrandID = x.BrandID,
                     Brand = x.Brand,
                     MU = x.Unit,
                     PerUnitPrice = x.UnitPrice,
                     Purchaser = x.CreatedBy,
                     Quantity = x.Qty,
                     ID = x.ID,
                     CategoryID = x.CategoryID,
                     RefNo = x.ReferenceNo,
                     Remarks = x.Remarks,
                     Site = x.Site,
                     TotalAmount = x.Qty * x.UnitPrice,
                     Vendor = x.vendor,
                     DisplaySite = x.DisplaySite,
                     DisplayVendor = x.DisplayVendor
                 }).ToList();
            return data;
        }

        public IEnumerable<MConsumptionReport> GetConsumptionDetails(DateTime? sdate, DateTime? edate, int? siteid, int? vendorid, int? itemid, int? sizeid, int? brandid)
        {
            var data = dc.GetConsumptionDetails(sdate, edate, siteid, vendorid, itemid, sizeid, brandid)
                .OrderByDescending(x => x.ForDate)
                .Select(x => new MConsumptionReport()
                {
                    CategoryID = x.CategoryID,
                    ForDate = x.ForDate,
                    ID = x.ID,
                    RefNo = x.ReferenceNo,
                    Item = x.ItemDetail,
                    ItemCategory = x.Item,
                    ItemID = x.ItemID,
                    SizeID = x.SizeID,
                    Size = x.Size,
                    BrandID = x.BrandID,
                    Brand = x.Brand,
                    LineID = x.LineID,
                    Qty = x.Qty,
                    Remarks = x.Remarks,
                    Site = x.Site,
                    Type = x.Type,
                    Unit = x.Unit,
                    DisplaySite = x.DisplaySite,
                    CreatedBy = x.CreatedBy
                }).ToList();
            return data;
        }

        public IEnumerable<MConsumptionReport> TotalReceiveByDates(DateTime? date1, DateTime? date2)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<MMTRReport> TotalReceiveBySiteAndDates(int? siteid, DateTime? date1, DateTime? date2)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<MMOReport> GetMOdetails(DateTime? sdate, DateTime? edate, int? siteid, int? itemid, int? sizeid, int? brandid)
        {
            var data = dc.GetMODetails(sdate, edate, siteid, itemid, sizeid, brandid)
                .OrderByDescending(x => x.MTRID).ToList()
                .Select(x => new MMOReport()
                {
                    ID = x.MTRID,
                    TransferringSite = x.Transferring_Site,
                    ReceivingSite = x.Requesting_Site,
                    RefNo = x.ReferenceNo,
                    ItemID = x.ItemID,
                    Item = x.Item_Name,
                    SizeID = x.SizeID,
                    Size = x.Size,
                    BrandID = x.BrandID,
                    Brand = x.Brand,
                    RequestingQty = x.RequiredQty,
                    ApprovedQty = x.ApprovedQty,
                    PendingQty = x.InTransitQty,
                    ReleasedQty = x.RequiredQty,
                    TransferedQty = x.TransferredQty,
                    Remarks = x.Remarks,
                    Date = x.TransferBefore,
                    Status = x.Status,
                    MU = x.MU
                });
            return data;
        }

        public IEnumerable<MBilledReceivings> GetBilledReceivings(DateTime? sdate, DateTime? edate, int? siteid, int? vendorid, int? itemid, int? sizeid, int? brandid)
        {
            var data1 = dc.GetBilledReceivingDetails(sdate, edate, siteid, vendorid, itemid, sizeid, brandid).ToList();
            var data = data1
                .Select(x => new MBilledReceivings()
                {
                    ID = x.ID,
                    RefNo = x.ReferenceNo,
                    InvoiceNo = x.InvoiceNo,
                    ChallanNo = x.ChallanNo,
                    LineChallanNo = x.LineChallanNo,
                    ItemID = x.ItemID,
                    Item = x.ItemDetail,
                    ItemCategory = x.Item,
                    CategoryID = x.CategoryID,
                    SizeID = x.SizeID,
                    Size = x.Size,
                    BrandID = x.BrandID,
                    Brand = x.Brand,
                    MU = x.Unit,
                    PerUnitPrice = x.UnitPrice.Value,
                    Purchaser = x.CreatedBy,
                    Remarks = x.Remarks,
                    TotalAmount = x.Qty * x.UnitPrice.Value,
                    Vendor = x.Vendor,
                    Site = x.Site,
                    DisplaySite = x.DisplaySite,
                    DisplayVendor = x.DisplayVendor,
                    LastUpdatedOn = x.LastUpdatedOn,
                    QtyReceived = x.Qty,
                    DateofReceipt = x.ForDate,
                    ApprovedBy = x.ApprovedBy,
                    ApprovedOn = x.ApprovedOn,
                    BillDate = x.BillDate,
                    BillingID = x.BillingID,
                    BillingLineID = x.BillingLineID,
                    BillingRemarks = x.BillingRemarks,
                    BillNo = x.BillNo,
                    BillRefNo = x.BillRefNo,
                    Cartage = x.Cartage,
                    Deductions = x.Deductions,
                    DefaultPaymentDays = x.DefaultPaymentDays,
                    GrossAmount = x.GrossAmount,
                    NetAmount = x.NetAmount,
                    PaymentNotBefore = x.PaymentNotBefore,
                    Surcharge = x.Surcharge,
                    TotalBillAmount = x.TotalAmount,
                    VAT = x.VAT
                }).ToList();
            return data;
        }

        public IEnumerable<MDirectGRNReport> GetUnBilledReceivings(DateTime? sdate, DateTime? edate, int? siteid, int? vendorid, int? itemid, int? sizeid, int? brandid)
        {
            var data1 = dc.GetUnBilledReceivingDetails(sdate, edate, siteid, vendorid, itemid, sizeid, brandid).ToList();
            var data = data1
                .Select(x => new MDirectGRNReport()
                {
                    ID = x.ID,
                    RefNo = x.ReferenceNo,
                    InvoiceNo = x.InvoiceNo,
                    ChallanNo = x.ChallanNo,
                    LineChallanNo = x.LineChallanNo,
                    ItemID = x.ItemID,
                    Item = x.ItemDetail,
                    ItemCategory = x.Item,
                    CategoryID = x.CategoryID,
                    SizeID = x.SizeID,
                    Size = x.Size,
                    BrandID = x.BrandID,
                    Brand = x.Brand,
                    MU = x.Unit,
                    PerUnitPrice = x.UnitPrice.Value,
                    Purchaser = x.CreatedBy,
                    Remarks = x.Remarks,
                    TotalAmount = x.Qty * x.UnitPrice.Value,
                    Vendor = x.Vendor,
                    Site = x.Site,
                    DisplaySite = x.DisplaySite,
                    DisplayVendor = x.DisplayVendor,
                    LastUpdatedOn = x.LastUpdatedOn,
                    PONO = x.POID,
                    PRNO = x.PRID,
                    QtyReceived = x.Qty,
                    DateofReceipt = x.ForDate
                }).ToList();
            return data;
        }

        public IEnumerable<MStockReport> GetDetailedStockReport(DateTime? sdate, DateTime? edate, int? siteid, int? itemCatID, int? itemid, int? sizeid, int? brandid)
        {
            var data1 = dc.GetDetailedStockReport(sdate, edate, siteid, itemCatID, itemid, sizeid, brandid).ToList();
            var data = data1.Select(x => new MStockReport()
            {
                ClosingBalance = x.ClosingBalance,
                OpeningBalance = x.OpeningBalance,
                ItemCategory = x.Item,
                Item = x.Name,
                MU = x.MeasurementID,
                QtyIn = x.QtyIn,
                QtyOut = x.QtyOut,
                ItemID = x.ItemID,
                SizeID = x.SizeID,
                Size = x.Size,
                BrandID = x.BrandID,
                Brand = x.Brand,
                CategoryID = x.CategoryID,
                DisplaySite = x.Site,
                SiteID = x.SiteID,
                DisplayItemName = x.DisplayItemName,
                TotalConsumedQty = x.ConsumedQty,
                LinesCount = data1.Count()
            });
            return data.AsQueryable();
        }

        public IEnumerable<MMonthlyStockReport> GetMonthlyStockReport(DateTime? sdate, DateTime? edate, int? siteid, int? itemCatID, int? itemid, int? sizeid, int? brandid)
        {
            var data1 = dc.GetMonthlySiteWiseStockReport(sdate, edate, siteid, itemCatID, itemid, sizeid, brandid).ToList()
                .OrderByDescending(x => x.monthNo)
                .OrderByDescending(x => x.YearNo);
            var data = data1.Select(x => new MMonthlyStockReport()
            {
                ClosingBalance = x.ClosingBalance,
                OpeningBalance = x.OpeningBalance,
                QtyIn = x.QtyIn,
                QtyOut = x.QtyOut,
                NetQtyIN = x.NetIn,
                TotalConsumedQty = x.ConsumedQty,
                month = x.monthNo,
                MonthName = x.FullMonthName,
                Year = x.YearNo,
                LinesCount = data1.Count()
            });
            return data.AsQueryable();
        }

        public IEnumerable<MDailyStockReport> GetDailyStockReport(DateTime? sdate, DateTime? edate, int? siteid, int? itemCatID, int? itemid, int? sizeid, int? brandid)
        {
            var data1 = dc.GetDailySiteWiseStockReport(sdate, edate, siteid, itemCatID, itemid, sizeid, brandid).ToList()
                .OrderByDescending(x => x.ForDate);
            var data = data1.Select(x => new MDailyStockReport()
            {
                ClosingBalance = x.ClosingBalance,
                AccFor = x.AccFor,
                AccForID = x.AccForID,
                AccForName = x.AccForName,
                ForDate = x.ForDate,
                ItemID = x.ItemID,
                SizeID = x.SizeID,
                BrandID = x.BrandID,
                StockIn = x.StockIn,
                StockOut = x.StockOut,
                TransactionHeaderID = x.HeaderID,
                TransactionID = x.ID,
                Type = x.TransactionType,
                TypeID = x.TransactionTypeID,
                TypeHeaderID = x.TransactionTypeHeaderID,
                LinesCount = data1.Count()
            });
            return data.AsQueryable();
        }

        public IEnumerable<MMIReportSummary> GetMIReportSummary(DateTime? sdate, DateTime? edate, int? itemCatID, int? itemid, int? sizeid, int? brandid, int? siteid, string Contype, string status, bool showzero)
        {
            var data1 = dc.MIReportSummary(sdate, edate, itemCatID, itemid, sizeid, brandid, siteid, Contype, status, showzero)
                .OrderBy(x => x.DisplayItem).ToList();
            var data = data1.Select(x => new MMIReportSummary()
            {
                Brand = x.Brand,
                BrandID = x.BrandID,
                ConsumableType = x.ConsumableType,
                DisplayItem = x.DisplayItem,
                Item = x.Item,
                ItemID = x.ItemID,
                MRNQty = x.MRNQty ?? 0,
                CPQty = x.CPQty ?? 0,
                NumberofEntries = x.NumberOfEntries.Value,
                POQty = x.POQty ?? 0,
                RecordsCount = data1.Count(),
                Size = x.Size,
                SizeID = x.SizeID,
                TobePOQty = x.TobePO_Qty ?? 0,
                TODispatchQty = x.TO_DispatchQty ?? 0,
                TOQty = x.TOQty ?? 0,
                TotalMIQty = x.Total_MIQty ?? 0,
                QtyInCloseMIs = x.QtyInCloseMIs ?? 0
            });
            return data.AsQueryable();
        }

        public IEnumerable<MMIReportSiteWiseSummary> GetMIReportSiteWiseSummary(DateTime? sdate, DateTime? edate, int? itemCatID, int? itemid, int? sizeid, int? brandid, int? siteid, string Contype, string status, bool showzero)
        {
            dc.CommandTimeout = 5000;
            var data1 = dc.MIReportSiteWiseSummary(sdate, edate, itemCatID, itemid, sizeid, brandid, siteid, Contype, status, showzero)
                .OrderBy(x => x.Site).ToList();
            var data = data1.Select(x => new MMIReportSiteWiseSummary()
            {
                Site = x.Site,
                SiteID = x.SiteID,
                Brand = x.Brand,
                BrandID = x.BrandID,
                ConsumableType = x.ConsumableType,
                DisplayItem = x.DisplayItem,
                Item = x.Item,
                ItemID = x.ItemID,
                MRNQty = x.MRNQty ?? 0,
                CPQty = x.CPQty ?? 0,
                NumberofEntries = x.NumberOfEntries.Value,
                POQty = x.POQty ?? 0,
                RecordsCount = data1.Count(),
                Size = x.Size,
                SizeID = x.SizeID,
                TobePOQty = x.TobePO_Qty ?? 0,
                TODispatchQty = x.TO_DispatchQty ?? 0,
                TOQty = x.TOQty ?? 0,
                TotalMIQty = x.Total_MIQty ?? 0,
                QtyInCloseMIs = x.QtyInCloseMIs ?? 0
            });
            return data.AsQueryable();
        }

        public IEnumerable<MMIReportSummaryLine> GetMIReportSummaryDetails(DateTime? sdate, DateTime? edate, int? itemCatID, int? itemid, int? sizeid, int? brandid, int? siteid, string Contype, string status)
        {
            var data1 = dc.MIReportSummaryDetails(sdate, edate, itemCatID, itemid, sizeid, brandid, siteid, Contype, status).ToList()
                .OrderBy(x => x.DisplayItem);
            var data = data1.Select(x => new MMIReportSummaryLine()
            {
                ConvertedToPO = x.ConvertedToPO,
                IsDeleted = x.IsDeleted,
                IsSplittedToTO = x.IsSplittedtoTO,
                PRID = x.PRID,
                PRLineID = x.PRLineID,
                PRRefNo = x.PRRefNo,
                RaisedOn = x.RaisedOn,
                Site = x.Site,
                SiteID = x.SiteID,
                Status = x.Status,
                Brand = x.Brand,
                BrandID = x.BrandID,
                ConsumableType = x.ConsumableType,
                DisplayItem = x.DisplayItem,
                Item = x.Item,
                ItemID = x.ItemID,
                MRNQty = x.MRNQty ?? 0,
                MOID = x.MOID,
                MOLineID = x.MOLineID,
                MORefNo = x.MORefNo ?? "N/A",
                POQty = x.POQty ?? 0,
                RecordsCount = data1.Count(),
                Size = x.Size,
                SizeID = x.SizeID,
                TobePOQty = x.TobePO_Qty ?? 0,
                TODisplatchQty = x.TO_DispatchQty ?? 0,
                TOQty = x.TOQty,
                TotalMIQty = x.Total_MIQty,
                QtyInCloseMIs = x.QtyInCloseMIs ?? 0
            });
            return data.AsQueryable();
        }

        public IEnumerable<MMIReportSummary> GetMIDashBoard(DateTime? sdate, DateTime? edate, int? itemid, int? sizeid, int? brandid, int? siteid, string Contype)
        {
            var data1 = dc.QtyReport(sdate, edate, itemid, sizeid, brandid, siteid, Contype).ToList()
                .OrderBy(x => x.DisplayItem);
            var data = data1.Select(x => new MMIReportSummary()
            {
                Brand = x.Brand,
                BrandID = x.BrandID,
                ConsumableType = x.ConsumableType,
                DisplayItem = x.DisplayItem,
                Item = x.Item,
                ItemID = x.ItemID,
                MRNQty = x.GRNQty ?? 0,
                //CPQty = x.CPQty ?? 0,
                //NumberofEntries = x.NumberOfEntries.Value,
                POQty = x.POQty ?? 0,
                RecordsCount = data1.Count(),
                SiteID = x.SiteID,
                Site = x.Site,
                Size = x.Size,
                SizeID = x.SizeID,
                TobePOQty = x.ToBePOQty ?? 0,
                TODispatchQty = x.TODispatched ?? 0,
                TOQty = x.TOQty ?? 0,
                TotalMIQty = x.RaisedQty ?? 0
            });
            return data.AsQueryable();
        }

        public IEnumerable<MStockBreakUp> GetStockBreakUpReport(DateTime? sdate, DateTime? edate, int? itemid, int? itemCatID, int? sizeid, int? brandid, int? siteid, string Contype)
        {
            var data1 = dc.GetStockFurcation(sdate, edate, siteid, itemid, itemCatID, sizeid, brandid, Contype)
                .OrderBy(x => x.Item).ToList();
            var data = data1.Select(x => new MStockBreakUp()
            {
                Site = x.DisplaySite,
                SiteID = x.SiteID,
                Brand = x.Brand,
                BrandID = x.BrandID,
                ConsumableType = x.ConsumableType,
                Item = x.Item,
                ItemID = x.ItemID,
                MRNQty = x.MRNQty ?? 0,
                Size = x.Size,
                SizeID = x.SizeID,
                CPQty = x.CPQty ?? 0,
                ReceivedQty = x.ReceivedQty ?? 0,
                ReleasedQty = x.ReleasedQty ?? 0,
                ToBeReceivedQty = x.ToBeReceiveQty ?? 0,
                ConsumedQty = 0
            });
            return data.AsQueryable();
        }

        public IEnumerable<MDailyStockBreakUp> GetDailyStockBreakUpReport(DateTime? sdate, DateTime? edate, int? itemid, int? itemCatID, int? sizeid, int? brandid, int? siteid, string Contype)
        {
            var data1 = dc.GetDailyStockFurcation(sdate, edate, siteid, itemid, itemCatID, sizeid, brandid, Contype).ToList()
                .OrderBy(x => x.Item);
            var data = data1.Select(x => new MDailyStockBreakUp()
            {
                Site = x.DisplaySite,
                SiteID = x.SiteID,
                Brand = x.Brand ?? "N/A",
                BrandID = x.BrandID,
                ConsumableType = x.ConsumableType,
                Item = x.Item ?? "N/A",
                ItemID = x.ItemID,
                Size = x.Size ?? "N/A",
                SizeID = x.SizeID,
                CPID = x.CPID,
                CPLineID = x.CPLineID,
                CPRefNo = x.CPRefNo,
                CPQty = x.CPQty ?? 0,
                RecMOID = x.RecMOID,
                RecMOLineID = x.RecMOLineID,
                RecMORefNo = x.RecMORefNo,
                ReceivedQty = x.ReceivedQty ?? 0,
                RelMOID = x.RelMOID,
                RelMOLineID = x.RelMOLineID,
                RelMORefNo = x.RelMORefNo,
                ReleasedQty = x.ReleasedQty ?? 0,
                MOID = x.MOID,
                MOLineID = x.MOLineID,
                MORefNo = x.MORefNo,
                ToBeReceivedQty = x.ToBeReceiveQty ?? 0,
                MRNID = x.MRNID,
                MRNLineID = x.MRNLineID,
                MRNRefNo = x.MRNRefNo,
                MRNQty = x.MRNQty ?? 0
            });
            return data.AsQueryable();
        }
    }
}
