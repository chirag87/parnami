﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace SIMS.MM.BLL.ReportingModels
{
    [DataContract]
    class MCPReport
    {
        [DataMember]
        public int RefNo { set; get; }

        [DataMember]
        public DateTime Date { get; set; }

        [DataMember]
        public string Vender { get; set; }

        [DataMember]
        public string Site { get; set; }

        [DataMember]
        public string Purchaser { get; set; }

        [DataMember]
        public string item { get; set; }

        [DataMember]
        public double Quantity { get; set; }

        [DataMember]
        public string MU { get; set; }

        [DataMember]
        public double PerUnitPrice { get; set; }

        [DataMember]
        public double TotalAmount { get; set; }

        [DataMember]
        public int InvoiceNo { get; set; }

        [DataMember]
        public string Remarks { get; set; }

    }
}
