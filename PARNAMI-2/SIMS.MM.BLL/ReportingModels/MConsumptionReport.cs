﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace SIMS.MM.BLL.ReportingModels
{
    [DataContract]
    public class MConsumptionReport
    {
        [DataMember]
        public int RefNo { get; set; }

        [DataMember]
        public DateTime DateFrom { get; set; }

        [DataMember]
        public DateTime DateTo { get; set; }

        [DataMember]
        public string Site { get; set; }

        [DataMember]
        public string Item { get; set; }

        [DataMember]
        public string MU { get; set; }

        [DataMember]
        public double OpeningBalnce { get; set; }

        [DataMember]
        public double Consumption { get; set; }


    }
}
