﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace SIMS.MM.BLL.ReportingModels
{
    [DataContract]
    public class MGRNReport 
    {
        [DataMember]
        public int SNO { get; set; }

        [DataMember]
        public string Vendor { get; set; }

        [DataMember]
        public int GRN { get; set; }

        [DataMember]
        public string Site { get; set; }

        [DataMember]
        public DateTime DateofReceipt { get; set; }

        [DataMember]
        public string itemReceived { get; set; }     
     
        [DataMember]
        public double QtyReceived { get; set; }

        [DataMember]
        public string MU { get; set; }

        [DataMember]
        public double PerUnitPrice { get; set; }

        [DataMember]
        public double TotalAmount { get; set; }

        [DataMember]
        public int PRNO { get; set; }

        [DataMember]
        public int PONO { get; set; }

        [DataMember]
        public string Remarks { get; set; }
    }
}
