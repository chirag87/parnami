﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SIMS.MM.BLL
{
    using DAL;
    using MODEL;
    using System.Collections.ObjectModel;
    public class MOManager : IMOManager
    {
        parnamidb1Entities db = new parnamidb1Entities();
        MMReportsDBDataContext dc = new MMReportsDBDataContext();
        MODataProvider provider = new MODataProvider();

        public MMO CreateNewMO(MMO newMO)
        {
            var qry = ConvertTOMO(newMO);
            qry.ID = db.GetMAX_MTR_ID() + 1;
            qry.CurrentOwnerID = "dba";
            qry.Year = DateTime.UtcNow.AddHours(5.5).Year;
            qry.SiteID = db.GetSiteCode(qry.RequestedSiteID);//???
            qry.RID = db.GetMaxMORID(qry.Year, qry.RequestedSiteID, qry.ConsumableType) + 1;
            if (qry.ConsumableType == "Consumable")
                qry.ReferenceNo = "TO/" + qry.Year + "/S-" + qry.SiteID + "/C-" + qry.RID;
            if (qry.ConsumableType == "Returnable")
                qry.ReferenceNo = "TO/" + qry.Year + "/S-" + qry.SiteID + "/R-" + qry.RID;
            db.MTRs.AddObject(qry);
            db.SaveChanges();
            return provider.GetMObyId(qry.ID);
        }

        public MMO SplitToMO(MMO NewMO, int OldPRID, ObservableCollection<int> OldPRLines)
        {
            var qry = ConvertTOMO(NewMO);
            NewMO.UpdateLineNumbers();
            qry.ID = db.GetMAX_MTR_ID() + 1;
            qry.CurrentOwnerID = "dba";
            qry.Year = DateTime.UtcNow.AddHours(5.5).Year;
            qry.SiteID = db.GetSiteCode(qry.RequestedSiteID);//???
            //qry.RID = db.GetMaxRRID("Consumption", qry.Year, qry.BUID) + 1;
            qry.ReferenceNo = "TO/" + qry.Year + "/S-" + qry.SiteID + "/" + qry.ID;
            db.MTRs.AddObject(qry);
            db.SaveChanges();

            foreach (var LineID in OldPRLines)
            {
                var data = db.PRDetails.Single(x => x.PRID == OldPRID && x.ID == LineID);
                if (data != null)
                {
                    db.PRDetails.DeleteObject(data);
                    db.SaveChanges();
                }
            }

            return provider.GetMObyId(qry.ID);
        }

        //public MMO SplitToMOwithPartialQty(MMO NewMO, int OldPRID, ObservableCollection<MSplittedLines> SplittedLines)
        //{
        //    NewMO.MOLines = new ObservableCollection<MMOLine>();
        //    var oldpr = db.PRs.Single(x => x.ID == OldPRID);
        //    MTR newmtr = new MTR();
        //    newmtr = ConvertTOMO(NewMO);

        //    var FullSplittedLines = SplittedLines.Where(x => !x.IsPartial).ToList();
        //    var PartialSplittedLines = SplittedLines.Where(x => x.IsPartial && x.ConfirmedQty > 0).ToList();

        //    if (FullSplittedLines.Count > 0)
        //    {
        //        foreach (var line in FullSplittedLines)
        //        {
        //            var oldline = db.PRDetails.Single(x => x.ID == line.LineID && x.PRID == OldPRID);
        //            MTRLine newline = new MTRLine();
        //            newline = ConvertfromPRLineToMOLine(oldline, NewMO.ID);
        //            newmtr.MTRLines.Add(newline);
        //            oldpr.PRDetails.Remove(oldline);
        //        }
        //    }


        //    if (PartialSplittedLines.Count > 0)
        //    {
        //        foreach (var line in PartialSplittedLines)
        //        {
        //            var oldline = db.PRDetails.Single(x => x.ID == line.LineID && x.PRID == OldPRID);
        //            oldline.Qty = line.DiffQty;

        //            MTRLine newline = new MTRLine();
        //            newline = ConvertfromPRLineToMOLine(oldline, NewMO.ID);
        //            newline.RequiredQty = line.ConfirmedQty;
        //            newmtr.MTRLines.Add(newline);
        //        }
        //    }

        //    if (oldpr.PRDetails.Count == 0)
        //    {
        //        // Close OLD Pr
        //        oldpr.IsClosed = true;
        //        oldpr.Status = "Closed";
        //    }
        //    newmtr.RaisedOn = DateTime.UtcNow.AddHours(5.5);
        //    newmtr.ID = db.GetMAX_MTR_ID() + 1;
        //    newmtr.CurrentOwnerID = "dba";
        //    newmtr.SiteID = db.GetSiteCode(newmtr.RequestedSiteID);//???
        //    newmtr.Year = DateTime.UtcNow.AddHours(5.5).Year;
        //    //newmtr.RID = db.GetMaxMORID(newmtr.Year, newmtr.RequestedSiteID) + 1;
        //    //newmtr.ReferenceNo = "TO/" + newmtr.Year + "/S-" + newmtr.SiteID + "/" + newmtr.RID;
        //    newmtr.RID = db.GetMaxMORID(newmtr.Year, newmtr.RequestedSiteID, newmtr.ConsumableType) + 1;
        //    if (newmtr.ConsumableType == "Consumable")
        //        newmtr.ReferenceNo = "TO/" + newmtr.Year + "/S-" + newmtr.SiteID + "/C-" + newmtr.RID;
        //    if (newmtr.ConsumableType == "Returnable")
        //        newmtr.ReferenceNo = "TO/" + newmtr.Year + "/S-" + newmtr.SiteID + "/R-" + newmtr.RID;
        //    int i = 1;
        //    foreach (var item in newmtr.MTRLines)
        //    {
        //        item.LineID = i;
        //        i++;
        //    }
        //    db.MTRs.AddObject(newmtr);
        //    db.SaveChanges();
        //    return provider.GetMObyId(newmtr.ID);
        //}



        //public MMO SplitToMOwithPartialQty(MMO NewMO, int OldPRID, ObservableCollection<MMOSplittedLine> MOSplittedLines)
        //{
        //    ObservableCollection<PRtoMOConvertion> PRtoMOs = new ObservableCollection<PRtoMOConvertion>(); // For Entry in PRtoMOConvertions Table
        //    NewMO.MOLines = new ObservableCollection<MMOLine>();

        //    var oldpr = db.PRs.Single(x => x.ID == OldPRID);
        //    MTR newmtr = new MTR();
        //    newmtr = ConvertTOMO(NewMO); // Prepare newmtr object From NewMO Object

        //    this.ProcessFullSplittedLines(MOSplittedLines.Where(x => !x.IsPartial).ToList(), ref PRtoMOs, ref newmtr, oldpr, NewMO);    //Processing SplittedLines with Full Qunatity
        //    this.ProcessPartialSplittedLines(MOSplittedLines.Where(x => x.IsPartial && x.ConfirmedQty > 0).ToList(), ref PRtoMOs, ref newmtr, oldpr, NewMO);    //Processing SplittedLines with Partial Qunatity

        //    if (oldpr.PRDetails.All(x => x.PRDetails_AdditionalInfo.IsDeleted == true))
        //    {
        //        // *** Close OLD PR ***//
        //        oldpr.IsClosed = true;
        //        oldpr.Status = "Closed";
        //    }

        //    MTRLine mtrline = null;
        //    /*Get First MTR Line of Last Created MTR on the basis of Sites and ConsumableType*/
        //    mtrline = db.GetMTRLine(NewMO.TransferringSiteID, NewMO.RequestingSiteID, NewMO.ConsumableType);

        //    if (mtrline != null)
        //    {
        //        List<MTRLine> tempList = new List<MTRLine>();

        //        /*Get MTR associated with the First matched MTRLine */
        //        MTR mtr = db.MTRs.Single(x => x.ID == mtrline.MTRID);
        //        var mtrlines = mtr.MTRLines;
        //        int count = mtrlines.Count();

        //        foreach (var line in newmtr.MTRLines)
        //        {
        //            var item = mtrlines.SingleOrDefault(x => x.ItemID == line.ItemID && x.SizeID == line.SizeID && x.BrandID == line.BrandID);
        //            if (item != null)
        //            {
        //                item.RequiredQty = item.RequiredQty + line.RequiredQty;
        //            }
        //            else
        //            {
        //                tempList.Add(line);
        //            }
        //        }

        //        foreach (var line in tempList)
        //        {
        //            line.LineID = ++count;
        //            mtr.MTRLines.Add(line);
        //        }

        //        db.SaveChanges();
        //        return provider.GetMObyId(mtrline.MTR.ID);
        //    }

        //    else
        //    {
        //        db.MTRs.AddObject(newmtr);
        //        db.SaveChanges();

        //        /*Putting Entries in PRtoMOConvertions*/
        //        foreach (var item in PRtoMOs)
        //        {
        //            db.PRtoMOConvertions.AddObject(item);
        //        }
        //        db.SaveChanges();
        //        return provider.GetMObyId(newmtr.ID);
        //    }
        //}

        //private void ProcessFullSplittedLines(List<MMOSplittedLine> FullSplittedLines, ref ObservableCollection<PRtoMOConvertion> PRtoMOs, ref MTR newmtr, PR oldpr, MMO NewMO)
        //{
        //    if (FullSplittedLines.Count < 0) return;

        //    foreach (var line in FullSplittedLines)
        //    {
        //        var PRtoMO = new PRtoMOConvertion();
        //        var oldline = oldpr.PRDetails.Single(x => x.ID == line.ID);
        //        var oldpradditionalline = oldline.PRDetails_AdditionalInfo;

        //        MTRLine newline = new MTRLine();
        //        newline = ConvertfromPRLineToMOLine(oldline, NewMO.ID);
        //        newline.ConsumableType = line.ConsumableType;
        //        newmtr.MTRLines.Add(newline);

        //        //oldpr.PRDetails.Remove(oldline);

        //        /**********************************************************************************************/
        //        PrepareNewMTR(ref newmtr);
        //        PrepareNewMTR(ref PRtoMO, ref newmtr, ref oldline, oldline.Qty, ref newline);      /*Entring for PRtoMOConvertions Table*/
        //        PRtoMOs.Add(PRtoMO);
        //        PRtoMO = new PRtoMOConvertion();
        //        /**********************************************************************************************/

        //        oldpradditionalline.IsSplittedtoTO = true;  //Update PRDetails_AdditionalInfo Table
        //        oldpradditionalline.IsDeleted = true;
        //        oldpradditionalline.SplittedQty = oldpradditionalline.SplittedQty + oldline.Qty;
        //        oldline.Qty = 0;
        //    }
        //}

        //private void ProcessPartialSplittedLines(List<MMOSplittedLine> PartialSplittedLines, ref ObservableCollection<PRtoMOConvertion> PRtoMOs, ref MTR newmtr, PR oldpr, MMO NewMO)
        //{
        //    if (PartialSplittedLines.Count < 0) return;

        //    foreach (var line in PartialSplittedLines)
        //    {
        //        var PRtoMO = new PRtoMOConvertion();
        //        var oldline = oldpr.PRDetails.Single(x => x.ID == line.ID);
        //        var oldpradditionalline = oldline.PRDetails_AdditionalInfo;

        //        oldpradditionalline.IsSplittedtoTO = true;  //Update PRDetails_AdditionalInfo Table
        //        oldpradditionalline.SplittedQty = oldpradditionalline.SplittedQty + line.ConfirmedQty;
        //        oldline.Qty = line.DiffQty;

        //        MTRLine newline = new MTRLine();
        //        newline = ConvertfromPRLineToMOLine(oldline, NewMO.ID);
        //        newline.RequiredQty = line.ConfirmedQty;
        //        newline.ConsumableType = line.ConsumableType;
        //        newmtr.MTRLines.Add(newline);

        //        /**********************************************************************************************/
        //        PrepareNewMTR(ref newmtr);
        //        PrepareNewMTR(ref PRtoMO, ref newmtr, ref oldline, line.ConfirmedQty, ref newline);  /*Entring for PRtoMOConvertions Table*/
        //        PRtoMOs.Add(PRtoMO);
        //        PRtoMO = new PRtoMOConvertion();
        //        /**********************************************************************************************/
        //    }
        //}

        //private MMO ProcessIfOldMOExists(MMO NewMO, MTR newmtr)
        //{
        //    MTRLine mtrline = null;
        //    /*Get First MTR Line of Last Created MTR on the basis of Sites and ConsumableType*/
        //    mtrline = db.GetMTRLine(NewMO.TransferringSiteID, NewMO.RequestingSiteID, NewMO.ConsumableType);

        //    if (mtrline == null) return new MMO();

        //    List<MTRLine> tempList = new List<MTRLine>();

        //    /*Get MTR associated with the First matched MTRLine */
        //    MTR mtr = db.MTRs.Single(x => x.ID == mtrline.MTRID);
        //    var mtrlines = mtr.MTRLines;
        //    int count = mtrlines.Count();

        //    foreach (var line in newmtr.MTRLines)
        //    {
        //        var item = mtrlines.SingleOrDefault(x => x.ItemID == line.ItemID && x.SizeID == line.SizeID && x.BrandID == line.BrandID);
        //        if (item != null)
        //        {
        //            item.RequiredQty = item.RequiredQty + line.RequiredQty;
        //        }
        //        else
        //        {
        //            tempList.Add(line);
        //        }
        //    }

        //    foreach (var line in tempList)
        //    {
        //        line.LineID = ++count;
        //        mtr.MTRLines.Add(line);
        //    }
        //    db.SaveChanges();
        //    return provider.GetMObyId(mtrline.MTR.ID);
        //}

        public MMO SplitToMOwithPartialQty(MMO NewMO, int OldPRID, ObservableCollection<MMOSplittedLine> MOSplittedLines)
        {
            ObservableCollection<PRtoMOConvertion> PRtoMOs = new ObservableCollection<PRtoMOConvertion>(); // For Entry in PRtoMOConvertions Table

            var oldpr = db.PRs.Single(x => x.ID == OldPRID);    // Get PR from Database on the Basis of OldPRID

            MTR newmtr = new MTR();
            NewMO.MOLines = new ObservableCollection<MMOLine>();    // NewMO Object doesn't carry Lines, as those are available here as MOSpliitedLines
            newmtr = ConvertTOMO(NewMO);    // Prepare newmtr object From NewMO Object

            /*Check if any MO with Same specifications (ReqSite, TranfSite and ConsumptionType) exists in Database*/

            MTRLine mtrline = null;
            mtrline = db.GetMTRLine(NewMO.TransferringSiteID, NewMO.RequestingSiteID, NewMO.ConsumableType); //Get First MTR Line of Last Created MTR on the basis of Sites and ConsumableType

            #region Modify Old MTR

            if (mtrline != null)    //If YES, then process Old MTR
            {
                MTR mtr = db.MTRs.Single(x => x.ID == mtrline.MTRID);   //Get MTR associated with the First matched MTRLine
                int count = mtr.MTRLines.Count();                       //Get count of Line available in matched MTR

                foreach (var splittedline in MOSplittedLines)
                {
                    /*Check if in this MTR, any line matches the Item, Size and Make same as any of the MOSplittedLines*/
                    var matchedmtrline = mtr.MTRLines.Where(x => x.ItemID == splittedline.ItemID && x.SizeID == splittedline.SizeID && x.BrandID == splittedline.BrandID).FirstOrDefault();

                    if (matchedmtrline != null)   //If YES, then add the Quantity of SplittedLine into the Old Line
                    {
                        matchedmtrline.RequiredQty = matchedmtrline.RequiredQty + splittedline.ConfirmedQty;
                        ModifyOldPR(ref PRtoMOs, ref oldpr, splittedline, ref mtr, matchedmtrline.LineID);   //Modification in Old PRDetails Quantities as well as Entry Updation in PRDetails_AdditionalInfo Table
                    }
                    else            //If NO, then add SplittedLine as New Line in OLD MTRLine
                    {
                        splittedline.LineID = ++count;
                        mtr.MTRLines.Add(ConvertFromMOSplittedLineToMTRLine(splittedline));
                        ModifyOldPR(ref PRtoMOs, ref oldpr, splittedline, ref mtr, splittedline.LineID);   //Modification in Old PRDetails Quantities as well as Entry Updation in PRDetails_AdditionalInfo Table
                    }
                }

                CloseOldPR(ref oldpr);  // Close Old PR if all Lines are tranferred in MO

                /*Putting Entries in PRtoMOConvertions*/
                foreach (var item in PRtoMOs)
                {
                    db.PRtoMOConvertions.AddObject(item);
                }

                db.SaveChanges();

                return provider.GetMObyId(mtrline.MTR.ID);
            }

            #endregion

            #region Create New MTR

            else    //If NO, then Create New MTR
            {
                newmtr = PrepareNewMTR(ref newmtr);

                int i = 1;  // Modifying Line Numbers
                foreach (var splittedline in MOSplittedLines)
                {
                    splittedline.LineID = i;
                    newmtr.MTRLines.Add(ConvertFromMOSplittedLineToMTRLine(splittedline));
                    ModifyOldPR(ref PRtoMOs, ref oldpr, splittedline, ref newmtr, i);
                    i++;
                }
                db.MTRs.AddObject(newmtr);

                CloseOldPR(ref oldpr);  // Close Old PR if all Lines are tranferred in MO

                /*Putting Entries in PRtoMOConvertions*/
                foreach (var item in PRtoMOs)
                {
                    db.PRtoMOConvertions.AddObject(item);
                }

                db.SaveChanges();
                return provider.GetMObyId(newmtr.ID);
            }

            #endregion
        }

        private MTRLine ConvertFromMOSplittedLineToMTRLine(MMOSplittedLine line)
        {
            MTRLine mtrline = new MTRLine()
            {
                ItemID = line.ItemID,
                SizeID = line.SizeID,
                BrandID = line.BrandID,
                LineID = line.LineID,
                ConsumableType = line.ConsumableType,
                RequiredQty = line.ConfirmedQty,
                ApprovedQty = 0,
                ReleasedQty = 0,
                TransferredQty = 0,
                InTransitQty = 0,
                Remarks = line.Remarks,
                ChallanNo = null
            };
            return mtrline;
        }

        public void ModifyOldPR(ref ObservableCollection<PRtoMOConvertion> PRtoMOs, ref PR oldpr, MMOSplittedLine MOSplittedLine, ref MTR mtr, int molineid)
        {
            var oldline = oldpr.PRDetails.Single(x => x.ID == MOSplittedLine.ID);
            var oldpradditionalline = oldline.PRDetails_AdditionalInfo;

            /*Update PRDetails_AdditionalInfo Table*/
            //if (!MOSplittedLine.IsPartial)
            if (oldline.Qty - MOSplittedLine.ConfirmedQty == 0)
            {
                oldpradditionalline.IsDeleted = true;
            }
            oldpradditionalline.IsSplittedtoTO = true;
            oldpradditionalline.SplittedQty = oldpradditionalline.SplittedQty + MOSplittedLine.ConfirmedQty;
            /*Update PRDetails Table*/
            //oldline.Qty = MOSplittedLine.DiffQty;
            oldline.Qty = oldline.Qty - MOSplittedLine.ConfirmedQty;

            PRtoMOs.Add(PreparePRtoMOConvertion(ref mtr, ref oldline, MOSplittedLine.ConfirmedQty, molineid));
        }

        public void CloseOldPR(ref PR oldpr)
        {
            if (oldpr.PRDetails.All(x => x.PRDetails_AdditionalInfo.IsDeleted == true))
            {
                // *** Close OLD PR ***//
                oldpr.IsClosed = true;
                oldpr.Status = "Closed";
            }
        }

        public MTR PrepareNewMTR(ref MTR newmtr)
        {
            newmtr.RaisedOn = DateTime.UtcNow.AddHours(5.5);
            newmtr.ID = db.GetMAX_MTR_ID() + 1;
            newmtr.CurrentOwnerID = "dba";
            newmtr.SiteID = db.GetSiteCode(newmtr.RequestedSiteID);//???
            newmtr.Year = DateTime.UtcNow.AddHours(5.5).Year;
            newmtr.RID = db.GetMaxMORID(newmtr.Year, newmtr.RequestedSiteID, newmtr.ConsumableType) + 1;
            if (newmtr.ConsumableType == "Consumable")
                newmtr.ReferenceNo = "TO/" + newmtr.Year + "/S-" + newmtr.SiteID + "/C-" + newmtr.RID;
            if (newmtr.ConsumableType == "Returnable")
                newmtr.ReferenceNo = "TO/" + newmtr.Year + "/S-" + newmtr.SiteID + "/R-" + newmtr.RID;

            int i = 1;
            foreach (var item in newmtr.MTRLines)
            {
                item.LineID = i;
                i++;
            }
            return newmtr;
        }

        public PRtoMOConvertion PreparePRtoMOConvertion(ref MTR mtr, ref PRDetail olrPRLine, double ShiftedQty, int lineid)
        {
            var PRtoMO = new PRtoMOConvertion();

            PRtoMO.PRID = olrPRLine.PRID;
            PRtoMO.PRRefNo = olrPRLine.PR.ReferenceNo;
            PRtoMO.PRRaisedOn = olrPRLine.PR.RaisedOn;
            PRtoMO.PRLineID = olrPRLine.ID;

            PRtoMO.RaisedQty = olrPRLine.Qty + ShiftedQty;
            PRtoMO.SplittedToMOQty = ShiftedQty;

            PRtoMO.MOID = mtr.ID;
            PRtoMO.MORefNo = mtr.ReferenceNo;
            PRtoMO.MOSplittedOn = mtr.RaisedOn;
            PRtoMO.MOLineID = lineid;

            return PRtoMO;
        }

        public PRtoMOConvertion ConvertToPRtoMOConvertion(MPRtoMOConvertion x)
        {
            PRtoMOConvertion obj = new PRtoMOConvertion()
            {
                ID = x.ID,
                MOID = x.MOID,
                MOLineID = x.MOLineID,
                MORefNo = x.MORefNo,
                MOSplittedOn = x.MOSplittedOn,
                PRID = x.PRID,
                PRLineID = x.PRLineID,
                PRRaisedOn = x.PRRaisedOn,
                PRRefNo = x.PRRefNo,
                RaisedQty = x.RaisedQty,
                SplittedToMOQty = x.SplittedQty
            };
            return obj;
        }

        public MTRLine ConvertfromPRLineToMOLine(PRDetail x, int mtrID)
        {
            MTRLine line = new MTRLine()
            {
                LineID = x.LineID,
                MTRID = mtrID,
                ItemID = x.ItemID,
                SizeID = x.SizeID,
                BrandID = x.BrandID,
                ConsumableType = x.ConsumableType,
                RequiredQty = x.Qty,
                ApprovedQty = 0,
                ReleasedQty = 0,
                TransferredQty = 0,
                InTransitQty = 0,
                Remarks = x.Remarks
            };
            return line;
        }

        MTR ConvertTOMO(MMO _mmo)
        {
            MTR mo = new MTR()
            {
                ID = _mmo.ID,
                Remarks = _mmo.Remarks,
                RequestedBy = _mmo.Requester,
                RequestedSiteID = _mmo.RequestingSiteID,
                TransferringSiteID = _mmo.TransferringSiteID,
                TransferBefore = _mmo.TransferDate,
                RaisedBy = _mmo.RaisedBy,
                RaisedOn = _mmo.RaisedOn,
                ConsumableType = _mmo.ConsumableType
            };
            if (_mmo.MOLines != null) _mmo.MOLines.ToList()
                  .ForEach(x => mo.MTRLines.Add(ConvertToMOLine(x, _mmo.ConsumableType)));
            db.SaveChanges();
            return mo;
        }

        MTRLine ConvertToMOLine(MMOLine x, string ct)
        {
            MTRLine line = new MTRLine()
            {
                LineID = x.LineID,
                ItemID = x.ItemID,
                InTransitQty = 0,
                ReleasedQty = 0,
                TransferredQty = 0,
                SizeID = x.SizeID,
                BrandID = x.BrandID,
                Remarks = x.LineRemarks,
                RequiredQty = x.Quantity,
                ConsumableType = ct,
                MTRID = x.ID//?
            };
            return line;
        }

        //MTRLine ConvertFromMMOSplittedLineToMOLine(MMOSplittedLine x)
        //{
        //    MTRLine line = new MTRLine()
        //    {
        //        LineID = x.LineID,
        //        ItemID = x.ItemID,
        //        InTransitQty = 0,
        //        ReleasedQty = 0,
        //        TransferredQty = 0,
        //        SizeID = x.SizeID,
        //        BrandID = x.BrandID,
        //        Remarks = x.Remarks,
        //        RequiredQty = x.ConfirmedQty,
        //        ConsumableType = x.ConsumableType,
        //        //MTRID = x.MOID//?
        //    };
        //    return line;
        //}

        //public void ReleaseMO(int moid)
        //{
        //   var data= db.MTRs.Single(x=>x.ID==moid);
        //   data.IsReleased = true;
        //   data.ReleasedOn = DateTime.UtcNow.AddHours(5.5);
        //   data.ReleasedBy = "dba";
        //   db.SaveChanges();
        //}

        public void ReceivedMO(int moid)
        {
            throw new NotImplementedException();
        }

        //public string CreateMORelease(MReceiving mrec)
        //{
        //    ReceivingManager rm = new ReceivingManager();
        //    mrec.TransactionID = db.GetMaxMOId("MORelease") + 1;
        //    mrec.TransactionTypeID = "MORelease";
        //    mrec.CreatedBy = "dba";
        //    mrec.LastUpdatedBy = "dba";
        //    mrec.LastUpdaetdOn = DateTime.UtcNow.AddHours(5.5);
        //    mrec.CreatedOn = DateTime.UtcNow.AddHours(5.5);
        //    var rec = rm.ConvertToINTR(mrec);
        //    //rec.CreatedBy = mrec.Purchaser;
        //    //rec.LastUpdatedBy = mrec.Purchaser;
        //    rec.Year = DateTime.UtcNow.AddHours(5.5).Year;
        //    rec.SiteID = db.GetSiteCode(rec.BUID);//???-------------
        //    rec.RID = db.GetMaxRRID("MORelease", rec.Year, rec.BUID) + 1;
        //    rec.ReferenceNo = rec.TypeID + "/" + rec.Year + "/S-" + rec.SiteID + "/" + rec.RID;
        //    rec.TransitID = 1;
        //    db.InventoryTransactions.AddObject(rec);
        //    db.SaveChanges();
        //    return rec.ReferenceNo;
        //}

        //public int CreateMOReceive(MReceiving mrec)
        //{
        //    ReceivingManager rm = new ReceivingManager();
        //    mrec.TransactionID = db.GetMaxMOId("MOReceive") + 1;
        //    mrec.TransactionTypeID = "MOReceive";
        //    mrec.CreatedBy = "dba";
        //    mrec.LastUpdatedBy = "dba";
        //    mrec.LastUpdaetdOn = DateTime.UtcNow.AddHours(5.5);
        //    mrec.CreatedOn = DateTime.UtcNow.AddHours(5.5);
        //    var rec = rm.ConvertToINTR(mrec);
        //    //rec.CreatedBy = mrec.Purchaser;
        //    // rec.LastUpdatedBy = mrec.Purchaser;
        //    rec.Year = DateTime.UtcNow.AddHours(5.5).Year;
        //    rec.SiteID = db.GetSiteCode(rec.BUID);//???-------------
        //    rec.RID = db.GetMaxRRID("MOReceive", rec.Year, rec.BUID) + 1;
        //    rec.ReferenceNo = rec.TypeID + "/" + rec.Year + "/S" + rec.SiteID + "/" + rec.RID;
        //    db.InventoryTransactions.AddObject(rec);
        //    rec.TransitID = 1;
        //    db.SaveChanges();
        //    db.CloseMO_IFRequired(mrec.MOID);
        //    return mrec.TransactionID;
        //}

        public void CloseMO(int moid)
        {
            var data = db.MTRs.Single(x => x.ID == moid);
            data.IsClosed = true;
            data.ClosedBy = "dba";
            db.SaveChanges();
        }

        public MMO CombineMO(int moid)
        {
            dc.CombineMOs(moid);
            var data = provider.GetMObyId(moid);
            return data;
        }

        /********************************************************************/
        /*NEW METHODS after Double Entry System*/
        /*By: MADHUR*/

        public string MORelease(MMOReleasing mrec, string username)
        {
            mrec.ID = db.GetMaxMOReleaseID() + 1;
            mrec.LastUpdatedBy = username;
            mrec.LastUpdatedOn = DateTime.UtcNow.AddHours(5.5);

            var rec = ConvertToMOReleasing(mrec);

            rec.Year = DateTime.UtcNow.AddHours(5.5).Year;
            rec.SiteCode = db.GetSiteCode(rec.TransferringSiteID);
            rec.RID = db.GetMaxMOReleaseRID(rec.MTRID) + 1;
            //rec.RID = db.GetMaxMOReleaseRID(rec.Year, rec.TransferringSiteID) + 1;
            //rec.ReferenceNo = "TORelease/" + rec.Year + "/S-" + rec.SiteCode + "/" + rec.RID;
            rec.ReferenceNo = mrec.MOReferenceNo + "/Dis-" + rec.RID;
            rec.ChallanNo = rec.RID.ToString();    /*Copying RID as challan no.*/

            db.MOReleasings.AddObject(rec);

            db.SaveChanges();

            return rec.ReferenceNo;
        }

        public int MOReceive(MMOReceiving mrec, string username)
        {
            mrec.ID = db.GetMaxMOReceiveID() + 1;
            mrec.LastUpdatedBy = username;
            mrec.LastUpdatedOn = DateTime.UtcNow.AddHours(5.5);

            var rec = ConvertToMOReceiving(mrec);

            rec.Year = DateTime.UtcNow.AddHours(5.5).Year;
            rec.SiteCode = db.GetSiteCode(rec.RequestingSiteID);
            rec.RID = db.GetMaxMOReceiveRID(rec.MTRID) + 1;
            //rec.RID = db.GetMaxMOReceiveRID(rec.Year, rec.RequestingSiteID) + 1;
            //rec.ReferenceNo = "TOReceive/" + rec.Year + "/S-" + rec.SiteCode + "/" + rec.RID;
            rec.ReferenceNo = mrec.MOReleaseRefNo + "/Rec-" + rec.RID;
            rec.ChallanNo = rec.RID.ToString();    /*Copying RID as challan no.*/
            db.MOReceivings.AddObject(rec);
            db.SaveChanges();

            return rec.ID;
        }

        #region Convertion Methods

        #region MO Release

        public MOReleasing ConvertToMOReleasing(MMOReleasing data)
        {
            MOReleasing mrec = new MOReleasing()
            {
                ReferenceNo = data.ReferenceNo,
                ID = data.ID,
                TransferringSiteID = data.TransferringSiteID,
                ChallanNo = data.ChallanNo,
                ReleasedBy = data.ReleasedBy,
                ReleasedOn = data.ReleasedOn,
                LastUpdatedOn = data.LastUpdatedOn,
                LastUpdatedBy = data.LastUpdatedBy,
                Remarks = data.Remarks,
                TransitID = data.TransporterID,
                MTRID = data.MTRID,
                ConsumableType = data.ConsumableType
            };
            if (data.MOReleasingLines != null)
            {
                data.MOReleasingLines.ToList()
                    .ForEach(x => mrec.MOReleasingLines.Add(ConvertToMOReleasingLines(x)));
            }
            return mrec;
        }

        public MOReleasingLine ConvertToMOReleasingLines(MMOReleasingLine x)
        {
            MOReleasingLine mrline = new MOReleasingLine()
            {
                ID = x.LineID,
                ItemID = x.ItemID,
                SizeID = x.SizeID,
                BrandID = x.BrandID,
                HeaderID = x.HeaderID,
                Remarks = x.LineRemarks,
                Qty = x.Quantity,
                TruckNo = x.TruckNo,
                ChallanNo = x.LineChallanNo,
                OrderedQty = x.OrderedQty,
                EarlierReleasedQty = x.EarlierReleasedQty,
                MOID = x.MOID,
                MOLineID = x.MOLineID
            };
            mrline.IsReceived = false;
            mrline.ReceivingQty = 0;
            return mrline;
        }

        #endregion

        #region MO Receive

        public MOReceiving ConvertToMOReceiving(MMOReceiving data)
        {
            MOReceiving mrec = new MOReceiving()
            {
                ReferenceNo = data.ReferenceNo,
                ID = data.ID,
                RequestingSiteID = data.RequestingSiteID,
                ChallanNo = data.ChallanNo,
                ReceivedBy = data.ReceivedBy,
                ReceivedOn = data.ReceivedOn,
                LastUpdatedOn = data.LastUpdatedOn,
                LastUpdatedBy = data.LastUpdatedBy,
                Remarks = data.Remarks,
                TransitID = data.TranporterID,
                MTRID = data.MTRID,
                ConsumableType = data.ConsumableType
            };
            if (data.MOReceivingLines != null)
            {
                data.MOReceivingLines.ToList()
                    .ForEach(x => mrec.MOReceivingLines.Add(ConvertToMOReceivingLines(x)));
            }
            return mrec;
        }

        public MOReceivingLine ConvertToMOReceivingLines(MMOReceivingLine x)
        {
            MOReceivingLine mrline = new MOReceivingLine()
            {
                ID = x.LineID,
                ItemID = x.ItemID,
                SizeID = x.SizeID,
                BrandID = x.BrandID,
                HeaderID = x.HeaderID,
                Remarks = x.LineRemarks,
                Qty = x.Quantity,
                TruckNo = x.TruckNo,
                ChallanNo = x.LineChallanNo,
                OrderedQty = x.OrderedQty,
                EarlierReceivedQty = x.EarlierReceivedQty,
                MOID = x.MOID,
                MOLineID = x.MOLineID,
                ReleasingID = x.ReleasingID,
                ReleasingLineID = x.ReleasingLineID
            };
            return mrline;
        }

        #endregion

        #endregion

        /********************************************************************/
    }
}
