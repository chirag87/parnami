﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Net.Mail;
using System.Net;

namespace SIMS.MM.BLL.Emailer
{
    public class EmailSendor
    {
        public static MailMessage SendTextEmail(string Tos, string From, string Subject, string attachment, string Text)
        {
            if (String.IsNullOrWhiteSpace(attachment))
                return SendTextEmail(Tos, From, Subject, Text, true, null);
            else
                return SendTextEmail(Tos, From, Subject, Text, true,attachment);
        }

        public static MailMessage SendTextEmail(string Tos, string From, string Subject, string Text, bool IsAsync, params string[] Attachments)
        {
            var _brokenTos = Tos.Split(';').ToList();   // listed all EMail Addressess which are stored in appsettings wigth separator ';'

            var settings = Services.AppSettingsService.GetLatestAppSettings();
            MailMessage msg = new MailMessage();

            _brokenTos.ForEach(x => msg.To.Add(x.Trim()));  // Trimmed all email addresses and added to the EmailTo's List
            //msg.To.Add(Tos);

            msg.From = new MailAddress(settings.mailfrom);
            msg.Subject = Subject;
            msg.Body = Text;
            msg.IsBodyHtml = true;
            if (Attachments != null)
            {
                foreach (var file in Attachments)
                {
                    msg.Attachments.Add(new Attachment(file));
                }
            }
            SmtpClient client = new SmtpClient(settings.mailserver, 25);
            NetworkCredential credential = new NetworkCredential(settings.mailserverusername, settings.mailserverpassword);
            client.Credentials = credential;
            if (IsAsync)
                client.SendAsync(msg, null);
            else
                client.Send(msg);
            return msg;
        }
    }
}
