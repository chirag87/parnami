﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;
using SIMS.MM.MODEL;
using SIMS.MM.DAL;
using System.Collections.ObjectModel;
using SIMS.Core;

namespace SIMS.MM.BLL
{
    [ServiceContract]
    public interface IReturnToVendorManager
    {
        [OperationContract]
        IPaginated<MReturnToVendor> GetAllReturnToVendors(string username, int? pageindex, int? pagesize, string key, int? vendorid, int? siteid, bool showall = true);

        [OperationContract]
        MReturnToVendor GetRTVByID(int ID);

        [OperationContract]
        MReturnToVendor CreateRTV(MReturnToVendor newRTV, string username);

        [OperationContract]
        MReturnToVendor UpdateRTV(MReturnToVendor rtv);

        [OperationContract]
        MReturnToVendor UpdateRTVReceivingInfo(MReturnToVendor rtv);

        [OperationContract]
        void DeleteRTVByID(int ID);

        [OperationContract]
        void DeleteRTV(MReturnToVendor rtv);

        [OperationContract]
        MReturnToVendor ChangeConfirmationStatus(int RTVID, bool status);
    }

    public class ReturnToVendorManager : IReturnToVendorManager
    {
        parnamidb1Entities db = new parnamidb1Entities();
        MMReportsDBDataContext dc = new MMReportsDBDataContext();

        public IPaginated<MReturnToVendor> GetAllReturnToVendors(string username, int? pageindex, int? pagesize, string key, int? vendorid, int? siteid, bool showall = true)
        {
            var data1 = dc.GetRTVHeaders(username, key, vendorid, siteid, showall);
            var data = data1.OrderByDescending(x => x.SentDate)
                .ToPaginate(pageindex, pagesize)
                .Select
                (x => new MReturnToVendor
                {
                    CreatedBy = x.CreatedBy,
                    CreatedOn = x.CreatedOn,
                    ID = x.ID,
                    IsReceived = x.IsReceived,
                    LastUpdatedBy = x.LastUpdatedBy,
                    LastUpdatedOn = x.LastUpdatedOn,
                    ReceivingDate = x.ReceivingDate,
                    ReferenceNo = x.ReferenceNo,
                    Remarks = x.Remarks,
                    ReturnedBy = x.ReturnedBy,
                    SentDate = x.SentDate,
                    SiteChallanNo = x.SiteChallanNo,
                    SiteID = x.BUID,
                    Site = x.Site,
                    TransitID = x.TransitID,
                    Transporter = x.Transporter,
                    VendorChallanNo = x.VendorChallanNo,
                    VendorID = x.VendorID,
                    Vendor = x.Vendor
                });
            return data;
        }

        public MReturnToVendor GetRTVByID(int ID)
        {
            var data = db.ReturnToVendors.Single(x => x.ID == ID);
            return (ConvertToMRTV(data));
        }

        #region Convertions...
        public MReturnToVendor ConvertToMRTV(ReturnToVendor data)
        {
            MReturnToVendor rtv = new MReturnToVendor()
            {
                CreatedBy = data.CreatedBy,
                CreatedOn = data.CreatedOn,
                ID = data.ID,
                IsReceived = data.IsReceived,
                LastUpdatedBy = data.LastUpdatedBy,
                LastUpdatedOn = data.LastUpdatedOn,
                ReceivingDate = data.ReceivingDate,
                ReferenceNo = data.ReferenceNo,
                Remarks = data.Remarks,
                ReturnedBy = data.ReturnedBy,
                SentDate = data.SentDate,
                SiteChallanNo = data.SiteChallanNo,
                SiteID = data.BUID,
                TransitID = data.TransitID,
                VendorChallanNo = data.VendorChallanNo,
                VendorID = data.VendorID
            };
            if (data.BUID != null) { rtv.Site = data.BUs.DisplaySite; }
            if (data.VendorID != null) { rtv.Vendor = data.Vendor.DisplayVendor; }
            if (data.TransitID != null) { rtv.Transporter = data.Transporter.Name; }
            rtv.RTVLines = new ObservableCollection<MReturnToVendorLine>();
            if (data.ReturnToVendorLines != null)
            {
                data.ReturnToVendorLines.ToList()
                    .ForEach(x => rtv.RTVLines.Add(ConvertToMRTVLine(x)));
            }
            return rtv;
        }

        public MReturnToVendorLine ConvertToMRTVLine(ReturnToVendorLine x)
        {
            MReturnToVendorLine line = new MReturnToVendorLine()
            {
                ConsumableType = x.ConsumableType,
                HeaderID = x.HeaderID,
                ItemID = x.ItemID,
                SizeID = x.SizeID,
                BrandID = x.BrandID,
                LineID = x.ID,
                LineRemarks = x.Remarks,
                Quantity = x.Qty,
                ScrapQty = x.ScrapQty,
                ExtraQty = x.ExtraQty,
                TruckNo = x.TruckNo
            };
            if (x.ItemID != null) { line.Item = x.Item.DisplayName; }
            if (x.SizeID != null) { line.Size = dc.GetSizeByID(x.SizeID); }
            if (x.BrandID != null) { line.Brand = dc.GetBrandByID(x.BrandID); }
            return line;
        }

        public ReturnToVendor ConvertToRTV(MReturnToVendor newRTV)
        {
            ReturnToVendor rtv = new ReturnToVendor()
            {
                CreatedBy = newRTV.CreatedBy,
                CreatedOn = newRTV.CreatedOn,
                ID = newRTV.ID,
                IsReceived = newRTV.IsReceived,
                LastUpdatedBy = newRTV.LastUpdatedBy,
                LastUpdatedOn = newRTV.LastUpdatedOn,
                ReceivingDate = newRTV.ReceivingDate,
                ReferenceNo = newRTV.ReferenceNo,
                Remarks = newRTV.Remarks,
                ReturnedBy = newRTV.ReturnedBy,
                SentDate = newRTV.SentDate,
                SiteChallanNo = newRTV.SiteChallanNo,
                BUID = newRTV.SiteID,
                TransitID = newRTV.TransitID,
                VendorChallanNo = newRTV.VendorChallanNo,
                VendorID = newRTV.VendorID
            };
            if (newRTV.RTVLines != null)
            {
                newRTV.RTVLines.ToList()
                    .ForEach(x => rtv.ReturnToVendorLines.Add(ConvertToRTVLine(x)));
            }
            return rtv;
        }

        public ReturnToVendorLine ConvertToRTVLine(MReturnToVendorLine newRTVLine)
        {
            ReturnToVendorLine line = new ReturnToVendorLine()
            {
                HeaderID = newRTVLine.HeaderID,
                ItemID = newRTVLine.ItemID,
                SizeID = newRTVLine.SizeID,
                BrandID = newRTVLine.BrandID,
                ConsumableType = newRTVLine.ConsumableType,
                ID = newRTVLine.LineID,
                Remarks = newRTVLine.LineRemarks,
                Qty = newRTVLine.Quantity,
                ScrapQty = newRTVLine.ScrapQty ?? 0,
                ExtraQty = newRTVLine.ExtraQty ?? 0,
                TruckNo = newRTVLine.TruckNo
            };
            return line;
        }
        #endregion

        public MReturnToVendor CreateRTV(MReturnToVendor newRTV, string username)
        {
            newRTV.ID = db.GetMaxRTVID() + 1;
            newRTV.CreatedBy = username;
            newRTV.LastUpdatedBy = username;
            newRTV.ReturnedBy = username;
            newRTV.LastUpdatedOn = DateTime.UtcNow.AddHours(5.5);
            newRTV.CreatedOn = DateTime.UtcNow.AddHours(5.5);

            var data = ConvertToRTV(newRTV);

            if (String.IsNullOrWhiteSpace(data.Remarks)) data.Remarks = null;
            data.ReturnedBy = username;
            data.Year = DateTime.UtcNow.AddHours(5.5).Year;
            data.SiteCode = db.GetSiteCode(data.BUID);
            data.RID = db.GetMaxRTVRID(data.Year, data.BUID) + 1;
            data.ReferenceNo = "RTV/" + data.Year + "/S-" + data.SiteCode + "/" + data.RID;

            db.ReturnToVendors.AddObject(data);
            db.SaveChanges();
            return newRTV;
        }

        public MReturnToVendor UpdateRTV(MReturnToVendor rtv)
        {
            var Obj = ConvertToRTV(rtv);
            db.ReturnToVendors.Attach(Obj);
            db.ObjectStateManager.ChangeObjectState(Obj, System.Data.EntityState.Modified);
            db.SaveChanges();
            return rtv;
        }

        public MReturnToVendor UpdateRTVReceivingInfo(MReturnToVendor rtv)
        {
            var data = db.ReturnToVendors.Single(x => x.ID == rtv.ID);
            data.IsReceived = rtv.IsReceived;
            data.ReceivingDate = rtv.ReceivingDate;
            data.VendorChallanNo = rtv.VendorChallanNo;
            data.Remarks = rtv.Remarks;

            foreach (var line in rtv.RTVLines)
            {
                var linedata = db.ReturnToVendorLines.Single(x => x.ID == line.LineID && x.HeaderID == line.HeaderID);
                linedata.ItemID = line.ItemID;
                linedata.SizeID = line.SizeID;
                linedata.BrandID = line.BrandID;
                linedata.ConsumableType = line.ConsumableType;
                linedata.Qty = line.Quantity;
                linedata.ScrapQty = line.ScrapQty;
                linedata.ExtraQty = line.ExtraQty;
                linedata.TruckNo = line.TruckNo;
                linedata.Remarks = line.LineRemarks;
            }

            db.SaveChanges();
            return rtv;
        }

        public void DeleteRTVByID(int ID)
        {
            var data = GetRTVByID(ID);
            db.ReturnToVendors.DeleteObject(ConvertToRTV(data));
            db.SaveChanges();
        }

        public void DeleteRTV(MReturnToVendor rtv)
        {
            db.ReturnToVendors.DeleteObject(ConvertToRTV(rtv));
            db.SaveChanges();
        }

        public MReturnToVendor ChangeConfirmationStatus(int RTVID, bool status)
        {
            db.ReturnToVendors.Single(x => x.ID == RTVID).IsReceived = status;
            db.SaveChanges();
            var data = GetRTVByID(RTVID);
            return data;
        }
    }
}
