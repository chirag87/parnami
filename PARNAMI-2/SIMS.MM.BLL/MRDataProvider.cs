﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SIMS.MM.BLL
{
    using MODEL;
    using DAL;
    using System.Collections.ObjectModel;
    using SIMS.Core;
    public class MRDataProvider : IMRDataProvider
    {
        parnamidb1Entities db = new parnamidb1Entities();
        MMReportsDBDataContext db1 = new MMReportsDBDataContext();

        public IPaginated<MMRHeader> GetMRHeadersNew(string username, int? pageindex, int? pagesize, string Key, int? sideid, bool showall = true)
        {
            var data = db1.GetMRHeaders(username, Key, sideid, showall);
            var data1 = data.OrderByDescending(x => x.ID)
                .ToPaginate(pageindex, pagesize)
                .Select(x => new MMRHeader()
                {
                    ID = x.ID,
                    ReferenceNo = x.ReferenceNo,
                    Status = x.Status,
                    DisplaySite = x.Site_Name,
                    RaiseOn = x.RaisedOn,
                    Remarks = x.Remarks,
                    // = x.BUs.Name,
                    CanIssue = x.Can_Issue.Value,
                    CanReturn = x.Can_Return.Value

                });
            return data1;
        }


        public MMR GetMRByID(int MRID)
        {
            var mr = db.MRs.Single(x => x.ID == MRID);
            var data = ConvertTOMMR(mr);
            data.Status = db1.StatusOfMR(data.ID);
            return data;
        }

        private MMR ConvertTOMMR(MR _mr)
        {
            MMR _mmr = new MMR()
            {
                ID = _mr.ID,
                RequestedBy = _mr.RequestedBy,
                ApprovalStatus = _mr.ApprovalStatus,
                ContactPerson = _mr.ContactPersonName,
                ContactPhone = _mr.ConactPersonPhone,
                CurrentOwnerID = _mr.CurrentOwnerID,
                DisplaySite = _mr.BUs.DisplaySite,
                IsApproved = _mr.IsApproved,
                RaisedBy = _mr.RaisedBy,
                RaisedOn = _mr.RaisedOn,
                ReferenceNo = _mr.ReferenceNo,
                Remarks = _mr.Remarks,
                Site = _mr.BUs.Name,
                SiteID = _mr.SiteID
            };
            _mmr.MRLines = new ObservableCollection<MMRLine>();
            _mr.MRLines.Select(x => ConvertToMRLines(x)).ToList()
                .ForEach(x => _mmr.MRLines.Add(x));
            return _mmr;
        }

        private MMRLine ConvertToMRLines(MRLine _mrline)
        {
            MMRLine line = new MMRLine()
            {
                ItemID = _mrline.ItemID,
                LineID = _mrline.ID,
                SizeID = _mrline.SizeID,
                BrandID = _mrline.BrandID,
                MRID = _mrline.MRID,
                IssuedOn = _mrline.IssuedOn,
                IssuedQty = _mrline.IssuedQty,
                Item = _mrline.Item.DisplayName,
                LineRemarks = _mrline.Remarks,
                Quantity = _mrline.Qty,
                ReturnedQty = _mrline.ReturnedQty,
                ReturnedOn = _mrline.ReturnedOn,
                ConsumableType = _mrline.ConsumableType
            };
            if (_mrline.ItemBrand != null)
                line.Brand = _mrline.ItemBrand.Value;
            if (_mrline.ItemSize != null)
                line.Size = _mrline.ItemSize.Value;
            return line;
        }

        public MReceiving GetMReceivingforMRIssue(int mrid)
        {
            var mr = db.MRs.Single(x => x.ID == mrid);
            return ConvertMR_TO_MRReceivingforIssue(mr);
        }

        private MReceiving ConvertMR_TO_MRReceivingforIssue(MR mr)
        {
            MReceiving mrec = new MReceiving()
            {
                TransactionID = mr.ID,
                SiteID = mr.SiteID,
                CreatedBy = mr.RaisedBy,
                Purchaser = mr.RequestedBy,
                CreatedOn = DateTime.UtcNow.AddHours(5.5),
                ReceivingRemarks = mr.Remarks,
                Site = mr.BUs.Name,
                TransactionTypeID = "Issue"
            };
            mrec.ReceivingLines = new ObservableCollection<MReceivingLine>();
            mr.MRLines.Select(x => ConvertTO_RLines(x)).ToList()
                .ForEach(x => mrec.ReceivingLines.Add(x));
            return mrec;
        }

        private MReceivingLine ConvertTO_RLines(MRLine x)
        {
            MReceivingLine line = new MReceivingLine()
            {
                LineID = x.ID,
                ItemID = x.ItemID,
                SizeID = x.SizeID.Value,
                BrandID = x.BrandID.Value,
                MRID = x.MRID,
                Item = x.Item.DisplayName,
                MRLineID = x.ID,
                LineRemarks = x.Remarks,
                Quantity = 0,
                OrderedQty = x.Qty,
                EarlierIssuedQty = x.IssuedQty ?? 0,
                IssuedQty = x.IssuedQty.Value,
                TransactionTypeID = "Issue"
            };
            if (x.ItemBrand != null)
                line.Brand = x.ItemBrand.Value;
            if (x.ItemSize != null)
                line.Size = x.ItemSize.Value;
            return line;
        }

        public MReceiving GetMReceivingforMRReturn(int mrid)
        {
            var mr = db.MRs.Single(x => x.ID == mrid);
            return ConvertMR_TO_MRReceivingforReturn(mr);
        }

        private MReceiving ConvertMR_TO_MRReceivingforReturn(MR mr)
        {
            MReceiving mrec = new MReceiving()
            {
                SiteID = mr.SiteID,
                CreatedBy = mr.RaisedBy,
                Purchaser = mr.RequestedBy,
                CreatedOn = DateTime.UtcNow.AddHours(5.5),
                ReceivingRemarks = mr.Remarks,
                Site = mr.BUs.Name,
                TransactionTypeID = "Return",
                MRID = mr.ID
            };
            mrec.ReceivingLines = new ObservableCollection<MReceivingLine>();
            mr.MRLines.Select(x => ConvertTo_RLines2(x)).ToList()
                .ForEach(x => mrec.ReceivingLines.Add(x));
            return mrec;
        }

        private MReceivingLine ConvertTo_RLines2(MRLine x)
        {
            MReceivingLine line = new MReceivingLine()
            {
                LineID = x.ID,
                ItemID = x.ItemID,
                SizeID = x.SizeID.Value,
                BrandID = x.BrandID.Value,
                MRID = x.MRID,
                Item = x.Item.DisplayName,
                MRLineID = x.ID,
                LineRemarks = x.Remarks,
                Quantity = 0,
                EarlierReturnedQty = x.ReturnedQty ?? 0,
                OrderedQty = x.Qty,
                IssuedQty = x.IssuedQty.Value,
                //ReturnQty = x.ReturnedQty.Value,
                TransactionTypeID = "Return"
            };
            if (x.ItemBrand != null)
                line.Brand = x.ItemBrand.Value;
            if (x.ItemSize != null)
                line.Size = x.ItemSize.Value;
            return line;
        }

        /********************************************************************/
        /*NEW METHODS after Double Entry System*/
        /*By: MADHUR*/

        public MMaterialIssue GetMRIssueTransactionByMRID(int mrid)
        {
            var mr = db.MRs.Single(x => x.ID == mrid);
            return ConvertToMMaterialIssue(mr);
        }

        public MMaterialReturn GetMRReturnTransactionByMRID(int mrid)
        {
            var mr = db.MRs.Single(x => x.ID == mrid);
            return ConvertToMMaterialReturn(mr);
        }

        #region Convertion Methods

        #region MR Issue

        public MMaterialIssue ConvertToMMaterialIssue(MR data)
        {
            MMaterialIssue mrec = new MMaterialIssue()
            {
                ReferenceNo = data.ReferenceNo,
                ID = data.ID,
                SiteID = data.SiteID,
                IssuedBy = data.RaisedBy,
                IssuedOn = data.RaisedOn,
                Requester = data.RequestedBy,
                Remarks = data.Remarks
            };
            if (data.BUs != null) { mrec.Site = data.BUs.DisplaySite; }
            if (data.MRLines != null)
            {
                mrec.MaterialIssueLines = new ObservableCollection<MMaterialIssueLine>();
                data.MRLines.Select(x => ConvertToMMaterialIssueLines(x)).ToList()
                    .ForEach(x => mrec.MaterialIssueLines.Add(x));
            }
            return mrec;
        }

        public MMaterialIssueLine ConvertToMMaterialIssueLines(MRLine x)
        {
            MMaterialIssueLine mrline = new MMaterialIssueLine()
            {
                LineID = x.ID,
                ItemID = x.ItemID,
                SizeID = x.SizeID.Value,
                BrandID = x.BrandID.Value,
                LineRemarks = x.Remarks,
                Quantity = 0,
                MRID = x.MRID,
                MRLineID = x.ID,
                OrderedQty = x.Qty,
                EarlierIssuedQty = x.IssuedQty ?? 0
            };
            if (x.Item != null) { mrline.Item = x.Item.DisplayName; }
            if (x.ItemSize != null) { mrline.Size = x.ItemSize.Value; }
            if (x.ItemBrand != null) { mrline.Brand = x.ItemBrand.Value; }
            return mrline;
        }

        #endregion

        #region MR Return

        public MMaterialReturn ConvertToMMaterialReturn(MR data)
        {
            MMaterialReturn mrec = new MMaterialReturn()
            {
                ReferenceNo = data.ReferenceNo,
                ID = data.ID,
                SiteID = data.SiteID,
                ReturnedBy = data.RaisedBy,
                ReturnedOn = data.RaisedOn,
                Requester = data.RequestedBy,
                Remarks = data.Remarks
            };
            if (data.BUs != null) { mrec.Site = data.BUs.DisplaySite; }
            if (data.MRLines != null)
            {
                mrec.MaterialReturnLines = new ObservableCollection<MMaterialReturnLine>();
                data.MRLines.Select(x => ConvertToMMaterialReturnLiness(x)).ToList()
                    .ForEach(x => mrec.MaterialReturnLines.Add(x));
            }
            return mrec;
        }

        public MMaterialReturnLine ConvertToMMaterialReturnLiness(MRLine x)
        {
            MMaterialReturnLine mrline = new MMaterialReturnLine()
            {
                LineID = x.ID,
                ItemID = x.ItemID,
                SizeID = x.SizeID.Value,
                BrandID = x.BrandID.Value,
                LineRemarks = x.Remarks,
                Quantity = 0,
                MRID = x.MRID,
                MRLineID = x.ID,
                OrderedQty = x.Qty,
                EarlierReturnedQty = x.ReturnedQty ?? 0,
                AleadyIssuedQty = x.IssuedQty ?? 0
            };
            if (x.Item != null) { mrline.Item = x.Item.DisplayName; }
            if (x.ItemSize != null) { mrline.Size = x.ItemSize.Value; }
            if (x.ItemBrand != null) { mrline.Brand = x.ItemBrand.Value; }
            return mrline;
        }

        #endregion

        #endregion

        /********************************************************************/
    }
}
