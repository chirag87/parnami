﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.Collections.ObjectModel;

namespace SIMS.TM.Model
{
    [DataContract]
    public class MTenederLine
    {
        [DataMember]
        public int TenderID { get; set; }

        [DataMember]
        public int LineID { get; set; }

        [DataMember]
        public int? ParentLineID { get; set; }

        [DataMember]
        public bool IsBillable { get; set; }

        [DataMember]
        public string Title { get; set; }

        [DataMember]
        public double? ExpBillingQty { get; set; }
        [DataMember]
        public string UM { get; set; }
        [DataMember]
        public double? Pricing { get; set; }
        [DataMember]
        public double? ForUnits { get; set; }
        [DataMember]
        public double? PerUnitPricing { get { return Pricing / ForUnits; } set{}}
        [DataMember]
        public double? ExpLineTotal { get { return PerUnitPricing*ExpBillingQty; }set{} }
    }

    [DataContract]
    public partial class MTender
    {
        [DataMember]
        public int ID { get;set; }

        [DataMember]
        public string Title { get; set; }

        [DataMember]
        public int? ProjectID { get; set; }

        [DataMember]
        public string CurrentOwner { get; set; }

        [DataMember]
        public string ClientName { get; set; }

        [DataMember]
        public string ClientAddress { get; set; }

        [DataMember]
        public string SiteAddress { get; set; }

        [DataMember]
        public string SiteCity { get; set; }

        [DataMember]
        public string SiteLocation { get; set; }

        [DataMember]
        public string SitePincode { get; set; }

        [DataMember]
        public string SiteState { get; set; }

        [DataMember]
        public double? ExpectedTotal { get; set; }

        [DataMember]
        public double? Taxes { get; set; }

        [DataMember]
        public double? TotalAfterTax { get; set; }

        [DataMember]
        public bool IsWon { get; set; }

        [DataMember]
        public DateTime? DueOn { get; set; }

        [DataMember]
        public DateTime? FirstSubmittedOn { get; set; }

        [DataMember]
        public DateTime? ExpStartDate { get; set; }

        [DataMember]
        public DateTime? ExpEndDate { get; set; }

        [DataMember]
        public ObservableCollection<MTenederLine> TenderLines { get; set; }

        public MTender() { TenderLines = new ObservableCollection<MTenederLine>(); }

        public static MTender GetTestMTender()
        {
            MTender tender = new MTender() { Title="Test Tender", ID=1};

            tender.TenderLines.Add(new MTenederLine()
            {
                Title = "Excavation",
                LineID = 1,
                TenderID = 1,
                IsBillable = false
            });

            tender.TenderLines.Add(new MTenederLine() { 
                Title = "Excavation-1",
                LineID = 2,
                TenderID = 1,
                IsBillable = true,
                UM="CFT",
                ExpBillingQty = 4000,
                ForUnits = 1,
                Pricing = 110,
                ParentLineID = 1
            });

            

            tender.TenderLines.Add(new MTenederLine()
            {
                Title = "BrickWork",
                LineID = 4,
                TenderID = 1,
                IsBillable = true,
                UM = "CFT",
                ExpBillingQty = 2000,
                ForUnits = 1,
                Pricing = 120,
                ParentLineID = null
            });

            tender.TenderLines.Add(new MTenederLine()
            {
                Title = "Excavation-2",
                LineID = 3,
                TenderID = 1,
                IsBillable = true,
                UM = "CFT",
                ExpBillingQty = 2000,
                ForUnits = 1,
                Pricing = 120,
                ParentLineID = 1
            });

            return tender;
        }
    }

    [DataContract]
    public partial class MTenderLineGrp:MTenederLine
    {
        public MTenderLineGrp()
            : base()
        {
            ChildLines = new ObservableCollection<MTenderLineGrp>();
        }

        public MTenderLineGrp(MTenederLine line)
            : this()
        {
            this.Title = line.Title;
            this.UM = line.UM;
            this.Pricing = line.Pricing;
            this.PerUnitPricing = line.PerUnitPricing;
            this.IsBillable = line.IsBillable;
            this.LineID = line.LineID;
            this.ParentLineID = line.ParentLineID;
            this.ExpBillingQty = line.ExpBillingQty;
        }

        public ObservableCollection<MTenderLineGrp> ChildLines { get; set; }
        public MTenderLineGrp ParentLine { get; set; }

        public void AddChild(MTenderLineGrp cgrp)
        {
            this.ChildLines.Add(cgrp);
            cgrp.ParentLine = this;
        }

        public static  ObservableCollection<MTenderLineGrp> GetByGrps(MTender Tender)
        {
            return GetGrps(Tender, null);
        }

        static ObservableCollection<MTenderLineGrp> GetGrps(MTender Tender, int? parentid)
        {
            ObservableCollection<MTenderLineGrp> list = new ObservableCollection<MTenderLineGrp>();
            foreach (var line in Tender.TenderLines.Where(x => x.ParentLineID == parentid))
            {
                MTenderLineGrp grp = new MTenderLineGrp(line);
                foreach (var item in GetGrps(Tender, line.LineID))
                {
                    grp.AddChild(item);
                }
                list.Add(grp);
            }
            return list;
        }

    }
}
