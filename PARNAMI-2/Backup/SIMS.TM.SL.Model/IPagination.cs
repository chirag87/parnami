﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.Collections;

namespace SIMS.TM.Model
{  
    public interface IPagination
    {
        [DataMember]
         int TotalPages { get; set; }
        [DataMember]
        int CurrentPage { get; set; }
        [DataMember]
        int TotalEntities { get; set; }
        [DataMember]
        int PageSize { get; set; }
        [DataMember]
        int Count{get;set;}
    }

    public interface IPaginated<T>:IPagination
    {
        //[DataMember]
        //int TotalPages { get; set; }
        //[DataMember]
        //int CurrentPage { get; set; }
        //[DataMember]
        //int TotalEntities { get; set; }
        //[DataMember]
        //int PageSize { get; set; }
        //[DataMember]
        //int Count { get; set; }
        [DataMember]
        List<T> Data { get; set; }
        void Add(T entity);
        void AddRange(IEnumerable<T> range);
    }

    [DataContract]
    public class PaginatedData<T>: IPaginated<T>
    {
        [DataMember]
        public int TotalPages
        {
            get;
            set;
        }

        [DataMember]
        public int CurrentPage
        {
            get;
            set;
        }

        [DataMember]
        public int TotalEntities
        {
            get;
            set;
        }

        [DataMember]
        public int PageSize
        {
            get;
            set;
        }

        [DataMember]
        public List<T> Data
        {
            get;
            set;
        }

        [DataMember]
        public int Count
        {
            get
            {
                if (Data == null) return 0;
                return Data.Count;
            }
            set
            {
            }
        }

        public void Add(T entity)
        {
            if (Data == null) Data = new List<T>();
            Data.Add(entity);
        }

        public void AddRange(IEnumerable<T> range)
        {
            if (Data == null) Data = new List<T>();
            Data.AddRange(range);
        }
    }

    public static class PaginatedExtensions
    {
        public static IPaginated<T> ToPaginate<T>(this IEnumerable<T> qry, int pageindex, int pagesize)
        {
            if (pagesize == 0) throw new Exception("Page Size has to be greater than zero!");
            PaginatedData<T> data = new PaginatedData<T>();
            data.CurrentPage = pageindex;
            data.PageSize = pagesize;
            data.TotalPages = (int)Math.Ceiling(qry.Count() * 1.0 / pagesize);
            data.TotalEntities = qry.Count();
            int toskip = pagesize * pageindex;
            data.AddRange(qry.Skip(toskip).Take(pagesize));
            return data;
        }

        public static IPaginated<T> ToPaginate<T>(this IEnumerable<T> qry, int? pageindex, int? pagesize)
        {
            pageindex = pageindex ?? 0;
            pagesize = pagesize ?? 20;
            return qry.ToPaginate(pageindex.Value, pagesize.Value);
        }

        public static IPaginated<T> ToPaginate<T>(this IQueryable<T> qry, int pageindex, int pagesize)
        {
            if (pagesize == 0) throw new Exception("Page Size has to be greater than zero!");
            PaginatedData<T> data = new PaginatedData<T>();
            data.CurrentPage = pageindex;
            data.PageSize = pagesize;
            data.TotalPages = (int)Math.Ceiling(qry.Count() * 1.0 / pagesize);
            data.TotalEntities = qry.Count();
            int toskip = pagesize * pageindex;
            data.AddRange(qry.Skip(toskip).Take(pagesize));
            return data;
        }

        public static IPaginated<T> ToPaginate<T>(this IQueryable<T> qry, int? pageindex, int? pagesize)
        {
            pageindex = pageindex ?? 0;
            pagesize = pagesize ?? 20;
            return qry.ToPaginate(pageindex.Value, pagesize.Value);
        }

        public static IPaginated<TResult> Select<T, TResult>(this IPaginated<T> page, Func<T, TResult> selector)
        {
            PaginatedData<TResult> data = new PaginatedData<TResult>();
            data.CurrentPage = page.CurrentPage;
            data.PageSize = page.PageSize;
            data.TotalPages = page.TotalPages;
            data.TotalEntities = page.TotalEntities;
            data.AddRange(page.Data.Select(selector));
            return data;
        }

        public static IPaginated<TResult> Cast<T,TResult>(this IPaginated<T> page)
        {
            PaginatedData<TResult> data = new PaginatedData<TResult>();
            data.CurrentPage = page.CurrentPage;
            data.PageSize = page.PageSize;
            data.TotalPages = page.TotalPages;
            data.TotalEntities = page.TotalEntities;
            data.AddRange(page.Data.Cast<TResult>());
            return data;
        }
    }
}
