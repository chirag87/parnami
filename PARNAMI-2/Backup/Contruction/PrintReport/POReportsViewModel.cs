﻿using System;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using SIMS.MM.MODEL;
using System.Collections.Generic;
using Contruction.PurchasingService;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Windows.Printing;

namespace Contruction
{
    public class POReportsViewModel : ViewModel
    {
        //PurchasingServiceClient service = new PurchasingServiceClient();

        public POReportsViewModel()
        {
            //if (!DesignerProperties.GetIsInDesignMode(Application.Current.RootVisual))
            //{
            //    OnInit();
            //}
        }

        //public void OnInit()
        //{
        //    service.GetPObyIDCompleted +=new EventHandler<GetPObyIDCompletedEventArgs>(service_GetPObyIDCompleted);
        //}

        //void service_GetPObyIDCompleted(object sender, GetPObyIDCompletedEventArgs e)
        //{
        //    if (e.Error != null)
        //    {
        //        Base.ShowError(e.Error);
        //        IsBusy = false;
        //    }
        //    else
        //    {
        //        SelectedPO = e.Result;
        //        IsBusy = false;
        //    }
        //}

        MPO _SelectedPO = new MPO();
        public MPO SelectedPO
        {
            get { return _SelectedPO; }
            set
            {
                _SelectedPO = value;
                OnPOSet();
                Notify("SelectedPO");
            }
        }

        private void OnPOSet()
        {
            if(SelectedPO == null) return;
            Vendor = Base.Current.Vendors.Single(x => x.ID == SelectedPO.VendorID);
            Site = Base.Current.Sites.Single(x => x.ID == SelectedPO.SiteID);
            TotalInWords = "Rs. " + NumberToEnglish.changeCurrencyToWords(SelectedPO.GrandTotal);
            CVAT = (SelectedPO.TotalValue * SelectedPO.pVAT) / 100;
            CCST = (SelectedPO.TotalValue * SelectedPO.pCST) / 100;
        }

        double _CVAT = new double();
        public double CVAT
        {
            get { return _CVAT; }
            set
            {
                _CVAT = value;
                Notify("CVAT");
            }
        }

        double _CCST = new double();
        public double CCST
        {
            get { return _CCST; }
            set
            {
                _CCST = value;
                Notify("CCST");
            }
        }

        MVendor _Vendor = new MVendor();
        public MVendor Vendor
        {
            get { return _Vendor; }
            set
            {
                _Vendor = value;
                Notify("Vendor");
            }
        }

        MSite _Site = new MSite();
        public MSite Site
        {
            get { return _Site; }
            set
            {
                _Site = value;
                Notify("Site");
            }
        }

        string _TotalInWords;
        public string TotalInWords
        {
            get { return _TotalInWords; }
            set
            {
                _TotalInWords = value;
                Notify("TotalInWords");
            }
        }

        //public void ShowPOByID(int id)
        //{
        //    service.GetPObyIDAsync(id);
        //    IsBusy = true;
        //}
    }
}
