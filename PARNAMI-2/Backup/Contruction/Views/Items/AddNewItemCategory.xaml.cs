﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using SIMS.MM.MODEL;

namespace Contruction
{
    public partial class AddNewItemCategory : ChildWindow
    {
        public event EventHandler<UpdateItemCategoryArgs> Added;
        public void RaiseAdded(MItemCategory _category)
        {
            if (Added != null)
                Added(this, new UpdateItemCategoryArgs(_category));
        }

        public AddNewItemCategory()
        {
            InitializeComponent();
            Model.Added += new EventHandler<UpdateItemCategoryArgs>(Model_Added);
        }

        public AddNewItemCategory(MItemCategory leaf)
            :this()
        {
            Model.AddNewLeafCategory(leaf);
        }

        void Model_Added(object sender, UpdateItemCategoryArgs e)
        {
            RaiseAdded(e.Category);
        }

        public AddNewItemCartegoryVM Model
        {
            get { return (AddNewItemCartegoryVM)this.DataContext; }
        }
				
        public MItemCategory ParentCategory
        {
            get { return Model.ParentItemCategory; }
            set
            {
                Model.ParentItemCategory = value;
            }
        }

        public MItemCategory NewCategory
        {
            get { return Model.NewItemCategory; }
            set
            {
                Model.NewItemCategory = value;
            }
        }

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

        private void ChildWindow_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Escape)
                this.DialogResult = false;
        }

        private void ChildWindow_Loaded(object sender, RoutedEventArgs e)
        {
            tbxItemName.Focus();
        }
    }
}

