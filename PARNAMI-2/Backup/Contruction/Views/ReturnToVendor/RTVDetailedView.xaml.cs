﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.IO;

namespace Contruction
{
    public partial class RTVDetailedView : ChildWindow
    {
        public ReturnToVendorVM Model
        {
            get { return (ReturnToVendorVM)this.DataContext; }
        }

        public RTVDetailedView()
        {
            InitializeComponent();
        }

        public RTVDetailedView(int id)
            : this()
        {
            Model.GetRTVByID(id);
        }

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            if (Model.ValidateUpdate())
            {
                this.StatusBlock.Text = "";
                Model.UpdateRTV();
                Model.Load();
                this.DialogResult = true;
            }
            else
            {
                this.StatusBlock.Text = Model.Status;
            }
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

        private void ChildWindow_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Escape)
            {
                this.DialogResult = false;
            }
        }

        private void btnRevert_Click(object sender, RoutedEventArgs e)
        {
            var btn = sender as Button;
            var data = (ReturnToVendorVM)btn.DataContext;
            Model.ChangeConfirmStatus(data.RTV.ID, false);
        }

        SaveFileDialog sfd;

        private void download_Click(object sender, RoutedEventArgs e)
        {
            Model.IsBusy = true;
            sfd = new SaveFileDialog();
            sfd.Filter = "PDF file format|*.pdf";
            if ((bool)sfd.ShowDialog())
            {
                var hp = sender as HyperlinkButton;
                var data = (ReturnToVendorVM)hp.DataContext;

                WebClient webClient = new WebClient();
                webClient.OpenReadCompleted += (s, args) =>
                {
                    StreamReader sr = new StreamReader(args.Result);
                    byte[] bytes = new byte[args.Result.Length];
                    // revisit Here
                    args.Result.Read(bytes, 0, (int)args.Result.Length);
                    Stream st = sfd.OpenFile();
                    st.Write(bytes, 0, (int)args.Result.Length);
                    st.Close();
                    MessageBox.Show("DOWNLOAD COMPLETE!" + "\nFile Name: " + sfd.SafeFileName);
                    Model.IsBusy = false;
                };
                Uri uri = new Uri("http://" + Base.Current.AppSettings.ServerPath + "/GetReport.ashx?rtype=pdf&type=RTV&id=" + data.RTV.ID.ToString(), UriKind.Absolute);
                webClient.OpenReadAsync(uri);
            }
        }
    }
}