﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.IO;
using SIMS.MM.MODEL;

namespace Contruction
{
    public partial class PODetailView : UserControl
    {
        public PODetailView()
        {
            InitializeComponent();
        }

        public ViewPODetailsModel Model
        {
            get { return (ViewPODetailsModel)this.DataContext; }
        }

        internal void SetID(int p)
        {
            Model.SetID(p);
        }

        private void btn_print_Click(object sender, RoutedEventArgs e)
        {
            var data = this.Model.ConvertedPO;
            PrintFactory.PrintPO(data);
        }

        SaveFileDialog sfd;

        private void download_Click(object sender, RoutedEventArgs e)
        {
            Model.IsBusy = true;
            sfd = new SaveFileDialog();
            sfd.Filter = "PDF file format|*.pdf";
            if ((bool)sfd.ShowDialog())
            {
                var hp = sender as HyperlinkButton;
                var data = (ViewPODetailsModel)hp.DataContext;

                WebClient webClient = new WebClient();
                webClient.OpenReadCompleted += (s, args) =>
                {
                    StreamReader sr = new StreamReader(args.Result);
                    byte[] bytes = new byte[args.Result.Length];
                    // revisit Here
                    args.Result.Read(bytes, 0, (int)args.Result.Length);
                    Stream st = sfd.OpenFile();
                    st.Write(bytes, 0, (int)args.Result.Length);
                    st.Close();
                    MessageBox.Show("DOWNLOAD COMPLETE!" + "\nFile Name: " + sfd.SafeFileName);
                    Model.IsBusy = false;
                };
                Uri uri = new Uri("http://" + Base.Current.AppSettings.ServerPath + "/GetReport.ashx?rtype=pdf&type=PO&id=" + data.ConvertedPO.POID.ToString(), UriKind.Absolute);
                webClient.OpenReadAsync(uri);
            }
        }

        private void InfoIcon_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            var img = sender as Image;
            var data = (MPRLine)img.DataContext;
            if (data.ItemID == 0) return;
            var popup = new ItemSummary(data.ItemID, data.SizeID, data.BrandID);
            popup.CreatePopup();
        }

        #region CreatePopUP Members
        public bool IsPopUpMode
        {
            get { return MyPopUP != null; }
        }

        public PopUp MyPopUP { get; set; }

        public PopUp CreatePopup()
        {
            MyPopUP = this.ToPopup(false);
            MyPopUP.SetAutoLayout();
            MyPopUP.Title = "Purchase Order detailed View";
            return MyPopUP;
        }
        #endregion
    }
}
