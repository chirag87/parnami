﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using SIMS.MM.MODEL;
using System.Collections.ObjectModel;
using System.IO;

namespace Contruction
{
    public partial class PRView : UserControl
    {
        public ViewPRDetailsModel Model
        {
            get { return (ViewPRDetailsModel)this.DataContext; }
        }

        public PRView()
        {
            InitializeComponent();
            Base.AppEvents.POConverted += new EventHandler<PORequestedArgs>(AppEvents_POConverted);
        }

        #region Properties/Variables...

        int? poid;
        SaveFileDialog sfd;

        #endregion

        #region UserControl Event Handles...

        private void dataGrid_CellEditEnded(object sender, DataGridCellEditEndedEventArgs e)
        {
            Model.UpdateNetAmount();
            Model.UpdateTotal();
            Model.UpdateTotalDiscount();
        }

        private void btnToPR_Click(object sender, RoutedEventArgs e)
        {
            var btn = sender as Button;
            var data = (ViewPRDetailsModel)btn.DataContext;
            var lines = data.ConvertedPR.PRLines.Where(x => x.IsSelected == true);
            if (lines.Count() == 0)
            {
                MessageBox.Show("No Line Selected !");
                return;
            }
            Model.SplittedLines = new ObservableCollection<MSplittedLines>();
            foreach (var l in lines)
            {
                Model.SplittedLines.Add(ConvertToSplittedLines(l));
            }
            ConfirmSplittedQuantity con = new ConfirmSplittedQuantity();
            con.DataContext = data;
            con.Show();
            if (MyPopUP != null)
                MyPopUP.Close();
            con.Confirmed += new EventHandler(con_Confirmed);
        }

        private void btnToMO_Click(object sender, RoutedEventArgs e)
        {
            //IsMOClicked = true;
            var btn = sender as Button;
            var data = (ViewPRDetailsModel)btn.DataContext;
            var lines = data.ConvertedPR.PRLines.Where(x => x.IsSelected == true);
            if (lines.Count() == 0)
            {
                MessageBox.Show("No Line Selected !");
                return;
            }
            string conT = lines.First().ConsumableType;  // Checking if Consumable Type of All selected Lines is same
            if (lines.Any(x => x.ConsumableType != conT))
            {
                MessageBox.Show("Consumable Type of All Selected Lines should be Same !");
                return;
            }
            if (lines.Any(x => x.SplittedQuantity == x.AvailableQty))
            {
                MessageBox.Show("There is 0 Quantity left to Split in any of Selected Lines!");
                return;
            }

            Model.MOSplit = new MMOSplittedHeader();
            Model.MOSplit = ConvertToMMOSplitted(data.ConvertedPR);
            Model.MOSplit.Lines = new ObservableCollection<MMOSplittedLine>();
            int i = 1;
            foreach (var l in lines)
            {
                var line = ConvertToMOSplittedLines(l);
                line.LineID = i;
                Model.MOSplit.Lines.Add(line);
                i++;
            }

            ConfirmSplitMO con = new ConfirmSplitMO();
            con.DataContext = data;
            con.Show();
            if (MyPopUP != null)
                MyPopUP.Close();
            con.MOConfirm += new EventHandler(con_MOConfirm);

            #region ///
            //Model.SplittedLines = new ObservableCollection<MSplittedLines>();
            //foreach (var l in lines)
            //{
            //    Model.SplittedLines.Add(ConvertToSplittedLines(l));
            //}
            //ConfirmSplittedQuantity con = new ConfirmSplittedQuantity();
            //con.DataContext = data;
            //con.Show();
            //if (MyPopUP != null)
            //    MyPopUP.Close();
            //con.Confirmed+=new EventHandler(con_Confirmed);
            #endregion
        }

        private void InfoIcon_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            var img = sender as Image;
            var data = (MPRLine)img.DataContext;
            if (data.ItemID == 0) return;
            var popup = new ItemSummary(data.ItemID, data.SizeID, data.BrandID);
            popup.CreatePopup();
        }

        private void btnVerify_Click(object sender, RoutedEventArgs e)
        {
            Model.VerifyPR();
        }

        private void btnSubmitToMIApproval_Click(object sender, RoutedEventArgs e)
        {
            Model.Email();
            Model.SaveandChangeState("SubmittedForMIApproval");
        }

        private void btnInitialApprove_Click(object sender, RoutedEventArgs e)
        {
            Model.InitiallyApprovePR();
        }

        private void textBlock_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            var textblock = sender as TextBlock;
            var line = (MPRLine)textblock.DataContext;
            if (line.ID == 0)
            {
                Model.ConvertedPR.PRLines.Remove(line);
                Model.ConvertedPR.UpdateLineNumbers();
            }
            else
            {
                Model.DeleteLine(line.ID);
            }
        }

        private void btnSubmitToPurchase_Click(object sender, RoutedEventArgs e)
        {
            var btn = sender as Button;
            var data = (ViewPRDetailsModel)btn.DataContext;

            if (data.ConvertedPR.PRLines.All(x => x.UnavailableQty > 0))
            {
                Model.SaveandChangeState("SubmittedToPurchase");
            }
            else
            {
                MessageBox.Show("Please fill Purchase Quantity for All your Items");
            }
        }

        private void btnSubmitToHOPurchase_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            var btn = sender as Button;
            var data = (ViewPRDetailsModel)btn.DataContext;
            var lines = data.ConvertedPR.PRLines.Where(x => x.IsSelected == true);

            if (lines.Count() == 0)
            {
                var result = MessageBox.Show("No Line Selected ! Do you want to Purcahse All Items?", "Confirmation", MessageBoxButton.OKCancel);
                if (result == MessageBoxResult.OK)
                {
                    lines = data.ConvertedPR.PRLines;
                }
                else
                    return;
            }
            if (lines.All(x => x.UnavailableQty > 0))
            {
                Model.SaveandChangeState("SubmittedToHOPurchase");

                MPR NewMI = MPRwithSelectedLines(data.ConvertedPR, lines);
                Model.SubmitToHOPurchase(NewMI);

                if (MyPopUP != null)
                    MyPopUP.Close();
                Model.MICopied += new EventHandler<ViewPRDetailsModel.MICopyRequestedArgs>(Model_MICopied);
            }
            else
            {
                MessageBox.Show("Please fill Purchase Quantity for All your Selected Items");
            }
        }

        private void btnSubmitToPOApproval_Click(object sender, RoutedEventArgs e)
        {
            Model.Email();
            Model.SaveandChangeState("SubmittedForPOApproval");
        }

        private void btnSubmitToVerify_Click(object sender, RoutedEventArgs e)
        {
            Model.Email();
            Model.SaveandChangeState("SubmittedForVerification");
        }

        private void btnCloseMI_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            var MsgResult = MessageBox.Show("Are you Sure you want to Close this MI ?", "Warning", MessageBoxButton.OKCancel);
            if (MsgResult == MessageBoxResult.OK)
            {
                var btn = sender as Button;
                var data = (ViewPRDetailsModel)btn.DataContext;
                Model.CloseMI(data.ConvertedPR.PRNumber);
            }
            else
                MessageBox.Show("MI Not Closed !");
        }

        private void btnCopyPO_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            //var btn = sender as Button;
            //var data = (ViewPRDetailsModel)btn.DataContext;
            //Model.CopyPO(data.ConvertedPR);
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            VendorCombo.Focus();
        }

        private void download_Click(object sender, RoutedEventArgs e)
        {
            sfd = new SaveFileDialog();
            sfd.Filter = "PDF file format|*.pdf";
            if ((bool)sfd.ShowDialog())
            {
                var hp = sender as HyperlinkButton;
                var data = (ViewPRDetailsModel)hp.DataContext;

                WebClient webClient = new WebClient();
                webClient.OpenReadCompleted += (s, args) =>
                {
                    Model.IsBusy = true;
                    StreamReader sr = new StreamReader(args.Result);
                    byte[] bytes = new byte[args.Result.Length];
                    // revisit Here
                    args.Result.Read(bytes, 0, (int)args.Result.Length);
                    Stream st = sfd.OpenFile();
                    st.Write(bytes, 0, (int)args.Result.Length);
                    st.Close();
                    MessageBox.Show("DOWNLOAD COMPLETE!" + "\nFile Name: " + sfd.SafeFileName);
                    Model.IsBusy = false;
                };
                Uri uri = new Uri("http://" + Base.Current.AppSettings.ServerPath + "/GetReport.ashx?rtype=pdf&type=PR&id=" + data.ConvertedPR.PRNumber.ToString(), UriKind.Absolute);
                webClient.OpenReadAsync(uri);
            }
        }

        #endregion

        #region Methods...

        internal void SetID(int p)
        {
            this.VendorCombo.SelectedItem = null;
            Model.SetID(p);
        }

        public bool Validate()
        {
            Model.Status = "";
            if (Model.MOSplit.RequestingSiteID == 0) { Model.Status = "Please Select Requesting Site"; return false; }
            if (Model.MOSplit.TransferringSiteID == 0) { Model.Status = "Please Select Transferring Site"; return false; }
            if (String.IsNullOrWhiteSpace(Model.MOSplit.Requester)) { Model.Status = "Requester Cannot be Empty!"; return false; }
            if (Model.MOSplit.Lines == null) { Model.Status = "Transfer Order should have alteast one item."; return false; }
            if (Model.MOSplit.Lines.Count == 0) { Model.Status = "Transfer Order should have alteast one item."; return false; }
            if (Model.MOSplit.Lines.Any(x => x.ItemID == 0)) { Model.Status = "Transfer Order Lines has some invaid/unselected Items!"; return false; }
            if (Model.MOSplit.Lines.Any(X => X.ConfirmedQty == 0)) { Model.Status = "Transfer Order Lines must have some Quantity of Item!"; return false; }
            return true;
        }

        public MPRLine ConvertfromSplittedLines(MSplittedLines x)
        {
            MPRLine ln = new MPRLine()
            {
                LineID = x.LineID,
                ItemID = x.ItemID,
                Item = x.Item,
                SizeID = x.SizeID,
                Size = x.Size,
                BrandID = x.BrandID,
                Brand = x.Brand,
                ConsumableType = x.ConsumableType,
                Quantity = x.ConfirmedQty
            };
            return ln;
        }

        public MSplittedLines ConvertToSplittedLines(MPRLine l)
        {
            MSplittedLines ln = new MSplittedLines()
            {
                ID = l.ID,
                LineID = l.LineID,
                ItemID = l.ItemID,
                Item = l.Item,
                SizeID = l.SizeID,
                Size = l.Size,
                BrandID = l.BrandID,
                Brand = l.Brand,
                ConsumableType = l.ConsumableType,
                OriginalQty = l.Quantity,
                ConfirmedQty = l.Quantity
            };
            return ln;
        }

        public MPR MPRwithSelectedLines(MPR _mpr, IEnumerable<MPRLine> lines)
        {
            MPR pr = new MPR()
            {
                PRNumber = _mpr.PRNumber,
                CurrentOwnerID = _mpr.CurrentOwnerID,
                VendorID = _mpr.VendorID,
                VerifiedBy = _mpr.VerifiedBy,
                VerifiedOn = _mpr.VerifiedOn,
                ReqApprovedOn = _mpr.ReqApprovedOn,
                ReqApprovedBy = _mpr.ReqApprovedBy,
                ApprovedBy = _mpr.ApprovedBy,
                ApprovedOn = _mpr.ApprovedOn,
                Frieght = _mpr.Frieght,
                TotalValue = _mpr.TotalValue,
                GrandTotal = _mpr.GrandTotal,
                pCST = _mpr.pCST,
                pVAT = _mpr.pVAT,
                TAX = _mpr.TAX,
                IsVerified = _mpr.IsVerified,
                IsReqApproved = _mpr.IsReqApproved,
                IsApproved = _mpr.IsApproved,
                SiteID = _mpr.SiteID,
                Purchaser = _mpr.Purchaser,
                PurchaseType = _mpr.PurchaseType,
                PRRemarks = _mpr.PRRemarks,
                RaisedBy = _mpr.RaisedBy,
                RaisedOn = _mpr.RaisedOn,
                ConvertedToPO = _mpr.ConvertedToPO,
                ContactPerson = _mpr.ContactPerson,
                ContactMobile = _mpr.ContactMobile,
                PaymentTerms = _mpr.PaymentTerms,
                TotalDiscount = _mpr.TotalDiscount,
                NetAmount = _mpr.NetAmount,
                PRStatus = _mpr.PRStatus,
                IsClosed = _mpr.IsClosed,
                ReferenceNo = _mpr.ReferenceNo,
                CostCenterID = _mpr.CostCenterID,
                FinalApprovalStatus = _mpr.FinalApprovalStatus,
                InitialApprovalStatus = _mpr.InitialApprovalStatus,
                POID = _mpr.POID,
                Site = _mpr.Site,
                Vendor = _mpr.Vendor,
                VerificationStatus = _mpr.VerificationStatus
            };

            pr.PRLines = new ObservableCollection<MPRLine>();
            if (lines != null && lines.Count() > 0)
            {
                lines.ToList().ForEach(x => pr.PRLines.Add(MPRLineswithSelectedLines(x)));
            }

            return pr;
        }

        public MPRLine MPRLineswithSelectedLines(MPRLine _mprLine)
        {
            MPRLine detail = new MPRLine()
            {
                LineID = _mprLine.LineID,
                DeliveryDate = _mprLine.DeliveryDate,
                ItemID = _mprLine.ItemID,
                SizeID = _mprLine.SizeID,
                BrandID = _mprLine.BrandID,
                PRID = _mprLine.PRID,
                OriginalQty = _mprLine.OriginalQty,
                Quantity = _mprLine.Quantity,
                LinePrice = _mprLine.LinePrice,
                UnitPrice = _mprLine.UnitPrice,
                LineRemarks = _mprLine.LineRemarks,
                LastPrice = _mprLine.LastPrice,
                DiscountType = _mprLine.DiscountType,
                Discount = _mprLine.Discount,
                TotalLinePrice = _mprLine.TotalLinePrice,
                ConsumableType = _mprLine.ConsumableType,
                AvailableQty = _mprLine.AvailableQty,
                DiscountAmount = _mprLine.DiscountAmount,
                ID = _mprLine.ID,
                IsSelected = _mprLine.IsSelected,
                Brand = _mprLine.Brand,
                Item = _mprLine.Item,
                ItemName = _mprLine.ItemName,
                MeasurementUnit = _mprLine.MeasurementUnit,
                POID = _mprLine.POID,
                Size = _mprLine.Size,
                SplittedQuantity = _mprLine.SplittedQuantity,
                TODispInfo=_mprLine.TODispInfo,
                TotalReceivedQty=_mprLine.TotalReceivedQty,
                TruckNo = _mprLine.TruckNo,
                UnavailableQty = _mprLine.UnavailableQty
            };
            return detail;
        }

        public void NewSplittedPR(int OldPRID, ObservableCollection<MSplittedLines> lines)
        {
            NewPRModel pr = new NewPRModel();
            pr.SplitPRwithPartialQty(OldPRID, lines);
            pr.SaveSplitPR += new EventHandler<NewPRModel.SplittedPRRequestedArgs>(pr_SaveSplitPR);
        }

        public void CreateNewSplittedMO(MMOSplittedHeader newMO, int OldPRID, ObservableCollection<MMOSplittedLine> lines)
        {
            NewMOfromSplittedPRModel mo = new NewMOfromSplittedPRModel();
            if (Validate())
                mo.SplitttoMOwithPartialQty(ConvertFromSplittedMOtoMMO(newMO), OldPRID, lines);
        }

        void pr_SaveSplitPR(object sender, NewPRModel.SplittedPRRequestedArgs e)
        {
            var popup = new PRView();
            popup.Model.ConvertedPR = e.mpr;
            popup.CreatePopup();
        }

        public MPR ConvertToMPR(ViewPRDetailsModel data, ObservableCollection<MPRLine> SelectedLines)
        {
            MPR mpr = new MPR()
            {
                VerificationStatus = data.ConvertedPR.VerificationStatus,
                InitialApprovalStatus = data.ConvertedPR.InitialApprovalStatus,
                FinalApprovalStatus = data.ConvertedPR.FinalApprovalStatus,
                VerifiedBy = data.ConvertedPR.VerifiedBy,
                ReqApprovedBy = data.ConvertedPR.ReqApprovedBy,
                ApprovedBy = data.ConvertedPR.ApprovedBy,
                VerifiedOn = data.ConvertedPR.VerifiedOn,
                ReqApprovedOn = data.ConvertedPR.ReqApprovedOn,
                ApprovedOn = data.ConvertedPR.ApprovedOn,
                ContactMobile = data.ConvertedPR.ContactMobile,
                ContactPerson = data.ConvertedPR.ContactPerson,
                ConvertedToPO = data.ConvertedPR.ConvertedToPO,
                CurrentOwnerID = data.ConvertedPR.CurrentOwnerID,
                Frieght = data.ConvertedPR.Frieght,
                GrandTotal = data.ConvertedPR.GrandTotal,
                HasChanges = data.ConvertedPR.HasChanges,
                IsVerified = data.ConvertedPR.IsVerified,
                IsReqApproved = data.ConvertedPR.IsReqApproved,
                IsApproved = data.ConvertedPR.IsApproved,
                IsBlocked = data.ConvertedPR.IsBlocked,
                NetAmount = data.ConvertedPR.NetAmount,
                PaymentTerms = data.ConvertedPR.PaymentTerms,
                pCST = data.ConvertedPR.pCST,
                PRNumber = data.ConvertedPR.PRNumber,
                PRRemarks = data.ConvertedPR.PRRemarks,
                Purchaser = data.ConvertedPR.Purchaser,
                PurchaseType = data.ConvertedPR.PurchaseType,
                pVAT = data.ConvertedPR.pVAT,
                RaisedBy = data.ConvertedPR.RaisedBy,
                RaisedOn = data.ConvertedPR.RaisedOn,
                ReferenceNo = data.ConvertedPR.ReferenceNo,
                Site = data.ConvertedPR.Site,
                SiteID = data.ConvertedPR.SiteID,
                TAX = data.ConvertedPR.TAX,
                TotalDiscount = data.ConvertedPR.TotalDiscount,
                TotalValue = data.ConvertedPR.TotalValue,
                Vendor = data.ConvertedPR.Vendor,
                VendorID = data.ConvertedPR.VendorID,
                PRLines = SelectedLines
            };

            return mpr;
        }

        public MMOSplittedHeader ConvertToMMOSplitted(MPR data)
        {
            MMOSplittedHeader mo = new MMOSplittedHeader()
            {
                RaisedBy = data.RaisedBy,
                RaisedOn = data.RaisedOn,
                RequestingSiteID = data.SiteID,
                RequestingSite = data.Site,
                Requester = data.Purchaser
            };
            mo.Lines = new ObservableCollection<MMOSplittedLine>();
            foreach (var line in data.PRLines.Where(x => x.IsSelected))
            {
                mo.Lines.Add(ConvertToMOSplittedLines(line));
            }
            mo.ConsumableType = mo.Lines.First().ConsumableType;
            return mo;
        }

        public MMOLine ConvertfromSplittedLineToMOLine(MMOSplittedLine line)
        {
            MMOLine moline = new MMOLine()
            {
                LineID = line.LineID,
                ItemID = line.ItemID,
                Item = line.Item,
                SizeID = line.SizeID,
                Size = line.Size,
                BrandID = line.BrandID,
                Brand = line.Brand,
                Quantity = line.ConfirmedQty,
                ConsumableType = line.ConsumableType,
                LineRemarks = line.Remarks
            };
            return moline;
        }

        public MMOSplittedLine ConvertToMOSplittedLines(MPRLine l)
        {
            MMOSplittedLine ln = new MMOSplittedLine()
            {
                ID = l.ID,
                LineID = l.LineID,
                ItemID = l.ItemID,
                Item = l.Item,
                SizeID = l.SizeID,
                Size = l.Size,
                BrandID = l.BrandID,
                Brand = l.Brand,
                ConsumableType = l.ConsumableType,
                //OriginalQty = l.Quantity,
                //ConfirmedQty = l.Quantity,
                OriginalQty = (l.AvailableQty - l.SplittedQuantity),
                ConfirmedQty = (l.AvailableQty - l.SplittedQuantity),
                //OriginalQty = (l.AvailableQty),
                //ConfirmedQty = (l.AvailableQty),
                Remarks = l.LineRemarks
            };
            return ln;
        }

        public MMO ConvertFromSplittedMOtoMMO(MMOSplittedHeader splitmo)
        {
            MMO mo = new MMO()
            {
                RaisedBy = splitmo.RaisedBy,
                RaisedOn = splitmo.RaisedOn,
                RequestingSiteID = splitmo.RequestingSiteID.Value,
                TransferringSiteID = splitmo.TransferringSiteID.Value,
                TransferringSite = splitmo.TransferringSite,
                Requester = splitmo.Requester,
                ConsumableType = splitmo.ConsumableType,
                Remarks = splitmo.Remarks
            };
            mo.MOLines = new ObservableCollection<MMOLine>();
            //foreach (var line in splitmo.Lines)
            //{
            //    line.ConsumableType = splitmo.ConsumableType;   //Manually assigning Consumable Types of MO into MOLines
            //    mo.MOLines.Add(ConvertfromMOSplitLineToMMOLine(line));
            //}
            return mo;
        }

        #endregion

        #region Event Handlers...

        void Model_MICopied(object sender, ViewPRDetailsModel.MICopyRequestedArgs e)
        {
            var Popup = new PRView();
            Popup.Model.ConvertedPR = e.mpr;
            Popup.CreatePopup();
        }

        void AppEvents_POConverted(object sender, PORequestedArgs e)
        {
            if (IsPopUpMode)
            {
                poid = e.POID;
                this.MyPopUP.Closed += new EventHandler(MyPopUP_Closed);
                this.MyPopUP.Close();
            }
        }

        void MyPopUP_Closed(object sender, EventArgs e)
        {
            if (!poid.HasValue) return;
            var win = new PODetailView();
            win.SetID(poid.Value);
            win.CreatePopup();
        }

        void con_Confirmed(object sender, EventArgs e)
        {
            var btn = sender as ConfirmSplittedQuantity;
            var data = (ViewPRDetailsModel)btn.DataContext;
            if (data.SplittedLines.All(x => x.ConfirmedQty == x.OriginalQty) && (data.SplittedLines.Count() == data.ConvertedPR.PRLines.Count()))
            {
                MessageBox.Show("All Lines with full Quantity can not be splitted into new MI!");
                return;
            }
            NewSplittedPR(data.ConvertedPR.PRNumber, data.SplittedLines);

            #region ///
            //if (data.SplittedLines.All(x => x.ConfirmedQty == x.OriginalQty) && !IsMOClicked && (data.SplittedLines.Count() == data.ConvertedPR.PRLines.Count()))
            //{
            //    MessageBox.Show("All Lines with full Quantity can not be splitted into new MI!");
            //    return;
            //}
            //if (!IsMOClicked)
            //    NewSplittedPR(data.ConvertedPR.PRNumber, data.SplittedLines);
            //else
            //    CreateNewSplittedMO(ConvertToMMO(data, Model.SplittedLines), data.ConvertedPR.PRNumber, Model.SplittedLines);
            #endregion
        }

        void con_MOConfirm(object sender, EventArgs e)
        {
            var btn = sender as ConfirmSplitMO;
            var data = (ViewPRDetailsModel)btn.DataContext;
            CreateNewSplittedMO(data.MOSplit, data.ConvertedPR.PRNumber, data.MOSplit.Lines);
        }

        #endregion

        #region CreatePopUP Members
        public bool IsPopUpMode
        {
            get { return MyPopUP != null; }
        }

        public PopUp MyPopUP { get; set; }

        public PopUp CreatePopup()
        {
            MyPopUP = this.ToPopup(false);
            MyPopUP.SetAutoLayout();
            MyPopUP.Title = "PR Detailed View";
            return MyPopUP;
        }

        #endregion
    }
}

//if (data.ConvertedPR.PRLines.Any(x => x.IsSelected == true))
//    {
//        ObservableCollection<MPRLine> SelectedLines = new ObservableCollection<MPRLine>();
//        data.ConvertedPR.PRLines.Where(x => x.IsSelected == true).ToList().ForEach(x => SelectedLines.Add(x));

//        if (data.ConvertedPR.PRLines.Count() == SelectedLines.Count())
//        {
//            MessageBox.Show("All Lines Can not be Splitted!");
//        }
//        else
//        {
//            ObservableCollection<int> selectedLineIDs = new ObservableCollection<int>();    //Getting Line IDs of Selected Lines
//            foreach (var lineid in SelectedLines)
//            {
//                selectedLineIDs.Add(lineid.LineID);
//            }

//            CreateNewSplittedPR(ConvertToMPR(data, SelectedLines), data.ConvertedPR.PRNumber, selectedLineIDs);
//        }
//    }
//    else
//    {
//        MessageBox.Show("No Row Selected!");
//    }



//public void CreateNewSplittedPR(MPR newpr, int OldPRID, ObservableCollection<int> lineIDs)
//{
//    NewPRModel pr = new NewPRModel();
//    pr.CreateNewSplittedPR(newpr, OldPRID, lineIDs);
//    pr.SaveSplitPR += new EventHandler<NewPRModel.SplittedPRRequestedArgs>(pr_SaveSplitPR);
//}

//var btn = sender as Button;
//var data = (ViewPRDetailsModel)btn.DataContext;
//if (data.ConvertedPR.PRLines.Any(x => x.IsSelected == true))
//{
//    ObservableCollection<MMOLine> SelectedLines = new ObservableCollection<MMOLine>();
//    data.ConvertedPR.PRLines.Where(x => x.IsSelected == true).ToList().ForEach(x => SelectedLines.Add(ConvertToMOLines(x)));

//    if (data.ConvertedPR.PRLines.Count() == SelectedLines.Count())
//    {
//        MessageBox.Show("All Lines Can not be Splitted!");
//    }
//    else
//    {
//        ObservableCollection<int> selectedLineIDs = new ObservableCollection<int>();    //Getting Line IDs of Selected Lines
//        foreach (var lineid in SelectedLines)
//        {
//            selectedLineIDs.Add(lineid.LineID);
//        }

//        CreateNewSplittedMO(ConvertToMMO(data, SelectedLines), data.ConvertedPR.PRNumber, selectedLineIDs);
//    }
//}
//else
//{
//    MessageBox.Show("No Row Selected!");
//}

//public void CreateNewSplittedMO(MMO newMO, int OldPRID, ObservableCollection<MSplittedLines> lines)
//{
//    NewMOfromSplittedPRModel mo = new NewMOfromSplittedPRModel();
//    var popup = new NewMOfromSplittedPR();
//    popup.Model.NewMO = newMO;
//    popup.OldPRID = OldPRID;
//    popup.SplittedLines = lines;
//    popup.CreatePopup();
//}



//public MMOLine ConvertToMOLines(MPRLine x)
//{
//    MMOLine line = new MMOLine()
//    {
//        LineID = x.LineID,
//        Item = x.Item,
//        ItemID = x.ItemID,
//        Quantity = x.Quantity,
//    };
//    return line;
//}

//public MMO ConvertToMMO(ViewPRDetailsModel data, ObservableCollection<MSplittedLines> SplittedLines)
//{
//    MMO mo = new MMO()
//    {
//        ApprovedBy = data.ConvertedPR.ApprovedBy,
//        HasChanges = data.ConvertedPR.HasChanges,
//        IsApproved = data.ConvertedPR.IsApproved,
//        IsBlocked = data.ConvertedPR.IsBlocked,
//        RaisedBy = data.ConvertedPR.RaisedBy,
//        RaisedOn = data.ConvertedPR.RaisedOn,
//        ReferenceNo = data.ConvertedPR.ReferenceNo,
//        RequestingSiteID = data.ConvertedPR.SiteID,
//        Requester = data.ConvertedPR.Purchaser,
//        ConsumableType = data.ConvertedPR.PRLines.First().ConsumableType
//    };
//    mo.MOLines = new ObservableCollection<MMOLine>();
//    foreach (var line in SplittedLines)
//    {
//        mo.MOLines.Add(ConvertfromSplittedLineToMOLine(line));
//    }
//    return mo;
//}

//public MMOLine ConvertfromSplittedLineToMOLine(MSplittedLines line)
//{
//    MMOLine moline = new MMOLine()
//    {
//        LineID = line.LineID,
//        ItemID = line.ItemID,
//        Item = line.Item,
//        SizeID = line.SizeID,
//        Size = line.Size,
//        BrandID = line.BrandID,
//        Brand = line.Brand,
//        Quantity = line.ConfirmedQty
//    };
//    return moline;
//}

//MPRtoMOConvertion _ConvertionEntry = new MPRtoMOConvertion();
//public MPRtoMOConvertion ConvertionEntry
//{
//    get { return _ConvertionEntry; }
//    set
//    {
//        _ConvertionEntry = value;
//        Model.Notify("ConvertionEntry");
//    }
//}



//public MMOLine ConvertfromMOSplitLineToMMOLine(MMOSplittedLine l)
//{
//    MMOLine ln = new MMOLine()
//    {
//        LineID = l.LineID,
//        ItemID = l.ItemID,
//        Item = l.Item,
//        SizeID = l.SizeID,
//        Size = l.Size,
//        BrandID = l.BrandID,
//        Brand = l.Brand,
//        ConsumableType = l.ConsumableType,
//        Quantity = l.ConfirmedQty,
//        LineRemarks = l.Remarks
//    };
//    return ln;
//}