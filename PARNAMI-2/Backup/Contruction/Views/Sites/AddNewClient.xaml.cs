﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using SIMS.MM.MODEL;

namespace Contruction
{
    public partial class AddNewClient : ChildWindow
    {
        public NewClientModel Model
        {
            get { return (NewClientModel)this.DataContext; }
        }
				
        public AddNewClient()
        {
            InitializeComponent();
        }

        public AddNewClient(MClient Client)
            : this()
        {
            Model.NewClient = Client;
        }

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            if (Base.Current.IsNew == false)
            {
                if (String.IsNullOrWhiteSpace(Model.NewClient.Name))
                {
                    MessageBox.Show("Enter a Valid Client Name !");
                }
                else
                {
                    Model.UpdateClient();
                }
            }
            else
            {
                if (String.IsNullOrWhiteSpace(Model.NewClient.Name))
                {
                    MessageBox.Show("Enter a Valid Client Name !"); 
                }
                else
                {
                    Model.SubmitToDB();
                }
            }
            this.DialogResult = true;
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

        private void ChildWindow_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Escape)
            {
                this.DialogResult = false;
            }
        }
    }
}