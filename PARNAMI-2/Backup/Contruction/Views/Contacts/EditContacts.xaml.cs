﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace Contruction
{
    public partial class EditContacts : ChildWindow
    {
        public ContactsModel Model
        {
            get { return (ContactsModel)this.DataContext; }
        }
				
        public EditContacts()
        {
            InitializeComponent();
        }

        public EditContacts(int id)
            :this()
        {
            Model.SelectedContact.ContactFor = "Vendor";
            Model.SelectedContact.ContactForID = id;
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            Model.CreateContact();
            this.DialogResult = true;
        }

        private void ChildWindow_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Escape)
            {
                this.DialogResult = false;
            }
        }

        private void ChildWindow_Loaded(object sender, RoutedEventArgs e)
        {
            tbxAddressName.Focus();
        }
    }
}

