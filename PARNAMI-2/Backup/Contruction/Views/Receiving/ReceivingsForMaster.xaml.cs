﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.ComponentModel;
using SIMS.MM.MODEL;
using System.Collections.ObjectModel;

namespace Contruction
{
    public partial class ReceivingsForMaster : UserControl, INotifyPropertyChanged
    {
        public ReceivingsForMastersModel Model
        {
            get { return (ReceivingsForMastersModel)this.DataContext; }
        }
		
        public ReceivingsForMaster()
        {
            InitializeComponent();
        }

        public int? VendorID
        {
            get { return Model.VendorID; }
            set
            {
                Model.VendorID = value;
                Notify("VendorID");
            }
        }

        public int? SiteID
        {
            get { return Model.SiteID; }
            set
            {
                Model.SiteID = value;
                Notify("SiteID");
            }
        }

        private void hpbtnReload_Click(object sender, RoutedEventArgs e)
        {
            //Model.Load();
        }
        
        public event PropertyChangedEventHandler PropertyChanged;
        public void Notify(string propertyName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
