﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using SIMS.MM.MODEL;
using System.ComponentModel;

namespace Contruction
{
    public partial class StockSummary : UserControl, INotifyPropertyChanged
    {
        public StockSummaryModel Model
        {
            get { return (StockSummaryModel)this.DataContext; }
        }
        
        public StockSummary()
        {
            InitializeComponent();
            //Model.LoadStockReport();
        }

        #region Model

        public DateTime? StartDate
        {
            get { return Model.StartDate; }
            set
            {
                Model.StartDate = value;
                Notify("StartDate");
            }
        }

        public DateTime? EndDate
        {
            get { return Model.EndDate; }
            set
            {
                Model.EndDate = value;
                Notify("EndDate");
            }
        }

        public int? SiteID
        {
            get { return Model.SiteID; }
            set
            {
                Model.SiteID = value;
                Notify("SiteID");
            }
        }

        public int? ItemID
        {
            get { return Model.ItemID; }
            set
            {
                Model.ItemID = value;
                Notify("ItemID");
            }
        }

        #endregion

        private void hpbtnClear_Click(object sender, RoutedEventArgs e)
        {
            StartDate = null;
            EndDate = null;
        }

        private void btnLoad_Click(object sender, RoutedEventArgs e)
        {
            if (startdate.SelectedDate.HasValue && enddate.SelectedDate.HasValue)
            {
                Model.StartDate = startdate.SelectedDate.Value;
                Model.EndDate = enddate.SelectedDate.Value;
            }
            Model.LoadStockReport();
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void Notify(string propertyName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
