﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Windows.Navigation;
using Contruction;
using Contruction.SrvLogin;

namespace Innosols
{
    using Web;
    using Web.Services;
    using SIMS.MM.MODEL;
    using Contruction.Views.Usermanagement;
    public partial class UserManagement : Page
    {
        UserManagementModel model;
        LoginServiceClient service;
        public UserManagement()
        {
            InitializeComponent();
            model = new UserManagementModel();
            service = model.service;
            this.DataContext = model;
            model.GetAll();
        }

        // Executes when the user navigates to this page.
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {

        }

        private void approveAction_Click(object sender, RoutedEventArgs e)
        {
            UserInfo user = (UserInfo)(sender as HyperlinkButton).DataContext;
            model.SelectedUser = user;
            if (user.IsApproved)
                model.BlockSelected();
            else
                model.ApproveSelected();
        }

        private void AddRoleLink_Click(object sender, RoutedEventArgs e)
        {
            UserInfo user = (UserInfo)(sender as HyperlinkButton).DataContext;
            var win = new SelectRoles();
            win.SetRolesList(model.AllRoles.Except(user.Roles));
            win.Show();
            win.Submitted += (s, args) => 
            {
                model.SelectedUser = user;
                model.AddRolesToUser(win.SelectedRoles);
            };
        }

        private void RemoveRoleLink_Click(object sender, RoutedEventArgs e)
        {
            UserInfo user = (UserInfo)(sender as HyperlinkButton).DataContext;
            var win = new SelectRoles();
            win.SetRolesList(user.Roles);
            win.Show();
            win.Submitted += (s, args) =>
            {
                model.SelectedUser = user;
                model.RemoveRolesToUser(win.SelectedRoles);
            };
        }

        private void SelectSiteLink_Click(object sender, RoutedEventArgs e)
        {
            UserInfo user = (UserInfo)(sender as HyperlinkButton).DataContext;
            var win = new SelectSites();
            win.SetSiteIDsExcept(user.AllowedSiteIDs);
            win.CreatePopup();
            win.Selected += (s, args) =>
            {
                model.SelectedUser = user;
                model.AddSitesToUser(win.SelectedIDs);
            };
        }

        private void RemoveSiteLink_Click(object sender, RoutedEventArgs e)
        {
            UserInfo user = (UserInfo)(sender as HyperlinkButton).DataContext;
            var win = new SelectSites();
            win.SetSiteIDs(user.AllowedSiteIDs);
            win.CreatePopup();
            win.Selected += (s, args) =>
            {
                model.SelectedUser = user;
                model.RemoveSitesToUser(win.SelectedIDs);
            };
        }

        private void AddNewRole_Clicked(object sender, RoutedEventArgs e)
        {
            var win = new CreateNewRole();
            win.Show();
        }

        private void AddNewUser_Clicked(object sender, RoutedEventArgs e)
        {
            //var win = new LoginUI.LoginRegistrationWindow();
            //win.LayoutUpdated += (s, a) => { win.NavigateToRegistration(); };
            //win.Show();
        }

        private void btnNewUser_Click(object sender, RoutedEventArgs e)
        {
            CreateNewUser cnu = new CreateNewUser();
            cnu.Show();
        }

        private void btnReload_Click(object sender, RoutedEventArgs e)
        {
            model.GetAll();
        }
    }
}
