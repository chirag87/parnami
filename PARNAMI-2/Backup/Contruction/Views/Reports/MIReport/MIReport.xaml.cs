﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using SIMS.MM.MODEL;
using System.IO;

namespace Contruction
{
    public partial class MIReport : UserControl
    {
        public MIReportVM Model
        {
            get { return (MIReportVM)this.DataContext; }
        }

        public MIReport()
        {
            InitializeComponent();
        }

        private void btnClearSet1_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            CloseOpenedTabItems();
            Model.ClearSet1();
        }

        private void btnClearSet2_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            CloseOpenedTabItems();
            Model.ClearSet2();

            this.ItemCategoryCombo.SelectedItem = null;
            this.ItemCombo.SelectedItem = null;
            this.SizeCombo.SelectedItem = null;
            this.BrandCombo.SelectedItem = null;
            this.SiteCombo.SelectedItem = null;
        }

        private void hpbtnItem_Click(object sender, RoutedEventArgs e)
        {
            var btn = sender as HyperlinkButton;
            var data = (MMIReportSummary)btn.DataContext;
            if (data == null) return;
            OpenNewTabItem(data);
        }

        private void hpbtnItemSiteWise_Click(object sender, RoutedEventArgs e)
        {
            var btn = sender as HyperlinkButton;
            var data = (MMIReportSiteWiseSummary)btn.DataContext;
            if (data == null) return;
            OpenNewTabItem(data);
        }

        private void btnLoad_Click(object sender, RoutedEventArgs e)
        {
            CloseOpenedTabItems();
            Model.Load();
        }

        public void CloseOpenedTabItems()
        {
            //Close all open tab items before loading data with filters
            this.tabMIReport.Items.OfType<CloseableTabItem>().ToList().ForEach(x => x.Close());
        }

        public void OpenNewTabItem(MMIReportSummary data)
        {
            if (data != null)
            {
                var tab = new CloseableTabItem();

                //DataTemplate headertemplate = this.Resources["TabHeaderTemplateForItems"] as DataTemplate;
                //tab.HeaderTemplate = headertemplate;

                tab.Header = data.Item;
                var uc = new MIReportDetails(data.ItemID, data.SizeID, data.BrandID, Model.SDate, Model.EDate, Model.SiteID, Model.ConsumableType, Model.Status);
                tab.Content = uc;
                uc.TabControl = tabMIReport;
                tab.Name = data.Item + " Details with Size: " + data.SizeID + " & Brand: " + data.BrandID;

                string name = data.Item + " Details with Size: " + data.SizeID + " & Brand: " + data.BrandID;
                var tabitem = tabMIReport.Items.OfType<TabItem>().SingleOrDefault(x => x.Name == name);
                if (tabMIReport.Items.Contains(tabitem))
                {
                    tabMIReport.SelectedItem = tabitem;
                }
                else
                {
                    try
                    {
                        tabMIReport.Items.Add(tab);
                        tabMIReport.SelectedItem = tab;
                    }
                    catch
                    {
                    }
                }
            }
        }

        public void OpenNewTabItem(MMIReportSiteWiseSummary data)
        {
            if (data != null)
            {
                var tab = new CloseableTabItem();

                //DataTemplate headertemplate = this.Resources["TabHeaderTemplateForItems"] as DataTemplate;
                //tab.HeaderTemplate = headertemplate;

                tab.Header = data.Item;
                var uc = new MIReportDetails(data.ItemID, data.SizeID, data.BrandID, Model.SDate, Model.EDate, data.SiteID, Model.ConsumableType, Model.Status);
                tab.Content = uc;
                uc.TabControl = tabMIReport;
                tab.Name = data.Item + " Details with Size: " + data.SizeID + " & Brand: " + data.BrandID;

                string name = data.Item + " Details with Size: " + data.SizeID + " & Brand: " + data.BrandID;
                var tabitem = tabMIReport.Items.OfType<TabItem>().SingleOrDefault(x => x.Name == name);
                if (tabMIReport.Items.Contains(tabitem))
                {
                    tabMIReport.SelectedItem = tabitem;
                }
                else
                {
                    try
                    {
                        tabMIReport.Items.Add(tab);
                        tabMIReport.SelectedItem = tab;
                    }
                    catch
                    {
                    }
                }
            }
        }

        private void ReportType_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var cmb = sender as ComboBox;
            var data = cmb.SelectedValue;
            if (data == null) return;
            Model.ReportType = data.ToString();
            if (Model.ReportType == "Site-Wise")
            {
                Model.IsSiteWiseReportSelected = true;
            }
            else
                Model.IsSiteWiseReportSelected = false;
        }

        SaveFileDialog sfd;

        private void download_Click(object sender, RoutedEventArgs e)
        {
            sfd = new SaveFileDialog();
            if (Model.RType != null)
            {
                if (Model.RType == "Excel")
                    sfd.Filter = "Excel files (*.xls)|*.xls";
                else if (Model.RType == "PDF")
                    sfd.Filter = "PDF file format|*.pdf";
            }
            else
            {
                sfd.Filter = "PDF file format|*.pdf";
            }
            if ((bool)sfd.ShowDialog())
            {
                Model.IsBusy = true;
                var hp = sender as HyperlinkButton;
                var data = (MIReportVM)hp.DataContext;

                WebClient webClient = new WebClient();
                webClient.OpenReadCompleted += (s, args) =>
                {
                    StreamReader sr = new StreamReader(args.Result);
                    byte[] bytes = new byte[args.Result.Length];
                    // revisit Here
                    args.Result.Read(bytes, 0, (int)args.Result.Length);
                    Stream st = sfd.OpenFile();
                    st.Write(bytes, 0, (int)args.Result.Length);
                    st.Close();
                    MessageBox.Show("DOWNLOAD COMPLETE!" + "\nFile Name: " + sfd.SafeFileName);
                    Model.IsBusy = false;
                };
                if (Model.ReportType == "Cumulative")
                {
                    Uri uri = new Uri("http://" + Base.Current.AppSettings.ServerPath + "/GetReport.ashx?type=MISummary&rtype=" + Model.RType + "&sdate=" + Model.SDate.ToString() + "&edate=" + Model.EDate.ToString() + "&itemid=" + Model.ItemID.ToString() + "&sizeid=" + Model.SizeID.ToString() + "&brandid=" + Model.BrandID.ToString() + "&siteid=" + Model.SiteID.ToString() + "&contype=" + Model.ConsumableType + "&status=" + Model.Status + "&showzero=" + Model.ShowZeroQty, UriKind.Absolute);
                    webClient.OpenReadAsync(uri);
                }
                else
                {
                    Uri uri = new Uri("http://" + Base.Current.AppSettings.ServerPath + "/GetReport.ashx?type=MISiteWiseSummary&rtype=" + Model.RType + "&sdate=" + Model.SDate.ToString() + "&edate=" + Model.EDate.ToString() + "&itemid=" + Model.ItemID.ToString() + "&sizeid=" + Model.SizeID.ToString() + "&brandid=" + Model.BrandID.ToString() + "&siteid=" + Model.SiteID.ToString() + "&contype=" + Model.ConsumableType + "&status=" + Model.Status + "&showzero=" + Model.ShowZeroQty, UriKind.Absolute);
                    webClient.OpenReadAsync(uri);
                }
            }
        }
    }
}
