﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Collections.ObjectModel;
using SIMS.MM.MODEL;

namespace Contruction
{
    public partial class NewMOfromSplittedPR : UserControl
    {
        public NewMOfromSplittedPRModel Model
        {
            get { return (NewMOfromSplittedPRModel)this.DataContext; }
        }
        
        public NewMOfromSplittedPR()
        {
            InitializeComponent();
            Model.CloseMOScreen += new EventHandler(Model_CloseMOScreen);
        }

        void Model_CloseMOScreen(object sender, EventArgs e)
        {
            if (this.MyPopUP != null)
                this.MyPopUP.Close();
        }

        int _OldPRID;
        public int OldPRID
        {
            get { return _OldPRID; }
            set
            {
                _OldPRID = value;
                Model.Notify("OldPRID");
            }
        }

        //ObservableCollection<int> _OldPRLineIDs = new ObservableCollection<int>();
        //public ObservableCollection<int> OldPRLineIDs
        //{
        //    get { return _OldPRLineIDs; }
        //    set
        //    {
        //        _OldPRLineIDs = value;
        //        Model.Notify("OldPRLineIDs");
        //    }
        //}

        ObservableCollection<MSplittedLines> _SplittedLines = new ObservableCollection<MSplittedLines>();
        public ObservableCollection<MSplittedLines> SplittedLines
        {
            get { return _SplittedLines; }
            set
            {
                _SplittedLines = value;
                Model.Notify("SplittedLines");
            }
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            this.MyPopUP.Close();
        }

        private void btnCreateMO_Click(object sender, RoutedEventArgs e)
        {
            //Model.CreateNewSplittedMO(OldPRID, OldPRLineIDs);
            //Model.SplitttoMOwithPartialQty(OldPRID, SplittedLines);
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            transferringSiteCombo.Focus();
        }

        #region CreatePopUP Members
        public bool IsPopUpMode
        {
            get { return MyPopUP != null; }
        }

        public PopUp MyPopUP { get; set; }

        public PopUp CreatePopup()
        {
            MyPopUP = this.ToPopup(false);
            MyPopUP.SetAutoLayout();
            MyPopUP.Title = "New Move Order";
            return MyPopUP;
        }
        #endregion
    }
}
