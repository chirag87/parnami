﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using SIMS.MM.MODEL;

namespace Contruction
{
    public partial class NewMO : UserControl
    {
        public NewMOModel Model
        {
            get { return (NewMOModel)this.DataContext; }
        }

        public NewMO()
        {
            InitializeComponent();
        }

        private void textBlock_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            var textblock = sender as TextBlock;
            var line = (MMOLine)textblock.DataContext;
            Model.DeleteLine(line.LineID);
        }
        
        private void btnCreateandRelease_Click(object sender, RoutedEventArgs e)
        {
            Model.RequireRelease = true;
            Model.SubmitToDB();
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            transferringSiteCombo.Focus();
        }

        private void UserControl_KeyDown(object sender, KeyEventArgs e)
        {
            if (Keyboard.Modifiers == (ModifierKeys.Control | ModifierKeys.Shift) && e.Key == Key.L)
            {
                MessageBox.Show("Done!");
            }
        }

        private void InfoIcon_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            var img = sender as Image;
            var data = (MMOLine)img.DataContext;
            if (data.ItemID == 0) return;
            var popup = new ItemSummary(data.ItemID, data.SizeID, data.BrandID);
            popup.CreatePopup();
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            Model.Reset();
            this.ResetForm();
        }

        public void ResetForm()
        {
            this.status = null;
            this.requestingSiteCombo.Text = " ";
            this.transferringSiteCombo.Text = " ";
            this.requestingSiteCombo.SelectedItem = null;
            this.transferringSiteCombo.SelectedItem = null;
        }

        #region CreatePopUP Members
        public bool IsPopUpMode
        {
            get { return MyPopUP != null; }
        }

        public PopUp MyPopUP { get; set; }

        public PopUp CreatePopup()
        {
            MyPopUP = this.ToPopup(false);
            MyPopUP.SetAutoLayout();
            MyPopUP.Title = "New Move Order";
            return MyPopUP;
        }
        #endregion
    }
}