﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace Contruction
{
    public partial class MOReleaseDetailedView : ChildWindow
    {
        public MOReleaseDetailedViewVM Model
        {
            get { return (MOReleaseDetailedViewVM)this.DataContext; }
        }
				
        public MOReleaseDetailedView()
        {
            InitializeComponent();
        }

        public MOReleaseDetailedView(int MOID, int RELID)
            : this()
        {
            Model.GetMOReleaseByID(MOID, RELID);
        }

        private void btn_OK_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

        private void ChildWindow_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Escape)
                this.DialogResult = false;
        }
    }
}

