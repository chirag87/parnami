﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Threading;

namespace Contruction
{
    public class FixedTreeViewDragDropTarget : TreeViewDragDropTarget
    {
        private Timer dragDropDelayTimer;

        public FixedTreeViewDragDropTarget()
        {
            AddHandler(MouseLeftButtonUpEvent, new MouseButtonEventHandler(HandleMouseLeftButtonUp), true);
            AddHandler( MouseLeftButtonDownEvent, new MouseButtonEventHandler( HandleMouseLeftButtonDown ), true );
        }

        private void HandleMouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            IsMouseDown = false;
            lock (this)
            {
                // Cancel the pending drag operation
                if (dragDropDelayTimer != null)
                    dragDropDelayTimer.Dispose();

                dragDropDelayTimer = null;
            }
        }

        //protected override void OnQueryContinueDrag(QueryContinueDragEventArgs args)
        //{
        //    if (!IsMouseDown)
        //    {
        //        args.Action = DragAction.Cancel;
        //        args.Handled = true;
        //    }
        //    base.OnQueryContinueDrag(args);
        //}

        #region Constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="T:FixedListBoxDragDropTarget"/> class.
		/// </summary>
		
		#endregion

		#region Properties

		/// <summary>
		/// Whether or not the mouse is currently down on the element beneath it.
		/// </summary>
		public static bool IsMouseDown
		{
			get;
			private set;
		}

		#endregion

		#region Event Handlers

		/// <summary>
		/// Handles the MouseLeftButtonDown event of the control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="T:MouseButtonEventArgs"/> instance containing the event data.</param>
		private void HandleMouseLeftButtonDown( object sender, MouseButtonEventArgs e )
		{
			IsMouseDown = true;
		}

		/// <summary>
		/// Handles the MouseLeftButtonUp event of the control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="T:MouseButtonEventArgs"/> instance containing the event data.</param>
		

		/// <summary>
		/// Adds all selected items when drag operation begins.
		/// </summary>
		/// <param name="eventArgs">Information about the event.</param>
		protected override void OnItemDragStarting( ItemDragEventArgs eventArgs )
		{
			if ( IsMouseDown ) {
				base.OnItemDragStarting( eventArgs );
			}   // if
			else {
				eventArgs.Cancel = true;
				eventArgs.Handled = true;
			}   // else
		}

		#endregion

    }
}
