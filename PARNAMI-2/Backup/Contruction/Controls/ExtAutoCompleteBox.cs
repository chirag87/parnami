﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Collections.ObjectModel;

namespace Contruction
{
    public class ExtAutoCompleteBox : AutoCompleteBox
    {
        private string _extText;

        public string ExtText
        {
            get
            {
                string s1 = "";
                foreach (string s in SelectedItems)
                    s1 += s + ";";

                if (s1 == "")
                    return "";

                return s1.Substring(0, s1.Length - 1);

            }
            set
            {
                _extText = value;
                AddItems();
            }
        }
        public ExtAutoCompleteBox()
        {
            DefaultStyleKey = typeof(ExtAutoCompleteBox);
            SelectedItems = new ObservableCollection<string>();
        }



        public ObservableCollection<string> SelectedItems
        {
            get { return (ObservableCollection<string>)GetValue(SelectedItemsProperty); }
            set { SetValue(SelectedItemsProperty, value); }
        }

        // Using a DependencyProperty as the backing store for SelectedItems.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty SelectedItemsProperty =
            DependencyProperty.Register("SelectedItems", typeof(ObservableCollection<string>), typeof(ExtAutoCompleteBox), null);



        public DataTemplate SelectedItemsTemplate
        {
            get { return (DataTemplate)GetValue(SelectedItemsTemplateProperty); }
            set { SetValue(SelectedItemsTemplateProperty, value); }
        }

        // Using a DependencyProperty as the backing store for SelectedItemsTemplate.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty SelectedItemsTemplateProperty =
            DependencyProperty.Register("SelectedItemsTemplate", typeof(DataTemplate), typeof(ExtAutoCompleteBox), null);


        ItemsControl _selectedItemsControl;
        TextBox txtBox;

        private void AddItems()
        {
            if (string.IsNullOrEmpty(_extText))
                return;

            if (_selectedItemsControl == null)
                return;

            _selectedItemsControl.Items.Clear();
            foreach (string s in _extText.Split(';'))
            {
                AddItem(s);
            }
        }

        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();
            _selectedItemsControl = GetTemplateChild("SelectedItemsControl") as ItemsControl;
            txtBox = GetTemplateChild("Text") as TextBox;

            AddItems();
        }

        string previtem = null;
        protected override ISelectionAdapter GetSelectionAdapterPart()
        {
            ISelectionAdapter adapter = base.GetSelectionAdapterPart();
            adapter.Commit += new RoutedEventHandler(adapter_Commit);
            adapter.Cancel += new RoutedEventHandler(adapter_Cancel);
            return adapter;
        }



        void adapter_Cancel(object sender, RoutedEventArgs e)
        {
            previtem = null;
        }

        void adapter_Commit(object sender, RoutedEventArgs e)
        {
            if (previtem != null)
            {
                AddItem(previtem);
                txtBox.Text = "";
            }
        }
        protected override void OnSelectionChanged(SelectionChangedEventArgs e)
        {
            base.OnSelectionChanged(e);
            if (e.AddedItems.Count > 0)
                previtem = e.AddedItems[0].ToString();
        }

        private void AddItem(string s)
        {
            _selectedItemsControl.Items.Add(new ExtAutoCompleteBoxItem(s, this));
            SelectedItems.Add(s);
        }

        public void RemoveItem(string s)
        {
            foreach (ExtAutoCompleteBoxItem item in _selectedItemsControl.Items)
            {
                if (item.Content.ToString() == s)
                {
                    _selectedItemsControl.Items.Remove(item);
                    SelectedItems.Remove(item.Content.ToString());
                    break;
                }
            }
        }
    }
}
