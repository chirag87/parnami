﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace Contruction
{
    public partial class ExcelExporter : UserControl
    {
        public ExcelExporter()
        {
            // Required to initialize variables
            InitializeComponent();
        }

        public DataGrid DataGrid
        {
            get { return (DataGrid)GetValue(DataGridProperty); }
            set { SetValue(DataGridProperty, value); }
        }

        // Using a DependencyProperty as the backing store for DataGrid.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty DataGridProperty =
            DependencyProperty.Register("DataGrid", typeof(DataGrid), typeof(ExcelExporter), new PropertyMetadata(null));

        public bool ShowText
        {
            get { return (bool)GetValue(ShowTextProperty); }
            set
            {
                SetValue(ShowTextProperty, value);
                this.tbx_text.Visibility = value ? Visibility.Visible : Visibility.Collapsed;
            }
        }

        // Using a DependencyProperty as the backing store for ShowText.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ShowTextProperty =
            DependencyProperty.Register("ShowText", typeof(bool), typeof(ExcelExporter), new PropertyMetadata(true));

        private void button_Click(object sender, RoutedEventArgs e)
        {
            Export();
        }

        public void Export()
        {
            if (this.DataGrid != null)
                this.DataGrid.Export();
        }
    }
}
