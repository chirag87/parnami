﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace Contruction
{
    public class SSUpdatedArgs : EventArgs
    {
        public int EmpID { get; set; }
        public SSUpdatedArgs(int _EmpID) { EmpID = _EmpID; }
    }

    public class SSShiftArgs : EventArgs
    {
        public int EmpID { get; set; }
        public bool IsFromEditPage { get; set; }
        public SSShiftArgs(int _EmpID, bool _IsFromEditPage) { EmpID = _EmpID; IsFromEditPage = _IsFromEditPage; }
    }
    public class EmployeeEvents
    {
        public ChildWindow SS = new ChildWindow();
        public EmployeeEvents()
        {
            OnInit();
        }

        private void OnInit()
        {
            
        }

        static EmployeeEvents _Current=new EmployeeEvents();
        public static EmployeeEvents Current
        {
            get { return _Current; }
            set
            {
                _Current = value;
            }
        }

        public event EventHandler NewEmployeeAdded;
        public void RaiseNewEmployeeAdded()
        {
            if (NewEmployeeAdded != null)
                NewEmployeeAdded(this, new EventArgs());
        }

        public event EventHandler NewHolidayAdded;
        public void RaiseNewHolidayAdded()
        {
            if (NewHolidayAdded != null)
                NewHolidayAdded(this, new EventArgs());
        }

        public event EventHandler<SSUpdatedArgs> SSUpdated;
        public void RaiseSSUpdated(int EmpID)
        {
            if (SSUpdated != null)
                SSUpdated(this, new SSUpdatedArgs(EmpID));
        }

        public event EventHandler<SSShiftArgs> SSShifted;
        public void RaiseSSShifted(int EmpID, bool IsFromEditPage)
        {
            if (SSShifted != null)
                SSShifted(this, new SSShiftArgs(EmpID, IsFromEditPage));
        }

    }
}
