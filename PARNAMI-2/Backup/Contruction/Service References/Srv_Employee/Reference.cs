﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.296
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

// 
// This code was auto-generated by Microsoft.Silverlight.ServiceReference, version 4.0.50826.0
// 
namespace Contruction.Srv_Employee {
    
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ServiceModel.ServiceContractAttribute(ConfigurationName="Srv_Employee.IEmployeeService")]
    public interface IEmployeeService {
        
        [System.ServiceModel.OperationContractAttribute(AsyncPattern=true, Action="http://tempuri.org/IEmployeeService/GetAllEmployees", ReplyAction="http://tempuri.org/IEmployeeService/GetAllEmployeesResponse")]
        System.IAsyncResult BeginGetAllEmployees(string username, System.Nullable<int> pageindex, System.Nullable<int> pagesize, string Key, System.Nullable<int> siteid, System.Nullable<System.DateTime> AttDate, bool showall, System.AsyncCallback callback, object asyncState);
        
        SIMS.Core.PaginatedData<SIMS.EM.Model.MEmployee> EndGetAllEmployees(System.IAsyncResult result);
        
        [System.ServiceModel.OperationContractAttribute(AsyncPattern=true, Action="http://tempuri.org/IEmployeeService/GetEmployeeByID", ReplyAction="http://tempuri.org/IEmployeeService/GetEmployeeByIDResponse")]
        System.IAsyncResult BeginGetEmployeeByID(int EmployeeID, System.AsyncCallback callback, object asyncState);
        
        SIMS.EM.Model.MEmployee EndGetEmployeeByID(System.IAsyncResult result);
        
        [System.ServiceModel.OperationContractAttribute(AsyncPattern=true, Action="http://tempuri.org/IEmployeeService/CreateNewEmployee", ReplyAction="http://tempuri.org/IEmployeeService/CreateNewEmployeeResponse")]
        System.IAsyncResult BeginCreateNewEmployee(SIMS.EM.Model.MEmployee _NewEmployee, System.AsyncCallback callback, object asyncState);
        
        SIMS.EM.Model.MEmployee EndCreateNewEmployee(System.IAsyncResult result);
        
        [System.ServiceModel.OperationContractAttribute(AsyncPattern=true, Action="http://tempuri.org/IEmployeeService/UpdateEmployee", ReplyAction="http://tempuri.org/IEmployeeService/UpdateEmployeeResponse")]
        System.IAsyncResult BeginUpdateEmployee(SIMS.EM.Model.MEmployee _UpdatedEmployee, System.AsyncCallback callback, object asyncState);
        
        SIMS.EM.Model.MEmployee EndUpdateEmployee(System.IAsyncResult result);
        
        [System.ServiceModel.OperationContractAttribute(AsyncPattern=true, Action="http://tempuri.org/IEmployeeService/DeleteEmployee", ReplyAction="http://tempuri.org/IEmployeeService/DeleteEmployeeResponse")]
        System.IAsyncResult BeginDeleteEmployee(int EmpID, System.AsyncCallback callback, object asyncState);
        
        void EndDeleteEmployee(System.IAsyncResult result);
        
        [System.ServiceModel.OperationContractAttribute(AsyncPattern=true, Action="http://tempuri.org/IEmployeeService/CreateNewEmployeeAttendance", ReplyAction="http://tempuri.org/IEmployeeService/CreateNewEmployeeAttendanceResponse")]
        System.IAsyncResult BeginCreateNewEmployeeAttendance(SIMS.EM.Model.MEmployeeAttendance _NewEmpAttendance, System.AsyncCallback callback, object asyncState);
        
        SIMS.EM.Model.MEmployeeAttendance EndCreateNewEmployeeAttendance(System.IAsyncResult result);
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public interface IEmployeeServiceChannel : Contruction.Srv_Employee.IEmployeeService, System.ServiceModel.IClientChannel {
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public partial class GetAllEmployeesCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs {
        
        private object[] results;
        
        public GetAllEmployeesCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState) : 
                base(exception, cancelled, userState) {
            this.results = results;
        }
        
        public SIMS.Core.PaginatedData<SIMS.EM.Model.MEmployee> Result {
            get {
                base.RaiseExceptionIfNecessary();
                return ((SIMS.Core.PaginatedData<SIMS.EM.Model.MEmployee>)(this.results[0]));
            }
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public partial class GetEmployeeByIDCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs {
        
        private object[] results;
        
        public GetEmployeeByIDCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState) : 
                base(exception, cancelled, userState) {
            this.results = results;
        }
        
        public SIMS.EM.Model.MEmployee Result {
            get {
                base.RaiseExceptionIfNecessary();
                return ((SIMS.EM.Model.MEmployee)(this.results[0]));
            }
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public partial class CreateNewEmployeeCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs {
        
        private object[] results;
        
        public CreateNewEmployeeCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState) : 
                base(exception, cancelled, userState) {
            this.results = results;
        }
        
        public SIMS.EM.Model.MEmployee Result {
            get {
                base.RaiseExceptionIfNecessary();
                return ((SIMS.EM.Model.MEmployee)(this.results[0]));
            }
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public partial class UpdateEmployeeCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs {
        
        private object[] results;
        
        public UpdateEmployeeCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState) : 
                base(exception, cancelled, userState) {
            this.results = results;
        }
        
        public SIMS.EM.Model.MEmployee Result {
            get {
                base.RaiseExceptionIfNecessary();
                return ((SIMS.EM.Model.MEmployee)(this.results[0]));
            }
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public partial class CreateNewEmployeeAttendanceCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs {
        
        private object[] results;
        
        public CreateNewEmployeeAttendanceCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState) : 
                base(exception, cancelled, userState) {
            this.results = results;
        }
        
        public SIMS.EM.Model.MEmployeeAttendance Result {
            get {
                base.RaiseExceptionIfNecessary();
                return ((SIMS.EM.Model.MEmployeeAttendance)(this.results[0]));
            }
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public partial class EmployeeServiceClient : System.ServiceModel.ClientBase<Contruction.Srv_Employee.IEmployeeService>, Contruction.Srv_Employee.IEmployeeService {
        
        private BeginOperationDelegate onBeginGetAllEmployeesDelegate;
        
        private EndOperationDelegate onEndGetAllEmployeesDelegate;
        
        private System.Threading.SendOrPostCallback onGetAllEmployeesCompletedDelegate;
        
        private BeginOperationDelegate onBeginGetEmployeeByIDDelegate;
        
        private EndOperationDelegate onEndGetEmployeeByIDDelegate;
        
        private System.Threading.SendOrPostCallback onGetEmployeeByIDCompletedDelegate;
        
        private BeginOperationDelegate onBeginCreateNewEmployeeDelegate;
        
        private EndOperationDelegate onEndCreateNewEmployeeDelegate;
        
        private System.Threading.SendOrPostCallback onCreateNewEmployeeCompletedDelegate;
        
        private BeginOperationDelegate onBeginUpdateEmployeeDelegate;
        
        private EndOperationDelegate onEndUpdateEmployeeDelegate;
        
        private System.Threading.SendOrPostCallback onUpdateEmployeeCompletedDelegate;
        
        private BeginOperationDelegate onBeginDeleteEmployeeDelegate;
        
        private EndOperationDelegate onEndDeleteEmployeeDelegate;
        
        private System.Threading.SendOrPostCallback onDeleteEmployeeCompletedDelegate;
        
        private BeginOperationDelegate onBeginCreateNewEmployeeAttendanceDelegate;
        
        private EndOperationDelegate onEndCreateNewEmployeeAttendanceDelegate;
        
        private System.Threading.SendOrPostCallback onCreateNewEmployeeAttendanceCompletedDelegate;
        
        private BeginOperationDelegate onBeginOpenDelegate;
        
        private EndOperationDelegate onEndOpenDelegate;
        
        private System.Threading.SendOrPostCallback onOpenCompletedDelegate;
        
        private BeginOperationDelegate onBeginCloseDelegate;
        
        private EndOperationDelegate onEndCloseDelegate;
        
        private System.Threading.SendOrPostCallback onCloseCompletedDelegate;
        
        public EmployeeServiceClient() {
        }
        
        public EmployeeServiceClient(string endpointConfigurationName) : 
                base(endpointConfigurationName) {
        }
        
        public EmployeeServiceClient(string endpointConfigurationName, string remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public EmployeeServiceClient(string endpointConfigurationName, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public EmployeeServiceClient(System.ServiceModel.Channels.Binding binding, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(binding, remoteAddress) {
        }
        
        public System.Net.CookieContainer CookieContainer {
            get {
                System.ServiceModel.Channels.IHttpCookieContainerManager httpCookieContainerManager = this.InnerChannel.GetProperty<System.ServiceModel.Channels.IHttpCookieContainerManager>();
                if ((httpCookieContainerManager != null)) {
                    return httpCookieContainerManager.CookieContainer;
                }
                else {
                    return null;
                }
            }
            set {
                System.ServiceModel.Channels.IHttpCookieContainerManager httpCookieContainerManager = this.InnerChannel.GetProperty<System.ServiceModel.Channels.IHttpCookieContainerManager>();
                if ((httpCookieContainerManager != null)) {
                    httpCookieContainerManager.CookieContainer = value;
                }
                else {
                    throw new System.InvalidOperationException("Unable to set the CookieContainer. Please make sure the binding contains an HttpC" +
                            "ookieContainerBindingElement.");
                }
            }
        }
        
        public event System.EventHandler<GetAllEmployeesCompletedEventArgs> GetAllEmployeesCompleted;
        
        public event System.EventHandler<GetEmployeeByIDCompletedEventArgs> GetEmployeeByIDCompleted;
        
        public event System.EventHandler<CreateNewEmployeeCompletedEventArgs> CreateNewEmployeeCompleted;
        
        public event System.EventHandler<UpdateEmployeeCompletedEventArgs> UpdateEmployeeCompleted;
        
        public event System.EventHandler<System.ComponentModel.AsyncCompletedEventArgs> DeleteEmployeeCompleted;
        
        public event System.EventHandler<CreateNewEmployeeAttendanceCompletedEventArgs> CreateNewEmployeeAttendanceCompleted;
        
        public event System.EventHandler<System.ComponentModel.AsyncCompletedEventArgs> OpenCompleted;
        
        public event System.EventHandler<System.ComponentModel.AsyncCompletedEventArgs> CloseCompleted;
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        System.IAsyncResult Contruction.Srv_Employee.IEmployeeService.BeginGetAllEmployees(string username, System.Nullable<int> pageindex, System.Nullable<int> pagesize, string Key, System.Nullable<int> siteid, System.Nullable<System.DateTime> AttDate, bool showall, System.AsyncCallback callback, object asyncState) {
            return base.Channel.BeginGetAllEmployees(username, pageindex, pagesize, Key, siteid, AttDate, showall, callback, asyncState);
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        SIMS.Core.PaginatedData<SIMS.EM.Model.MEmployee> Contruction.Srv_Employee.IEmployeeService.EndGetAllEmployees(System.IAsyncResult result) {
            return base.Channel.EndGetAllEmployees(result);
        }
        
        private System.IAsyncResult OnBeginGetAllEmployees(object[] inValues, System.AsyncCallback callback, object asyncState) {
            string username = ((string)(inValues[0]));
            System.Nullable<int> pageindex = ((System.Nullable<int>)(inValues[1]));
            System.Nullable<int> pagesize = ((System.Nullable<int>)(inValues[2]));
            string Key = ((string)(inValues[3]));
            System.Nullable<int> siteid = ((System.Nullable<int>)(inValues[4]));
            System.Nullable<System.DateTime> AttDate = ((System.Nullable<System.DateTime>)(inValues[5]));
            bool showall = ((bool)(inValues[6]));
            return ((Contruction.Srv_Employee.IEmployeeService)(this)).BeginGetAllEmployees(username, pageindex, pagesize, Key, siteid, AttDate, showall, callback, asyncState);
        }
        
        private object[] OnEndGetAllEmployees(System.IAsyncResult result) {
            SIMS.Core.PaginatedData<SIMS.EM.Model.MEmployee> retVal = ((Contruction.Srv_Employee.IEmployeeService)(this)).EndGetAllEmployees(result);
            return new object[] {
                    retVal};
        }
        
        private void OnGetAllEmployeesCompleted(object state) {
            if ((this.GetAllEmployeesCompleted != null)) {
                InvokeAsyncCompletedEventArgs e = ((InvokeAsyncCompletedEventArgs)(state));
                this.GetAllEmployeesCompleted(this, new GetAllEmployeesCompletedEventArgs(e.Results, e.Error, e.Cancelled, e.UserState));
            }
        }
        
        public void GetAllEmployeesAsync(string username, System.Nullable<int> pageindex, System.Nullable<int> pagesize, string Key, System.Nullable<int> siteid, System.Nullable<System.DateTime> AttDate, bool showall) {
            this.GetAllEmployeesAsync(username, pageindex, pagesize, Key, siteid, AttDate, showall, null);
        }
        
        public void GetAllEmployeesAsync(string username, System.Nullable<int> pageindex, System.Nullable<int> pagesize, string Key, System.Nullable<int> siteid, System.Nullable<System.DateTime> AttDate, bool showall, object userState) {
            if ((this.onBeginGetAllEmployeesDelegate == null)) {
                this.onBeginGetAllEmployeesDelegate = new BeginOperationDelegate(this.OnBeginGetAllEmployees);
            }
            if ((this.onEndGetAllEmployeesDelegate == null)) {
                this.onEndGetAllEmployeesDelegate = new EndOperationDelegate(this.OnEndGetAllEmployees);
            }
            if ((this.onGetAllEmployeesCompletedDelegate == null)) {
                this.onGetAllEmployeesCompletedDelegate = new System.Threading.SendOrPostCallback(this.OnGetAllEmployeesCompleted);
            }
            base.InvokeAsync(this.onBeginGetAllEmployeesDelegate, new object[] {
                        username,
                        pageindex,
                        pagesize,
                        Key,
                        siteid,
                        AttDate,
                        showall}, this.onEndGetAllEmployeesDelegate, this.onGetAllEmployeesCompletedDelegate, userState);
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        System.IAsyncResult Contruction.Srv_Employee.IEmployeeService.BeginGetEmployeeByID(int EmployeeID, System.AsyncCallback callback, object asyncState) {
            return base.Channel.BeginGetEmployeeByID(EmployeeID, callback, asyncState);
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        SIMS.EM.Model.MEmployee Contruction.Srv_Employee.IEmployeeService.EndGetEmployeeByID(System.IAsyncResult result) {
            return base.Channel.EndGetEmployeeByID(result);
        }
        
        private System.IAsyncResult OnBeginGetEmployeeByID(object[] inValues, System.AsyncCallback callback, object asyncState) {
            int EmployeeID = ((int)(inValues[0]));
            return ((Contruction.Srv_Employee.IEmployeeService)(this)).BeginGetEmployeeByID(EmployeeID, callback, asyncState);
        }
        
        private object[] OnEndGetEmployeeByID(System.IAsyncResult result) {
            SIMS.EM.Model.MEmployee retVal = ((Contruction.Srv_Employee.IEmployeeService)(this)).EndGetEmployeeByID(result);
            return new object[] {
                    retVal};
        }
        
        private void OnGetEmployeeByIDCompleted(object state) {
            if ((this.GetEmployeeByIDCompleted != null)) {
                InvokeAsyncCompletedEventArgs e = ((InvokeAsyncCompletedEventArgs)(state));
                this.GetEmployeeByIDCompleted(this, new GetEmployeeByIDCompletedEventArgs(e.Results, e.Error, e.Cancelled, e.UserState));
            }
        }
        
        public void GetEmployeeByIDAsync(int EmployeeID) {
            this.GetEmployeeByIDAsync(EmployeeID, null);
        }
        
        public void GetEmployeeByIDAsync(int EmployeeID, object userState) {
            if ((this.onBeginGetEmployeeByIDDelegate == null)) {
                this.onBeginGetEmployeeByIDDelegate = new BeginOperationDelegate(this.OnBeginGetEmployeeByID);
            }
            if ((this.onEndGetEmployeeByIDDelegate == null)) {
                this.onEndGetEmployeeByIDDelegate = new EndOperationDelegate(this.OnEndGetEmployeeByID);
            }
            if ((this.onGetEmployeeByIDCompletedDelegate == null)) {
                this.onGetEmployeeByIDCompletedDelegate = new System.Threading.SendOrPostCallback(this.OnGetEmployeeByIDCompleted);
            }
            base.InvokeAsync(this.onBeginGetEmployeeByIDDelegate, new object[] {
                        EmployeeID}, this.onEndGetEmployeeByIDDelegate, this.onGetEmployeeByIDCompletedDelegate, userState);
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        System.IAsyncResult Contruction.Srv_Employee.IEmployeeService.BeginCreateNewEmployee(SIMS.EM.Model.MEmployee _NewEmployee, System.AsyncCallback callback, object asyncState) {
            return base.Channel.BeginCreateNewEmployee(_NewEmployee, callback, asyncState);
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        SIMS.EM.Model.MEmployee Contruction.Srv_Employee.IEmployeeService.EndCreateNewEmployee(System.IAsyncResult result) {
            return base.Channel.EndCreateNewEmployee(result);
        }
        
        private System.IAsyncResult OnBeginCreateNewEmployee(object[] inValues, System.AsyncCallback callback, object asyncState) {
            SIMS.EM.Model.MEmployee _NewEmployee = ((SIMS.EM.Model.MEmployee)(inValues[0]));
            return ((Contruction.Srv_Employee.IEmployeeService)(this)).BeginCreateNewEmployee(_NewEmployee, callback, asyncState);
        }
        
        private object[] OnEndCreateNewEmployee(System.IAsyncResult result) {
            SIMS.EM.Model.MEmployee retVal = ((Contruction.Srv_Employee.IEmployeeService)(this)).EndCreateNewEmployee(result);
            return new object[] {
                    retVal};
        }
        
        private void OnCreateNewEmployeeCompleted(object state) {
            if ((this.CreateNewEmployeeCompleted != null)) {
                InvokeAsyncCompletedEventArgs e = ((InvokeAsyncCompletedEventArgs)(state));
                this.CreateNewEmployeeCompleted(this, new CreateNewEmployeeCompletedEventArgs(e.Results, e.Error, e.Cancelled, e.UserState));
            }
        }
        
        public void CreateNewEmployeeAsync(SIMS.EM.Model.MEmployee _NewEmployee) {
            this.CreateNewEmployeeAsync(_NewEmployee, null);
        }
        
        public void CreateNewEmployeeAsync(SIMS.EM.Model.MEmployee _NewEmployee, object userState) {
            if ((this.onBeginCreateNewEmployeeDelegate == null)) {
                this.onBeginCreateNewEmployeeDelegate = new BeginOperationDelegate(this.OnBeginCreateNewEmployee);
            }
            if ((this.onEndCreateNewEmployeeDelegate == null)) {
                this.onEndCreateNewEmployeeDelegate = new EndOperationDelegate(this.OnEndCreateNewEmployee);
            }
            if ((this.onCreateNewEmployeeCompletedDelegate == null)) {
                this.onCreateNewEmployeeCompletedDelegate = new System.Threading.SendOrPostCallback(this.OnCreateNewEmployeeCompleted);
            }
            base.InvokeAsync(this.onBeginCreateNewEmployeeDelegate, new object[] {
                        _NewEmployee}, this.onEndCreateNewEmployeeDelegate, this.onCreateNewEmployeeCompletedDelegate, userState);
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        System.IAsyncResult Contruction.Srv_Employee.IEmployeeService.BeginUpdateEmployee(SIMS.EM.Model.MEmployee _UpdatedEmployee, System.AsyncCallback callback, object asyncState) {
            return base.Channel.BeginUpdateEmployee(_UpdatedEmployee, callback, asyncState);
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        SIMS.EM.Model.MEmployee Contruction.Srv_Employee.IEmployeeService.EndUpdateEmployee(System.IAsyncResult result) {
            return base.Channel.EndUpdateEmployee(result);
        }
        
        private System.IAsyncResult OnBeginUpdateEmployee(object[] inValues, System.AsyncCallback callback, object asyncState) {
            SIMS.EM.Model.MEmployee _UpdatedEmployee = ((SIMS.EM.Model.MEmployee)(inValues[0]));
            return ((Contruction.Srv_Employee.IEmployeeService)(this)).BeginUpdateEmployee(_UpdatedEmployee, callback, asyncState);
        }
        
        private object[] OnEndUpdateEmployee(System.IAsyncResult result) {
            SIMS.EM.Model.MEmployee retVal = ((Contruction.Srv_Employee.IEmployeeService)(this)).EndUpdateEmployee(result);
            return new object[] {
                    retVal};
        }
        
        private void OnUpdateEmployeeCompleted(object state) {
            if ((this.UpdateEmployeeCompleted != null)) {
                InvokeAsyncCompletedEventArgs e = ((InvokeAsyncCompletedEventArgs)(state));
                this.UpdateEmployeeCompleted(this, new UpdateEmployeeCompletedEventArgs(e.Results, e.Error, e.Cancelled, e.UserState));
            }
        }
        
        public void UpdateEmployeeAsync(SIMS.EM.Model.MEmployee _UpdatedEmployee) {
            this.UpdateEmployeeAsync(_UpdatedEmployee, null);
        }
        
        public void UpdateEmployeeAsync(SIMS.EM.Model.MEmployee _UpdatedEmployee, object userState) {
            if ((this.onBeginUpdateEmployeeDelegate == null)) {
                this.onBeginUpdateEmployeeDelegate = new BeginOperationDelegate(this.OnBeginUpdateEmployee);
            }
            if ((this.onEndUpdateEmployeeDelegate == null)) {
                this.onEndUpdateEmployeeDelegate = new EndOperationDelegate(this.OnEndUpdateEmployee);
            }
            if ((this.onUpdateEmployeeCompletedDelegate == null)) {
                this.onUpdateEmployeeCompletedDelegate = new System.Threading.SendOrPostCallback(this.OnUpdateEmployeeCompleted);
            }
            base.InvokeAsync(this.onBeginUpdateEmployeeDelegate, new object[] {
                        _UpdatedEmployee}, this.onEndUpdateEmployeeDelegate, this.onUpdateEmployeeCompletedDelegate, userState);
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        System.IAsyncResult Contruction.Srv_Employee.IEmployeeService.BeginDeleteEmployee(int EmpID, System.AsyncCallback callback, object asyncState) {
            return base.Channel.BeginDeleteEmployee(EmpID, callback, asyncState);
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        void Contruction.Srv_Employee.IEmployeeService.EndDeleteEmployee(System.IAsyncResult result) {
            base.Channel.EndDeleteEmployee(result);
        }
        
        private System.IAsyncResult OnBeginDeleteEmployee(object[] inValues, System.AsyncCallback callback, object asyncState) {
            int EmpID = ((int)(inValues[0]));
            return ((Contruction.Srv_Employee.IEmployeeService)(this)).BeginDeleteEmployee(EmpID, callback, asyncState);
        }
        
        private object[] OnEndDeleteEmployee(System.IAsyncResult result) {
            ((Contruction.Srv_Employee.IEmployeeService)(this)).EndDeleteEmployee(result);
            return null;
        }
        
        private void OnDeleteEmployeeCompleted(object state) {
            if ((this.DeleteEmployeeCompleted != null)) {
                InvokeAsyncCompletedEventArgs e = ((InvokeAsyncCompletedEventArgs)(state));
                this.DeleteEmployeeCompleted(this, new System.ComponentModel.AsyncCompletedEventArgs(e.Error, e.Cancelled, e.UserState));
            }
        }
        
        public void DeleteEmployeeAsync(int EmpID) {
            this.DeleteEmployeeAsync(EmpID, null);
        }
        
        public void DeleteEmployeeAsync(int EmpID, object userState) {
            if ((this.onBeginDeleteEmployeeDelegate == null)) {
                this.onBeginDeleteEmployeeDelegate = new BeginOperationDelegate(this.OnBeginDeleteEmployee);
            }
            if ((this.onEndDeleteEmployeeDelegate == null)) {
                this.onEndDeleteEmployeeDelegate = new EndOperationDelegate(this.OnEndDeleteEmployee);
            }
            if ((this.onDeleteEmployeeCompletedDelegate == null)) {
                this.onDeleteEmployeeCompletedDelegate = new System.Threading.SendOrPostCallback(this.OnDeleteEmployeeCompleted);
            }
            base.InvokeAsync(this.onBeginDeleteEmployeeDelegate, new object[] {
                        EmpID}, this.onEndDeleteEmployeeDelegate, this.onDeleteEmployeeCompletedDelegate, userState);
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        System.IAsyncResult Contruction.Srv_Employee.IEmployeeService.BeginCreateNewEmployeeAttendance(SIMS.EM.Model.MEmployeeAttendance _NewEmpAttendance, System.AsyncCallback callback, object asyncState) {
            return base.Channel.BeginCreateNewEmployeeAttendance(_NewEmpAttendance, callback, asyncState);
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        SIMS.EM.Model.MEmployeeAttendance Contruction.Srv_Employee.IEmployeeService.EndCreateNewEmployeeAttendance(System.IAsyncResult result) {
            return base.Channel.EndCreateNewEmployeeAttendance(result);
        }
        
        private System.IAsyncResult OnBeginCreateNewEmployeeAttendance(object[] inValues, System.AsyncCallback callback, object asyncState) {
            SIMS.EM.Model.MEmployeeAttendance _NewEmpAttendance = ((SIMS.EM.Model.MEmployeeAttendance)(inValues[0]));
            return ((Contruction.Srv_Employee.IEmployeeService)(this)).BeginCreateNewEmployeeAttendance(_NewEmpAttendance, callback, asyncState);
        }
        
        private object[] OnEndCreateNewEmployeeAttendance(System.IAsyncResult result) {
            SIMS.EM.Model.MEmployeeAttendance retVal = ((Contruction.Srv_Employee.IEmployeeService)(this)).EndCreateNewEmployeeAttendance(result);
            return new object[] {
                    retVal};
        }
        
        private void OnCreateNewEmployeeAttendanceCompleted(object state) {
            if ((this.CreateNewEmployeeAttendanceCompleted != null)) {
                InvokeAsyncCompletedEventArgs e = ((InvokeAsyncCompletedEventArgs)(state));
                this.CreateNewEmployeeAttendanceCompleted(this, new CreateNewEmployeeAttendanceCompletedEventArgs(e.Results, e.Error, e.Cancelled, e.UserState));
            }
        }
        
        public void CreateNewEmployeeAttendanceAsync(SIMS.EM.Model.MEmployeeAttendance _NewEmpAttendance) {
            this.CreateNewEmployeeAttendanceAsync(_NewEmpAttendance, null);
        }
        
        public void CreateNewEmployeeAttendanceAsync(SIMS.EM.Model.MEmployeeAttendance _NewEmpAttendance, object userState) {
            if ((this.onBeginCreateNewEmployeeAttendanceDelegate == null)) {
                this.onBeginCreateNewEmployeeAttendanceDelegate = new BeginOperationDelegate(this.OnBeginCreateNewEmployeeAttendance);
            }
            if ((this.onEndCreateNewEmployeeAttendanceDelegate == null)) {
                this.onEndCreateNewEmployeeAttendanceDelegate = new EndOperationDelegate(this.OnEndCreateNewEmployeeAttendance);
            }
            if ((this.onCreateNewEmployeeAttendanceCompletedDelegate == null)) {
                this.onCreateNewEmployeeAttendanceCompletedDelegate = new System.Threading.SendOrPostCallback(this.OnCreateNewEmployeeAttendanceCompleted);
            }
            base.InvokeAsync(this.onBeginCreateNewEmployeeAttendanceDelegate, new object[] {
                        _NewEmpAttendance}, this.onEndCreateNewEmployeeAttendanceDelegate, this.onCreateNewEmployeeAttendanceCompletedDelegate, userState);
        }
        
        private System.IAsyncResult OnBeginOpen(object[] inValues, System.AsyncCallback callback, object asyncState) {
            return ((System.ServiceModel.ICommunicationObject)(this)).BeginOpen(callback, asyncState);
        }
        
        private object[] OnEndOpen(System.IAsyncResult result) {
            ((System.ServiceModel.ICommunicationObject)(this)).EndOpen(result);
            return null;
        }
        
        private void OnOpenCompleted(object state) {
            if ((this.OpenCompleted != null)) {
                InvokeAsyncCompletedEventArgs e = ((InvokeAsyncCompletedEventArgs)(state));
                this.OpenCompleted(this, new System.ComponentModel.AsyncCompletedEventArgs(e.Error, e.Cancelled, e.UserState));
            }
        }
        
        public void OpenAsync() {
            this.OpenAsync(null);
        }
        
        public void OpenAsync(object userState) {
            if ((this.onBeginOpenDelegate == null)) {
                this.onBeginOpenDelegate = new BeginOperationDelegate(this.OnBeginOpen);
            }
            if ((this.onEndOpenDelegate == null)) {
                this.onEndOpenDelegate = new EndOperationDelegate(this.OnEndOpen);
            }
            if ((this.onOpenCompletedDelegate == null)) {
                this.onOpenCompletedDelegate = new System.Threading.SendOrPostCallback(this.OnOpenCompleted);
            }
            base.InvokeAsync(this.onBeginOpenDelegate, null, this.onEndOpenDelegate, this.onOpenCompletedDelegate, userState);
        }
        
        private System.IAsyncResult OnBeginClose(object[] inValues, System.AsyncCallback callback, object asyncState) {
            return ((System.ServiceModel.ICommunicationObject)(this)).BeginClose(callback, asyncState);
        }
        
        private object[] OnEndClose(System.IAsyncResult result) {
            ((System.ServiceModel.ICommunicationObject)(this)).EndClose(result);
            return null;
        }
        
        private void OnCloseCompleted(object state) {
            if ((this.CloseCompleted != null)) {
                InvokeAsyncCompletedEventArgs e = ((InvokeAsyncCompletedEventArgs)(state));
                this.CloseCompleted(this, new System.ComponentModel.AsyncCompletedEventArgs(e.Error, e.Cancelled, e.UserState));
            }
        }
        
        public void CloseAsync() {
            this.CloseAsync(null);
        }
        
        public void CloseAsync(object userState) {
            if ((this.onBeginCloseDelegate == null)) {
                this.onBeginCloseDelegate = new BeginOperationDelegate(this.OnBeginClose);
            }
            if ((this.onEndCloseDelegate == null)) {
                this.onEndCloseDelegate = new EndOperationDelegate(this.OnEndClose);
            }
            if ((this.onCloseCompletedDelegate == null)) {
                this.onCloseCompletedDelegate = new System.Threading.SendOrPostCallback(this.OnCloseCompleted);
            }
            base.InvokeAsync(this.onBeginCloseDelegate, null, this.onEndCloseDelegate, this.onCloseCompletedDelegate, userState);
        }
        
        protected override Contruction.Srv_Employee.IEmployeeService CreateChannel() {
            return new EmployeeServiceClientChannel(this);
        }
        
        private class EmployeeServiceClientChannel : ChannelBase<Contruction.Srv_Employee.IEmployeeService>, Contruction.Srv_Employee.IEmployeeService {
            
            public EmployeeServiceClientChannel(System.ServiceModel.ClientBase<Contruction.Srv_Employee.IEmployeeService> client) : 
                    base(client) {
            }
            
            public System.IAsyncResult BeginGetAllEmployees(string username, System.Nullable<int> pageindex, System.Nullable<int> pagesize, string Key, System.Nullable<int> siteid, System.Nullable<System.DateTime> AttDate, bool showall, System.AsyncCallback callback, object asyncState) {
                object[] _args = new object[7];
                _args[0] = username;
                _args[1] = pageindex;
                _args[2] = pagesize;
                _args[3] = Key;
                _args[4] = siteid;
                _args[5] = AttDate;
                _args[6] = showall;
                System.IAsyncResult _result = base.BeginInvoke("GetAllEmployees", _args, callback, asyncState);
                return _result;
            }
            
            public SIMS.Core.PaginatedData<SIMS.EM.Model.MEmployee> EndGetAllEmployees(System.IAsyncResult result) {
                object[] _args = new object[0];
                SIMS.Core.PaginatedData<SIMS.EM.Model.MEmployee> _result = ((SIMS.Core.PaginatedData<SIMS.EM.Model.MEmployee>)(base.EndInvoke("GetAllEmployees", _args, result)));
                return _result;
            }
            
            public System.IAsyncResult BeginGetEmployeeByID(int EmployeeID, System.AsyncCallback callback, object asyncState) {
                object[] _args = new object[1];
                _args[0] = EmployeeID;
                System.IAsyncResult _result = base.BeginInvoke("GetEmployeeByID", _args, callback, asyncState);
                return _result;
            }
            
            public SIMS.EM.Model.MEmployee EndGetEmployeeByID(System.IAsyncResult result) {
                object[] _args = new object[0];
                SIMS.EM.Model.MEmployee _result = ((SIMS.EM.Model.MEmployee)(base.EndInvoke("GetEmployeeByID", _args, result)));
                return _result;
            }
            
            public System.IAsyncResult BeginCreateNewEmployee(SIMS.EM.Model.MEmployee _NewEmployee, System.AsyncCallback callback, object asyncState) {
                object[] _args = new object[1];
                _args[0] = _NewEmployee;
                System.IAsyncResult _result = base.BeginInvoke("CreateNewEmployee", _args, callback, asyncState);
                return _result;
            }
            
            public SIMS.EM.Model.MEmployee EndCreateNewEmployee(System.IAsyncResult result) {
                object[] _args = new object[0];
                SIMS.EM.Model.MEmployee _result = ((SIMS.EM.Model.MEmployee)(base.EndInvoke("CreateNewEmployee", _args, result)));
                return _result;
            }
            
            public System.IAsyncResult BeginUpdateEmployee(SIMS.EM.Model.MEmployee _UpdatedEmployee, System.AsyncCallback callback, object asyncState) {
                object[] _args = new object[1];
                _args[0] = _UpdatedEmployee;
                System.IAsyncResult _result = base.BeginInvoke("UpdateEmployee", _args, callback, asyncState);
                return _result;
            }
            
            public SIMS.EM.Model.MEmployee EndUpdateEmployee(System.IAsyncResult result) {
                object[] _args = new object[0];
                SIMS.EM.Model.MEmployee _result = ((SIMS.EM.Model.MEmployee)(base.EndInvoke("UpdateEmployee", _args, result)));
                return _result;
            }
            
            public System.IAsyncResult BeginDeleteEmployee(int EmpID, System.AsyncCallback callback, object asyncState) {
                object[] _args = new object[1];
                _args[0] = EmpID;
                System.IAsyncResult _result = base.BeginInvoke("DeleteEmployee", _args, callback, asyncState);
                return _result;
            }
            
            public void EndDeleteEmployee(System.IAsyncResult result) {
                object[] _args = new object[0];
                base.EndInvoke("DeleteEmployee", _args, result);
            }
            
            public System.IAsyncResult BeginCreateNewEmployeeAttendance(SIMS.EM.Model.MEmployeeAttendance _NewEmpAttendance, System.AsyncCallback callback, object asyncState) {
                object[] _args = new object[1];
                _args[0] = _NewEmpAttendance;
                System.IAsyncResult _result = base.BeginInvoke("CreateNewEmployeeAttendance", _args, callback, asyncState);
                return _result;
            }
            
            public SIMS.EM.Model.MEmployeeAttendance EndCreateNewEmployeeAttendance(System.IAsyncResult result) {
                object[] _args = new object[0];
                SIMS.EM.Model.MEmployeeAttendance _result = ((SIMS.EM.Model.MEmployeeAttendance)(base.EndInvoke("CreateNewEmployeeAttendance", _args, result)));
                return _result;
            }
        }
    }
}
