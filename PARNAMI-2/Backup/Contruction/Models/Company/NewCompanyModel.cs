﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Contruction.MasterDBService;
using System.ComponentModel;
using SIMS.MM.MODEL;

namespace Contruction
{
    public class NewCompanyModel : ViewModel
    {
        MasterDataServiceClient service = new MasterDataServiceClient();

        public NewCompanyModel()
        {
            if (!DesignerProperties.GetIsInDesignMode(Application.Current.RootVisual))
            {
                OnInit();
            }
        }

        public void OnInit()
        {
            service.CreateNewCompanyCompleted += new EventHandler<CreateNewCompanyCompletedEventArgs>(service_CreateNewCompanyCompleted);
            service.GetCompanyByIDCompleted += new EventHandler<GetCompanyByIDCompletedEventArgs>(service_GetCompanyByIDCompleted);
            service.UpdateCompanyCompleted +=new EventHandler<UpdateCompanyCompletedEventArgs>(service_UpdateCompanyCompleted);
        }

        void service_UpdateCompanyCompleted(object sender, UpdateCompanyCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                Base.ShowError(e.Error);
                IsBusy = false;
            }
            else
            {
                NewCompany = e.Result;
                MessageBox.Show("Updation Successfully !");
                Base.Current.LoadCompanies();
                IsBusy = false;
            }
        }

        void service_GetCompanyByIDCompleted(object sender, GetCompanyByIDCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                Base.ShowError(e.Error);
                IsBusy = false;
            }
            else
            {
                NewCompany = e.Result;
                IsBusy = false;
            }
        }

        void service_CreateNewCompanyCompleted(object sender, CreateNewCompanyCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                Base.ShowError(e.Error);
                IsBusy = false;
            }
            else
            {
                NewCompany = e.Result;
                MessageBox.Show("New Company Added Successfully !");
                Base.Current.LoadCompanies();
                IsBusy = false;
            }
        }

        MCompany _NewCompany = new MCompany();
        public MCompany NewCompany
        {
            get { return _NewCompany; }
            set
            {
                _NewCompany = value;
                Notify("NewCompany");
            }
        }

        bool _IsNew = true;
        public bool IsNew
        {
            get { return _IsNew; }
            set
            {
                _IsNew = value;
                Notify("IsNew");
            }
        }

        public void SubmitToDB()
        {
            service.CreateNewCompanyAsync(NewCompany);
            IsBusy = true;
        }

        public void GetCompanybyID(int id)
        {
            service.GetCompanyByIDAsync(id);
            IsBusy = true;
        }

        public void UpdateCompany()
        {
            service.UpdateCompanyAsync(NewCompany);
            IsBusy = true;
        }
    }
}
