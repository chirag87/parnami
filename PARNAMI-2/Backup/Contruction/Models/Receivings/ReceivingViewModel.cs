﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using SIMS.MM.MODEL;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using SIMS.Core;
using Contruction.TransactionService;

namespace Contruction
{
    public class ReceivingViewModel : ViewModel
    {
        //ReceivingServiceClient service = new ReceivingServiceClient();
        TransactionServiceClient newservice = new TransactionServiceClient();

        public ReceivingViewModel()
        {
            if (!DesignerProperties.GetIsInDesignMode(Application.Current.RootVisual))
            {
                OnInit();
            }
        }

        public void OnInit()
        {
            //service.GetAllReceivingsCompleted += new EventHandler<GetAllReceivingsCompletedEventArgs>(service_GetAllReceivingsCompleted);
            newservice.GetAllGRNsCompleted += new EventHandler<GetAllGRNsCompletedEventArgs>(newservice_GetAllGRNsCompleted);
            LoadGRNs();
            Base.AppEvents.ReceivingDataChanged += new EventHandler(AppEvents_ReceivingDataChanged);
        }

        void newservice_GetAllGRNsCompleted(object sender, GetAllGRNsCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                Base.ShowError(e.Error);
                IsBusy = false;
            }
            else
            {
                GRNs = e.Result;
                IsBusy = false;
            }
        }

        public event EventHandler GRNChanged;
        public void RaiseGRNChanged()
        {
            if (GRNChanged != null)
                GRNChanged(this, new EventArgs());
        }

        void AppEvents_ReceivingDataChanged(object sender, EventArgs e)
        {
            LoadGRNs();
        }

        int _PageIndex = 0;
        public int PageIndex
        {
            get { return _PageIndex; }
            set
            {
                _PageIndex = value;
                Notify("PageIndex");
                LoadGRNs();
            }
        }

        //void service_GetAllReceivingsCompleted(object sender, GetAllReceivingsCompletedEventArgs e)
        //{
        //    if (e.Error != null)
        //    {
        //        Base.ShowError(e.Error);
        //        IsBusy = false;
        //    }
        //    else
        //    {
        //        GRNs = e.Result;
        //        IsBusy = false;
        //    }
        //}

        //IPaginated<MReceivingHeader> _GRNs;
        //public IPaginated<MReceivingHeader> GRNs
        //{
        //    get { return _GRNs; }
        //    set
        //    {
        //        _GRNs = value;
        //        Notify("GRNs");
        //        RaiseGRNChanged();
        //    }
        //}

        IPaginated<MGRN> _GRNs;
        public IPaginated<MGRN> GRNs
        {
            get { return _GRNs; }
            set
            {
                _GRNs = value;
                Notify("GRNs");
                RaiseGRNChanged();
            }
        }

        string _SearchKey;
        public string SearchKey
        {
            get { return _SearchKey; }
            set
            {
                _SearchKey = value;
                Notify("SearchKey");
            }
        }

        int? _VendorID;
        public int? VendorID
        {
            get { return _VendorID; }
            set
            {
                _VendorID = value;
                Notify("VendorID");
            }
        }

        int? _SiteID;
        public int? SiteID
        {
            get { return _SiteID; }
            set
            {
                _SiteID = value;
                Notify("SiteID");
            }
        }

        string _RecType;
        public string RecType
        {
            get { return _RecType; }
            set
            {
                _RecType = value;
                Notify("RecType");
            }
        }

        public void Clear()
        {
            SearchKey = null;
            VendorID = null;
            SiteID = null;
            LoadGRNs();
        }

        public void LoadGRNs()
        {
            //service.GetAllReceivingsAsync(Base.Current.UserName, PageIndex, null, SearchKey, VendorID, SiteID);
            newservice.GetAllGRNsAsync(Base.Current.UserName, PageIndex, null, SearchKey, VendorID, SiteID);
            IsBusy = true;
        }

        internal void MoveToPage(int p)
        {
            PageIndex = p;
        }
    }
}
