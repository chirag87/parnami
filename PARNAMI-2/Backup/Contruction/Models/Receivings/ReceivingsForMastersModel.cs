﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.ComponentModel;
using Contruction.MasterDBService;
using SIMS.MM.MODEL;
using System.Collections.ObjectModel;

namespace Contruction
{
    public class ReceivingsForMastersModel : ViewModel
    {
        MasterDataServiceClient service = new MasterDataServiceClient();

        public ReceivingsForMastersModel()
        {
            if (!DesignerProperties.GetIsInDesignMode(Application.Current.RootVisual))
            {
                OnInit();
            }
        }

        public void OnInit()
        {
            service.GetAllReceivingsforMastersCompleted += new EventHandler<GetAllReceivingsforMastersCompletedEventArgs>(service_GetAllReceivingsforMastersCompleted);
        }

        void service_GetAllReceivingsforMastersCompleted(object sender, GetAllReceivingsforMastersCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                Base.ShowError(e.Error);
                IsBusy = false;
            }
            else
            {
                AllReceivings = e.Result;
                IsBusy = false;
            }
        }

        int? _VendorID;
        public int? VendorID
        {
            get { return _VendorID; }
            set
            {
                _VendorID = value;
                Load();
                Notify("VendorID");
            }
        }

        int? _SiteID;
        public int? SiteID
        {
            get { return _SiteID; }
            set
            {
                _SiteID = value;
                Load();
                Notify("SiteID");
            }
        }

        ObservableCollection<MReceivingDetailsforMasters> _AllReceivings = new ObservableCollection<MReceivingDetailsforMasters>();
        public ObservableCollection<MReceivingDetailsforMasters> AllReceivings
        {
            get { return _AllReceivings; }
            set
            {
                _AllReceivings = value;
                Notify("AllReceivings");
            }
        }

        public void Load()
        {
            service.GetAllReceivingsforMastersAsync(VendorID, SiteID);
            IsBusy = true;
        }
    }
}
