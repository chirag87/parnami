﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Collections.ObjectModel;
using SIMS.MM.MODEL;
using System.ComponentModel;
using Contruction.TransactionService;

namespace Contruction
{
    public class ViewConsumptionDetailsModel : ViewModel
    {
        TransactionServiceClient newservice = new TransactionServiceClient();

        public ViewConsumptionDetailsModel()
        {
            if (!DesignerProperties.GetIsInDesignMode(Application.Current.RootVisual))
            {
                OnInit();
            }
        }

        public void OnInit()
        {
            //service.GetConsumptionByIDCompleted +=new EventHandler<GetConsumptionByIDCompletedEventArgs>(service_GetConsumptionByIDCompleted);
            newservice.GetConsumptionByIDCompleted += new EventHandler<TransactionService.GetConsumptionByIDCompletedEventArgs>(newservice_GetConsumptionByIDCompleted);
        }

        void newservice_GetConsumptionByIDCompleted(object sender, TransactionService.GetConsumptionByIDCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                Base.ShowError(e.Error);
                IsBusy = false;
            }
            else
            {
                NewConsumption = e.Result;
                IsBusy = false;
                IsOld = true;
            }
        }

        //void service_GetConsumptionByIDCompleted(object sender, GetConsumptionByIDCompletedEventArgs e)
        //{
        //    if (e.Error != null)
        //    {
        //        Base.ShowError(e.Error);
        //        IsBusy = false;
        //    }
        //    else
        //    {
        //        NewConsumption = e.Result;
        //        IsBusy = false;
        //        IsOld = true;
        //    }
        //}

        //MReceiving _NewConsumption;
        //public MReceiving NewConsumption
        //{
        //    get { return _NewConsumption; }
        //    set
        //    {
        //        _NewConsumption = value;
        //        Notify("NewConsumption");
        //    }
        //}

        MConsumption _NewConsumption = new MConsumption();
        public MConsumption NewConsumption
        {
            get { return _NewConsumption; }
            set
            {
                _NewConsumption = value;
                Notify("NewConsumption");
            }
        }

        void Current_ConsumptionRequested(object sender, ConsumptionRequestedArgs e)
        {
            int receivingid = e.ConsumptionID;
            SetID(receivingid);
        }

        bool _IsOld = false;
        public bool IsOld
        {
            get { return _IsOld; }
            set
            {
                _IsOld = value;
                Notify("IsOld");
                Notify("CanEdit");
            }
        }

        public override bool CanEdit
        {
            get
            {
                return !IsOld;
            }
        }

        public bool CanPrint
        {
            get
            {
                return true;
            }
        }

        internal void SetID(int p)
        {
            //service.GetConsumptionByIDAsync(p);
            newservice.GetConsumptionByIDAsync(p);
            IsBusy = true;
        }
    }
}
