﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Contruction.MasterReportingService;
using System.ComponentModel;
using System.Collections.ObjectModel;
using SIMS.MM.MODEL;
using System.Collections.Generic;

namespace Contruction
{
    public class MIReportDetailsVM : ViewModel
    {
        MasterReportingServiceClient service = new MasterReportingServiceClient();

        public MIReportDetailsVM()
        {
            if (!DesignerProperties.GetIsInDesignMode(Application.Current.RootVisual))
            {
                OnInit();
            }
        }

        #region Events...

        #endregion

        #region CompletedEvents...

        void service_GetMIReportSummaryDetailsCompleted(object sender, GetMIReportSummaryDetailsCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                Base.ShowError(e.Error);
                IsBusy = false;
            }
            else
            {
                MIReportSummaryLines = e.Result;
                IsBusy = false;
            }
        }

        #endregion

        #region Properties...

        ObservableCollection<MMIReportSummaryLine> _MIReportSummaryLines = new ObservableCollection<MMIReportSummaryLine>();
        public ObservableCollection<MMIReportSummaryLine> MIReportSummaryLines
        {
            get { return _MIReportSummaryLines; }
            set
            {
                _MIReportSummaryLines = value;
                Notify("MIReportSummaryLines");
            }
        }

        DateTime? _SDate = DateTime.UtcNow.AddHours(5.5).AddMonths(-1);
        public DateTime? SDate
        {
            get { return _SDate; }
            set
            {
                _SDate = value;
                Notify("SDate");
            }
        }

        DateTime? _EDate = DateTime.UtcNow.AddHours(5.5);
        public DateTime? EDate
        {
            get { return _EDate; }
            set
            {
                _EDate = value;
                Notify("EDate");
            }
        }

        int? _ItemCatID;
        public int? ItemCatID
        {
            get { return _ItemCatID; }
            set
            {
                _ItemCatID = value;
                Notify("ItemCatID");
            }
        }

        int? _ItemID;
        public int? ItemID
        {
            get { return _ItemID; }
            set
            {
                _ItemID = value;
                Notify("ItemID");
            }
        }

        int? _SizeID;
        public int? SizeID
        {
            get { return _SizeID; }
            set
            {
                _SizeID = value;
                Notify("SizeID");
            }
        }

        int? _BrandID;
        public int? BrandID
        {
            get { return _BrandID; }
            set
            {
                _BrandID = value;
                Notify("BrandID");
            }
        }

        int? _SiteID;
        public int? SiteID
        {
            get { return _SiteID; }
            set
            {
                _SiteID = value;
                Notify("SiteID");
            }
        }

        string _Status;
        public string Status
        {
            get { return _Status; }
            set
            {
                _Status = value;
                Notify("Status");
            }
        }

        string _ConsumableType;
        public string ConsumableType
        {
            get { return _ConsumableType; }
            set
            {
                _ConsumableType = value;
                Notify("ConsumableType");
            }
        }

        string _RType;
        public string RType
        {
            get { return _RType; }
            set
            {
                _RType = value;
                Notify("RType");
            }
        }

        IEnumerable<string> _StatusList = new List<string>();
        public IEnumerable<string> StatusList
        {
            get { return _StatusList; }
            set
            {
                _StatusList = value;
                Notify("StatusList");
            }
        }

        IEnumerable<string> _RTypes = new List<string>();
        public IEnumerable<string> RTypes
        {
            get { return _RTypes; }
            set
            {
                _RTypes = value;
                Notify("RTypes");
            }
        }

        #endregion

        #region Methods...

        public void OnInit()
        {
            service.GetMIReportSummaryDetailsCompleted += new EventHandler<GetMIReportSummaryDetailsCompletedEventArgs>(service_GetMIReportSummaryDetailsCompleted);
            StatusList = (new string[] { "Draft", "SubmittedForVerification", "MIVerified", "SubmittedForMIApproval", "MIApproved", "SubmittedToPurchase", "SubmittedToHOPurchase", "SubmittedForPOApproval", "POApproved", "Closed", "MIRejected", "PORejected", "All" });
            RTypes = (new string[] { "Excel", "PDF" });
        }

        public void Load()
        {
            if (Status == " " || Status == "All") Status = null;
            service.GetMIReportSummaryDetailsAsync(SDate, EDate, ItemCatID, ItemID, SizeID, BrandID, SiteID, ConsumableType, Status);
            IsBusy = true;
        }

        public void Load(int? itemid, int? sizeid, int? brandid, DateTime? sdate, DateTime? edate, int? siteid, string contype, string status)
        {
            if (Status == " " || Status == "All") Status = null;
            service.GetMIReportSummaryDetailsAsync(sdate, edate, ItemCatID, itemid, sizeid, brandid, siteid, contype, status);
            IsBusy = true;
        }

        #endregion
    }
}
