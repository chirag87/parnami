﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.ComponentModel;

namespace Contruction
{
    public class ViewModel : INotifyPropertyChanged
    {
        public ViewModel()
        {
            Init();
        }

        ControlStates _ControlState = ControlStates.Add;

        public virtual void OnStateChanging(ControlStates value) { }
        public virtual void OnStateChanged() { }
        public virtual void Init() { }

        public ControlStates ControlState
        {
            get { return _ControlState; }
            set
            {
                if (ControlState != value)
                {
                    OnStateChanging(value);
                    _ControlState = value;
                    Notify("ControlState");
                    Notify("IsInsertMode");
                    Notify("IsEditMode");
                    Notify("IsDetailedMode");
                    Notify("IsReadOnly");
                    OnStateChanged();
                }
            }
        }

        public bool IsInsertMode
        {
            get { return ControlState == ControlStates.Add; }
        }

        public bool IsEditMode
        {
            get { return ControlState == ControlStates.Edit; }
        }

        public bool IsDetailedMode
        {
            get { return ControlState == ControlStates.Detail; }
        }

        public bool IsReadOnly
        {
            get { return !CanEdit; }
        }

        public virtual bool CanEdit
        {
            get { return ControlState == ControlStates.Add || ControlState == ControlStates.Edit; }
        }

        public bool IsDesignTime
        {
            get
            {
                return false;
            }
        }

        public bool IsRealTime
        {
            get { return !IsDesignTime; }
        }

        bool _IsBusy = false;
        public bool IsBusy
        {
            get { return _IsBusy; }
            set
            {
                _IsBusy = value;
                Notify("IsBusy");
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void Notify(string propertyName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
