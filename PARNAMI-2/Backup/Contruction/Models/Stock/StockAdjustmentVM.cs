﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.ComponentModel;
using System.Linq;
using SIMS.MM.MODEL;
using System.Collections.Generic;
using SIMS.MM.MODEL;
using Contruction.MasterDBService;
using System.Collections.ObjectModel;
namespace Contruction.Models.Stock
{
    public class StockAdjustmentVM:ViewModel
    {
        //ReceivingServiceClient proxy = new ReceivingServiceClient();
        MasterDataServiceClient service = new MasterDataServiceClient();

        public event EventHandler SAAdded;
        public void RaiseSAAdded()
        {
            if (SAAdded != null)
                SAAdded(this, new EventArgs());
        }

        public StockAdjustmentVM()
        {
            if (!DesignerProperties.GetIsInDesignMode(Application.Current.RootVisual))
            {
                OnInit();
            }
        }

        private void OnInit()
        {
            //proxy.CreateNewSACompleted += new EventHandler<CreateNewSACompletedEventArgs>(proxy_CreateNewSACompleted);
            service.GetStockReportCompleted += new EventHandler<GetStockReportCompletedEventArgs>(service_GetStockReportCompleted);
            NewTransaction.SiteIDChanged += new EventHandler(NewTransaction_SiteIDChanged);
        }

        void service_GetStockReportCompleted(object sender, GetStockReportCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                Base.ShowError(e.Error);
                IsBusy = false;
            }
            else
            {
                StockSummary = e.Result;
                IsBusy = false;
            }
        }

        void NewTransaction_SiteIDChanged(object sender, EventArgs e)
        {
            service.GetStockReportAsync(null, null, NewTransaction.SiteID, null, null, null);
        }

        //void proxy_CreateNewSACompleted(object sender, CreateNewSACompletedEventArgs e)
        //{
        //    if (e.Error != null)
        //    {
        //        Base.ShowError(e.Error);
        //        IsBusy = false;
        //    }
        //    else
        //    {
        //        IsBusy = false;
        //        MessageBox.Show("Changes Saved Successfully");
        //        service.GetStockReportAsync(null, null, NewTransaction.SiteID, null, null, null);
        //        Clear();
        //    }
        //}

        MReceiving _NewTransaction=new MReceiving();
        public MReceiving NewTransaction
        {
            get { return _NewTransaction; }
            set
            {
                _NewTransaction = value;
                Notify("NewTransaction");
            }
        }

        string _Status;
        public string Status
        {
            get { return _Status; }
            set
            {
                _Status = value;
                Notify("Status");
            }
        }

        IEnumerable<MStockReport> _StockSummary;
        public IEnumerable<MStockReport> StockSummary
        {
            get { return _StockSummary; }
            set
            {
                _StockSummary = value;
                Notify("StockSummary");
            }
        }

        public void AddNewLine()
        {
            var line=NewTransaction.AddNewLine();
            line.ItemIDChanged += (s, e) =>
                {
                    line.CurrentStock = new double();
                    var count = StockSummary.Where(x => x.ItemID == line.ItemID).Select(x => x.OpeningBalance);
                    if (count.Count() == 0) line.CurrentStock = 0;
                    else
                    {
                        var stock=StockSummary.Single(x => x.ItemID == line.ItemID).OpeningBalance.Value;
                        line.CurrentStock = stock;
                    }
                };
            Notify("NewTransaction");
        }

        public void DeleteLine(int lid)
        {
            try
            {
                var line = NewTransaction.ReceivingLines.Single(x => x.LineID == lid);
                NewTransaction.ReceivingLines.Remove(line);
            }
            catch { }            
        }

        public void SubmitToDB()
        {
            if (!Validate()) return;
            //proxy.CreateNewSAAsync(NewTransaction, Base.Current.UserName);
            IsBusy = true;
        }

        public void Clear()
        {
            SelectedSiteID = null;
            NewTransaction.ReceivingLines = new ObservableCollection<MReceivingLine>();
            Notify("NewTransaction");
            RaiseSAAdded();
        }

        public int? _SelectedSiteID = new int();
        public int? SelectedSiteID
        {
            get { return _SelectedSiteID; }
            set
            {
                _SelectedSiteID = value;
                if(value!=null)NewTransaction.SiteID = value.Value;
                Notify("NewTransaction");
                Notify("SelectedSiteID");
            }
        }

        private bool Validate()
        {
            Status = "";
            if (NewTransaction.SiteID == 0)
            {
                Status = "Please Select Site"; 
                return false; 
            }

            if (NewTransaction.ReceivingLines == null) 
            { 
                Status = "Stock Adjustment should have alteast one item.";
                return false;
            }

            if (NewTransaction.ReceivingLines.Count == 0) 
            {
                Status = "Stock Adjustment should have alteast one item."; 
                return false; 
            }

            if (NewTransaction.ReceivingLines.Any(x => x.ItemID == 0)) 
            {
                Status = "Stock Adjustment Lines has some invaid/unselected Items!"; 
                return false; 
            }
            return true;
        }
    }
}
