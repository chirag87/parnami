﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.ComponentModel;
using Contruction.TestService;
using System.Collections.Generic;
using SIMS.MM.MODEL;

namespace Contruction
{
    public class TestItemViewModel : ViewModel
    {
        TestServiceClient service = new TestServiceClient();

        public TestItemViewModel()
        {
            if (!DesignerProperties.GetIsInDesignMode(Application.Current.RootVisual))
            {
                OnInit();
            }
        }

        void service_GetAllMItemsCompleted(object sender, GetAllMItemsCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                Base.ShowError(e.Error);
            }
            else
            {
                AllItems = e.Result;
            }
        }

        IEnumerable<MItem> _AllItems;
        public IEnumerable<MItem> AllItems
        {
            get { return _AllItems; }
            set
            {
                _AllItems = value;
                Notify("AllItems");
            }
        }

        public void OnInit()
        {
            service.GetAllMItemsCompleted += new EventHandler<GetAllMItemsCompletedEventArgs>(service_GetAllMItemsCompleted);
            GetAllItems();
        }

        public void GetAllItems()
        {
            service.GetAllMItemsAsync();
        }
    }
}
