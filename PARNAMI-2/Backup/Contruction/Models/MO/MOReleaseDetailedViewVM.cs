﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Contruction.MOService;
using System.ComponentModel;
using SIMS.MM.MODEL;

namespace Contruction
{
    public class MOReleaseDetailedViewVM : ViewModel
    {
        MOServiceClient service = new MOServiceClient();

        public MOReleaseDetailedViewVM()
        {
            if (!DesignerProperties.GetIsInDesignMode(Application.Current.RootVisual))
            {
                OnInit();
            }
        }

        #region Methods...

        public void OnInit()
        {
            service.GetUniqueMOReleasingByIDCompleted += new EventHandler<GetUniqueMOReleasingByIDCompletedEventArgs>(service_GetUniqueMOReleasingByIDCompleted);
        }

        public void GetMOReleaseByID(int MOID, int RELID)
        {
            service.GetUniqueMOReleasingByIDAsync(MOID, RELID);
            IsBusy = true;
        }

        #endregion

        #region Completed Events...

        void service_GetUniqueMOReleasingByIDCompleted(object sender, GetUniqueMOReleasingByIDCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                Base.ShowError(e.Error);
                IsBusy = false;
            }
            else
            {
                SelectedRelease = e.Result;
                IsBusy = false;
            }
        }

        #endregion

        #region Properties...

        MMOReleasing _SelectedRelease = new MMOReleasing();
        public MMOReleasing SelectedRelease
        {
            get { return _SelectedRelease; }
            set
            {
                _SelectedRelease = value;
                Notify("SelectedRelease");
            }
        }

        #endregion
    }
}
