﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.ComponentModel;
using Contruction.MOService;
using SIMS.MM.MODEL;
using SIMS.Core;
using System.Collections.ObjectModel;
using System.Collections.Generic;

namespace Contruction
{
    public class MOReleaseIndexVM : ViewModel
    {
        MOServiceClient service = new MOServiceClient();

        public MOReleaseIndexVM()
        {
            if (!DesignerProperties.GetIsInDesignMode(Application.Current.RootVisual))
            {
                OnInit();
            }
        }

        public event EventHandler MOReleaseChanged;
        public void RaiseMOReleaseChanged()
        {
            if (MOReleaseChanged != null)
                MOReleaseChanged(this, new EventArgs());
        }

        public void OnInit()
        {
            service.GetMOReleaseHeadersCompleted += new EventHandler<GetMOReleaseHeadersCompletedEventArgs>(service_GetMOReleaseHeadersCompleted);
            LoadData();
            Base.AppEvents.MODataChanged += new EventHandler(AppEvents_MODataChanged);
            StatusTypes.AddRange(new string[] { "Pending", "Closed", "All" });
            ConsumableTypes.AddRange(new string[] { "Consumable", "Returnable", "All" });
        }

        void AppEvents_MODataChanged(object sender, EventArgs e)
        {
            LoadData();
        }

        void service_GetMOReleaseHeadersCompleted(object sender, GetMOReleaseHeadersCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                Base.ShowError(e.Error);
                IsBusy = false;
            }
            else
            {
                MOReleases = e.Result;
                IsBusy = false;
            }
        }

        #region Properties...

        IPaginated<MMOReleaseHeader> _MOReleases = new PaginatedData<MMOReleaseHeader>();
        public IPaginated<MMOReleaseHeader> MOReleases
        {
            get { return _MOReleases; }
            set
            {
                _MOReleases = value;
                Notify("MOReleases");
                RaiseMOReleaseChanged();
            }
        }

        public bool CanViewTOs { get { return Base.Current.Rights.TO_CanViewAll && !Base.Current.Rights.TO_CanRelease && !Base.Current.Rights.TO_CanReceive; } }

        public bool CanReceiveTO { get { return Base.Current.Rights.TO_CanReceive && !Base.Current.Rights.TO_CanRelease; } }

        public bool CanReleaseandReceive { get { return Base.Current.Rights.TO_CanRelease && Base.Current.Rights.TO_CanReceive; } }

        int _PageIndex = 0;
        public int PageIndex
        {
            get { return _PageIndex; }
            set
            {
                _PageIndex = value;
                Notify("PageIndex");
                LoadData();
            }
        }

        string _SearchKey;
        public string SearchKey
        {
            get { return _SearchKey; }
            set
            {
                _SearchKey = value;
                Notify("SearchKey");
            }
        }

        int? _ReqSiteID;
        public int? ReqSiteID
        {
            get { return _ReqSiteID; }
            set
            {
                _ReqSiteID = value;
                Notify("ReqSiteID");
            }
        }

        int? _TransSiteID;
        public int? TransSiteID
        {
            get { return _TransSiteID; }
            set
            {
                _TransSiteID = value;
                Notify("TransSiteID");
            }
        }

        string _ConsumableType;
        public string ConsumableType
        {
            get { return _ConsumableType; }
            set
            {
                _ConsumableType = value;
                Notify("ConsumableType");
            }
        }

        List<string> _StatusTypes = new List<string>();
        public List<string> StatusTypes
        {
            get { return _StatusTypes; }
            set
            {
                _StatusTypes = value;
                Notify("StatusTypes");
            }
        }

        List<string> _ConsumableTypes = new List<string>();
        public List<string> ConsumableTypes
        {
            get { return _ConsumableTypes; }
            set
            {
                _ConsumableTypes = value;
                Notify("ConsumableTypes");
            }
        }

        string _Status;
        public string Status
        {
            get { return _Status; }
            set
            {
                _Status = value;
                Notify("Status");
            }
        }

        #endregion

        #region Methods...

        public void Clear()
        {
            SearchKey = null;
            ReqSiteID = null;
            TransSiteID = null;
            LoadData();
        }

        public void LoadData()
        {
            if (ConsumableType == "All") ConsumableType = null;
            service.GetMOReleaseHeadersAsync(Base.Current.UserName, PageIndex, null, SearchKey, TransSiteID, ReqSiteID, ConsumableType);
            IsBusy = true;
        }

        internal void MoveToPage(int p)
        {
            PageIndex = p;
        }

        #endregion
    }
}
