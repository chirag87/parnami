﻿using System;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using SIMS.MM.MODEL;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Contruction.MasterDBService;
using System.ComponentModel;

namespace Contruction
{
    public class NewAddressWindowVM : ViewModel
    {
        MasterDataServiceClient service = new MasterDataServiceClient();

        //public override void Init()
        //{
        //    base.Init();
        //    //For = "Vendor";
        //    //ForID = 0;
        //}

        public NewAddressWindowVM()
        {
            if (!DesignerProperties.GetIsInDesignMode(Application.Current.RootVisual))
            {
                OnInit();
            }
        }

        public void OnInit()
        {
            service.GetAddressbyForIDCompleted += new EventHandler<GetAddressbyForIDCompletedEventArgs>(service_GetAddressbyForIDCompleted);
            service.UpdateAddressCompleted += new EventHandler<UpdateAddressCompletedEventArgs>(service_UpdateAddressCompleted);
            service.CreateNewAddressCompleted += new EventHandler<CreateNewAddressCompletedEventArgs>(service_CreateNewAddressCompleted);
        }

        void service_CreateNewAddressCompleted(object sender, CreateNewAddressCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                Base.ShowError(e.Error);
                IsBusy = false;
            }
            else
            {
                MessageBox.Show("New Address Created Successful !");
                IsBusy = false;
                Load();
                Notify("SelectedAddress");
                Notify("AllAddressess");
                Base.Current.RaiseVendorDetailUpdated();
            }
        }

        void service_UpdateAddressCompleted(object sender, UpdateAddressCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                Base.ShowError(e.Error);
                IsBusy = false;
            }
            else
            {
                SelectedAddress = e.Result;
                MessageBox.Show("Updation Successful !");
                IsBusy = false;
                Load();
                Notify("SelectedAddress");
                Base.Current.RaiseVendorDetailUpdated();
            }
        }

        void service_GetAddressbyForIDCompleted(object sender, GetAddressbyForIDCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                Base.ShowError(e.Error);
                IsBusy = false;
            }
            else
            {
                AllAddressess = e.Result;
                IsBusy = false;
                Notify("AllAddressess");
            }
        }

        #region Models

        ObservableCollection<MAddress> _AllAddressess = new ObservableCollection<MAddress>();
        public ObservableCollection<MAddress> AllAddressess
        {
            get { return _AllAddressess; }
            set
            {
                _AllAddressess = value;
                Notify("AllAddressess");
                if (value.Count > 0)
                {
                    SelectedAddress = value.First();
                }
            }
        }

        MAddress _SelectedAddress = new MAddress();
        public MAddress SelectedAddress
        {
            get { return _SelectedAddress; }
            set
            {
                _SelectedAddress = value;
                Notify("SelectedAddress");
            }
        }

        #endregion

        string _For;
        public string For
        {
            get { return _For; }
            set
            {
                _For = value;
                Load();
                Notify("For");
            }
        }

        int _ForID;
        public int ForID
        {
            get { return _ForID; }
            set
            {
                _ForID = value;
                Load();
                Notify("ForID");
            }
        }

        public void Load()
        {
            AllAddressess.Clear();
            if (String.IsNullOrWhiteSpace(For)) return;
            if (ForID == 0) return;
            service.GetAddressbyForIDAsync(ForID, For);
            IsBusy = true;
        }

        public void UpdateAddress()
        {
            service.UpdateAddressAsync(SelectedAddress);
            IsBusy = true;
        }

        public void CreateNewAddress()
        {
            service.CreateNewAddressAsync(SelectedAddress);
            IsBusy = true;
        }
    }
}
