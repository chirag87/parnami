﻿#pragma checksum "D:\Softwares\Office Works\PARNAMI\PARNAMI-2\Backup\Contruction\Views\Purchasing\PRView.xaml" "{406ea660-64cf-4c82-b6f0-42d48172a799}" "9D5BDE44F3F68A2A4889CF3DBF058924"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.18408
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using Contruction;
using SyedMehrozAlam.CustomControls;
using System;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Automation.Peers;
using System.Windows.Automation.Provider;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Interop;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Resources;
using System.Windows.Shapes;
using System.Windows.Threading;


namespace Contruction {
    
    
    public partial class PRView : System.Windows.Controls.UserControl {
        
        internal System.Windows.VisualStateGroup PurchasingDetailesView;
        
        internal System.Windows.VisualState Show;
        
        internal System.Windows.VisualState Hide;
        
        internal System.Windows.Controls.Grid LayoutRoot;
        
        internal System.Windows.Controls.TextBlock status;
        
        internal System.Windows.Controls.Button btnCloseMI;
        
        internal System.Windows.Controls.TextBlock RefNo;
        
        internal System.Windows.Controls.Grid PR_IDs;
        
        internal System.Windows.Controls.TextBlock ID;
        
        internal System.Windows.Controls.TextBlock raisedOn;
        
        internal System.Windows.Controls.TextBlock raisedBy;
        
        internal System.Windows.Controls.Grid Approval_Status;
        
        internal System.Windows.Controls.TextBlock verStatus;
        
        internal System.Windows.Controls.TextBlock verBy;
        
        internal System.Windows.Controls.TextBlock verOn;
        
        internal System.Windows.Controls.TextBlock initialApprovalStatus;
        
        internal System.Windows.Controls.TextBlock IAppBy;
        
        internal System.Windows.Controls.TextBlock IAppOn;
        
        internal System.Windows.Controls.TextBlock finalApprovalStatus;
        
        internal System.Windows.Controls.TextBlock FAppBy;
        
        internal System.Windows.Controls.TextBlock FAppOn;
        
        internal System.Windows.Controls.Grid Vendor_Details;
        
        internal SyedMehrozAlam.CustomControls.AutoCompleteComboBox VendorCombo;
        
        internal System.Windows.Controls.Grid BU_Details;
        
        internal System.Windows.Controls.HyperlinkButton hpbtnAddLine;
        
        internal System.Windows.Controls.DataGrid dataGrid;
        
        internal System.Windows.Controls.DataGrid dataGrid_AfterVerify;
        
        internal System.Windows.Controls.DataGrid dataGrid_AfterMIApprove;
        
        internal System.Windows.Controls.DataGrid dataGrid_AfterPOApproval;
        
        internal System.Windows.Controls.Grid SP_PR_Remarks;
        
        internal System.Windows.Controls.TextBox tbxRemarks;
        
        internal System.Windows.Controls.TextBox tbxApprovalRemarks;
        
        internal System.Windows.Controls.Grid Bottom;
        
        internal System.Windows.Controls.StackPanel sp_Vat;
        
        internal System.Windows.Controls.TextBlock lbl_vat;
        
        internal System.Windows.Controls.TextBox txt_Vat;
        
        internal System.Windows.Controls.TextBlock lbl_vat_Copy;
        
        internal System.Windows.Controls.StackPanel sp_cst;
        
        internal System.Windows.Controls.TextBlock lbl_vat2;
        
        internal System.Windows.Controls.TextBox txt_CST;
        
        internal System.Windows.Controls.TextBlock lbl_vat2_Copy;
        
        internal System.Windows.Controls.StackPanel sp_TransPortation;
        
        internal System.Windows.Controls.TextBlock lbl_Transportation;
        
        internal System.Windows.Controls.TextBox txt_Transportation;
        
        internal System.Windows.Controls.StackPanel sp_Payment;
        
        internal System.Windows.Controls.TextBlock lbl_Payment;
        
        internal System.Windows.Controls.TextBlock txt_Total;
        
        internal System.Windows.Controls.StackPanel stackPanel;
        
        internal System.Windows.Controls.TextBox txt_Payment;
        
        internal System.Windows.Controls.TextBlock lbl_comment;
        
        internal System.Windows.Controls.TextBlock lbl_ContactPersonPh_Copy;
        
        internal System.Windows.Controls.TextBox txt_ContactPersonName;
        
        internal System.Windows.Controls.TextBlock lbl_ContactPersonPh;
        
        internal System.Windows.Controls.TextBox txt_ContactPersonPh;
        
        internal System.Windows.Controls.TextBlock lbl_Comment2;
        
        internal System.Windows.Controls.StackPanel PR_Btn_Panel;
        
        internal System.Windows.Controls.Button btnSave;
        
        internal System.Windows.Controls.Button btnEmail;
        
        internal System.Windows.Controls.Button ConvertToPO;
        
        internal System.Windows.Controls.Button btnSubmitToVerify;
        
        internal System.Windows.Controls.Button btnVerify;
        
        internal System.Windows.Controls.Button btnSubmitToMIApproval;
        
        internal System.Windows.Controls.Button btnInitialApprove;
        
        internal System.Windows.Controls.Button btnSubmitToPurchase;
        
        internal System.Windows.Controls.Button btnSubmitToHOPurchase;
        
        internal System.Windows.Controls.Button btnSubmitToPOApproval;
        
        internal System.Windows.Controls.Button btnFinalApprove;
        
        internal System.Windows.Controls.Button btnToPR;
        
        internal System.Windows.Controls.Button btnToMO;
        
        internal Contruction.VendorPrintLink download;
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Windows.Application.LoadComponent(this, new System.Uri("/Contruction;component/Views/Purchasing/PRView.xaml", System.UriKind.Relative));
            this.PurchasingDetailesView = ((System.Windows.VisualStateGroup)(this.FindName("PurchasingDetailesView")));
            this.Show = ((System.Windows.VisualState)(this.FindName("Show")));
            this.Hide = ((System.Windows.VisualState)(this.FindName("Hide")));
            this.LayoutRoot = ((System.Windows.Controls.Grid)(this.FindName("LayoutRoot")));
            this.status = ((System.Windows.Controls.TextBlock)(this.FindName("status")));
            this.btnCloseMI = ((System.Windows.Controls.Button)(this.FindName("btnCloseMI")));
            this.RefNo = ((System.Windows.Controls.TextBlock)(this.FindName("RefNo")));
            this.PR_IDs = ((System.Windows.Controls.Grid)(this.FindName("PR_IDs")));
            this.ID = ((System.Windows.Controls.TextBlock)(this.FindName("ID")));
            this.raisedOn = ((System.Windows.Controls.TextBlock)(this.FindName("raisedOn")));
            this.raisedBy = ((System.Windows.Controls.TextBlock)(this.FindName("raisedBy")));
            this.Approval_Status = ((System.Windows.Controls.Grid)(this.FindName("Approval_Status")));
            this.verStatus = ((System.Windows.Controls.TextBlock)(this.FindName("verStatus")));
            this.verBy = ((System.Windows.Controls.TextBlock)(this.FindName("verBy")));
            this.verOn = ((System.Windows.Controls.TextBlock)(this.FindName("verOn")));
            this.initialApprovalStatus = ((System.Windows.Controls.TextBlock)(this.FindName("initialApprovalStatus")));
            this.IAppBy = ((System.Windows.Controls.TextBlock)(this.FindName("IAppBy")));
            this.IAppOn = ((System.Windows.Controls.TextBlock)(this.FindName("IAppOn")));
            this.finalApprovalStatus = ((System.Windows.Controls.TextBlock)(this.FindName("finalApprovalStatus")));
            this.FAppBy = ((System.Windows.Controls.TextBlock)(this.FindName("FAppBy")));
            this.FAppOn = ((System.Windows.Controls.TextBlock)(this.FindName("FAppOn")));
            this.Vendor_Details = ((System.Windows.Controls.Grid)(this.FindName("Vendor_Details")));
            this.VendorCombo = ((SyedMehrozAlam.CustomControls.AutoCompleteComboBox)(this.FindName("VendorCombo")));
            this.BU_Details = ((System.Windows.Controls.Grid)(this.FindName("BU_Details")));
            this.hpbtnAddLine = ((System.Windows.Controls.HyperlinkButton)(this.FindName("hpbtnAddLine")));
            this.dataGrid = ((System.Windows.Controls.DataGrid)(this.FindName("dataGrid")));
            this.dataGrid_AfterVerify = ((System.Windows.Controls.DataGrid)(this.FindName("dataGrid_AfterVerify")));
            this.dataGrid_AfterMIApprove = ((System.Windows.Controls.DataGrid)(this.FindName("dataGrid_AfterMIApprove")));
            this.dataGrid_AfterPOApproval = ((System.Windows.Controls.DataGrid)(this.FindName("dataGrid_AfterPOApproval")));
            this.SP_PR_Remarks = ((System.Windows.Controls.Grid)(this.FindName("SP_PR_Remarks")));
            this.tbxRemarks = ((System.Windows.Controls.TextBox)(this.FindName("tbxRemarks")));
            this.tbxApprovalRemarks = ((System.Windows.Controls.TextBox)(this.FindName("tbxApprovalRemarks")));
            this.Bottom = ((System.Windows.Controls.Grid)(this.FindName("Bottom")));
            this.sp_Vat = ((System.Windows.Controls.StackPanel)(this.FindName("sp_Vat")));
            this.lbl_vat = ((System.Windows.Controls.TextBlock)(this.FindName("lbl_vat")));
            this.txt_Vat = ((System.Windows.Controls.TextBox)(this.FindName("txt_Vat")));
            this.lbl_vat_Copy = ((System.Windows.Controls.TextBlock)(this.FindName("lbl_vat_Copy")));
            this.sp_cst = ((System.Windows.Controls.StackPanel)(this.FindName("sp_cst")));
            this.lbl_vat2 = ((System.Windows.Controls.TextBlock)(this.FindName("lbl_vat2")));
            this.txt_CST = ((System.Windows.Controls.TextBox)(this.FindName("txt_CST")));
            this.lbl_vat2_Copy = ((System.Windows.Controls.TextBlock)(this.FindName("lbl_vat2_Copy")));
            this.sp_TransPortation = ((System.Windows.Controls.StackPanel)(this.FindName("sp_TransPortation")));
            this.lbl_Transportation = ((System.Windows.Controls.TextBlock)(this.FindName("lbl_Transportation")));
            this.txt_Transportation = ((System.Windows.Controls.TextBox)(this.FindName("txt_Transportation")));
            this.sp_Payment = ((System.Windows.Controls.StackPanel)(this.FindName("sp_Payment")));
            this.lbl_Payment = ((System.Windows.Controls.TextBlock)(this.FindName("lbl_Payment")));
            this.txt_Total = ((System.Windows.Controls.TextBlock)(this.FindName("txt_Total")));
            this.stackPanel = ((System.Windows.Controls.StackPanel)(this.FindName("stackPanel")));
            this.txt_Payment = ((System.Windows.Controls.TextBox)(this.FindName("txt_Payment")));
            this.lbl_comment = ((System.Windows.Controls.TextBlock)(this.FindName("lbl_comment")));
            this.lbl_ContactPersonPh_Copy = ((System.Windows.Controls.TextBlock)(this.FindName("lbl_ContactPersonPh_Copy")));
            this.txt_ContactPersonName = ((System.Windows.Controls.TextBox)(this.FindName("txt_ContactPersonName")));
            this.lbl_ContactPersonPh = ((System.Windows.Controls.TextBlock)(this.FindName("lbl_ContactPersonPh")));
            this.txt_ContactPersonPh = ((System.Windows.Controls.TextBox)(this.FindName("txt_ContactPersonPh")));
            this.lbl_Comment2 = ((System.Windows.Controls.TextBlock)(this.FindName("lbl_Comment2")));
            this.PR_Btn_Panel = ((System.Windows.Controls.StackPanel)(this.FindName("PR_Btn_Panel")));
            this.btnSave = ((System.Windows.Controls.Button)(this.FindName("btnSave")));
            this.btnEmail = ((System.Windows.Controls.Button)(this.FindName("btnEmail")));
            this.ConvertToPO = ((System.Windows.Controls.Button)(this.FindName("ConvertToPO")));
            this.btnSubmitToVerify = ((System.Windows.Controls.Button)(this.FindName("btnSubmitToVerify")));
            this.btnVerify = ((System.Windows.Controls.Button)(this.FindName("btnVerify")));
            this.btnSubmitToMIApproval = ((System.Windows.Controls.Button)(this.FindName("btnSubmitToMIApproval")));
            this.btnInitialApprove = ((System.Windows.Controls.Button)(this.FindName("btnInitialApprove")));
            this.btnSubmitToPurchase = ((System.Windows.Controls.Button)(this.FindName("btnSubmitToPurchase")));
            this.btnSubmitToHOPurchase = ((System.Windows.Controls.Button)(this.FindName("btnSubmitToHOPurchase")));
            this.btnSubmitToPOApproval = ((System.Windows.Controls.Button)(this.FindName("btnSubmitToPOApproval")));
            this.btnFinalApprove = ((System.Windows.Controls.Button)(this.FindName("btnFinalApprove")));
            this.btnToPR = ((System.Windows.Controls.Button)(this.FindName("btnToPR")));
            this.btnToMO = ((System.Windows.Controls.Button)(this.FindName("btnToMO")));
            this.download = ((Contruction.VendorPrintLink)(this.FindName("download")));
        }
    }
}

