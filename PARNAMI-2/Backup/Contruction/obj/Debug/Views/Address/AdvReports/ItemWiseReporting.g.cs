﻿#pragma checksum "D:\Softwares\Office Works\PARNAMI\PARNAMI-2\Backup\Contruction\Views\Address\AdvReports\ItemWiseReporting.xaml" "{406ea660-64cf-4c82-b6f0-42d48172a799}" "41F52F9B7387A546B9536F71A444CBA3"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.18408
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Automation.Peers;
using System.Windows.Automation.Provider;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Interop;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Resources;
using System.Windows.Shapes;
using System.Windows.Threading;


namespace Contruction {
    
    
    public partial class ItemWiseReporting : System.Windows.Controls.UserControl {
        
        internal System.Windows.Controls.Grid LayoutRoot;
        
        internal System.Windows.Controls.AutoCompleteBox ACBVendor;
        
        internal System.Windows.Controls.AutoCompleteBox ACBItem;
        
        internal System.Windows.Controls.DatePicker DTPStartDate;
        
        internal System.Windows.Controls.DatePicker DTPEndDate;
        
        internal System.Windows.Controls.Button BtnSubmit;
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Windows.Application.LoadComponent(this, new System.Uri("/Contruction;component/Views/Address/AdvReports/ItemWiseReporting.xaml", System.UriKind.Relative));
            this.LayoutRoot = ((System.Windows.Controls.Grid)(this.FindName("LayoutRoot")));
            this.ACBVendor = ((System.Windows.Controls.AutoCompleteBox)(this.FindName("ACBVendor")));
            this.ACBItem = ((System.Windows.Controls.AutoCompleteBox)(this.FindName("ACBItem")));
            this.DTPStartDate = ((System.Windows.Controls.DatePicker)(this.FindName("DTPStartDate")));
            this.DTPEndDate = ((System.Windows.Controls.DatePicker)(this.FindName("DTPEndDate")));
            this.BtnSubmit = ((System.Windows.Controls.Button)(this.FindName("BtnSubmit")));
        }
    }
}

