﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SIMS.TM.BLL
{
    using DAL;
    using Model;
    public class MBookDataProvider:IMBookDataProvider
    {
        TMDBDataContext db = new TMDBDataContext();
        public IPaginated<MMBook> GetMBookHeaders(string username, int? pageindex, int? pagesize, string Key, DateTime? startDate,DateTime? endDate, int? sideid, bool showall = true)
        {
            var data = db.MeasurementBooks.OrderByDescending(x => x.ID);
            if(startDate.HasValue && endDate.HasValue)
                data.Where(a=>a.CreatedOn.Value.CompareTo(startDate.Value)>=0 && a.CreatedOn.Value.CompareTo(endDate)<=0);
            return data.FilterBySite(sideid)
                        .ToPaginate(pageindex, pagesize)
                        .Select(x => new MMBook()
                        {
                            ID = x.ID,
                            Description=x.Description,
                            Floor=x.Floor,
                            Remarks=x.Remarks,
                            Site=x.BUs.DisplaySite,
                            CreatedOn=x.CreatedOn
                        });
            
        }

        public MMBook GetMBookByID(int MBookID)
        {
            throw new NotImplementedException();
        }
    }
}
