﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SIMS.VendorPayments
{
    public partial class BUs
    {
        public string DisplaySite
        {
            get 
            {
                return Name + " (ID: " + ID + ", Code: " + Code + ", Address: " + Address + ", " + City + ")";
            }
        }
    }

}
