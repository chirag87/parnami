﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SIMS.MM.DAL.PrintReports
{
    using SIMS.ReportLoader;

    public class TOReceivePrintLoader : ReportLoader
    {
        public override RDLCReportData GetReportData(ReportParameterDictionary Dic)
        {
            RDLCReportData data = new RDLCReportData();

            parnamidb1Entities db = new parnamidb1Entities();
            MMReportsDBDataContext rdb = new MMReportsDBDataContext();
            rdb.CommandTimeout = 5000;
            int mo_rec_id = (int)Dic["id"];

            var header = rdb.GetMOReceiveHeaders().Where(x => x.ID == mo_rec_id).ToList();
            var Tsite = rdb.GetSiteDetailsbySiteID(header.First().TransferringSiteID);
            var Rsite = rdb.GetSiteDetailsbySiteID(header.First().RequestingSiteID);
            var detail = rdb.GetMOReceiveDetailsByUniqueID(mo_rec_id);
            var app = db.AppSettings;

            data.Add("Headers", header);
            data.Add("Details", detail);
            data.Add("TransferringSiteDetails", Tsite);
            data.Add("RequestingSiteDetails", Rsite);
            data.Add("AppSettings", app);

            return data;
        }
    }
}
