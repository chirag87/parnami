﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SIMS.MM.DAL
{
    public partial class Item
    {
        public string DisplayName
        {
            get 
            { 
                //return this.ItemCategory.Name +" ("+ Name +", Units: "+this.MeasurementID + ")"; 
                return this.Name + " (" + ItemCategory.Name + ", Units: " + this.MeasurementID + ")";
            }
        }
    }
}
