﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SIMS.MM.DAL
{
    public partial class parnamidb1Entities
    {

        //public parnamidb1Entities()
        //    : this(ConnectionProvider.ForEntities())
        //{

        //}

        //public IQueryable<InventoryTransaction> CashPurchaseReceivings
        //{
        //    get
        //    {
        //        return this.InventoryTransactions.FilterByType("Cash");
        //    }
        //}

        //public IQueryable<InventoryTransaction> MORelease
        //{
        //    get
        //    {
        //        return this.InventoryTransactions.FilterByType("MTNRelease");
        //    }
        //}

        public void UpdateLastPrices(PR _pr, bool IsUpdate = false)
        {
            foreach (var line in _pr.PRDetails)
            {
                if (IsUpdate)
                    line.LastPrice = GetLastPrice(line.ItemID, _pr.SiteID,line.SizeID,line.BrandID,_pr.ID);
                else
                    line.LastPrice = GetLastPrice(line.ItemID, _pr.SiteID, line.SizeID, line.BrandID);
            }
        }

        public double GetLastPrice(int itemid, int siteid, int? sizeid, int? brandid, int? prid = null)
        {
            var qry = this.PRDetails.Where(x => x.PR.SiteID == siteid && x.ItemID == itemid);
            if (sizeid.HasValue)
                qry = qry.Where(x => x.SizeID == sizeid.Value);
            if (brandid.HasValue)
                qry = qry.Where(x => x.BrandID == brandid.Value);
            if (prid.HasValue)
                qry = qry.Where(x => x.PRID < prid.Value);
            if (qry.Count() == 0) return 0;
            return qry.OrderByDescending(x => x.PRID).First().UnitPrice;
        }

        public IEnumerable<int> GetAllowedSiteIDs(string username, bool ShowAll = false)
        {
            if (ShowAll)
                return this.Buses.Select(x => x.ID).ToList();
            var data = this.AllowedBUs.Where(x => x.LoginID == username).Select(x => x.BUID).ToList();
            return data;
        }

        //public IQueryable<InventoryTransaction> DirectPurchases
        //{
        //    get
        //    {
        //        return this.InventoryTransactions.FilterByType("Direct");
        //    }
        //}

        //public IQueryable<InventoryTransaction> Receivings
        //{
        //    get
        //    {
        //        return this.InventoryTransactions.FilterByMultipleTypes("GRN", "Direct");
        //    }
        //}

        //public IQueryable<InventoryTransaction> AllGRNs
        //{
        //    get
        //    {
        //        //var inv = this.InventoryTransactions;
        //        //return StaticDBExtensions.FilterByType(inv, "GRN");
        //        return this.InventoryTransactions.FilterByType("GRN");
        //    }
        //}

        //public IQueryable<InventoryTransaction> AllConsumptions
        //{
        //    get
        //    {
        //        return this.InventoryTransactions.FilterByType("Consumption");
        //    }
        //}

        public int GetMaxPRID()
        {
            if (PRs.Count() == 0) return 0;
            return PRs.Max(x => x.ID);
        }

        public string GetSiteCode(int id)
        {
            try { return this.Buses.Single(x => x.ID == id).Code; }
            catch { return ""; }
        }

        public int GetMaxPOID()
        {
            var data = POs.AsQueryable();
            if (data.Count() == 0) return 0;
            return data.Max(x => x.ID);
        }

        //public int GetMaxRID(string type)
        //{
        //    var qry = InventoryTransactions.FilterByType(type);
        //    if (qry.Count() == 0) return 0;
        //    return qry.Max(x => x.ID);
        //}

        public int GetMaxGRNID()
        {
            if (GRNs.Count() == 0) return 0;
            return GRNs.Max(x => x.ID);
        }

        public int GetMaxRTVID()
        {
            if (ReturnToVendors.Count() == 0) return 0;
            return ReturnToVendors.Max(x => x.ID);
        }

        public int GetMaxCPID()
        {
            if (CashPurchases.Count() == 0) return 0;
            return CashPurchases.Max(x => x.ID);
        }

        public int GetMaxConsumptionID()
        {
            if (Consumptions.Count() == 0) return 0;
            return Consumptions.Max(x => x.ID);
        }

        //public int GetMaxMOId(string p)
        //{
        //    var qry = InventoryTransactions.FilterByType(p);
        //    if (qry.Count() == 0) return 0;
        //    return qry.Max(x => x.ID);
        //}

        public int GetMaxMOReleaseID()
        {
            if (MOReleasings.Count() == 0) return 0;
            return MOReleasings.Max(x => x.ID);
        }

        public int GetMaxMOReceiveID()
        {
            if (MOReceivings.Count() == 0) return 0;
            return MOReceivings.Max(x => x.ID);
        }

        public int GetMaxMRIssueID()
        {
            if (MaterialIssues.Count() == 0) return 0;
            return MaterialIssues.Max(x => x.ID);
        }

        public int GetMaxMRReturnID()
        {
            if (MaterialReturns.Count() == 0) return 0;
            return MaterialReturns.Max(x => x.ID);
        }

        public int GetMaxRRID(string type, int year, int siteid)
        {
            //var qry = InventoryTransactions.Where(x => x.TypeID == type && x.Year == year && x.BUID == siteid);
            var qry = InventoryTransactions.Where(x => x.TypeID == type && x.BUID == siteid);
            if (qry.Count() == 0) return 0;
            return qry.Max(x => x.RID);
        }

        public int GetMaxGRNRID(int year, int siteid)
        {
            //var qry = GRNs.Where(x => x.Year == year && x.BUID == siteid);
            var qry = GRNs.Where(x => x.BUID == siteid);
            if (qry.Count() == 0) return 0;
            return qry.Max(x => x.RID);
        }

        public int GetMaxRTVRID(int year, int siteid)
        {
            var qry = ReturnToVendors.Where(x => x.Year == year && x.BUID == siteid);
            if (qry.Count() == 0) return 0;
            return qry.Max(x => x.RID);
        }

        public int GetMaxCashPurchaseRID(int year, int siteid)
        {
            //var qry = CashPurchases.Where(x => x.Year == year && x.BUID == siteid);
            var qry = CashPurchases.Where(x => x.BUID == siteid);
            if (qry.Count() == 0) return 0;
            return qry.Max(x => x.RID);
        }

        public int GetMaxConsumptionRID(int year, int siteid)
        {
            //var qry = Consumptions.Where(x => x.Year == year && x.BUID == siteid);
            var qry = Consumptions.Where(x => x.BUID == siteid);
            if (qry.Count() == 0) return 0;
            return qry.Max(x => x.RID);
        }

        public int GetMaxMORID(int year, int siteid, string conT)
        {
            //var qry = MTRs.Where(x => x.Year == year && x.RequestedSiteID == siteid && x.ConsumableType == conT);
            var qry = MTRs.Where(x => x.RequestedSiteID == siteid && x.ConsumableType == conT);
            if (qry.Count() == 0) return 0;
            return qry.Max(x => x.RID);
        }

        //public int GetMaxMOReleaseRID(int year, int siteid)
        //{
        //    var qry = MOReleasings.Where(x => x.Year == year && x.TransferringSiteID == siteid);
        //    if (qry.Count() == 0) return 0;
        //    return qry.Max(x => x.RID);
        //}

        //public int GetMaxMOReceiveRID(int year, int siteid)
        //{
        //    var qry = MOReceivings.Where(x => x.Year == year && x.RequestingSiteID == siteid);
        //    if (qry.Count() == 0) return 0;
        //    return qry.Max(x => x.RID);
        //}

        public int GetMaxMOReleaseRID(int moid)
        {
            var qry = MOReleasings.Where(x => x.MTRID == moid);
            if (qry.Count() == 0) return 0;
            return qry.Max(x => x.RID);
        }

        public int GetMaxMOReceiveRID(int moid)
        {
            var qry = MOReceivings.Where(x => x.MTRID == moid);
            if (qry.Count() == 0) return 0;
            return qry.Max(x => x.RID);
        }

        public int GetMaxMRIssueRID(int year, int siteid)
        {
            //var qry = MaterialIssues.Where(x => x.Year == year && x.BUID == siteid);
            var qry = MaterialIssues.Where(x => x.BUID == siteid);
            if (qry.Count() == 0) return 0;
            return qry.Max(x => x.RID);
        }

        public int GetMaxMRReturnRID(int year, int siteid)
        {
            //var qry = MaterialReturns.Where(x => x.Year == year && x.BUID == siteid);
            var qry = MaterialReturns.Where(x => x.BUID == siteid);
            if (qry.Count() == 0) return 0;
            return qry.Max(x => x.RID);
        }

        public int GetMaxVID()
        {
            if (Vendors.Count() == 0) return 0;
            return Vendors.Max(x => x.ID);
        }

        public int GetMaxAddressID()
        {
            if (Addresses.Count() == 0) return 0;
            return Addresses.Max(x => x.ID);
        }

        public int GetMaxDocumentID()
        {
            if (Documents.Count() == 0) return 0;
            return Documents.Max(x => x.ID);
        }

        public int GetMaxContactID()
        {
            if (Contacts.Count() == 0) return 0;
            return Contacts.Max(x => x.ID);
        }

        public int GetMaxPriceID()
        {
            if (PriceMasters.Count() == 0) return 0;
            return PriceMasters.Max(x => x.ID);
        }

        public int GetMaxBillingID()
        {
            if (Billings.Count() == 0) return 0;
            return Billings.Max(x => x.ID);
        }

        public int GetMaxBillingVerificationID()
        {
            if (BillingVerifications.Count() == 0) return 0;
            return BillingVerifications.Max(x => x.ID);
        }

        public int GetBillingVerificationCounter(int billingid)
        {
            if (BillingVerifications.Where(y => y.BillingID == billingid).Count() == 0) return 1;
            return BillingVerifications.Where(y => y.BillingID == billingid).Max(x => x.VerificationCounter.Value) + 1;
        }

        public int GetMaxAccountID()
        {
            if (AccountDetails.Count() == 0) return 0;
            return AccountDetails.Max(x => x.ID);
        }

        public int GetMAX_MTR_ID()
        {
            if (MTRs.Count() == 0) return 0;
            return MTRs.Max(x => x.ID);
        }
        public int GetMaxItemID()
        {
            if (Items.Count() == 0) return 0;
            return Items.Max(x => x.ID);
        }

        public int GetMaxPRsRefID(string sitecode, int year)
        {
            //var qry = PRs.Where(x => x.Year == year && x.SiteCode == sitecode);
            var qry = PRs.Where(x => x.SiteCode == sitecode);
            if (qry.Count() == 0) return 0;
            return qry.Max(x => x.RID);
        }

        public int GetMaxPORID(string siteid, int year)
        {
            //var qry = POs.Where(x => x.Year == year && x.SiteID == siteid);
            var qry = POs.Where(x => x.SiteID == siteid);
            if (qry.Count() == 0) return 0;
            return qry.Max(x => x.RID);
        }

        public int getmaxSiteID()
        {
            var qry = Buses.AsQueryable();
            if (qry.Count() == 0) return 0;
            return qry.Max(x => x.ID);
        }

        public int GetMAX_TransporterID()
        {
            if (Transporters.Count() == 0) return 0;
            return Transporters.Max(x => x.ID);
        }

        public int GetMax_MR_ID()
        {
            if (MRs.Count() == 0) return 0;
            return MRs.Max(x => x.ID);
        }

        //public void CheckForMO_Close(int? mtrId)
        //{
        //    var data = MTRs.Single(x => x.ID == mtrId);
        //    if (data.CanReceive == false && data.CanRelease == false)
        //    {
        //        data.IsClosed = true;
        //        SaveChanges();
        //    }
        //}

        public void CloseMO_IFRequired(int? moid)
        {
            var data = MTRs.Single(x => x.ID == moid);
            var CanRelease = data.MTRLines.Any(y => y.RequiredQty > y.ReleasedQty);
            bool CanReceive = data.MTRLines.Any(y => y.InTransitQty > 0);
            if (!CanReceive && !CanRelease)
            {
                data.IsClosed = true;
                SaveChanges();
            }
        }

        public void CloseMR_IFRequired(int? mrid)
        {
            if (MRs.Count() > 0)
            {
                var data = MRs.Single(x => x.ID == mrid);
                var CanIssue = data.MRLines.Any(y => y.Qty > y.IssuedQty);
                bool CanReturn = data.MRLines.Any(y => y.IssuedQty > y.ReturnedQty);
                if (!CanReturn && !CanIssue)
                {
                    data.IsClosed = true;
                    SaveChanges();
                }
            }
        }

        public int GetMAX_PriceMID()
        {
            var data = PriceMasters.AsQueryable();
            if (data.Count() == 0) return 0;
            return data.Max(x => x.ID);
        }

        public MTRLine GetMTRLine(int transSiteID, int ReqSiteID, string ConsumableType)
        {
            var data = MTRLines
            .Where(x =>
                x.MTR.TransferringSiteID == transSiteID &&
                x.MTR.RequestedSiteID == ReqSiteID &&
                x.MTR.ConsumableType == ConsumableType &&
                x.MTR.MTRLines.All(y => y.ReleasedQty == 0) &&
                !x.MTR.IsClosed)
                //.Where(x => x.ReleasedQty == 0)
                .OrderByDescending(x => x.MTRID)
                //.Where(x => x.ReleasedQty < x.RequiredQty)
            .FirstOrDefault();
            return data;
        }
    }

    public static class StaticDBExtensions
    {

        //public static IQueryable<InventoryTransaction> FilterByType(this IQueryable<InventoryTransaction> qry, string type)
        //{
        //    if (type != null && type != "ALL")
        //        return qry.Where(x => x.TypeID.ToLower() == type.ToLower()).OrderByDescending(x => x.ForDate);
        //    else
        //        return qry;
        //}

        //public static IQueryable<InventoryTransaction> FilterByMultipleTypes(this IQueryable<InventoryTransaction> qry, string type, string type1)
        //{
        //    var data = qry.Where(x => x.TypeID.ToLower() == type.ToLower() || x.TypeID.ToLower() == type1.ToLower());
        //    return data;
        //}

        public static IQueryable<GetPendingReceivingsForBillingResult> FilterByItem(this IQueryable<GetPendingReceivingsForBillingResult> qry, int? itemid)
        {
            if (itemid.HasValue)
                return qry.Where(x => x.ItemID == itemid);
            return qry;
        }

        public static IQueryable<GetPendingReceivingsForBillingResult> FilterByVendor(this IQueryable<GetPendingReceivingsForBillingResult> qry, int? vendorid)
        {
            if (vendorid.HasValue)
                return qry.Where(x => x.VendorID == vendorid);
            return qry;
        }

        public static IQueryable<GetPendingReceivingsForBillingResult> FilterByDate(this IQueryable<GetPendingReceivingsForBillingResult> qry, DateTime? sdate, DateTime? edate)
        {
            if (sdate.HasValue && edate.HasValue)
                return qry.Where(x => x.ForDate >= sdate && x.ForDate <= edate);
            return qry;
        }

        public static IQueryable<GetPendingReceivingsForBillingResult> FilterByChallanNo(this IQueryable<GetPendingReceivingsForBillingResult> qry, string challan)
        {
            if (challan != null)
                return qry.Where(x => x.ChallanNo.ToLower().Contains(challan.ToLower()) || x.LineChallanNo.ToLower().Contains(challan.ToLower()));
            return qry;
        }

        public static IQueryable<GetPendingReceivingsForBillingResult> FilterBySite(this IQueryable<GetPendingReceivingsForBillingResult> qry, int? siteid)
        {
            if (siteid.HasValue)
                return qry.Where(x => x.BUID == siteid);
            return qry;
        }

        //public static IQueryable<InventoryTransaction> FilterByVendor(this IQueryable<InventoryTransaction> qry, int? vendorid)
        //{
        //    if (vendorid.HasValue)
        //        return qry.Where(x => x.VendorID == vendorid);
        //    return qry;
        //}

        //public static IQueryable<InventoryTransaction> FilterBySite(this IQueryable<InventoryTransaction> qry, int? siteid)
        //{
        //    if (siteid.HasValue)
        //        return qry.Where(x => x.BUID == siteid.Value);
        //    return qry;
        //}

        public static IQueryable<PR> FilterBySite(this IQueryable<PR> qry, int? siteid)
        {
            if (siteid.HasValue)
                return qry.Where(x => x.SiteID == siteid);
            return qry;
        }

        public static IQueryable<PriceMaster> FilterBySite(this IQueryable<PriceMaster> qry, int? siteid)
        {
            if (siteid.HasValue)
                return qry.Where(x => x.SiteID == siteid);
            return qry;
        }

        public static IQueryable<PriceMaster> FilterByVendor(this IQueryable<PriceMaster> qry, int? vendorid)
        {
            if (vendorid.HasValue)
                return qry.Where(x => x.VendorID == vendorid);
            return qry;
        }

        public static IQueryable<PriceMaster> FilterByItem(this IQueryable<PriceMaster> qry, int? itemid)
        {
            if (itemid.HasValue)
                return qry.Where(x => x.ItemID == itemid);
            return qry;
        }

        public static IQueryable<MR> FilterBySite(this IQueryable<MR> qry, int? siteid)
        {
            if (siteid.HasValue)
                return qry.Where(x => x.SiteID == siteid);
            return qry;
        }

        public static IQueryable<MTR> FilterBySite(this IQueryable<MTR> qry, int? siteid, int? siteid2)
        {
            if (siteid.HasValue)
                return qry.Where(x => x.RequestedSiteID == siteid || x.TransferringSiteID == siteid2);
            return qry;
        }

        public static IQueryable<PR> FilterByVendor(this IQueryable<PR> qry, int? vendorid)
        {
            if (vendorid.HasValue)
                return qry.Where(x => x.VendorID == vendorid);
            return qry;

        }

        public static IQueryable<T> Paginate<T>(this IQueryable<T> qry, int? pageindex, int? pagesize)
        {
            pageindex = pageindex ?? 0;
            pagesize = pagesize ?? 20;
            int toskip = (pageindex.Value * pagesize.Value);
            return qry.Skip(toskip).Take(pagesize.Value);
        }

        public static IQueryable<Billing> FilterByVendor(this IQueryable<Billing> qry, int? vendorid)
        {
            if (vendorid.HasValue)
                return qry.Where(x => x.VendorID == vendorid);
            return qry;
        }

        public static IQueryable<Billing> FilterBySite(this IQueryable<Billing> qry, int? siteid)
        {
            if (siteid.HasValue)
                return qry.Where(x => x.SiteID == siteid);
            return qry;
        }

        public static IQueryable<Billing> FilterByfilter(this IQueryable<Billing> qry, string filter)
        {
            if (filter != null)
            {
                if (filter == "Approved (Pending Payment)")
                    return qry.Where(x => x.IsApproved == true);
                else if (filter == "Draft")
                {
                    var drafts = qry.Where(x => x.BillingVerifications.Count() == 0 && x.IsApproved == false);
                    return drafts;
                }
                else if (filter == "Verified")
                {
                    var verifiedbills = qry.Where(x => x.BillingVerifications.Count() > 0 && x.IsApproved == false);
                    return verifiedbills;
                }
                else if (filter == "Paid")
                    return qry;
                else if (filter == "All")
                    return qry;
                else
                    return qry;
            }
            else
                return qry;
        }
    }
}
