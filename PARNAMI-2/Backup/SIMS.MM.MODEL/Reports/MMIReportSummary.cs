﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace SIMS.MM.MODEL
{
    [DataContract]
    public class MMIReportSummary : EntityModel
    {
        [DataMember]
        public int? SiteID { get; set; }

        [DataMember]
        public string Site { get; set; }

        [DataMember]
        public int? ItemID { get; set; }

        [DataMember]
        public string Item { get; set; }

        [DataMember]
        public string DisplayItem { get; set; }

        [DataMember]
        public int? SizeID { get; set; }

        [DataMember]
        public string Size { get; set; }

        [DataMember]
        public int? BrandID { get; set; }

        [DataMember]
        public string Brand { get; set; }

        [DataMember]
        public string ConsumableType { get; set; }

        [DataMember]
        public double TotalMIQty { get; set; }

        [DataMember]
        public double TobePOQty { get; set; }

        [DataMember]
        public double TOQty { get; set; }

        [DataMember]
        public double TODispatchQty { get; set; }

        [DataMember]
        public double POQty { get; set; }

        [DataMember]
        public double MRNQty { get; set; }

        [DataMember]
        public double CPQty { get; set; }

        public double PendingQty
        {
            get { return TotalMIQty - TOQty - MRNQty; }
            set { }
        }

        [DataMember]
        public double QtyInCloseMIs { get; set; }

        [DataMember]
        public double PendingQtyAfterClose
        {
            get { return TobePOQty - QtyInCloseMIs; }
            set { }
        }

        [DataMember]
        public int NumberofEntries { get; set; }

        [DataMember]
        public int RecordsCount { get; set; }
    }
}
