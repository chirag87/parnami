﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;

namespace SIMS.MM.MODEL
{
    [DataContract]
    public class MCPReport : EntityModel
    {
        [DataMember]
        public int ID { get; set; }

        [DataMember]
        public string RefNo { set; get; }

        [DataMember]
        public DateTime Date { get; set; }

        [DataMember]
        [Display(AutoGenerateField = false)]
        public string Vendor { get; set; }

        [DataMember]
        [Display(Name = "Vendor")]
        public string DisplayVendor { get; set; }

        [DataMember]
        [Display(AutoGenerateField = false)]
        public string Site { get; set; }

        [DataMember]
        [Display(Name = "Site")]
        public string DisplaySite { get; set; }

        [DataMember]
        public string Purchaser { get; set; }

        [DataMember]
        [Display(AutoGenerateField = false)]
        public int ItemID { get; set; }

        [DataMember]
        public string Item { get; set; }

        [DataMember]
        [Display(AutoGenerateField = false)]
        public int CategoryID { get; set; }

        [DataMember]
        [Display(AutoGenerateField = false)]
        public string ItemCategory { get; set; }

        [DataMember]
        [Display(AutoGenerateField = false)]
        public int SizeID { get; set; }

        [DataMember]
        public string Size { get; set; }

        [DataMember]
        [Display(AutoGenerateField = false)]
        public int BrandID { get; set; }

        [DataMember]
        public string Brand { get; set; }

        [DataMember]
        public string MU { get; set; }

        [DataMember]
        public double Quantity { get; set; }

        [DataMember]
        public double? PerUnitPrice { get; set; }

        [DataMember]
        public double? TotalAmount { get; set; }

        [DataMember]
        public string InvoiceNo { get; set; }

        [DataMember]
        public string ChallanNo { get; set; }

        [DataMember]
        public string Remarks { get; set; }
    }
}
