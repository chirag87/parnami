﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;

namespace SIMS.MM.MODEL
{
    [DataContract]
    public class MConsumptionReport : EntityModel
    {     
        [DataMember]
        [Display(AutoGenerateField = false)]
        public string Type{ get; set; }

        [DataMember]
        public int ID { get; set; }

        [DataMember]
        public string RefNo { get; set; }

        [DataMember]
        public System.DateTime ForDate { get; set; }

        [DataMember]
        public int LineID { get; set; }

        [DataMember]
        [Display(AutoGenerateField = false)]
        public string Site { get; set; }

        [DataMember]
        [Display(Name="Site")]
        public string DisplaySite { get; set; }

        [DataMember]
        public int ItemID { get; set; }

        [DataMember]
        public string Item { get; set; }

        [DataMember]
        public int CategoryID { get; set; }

        [DataMember]
        public int SizeID { get; set; }

        [DataMember]
        public string Size { get; set; }

        [DataMember]
        public int BrandID { get; set; }

        [DataMember]
        public string Brand { get; set; }

        [DataMember]
        public string CreatedBy { get; set; }

        [DataMember]
        public string ItemCategory { get; set; }

        [DataMember]
        public string Unit { get; set; }

        [DataMember]
        public System.Nullable<double> Qty { get; set; }

        [DataMember]
        public string Remarks { get; set; }
    }
}
