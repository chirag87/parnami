﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace SIMS.MM.MODEL
{
    [DataContract]
    public class MVendorHeader
    {
        [DataMember]
        public int ID { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public string Address { get; set; }

        [DataMember]
        public string City { get; set; }

        [DataMember]
        public string State { get; set; }

        [DataMember]
        public string PinCode { get; set; }

        [DataMember]
        public string ContactName { get; set; }

        [DataMember]
        public string ContactMobile { get; set; }

        [DataMember]
        public string Address2 { get; set; }

        [DataMember]
        public string City2 { get; set; }

        [DataMember]
        public string State2 { get; set; }

        [DataMember]
        public string PinCode2 { get; set; }

        [DataMember]
        public string TINNo { get; set; }

        [DataMember]
        public string CST { get; set; }

        [DataMember]
        public string PANNo { get; set; }

        [DataMember]
        public string ServiceTaxNo { get; set; }
    }
}
