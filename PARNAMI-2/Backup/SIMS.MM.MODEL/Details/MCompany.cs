﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace SIMS.MM.MODEL
{
    [DataContract]
    public class MCompany : EntityModel
    {
        [DataMember]
        public int ID { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public string DisplayName
        {
            get { return Name + " - " + Code + " ( " + FullAddress + " )"; }
            set { }
        }

        [DataMember]
        public string Code { get; set; }

        [DataMember]
        public string AddressLine1 { get; set; }

        [DataMember]
        public string AddressLine2 { get; set; }

        [DataMember]
        public string AddressLine3 { get; set; }

        [DataMember]
        public string FullAddress
        {
            get { return AddressLine1 + "," + AddressLine2 + "," + AddressLine3 + "," + City + " ( " + State + " ) - " + Pincode + " " + Country; }
            set { }
        }

        [DataMember]
        public string City { get; set; }

        [DataMember]
        public string State { get; set; }

        [DataMember]
        public string Country { get; set; }

        [DataMember]
        public string Pincode { get; set; }

        [DataMember]
        public string Email { get; set; }

        [DataMember]
        public string Phone { get; set; }

        [DataMember]
        public string Fax { get; set; }

        [DataMember]
        public string Mobile1 { get; set; }

        [DataMember]
        public string Mobile2 { get; set; }

        [DataMember]
        public string Website { get; set; }

        [DataMember]
        public string PANNo { get; set; }

        [DataMember]
        public string TINNo { get; set; }

        [DataMember]
        public string ServiceTaxNo { get; set; }
    }
}
