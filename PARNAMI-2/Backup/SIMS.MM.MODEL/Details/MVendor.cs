﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;

namespace SIMS.MM.MODEL
{
    [DataContract]
    public class MVendor : EntityModel
    {
        [DataMember]
        [Display(AutoGenerateField = false)]
        public int ID { get; set; }

        [DataMember]
        [Display(AutoGenerateField = false)]
        public string Display
        {
            get
            {
                return Name + " (ID: " + ID + "," + " Address: " + Address + ", " + City + ")";
            }
            set { }
        }

        [DataMember]
        public string Logo { get; set; }

        [DataMember]
        [Required]
        public string Name { get; set; }

        [DataMember]
        public string CompanyType { get; set; }

        [DataMember]
        public string CompanyOwner { get; set; }

        [DataMember]
        public string OwnerDesignation { get; set; }

        [DataMember]
        public string OwnerMobile { get; set; }

        [DataMember]
        public string OwnerEmail { get; set; }

        [DataMember]
        public string Address { get; set; }

        [DataMember]
        public string City { get; set; }

        [DataMember]
        public string State { get; set; }

        [DataMember]
        public string Country { get; set; }

        [DataMember]
        public string PinCode { get; set; }

        [DataMember]
        public string Phone { get; set; }

        [DataMember]
        public string Fax { get; set; }

        [DataMember]
        public string Email { get; set; }

        [DataMember]
        public string Website { get; set; }

        [DataMember]
        [Display(AutoGenerateField = false)]
        public bool IsActive { get; set; }

        [DataMember]
        public double? Rating { get; set; }

        [DataMember]
        public string TermsandConditions { get; set; }

        [DataMember]
        public int? DefaultPaymentDays { get; set; }

        [DataMember]
        public string Remarks { get; set; }

        [DataMember]
        public string TINNo { get; set; }

        [DataMember]
        public string CST { get; set; }

        [DataMember]
        public string PANNo { get; set; }

        [DataMember]
        public string ServiceTaxNo { get; set; }
    }
}
