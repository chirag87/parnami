﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace SIMS.MM.MODEL
{
    [DataContract]
    public class MBilling : EntityModel
    {
        public MBilling()
        {
            BillDate = DateTime.UtcNow.AddHours(5.5);
            PaymentNotBefore = DateTime.UtcNow.AddHours(5.5);
            GrossAmount = 0;
            Deductions = 0;
            TotalAmount = 0;
            DefaultPaymentDays = 45;
            //ApprovedOn = DateTime.UtcNow.AddHours(5.5);
        }

        public event EventHandler VendorIDChanged;
        public void RaiseVendorIDChanged()
        {
            if (VendorIDChanged != null)
                VendorIDChanged(this, new EventArgs());
        }

        public event EventHandler SiteIDChanged;
        public void RaiseSiteIDChanged()
        {
            if (SiteIDChanged != null)
                SiteIDChanged(this, new EventArgs());
        }

        [DataMember]
        public int ID { get; set; }

        [DataMember]
        public string Vendor { get; set; }

        int? _VendorID = new int();
        [DataMember]
        public int? VendorID
        {
            get { return _VendorID; }
            set
            {
                _VendorID = value;
                Notify("VendorID");
                RaiseVendorIDChanged();
            }
        }

        [DataMember]
        public string Site { get; set; }

        int? _SiteID = new int();
        [DataMember]
        public int? SiteID
        {
            get { return _SiteID; }
            set
            {
                _SiteID = value;
                Notify("SiteID");
                RaiseSiteIDChanged();
            }
        }

        [DataMember]
        public string PANNo { get; set; }

        [DataMember]
        public string BillNo { get; set; }

        DateTime _BillDate;
        [DataMember]
        public DateTime BillDate
        {
            get
            {
                if (_BillDate < DateTime.UtcNow.AddHours(5.5))
                    return _BillDate;
                else
                    return DateTime.UtcNow.AddHours(5.5);
            }
            set
            {
                _BillDate = value;
                UpdatePayNotBefore();
                Notify("BillDate");
            }
        }

        int _DefaultPaymentDays;
        [DataMember]
        public int DefaultPaymentDays
        {
            get { return _DefaultPaymentDays; }
            set
            {
                _DefaultPaymentDays = value;
                UpdatePayNotBefore();
                Notify("DefaultPaymentDays");
            }
        }

        [DataMember]
        public DateTime PaymentNotBefore { get; set; }

        bool? _IsApproved = false;
        [DataMember]
        public bool? IsApproved
        {
            get { return _IsApproved; }
            set
            {
                _IsApproved = value;
                Notify("IsApproved");
            }
        }

        [DataMember]
        public string ApprovedBy { get; set; }

        //[DataMember]
        //public DateTime? ApprovedOn { get; set; }
        DateTime? _ApprovedOn;
        [DataMember]
        public DateTime? ApprovedOn
        {
            get
            {
                if (_ApprovedOn < DateTime.UtcNow.AddHours(5.5))
                    return _ApprovedOn;
                else
                    return DateTime.UtcNow.AddHours(5.5);
            }
            set
            {
                _ApprovedOn = value;
                Notify("ApprovedOn");
            }
        }

        double? _GrossAmount = 0;
        [DataMember]
        public double? GrossAmount
        {
            get { return _GrossAmount; }
            set
            {
                if(BillingLines!=null)
                    _GrossAmount = BillingLines.Sum(x => x.LinePrice);
                Notify("GrossAmount");
            }
        }

        #region Properties for Get All Billings 

        [DataMember]
        public double? GrossAmnt { get; set; }

        [DataMember]
        public double? TotalAmnt { get; set; }

        [DataMember]
        public double? NetAmnt { get; set; }

        [DataMember]
        public string PaymentStatus { get; set; }

        [DataMember]
        public int? TotalBillingLines { get; set; }

        [DataMember]
        public int? PaidBillingLines { get; set; }

        [DataMember]
        public int? UnpaidBillignLines { get; set; }

        [DataMember]
        public double? TotalPaymentAmount { get; set; }

        [DataMember]
        public string PaymentRefno { get; set; }

        [DataMember]
        public double? CurrentPayment { get; set; }

        [DataMember]
        public DateTime? PaymentDate { get; set; }

        [DataMember]
        public double? TotalPaidAmount { get; set; }

        [DataMember]
        public double? PendingPayment
        {
            get 
            {
                return TotalPaymentAmount - TotalPaidAmount; 
            }
            set
            {
                Notify("PendingPayment");
            }
        }

        [DataMember]
        public bool IsPaid
        {
            get
            {
                if (PaymentStatus != null && PaymentStatus != "Unpaid")
                    return true;
                else
                    return false;
            }
            set
            {
                Notify("IsPaid");
                Notify("PaymentStatus");
            }
        }

        #endregion

        [DataMember]
        public double? TotalAmount
        {
            get { return NetAmount - Deductions; }
            set { }
        }

        [DataMember]
        public double? NetAmount
        {
            get { return GrossAmount + VAT + Surcharge + Cartage; }
            set { }
        }

        double? _Deductions = 0;
        [DataMember]
        public double? Deductions
        {
            get { return _Deductions; }
            set
            {
                _Deductions = value;
                UpdateTotalAmount();
                Notify("Deductions");
            }
        }

        double? _VATPercent = 0;
        [DataMember]
        public double? VATPercent
        {
            get { return _VATPercent; }
            set
            {
                _VATPercent = value;
                Notify("VATPercent");
                UpdateNetAmount();
                Notify("VAT");
            }
        }

        [DataMember]
        public double? VAT
        {
            get { return (GrossAmount * VATPercent) / 100; }
            set { }
        }

        double? _SurchargePercent = 0;
        [DataMember]
        public double? SurchargePercent
        {
            get { return _SurchargePercent; }
            set
            {
                _SurchargePercent = value;
                Notify("SurchargePercent");
                UpdateNetAmount();
                Notify("Surcharge");
            }
        }

        [DataMember]
        public double? Surcharge
        {
            get { return (VAT * SurchargePercent) / 100; }
            set { }
        }

        double? _Cartage = 0;
        [DataMember]
        public double? Cartage
        {
            get { return _Cartage; }
            set
            {
                _Cartage = value;
                UpdateNetAmount();
                Notify("Cartage");
            }
        }

        [DataMember]
        public string Remarks { get; set; }

        [DataMember]
        public string Status { get; set; }

        [DataMember]
        public ObservableCollection<MBillingLines> BillingLines { get; set; }

        [DataMember]
        public ObservableCollection<MBillingVerified> BillingVerifications { get; set; }

        public void UpdateGrossAmount()
        {
            if (this.BillingLines != null)
                GrossAmount = BillingLines.Sum(x => x.LinePrice);
        }

        public void UpdateNetAmount()
        {
            if (this.BillingLines != null)
                NetAmount = GrossAmount + VAT + Surcharge + Cartage; 
        }

        public void UpdateTotalAmount()
        {
            if (this.BillingLines != null)
                TotalAmount = NetAmount - Deductions;
        }

        public void UpdatePayNotBefore()
        {
            PaymentNotBefore = BillDate.AddDays((double)DefaultPaymentDays);
        }

        public void DeleteLine(int lid)
        {
            try
            {
                var line = BillingLines.Single(x => x.LineID == lid);
                BillingLines.Remove(line);
            }
            catch { }
        }
    }
}