﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace SIMS.MM.MODEL
{
    [DataContract]
    public class MAddress : EntityModel
    {
        [DataMember]
        public int ID { get; set; }

        [DataMember]
        public string Display 
        {
            get
            {
                return Title + ":\n\n" + AddressLine1 + "\n" + AddressLine2 + "\n" + City + "( " + State + " ) " + Country + " - " + Pincode + "\nPHONE:- " + Phone + "\nFAX:- " + Fax + "\nEMAIL:- " + Email + "\nWEBSITE:- " + Website;
            }
            set { }
        }

        [DataMember]
        public string Title { get; set; }

        [DataMember]
        public string AddressFor { get; set; }

        [DataMember]
        public int AddressForID { get; set; }

        [DataMember]
        public string AddressLine1 { get; set; }

        [DataMember]
        public string AddressLine2 { get; set; }

        [DataMember]
        public string City { get; set; }

        [DataMember]
        public string State { get; set; }

        [DataMember]
        public string Country { get; set; }

        [DataMember]
        public string Pincode { get; set; }

        [DataMember]
        public string Phone { get; set; }

        [DataMember]
        public string Fax { get; set; }

        [DataMember]
        public string Email { get; set; }

        [DataMember]
        public string Website { get; set; }

        [DataMember]
        public string AddressType { get; set; }
    }
}
