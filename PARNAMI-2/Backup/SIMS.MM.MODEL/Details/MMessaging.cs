﻿using System;
using System.Net;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;

namespace SIMS.MM.MODEL
{
    [DataContract]
    public class MMessaging : EntityModel
    {
        public MMessaging()
        {
            SentDate = DateTime.UtcNow.AddHours(5.5);
        }

        [DataMember]
        public int ID { get; set; }

        [DataMember]
        public string Sender { get; set; }

        [DataMember]
        public DateTime SentDate { get; set; }

        [DataMember]
        public string Subject { get; set; }

        [DataMember]
        public string Message { get; set; }

        [DataMember]
        public bool HasAttachment { get; set; }

        [DataMember]
        public bool IsFlag { get; set; }

        [DataMember]
        public bool IsStar { get; set; }

        bool _IsRead { get; set; }
        [DataMember]
        public bool IsRead {
            get { return _IsRead; }
            set { _IsRead = value; Notify("IsRead"); }
        }

        ObservableCollection<MMessageEmailer> _Emailers = new ObservableCollection<MMessageEmailer>();
        [DataMember]
        public ObservableCollection<MMessageEmailer> Emailers
        {
            get { return _Emailers; }
            set
            {
                _Emailers = value;
                Notify("Emailers");
            }
        }

        ObservableCollection<MMessageReceiver> _Receivers = new ObservableCollection<MMessageReceiver>();
        [DataMember]
        public ObservableCollection<MMessageReceiver> Receivers
        {
            get { return _Receivers; }
            set
            {
                _Receivers = value;
                Notify("Receivers");
            }
        }

        [DataMember]
        public ObservableCollection<string> EmailersName { get; set; }

        [DataMember]
        public ObservableCollection<string> ReceiversName { get; set; }

        [DataMember]
        public string Day
        {
            get
            {
                return SentDate.ToString("dddd, MMM dd, yyyy");
            }
            set { }
        }
    }





    [DataContract]
    public class MMessageReceiver : EntityModel
    {
        [DataMember]
        public int ID { get; set; }

        [DataMember]
        public int MessageID { get; set; }

        [DataMember]
        public string Receiver { get; set; }

        [DataMember]
        public bool IsRead { get; set; }

        [DataMember]
        public DateTime? ReadOn { get; set; }

        [DataMember]
        public bool IsDeleted { get; set; }

        [DataMember]
        public bool IsFlag { get; set; }

        [DataMember]
        public bool IsStar { get; set; }
    }






    [DataContract]
    public class MMessageEmailer : EntityModel
    {
        [DataMember]
        public int ID { get; set; }

        [DataMember]
        public int MessageID { get; set; }

        //[DataMember]
        //public int EmailID { get; set; }

        [DataMember]
        public string EmailAddress { get; set; }
    }






    [DataContract]
    public class MMessagingHeader : EntityModel
    {
        [DataMember]
        public int MsgID { get; set; }

        [DataMember]
        public int EmailID { get; set; }

        [DataMember]
        public int EmailerID { get; set; }

        [DataMember]
        public int MsgEmailerID { get; set; }

        [DataMember]
        public int MsgReceiverID { get; set; }

        [DataMember]
        public int ReceiverID { get; set; }

        [DataMember]
        public string Receiver { get; set; }

        [DataMember]
        public bool IsRead { get; set; }

        [DataMember]
        public DateTime ReadOn { get; set; }

        [DataMember]
        public bool IsDeleted { get; set; }

        [DataMember]
        public bool IsFlag { get; set; }

        [DataMember]
        public bool IsStar { get; set; }

        [DataMember]
        public string Sender { get; set; }

        [DataMember]
        public DateTime SentDate { get; set; }

        [DataMember]
        public string Subject { get; set; }

        [DataMember]
        public string Message { get; set; }

        [DataMember]
        public bool HasAttachment { get; set; }
    }
}
