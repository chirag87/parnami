﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.Collections.ObjectModel;

namespace SIMS.MM.MODEL
{
    [DataContract]
    public class MConsumption : EntityModel
    {
        public MConsumption()
        {
            ForDate = DateTime.UtcNow.AddHours(5.5);
            LastUpdatedOn = DateTime.UtcNow.AddHours(5.5);
            CreatedOn = DateTime.UtcNow.AddHours(5.5);
        }

        #region Events

        public event EventHandler SiteIDChanged;
        public void RaiseSiteIDChanged()
        {
            if (SiteIDChanged != null)
                SiteIDChanged(this, new EventArgs());
        }

        #endregion

        #region Model

        [DataMember]
        public int ID { get; set; }

        [DataMember]
        public string ReferenceNo { get; set; }

        public int _SiteID = new int();
        [DataMember]
        public int SiteID
        {
            get { return _SiteID; }
            set
            {
                _SiteID = value;
                Notify("SiteID");
                RaiseSiteIDChanged();
            }
        }

        [DataMember]
        public string Site { get; set; }

        [DataMember]
        public string Remarks { get; set; }

        [DataMember]
        public string ConsumedBy { get; set; }

        DateTime _ForDate;
        [DataMember]
        public DateTime ForDate
        {
            get
            {
                if (_ForDate < DateTime.UtcNow.AddHours(5.5))
                    return _ForDate;
                else
                    return DateTime.UtcNow.AddHours(5.5);
            }
            set
            {
                _ForDate = value;
                Notify("ForDate");
            }
        }

        DateTime _CreatedOn;
        [DataMember]
        public DateTime CreatedOn
        {
            get
            {
                if (_CreatedOn < DateTime.UtcNow.AddHours(5.5))
                    return _CreatedOn;
                else
                    return DateTime.UtcNow.AddHours(5.5);
            }
            set
            {
                _CreatedOn = value;
                Notify("CreatedOn");
            }
        }

        [DataMember]
        public string CreatedBy { get; set; }

        DateTime _LastUpdatedOn;
        [DataMember]
        public DateTime LastUpdatedOn
        {
            get
            {
                if (_LastUpdatedOn < DateTime.UtcNow.AddHours(5.5))
                    return _LastUpdatedOn;
                else
                    return DateTime.UtcNow.AddHours(5.5);
            }
            set
            {
                _LastUpdatedOn = value;
                Notify("LastUpdatedOn");
            }
        }

        [DataMember]
        public string LastUpdatedBy { get; set; }

        ObservableCollection<MConsumptionLine> _ConsumptionLines = new ObservableCollection<MConsumptionLine>();
        [DataMember]
        public ObservableCollection<MConsumptionLine> ConsumptionLines
        {
            get { return _ConsumptionLines; }
            set
            {
                _ConsumptionLines = value;
                Notify("ConsumptionLines");
            }
        }

        #endregion

        #region Methods

        public MConsumptionLine AddNewLine()
        {
            if (this.ConsumptionLines == null)
                this.ConsumptionLines = new ObservableCollection<MConsumptionLine>();
            var line = new MConsumptionLine();
            this.ConsumptionLines.Add(line);
            this.UpdateLineNumbers();
            return line;
        }

        public int UpdateLineNumbers()
        {
            if (this.ConsumptionLines == null) return 0;
            int i = 1;
            foreach (var item in this.ConsumptionLines)
            {
                item.LineID = i;
                i++;
            }
            return i - 1;
        }

        #endregion
    }

    [DataContract]
    public class MConsumptionLine : EntityModel
    {
        #region Events...

        public event EventHandler ItemIDChanged;
        public void RaiseItemIDChanged()
        {
            if (ItemIDChanged != null)
                ItemIDChanged(this, new EventArgs());
        }

        #endregion

        #region Model

        [DataMember]
        public int HeaderID { get; set; }

        int _LineID;
        [DataMember]
        public int LineID
        {
            get { return _LineID; }
            set
            {
                _LineID = value;
                Notify("LineID");
            }
        }

        int _itemID { get; set; }
        [DataMember]
        public int ItemID
        {
            get { return _itemID; }
            set
            {
                _itemID = value;
                Notify("ItemID");
                RaiseItemIDChanged();
            }
        }

        [DataMember]
        public string Item { get; set; }

        int _SizeID;
        [DataMember]
        public int SizeID
        {
            get { return _SizeID; }
            set
            {
                _SizeID = value;
                Notify("SizeID");
            }
        }

        [DataMember]
        public string Size { get; set; }

        int _BrandID;
        [DataMember]
        public int BrandID
        {
            get { return _BrandID; }
            set
            {
                _BrandID = value;
                Notify("BrandID");
            }
        }

        [DataMember]
        public string Brand { get; set; }

        [DataMember]
        public string LineRemarks { get; set; }

        [DataMember]
        public double Quantity { get; set; }

        #endregion
    }
}
