﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace SIMS.MM.MODEL
{
    [DataContract]
    public class MEmail : EntityModel
    {
        [DataMember]
        public int ID { get; set; }

        [DataMember]
        public string EmailAddress { get; set; }
    }
}
