﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.ComponentModel;

namespace SIMS.MM.MODEL
{
    [DataContract]
    public class MBillingLines : EntityModel
    {
        [DataMember]
        public int SrNo { get; set; }

        [DataMember]
        public int ID { get; set; }

        [DataMember]
        public int LineID { get; set; }

        [DataMember]
        public int BillingID { get; set; }

        [DataMember]
        public int? VendorID { get; set; }

        [DataMember]
        public string Vendor { get; set; }

        [DataMember]
        public string ReferenceNo { get; set; }

        [DataMember]
        public int TransactionDetailsID { get; set; }

        [DataMember]
        public string TransactionType { get; set; }

        [DataMember]
        public int TransactionID { get; set; }

        //[DataMember]
        //public DateTime? Date { get; set; }
        DateTime? _Date;
        [DataMember]
        public DateTime? Date
        {
            get
            {
                if (_Date < DateTime.UtcNow.AddHours(5.5))
                    return _Date;
                else
                    return DateTime.UtcNow.AddHours(5.5);
            }
            set
            {
                _Date = value;
                Notify("Date");
            }
        }

        [DataMember]
        public int ItemID { get; set; }

        [DataMember]
        public string Item { get; set; }

        [DataMember]
        public int? SizeID { get; set; }

        [DataMember]
        public string Size { get; set; }

        [DataMember]
        public int? BrandID { get; set; }

        [DataMember]
        public string Brand { get; set; }

        [DataMember]
        public double? SuggestedQty { get; set; }

        double? _ActualQty;
        [DataMember]
        public double? ActualQty
        {
            get { return _ActualQty; }
            set
            {
                _ActualQty = value;
                UpdateLinePrice();
                Notify("ActualQty");
            }
        }

        [DataMember]
        public double? SuggestedPrice { get; set; }

        double? _ActualPrice;
        [DataMember]
        public double? ActualPrice
        {
            get { return _ActualPrice; }
            set
            {
                _ActualPrice = value;
                UpdateLinePrice();
                Notify("ActualPrice");
            }
        }

        [DataMember]
        public double? LinePrice { get { return ActualQty * ActualPrice; } set { } }

        [DataMember]
        public string ChallanNo { get; set; }

        [DataMember]
        public string LineChallanNo { get; set; }

        [DataMember]
        public string LineRemarks { get; set; }

        public void UpdateLinePrice()
        {
            LinePrice = ActualQty * ActualPrice;
            Notify("LinePrice");
        }
    }
}
