﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace SIMS.MM.MODEL
{
    [DataContract]
    public class MContact : EntityModel
    {
        [DataMember]
        public int ID { get; set; }

        [DataMember]
        public string Title { get; set; }

        [DataMember]
        public string ContactFor { get; set; }

        [DataMember]
        public int ContactForID { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public string Mobile1 { get; set; }

        [DataMember]
        public string Mobile2 { get; set; }

        [DataMember]
        public string Email1 { get; set; }

        [DataMember]
        public string Email2 { get; set; }

        [DataMember]
        public string Fax { get; set; }

        [DataMember]
        public string Phone { get; set; }
    }
}
