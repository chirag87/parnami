﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;

namespace SIMS.MM.MODEL
{
    [DataContract]
    public class MMOReleaseHeader : EntityModel
    {
        [DataMember]
        public int ID { get; set; }

        [DataMember]
        public string SiteCode { get; set; }

        [DataMember]
        public int RID { get; set; }

        [DataMember]
        public int Year { get; set; }

        [DataMember]
        public int MOID { get; set; }

        [DataMember]
        public string MORefNo { get; set; }

        [DataMember]
        public string ReferenceNo { get; set; }

        [DataMember]
        public DateTime ReleasedOn { get; set; }

        [DataMember]
        public string ReleasedBy { get; set; }

        [DataMember]
        public DateTime? LastUpdatedOn { get; set; }

        [DataMember]
        public string LastUpdatedBy { get; set; }

        [DataMember]
        public int TransferringSiteID { get; set; }

        [DataMember]
        public string DisplayTransferringSite { get; set; }

        [DataMember]
        public int? RequestingSiteID { get; set; }

        [DataMember]
        public string DisplayRequestedBySite { get; set; }

        [DataMember]
        public int? TransitID { get; set; }

        [DataMember]
        public string Transporter { get; set; }

        [DataMember]
        public string MOStatus { get; set; }

        [DataMember]
        public string MOReleaseStatus { get; set; }

        [DataMember]
        public string MOReceiveStatus { get; set; }

        [DataMember]
        public string Remarks { get; set; }

        [DataMember]
        public bool? CanReceive { get; set; }

        [DataMember]
        public bool? CanRelease { get; set; }

        [DataMember]
        public String ChallanNo { get; set; }

        [DataMember]
        public string ConsumableType { get; set; }
    }
}
