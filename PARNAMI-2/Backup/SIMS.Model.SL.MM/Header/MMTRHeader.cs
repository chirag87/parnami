﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;

namespace SIMS.MM.MODEL
{
    [DataContract]
    public class MMTRHeader : EntityModel
    {
        [DataMember]
        public int ID { get; set; }

        [DataMember]
        public DateTime? Date { get; set; }

        [DataMember]
        public string RequestedBy { get; set; }

        [DataMember]
        [Display(AutoGenerateField = false)]
        public string TransferringSite { get; set; }

        [DataMember]
        [Display(Name = "TransferringSite")]
        public string DisplayTransferringSite { get; set; }

        [DataMember]
        [Display(AutoGenerateField = false)]
        public string RequestedBySite { get; set; }

        [DataMember]
        [Display(Name = "RequestedBySite")]
        public string DisplayRequestedBySite { get; set; }

        [DataMember]
        public bool IsApproved { get; set; }

        [DataMember]
        public string ApprovalStatus { get; set; }

        [DataMember]
        public string ReleaseStatus { get; set; }

        [DataMember]
        public string ReceiveStatus { get; set; }

        [DataMember]
        public string Remarks { get; set; }
    }
}
