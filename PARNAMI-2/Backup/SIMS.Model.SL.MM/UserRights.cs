﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace SIMS.MM.MODEL
{
    [DataContract]
    public class UserRights : EntityModel
    {
        public UserRights()
        {
            IsValid = false;
        }

        public void SetAllCans(bool value)
        {
            foreach (var prop in this.GetType().GetProperties())
            {
                if (prop.Name.StartsWith("Can") && prop.PropertyType == typeof(bool))
                {
                    prop.SetValue(this, value, null);
                }
            }
        }

        [DataMember]
        public string Status { get; set; }

        [DataMember]
        public string Key { get; set; }

        [DataMember]
        public string UserName { get; set; }

        [DataMember]
        public bool IsValid { get; set; }

        [DataMember]
        public string[] Roles { get; set; }

        #region Old Rights
        //[DataMember]
        //public bool CanManageTransporter { get; set; }

        //[DataMember]
        //public bool CanManageSites { get; set; }

        //[DataMember]
        //public bool CanManageVendors { get; set; }

        //[DataMember]
        //public bool CanManageItems { get; set; }

        //[DataMember]
        //public bool CanManageUsers { get; set; }

        //[DataMember]
        //public bool CanRaisePR { get; set; }

        //[DataMember]
        //public bool CanApprovePR { get; set; }

        //[DataMember]
        //public bool CanApprovePO { get; set; }

        //[DataMember]
        //public bool CanVerifyMI { get; set; }

        //[DataMember]
        //public bool CanRaisePO { get; set; }

        //[DataMember]
        //public bool CanRaiseTO { get; set; }

        //[DataMember]
        //public bool CanManageTO { get; set; }

        //[DataMember]
        //public bool CanEmailPR { get; set; }

        //[DataMember]
        //public bool CanReceive { get; set; }

        //[DataMember]
        //public bool CanApproveMTR { get; set; }

        //[DataMember]
        //public bool CanConsume { get; set; }

        //[DataMember]
        //public bool CanCreateMO { get; set; }

        //[DataMember]
        //public bool CanViewReports { get; set; }

        //[DataMember]
        //public bool CanAdmin { get; set; }

        //[DataMember]
        //public bool CanApproveBill { get; set; }

        //[DataMember]
        //public bool CanCreateBill { get; set; }

        //[DataMember]
        //public bool CanConvertToTO { get; set; }
        #endregion

        #region New Rights
        [DataMember]
        public bool MI_AllRights { get; set; }
        [DataMember]
        public bool MI_CanRaise { get; set; }
        [DataMember]
        public bool MI_CanViewAll { get; set; }
        [DataMember]
        public bool MI_CanEdit { get; set; }
        [DataMember]
        public bool MI_CanSubmitForVerify { get; set; }
        [DataMember]
        public bool MI_CanVerify { get; set; }
        [DataMember]
        public bool MI_CanSubmitForMIApprove { get; set; }
        [DataMember]
        public bool MI_CanApproveMI { get; set; }
        [DataMember]
        public bool MI_CanSubmitToPurchase { get; set; }
        [DataMember]
        public bool MI_CanConvertToTO { get; set; }
        [DataMember]
        public bool MI_CanSubmitForPOApprove { get; set; }
        [DataMember]
        public bool MI_CanApprovePO { get; set; }
        [DataMember]
        public bool MI_CanConvertToPO { get; set; }
        [DataMember]
        public bool MI_CanEmailMI { get; set; }

        [DataMember]
        public bool TO_AllRights { get; set; }
        [DataMember]
        public bool TO_CanRaise { get; set; }
        [DataMember]
        public bool TO_CanViewAll { get; set; }
        [DataMember]
        public bool TO_CanRelease { get; set; }
        [DataMember]
        public bool TO_CanReceive { get; set; }
        [DataMember]
        public bool TO_CanClose { get; set; }
        [DataMember]
        public bool TO_CanMergeTOs { get; set; }

        [DataMember]
        public bool PO_AllRights { get; set; }
        [DataMember]
        public bool PO_CanEmailPO { get; set; }
        [DataMember]
        public bool PO_CanViewAll { get; set; }

        [DataMember]
        public bool MRN_AllRights { get; set; }
        [DataMember]
        public bool MRN_CanRaise { get; set; }
        [DataMember]
        public bool MRN_CanViewAll { get; set; }

        [DataMember]
        public bool CP_AllRights { get; set; }
        [DataMember]
        public bool CP_CanRaise { get; set; }
        [DataMember]
        public bool CP_CanViewAll { get; set; }

        [DataMember]
        public bool Consumtion_AllRights { get; set; }
        [DataMember]
        public bool Consumtion_CanRaise { get; set; }
        [DataMember]
        public bool Consumtion_CanViewAll { get; set; }

        [DataMember]
        public bool Employee_Admin { get; set; }
        [DataMember]
        public bool Employee_Attendance { get; set; }

        [DataMember]
        public bool ReportingUser { get; set; }

        [DataMember]
        public bool MasterAdmin { get; set; }

        [DataMember]
        public bool Vendors_AllRights { get; set; }
        [DataMember]
        public bool Vendors_CanAddNew { get; set; }
        [DataMember]
        public bool Vendors_CanViewAll { get; set; }
        [DataMember]
        public bool Vendors_CanEdit { get; set; }

        [DataMember]
        public bool Company_AllRights { get; set; }
        [DataMember]
        public bool Company_CanAddNew { get; set; }
        [DataMember]
        public bool Company_CanViewAll { get; set; }
        [DataMember]
        public bool Company_CanEdit { get; set; }

        [DataMember]
        public bool Clients_AllRights { get; set; }
        [DataMember]
        public bool Clients_CanAddNew { get; set; }
        [DataMember]
        public bool Clients_CanViewAll { get; set; }
        [DataMember]
        public bool Clients_CanEdit { get; set; }

        [DataMember]
        public bool CanViewClientsandSitesMenu { get; set; }

        [DataMember]
        public bool Sites_AllRights { get; set; }
        [DataMember]
        public bool Sites_CanAddNew { get; set; }
        [DataMember]
        public bool Sites_CanViewAll { get; set; }
        [DataMember]
        public bool Sites_CanEdit { get; set; }
        [DataMember]
        public bool Sites_CanViewAllDisplaySites { get; set; }
        [DataMember]
        public int[] AllowedSiteIDs { get; set; }

        [DataMember]
        public bool Transporters_AllRights { get; set; }
        [DataMember]
        public bool Transporters_CanAddNew { get; set; }
        [DataMember]
        public bool Transporters_CanViewAll { get; set; }
        [DataMember]
        public bool Transporters_CanEdit { get; set; }

        [DataMember]
        public bool Items_AllRights { get; set; }
        [DataMember]
        public bool Items_CanAddNew { get; set; }
        [DataMember]
        public bool Items_CanViewAll { get; set; }
        [DataMember]
        public bool Items_CanEdit { get; set; }

        [DataMember]
        public bool ItemCategories_AllRights { get; set; }
        [DataMember]
        public bool ItemCategories_CanAddNew { get; set; }
        [DataMember]
        public bool ItemCategories_CanViewAll { get; set; }
        [DataMember]
        public bool ItemCategories_CanEdit { get; set; }

        [DataMember]
        public bool Sizes_AllRights { get; set; }
        [DataMember]
        public bool Sizes_CanAddNew { get; set; }
        [DataMember]
        public bool Sizes_CanViewAll { get; set; }
        [DataMember]
        public bool Sizes_CanEdit { get; set; }

        [DataMember]
        public bool CanViewItemSizeandMakeMenu { get; set; }

        [DataMember]
        public bool Makes_AllRights { get; set; }
        [DataMember]
        public bool Makes_CanAddNew { get; set; }
        [DataMember]
        public bool Makes_CanViewAll { get; set; }
        [DataMember]
        public bool Makes_CanEdit { get; set; }

        [DataMember]
        public bool MU_AllRights { get; set; }
        [DataMember]
        public bool MU_CanAddNew { get; set; }
        [DataMember]
        public bool MU_CanViewAll { get; set; }
        [DataMember]
        public bool MU_CanEdit { get; set; }

        [DataMember]
        public bool Pricings_AllRights { get; set; }
        [DataMember]
        public bool Pricings_CanAddNew { get; set; }
        [DataMember]
        public bool Pricings_CanViewAll { get; set; }
        [DataMember]
        public bool Pricings_CanEdit { get; set; }

        [DataMember]
        public bool HR { get; set; }
        [DataMember]
        public bool Users_CanAddNew { get; set; }
        [DataMember]
        public bool Users_CanManageRoles { get; set; }
        [DataMember]
        public bool Users_CanManageSites { get; set; }
        [DataMember]
        public bool Users_CanBlockUser { get; set; }
        #endregion
    }
}
