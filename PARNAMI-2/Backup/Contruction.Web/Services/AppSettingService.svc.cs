﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
//using Contruction.Web.Data;
using SIMS.MM.MODEL;
using SIMS.MM.BLL;
using SIMS.MM.BLL.Services;
using System.Web.Configuration;
using System.Web;
using System.Configuration;

namespace Contruction.Web.Services
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "AppSettingService" in code, svc and config file together.
    public class AppSettingService : IAppSettingService
    {
        IAppsettings dp = new AppSettingsService();
    
        public MAppSettings GetAllAppSettings()
        {
            var settings =  dp.GetAppSettings();

            var url = System.ServiceModel.OperationContext.Current.RequestContext.RequestMessage.Headers.To;
            var host = url.Host;
            if (!url.IsDefaultPort) host += ":" + url.Port;
            settings.LogoPath = "http://"+ host + "/Images/" + settings.LogoPath;
            settings.ServerPath = ConfigurationManager.AppSettings["serverpath"];
            return settings;
 	    //    MAppSettings settings = new MAppSettings();
        //    settings.HasLogo = true;
        //    settings.LogoPath = "http://localhost:3385/Images/Logo.png";
        //    settings.CompanyName = "D. K. Buildcon Pvt. Ltd.";
        //    settings.HasCompanyTitle1 = true;
        //    settings.CompanyTitle1 = "(Engineers & Contractors)";
        //    settings.HasCompanyTitle2 = true;
        //    settings.CompanyTitle2 = "(ISO 9001:2000, 14001:2004 & OHSAS)";
        //    settings.Address1 = "147, 1st Floor, Om Shubham Tower,";
        //    settings.Address2 = "Neelam Bata Road, N.I.T., Faridabada - 121001";
        //    settings.HasTinNo = true;
        //    settings.TinNo = "06841325968 Dt-08/05/2006";
        //    settings.HasPhone = true;
        //    settings.Phone = "0129-4064147, 0129-4164144-47";
        //    settings.HasFax = true;
        //    settings.FaxNo = "0129-4054147";
        //    settings.HasWebsite = true;
        //    settings.WebSite = "www.dkbuildcon.in";
        //    settings.HasEmail = true;
        //    settings.Email = "dkbuildcon@gmail.com";
        //    return settings;
        }
    }
}
