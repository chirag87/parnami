﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
//using Contruction.Web.Data;
using SIMS.MM.BLL;
using SIMS.MM.MODEL;
using SIMS.Core;
using System.Collections.ObjectModel;

namespace Contruction.Web.Services
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IMasterDataService" in both code and config file together.
    [ServiceContract]
    public interface IMasterDataService
    {
        [OperationContract]
        IEnumerable<MItem> GetAllItems();

        [OperationContract]
        IEnumerable<MDisplayItemCategories> GetDisplayItemCategories();

        [OperationContract]
        IEnumerable<MMeasurementUnit> GetAllMU();

        [OperationContract]
        IEnumerable<string> GetAllMUTypes();

        [OperationContract]
        IEnumerable<MItemCategory> GetAllItemCategories();

        [OperationContract]
        IEnumerable<string> GetAllItemNames();

        [OperationContract]
        IEnumerable<MVendor> GetAllVendors();

        [OperationContract]
        IEnumerable<MSite> GetAllSites();

        [OperationContract]
        IEnumerable<MTransporter> GetAllTransporters();

        [OperationContract]
        IEnumerable<MPriceMaster> GetAllPricing(string key, int? vendorid, int? siteid, int? itemid);

        [OperationContract]
        IEnumerable<MAddress> GetAllAddresses();

        [OperationContract]
        IEnumerable<MDocument> GetAllDocuments();

        [OperationContract]
        IEnumerable<MContact> GetAllContacts();

        [OperationContract]
        IEnumerable<MReceivingDetailsforMasters> GetAllReceivingsforMasters(int? vendorid, int? siteid);

        [OperationContract]
        IEnumerable<MAccounts> GetAllAccounts();

        [OperationContract]
        IEnumerable<MCompany> GetAllCompanies();

        [OperationContract]
        IEnumerable<MItemSize> GetAllItemSizes();

        [OperationContract]
        IEnumerable<MItemBrand> GetAllItemBrands();

        [OperationContract]
        IEnumerable<MClient> GetAllClients();



        [OperationContract]
        IEnumerable<MSafetyStock> GetSafetyStockbySiteID(int? siteid);

        [OperationContract]
        MVendor GetVendorbyID(int id);

        [OperationContract]
        MSite GetSitebyID(int id);

        [OperationContract]
        bool ValidateSiteName(string name);

        [OperationContract]
        bool ValidateSiteCode(string code);

        [OperationContract]
        MItem GetItembyID(int id);

        [OperationContract]
        MAddress GetAddressbyID(int addressid);

        [OperationContract]
        IEnumerable<MAddress> GetAddressbyForID(int ForID, string For);

        [OperationContract]
        IEnumerable<MItem> GetItemsByCategoryID(int _CategoryID);

        [OperationContract]
        MDocument GetDocumentbyID(int documentid);

        [OperationContract]
        IEnumerable<MDocument> GetDocumentsbyForID(int ForID, string For);

        [OperationContract]
        MContact GetContactbyID(int contactid);

        [OperationContract]
        IEnumerable<MContact> GetContactsbyForID(int ForID, string For);

        [OperationContract]
        IEnumerable<MAccounts> GetAccountsbyForID(int ForID, string For);

        [OperationContract]
        MAccounts GetAccountsbyID(int _AcountsID);

        [OperationContract]
        List<MStockReport> GetStockReport(DateTime? sdate, DateTime? edate, int? siteid, int? itemid, int? sizeid, int? brandid);

        [OperationContract]
        IEnumerable<MItemCategory> GetItemParents(int itemID);

        [OperationContract]
        IEnumerable<MPriceMaster> GetAllPricesByISV(int vendorid, int? siteid, int itemid);

        [OperationContract]
        MCompany GetCompanyByID(int ID);





        [OperationContract]
        void CreateNewItem(MItem NewItem, string ItemCategory);

        [OperationContract]
        MItem CreateItem(MItem newItem);

        [OperationContract]
        void CreateMU(MMeasurementUnit newMU);

        [OperationContract]
        void CreateNewSite(MSite NewSite);

        [OperationContract]
        void CreateNewVendor(MVendor NewVendor);

        [OperationContract]
        void CreateNewPricing(IEnumerable<MPriceMaster> pricemasterlist);

        [OperationContract]
        void CreateNewTransporter(MTransporter NewTransporter);

        [OperationContract]
        MItemCategory CreateNewItemCategory(MItemCategory _newItemCategory);

        [OperationContract]
        MAddress CreateNewAddress(MAddress NewAddress);

        [OperationContract]
        MContact CreateNewContact(MContact NewContact);

        [OperationContract]
        MAccounts CreateNewAccount(MAccounts _NewAccount);

        [OperationContract]
        string NewPricing(MPriceMaster _NewPrice);

        [OperationContract]
        MCompany CreateNewCompany(MCompany NewCompany);

        [OperationContract]
        MItemSize CreateNewItemSize(MItemSize NewSize);

        [OperationContract]
        MItemBrand CreateNewItemBrand(MItemBrand NewBrand);

        [OperationContract]
        MClient CreateNewClient(MClient newClient);






        [OperationContract]
        void DeleteItem(MItem DeletedItem);

        [OperationContract]
        void DeleteSite(MSite DeletedSite);

        [OperationContract]
        void DeleteVendor(MVendor DeletedVendor);

        [OperationContract]
        void DeleteTransporter(MTransporter DeletedTransporter);

        [OperationContract]
        void DeleteAddress(MAddress DeletedAddress);

        [OperationContract]
        void DeleteDocument(MDocument deletedDocument);

        [OperationContract]
        void DeleteContact(int id);

        [OperationContract]
        void DeleteAccount(int _AccountID);

        [OperationContract]
        void DeleteCompany(int ID);





        [OperationContract]
        void UpdateSites(IEnumerable<MSite> UpdatedSite);

        [OperationContract]
        MSite UpdateSite(MSite UpdatedSite);

        [OperationContract]
        MClient UpdateClient(MClient updatedClient);

        [OperationContract]
        void UpdateVendors(IEnumerable<MVendor> UpdatedVendor);

        [OperationContract]
        MVendor UpdateVendor(MVendor UpdatedVendor);

        [OperationContract]
        MItem UpdateItem(MItem UpdatedItem);

        [OperationContract]
        void UpdateTransporter(IEnumerable<MTransporter> UpdatedTransporter);

        [OperationContract]
        void UpdateParentForItemCategory(int ID, int NewPID);

        [OperationContract]
        MItemCategory UpdateItemCategory(MItemCategory _updatedItemCategory);

        [OperationContract]
        MAddress UpdateAddress(MAddress UpdatedAddress);

        [OperationContract]
        void SaveDocument(IEnumerable<MDocument> newDocument);

        [OperationContract]
        MDocument SaveDocuments(string FileName, string For, int? ForID, string DocType);

        [OperationContract]
        void SaveImage(string ImageName, int ItemID);

        [OperationContract]
        MContact UpdateContact(MContact UpdatedContact);

        [OperationContract]
        MAccounts UpdateAccount(MAccounts _updatedAccount);

        [OperationContract]
        void UpdatePrice(MPriceMaster _updatedPrice);

        [OperationContract]
        MCompany UpdateCompany(MCompany company);

        [OperationContract]
        MItemSize UpdateItemSize(MItemSize updatedSize);

        [OperationContract]
        MItemBrand UpdateItemBrand(MItemBrand updatedBrand);

        [OperationContract]
        void UpdateItemBrands(IEnumerable<MItemBrand> updatedBrands);

        [OperationContract]
        void UpdateItemSizes(IEnumerable<MItemSize> updatedSizes);

        [OperationContract]
        IEnumerable<MItemCategory> GetItemCategoryLeaves(int CategoryID);   //CanHaveItem property must be rue For Given CategoryID

        [OperationContract]
        IEnumerable<MItem> GetVariationsforLeaf(int CategoryID);    //IsItem Property must be true For Given CategoryID 

        [OperationContract]
        bool CheckforDuplicateItemCode(string code);


        //Methods Added by Manish Soni Added On 5/14/14
        [OperationContract]
        MItemPagenatedData GetPagenatedItemsData(string LoginUser, int? PageSize, int? PageIndex);

    }
}
