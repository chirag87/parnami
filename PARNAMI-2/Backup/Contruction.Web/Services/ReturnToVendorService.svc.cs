﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using SIMS.MM.BLL;
using SIMS.Core;
using SIMS.MM.MODEL;

namespace Contruction.Web.Services
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "ReturnToVendorService" in code, svc and config file together.
    public class ReturnToVendorService : IReturnToVendorService
    {
        IReturnToVendorManager manager = new ReturnToVendorManager();

        public PaginatedData<MReturnToVendor> GetAllReturnToVendors(string username, int? pageindex, int? pagesize, string key, int? vendorid, int? siteid)
        {
            var showAll = LoginService.CanShowAllSites(username);
            var data = (PaginatedData<MReturnToVendor>)manager.GetAllReturnToVendors(username, pageindex, pagesize, key, vendorid, siteid, showAll);
            return data;
        }

        public MReturnToVendor GetRTVByID(int ID)
        {
            var data = manager.GetRTVByID(ID);
            return data;
        }

        public MReturnToVendor CreateRTV(MReturnToVendor newRTV, string username)
        {
            var data = manager.CreateRTV(newRTV, username);
            return data;
        }

        public MReturnToVendor UpdateRTV(MReturnToVendor rtv)
        {
            var data = manager.UpdateRTV(rtv);
            return data;
        }

        public MReturnToVendor UpdateRTVReceivingInfo(MReturnToVendor rtv)
        {
            var data = manager.UpdateRTVReceivingInfo(rtv);
            return data;
        }

        public void DeleteRTVByID(int ID)
        {
            manager.DeleteRTVByID(ID);
        }

        public void DeleteRTV(MReturnToVendor rtv)
        {
            manager.DeleteRTV(rtv);
        }

        public MReturnToVendor ChangeConfirmationStatus(int RTVID, bool status)
        {
            var data = manager.ChangeConfirmationStatus(RTVID, status);
            return data;
        }
    }
}
