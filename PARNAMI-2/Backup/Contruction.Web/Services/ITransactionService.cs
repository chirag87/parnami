﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using SIMS.Core;
using SIMS.MM.MODEL;

namespace Contruction.Web
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "ITransactionService" in both code and config file together.
    [ServiceContract]
    public interface ITransactionService
    {
        [OperationContract]
        PaginatedData<MGRN> GetAllGRNs(string username, int? pageindex, int? pagesize, string key, int? vendorid, int? siteid);

        [OperationContract]
        PaginatedData<MCashPurchase> GetAllCashPurchases(string username, int? pageindex, int? pagesize, string key, int? vendorid, int? siteid);

        [OperationContract]
        PaginatedData<MConsumption> GetAllConsumptions(string username, int? pageindex, int? pagesize, string key, int? siteid);

        [OperationContract]
        MGRN GetGRNByID(int id);

        [OperationContract]
        MCashPurchase GetCashPurchaseByID(int id);

        [OperationContract]
        MConsumption GetConsumptionByID(int id);

        [OperationContract]
        MGRN CreateNewGRN(MGRN NewGRN, string username);

        [OperationContract]
        MCashPurchase CreateNewCashPurchase(MCashPurchase newCP, string username);

        [OperationContract]
        MConsumption CreateNewConsumption(MConsumption newCon, string username);

        [OperationContract]
        IEnumerable<PendingPOInfo> GetPendingPOInfos(int vendorid, int siteid, int itemid, int? sizeid, int? brandid);

        [OperationContract]
        MGRN ConvertfromPO(int poid);

        [OperationContract]
        IEnumerable<int> GetAllowedItemIDs(int vendorid, int siteid);
    }
}
