﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SIMS.MM.MODEL;
using SIMS.MM.DAL;

namespace SIMS.MM.BLL
{
    public class BillingManager : IBillingManager
    {
        parnamidb1Entities db = new parnamidb1Entities();
        BillingDataProvider provider = new BillingDataProvider();

        public MBilling CreateNewBill(MBilling _NewBill)
        {
            _NewBill.ID = db.GetMaxBillingID() + 1;
            //var line = _NewBill.BillingLines.Single(x => x.BillingID == _NewBill.ID);
            if (_NewBill.BillingVerifications != null)
            {
                _NewBill.BillingVerifications.ToList().Last().ID = db.GetMaxBillingVerificationID() + 1;
                _NewBill.BillingVerifications.ToList().Last().VerificationCounter = db.GetBillingVerificationCounter(_NewBill.ID);
            }
            var bill = ConvertToBilling(_NewBill);
            db.Billings.AddObject(bill);
            db.SaveChanges();
            return provider.GetBillbyId(bill.ID);
        }

        public Billing ConvertToBilling(MBilling _NewBill)
        {
            Billing billing = new Billing()
            {
                ID = _NewBill.ID,
                ApprovedBy = _NewBill.ApprovedBy,
                ApprovedOn = _NewBill.ApprovedOn,
                BillDate = _NewBill.BillDate,
                BillNo = _NewBill.BillNo,
                Deductions = _NewBill.Deductions,
                DefaultPaymentDays = _NewBill.DefaultPaymentDays,
                PANNo = _NewBill.PANNo,
                PaymentNotBefore = _NewBill.PaymentNotBefore,
                Remarks = _NewBill.Remarks,
                TotalAmount = _NewBill.TotalAmount,
                VendorID = _NewBill.VendorID,
                SiteID = _NewBill.SiteID,
                GrossAmount = _NewBill.GrossAmount,
                IsApproved = _NewBill.IsApproved,
                VAT = _NewBill.VATPercent,
                Cartage = _NewBill.Cartage,
                NetAmount = _NewBill.NetAmount,
                Surcharge = _NewBill.SurchargePercent
            };
            if (_NewBill.BillingLines != null)
            {
                _NewBill.BillingLines.ToList()
                    .ForEach(x => billing.BillingDetails.Add(ConvertToBillingDetails(x)));
            }
            if (_NewBill.BillingVerifications != null)
            {
                var data = db.BillingVerifications.Where(x => x.BillingID == _NewBill.ID);
                if (data.Count() == 0)
                {
                    //add for new
                    _NewBill.BillingVerifications.ToList()
                        .ForEach(x => billing.BillingVerifications.Add(ConvertToBillingVerification(x)));
                }
                else
                {
                    //add new as well as update counter
                    _NewBill.BillingVerifications.ToList()
                        .ForEach(x => billing.BillingVerifications.Add(ConvertToBillingVerification(x)));
                }
            }
            return billing;
        }

        public BillingVerification ConvertToBillingVerification(MBillingVerified x)
        {
            BillingVerification verify = new BillingVerification()
            {
                ID = x.ID,
                BillingID = x.BillingID,
                //VerificationCounter = x.VerificationCounter,
                VerifiedBy=x.VerifiedBy,
                VerifiedOn=x.VerifiedOn,
            };
            return verify;
        }

        public BillingDetail ConvertToBillingDetails(MBillingLines _details)
        {
            BillingDetail detail = new BillingDetail()
            {
                ID = _details.ID,
                LineID = _details.LineID,
                ItemID = _details.ItemID,
                SizeID = _details.SizeID,
                BrandID = _details.BrandID,
                BillingID = _details.BillingID,
                LinePrice = _details.LinePrice.Value,
                LineRemarks = _details.LineRemarks,
                SuggestedPrice = _details.SuggestedPrice,
                SuggestedQty = _details.SuggestedQty,
                ActualPrice = _details.ActualPrice,
                ActualQty = _details.ActualQty,
                GRNLinesID = _details.TransactionDetailsID,
                GRNID = _details.TransactionID,
                Date = _details.Date,
                ChallanNo = _details.ChallanNo,
                LineChallanNo = _details.LineChallanNo,
                ReferenceNo = _details.ReferenceNo,
            };
            return detail;
        }

        public MBilling UpdateBill(MBilling _Bill)
        {
            //if (_Bill.BillingVerifications != null)
            //{
            //    _Bill.BillingVerifications.ToList().Last().ID = db.GetMaxBillingVerificationID() + 1;
            //    _Bill.BillingVerifications.ToList().Last().VerificationCounter = db.GetBillingVerificationCounter(_Bill.ID);
            //}
            var bill = ConvertToBilling(_Bill);
            db.Billings.Attach(bill);
            db.ObjectStateManager.ChangeObjectState(bill, System.Data.EntityState.Modified);
            db.SaveChanges();
            return _Bill;
        }


        public void VerifyBill(int BillID, string VerifiedBy)
        {
            var time = DateTime.UtcNow.AddHours(5.5);
            var bill = db.Billings.Single(x => x.ID == BillID);
            bill.BillingVerifications.Add(new BillingVerification()
            {
                ID = db.GetMaxBillingVerificationID() + 1,
                BillingID = BillID,
                VerifiedBy = VerifiedBy,
                VerifiedOn = time
            });
            db.SaveChanges();
        }
    }
}
