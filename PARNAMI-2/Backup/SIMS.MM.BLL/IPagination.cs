﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SIMS.MM.BLL
{
    public interface IPaginated
    {
        int TotalPages {get; set;}
        int CurrentPage { get; set; }
        int TotalEntities { get; set; }
        int PageSize { get; set; }
        int Count { get;}
    }

    public interface IPaginated<T> : IPaginated
    {
        IEnumerable<T> Data { get; set; }
    }

    public class PaginatedData<T> : IPaginated<T>
    {

        public IEnumerable<T> Data
        {
            get;
            set;
        }

        public int TotalPages
        {
            get;
            set;
        }

        public int CurrentPage
        {
            get;
            set;
        }

        public int TotalEntities
        {
            get;
            set;
        }

        public int PageSize
        {
            get;
            set;
        }


        public int Count
        {
            get { return Data.Count(); }
        }
    }

    public static class PaginatedExtensions
    {
        public static IPaginated<T> ToPaginate<T>(this IEnumerable<T> qry, int pageindex, int pagesize)
        {
            if (pagesize == 0) throw new Exception("Page Size has to be greater than zero!");
            PaginatedData<T> data = new PaginatedData<T>();
            data.CurrentPage = pageindex;
            data.PageSize = pagesize;
            data.TotalPages = (int)Math.Ceiling(qry.Count()*1.0 / pagesize);
            data.TotalEntities = qry.Count();
            int toskip = pagesize*pageindex;
            data.Data = qry.Skip(toskip).Take(pagesize);
            return data;
        }

        public static IPaginated<T> ToPaginate<T>(this IEnumerable<T> qry, int? pageindex, int? pagesize)
        {
            pageindex = pageindex ?? 0;
            pagesize = pagesize ?? 20;
            return qry.ToPaginate(pageindex.Value, pagesize.Value);
        }

        public static IPaginated<T> ToPaginate<T>(this IQueryable<T> qry, int pageindex, int pagesize)
        {
            if (pagesize == 0) throw new Exception("Page Size has to be greater than zero!");
            PaginatedData<T> data = new PaginatedData<T>();
            data.CurrentPage = pageindex;
            data.PageSize = pagesize;
            data.TotalPages = (int)Math.Ceiling(qry.Count() * 1.0 / pagesize);
            data.TotalEntities = qry.Count();
            int toskip = pagesize * pageindex;
            data.Data = qry.Skip(toskip).Take(pagesize);
            return data;
        }

        public static IPaginated<T> ToPaginate<T>(this IQueryable<T> qry, int? pageindex, int? pagesize)
        {
            pageindex = pageindex ?? 0;
            pagesize = pagesize ?? 20;
            return qry.ToPaginate(pageindex.Value, pagesize.Value);
        }

        public static IPaginated<TResult> Select<T,TResult>(this IPaginated<T> page,Func<T,TResult> selector)
        {
            PaginatedData<TResult> data = new PaginatedData<TResult>();
            data.CurrentPage = page.CurrentPage;
            data.PageSize = page.PageSize;
            data.TotalPages = page.TotalPages;
            data.TotalEntities = page.TotalEntities;
            data.Data = page.Data.Select(selector);
            return data;
        }

    }
}
