﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace SIMS.MM.BLL.ReportingModels
{
    [DataContract]
    public class MMTRReport
    {
        [DataMember]
        public int MTRID { get; set; }

        [DataMember]
        public int RefNo { get; set; }

        [DataMember]
        public DateTime Date { get; set; }

        [DataMember]
        public string TransferringSite { get; set; }

        [DataMember]
        public string ReceivingSite { get; set; }

        [DataMember]
        public string item { get; set; }

        [DataMember]
        public double RequestingQty { get; set; }

        [DataMember]
        public double ApprovedQty { get; set; }

        [DataMember]
        public double PendingQty { get; set; }

        [DataMember]
        public string MU { get; set; }

        [DataMember]
        public string Remarks { get; set; }
    }
}
