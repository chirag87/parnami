﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SIMS.MM.BLL
{
    public static class GetImage
    {
        public static Uri GetImageUri(string ImagePath)
        {
            var url = System.ServiceModel.OperationContext.Current.RequestContext.RequestMessage.Headers.To;
            var host = url.Host;
            if (!url.IsDefaultPort) host += ":" + url.Port;
            ImagePath = "http://" + host + "/Upload/" + ImagePath;
            Uri uri = new Uri(ImagePath);
            return uri;
        }
    }
}
