﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SIMS.MM.DAL;
using SIMS.MM.MODEL;
using System.Collections.ObjectModel;

namespace SIMS.MM.BLL
{
    public class TransactionManager : ITransactionManager
    {
        parnamidb1Entities db = new parnamidb1Entities();
        MMReportsDBDataContext db1 = new MMReportsDBDataContext();
        TransactionDataProvider provider = new TransactionDataProvider();
        PurchasingDataProvider PurchasingProvider = new PurchasingDataProvider();

        public MGRN CreateNewGRN(MGRN NewGRN, string username)
        {
            NewGRN.ID = db.GetMaxGRNID() + 1;
            NewGRN.CreatedBy = username;
            NewGRN.LastUpdatedBy = username;
            NewGRN.Requester = username;
            NewGRN.LastUpdatedOn = DateTime.UtcNow.AddHours(5.5);
            NewGRN.CreatedOn = DateTime.UtcNow.AddHours(5.5);
            
            var rec = ConvertToGRN(NewGRN);

            rec.Requester = username;
            rec.Year = DateTime.UtcNow.AddHours(5.5).Year;
            rec.SiteCode = db.GetSiteCode(rec.BUID);
            rec.RID = db.GetMaxGRNRID(rec.Year, rec.BUID) + 1;
            rec.ReferenceNo = "MRN/" + rec.Year + "/S-" + rec.SiteCode + "/" + rec.RID;

            db.GRNs.AddObject(rec);
            db.SaveChanges();

            return provider.GetGRNByID(rec.ID);
        }

        public MCashPurchase CreateNewCashPurchase(MCashPurchase newCP, string username)
        {
            newCP.ID = db.GetMaxCPID() + 1;
            newCP.CreatedBy = username;
            newCP.LastUpdatedBy = username;
            if (String.IsNullOrWhiteSpace(newCP.Requester)) newCP.Requester = username;
            newCP.LastUpdatedOn = DateTime.UtcNow.AddHours(5.5);
            newCP.CreatedOn = DateTime.UtcNow.AddHours(5.5);

            var rec = ConvertToCashPurchase(newCP);

            rec.Requester = username;
            rec.Year = DateTime.UtcNow.AddHours(5.5).Year;
            rec.SiteCode = db.GetSiteCode(rec.BUID);//???
            rec.RID = db.GetMaxCashPurchaseRID(rec.Year, rec.BUID) + 1;
            rec.ReferenceNo = "Cash/" + rec.Year + "/S-" + rec.SiteCode + "/" + rec.RID;

            db.CashPurchases.AddObject(rec);
            db.SaveChanges();

            return provider.GetCashPurchaseByID(rec.ID);
        }

        public MConsumption CreateNewConsumption(MConsumption newCon, string username)
        {
            newCon.ID = db.GetMaxConsumptionID() + 1;
            newCon.LastUpdatedOn = DateTime.UtcNow.AddHours(5.5);
            newCon.CreatedBy = username;
            newCon.LastUpdatedBy = username;

            var rec = ConvertToConsumption(newCon);

            rec.CreatedOn = DateTime.UtcNow.AddHours(5.5);
            rec.Year = DateTime.UtcNow.AddHours(5.5).Year;
            rec.SiteCode = db.GetSiteCode(rec.BUID);//???
            rec.RID = db.GetMaxConsumptionRID(rec.Year, rec.BUID) + 1;
            rec.ReferenceNo = "Consumption/" + rec.Year + "/S-" + rec.SiteCode + "/" + rec.RID;

            db.Consumptions.AddObject(rec);
            db.SaveChanges();

            return provider.GetConsumptionByID(rec.ID);
        }

        public MGRN ConvertfromPO(int poid)
        {
            var po = PurchasingProvider.GetPObyId(poid);

            MGRN rec = new MGRN()
            {
                VendorID = po.VendorID,
                Vendor = po.Vendor,
                SiteID = po.SiteID,
                Site = po.Site,
                ForDate = po.RaisedOn,
            };
            if (po.PRLines != null)
            {
                foreach (var item in po.PRLines)
                {
                    rec.GRNLines.Add(ConvertFromPRLinesToGRNLines(item, poid));
                }
            }
            return rec;
        }

        #region Convertion Methods...

        #region GRN

        public GRN ConvertToGRN(MGRN data, bool HeaderOnly = false)
        {
            GRN mrec = new GRN()
            {
                ReferenceNo = data.ReferenceNo,
                ID = data.ID,
                BUID = data.SiteID,
                VendorID = data.VendorID,
                ChallanNo = data.ChallanNo,
                CreatedBy = data.CreatedBy,
                CreatedOn = data.CreatedOn,
                InvoiceNo = data.InvoiceNo,
                LastUpdatedOn = data.LastUpdatedOn,
                LastUpdatedBy = data.LastUpdatedBy,
                ForDate = data.ForDate,
                Requester = data.Requester,
                Remarks = data.Remarks,
                VehicleDetails = data.VehicleDetails
            };
            if (data.GRNLines != null)
            {
                data.GRNLines.ToList()
                    .ForEach(x => mrec.GRNLines.Add(ConvertToGRNLines(x)));
            }
            return mrec;
        }

        public GRNLine ConvertToGRNLines(MGRNLine x)
        {
            GRNLine mrline = new GRNLine()
            {
                ID = x.LineID,
                ItemID = x.ItemID,
                SizeID = x.SizeID,
                BrandID = x.BrandID,
                POID = x.POID,
                PRID = x.PRID,
                PRLineID = x.PRLineID,
                HeaderID = x.HeaderID,
                Remarks = x.LineRemarks,
                Qty = x.Quantity,
                ConsumableType = x.ConsumableType,
                TruckNo = x.TruckNo,
                ChallanNo = x.LineChallanNo,
                DiscountType = x.DiscountType,
                Discount = x.Discount,
                //TotalLinePrice = x.TotalLinePrice,
                TotalLinePrice = x.LinePrice,
                UnitPrice = x.UnitPrice,
                LinePrice = x.LinePrice,
                EarlierReceivedQty = x.EarlierReceivedQty,
                OrderedQty = x.OrderedQty
            };
            if (x.TruckParameters != null)
            {
                TruckMeasurement tm = new TruckMeasurement();
                tm.Lth = x.TruckParameters.Length;
                tm.Bth = x.TruckParameters.Breadth;
                tm.Ht = x.TruckParameters.Height;
                mrline.TruckMeasurement = tm;
            }
            return mrline;
        }

        #endregion

        #region Cash Purchase

        public CashPurchas ConvertToCashPurchase(MCashPurchase data)
        {
            CashPurchas mrec = new CashPurchas()
            {
                ReferenceNo = data.ReferenceNo,
                ID = data.ID,
                BUID = data.SiteID,
                VendorID = data.VendorID,
                ChallanNo = data.ChallanNo,
                CreatedBy = data.CreatedBy,
                CreatedOn = data.CreatedOn,
                InvoiceNo = data.InvoiceNo,
                LastUpdatedOn = data.LastUpdatedOn,
                LastUpdatedBy = data.LastUpdatedBy,
                ForDate = data.ForDate,
                Requester = data.Requester,
                Remarks = data.Remarks,
                Freight = data.Freight,
                TAX = data.TAX,
                GrossAmount = data.GrossTotalofLines,
                TotalAmount = data.TotalLinePrice
            };
            if (data.CashPurchaseLines != null)
            {
                data.CashPurchaseLines.ToList()
                    .ForEach(x => mrec.CashPurchaseLines.Add(ConvertToCashPurchaseLines(x)));
            }
            return mrec;
        }

        public CashPurchaseLine ConvertToCashPurchaseLines(MCashPurchaseLine x)
        {
            CashPurchaseLine line = new CashPurchaseLine()
            {
                ID = x.LineID,
                ItemID = x.ItemID,
                SizeID = x.SizeID,
                BrandID = x.BrandID,
                HeaderID = x.HeaderID,
                Remarks = x.LineRemarks,
                Qty = x.Quantity,
                ConsumableType = x.ConsumableType,
                TruckNo = x.TruckNo,
                ChallanNo = x.LineChallanNo,
                UnitPrice = x.UnitPrice,
                LinePrice = x.LinePrice
            };
            if (x.TruckParameters != null)
            {
                TruckMeasurement tm = new TruckMeasurement();
                tm.Lth = x.TruckParameters.Length;
                tm.Bth = x.TruckParameters.Breadth;
                tm.Ht = x.TruckParameters.Height;
                line.TruckMeasurement = tm;
            }
            return line;
        }

        #endregion

        #region Consumption

        public Consumption ConvertToConsumption(MConsumption data)
        {
            Consumption mrec = new Consumption()
            {
                ReferenceNo = data.ReferenceNo,
                ID = data.ID,
                BUID = data.SiteID,
                CreatedBy = data.CreatedBy,
                CreatedOn = data.CreatedOn,
                LastUpdatedOn = data.LastUpdatedOn,
                LastUpdatedBy = data.LastUpdatedBy,
                ForDate = data.ForDate,
                ConsumedBy = data.ConsumedBy,
                Remarks = data.Remarks
            };
            if (data.ConsumptionLines != null)
            {
                data.ConsumptionLines.ToList()
                    .ForEach(x => mrec.ConsumptionLines.Add(ConvertToConsumptionLines(x)));
            }
            return mrec;
        }

        public ConsumptionLine ConvertToConsumptionLines(MConsumptionLine x)
        {
            ConsumptionLine line = new ConsumptionLine()
            {
                ID = x.LineID,
                ItemID = x.ItemID,
                SizeID = x.SizeID,
                BrandID = x.BrandID,
                HeaderID = x.HeaderID,
                Remarks = x.LineRemarks,
                Qty = x.Quantity
            };
            if (line.SizeID == 0) line.SizeID = null;
            if (line.BrandID == 0) line.BrandID = null;
            return line;

        }

        #endregion

        public MGRNLine ConvertFromPRLinesToGRNLines(MPRLine lines, int poid)
        {
            MGRNLine line = new MGRNLine()
            {
                LineID = lines.LineID,
                ItemID = lines.ItemID,
                Item = lines.Item,
                LinePrice = lines.LinePrice,
                OrderedQty = lines.UnavailableQty,
                EarlierReceivedQty = 0,
                ConsumableType = lines.ConsumableType,
                UnitPrice = lines.UnitPrice,
                PRID = lines.PRID,
                PRLineID = lines.ID,
                POID = poid,
            };
            if (lines.SizeID.HasValue && lines.SizeID != 0)
            {
                line.SizeID = lines.SizeID.Value;
                line.Size = lines.Size;
            }
            if (lines.BrandID.HasValue && lines.BrandID != 0)
            {
                line.BrandID = lines.BrandID.Value;
                line.Brand = lines.Brand;
            }
            if (poid != 0)
                line.PORefNo = db.POs.SingleOrDefault(x => x.ID == poid).Display ?? "";
            if (lines.TotalReceivedQty.HasValue) line.EarlierReceivedQty = lines.TotalReceivedQty.Value;
            return line;
        }

        #endregion
    }
}
