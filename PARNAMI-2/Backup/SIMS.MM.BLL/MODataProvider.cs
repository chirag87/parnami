﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SIMS.MM.BLL
{
    using DAL;
    using SIMS.MM.MODEL;
    using System.Collections.ObjectModel;
    using SIMS.Core;

    public class MODataProvider : IMODataProvider
    {
        parnamidb1Entities db = new parnamidb1Entities();
        MMReportsDBDataContext db1 = new MMReportsDBDataContext();

        //public IPaginated<MMOHeader> GetMOHeaders(string username, int? pageindex, int? pagesize, string Key, int? vendorid, int? sideid, int? sideid2, bool showall = true)
        //{
        //    var qry= db.MTRs
        //        .FilterBySite(sideid,sideid2)
        //        .OrderByDescending(x => x.ID)
        //        .AsQueryable();
        //    if (!showall)
        //    {
        //        var ids = db.GetAllowedSiteIDs(username, showall);
        //        qry = qry.Where(x => ids.Contains(x.TransferringSiteID) 
        //            || ids.Contains(x.RequestedSiteID));

        //    }
        //     return qry.ToList()
        //        .ToPaginate(pageindex, pagesize)                
        //        .Select(x => new MMOHeader()
        //        {
        //            Date=x.TransferBefore,
        //            ID=x.ID,          
        //            DisplayRequestedBySite=x.BUs.DisplaySite,
        //            DisplayTransferringSite=x.BUs1.DisplaySite,
        //            Remarks=x.Remarks,
        //            RequestedBy=x.RequestedBy,
        //            RequestedBySite=x.BUs.Name,
        //            TransferringSite=x.BUs1.Name,                    
        //            CanRelease=x.MTRLines.Any(y=>y.RequiredQty> y.ReleasedQty)&&!x.IsClosed,                    
        //            CanReceive = x.MTRLines.Any(y => y.InTransitQty>0&&!x.IsClosed),
        //            IsClosed=x.IsClosed,
        //            Status=x.ClosedStatus,
        //            ClosedBy=x.ClosedBy,
        //            ReferenceNo=x.ReferenceNo,      
        //            //Transporter=x.TransporterName
        //        });

        //}

        public IPaginated<MMOHeader> GetMOHeadersNew(string username, int? pageindex, int? pagesize, string Key, int? transiteid, int? reqsiteid, string ConType, bool? IsClosed, bool showall = true)//Using Table Valued Funtion Functions
        {
            var MOs = db1.GetMOHeaders(username, Key, transiteid, reqsiteid, ConType, IsClosed, showall);
            var data = MOs.OrderByDescending(x => x.ID)
                .ToPaginate(pageindex, pagesize)
                .Select(x => new MMOHeader()
                {
                    Date = x.RaisedOn,
                    ID = x.ID,
                    DisplayRequestedBySite = x.DisplayReqSite,
                    DisplayTransferringSite = x.DisplayTransSite,
                    Remarks = x.Remarks,
                    RequestedBy = x.RequestedBy,
                    CanRelease = x.CanRelease.Value,
                    CanReceive = x.CanRecieve.Value,
                    IsClosed = x.IsClosed,
                    Status = x.Status,
                    ClosedBy = x.ClosedBy,
                    ReferenceNo = x.ReferenceNo,
                    ChallanNOs = x.ChallanNOs,
                    ConsumableType = x.ConsumableType,
                    LastReleasingDate = x.LastReleasingDate
                });
            return data;
        }

        public IPaginated<MMOReleaseHeader> GetMOReleaseHeaders(string username, int? pageindex, int? pagesize, string Key, int? transiteid, int? reqsiteid, string ConType, bool showall = true)
        {
            var MOReleases = db1.GetMOReleasingHeaders(username, Key, transiteid, reqsiteid, ConType, showall);
            var data = MOReleases.Where(x => x.MOStatus == "Pending")
                .OrderByDescending(x => x.ID)
                .ToPaginate(pageindex, pagesize)
                .Select(x => new MMOReleaseHeader()
                {
                    CanReceive = x.CanRecieve,
                    ChallanNo = x.ChallanNo,
                    ConsumableType = x.ConsumableType,
                    DisplayRequestedBySite = x.DisplayReqSite,
                    DisplayTransferringSite = x.DisplayTransSite,
                    ID = x.ID,
                    LastUpdatedBy = x.LastUpdatedBy,
                    LastUpdatedOn = x.LastUpdatedOn,
                    MOID = x.MTRID,
                    MORefNo = x.MORefNo,
                    ReferenceNo = x.ReferenceNo,
                    ReleasedBy = x.ReleasedBy,
                    ReleasedOn = x.ReleasedOn,
                    Remarks = x.Remarks,
                    RequestingSiteID = x.RequestedSiteID,
                    RID = x.RID,
                    SiteCode = x.SiteCode,
                    MOStatus = x.MOStatus,
                    MOReceiveStatus = x.MOReceiveStatus,
                    MOReleaseStatus = x.MOReleaseStatus,
                    TransferringSiteID = x.TransferringSiteID,
                    TransitID = x.TransitID,
                    Transporter = x.Transporter,
                    Year = x.Year
                });
            return data;
        }

        public MMO GetMObyId(int mtrid)
        {
            var mo = db.MTRs.Single(x => x.ID == mtrid);
            return ConvertToMMO(mo);
        }

        private MMO ConvertToMMO(MTR _mo)
        {
            MMO mmo = new MMO()
             {
                 ID = _mo.ID,
                 RequestingSite = _mo.BUs.Name,
                 RequestingSiteID = _mo.RequestedSiteID,
                 Requester = _mo.RequestedBy,
                 TransferringSite = _mo.BUs1.Name,
                 TransferringSiteID = _mo.TransferringSiteID,
                 TransferDate = _mo.TransferBefore,
                 Remarks = _mo.Remarks,
                 Status = _mo.ClosedStatus,
                 IsClosed = _mo.IsClosed,
                 ReferenceNo = _mo.ReferenceNo,
                 TransporterID = _mo.TransporterID,
                 RaisedBy = _mo.RaisedBy,
                 RaisedOn = _mo.RaisedOn,
                 ConsumableType = _mo.ConsumableType
             };
            mmo.MOLines = new ObservableCollection<MMOLine>();
            _mo.MTRLines.Select(x => ConvertToMOLines(x)).ToList()
             .ForEach(x => mmo.MOLines.Add(x));
            return mmo;
        }

        private MMOLine ConvertToMOLines(MTRLine _mtrline)
        {
            MMOLine mmo_line = new MMOLine()
            {
                ID = _mtrline.MTRID,
                LineID = _mtrline.LineID,
                ItemID = _mtrline.ItemID,
                Item = _mtrline.Item.DisplayName,
                SizeID = _mtrline.SizeID,
                BrandID = _mtrline.BrandID,
                LineRemarks = _mtrline.Remarks,
                InTransitQty = _mtrline.InTransitQty,
                Quantity = _mtrline.RequiredQty,
                ReleasedQty = _mtrline.ReleasedQty,
                ReceivedQty = _mtrline.TransferredQty,
                LineChallanNo = _mtrline.ChallanNo,
                ConsumableType = _mtrline.ConsumableType
            };
            if (_mtrline.ItemBrand != null)
                mmo_line.Brand = _mtrline.ItemBrand.Value;
            if (_mtrline.ItemSize != null)
                mmo_line.Size = _mtrline.ItemSize.Value;
            return mmo_line;
        }

        public MReceiving GetMReceivingforMoRelease(int moid)
        {
            var data = db.MTRs.Single(x => x.ID == moid);
            return ConvertMTR_TO_MReleasing(data);
        }

        private MReceiving ConvertMTR_TO_MReleasing(MTR _it)
        {
            MReceiving mrec = new MReceiving()
            {
                TransactionID = _it.ID,
                SiteID = _it.RequestedSiteID,
                TranferSiteID = _it.TransferringSiteID,
                CreatedBy = _it.ReleasedBy,
                CreatedOn = DateTime.UtcNow.AddHours(5.5),
                ReceivingRemarks = _it.Remarks,
                Site = _it.BUs.Name,
                TransferSite = _it.BUs1.Name,
                TransporterID = _it.TransporterID,
                TransactionTypeID = "MORelease",
            };

            mrec.ReceivingLines = new ObservableCollection<MReceivingLine>();
            _it.MTRLines.Select(x => ConvertToRLines(x)).ToList()
                .ForEach(x => mrec.ReceivingLines.Add(x));
            return mrec;
        }

        public MReceivingLine ConvertToRLines(MTRLine _itd)
        {
            MReceivingLine mrline = new MReceivingLine()
            {
                LineID = _itd.LineID,
                Item = _itd.Item.DisplayName,
                ItemID = _itd.ItemID,
                SizeID = _itd.SizeID.Value,
                BrandID = _itd.BrandID.Value,
                MTRID = _itd.MTRID,
                MTRLineID = _itd.LineID,
                LineRemarks = _itd.Remarks,
                Quantity = 0,
                EarlierReleasedQty = _itd.ReleasedQty ?? 0,
                OrderedQty = _itd.RequiredQty,
                TransactionTypeID = "MORelease",
                LineChallanNo = _itd.ChallanNo
            };
            if (_itd.ItemBrand != null)
                mrline.Brand = _itd.ItemBrand.Value;
            if (_itd.ItemSize != null)
                mrline.Size = _itd.ItemSize.Value;
            return mrline;
        }

        public MReceiving GetMReceivingforMoReceiving(int moid)
        {
            var data = db.MTRs.Single(x => x.ID == moid);
            return ConvertMTR_TO_MReceiving(data);
        }

        private MReceiving ConvertMTR_TO_MReceiving(MTR _it)
        {
            MReceiving mrec = new MReceiving()
            {

                TransactionID = _it.ID,
                SiteID = _it.RequestedSiteID,
                CreatedBy = _it.ReleasedBy,
                CreatedOn = DateTime.UtcNow.AddHours(5.5),
                ReceivingRemarks = _it.Remarks,
                Site = _it.BUs.Name,
                MOID = _it.ID,
                TransferSite = _it.BUs1.Name,
                TranferSiteID = _it.TransferringSiteID,
                TransactionTypeID = "MOReceive",
                ReceivedBy = _it.ReceivedBy,
                ReceivedOn = DateTime.UtcNow.AddHours(5.5),
                ReleaseOn = DateTime.UtcNow.AddHours(5.5),
                ReleasedBy = _it.ReleasedBy
            };

            mrec.ReceivingLines = new ObservableCollection<MReceivingLine>();
            _it.MTRLines.Select(x => ConvertToRLines2(x)).ToList()
                .ForEach(x => mrec.ReceivingLines.Add(x));
            return mrec;
        }

        public MReceivingLine ConvertToRLines2(MTRLine _itd)
        {
            MReceivingLine mrline = new MReceivingLine()
            {
                LineID = _itd.LineID,
                Item = _itd.Item.DisplayName,
                ItemID = _itd.ItemID,
                SizeID = _itd.SizeID.Value,
                BrandID = _itd.BrandID.Value,
                MTRID = _itd.MTRID,
                MTRLineID = _itd.LineID,
                LineRemarks = _itd.Remarks,
                Quantity = 0,
                EarlierReceivedQty = _itd.TransferredQty ?? 0,
                OrderedQty = _itd.RequiredQty,
                ReleasedQty = _itd.ReleasedQty.Value,
                TransactionTypeID = "MOReceive",
                LineChallanNo = _itd.ChallanNo
            };
            if (_itd.ItemBrand != null)
                mrline.Brand = _itd.ItemBrand.Value;
            if (_itd.ItemSize != null)
                mrline.Size = _itd.ItemSize.Value;
            return mrline;
        }

        public bool IsSimilarMOsAvailable(int moid)
        {
            var data = db1.CheckSimilarMOsAvailability(moid);
            if (data == null) return false;
            else
                return data.Value;
        }

        /********************************************************************/
        /*NEW METHODS after Double Entry System*/
        /*By: MADHUR*/

        public MMOReleasing GetMOReleaseTransactionByMOID(int moid)
        {
            var data = db.MTRs.Single(x => x.ID == moid);
            return ConvertToMMOReleasing(data);
        }

        public MMOReceiving GetMOReceiveTransactionByMOID(int moid)
        {
            var data = db.MTRs.Single(x => x.ID == moid);
            var lines = db.MOReleasingLines.Where(x => x.MOID == moid && x.IsReceived == false && x.Qty > 0);
            return ConvertToMMOReceiving(data, lines);
            //return ConvertToMMOReceiving(data);
        }

        public MMOReleasing GetUniqueMOReleasingByID(int moid, int relid)
        {
            var mtr = db.MTRs.Single(x => x.ID == moid);
            var releasinglines = db.MOReleasingLines.Where(x => x.MOID == moid && x.HeaderID == relid);
            var data = ConvertToMMOReleasing(mtr, releasinglines);
            return data;
        }

        public MMOReceiving GetMOReceivingAgainstMORelease(int moid, int relid)
        {
            var data = db.MTRs.Single(x => x.ID == moid);
            var releasinglines = db.MOReleasingLines.Where(x => x.MOID == moid && x.HeaderID == relid && x.IsReceived == false && x.Qty > 0);
            return ConvertToMMOReceiving(data, releasinglines);
        }

        public IQueryable<MMOReleasing> GetMOReleasingsAgainstMO(int moid)
        {
            var data = db1.GetMOReleasingsAgainstMOID(moid).ToList();
            var fdata = data.Select(x => ConvertReleasingToMMOReleasing(x));
            return fdata.AsQueryable();
        }

        public IQueryable<MMOReceiving> GetMOReceivingsAgainstMO(int moid)
        {
            var data = db1.GetMOReceivingsAgainstMOID(moid).ToList();
            var fdata = data.Select(x => ConvertReceivingToMMOReceiving(x));
            return fdata.AsQueryable();
        }

        #region Convertion Methods

        #region MO Release

        public MMOReleasing ConvertToMMOReleasing(MTR data)
        {
            MMOReleasing mrec = new MMOReleasing()
            {
                ReferenceNo = data.ReferenceNo,
                ID = data.ID,
                RequestingSiteID = data.RequestedSiteID,
                TransferringSiteID = data.TransferringSiteID,
                ReleasedBy = data.RaisedBy,
                ReleasedOn = data.RaisedOn,
                LastUpdatedOn = data.ReleasedOn,
                LastUpdatedBy = data.ReleasedBy,
                Remarks = data.Remarks,
                TransporterID = data.TransporterID,
                MOReferenceNo = data.ReferenceNo,
                MTRID = data.ID,
                ConsumableType = data.ConsumableType
            };
            if (data.BUs1 != null) { mrec.RequestingSite = data.BUs.DisplaySite; }
            if (data.BUs != null) { mrec.TransferringSite = data.BUs1.DisplaySite; }
            if (data.MTRLines != null)
            {
                mrec.MOReleasingLines = new ObservableCollection<MMOReleasingLine>();
                data.MTRLines.Where(x => x.RequiredQty > x.ReleasedQty)
                    .Select(x => ConvertToMMOReleasingLines(x)).ToList()
                    .ForEach(x => mrec.MOReleasingLines.Add(x));
            }
            return mrec;
        }

        public MMOReleasingLine ConvertToMMOReleasingLines(MTRLine x)
        {
            MMOReleasingLine mrline = new MMOReleasingLine()
            {
                LineID = x.LineID,
                ItemID = x.ItemID,
                SizeID = x.SizeID,
                BrandID = x.BrandID,
                LineRemarks = x.Remarks,
                Quantity = 0,
                LineChallanNo = x.ChallanNo,
                OrderedQty = x.RequiredQty,
                EarlierReleasedQty = x.ReleasedQty ?? 0,
                MOID = x.MTRID,
                MOLineID = x.LineID
            };
            if (x.Item != null) { mrline.Item = x.Item.DisplayName; }
            if (x.ItemSize != null) { mrline.Size = x.ItemSize.Value; }
            if (x.ItemBrand != null) { mrline.Brand = x.ItemBrand.Value; }
            return mrline;
        }

        public MMOReleasing ConvertReleasingToMMOReleasing(GetMOReleasingsAgainstMOIDResult x)
        {
            MMOReleasing r = new MMOReleasing()
            {
                ChallanNo = x.ChallanNo,
                ID = x.ID,
                ReferenceNo = x.ReferenceNo,
                ReleasedBy = x.ReleasedBy,
                ReleasedOn = x.ReleasedOn,
                Remarks = x.Remarks,
                Transporter = x.Transporter,
                TransporterID = x.TransitID,
                TransferringSiteID = x.TransferringSiteID,
                TransferringSite = x.TransferringSite,
                LinesCount = x.LineCounts,
                MOReferenceNo = x.ReferenceNo,
                MTRID = x.ID,
                ConsumableType = x.ConsumableType
            };
            return r;
        }

        #endregion

        #region MO Receive

        public MMOReceiving ConvertToMMOReceiving(MTR data)
        {
            MMOReceiving mrec = new MMOReceiving()
            {
                ReferenceNo = data.ReferenceNo,
                ID = data.ID,
                RequestingSiteID = data.RequestedSiteID,
                TransferringSiteID = data.TransferringSiteID,
                ReceivedBy = data.RaisedBy,
                ReceivedOn = data.RaisedOn,
                LastUpdatedOn = data.ReceivedOn,
                LastUpdatedBy = data.ReceivedBy,
                Remarks = data.Remarks,
                TranporterID = data.TransporterID,
                ConsumableType = data.ConsumableType
            };
            if (data.BUs != null) { mrec.RequestingSite = data.BUs.DisplaySite; }
            if (data.BUs1 != null) { mrec.TransferringSite = data.BUs1.DisplaySite; }
            if (data.MTRLines != null)
            {
                mrec.MOReceivingLines = new ObservableCollection<MMOReceivingLine>();
                data.MTRLines.Select(x => ConvertToMMOReceivingLines(x)).ToList()
                    .ForEach(x => mrec.MOReceivingLines.Add(x));
            }
            return mrec;
        }

        public MMOReceivingLine ConvertToMMOReceivingLines(MTRLine x)
        {
            MMOReceivingLine mrline = new MMOReceivingLine()
            {
                LineID = x.LineID,
                ItemID = x.ItemID,
                LineRemarks = x.Remarks,
                Quantity = 0,
                LineChallanNo = x.ChallanNo,
                OrderedQty = x.RequiredQty,
                EarlierReceivedQty = x.TransferredQty ?? 0,
                MOID = x.MTRID,
                MOLineID = x.LineID,
                AlreadyReleasedQty = x.ReleasedQty ?? 0
            };
            if (x.SizeID.HasValue) mrline.SizeID = x.SizeID.Value;
            if (x.BrandID.HasValue) mrline.BrandID = x.BrandID.Value;
            if (x.Item != null) { mrline.Item = x.Item.DisplayName; }
            if (x.ItemSize != null) { mrline.Size = x.ItemSize.Value; }
            if (x.ItemBrand != null) { mrline.Brand = x.ItemBrand.Value; }
            return mrline;
        }

        public MMOReleasing ConvertToMMOReleasing(MTR data, IQueryable<MOReleasingLine> lines)
        {
            MMOReleasing mrel = new MMOReleasing()
            {
                ReferenceNo = data.ReferenceNo,
                ID = data.ID,
                RequestingSiteID = data.RequestedSiteID,
                TransferringSiteID = data.TransferringSiteID,
                ReleasedBy = data.RaisedBy,
                ReleasedOn = data.RaisedOn,
                LastUpdatedOn = data.ReceivedOn,
                LastUpdatedBy = data.ReceivedBy,
                Remarks = data.Remarks,
                TransporterID = data.TransporterID,
                MOReferenceNo = data.ReferenceNo,
                MTRID = data.ID,
                ConsumableType = data.ConsumableType
            };
            //Getting transporter name
            var transp = db.Transporters.Single(x => x.ID == data.TransporterID);
            mrel.Transporter = transp.Name;
            if (data.BUs != null) { mrel.RequestingSite = data.BUs.DisplaySite; }
            if (data.BUs1 != null) { mrel.TransferringSite = data.BUs1.DisplaySite; }

            //*****Picking Releasing Lines from MO Releasing Lines Table*****

            if (lines != null)
            {
                mrel.MOReleasingLines = new ObservableCollection<MMOReleasingLine>();
                lines.ToList()
                    .ForEach(x => mrel.MOReleasingLines.Add(ConverttoMMOReleasingLines(x)));
            }
            return mrel;
        }

        public MMOReleasingLine ConverttoMMOReleasingLines(MOReleasingLine x)
        {
            MMOReleasingLine line = new MMOReleasingLine()
            {
                LineID = x.ID,
                ItemID = x.ItemID,
                LineRemarks = x.Remarks,
                Qty = x.Qty,
                LineChallanNo = x.ChallanNo,
                OrderedQty = x.OrderedQty,
                EarlierReleasedQty = x.EarlierReleasedQty,
                PendingReleaseQty = x.OrderedQty - (x.EarlierReleasedQty + x.Qty),
                MOID = x.MOID,
                MOLineID = x.MOLineID,
                TruckNo = x.TruckNo,
                HeaderID = x.HeaderID
            };
            if (x.SizeID.HasValue) line.SizeID = x.SizeID.Value;
            if (x.BrandID.HasValue) line.BrandID = x.BrandID.Value;
            if (x.Item != null) { line.Item = x.Item.DisplayName; }
            if (x.ItemSize != null) { line.Size = x.ItemSize.Value; }
            if (x.ItemBrand != null) { line.Brand = x.ItemBrand.Value; }
            return line;
        }

        public MMOReceiving ConvertToMMOReceiving(MTR data, IQueryable<MOReleasingLine> lines)
        {
            MMOReceiving mrec = new MMOReceiving()
            {
                ReferenceNo = data.ReferenceNo,
                ID = data.ID,
                RequestingSiteID = data.RequestedSiteID,
                TransferringSiteID = data.TransferringSiteID,
                ReceivedBy = data.RaisedBy,
                ReceivedOn = data.RaisedOn,
                LastUpdatedOn = data.ReceivedOn,
                LastUpdatedBy = data.ReceivedBy,
                Remarks = data.Remarks,
                TranporterID = data.TransporterID,
                MOReleaseRefNo = data.ReferenceNo,
                MTRID = data.ID,
                ConsumableType = data.ConsumableType
            };
            //Getting transporter name
            var transp = db.Transporters.Single(x => x.ID == data.TransporterID);
            mrec.Transporter = transp.Name;
            if (data.BUs != null) { mrec.RequestingSite = data.BUs.DisplaySite; }
            if (data.BUs1 != null) { mrec.TransferringSite = data.BUs1.DisplaySite; }

            //*****Picking Receiving Lines from MO Releasing Lines Table*****

            if (lines != null)
            {
                mrec.MOReceivingLines = new ObservableCollection<MMOReceivingLine>();
                lines.ToList()
                    .ForEach(x => mrec.MOReceivingLines.Add(ConvertFromReleasingLinestoReceivingLines(x)));
            }

            return mrec;
        }
        
        public MMOReceivingLine ConvertFromReleasingLinestoReceivingLines(MOReleasingLine x)
        {            
            MMOReceivingLine line = new MMOReceivingLine()
            {
                LineID = x.ID,
                ItemID = x.ItemID,
                LineRemarks = x.Remarks,
                Quantity = x.Qty,
                LineChallanNo = x.ChallanNo,
                OrderedQty = x.OrderedQty,
                EarlierReceivedQty = x.ReceivingQty.Value,
                MOID = x.MOID,
                MOLineID = x.MOLineID,
                AlreadyReleasedQty = x.Qty,
                TruckNo = x.TruckNo,
                ReleasingID = x.HeaderID,
                ReleasingLineID = x.ID
            };
            if (x.SizeID.HasValue) line.SizeID = x.SizeID.Value;
            if (x.BrandID.HasValue) line.BrandID = x.BrandID.Value;
            
            if (x.Item != null) { line.Item = x.Item.DisplayName; }
            if (x.ItemSize != null) { line.Size = x.ItemSize.Value; }
            if (x.ItemBrand != null) { line.Brand = x.ItemBrand.Value; }
            return line;
        }

        public MMOReceiving ConvertReceivingToMMOReceiving(GetMOReceivingsAgainstMOIDResult x)
        {
            MMOReceiving r = new MMOReceiving()
            {
                ChallanNo = x.ChallanNo,
                ID = x.ID,
                ReferenceNo = x.ReferenceNo,
                ReceivedBy = x.ReceivedBy,
                ReceivedOn = x.ReceivedOn,
                Remarks = x.Remarks,
                Transporter = x.Transporter,
                TranporterID = x.TransitID,
                RequestingSiteID = x.RequestingSiteID,
                RequestingSite = x.RequestingSite,
                LinesCount = x.LineCounts,
                MOReleaseRefNo = x.ReferenceNo,
                MTRID = x.ID,
                ConsumableType = x.ConsumableType
            };
            return r;
        }
        #endregion

        #endregion

        /********************************************************************/
    }
}