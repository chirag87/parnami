﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Entity;
using System.Collections.ObjectModel;

namespace SIMS.MM.BLL
{
    using DAL;
    using MODEL;
    using SIMS.Core;

    public class PurchasingDataProvider : IPurchasingDataProvider
    {
        parnamidb1Entities db = new parnamidb1Entities();
        MMReportsDBDataContext db1 = new MMReportsDBDataContext();

        public IPaginated<MPOHeader> GetPOHeadersNew(string username, int? pageindex, int? pagesize, string key, int? vendorid, int? siteid, bool showall = true)
        {
            var PRs = db1.GetPOHeaders(username, key, vendorid, siteid, showall);
            return PRs.OrderByDescending(x => x.CreatedOn)
                .ToPaginate(pageindex, pagesize)
                .Select(x => new MPOHeader()
                {
                    POID = x.ID,
                    PRNo = x.PRID,
                    Date = x.CreatedOn,
                    //Vendor = x.Vendor.Name,
                    DisplayVendor = x.VendorName,
                    DisplaySite = x.SiteName,
                    //Site = x.BUs.Name,
                    Price = (double)x.Total,
                    POReferenceNo = x.Display,
                    ReferenceNo = x.PRRef,
                    IsClosed = x.IsClosed
                });
        }

        public IPaginated<MPRHeader> GetPRHeadersNew(string username, int? pageindex, int? pagesize, string key, int? vendorid, int? siteid, string status, bool showall = true)//Using Table-Valued Funtions
        {
            var PRs = db1.GetPRHeaders(username, key, vendorid, siteid, status, showall);
            var data = PRs.OrderByDescending(x => x.RaisedOn)
                .ToPaginate(pageindex, pagesize)
                .Select(x => new MPRHeader()
                {
                    PRNo = x.ID,
                    Date = x.RaisedOn,
                    DisplayVendor = x.VendorName,
                    DisplaySite = x.SiteName,
                    Price = (double)x.TotalPrice,
                    VerificationStatus = x.VerificationStatus,
                    RequestApprovalStatus = x.RequestApprovalStatus,
                    ApprovalStatus = x.ApprovalStatus,
                    ReferenceNo = x.ReferenceNo,
                    IsVerified = x.IsVerified,
                    IsReqApproved = x.IsReqApproved,
                    IsApproved = x.IsApproved,
                    IsClosed = x.IsClosed,
                    PRState = x.Status
                });
            return data;
        }

        public MPO GetPObyId(int poid)
        {
            var obj2 = db.PRs.Single(x => x.POID == poid);
            return ConvertPO_TO_MPO(obj2);
        }

        MPO ConvertPO_TO_MPO(PR _pr)
        {
            var _po = _pr.POs;
            MPO mpo = new MPO()
            {
                POID = _pr.POs.ID,
                //AcknowledgedOn=_po.AcknowledgedOn,
                AckRemarks = _po.Ack_Remarks,
                ContactMobile = _po.ContactPersonPhone,
                ContactPerson = _po.ContactPersonName,
                CreatedBy = _po.CreatedBy,
                CreatedOn = _po.CreatedOn,
                IsAcknowledged = _po.IsAcknowledged,
                IsClosed = _po.IsClosed,

                PRNumber = _pr.ID,
                VendorID = _pr.VendorID,
                Vendor = _pr.Vendor.Name,
                TotalValue = (double)_pr.SubTotal,
                IsApproved = _pr.IsApproved,
                SiteID = _pr.SiteID,
                Site = _pr.BUs.Name,
                Purchaser = _pr.PurchasedForUserID,
                PurchaseType = _pr.PurchaseTypeID,
                PRRemarks = _po.Remarks,
                FinalApprovalStatus = _pr.IsApproved ? "Approved" : "Pending",
                Frieght = _pr.Frieght,
                pCST = _pr.PCST,
                pVAT = _pr.PVat,
                TAX = _pr.Tax,
                GrandTotal = _pr.Total,
                CurrentOwnerID = _pr.CurrentOwnerID,
                ApprovedBy = _pr.ApprovedBy,
                RaisedOn = _pr.RaisedOn,
                PaymentTerms = _pr.PaymentTerms,
                POReferenceNo = _po.Display,
                TotalDiscount = _po.TotalDiscount.Value,
                NetAmount = _po.NetAmount.Value
            };
            if (_pr.BUs != null)
            {
                mpo.Site = _pr.BUs.DisplaySite;
            }
            if (_pr.Vendor != null)
            {
                mpo.Vendor = _pr.Vendor.DisplayVendor;
            }
            mpo.PRLines = new ObservableCollection<MPRLine>();
            _pr.PRDetails.Where(x => x.PRDetails_AdditionalInfo.IsDeleted == false && x.Qty > 0)
                .Select(x => ConvertToMPRLine(x)).Where(x => x.UnavailableQty > 0).ToList()
                .ForEach(x => mpo.PRLines.Add(x));
            return mpo;
        }

        MPR ConvertPRToMPR(PR _pr)
        {
            MPR mpr = new MPR()
            {
                PRNumber = _pr.ID,
                VendorID = _pr.VendorID,
                TotalValue = (double)_pr.SubTotal,
                IsReqApproved = _pr.IsReqApproved,
                IsVerified = _pr.IsVerified,
                IsApproved = _pr.IsApproved,
                SiteID = _pr.SiteID,
                Purchaser = _pr.PurchasedForUserID,
                PurchaseType = _pr.PurchaseTypeID,
                PRRemarks = _pr.Remarks,
                InitialApprovalStatus = _pr.IsReqApproved ? "MI Approved" : "Pending MI Approval",
                ReqApprovedBy = _pr.ReqApprovedBy,
                ReqApprovedOn = _pr.ReqApprovedOn,
                FinalApprovalStatus = _pr.IsApproved ? "PO Approved" : "Pending PO Approval",
                ApprovedOn = _pr.ApprovedOn,
                VerificationStatus = _pr.IsVerified ? "MI Verified" : "Not Verified",
                VerifiedBy = _pr.VerifiedBy,
                VerifiedOn = _pr.VerifiedOn,
                Frieght = _pr.Frieght,
                pCST = _pr.PCST,
                pVAT = _pr.PVat,
                TAX = _pr.Tax,
                GrandTotal = _pr.Total,
                CurrentOwnerID = _pr.CurrentOwnerID,
                ApprovedBy = _pr.ApprovedBy,
                RaisedOn = _pr.RaisedOn,
                RaisedBy = _pr.RaisedBy,
                PaymentTerms = _pr.PaymentTerms,
                ReferenceNo = _pr.ReferenceNo,
                ContactMobile = _pr.ContactPersonPhone,
                ContactPerson = _pr.ContactPersonName,
                TotalDiscount = _pr.TotalDiscount,
                NetAmount = _pr.NetAmount,
                PRStatus = _pr.Status,
                IsClosed = _pr.IsClosed,
                VerificationRemarks = _pr.VerificationRemarks,
                ReqApprovalRemarks = _pr.ReqApprovalRemarks,
                ApprovalRemarks = _pr.ApprovalRemarks
            };
            if (_pr.BUs != null)
            {
                mpr.Site = _pr.BUs.DisplaySite;
            }
            if (_pr.Vendor != null)
            {
                mpr.Vendor = _pr.Vendor.DisplayVendor;
            }
            mpr.PRLines = new ObservableCollection<MPRLine>();
            _pr.PRDetails.Where(x => x.PRDetails_AdditionalInfo.IsDeleted == false)
                .Select(x => ConvertToMPRLine(x)).ToList()
                .ForEach(x => mpr.PRLines.Add(x));
            return mpr;
        }

        MPRLine ConvertToMPRLine(PRDetail detail)
        {
            var line = new MPRLine()
            {
                ID = detail.ID,
                LineID = detail.LineID,
                PRID = detail.PRID,
                ItemID = detail.ItemID,
                Item = detail.Item.DisplayName,
                ItemName = detail.Item.Name,
                SizeID = detail.SizeID,
                BrandID = detail.BrandID,
                DeliveryDate = detail.DeliveryDate,
                OriginalQty = detail.OriginalQty,
                Quantity = detail.Qty,
                UnitPrice = detail.UnitPrice,
                LinePrice = detail.LinePrice,
                LastPrice = detail.LastPrice,
                ConsumableType = detail.ConsumableType,
                MeasurementUnit = detail.Item.MeasurementUnit.Value,
                LineRemarks = detail.Remarks,
                Discount = detail.Discount,                
                DiscountType = detail.DiscountType,
                Excise = detail.Excise,
                ExciseIn=detail.ExciseIn,
                TotalLinePrice = detail.TotalLinePrice,
                TotalReceivedQty = detail.TotalReceivedQty,
                //AvailableQty = detail.InStoreQty - detail.PRDetails_AdditionalInfo.SplittedQty,
                AvailableQty = detail.InStoreQty,
                SplittedQuantity = detail.PRDetails_AdditionalInfo.SplittedQty,
            };
            if (detail.ItemBrand != null)
                line.Brand = detail.ItemBrand.Value;
            if (detail.ItemSize != null)
                line.Size = detail.ItemSize.Value;
            return line;
        }

        public MPR GetPRbyId(int prid)
        {
            var pr = db.PRs.Single(x => x.ID == prid);
            return ConvertPRToMPR(pr);
        }

        public MTODispatchInfo GetTotalTODispacthQty(int? ID, int LineID, int SiteID, int ItemID, int? SizeID, int? BrandID, string ConType)
        {
            var data = db1.CalculateTODispatchQtyForASite(SiteID, ItemID, SizeID, BrandID, ConType);
            MTODispatchInfo info = new MTODispatchInfo()
            {
                ID = ID,
                LineID = LineID,
                SiteID = SiteID,
                ItemID = ItemID,
                SizeID = SizeID,
                BrandID = BrandID,
                TODispQty = data ?? 0
            };
            return info;
        }

        //public IQueryable<MODEL.MPOHeader> GetPOHeaders(int? pageindex, int? pagesize, string key, int? vendorid, int? siteid)
        //{
        //    return db.PRs.Where(x => x.POs != null)
        //        .FilterBySite(siteid)
        //        .FilterByVendor(vendorid)
        //        .OrderByDescending(x => x.RaisedOn)
        //        .Paginate(pageindex, pagesize)
        //        .ToList()
        //        .Select(x => new MODEL.MPOHeader()
        //    {
        //        POID = x.POs.ID,
        //        PRNo = x.ID,
        //        Date = x.POs.CreatedOn,
        //        Vendor = x.Vendor.Name,
        //        Site = x.BUs.Name,
        //        Price = (double)x.Total
        //    }).ToList().AsQueryable();
        //}

        //public IPaginated<MPOHeader> GetPOHeaders(string username, int? pageindex, int? pagesize, string key, int? vendorid, int? siteid, bool showall = true)
        //{
        //    var qry = db.PRs.Where(x => x.POs != null)
        //        .OrderByDescending(x => x.POID)
        //        .FilterBySite(siteid)
        //        .FilterByVendor(vendorid);

        //    if (!showall)
        //    {
        //        var ids = db.GetAllowedSiteIDs(username);
        //        qry = qry.Where(x => ids.Contains(x.SiteID));
        //    }
        //    if (!String.IsNullOrWhiteSpace(key))
        //    {
        //        key = key.ToLower();
        //        qry = qry.ToList().AsQueryable();
        //        qry = qry.Where(x =>
        //            x.ReferenceNo.ToLower().Contains(key)
        //            || x.PRDetails.Any(y => y.Item.Name.ToLower().Contains(key))
        //            || x.ID.ToString().Contains(key)
        //            );
        //    }
        //    return qry
        //       .ToPaginate(pageindex, pagesize)
        //        //.ToList()
        //       .Select(x => new MODEL.MPOHeader()
        //       {
        //           POID = x.POs.ID,
        //           PRNo = x.ID,
        //           Date = x.POs.CreatedOn,
        //           Vendor = x.Vendor.Name,
        //           DisplayVendor = x.Vendor.DisplayVendor,
        //           DisplaySite = x.BUs.DisplaySite,
        //           Site = x.BUs.Name,
        //           Price = (double)x.Total,
        //           POReferenceNo = x.POs.Display,
        //           IsClosed = x.POs.IsClosed
        //       });
        //}

        //public IQueryable<MODEL.MPRHeader> GetPRHeaders(int? pageindex, int? pagesize, string key, int? vendorid, int? siteid)
        //{

        //    return db.PRs.FilterBySite(siteid).Where(x=>x.POID==null)
        //        .FilterByVendor(vendorid)
        //        .OrderByDescending(x=>x.RaisedOn)
        //        .Paginate(pageindex,pagesize)
        //        .ToList()
        //        .Select(x => new MODEL.MPRHeader()
        //        {
        //            PRNo=x.ID,
        //            Date=x.RaisedOn,
        //            Vendor=x.Vendor.Name,
        //            Site=x.BUs.Name,
        //            Price = (double)x.TotalPrice,
        //            ApprovalStatus= x.ApprovalStatus
        //        }).AsQueryable();
        //}

        //public  IPaginated<MPOHeader> GetPOHeaders(int? pageindex, int? pagesize, string key, int? vendorid, int? siteid)
        //{
        //    return db.PRs.Where(x => x.POs != null)
        //       .FilterBySite(siteid)
        //       .FilterByVendor(vendorid)
        //       .OrderByDescending(x => x.RaisedOn)
        //       .ToList()
        //       .ToPaginate(pageindex,pagesize)               
        //       .Select(x => new MODEL.MPOHeader()
        //       {
        //           POID = x.POs.ID,
        //           PRNo = x.ID,
        //           Date = x.POs.CreatedOn,
        //           Vendor = x.Vendor.Name,
        //           Site = x.BUs.Name,
        //           Price = (double)x.Total
        //       });
        //}

        //public IPaginated<MPRHeader> GetPRHeaders(string username, int? pageindex, int? pagesize, string key, int? vendorid, int? siteid, bool showall = true)
        //{
        //    var qry= db.PRs.Where(x => x.POID == null)
        //        .OrderByDescending(x => x.RaisedOn)
        //        .FilterBySite(siteid)
        //        .FilterByVendor(vendorid);
        //    if(!showall)
        //    {
        //        var ids=db.GetAllowedSiteIDs(username,showall);
        //       qry=qry.Where(x=>ids.Contains(x.SiteID));

        //    }
        //        //.OrderByDescending(x => x.RaisedOn)
        //       return qry.ToList()
        //        .ToPaginate(pageindex, pagesize)                 
        //        .Select(x => new MPRHeader()
        //        {
        //            PRNo = x.ID,
        //            Date = x.RaisedOn,                    
        //            Vendor = x.Vendor.Name,
        //            DisplayVendor=x.Vendor.DisplayVendor,
        //            DisplaySite = x.BUs.DisplaySite,
        //            Site = x.BUs.Name,
        //            Price = (double)x.TotalPrice,
        //            ApprovalStatus = x.ApprovalStatus,
        //            ReferenceNo=x.ReferenceNo
        //        });
        //}
    }
}