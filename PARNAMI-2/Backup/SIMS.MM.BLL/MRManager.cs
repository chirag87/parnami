﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SIMS.MM.BLL
{
    using MODEL;
    using DAL;
    public class MRManager : IMRManager
    {
        parnamidb1Entities db = new parnamidb1Entities();
        public MMR CreateNewMR(MMR _NewMR)
        {
            MRDataProvider pr = new MRDataProvider();
            var MR = ConvertToMR(_NewMR);
            MR.ID = db.GetMax_MR_ID() + 1;
            MR.Year = DateTime.UtcNow.AddHours(5.5).Year;
            MR.SiteCode = db.GetSiteCode(MR.SiteID);
            MR.ReferenceNo = "MR/" + MR.Year + "/S-" + MR.SiteCode + "/" + MR.ID;
            db.MRs.AddObject(MR);
            db.SaveChanges();
            return pr.GetMRByID(MR.ID);
        }

        private MR ConvertToMR(MMR _NewMR)
        {
            MR _mr = new MR()
            {
                ID = _NewMR.ID,
                CurrentOwnerID = _NewMR.CurrentOwnerID,
                ConactPersonPhone = _NewMR.ContactPerson,
                IsApproved = _NewMR.IsApproved,
                ContactPersonName = _NewMR.ContactPerson,
                RaisedBy = _NewMR.RaisedBy,
                RaisedOn = _NewMR.RaisedOn,
                Remarks = _NewMR.Remarks,
                RequestedBy = _NewMR.RequestedBy,
                SiteID = _NewMR.SiteID
            };
            if (_NewMR.MRLines != null)
                _NewMR.MRLines.ToList()
                    .ForEach(x => _mr.MRLines.Add(ConvertToMRLIne(x)));
            db.SaveChanges();
            return _mr;
        }

        private MRLine ConvertToMRLIne(MMRLine x)
        {
            MRLine line = new MRLine()
            {
                ID = x.LineID,
                ItemID = x.ItemID,
                SizeID = x.SizeID,
                BrandID = x.BrandID,
                Remarks = x.LineRemarks,
                IssuedQty = 0,
                ReturnedQty = 0,
                MRID = x.MRID,
                Qty = x.Quantity,
                ConsumableType = x.ConsumableType
            };
            return line;
        }

        public int CreateMRIssue(MReceiving _mrec)
        {
            ReceivingManager rm = new ReceivingManager();
            _mrec.TransactionID = db.GetMaxMOId("Issue") + 1;
            _mrec.TransactionTypeID = "Issue";
            _mrec.CreatedBy = "dba";
            _mrec.LastUpdatedBy = "dba";
            _mrec.LastUpdaetdOn = DateTime.UtcNow.AddHours(5.5);
            _mrec.CreatedOn = DateTime.UtcNow.AddHours(5.5);
            var rec = rm.ConvertToINTR(_mrec);
            rec.Year = DateTime.UtcNow.AddHours(5.5).Year;
            rec.SiteID = db.GetSiteCode(rec.BUID);//???-------------
            rec.RID = db.GetMaxRRID("Issue", rec.Year, rec.BUID) + 1;
            rec.ReferenceNo = rec.TypeID + "/" + rec.Year + "/S-" + rec.SiteID + "/" + rec.RID;
            rec.TransitID = 1;
            db.InventoryTransactions.AddObject(rec);
            db.SaveChanges();
            return _mrec.TransactionID;
        }

        public int CreateMRReturn(MReceiving _mrec)
        {
            ReceivingManager rm = new ReceivingManager();
            _mrec.TransactionID = db.GetMaxMOId("Return") + 1;
            _mrec.TransactionTypeID = "Return";
            _mrec.CreatedBy = "dba";
            _mrec.LastUpdatedBy = "dba";
            _mrec.LastUpdaetdOn = DateTime.UtcNow.AddHours(5.5);
            _mrec.CreatedOn = DateTime.UtcNow.AddHours(5.5);
            var rec = rm.ConvertToINTR(_mrec);
            rec.Year = DateTime.UtcNow.AddHours(5.5).Year;
            rec.SiteID = db.GetSiteCode(rec.BUID);//???-------------
            rec.RID = db.GetMaxRRID("Return", rec.Year, rec.BUID) + 1;
            rec.ReferenceNo = rec.TypeID + "/" + rec.Year + "/S-" + rec.SiteID + "/" + rec.RID;
            rec.TransitID = 1;
            db.InventoryTransactions.AddObject(rec);
            db.SaveChanges();
            db.CloseMR_IFRequired(_mrec.MRID);
            return _mrec.TransactionID;
        }

        public void CloseMR(int mrid)
        {
            var mr = db.MRs.Single(x => x.ID == mrid);
            mr.IsClosed = true;
            mr.ClosedBy = "dba";
            db.SaveChanges();
        }

        /********************************************************************/
        /*NEW METHODS after Double Entry System*/
        /*By: MADHUR*/

        public int MRIssue(MMaterialIssue _mrec, string username)
        {
            _mrec.ID = db.GetMaxMRIssueID() + 1;
            _mrec.LastUpdatedBy = username;
            _mrec.LastUpdatedOn = DateTime.UtcNow.AddHours(5.5);

            var rec = ConvertToMaterialIssue(_mrec);

            rec.Year = DateTime.UtcNow.AddHours(5.5).Year;
            rec.SiteCode = db.GetSiteCode(rec.BUID);
            rec.RID = db.GetMaxMRIssueRID(rec.Year, rec.BUID) + 1;
            rec.ReferenceNo = "Issue/" + rec.Year + "/S-" + rec.SiteCode + "/" + rec.RID;
            db.MaterialIssues.AddObject(rec);
            db.SaveChanges();

            return _mrec.ID;
        }

        public int MRReturn(MMaterialReturn _mrec, string username)
        {
            _mrec.ID = db.GetMaxMRReturnID() + 1;
            _mrec.LastUpdatedBy = username;
            _mrec.LastUpdatedOn = DateTime.UtcNow.AddHours(5.5);

            var rec = ConvertToMaterialReturn(_mrec);

            rec.Year = DateTime.UtcNow.AddHours(5.5).Year;
            rec.SiteCode = db.GetSiteCode(rec.BUID);
            rec.RID = db.GetMaxMRReturnRID(rec.Year, rec.BUID) + 1;
            rec.ReferenceNo = "Return/" + rec.Year + "/S-" + rec.SiteCode + "/" + rec.RID;
            db.MaterialReturns.AddObject(rec);
            db.SaveChanges();

            return _mrec.ID;
        }

        #region Convertion Methods

        #region MR Issue

        public MaterialIssue ConvertToMaterialIssue(MMaterialIssue data)
        {
            MaterialIssue mrec = new MaterialIssue()
            {
                ReferenceNo = data.ReferenceNo,
                ID = data.ID,
                BUID = data.SiteID,
                IssuedBy = data.IssuedBy,
                IssuedOn = data.IssuedOn,
                LastUpdatedOn = data.LastUpdatedOn,
                LastUpdatedBy = data.LastUpdatedBy,
                Requester = data.Requester,
                Remarks = data.Remarks
            };
            if (data.MaterialIssueLines != null)
            {
                data.MaterialIssueLines.ToList()
                    .ForEach(x => mrec.MaterialIssueLines.Add(ConvertToMaterialIssueLines(x)));
            }
            return mrec;
        }

        public MaterialIssueLine ConvertToMaterialIssueLines(MMaterialIssueLine x)
        {
            MaterialIssueLine mrline = new MaterialIssueLine()
            {
                ID = x.LineID,
                ItemID = x.ItemID,
                SizeID = x.SizeID,
                BrandID = x.BrandID,
                HeaderID = x.HeaderID,
                Remarks = x.LineRemarks,
                Qty = x.Quantity,
                RequestedQty = x.OrderedQty,
                EarlierIssuedQty = x.EarlierIssuedQty,
                MRID = x.MRID,
                MRLineID = x.MRLineID
            };
            return mrline;
        }

        #endregion

        #region MR Return

        public MaterialReturn ConvertToMaterialReturn(MMaterialReturn data)
        {
            MaterialReturn mrec = new MaterialReturn()
            {
                ReferenceNo = data.ReferenceNo,
                ID = data.ID,
                BUID = data.SiteID,
                ReturnedBy = data.ReturnedBy,
                ReturnedOn = data.ReturnedOn,
                LastUpdatedOn = data.LastUpdatedOn,
                LastUpdatedBy = data.LastUpdatedBy,
                Requester = data.Requester,
                Remarks = data.Remarks
            };
            if (data.MaterialReturnLines != null)
            {
                data.MaterialReturnLines.ToList()
                    .ForEach(x => mrec.MaterialReturnLines.Add(ConvertToMaterialReturnLines(x)));
            }
            return mrec;
        }

        public MaterialReturnLine ConvertToMaterialReturnLines(MMaterialReturnLine x)
        {
            MaterialReturnLine mrline = new MaterialReturnLine()
            {
                ID = x.LineID,
                ItemID = x.ItemID,
                SizeID = x.SizeID,
                BrandID = x.BrandID,
                HeaderID = x.HeaderID,
                Remarks = x.LineRemarks,
                Qty = x.Quantity,
                RequestedQty = x.OrderedQty,
                EarlierReturnedQty = x.EarlierReturnedQty,
                MRID = x.MRID,
                MRLineID = x.MRLineID
            };
            return mrline;
        }

        #endregion

        #endregion

        /********************************************************************/
    }
}
