﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SIMS.MM.BLL;
using System.Collections.ObjectModel;
using SIMS.MM.BLL.Services;

namespace SIMS.MM.BLL
{
    using DAL;
    using MODEL;
    public class PurchasingManager : IPurchasingManger
    {
        parnamidb1Entities db = new parnamidb1Entities();
        PurchasingDataProvider provider = new PurchasingDataProvider();

        PRDetail ConvertToPRDetail(MPRLine _mprLine)
        {
            PRDetail detail = new PRDetail()
            {
                LineID = _mprLine.LineID,
                DeliveryDate = _mprLine.DeliveryDate,
                ItemID = _mprLine.ItemID,
                SizeID = _mprLine.SizeID,
                BrandID = _mprLine.BrandID,
                PRID = _mprLine.PRID,
                OriginalQty = _mprLine.OriginalQty,
                Qty = _mprLine.Quantity,
                LinePrice = _mprLine.LinePrice,
                UnitPrice = _mprLine.UnitPrice,
                Remarks = _mprLine.LineRemarks,
                LastPrice = _mprLine.LastPrice,
                DiscountType = _mprLine.DiscountType,
                Discount = _mprLine.Discount,
                Excise=_mprLine.Excise,
                ExciseIn=_mprLine.ExciseIn,
                TotalLinePrice = _mprLine.TotalLinePrice,
                ConsumableType = _mprLine.ConsumableType,
                AdjustedQty = 0,
                IsClosed = false,
                NetRejectedQty = 0,
                TotalReceivedQty = 0,
                InStoreQty = _mprLine.AvailableQty
            };
            return detail;
        }

        MPRLine ConvertMPRLinesToPRDetails(PRDetail _detail)
        {
            MPRLine mprline = new MPRLine()
            {
                //DeliveryDate=_detail.DeliveryDate,
                LineID = _detail.ID,
                ItemID = _detail.ItemID,
                SizeID = _detail.SizeID,
                BrandID = _detail.BrandID,
                PRID = _detail.PRID,
                OriginalQty = _detail.OriginalQty,
                Quantity = _detail.Qty,
                AvailableQty = _detail.InStoreQty,
                UnitPrice = (double)_detail.UnitPrice,
                LinePrice = (double)_detail.LinePrice,
                LastPrice = _detail.LastPrice,
                Discount = _detail.Discount,
                DiscountType = _detail.DiscountType,
                Excise = _detail.Excise,
                ExciseIn = _detail.ExciseIn,
                TotalLinePrice = _detail.TotalLinePrice,
                ConsumableType = _detail.ConsumableType
            };
            return mprline;
        }

        PR ConvertToPR(MPR _mpr)
        {
            PR pr = new PR()
            {
                ID = _mpr.PRNumber,
                CurrentOwnerID = _mpr.CurrentOwnerID,
                VendorID = _mpr.VendorID,
                VerifiedBy = _mpr.VerifiedBy,
                VerifiedOn = _mpr.VerifiedOn,
                ReqApprovedOn = _mpr.ReqApprovedOn,
                ReqApprovedBy = _mpr.ReqApprovedBy,
                ApprovedBy = _mpr.ApprovedBy,
                ApprovedOn = _mpr.ApprovedOn,
                Frieght = _mpr.Frieght,
                SubTotal = _mpr.TotalValue,
                TotalPrice = _mpr.TotalValue + _mpr.TAX,
                Total = _mpr.GrandTotal,
                PCST = _mpr.pCST,
                PVat = _mpr.pVAT,
                Tax = _mpr.TAX,
                IsVerified = _mpr.IsVerified,
                IsReqApproved = _mpr.IsReqApproved,
                IsApproved = _mpr.IsApproved,
                SiteID = _mpr.SiteID,
                PurchasedForUserID = _mpr.Purchaser,
                PurchaseTypeID = _mpr.PurchaseType,
                Remarks = _mpr.PRRemarks,
                RaisedBy = _mpr.RaisedBy,
                RaisedOn = _mpr.RaisedOn,
                ConvertedTOPO = _mpr.ConvertedToPO,
                ContactPersonName = _mpr.ContactPerson,
                ContactPersonPhone = _mpr.ContactMobile,
                PaymentTerms = _mpr.PaymentTerms,
                TotalDiscount = _mpr.TotalDiscount,
                NetAmount = _mpr.NetAmount,
                Status = _mpr.PRStatus,
                IsClosed = _mpr.IsClosed,
                VerificationRemarks = _mpr.VerificationRemarks,
                ReqApprovalRemarks = _mpr.ReqApprovalRemarks,
                ApprovalRemarks = _mpr.ApprovalRemarks
                //ReferenceNo = _mpr.ReferenceNo,
                //CostCenterID = _mpr.CostCenterID,
                //POID = _mpr.POID
            };
            if (_mpr.PRLines != null)
            {
                _mpr.PRLines
                    .ToList()
                    .ForEach(x => pr.PRDetails.Add(ConvertToPRDetail(x)));
            }
            return pr;
        }

        MPR ConvertPRToMPR(PR _pr)
        {
            MPR mpr = new MPR()
            {
                PRNumber = _pr.ID,
                VendorID = _pr.VendorID,
                IsVerified = _pr.IsVerified,
                IsReqApproved = _pr.IsReqApproved,
                IsApproved = _pr.IsApproved,
                SiteID = _pr.SiteID,
                Purchaser = _pr.PurchasedForUserID,
                PurchaseType = _pr.PurchaseTypeID,
                PRRemarks = _pr.Remarks,
                PaymentTerms = _pr.PaymentTerms,
                pCST = _pr.PCST,
                pVAT = _pr.PVat,
                TotalDiscount = _pr.TotalDiscount,
                NetAmount = _pr.NetAmount,
                IsClosed = _pr.IsClosed,
                VerificationRemarks = _pr.VerificationRemarks,
                ReqApprovalRemarks = _pr.ReqApprovalRemarks,
                ApprovalRemarks = _pr.ApprovalRemarks
            };
            return mpr;
        }

        public MPR CreatePR(MPR NewPR)
        {
            var year = DateTime.UtcNow.AddHours(5.5).Year;
            //NewPR.PRNumber = db.GetMaxPRID() + 1;
            NewPR.PRStatus = "Draft";
            NewPR.IsClosed = false;
            NewPR.UpdateLineNumbers();
            var pr = ConvertToPR(NewPR);
            pr.RaisedOn = DateTime.UtcNow.AddHours(5.5);
            pr.Year = year;
            pr.SiteID = NewPR.SiteID;
            pr.SiteCode = db.GetSiteCode(NewPR.SiteID);
            pr.RID = db.GetMaxPRsRefID(pr.SiteCode, pr.Year) + 1;
            pr.ReferenceNo = "MI/" + pr.Year + "/S-" + pr.SiteCode + "/" + pr.RID;
            pr.CurrentOwnerID = "dba";
            db.UpdateLastPrices(pr);
            db.PRs.AddObject(pr);
            db.SaveChanges();
            return provider.GetPRbyId(pr.ID);
        }

        public MPR SplitPR(MPR NewPR, int OldPRID, ObservableCollection<int> OldPRLines)
        {
            var year = DateTime.UtcNow.AddHours(5.5).Year;
            NewPR.UpdateLineNumbers();
            var pr = ConvertToPR(NewPR);
            pr.RaisedOn = DateTime.UtcNow.AddHours(5.5);
            pr.Year = year;
            pr.SiteID = NewPR.SiteID;
            pr.SiteCode = db.GetSiteCode(NewPR.SiteID);
            pr.RID = db.GetMaxPRsRefID(pr.SiteCode, pr.Year) + 1;
            pr.ReferenceNo = "MI/" + pr.Year + "/S-" + pr.SiteCode + "/" + pr.RID;
            pr.CurrentOwnerID = "dba";
            db.UpdateLastPrices(pr);
            db.PRs.AddObject(pr);
            db.SaveChanges();
            foreach (var LineID in OldPRLines)
            {
                var data = db.PRDetails.Single(x => x.PRID == OldPRID && x.ID == LineID);
                if (data != null)
                {
                    db.PRDetails.DeleteObject(data);
                    db.SaveChanges();
                }
            }
            return provider.GetPRbyId(pr.ID);
        }

        public PR CopyPRObject(PR x)
        {
            PR newpr = new PR()
            {
                ApprovedBy = x.ApprovedBy,
                ApprovedOn = x.ApprovedOn,
                ContactPersonName = x.ContactPersonName,
                ContactPersonPhone = x.ContactPersonPhone,
                ConvertedTOPO = x.ConvertedTOPO,
                CostCenterID = x.CostCenterID,
                CurrentOwnerID = x.CurrentOwnerID,
                Frieght = x.Frieght,
                ID = x.ID,
                IsApproved = x.IsApproved,
                IsReqApproved = x.IsReqApproved,
                IsVerified = x.IsVerified,
                NetAmount = x.NetAmount,
                PaymentTerms = x.PaymentTerms,
                PCST = x.PCST,
                POID = x.POID,
                PurchasedForUserID = x.PurchasedForUserID,
                PurchaseType = x.PurchaseType,
                PurchaseTypeID = x.PurchaseTypeID,
                PVat = x.PVat,
                RaisedBy = x.RaisedBy,
                RaisedOn = x.RaisedOn,
                Remarks = x.Remarks,
                ReqApprovedBy = x.ReqApprovedBy,
                ReqApprovedOn = x.ReqApprovedOn,
                SiteCode = x.SiteCode,
                SiteID = x.SiteID,
                SubTotal = x.SubTotal,
                Tax = x.Tax,
                Total = x.Total,
                TotalDiscount = x.TotalDiscount,
                TotalPrice = x.TotalPrice,
                VendorID = x.VendorID,
                VerifiedBy = x.VerifiedBy,
                VerifiedOn = x.VerifiedOn,
                Status = x.Status,
                IsClosed = x.IsClosed,
                VerificationRemarks = x.VerificationRemarks,
                ReqApprovalRemarks = x.ReqApprovalRemarks,
                ApprovalRemarks = x.ApprovalRemarks
            };
            return newpr;
        }

        public PRDetail CopyPRLineObject(PRDetail x)
        {
            PRDetail newprline = new PRDetail()
            {
                AdjustedQty = x.AdjustedQty,
                BrandID = x.BrandID,
                ClosingRemarks = x.ClosingRemarks,
                ConsumableType = x.ConsumableType,
                DeliveryDate = x.DeliveryDate,
                Discount = x.Discount,
                DiscountType = x.DiscountType,
                LineID = x.LineID,
                IsClosed = x.IsClosed,
                ItemID = x.ItemID,
                LastPrice = x.LastPrice,
                LinePrice = x.LinePrice,
                NetRejectedQty = x.NetRejectedQty,
                PRID = x.PRID,
                OriginalQty = x.OriginalQty,
                Qty = x.Qty,
                Remarks = x.Remarks,
                SizeID = x.SizeID,
                TotalLinePrice = x.TotalLinePrice,
                TotalReceivedQty = x.TotalReceivedQty,
                UnitPrice = x.UnitPrice,
                InStoreQty = x.InStoreQty
            };
            return newprline;
        }

        public MPR SplitPRwithPartialQty(int OldPRID, ObservableCollection<MSplittedLines> NewPRLines)
        {
            var year = DateTime.UtcNow.AddHours(5.5).Year;
            var oldpr = db.PRs.Single(x => x.ID == OldPRID);
            PR newpr = new PR();
            newpr = CopyPRObject(oldpr);

            var FullSplittedLines = NewPRLines.Where(x => !x.IsPartial).ToList();
            var PartialSplittedLines = NewPRLines.Where(x => x.IsPartial && x.ConfirmedQty > 0).ToList();

            if (FullSplittedLines.Count > 0)
            {
                foreach (var line in FullSplittedLines)
                {
                    var oldline = db.PRDetails.Single(x => x.ID == line.ID && x.PRID == OldPRID);

                    PRDetail newline = new PRDetail();
                    newline = CopyPRLineObject(oldline);
                    newpr.PRDetails.Add(newline);

                    oldline.Qty = 0;
                    oldline.InStoreQty = 0;
                    oldline.PRDetails_AdditionalInfo.IsDeleted = true;
                }
            }
            if (PartialSplittedLines.Count > 0)
            {
                foreach (var line in PartialSplittedLines)
                {
                    var oldline = db.PRDetails.Single(x => x.ID == line.ID && x.PRID == OldPRID);
                    oldline.Qty = line.DiffQty;

                    PRDetail newline = new PRDetail();
                    newline = CopyPRLineObject(oldline);
                    newline.Qty = line.ConfirmedQty;
                    newline.InStoreQty = 0;
                    newpr.PRDetails.Add(newline);
                }
            }
            newpr.RaisedOn = DateTime.UtcNow.AddHours(5.5);
            newpr.Year = year;
            newpr.RID = db.GetMaxPRsRefID(newpr.SiteCode, newpr.Year) + 1;
            newpr.ReferenceNo = "MI/" + newpr.Year + "/S-" + newpr.SiteCode + "/" + newpr.RID;

            int i = 1;
            foreach (var item in newpr.PRDetails)
            {
                item.ID = i;
                i++;
            }
            db.UpdateLastPrices(newpr);
            db.PRs.AddObject(newpr);
            db.SaveChanges();
            return provider.GetPRbyId(newpr.ID);
        }

        public MPRLine ConvertfromSplittedLines(MSplittedLines x)
        {
            MPRLine ln = new MPRLine()
            {
                LineID = x.LineID,
                ItemID = x.ItemID,
                Item = x.Item,
                SizeID = x.SizeID,
                Size = x.Size,
                BrandID = x.BrandID,
                Brand = x.Brand,
                Quantity = x.ConfirmedQty
            };
            return ln;
        }

        public MPR UpdatePR(MPR UPR)
        {
            var pr = db.PRs.Single(x => x.ID == UPR.PRNumber);
            pr.VendorID = UPR.VendorID;
            pr.PVat = UPR.pVAT;
            pr.PCST = UPR.pCST;
            pr.Frieght = UPR.Frieght;
            pr.Remarks = UPR.PRRemarks;
            pr.Tax = UPR.TAX;
            pr.ContactPersonName = UPR.ContactPerson;
            pr.ContactPersonPhone = UPR.ContactMobile;
            pr.PaymentTerms = UPR.PaymentTerms;

            pr.SubTotal = UPR.TotalValue;
            pr.TotalPrice = UPR.TotalValue + UPR.TAX;
            pr.Total = UPR.GrandTotal;
            pr.TotalDiscount = UPR.TotalDiscount;
            pr.NetAmount = UPR.NetAmount;
            pr.IsClosed = UPR.IsClosed;
            pr.VerificationRemarks = UPR.VerificationRemarks;
            pr.ReqApprovalRemarks = UPR.ReqApprovalRemarks;
            pr.ApprovalRemarks = UPR.ApprovalRemarks;

            List<PRDetail> prsToAdd = new List<PRDetail>();         //List of New PR Lines, which are to be Added
            List<PRDetail> prsToRemove = new List<PRDetail>();      //List of Old PR Lines, which are to be Deleted

            foreach (var line in UPR.PRLines)
            {
                PRDetail prline;
                prline = pr.PRDetails.SingleOrDefault(x => x.ID == line.ID);

                if (prline == null)     // If New Line is added
                {
                    prsToAdd.Add(AddNewLine(pr, line));
                    continue;
                }
                prline.LineID = line.LineID;
                prline.OriginalQty = line.OriginalQty;
                prline.Qty = line.Quantity;
                prline.UnitPrice = line.UnitPrice;
                prline.LinePrice = line.LinePrice;
                prline.ItemID = line.ItemID;
                prline.SizeID = line.SizeID;
                prline.BrandID = line.BrandID;
                prline.Remarks = line.LineRemarks;
                prline.Discount = line.Discount;
                prline.DiscountType = line.DiscountType;
                prline.Excise=line.Excise;
                prline.ExciseIn = line.ExciseIn;
                prline.TotalLinePrice = line.TotalLinePrice;
                prline.ConsumableType = line.ConsumableType;
                prline.InStoreQty = line.AvailableQty;
            }

            /* Adding PR Line in PRDetails Table*/
            foreach (var line in prsToAdd)
            {
                pr.PRDetails.Add(line);
            }

            /* Removing PR Line from PRDetails_AdditionalInfo Table*/

            /*Preparing List of PRs to be removed*/
            var list = pr.PRDetails.Where(x => !UPR.PRLines.Select(y => y.ID).Contains(x.ID));
            prsToRemove.AddRange(list);

            /*Editing in Database*/
            foreach (var line in prsToRemove)
            {
                var delobj = db.PRDetails_AdditionalInfo.Single(x => x.ID == line.ID);
                delobj.IsDeleted = true;
                line.LineID = 0;
            }

            db.SaveChanges();
            return UPR;
        }

        private PRDetail AddNewLine(PR pr, MPRLine line)
        {
            PRDetail prline = new PRDetail();

            prline.DeliveryDate = DateTime.UtcNow.AddHours(5.5);
            prline.TotalReceivedQty = 0;
            prline.NetRejectedQty = 0;
            prline.LineID = line.LineID;
            prline.OriginalQty = line.OriginalQty;
            prline.Qty = line.Quantity;
            prline.UnitPrice = line.UnitPrice;
            prline.LinePrice = line.LinePrice;
            prline.ItemID = line.ItemID;
            prline.SizeID = line.SizeID;
            prline.BrandID = line.BrandID;
            prline.Remarks = line.LineRemarks;
            prline.Discount = line.Discount;
            prline.DiscountType = line.DiscountType;
            prline.Excise=line.Excise;
            prline.ExciseIn = line.ExciseIn;
            prline.TotalLinePrice = line.TotalLinePrice;
            prline.ConsumableType = line.ConsumableType;

            return prline;
        }

        public void DeletePR(int prid)
        {
            var pr = db.PRs.Single(x => x.ID == prid);
            var prlines = db.PRDetails.Where(x => x.PRID == prid);
            if (prlines != null)
            {
                foreach (var line in prlines)
                {
                    var obj = line.PRDetails_AdditionalInfo;
                    db.PRDetails_AdditionalInfo.DeleteObject(obj);
                    db.PRDetails.DeleteObject(line);
                }
            }
            db.PRs.DeleteObject(pr);
            db.SaveChanges();
        }

        public void EmailDraftPR(int PRID)
        {
            var emailid = Services.AppSettingsService.GetLatestAppSettings().Email;
            var pr = db.PRs.Single(x => x.ID == PRID);

            /*Setting Email Address for MIVerify/Approve/POApprove Emails*/
            if (!pr.IsVerified && !pr.IsReqApproved && !pr.IsApproved && pr.Status == "Draft")
            {
                var empid = db.EmailScenarios.Single(x => x.Scenario == "MI-Draft").EmpID;
                string empemailids = db.Employees.Single(x => x.ID == empid).EmployeePersonalDetail.EmailID
                    + ";" + db.Employees.Single(x => x.ID == empid).EmployeePersonalDetail.EmailID2;
                if (!String.IsNullOrWhiteSpace(empemailids))
                    emailid = empemailids;
            }
            Emailer.Emailer.EmailDraftPR(emailid, pr);
        }

        public void EmailPR(int PRID)
        {
            var emailid = Services.AppSettingsService.GetLatestAppSettings().Email;
            var pr = db.PRs.Single(x => x.ID == PRID);

            /*Setting Email Address for MIVerify/Approve/POApprove Emails*/
            if (!pr.IsVerified && !pr.IsReqApproved && !pr.IsApproved)
            {
                var empid = db.EmailScenarios.Single(x => x.Scenario == "MI-Verify").EmpID;
                string empemailids = db.Employees.Single(x => x.ID == empid).EmployeePersonalDetail.EmailID
                    + ";" + db.Employees.Single(x => x.ID == empid).EmployeePersonalDetail.EmailID2;
                if (!String.IsNullOrWhiteSpace(empemailids))
                    emailid = empemailids;
            }
            else if (pr.IsVerified && !pr.IsReqApproved && !pr.IsApproved)
            {
                var empid = db.EmailScenarios.Single(x => x.Scenario == "MI-Approve").EmpID;
                string empemailids = db.Employees.Single(x => x.ID == empid).EmployeePersonalDetail.EmailID
                    + ";" + db.Employees.Single(x => x.ID == empid).EmployeePersonalDetail.EmailID2;
                if (!String.IsNullOrWhiteSpace(empemailids))
                    emailid = empemailids;
            }
            else if (pr.IsVerified && pr.IsReqApproved && !pr.IsApproved)
            {
                var empid = db.EmailScenarios.Single(x => x.Scenario == "PO-Approve").EmpID;
                string empemailids = db.Employees.Single(x => x.ID == empid).EmployeePersonalDetail.EmailID
                    + ";" + db.Employees.Single(x => x.ID == empid).EmployeePersonalDetail.EmailID2;
                if (!String.IsNullOrWhiteSpace(empemailids))
                    emailid = empemailids;
            }

            Emailer.Emailer.EmailPO(emailid, pr, !pr.IsApproved);
        }

        public string ConfirmEmail(int POID)
        {
            var pr = db.PRs.Single(x => x.POID == POID);
            string email = pr.Vendor.Email;
            return email;
        }

        public void EmailtoVendor(int POID)
        {
            var pr = db.PRs.Single(x => x.POID == POID);
            var emailid = pr.Vendor.Email;
            try
            {
                if (emailid == null)
                {
                    throw new Exception("This Vendor do not carry any Email Address!");
                }
                else
                {
                    Emailer.Emailer.SMSPO(emailid, pr, !pr.IsApproved);
                }
            }
            catch { }
            Emailer.Emailer.EmailPO(emailid, pr, !pr.IsApproved);
        }

        public MPR ApprovePR(int PRID, string ApprovedBy, DateTime? ApprovedOn, string ApprovalRemarks)
        {
            var obj = db.PRs.Single(x => x.ID == PRID);
            if (obj.IsApproved)
                throw new Exception("Already Approved!");
            obj.IsApproved = true;
            obj.ApprovedBy = ApprovedBy;
            obj.ApprovedOn = ApprovedOn;
            if (!String.IsNullOrWhiteSpace(ApprovalRemarks))
            {
                obj.ApprovalRemarks = obj.ApprovalRemarks;
            }
            obj.Status = "POApproved";
            db.SaveChanges();

            return provider.GetPRbyId(PRID);
        }

        public MPR VerifyPR(int PRID, string VerifiedBy, DateTime? VerifiedOn, string VerificationRemarks)
        {
            var obj = db.PRs.Single(x => x.ID == PRID);
            if (obj.IsVerified)
                throw new Exception("Already Verified!");
            obj.IsVerified = true;
            obj.VerifiedBy = VerifiedBy;
            obj.VerifiedOn = VerifiedOn;
            if (!String.IsNullOrWhiteSpace(VerificationRemarks))
            {
                obj.VerificationRemarks = VerificationRemarks;
                obj.ApprovalRemarks = obj.ApprovalRemarks + VerificationRemarks;
            }
            obj.Status = "MIVerified";
            db.SaveChanges();

            return provider.GetPRbyId(PRID);
        }

        public MPR InitialApprovePR(int PRID, string ReqApprovedBy, DateTime? ReqApprovedOn, string InApprovalRemarks)
        {
            var obj = db.PRs.Single(x => x.ID == PRID);
            if (obj.IsReqApproved)
                throw new Exception("MI Already Approved!");
            obj.IsReqApproved = true;
            obj.ReqApprovedBy = ReqApprovedBy;
            obj.ReqApprovedOn = ReqApprovedOn;
            if (!String.IsNullOrWhiteSpace(InApprovalRemarks))
            {
                obj.ReqApprovalRemarks = InApprovalRemarks;
                obj.ApprovalRemarks = obj.ApprovalRemarks + InApprovalRemarks;
            }
            obj.Status = "MIApproved";
            db.SaveChanges();

            return provider.GetPRbyId(PRID);
        }

        public MPR ChangesStateofPR(int PRID, string State)
        {
            var obj = db.PRs.Single(x => x.ID == PRID);
            obj.Status = State;
            if (State == "SubmittedForVerification")
                obj.ApprovalRemarks = "Verification Remarks: ";
            if (State == "SubmittedForMIApproval")
                obj.ApprovalRemarks = obj.ApprovalRemarks + "\nMI Approval Remarks: ";
            if (State == "SubmittedForPOApproval")
                obj.ApprovalRemarks = obj.ApprovalRemarks + "\nPO Approval Remarks: ";
            db.SaveChanges();

            return provider.GetPRbyId(PRID);
        }

        public void AcknowledgePO(int poid, string remark)
        {
            var po = db.POs.Single(x => x.ID == poid);
            po.AcknowledgedOn = DateTime.UtcNow.AddHours(5.5);
            po.IsAcknowledged = true;
            po.Ack_Remarks = remark;
            db.SaveChanges();
        }

        public int ConvertToPO(int prid)
        {
            var pr = db.PRs.Single(x => x.ID == prid);
            if (!pr.IsApproved)
                throw new Exception("MI Not Approved!");
            POs po = new POs();
            po.ID = db.GetMaxPOID() + 1;
            po.Year = pr.Year;
            po.SiteID = pr.SiteCode;
            po.RID = db.GetMaxPORID(po.SiteID, po.Year) + 1;
            po.Display = "PO/" + po.Year + "/S-" + pr.SiteCode + "/" + po.RID;
            po.IsAcknowledged = false;
            po.Tax = pr.Tax;
            po.Frieght = (double)pr.Frieght;
            po.Total = (double)pr.Total;
            po.NetTotal = pr.SubTotal;
            po.Remarks = pr.Remarks;
            po.CreatedOn = DateTime.UtcNow.AddHours(5.5);
            po.CreatedBy = pr.RaisedBy;
            po.ContactPersonName = pr.ContactPersonName;
            po.ContactPersonPhone = pr.ContactPersonPhone;
            po.TotalDiscount = pr.TotalDiscount;
            po.NetAmount = pr.NetAmount;
            pr.ConvertedTOPO = true;
            pr.POs = po;
            pr.Status = "ConvertedToPO";
            pr.IsClosed = true;
            db.SaveChanges();
            return po.ID;
        }

        public IEnumerable<PendingPOInfo> GetPendingInfo(int vendorid, int siteid, int itemid, int? sizeid, int? brandid)
        {
            var data = db.PRDetails.AsQueryable();
            var data2 = data.Where(x => x.PR.VendorID == vendorid && x.PR.SiteID == siteid
                && x.PR.POID != null
                && x.ItemID == itemid
                //&& (x.SizeID == null || x.SizeID == sizeid)
                //&& (x.BrandID == null || x.BrandID == brandid)
                && x.Qty > x.TotalReceivedQty).ToList();
            var list = new List<PendingPOInfo>();
            if (data2.Count != 0)
            {
                foreach (var item in data2)
                {
                    var info = new PendingPOInfo()
                    {
                        PRID = item.PRID,
                        ItemID = item.ItemID,
                        SizeID = item.SizeID,
                        BrandID = item.BrandID,
                        PRLineID = item.ID,
                        POID = item.PR.POID.Value,
                        OrderedQty = ((item.Qty + item.PRDetails_AdditionalInfo.SplittedQty) - item.InStoreQty),
                        UnitPrice = item.UnitPrice,
                        LinePrice = item.LinePrice,
                        ConsumableType = item.ConsumableType
                    };
                    if (item.TotalReceivedQty.HasValue)
                    {
                        info.EarlierReceivedQty = item.TotalReceivedQty.Value;
                    }
                    if (item.PR.POs != null)
                    {
                        info.RefNo = item.PR.POs.Display;
                    }
                    list.Add(info);
                }
            }
            return list;
        }

        public IEnumerable<int> GetAllowedItemId(int vendorid, int siteid)
        {
            var data = db.PRDetails;
            var data2 = data.Where(x => x.PR.VendorID == vendorid
                && x.PR.SiteID == siteid && x.PR.POID != null
                && x.Qty > x.TotalReceivedQty)
                .Select(x => x.ItemID).Distinct()
                .ToList();
            return data2;
        }

        public MPR RejectPR(int PRID, string comments, string State)
        {
            var pr = db.PRs.Single(x => x.ID == PRID);
            if (!String.IsNullOrWhiteSpace(pr.Remarks))
            {
                pr.Remarks = "Rejection Remarks: " + comments + "\n\n" + pr.Remarks;
            }
            else
            {
                pr.Remarks = "Rejection Remarks: " + comments;
            }
            pr.Status = State;
            db.SaveChanges();
            return provider.GetPRbyId(PRID);
        }

        public MPR CloseMI(string UserName, int PRID)
        {
            var pr = db.PRs.Single(x => x.ID == PRID);
            pr.Status = "Closed";
            pr.IsClosed = true;
            pr.Remarks = pr.Remarks + "\n\nClosed By:" + UserName;
            db.SaveChanges();

            return provider.GetPRbyId(pr.ID);
        }

        public MPO CopyPOfromMI(string UserName, MPR OldPR)
        {
            var year = DateTime.UtcNow.AddHours(5.5).Year;
            OldPR.UpdateLineNumbers();
            var pr = ConvertToPR(OldPR);

            PR NewPR = new PR();
            POs po = new POs();

            NewPR = CopyPRObject(pr);
            pr.RaisedBy = UserName;
            pr.RaisedOn = DateTime.UtcNow.AddHours(5.5);
            pr.Year = year;
            pr.SiteID = 3;
            pr.SiteCode = db.GetSiteCode(3);
            pr.RID = db.GetMaxPRsRefID(pr.SiteCode, pr.Year) + 1;
            pr.ReferenceNo = "MI/" + pr.Year + "/S-" + pr.SiteCode + "/" + pr.RID;
            pr.CurrentOwnerID = "dba";
            db.UpdateLastPrices(pr);
            db.PRs.AddObject(pr);
            db.SaveChanges();

            po.ID = db.GetMaxPOID() + 1;
            po.Year = pr.Year;
            po.SiteID = pr.SiteCode;
            po.CreatedBy = UserName;
            po.RID = db.GetMaxPORID(po.SiteID, po.Year) + 1;
            po.Display = "PO/" + po.Year + "/S-" + pr.SiteCode + "/" + po.RID;
            po.IsAcknowledged = false;
            po.Tax = pr.Tax;
            po.Frieght = (double)pr.Frieght;
            po.Total = (double)pr.Total;
            po.NetTotal = pr.SubTotal;
            po.Remarks = pr.Remarks;
            po.CreatedOn = DateTime.UtcNow.AddHours(5.5);
            po.CreatedBy = pr.RaisedBy;
            po.ContactPersonName = pr.ContactPersonName;
            po.ContactPersonPhone = pr.ContactPersonPhone;
            po.TotalDiscount = pr.TotalDiscount;
            po.NetAmount = pr.NetAmount;
            pr.ConvertedTOPO = true;
            pr.POs = po;
            pr.Status = "ConvertedToPO";
            pr.IsClosed = true;
            db.SaveChanges();

            return provider.GetPObyId(po.ID);
        }

        public MPR SubmitToHOPurchase(string UserName, MPR OldPR)
        {
            var year = DateTime.UtcNow.AddHours(5.5).Year;
            OldPR.UpdateLineNumbers();

            //var pr = ConvertToPR(OldPR);
            PR pr = new PR()
            {
                ID = OldPR.PRNumber,
                CurrentOwnerID = OldPR.CurrentOwnerID,
                VendorID = OldPR.VendorID,
                VerifiedBy = OldPR.VerifiedBy,
                VerifiedOn = OldPR.VerifiedOn,
                ReqApprovedOn = OldPR.ReqApprovedOn,
                ReqApprovedBy = OldPR.ReqApprovedBy,
                ApprovedBy = OldPR.ApprovedBy,
                ApprovedOn = OldPR.ApprovedOn,
                Frieght = OldPR.Frieght,
                SubTotal = OldPR.TotalValue,
                TotalPrice = OldPR.TotalValue + OldPR.TAX,
                Total = OldPR.GrandTotal,
                PCST = OldPR.pCST,
                PVat = OldPR.pVAT,
                Tax = OldPR.TAX,
                IsVerified = OldPR.IsVerified,
                IsReqApproved = OldPR.IsReqApproved,
                IsApproved = OldPR.IsApproved,
                SiteID = OldPR.SiteID,
                PurchasedForUserID = OldPR.Purchaser,
                PurchaseTypeID = OldPR.PurchaseType,
                Remarks = OldPR.PRRemarks,
                RaisedBy = OldPR.RaisedBy,
                RaisedOn = OldPR.RaisedOn,
                ConvertedTOPO = OldPR.ConvertedToPO,
                ContactPersonName = OldPR.ContactPerson,
                ContactPersonPhone = OldPR.ContactMobile,
                PaymentTerms = OldPR.PaymentTerms,
                TotalDiscount = OldPR.TotalDiscount,
                NetAmount = OldPR.NetAmount,
                Status = OldPR.PRStatus,
                IsClosed = OldPR.IsClosed,
                VerificationRemarks = OldPR.VerificationRemarks,
                ReqApprovalRemarks = OldPR.ReqApprovalRemarks,
                ApprovalRemarks = OldPR.ApprovalRemarks
            };
            pr.RaisedBy = UserName;
            pr.RaisedOn = DateTime.UtcNow.AddHours(5.5);
            pr.Year = year;
            pr.SiteID = 3;
            pr.SiteCode = db.GetSiteCode(3);
            pr.RID = db.GetMaxPRsRefID(pr.SiteCode, pr.Year) + 1;
            pr.ReferenceNo = "MI/" + pr.Year + "/S-" + pr.SiteCode + "/" + pr.RID;
            pr.CurrentOwnerID = "dba";
            pr.Status = "SubmittedToPurchase";
            db.UpdateLastPrices(pr);
            db.PRs.AddObject(pr);
            db.SaveChanges();

            foreach (var line in OldPR.PRLines.Where(x => x.UnavailableQty > 0))
            {
                var oldprdetail = db.PRDetails.Single(x => x.PRID == line.PRID && x.ID == line.ID);
                /*Setting Purchase Qty to '0' and Store Qty to Equal to the Quantity*/
                oldprdetail.InStoreQty = oldprdetail.Qty + oldprdetail.PRDetails_AdditionalInfo.SplittedQty;
                db.SaveChanges();

                PRDetail prline = new PRDetail()
                {
                    LineID = line.LineID,
                    DeliveryDate = line.DeliveryDate,
                    ItemID = line.ItemID,
                    SizeID = line.SizeID,
                    BrandID = line.BrandID,
                    LinePrice = line.LinePrice,
                    UnitPrice = line.UnitPrice,
                    Remarks = line.LineRemarks,
                    LastPrice = line.LastPrice,
                    DiscountType = line.DiscountType,
                    Discount = line.Discount,
                    Excise = line.Excise,
                    ExciseIn = line.ExciseIn,
                    TotalLinePrice = line.TotalLinePrice,
                    ConsumableType = line.ConsumableType,
                    AdjustedQty = 0,
                    IsClosed = false,
                    NetRejectedQty = 0,
                    TotalReceivedQty = 0,
                    InStoreQty = 0,
                    PRID = pr.ID,
                    /*Setting Quantity of New PR equal to the Purchase Qty of Old PR*/
                    OriginalQty = line.UnavailableQty,
                    Qty = line.UnavailableQty,
                };
                db.PRDetails.AddObject(prline);
                db.SaveChanges();

                /*Recording Spit into PRSplits Table*/
                PRSplit split = new PRSplit();
                split.OldPRID = OldPR.PRNumber;
                split.OldPRLineID = line.ID;
                split.OldPRRefNo = OldPR.ReferenceNo;
                split.NewPRID = pr.ID;
                split.NewPRLineID = prline.ID;    //db.PRDetails.Single(x => x.PRID == pr.ID && x.LineID == line.LineID).ID;
                split.NewPRRefNo = pr.ReferenceNo;
                split.OriginalQty = line.OriginalQty;
                split.SplittedQty = line.Quantity;   //db.PRDetails.Single(x => x.PRID == pr.ID && x.LineID == line.LineID).Qty;
                split.SplittedBy = UserName;
                split.SplittedOn = DateTime.UtcNow.AddHours(5.5);
                split.CurrentState = "SubmittedToHOPurchase";

                db.PRSplits.AddObject(split);
                db.SaveChanges();
            }
            return provider.GetPRbyId(pr.ID);
        }
    }

    public static class MPRExtentions
    {
        public static MPRLine AddNewLine(this MPR mpr)
        {
            if (mpr.PRLines == null)
                mpr.PRLines = new ObservableCollection<MPRLine>();
            var line = new MPRLine();
            mpr.PRLines.Add(line);
            mpr.UpdateLineNumbers();
            return line;
        }

        public static int UpdateLineNumbers(this MPR mpr)
        {
            if (mpr.PRLines == null) return 0;
            int i = 1;
            foreach (var item in mpr.PRLines)
            {
                item.LineID = i;
                i++;
            }
            return i - 1;
        }
    }
}