﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using System.ServiceModel;
using System.Web.Security;

namespace SIMS.MM.BLL.Services
{
    using DAL;
    using MODEL;

    [ServiceContract]
    public interface IAppsettings
    {
        [OperationContract]
        MAppSettings GetAppSettings();
    }

    [ServiceContract]
    public interface IAuthenticationManager
    {
        [OperationContract]
        bool ValidateUser(string username, string password);

        [OperationContract]
        void CreateUser(string username, string password, string email, string passwordQuestion, string passwordAnswer, bool isApproved, out MembershipCreateStatus status);

        [OperationContract]
        MembershipUser GetUser(string username);
    }
}
