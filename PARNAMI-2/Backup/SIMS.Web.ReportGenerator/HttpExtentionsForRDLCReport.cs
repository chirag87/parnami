﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using Microsoft.Reporting.WebForms;

namespace SIMS
{
    public static class HttpExtentionsForRDLCReport
    {
        public static void PushRDLC_ExcelReport(this HttpContext context, string reportPath, List<ReportDataSource> dataSources)
        {
            context.Response.ClearHeaders();

            string fname = context.Request.QueryString["fname"];
            if (String.IsNullOrWhiteSpace(fname))
                fname = Guid.NewGuid().ToString();

            LocalReport report = new LocalReport();
            report.ReportPath = context.Server.MapPath(reportPath);
            report.EnableExternalImages = true;

            foreach (var item in dataSources)
            {
                report.DataSources.Add(item);
            }

            string reportType = "Excel";
            string mimeType;
            string encoding;
            string fileNameExtension;

            Warning[] warnings;
            string[] stream;
            byte[] renderedBytes;

            renderedBytes = report.Render(
                reportType,
                null,
                out mimeType,
                out encoding,
                out fileNameExtension,
                out stream,
                out warnings);

            context.Response.ContentType = mimeType;
            context.Response.AddHeader("Content-Disposition", "attachment;filename=" + fname + ".xls");
            context.Response.BinaryWrite(renderedBytes);
            context.Response.End();
        }

        public static void PushRDLC_PDFReport(this HttpContext context, string reportPath, List<ReportDataSource> dataSources)
        {
            LocalReport report = new LocalReport();
            report.ReportPath = context.Server.MapPath(reportPath);
            report.EnableExternalImages = true;

            string fname = context.Request.QueryString["fname"];
            if (String.IsNullOrWhiteSpace(fname))
                fname = Guid.NewGuid().ToString();

            foreach (var item in dataSources)
            {
                report.DataSources.Add(item);
            }

            string reportType = "PDF";
            string mimeType;
            string encoding;
            string fileNameExtension;

            string deviceInfo =
                "<DeviceInfo>" +
                "   <OutputFormat>PDF</OutputFormat>" +
                "   <PageWidth>8.5in</PageWidth>" +
                "   <PageHeight>11in</PageHeight>" +
                "   <MarginTop>0.3in</MarginTop>" +
                "   <MarginLeft>0.25in</MarginLeft>" +
                "   <MarginRight>0.25in</MarginRight>" +
                "   <MarginBottom>0.3in</MarginBottom>" +
                "</DeviceInfo>";
            Warning[] warnings;
            string[] stream;
            byte[] renderedBytes;

            renderedBytes = report.Render(
                reportType,
                deviceInfo,
                out mimeType,
                out encoding,
                out fileNameExtension,
                out stream,
                out warnings);

            context.Response.ContentType = mimeType;
            context.Response.AddHeader("Content-Disposition", "attachment;filename=" + fname + ".pdf");
            context.Response.BinaryWrite(renderedBytes);
            context.Response.End();
        }
        
        //public static void PushRDLC_PDFReport(this HttpContext context, string reportPath, List<ReportDataSource> dataSources)
        //{
        //    LocalReport report = new LocalReport();
        //    report.ReportPath = context.Server.MapPath(reportPath);
        //    report.EnableExternalImages = true;

        //    foreach (var item in dataSources)
        //    {
        //        report.DataSources.Add(item);
        //    }

        //    string reportType = "PDF";
        //    string mimeType;
        //    string encoding;
        //    string fileNameExtension;

        //    string deviceInfo =
        //        "<DeviceInfo>" +
        //        "   <OutputFormat>PDF</OutputFormat>" +
        //        "   <PageWidth>8.5in</PageWidth>" +
        //        "   <PageHeight>11in</PageHeight>" +
        //        "   <MarginTop>0.3in</MarginTop>" +
        //        "   <MarginLeft>0.25in</MarginLeft>" +
        //        "   <MarginRight>0.25in</MarginRight>" +
        //        "   <MarginBottom>0.3in</MarginBottom>" +
        //        "</DeviceInfo>";
        //    Warning[] warnings;
        //    string[] stream;
        //    byte[] renderedBytes;

        //    renderedBytes = report.Render(
        //        reportType,
        //        deviceInfo,
        //        out mimeType,
        //        out encoding,
        //        out fileNameExtension,
        //        out stream,
        //        out warnings);

        //    context.Response.BinaryWrite(renderedBytes);
        //    context.Response.End();
        //}
    }
}
