﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using SIMS.MM.BLL.Services;
using SIMS.MM.MODEL;
using System.Web.Security;
using System.Web;
using SIMS.MM.DAL;

namespace Contruction.Web.Services
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "LoginService" in code, svc and config file together.
    public class LoginService : ILoginService
    {
        AuthenticationManager manager = new AuthenticationManager();
        string Username = "";

        public SIMS.MM.MODEL.UserRights Login(string username, string password)
        {
            UserRights rights = new UserRights();
            rights.IsValid = manager.ValidateUser(username, password);
            if (rights.IsValid)
            {
                Username = username;
                rights.UserName = username;
                SetRightsForUser(rights);
            }
            else
            {
                rights.Status = "Username or password is invalid!";
                rights.SetAllCans(false);
            }
            return rights;
        }

        public static bool CanShowAllSites(string Username)
        {
            return Roles.IsUserInRole(Username, "dba") || Roles.IsUserInRole(Username, "AllSites");
        }

        public static string CurrentUser { get { return HttpContext.Current.User.Identity.Name; } }
        public static bool Authenticate() { if (HttpContext.Current.User.Identity.IsAuthenticated) return true; throw new Exception("User Not Authenticated"); }
        public static bool IsUserInRole(string role) { return HttpContext.Current.User.IsInRole(role); }

        bool IsInRole(string roleName)
        {
            return Roles.IsUserInRole(Username, roleName);
        }

        //bool IsInRoles(params string[] roleNames)
        //{
        //    foreach (var roleName in roleNames)
        //    {
        //        if (Roles.IsUserInRole(Username, roleName)) return true;
        //    }
        //    return false;
        //}

        List<string> myroles = new List<string>();
        bool IsInRoles(params string[] roleNames)
        {
            foreach (var roleName in roleNames)
            {
                if (myroles.Any(x => x.ToLower() == roleName.ToLower()))
                    return true;
            }
            return false;
        }

        void SetRightsForUser(UserRights rights)
        {
            //if (Roles.IsUserInRole(Username, "dba"))
            //{
            //    rights.SetAllCans(true);
            //    return;
            //}

            #region Old Rights & Roles
            //rights.CanVerifyMI = IsInRoles("PurchaseAdmin", "MIVerify");
            //rights.CanApprovePR = IsInRoles("PurchaseAdmin", "MIApprove");
            //rights.CanApprovePO = IsInRoles("PurchaseAdmin", "POApprove");
            //rights.CanRaisePR = IsInRoles("Purchaser", "PurchaseAdmin", "admin");
            //rights.CanRaisePO = IsInRoles("PurchaseAdmin", "admin");
            //rights.CanConsume = IsInRoles("Store", "admin", "StoreAdmin");
            //rights.CanReceive = IsInRoles("Store", "admin", "StoreAdmin");
            //rights.CanViewReports = IsInRoles("ReportingUser");
            //rights.CanManageUsers = IsInRoles("HR");
            //rights.CanViewAllSites = IsInRoles("AllSites");
            //rights.CanManageItems = IsInRoles("admin");
            //rights.CanManageVendors = IsInRoles("admin");
            //rights.CanManageSites = IsInRoles("admin");
            //rights.CanManageTransporter = IsInRoles("admin");
            //rights.CanAdmin = IsInRoles("admin");
            //rights.CanConvertToTO = IsInRoles("PurchaseAdmin", "admin");
            //rights.CanCreateMO = false;
            #endregion

            #region NEW RIGHTS & ROLE
            /********************************************************************************************************/
            /*NEW RIGHTS & ROLES*/
            /********************************************************************************************************/
            myroles = Roles.GetRolesForUser(rights.UserName).ToList();
            rights.MI_AllRights = IsInRoles("MI_AllRights", "admin", "dba");
            rights.MI_CanRaise = IsInRoles("MI_CanRaise", "MI_AllRights", "admin", "dba");
            rights.MI_CanViewAll = IsInRoles("MI_CanViewAll", "MI_AllRights", "admin", "dba");
            rights.MI_CanEdit = IsInRoles("MI_CanEdit", "MI_AllRights", "admin", "dba");
            rights.MI_CanSubmitForVerify = IsInRoles("MI_CanSubmitForVerify", "MI_AllRights", "admin", "dba");
            rights.MI_CanVerify = IsInRoles("MI_CanVerify", "MI_AllRights", "admin", "dba");
            rights.MI_CanSubmitForMIApprove = IsInRoles("MI_CanSubmitForMIApprove", "MI_AllRights", "admin", "dba");
            rights.MI_CanApproveMI = IsInRoles("MI_CanApproveMI", "MI_AllRights", "admin", "dba");
            rights.MI_CanSubmitToPurchase = IsInRoles("MI_CanSubmitToPurchase", "MI_AllRights", "admin", "dba");
            rights.MI_CanConvertToTO = IsInRoles("MI_CanConvertToTO", "MI_AllRights", "admin", "dba");
            rights.MI_CanSubmitForPOApprove = IsInRoles("MI_CanSubmitForPOApprove", "MI_AllRights", "admin", "dba");
            rights.MI_CanApprovePO = IsInRoles("MI_CanApprovePO", "MI_AllRights", "admin", "dba");
            rights.MI_CanConvertToPO = IsInRoles("MI_CanConvertToPO", "MI_AllRights", "admin", "dba");
            rights.MI_CanEmailMI = IsInRoles("MI_CanEmailMI", "MI_AllRights", "admin", "dba");

            rights.TO_AllRights = IsInRoles("TO_AllRights", "admin", "dba");
            rights.TO_CanRaise = IsInRoles("TO_CanRaise", "TO_AllRights", "admin", "dba");
            rights.TO_CanViewAll = IsInRoles("TO_CanViewAll", "TO_AllRights", "admin", "dba");
            rights.TO_CanRelease = IsInRoles("TO_CanRelease", "TO_AllRights", "admin", "dba");
            rights.TO_CanReceive = IsInRoles("TO_CanReceive", "TO_AllRights", "admin", "dba");
            rights.TO_CanClose = IsInRoles("TO_CanClose", "TO_AllRights", "admin", "dba");
            rights.TO_CanMergeTOs = IsInRoles("TO_CanMergeTOs", "TO_AllRights", "admin", "dba");

            rights.PO_AllRights = IsInRoles("PO_AllRights", "admin", "dba");
            rights.PO_CanEmailPO = IsInRoles("PO_CanEmailPO", "PO_AllRights", "admin", "dba");
            rights.PO_CanViewAll = IsInRoles("PO_CanViewAll", "PO_AllRights", "admin", "dba");

            rights.MRN_AllRights = IsInRoles("MRN_AllRights", "admin", "dba", "admin", "dba");
            rights.MRN_CanRaise = IsInRoles("MRN_CanRaise", "MRN_AllRights", "admin", "dba");
            rights.MRN_CanViewAll = IsInRoles("MRN_CanViewAll", "MRN_AllRights", "admin", "dba");

            rights.CP_AllRights = IsInRoles("CP_AllRights", "admin", "dba");
            rights.CP_CanRaise = IsInRoles("CP_CanRaise", "CP_AllRights", "admin", "dba");
            rights.CP_CanViewAll = IsInRoles("CP_CanViewAll", "CP_AllRights", "admin", "dba");

            rights.Consumtion_AllRights = IsInRoles("Consumtion_AllRights", "admin", "dba");
            rights.Consumtion_CanRaise = IsInRoles("Consumtion_CanRaise", "Consumtion_AllRights", "admin", "dba");
            rights.Consumtion_CanViewAll = IsInRoles("Consumtion_CanViewAll", "Consumtion_AllRights", "admin", "dba");

            rights.ReportingUser = IsInRoles("ReportingUser", "dba");

            rights.Employee_Admin = IsInRoles("EmployeeAdmin", "dba");
            rights.Employee_Attendance = IsInRoles("EmployeeAttendance", "EmployeeAdmin", "dba");

            rights.MasterAdmin = IsInRoles("MasterAdmin", "dba");

            rights.Vendors_AllRights = IsInRoles("Vendors_AllRights", "MasterAdmin", "dba");
            rights.Vendors_CanAddNew = IsInRoles("Vendors_CanAddNew", "Vendors_AllRights", "MasterAdmin", "dba");
            rights.Vendors_CanViewAll = IsInRoles("Vendors_CanViewAll", "Vendors_AllRights", "MasterAdmin", "dba");
            rights.Vendors_CanEdit = IsInRoles("Vendors_CanEdit", "Vendors_AllRights", "MasterAdmin", "dba");

            rights.Company_AllRights = IsInRoles("Company_AllRights", "MasterAdmin", "dba");
            rights.Company_CanAddNew = IsInRoles("Company_CanAddNew", "Company_AllRights", "MasterAdmin", "dba");
            rights.Company_CanViewAll = IsInRoles("Company_CanViewAll", "Company_AllRights", "MasterAdmin", "dba");
            rights.Company_CanEdit = IsInRoles("Company_CanEdit", "Company_AllRights", "MasterAdmin", "dba");

            rights.Clients_AllRights = IsInRoles("Clients_AllRights", "MasterAdmin", "dba");
            rights.Clients_CanAddNew = IsInRoles("Clients_CanAddNew", "Clients_AllRights", "MasterAdmin", "dba");
            rights.Clients_CanViewAll = IsInRoles("Clients_CanViewAll", "Clients_AllRights", "MasterAdmin", "dba");
            rights.Clients_CanEdit = IsInRoles("Clients_CanEdit", "Clients_AllRights", "MasterAdmin", "dba");

            rights.CanViewClientsandSitesMenu = IsInRoles("Clients_CanViewAll", "Sites_CanViewAll", "MasterAdmin", "dba");

            rights.Sites_AllRights = IsInRoles("Sites_AllRights", "MasterAdmin", "dba");
            rights.Sites_CanAddNew = IsInRoles("Sites_CanAddNew", "Sites_AllRights", "MasterAdmin", "dba");
            rights.Sites_CanViewAll = IsInRoles("Sites_CanViewAll", "Sites_AllRights", "MasterAdmin", "dba");
            rights.Sites_CanEdit = IsInRoles("Sites_CanEdit", "Sites_AllRights", "MasterAdmin", "dba");
            rights.Sites_CanViewAllDisplaySites = IsInRoles("AllSites", "admin", "dba");

            rights.Transporters_AllRights = IsInRoles("Transporters_AllRights", "MasterAdmin", "dba");
            rights.Transporters_CanAddNew = IsInRoles("Transporters_CanAddNew", "Transporters_AllRights", "MasterAdmin", "dba");
            rights.Transporters_CanViewAll = IsInRoles("Transporters_CanViewAll", "Transporters_AllRights", "MasterAdmin", "dba");
            rights.Transporters_CanEdit = IsInRoles("Transporters_CanEdit", "Transporters_AllRights", "MasterAdmin", "dba");

            rights.Items_AllRights = IsInRoles("Items_AllRights", "MasterAdmin", "dba");
            rights.Items_CanAddNew = IsInRoles("Items_CanAddNew", "Items_AllRights", "MasterAdmin", "dba");
            rights.Items_CanViewAll = IsInRoles("Items_CanViewAll", "Items_AllRights", "MasterAdmin", "dba");
            rights.Items_CanEdit = IsInRoles("Items_CanEdit", "Items_AllRights", "MasterAdmin", "dba");

            rights.ItemCategories_AllRights = IsInRoles("ItemCategories_AllRights", "MasterAdmin", "dba");
            rights.ItemCategories_CanAddNew = IsInRoles("ItemCategories_CanAddNew", "ItemCategories_AllRights", "MasterAdmin", "dba");
            rights.ItemCategories_CanViewAll = IsInRoles("ItemCategories_CanViewAll", "ItemCategories_AllRights", "MasterAdmin", "dba");
            rights.ItemCategories_CanEdit = IsInRoles("ItemCategories_CanEdit", "ItemCategories_AllRights", "MasterAdmin", "dba");

            rights.Sizes_AllRights = IsInRoles("Sizes_AllRights", "MasterAdmin", "dba");
            rights.Sizes_CanAddNew = IsInRoles("Sizes_CanAddNew", "Sizes_AllRights", "MasterAdmin", "dba");
            rights.Sizes_CanViewAll = IsInRoles("Sizes_CanViewAll", "Sizes_AllRights", "MasterAdmin", "dba");
            rights.Sizes_CanEdit = IsInRoles("Sizes_CanEdit", "Sizes_AllRights", "MasterAdmin", "dba");

            rights.CanViewItemSizeandMakeMenu = IsInRoles("Sizes_CanViewAll", "Makes_CanViewAll", "MasterAdmin", "dba");

            rights.Makes_AllRights = IsInRoles("Makes_AllRights", "MasterAdmin", "dba");
            rights.Makes_CanAddNew = IsInRoles("Makes_CanAddNew", "Makes_AllRights", "MasterAdmin", "dba");
            rights.Makes_CanViewAll = IsInRoles("Makes_CanViewAll", "Makes_AllRights", "MasterAdmin", "dba");
            rights.Makes_CanEdit = IsInRoles("Makes_CanEdit", "Makes_AllRights", "MasterAdmin", "dba");

            rights.MU_AllRights = IsInRoles("MU_AllRights", "MasterAdmin", "dba");
            rights.MU_CanAddNew = IsInRoles("MU_CanAddNew", "MU_AllRights", "MasterAdmin", "dba");
            rights.MU_CanViewAll = IsInRoles("MU_CanViewAll", "MU_AllRights", "MasterAdmin", "dba");
            rights.MU_CanEdit = IsInRoles("MU_CanEdit", "MU_AllRights", "MasterAdmin", "dba");

            rights.Pricings_AllRights = IsInRoles("Pricings_AllRights", "MasterAdmin", "dba");
            rights.Pricings_CanAddNew = IsInRoles("Pricings_CanAddNew", "Pricings_AllRights", "MasterAdmin", "dba");
            rights.Pricings_CanViewAll = IsInRoles("Pricings_CanViewAll", "Pricings_AllRights", "MasterAdmin", "dba");
            rights.Pricings_CanEdit = IsInRoles("Pricings_CanEdit", "Pricings_AllRights", "MasterAdmin", "dba");

            rights.HR = IsInRoles("HR", "dba");
            rights.Users_CanAddNew = IsInRoles("Users_CanAddNew", "HR", "dba");
            rights.Users_CanManageRoles = IsInRoles("Users_CanManageRoles", "HR", "dba");
            rights.Users_CanManageSites = IsInRoles("Users_CanManageSites", "HR", "dba");
            rights.Users_CanBlockUser = IsInRoles("Users_CanBlockUser", "HR", "dba");
            /********************************************************************************************************/
            #endregion

            parnamidb1Entities db = new parnamidb1Entities();
            rights.AllowedSiteIDs = db.GetAllowedSiteIDs(Username, rights.Sites_CanViewAllDisplaySites).ToArray();
        }

        public IEnumerable<UserInfo> GetAllUsersInfo()
        {
            SIMS.MM.DAL.parnamidb1Entities db = new SIMS.MM.DAL.parnamidb1Entities();

            var usrs = Membership.GetAllUsers();
            List<UserInfo> list = new List<UserInfo>();
            foreach (var v in usrs)
            {
                var usr = (MembershipUser)v;
                UserInfo info = new UserInfo()
                {
                    Username = usr.UserName,
                    IsApproved = usr.IsApproved,
                    IsOnline = usr.IsOnline,
                    Roles = Roles.GetRolesForUser(usr.UserName)
                };
                info.AllowedSiteIDs = db.GetAllowedSiteIDs(usr.UserName).ToList();
                foreach (var siteid in info.AllowedSiteIDs)
                {
                    info.AllowedSites += db.Buses.Single(x => x.ID == siteid).Name + ", ";
                }
                list.Add(info);
            }
            return list;
        }

        public bool ChangePasswordForced(string username, string newpassword)
        {
            throw new NotImplementedException();
        }

        public bool ChangePassword(string username, string oldPassword, string newpassword)
        {
            Membership.GetUser(username).ChangePassword(oldPassword, newpassword);
            return true;
        }

        public IEnumerable<UserInfo> AddRolesToUser(string username, string[] roles)
        {
            Roles.AddUserToRoles(username, roles);
            return this.GetAllUsersInfo();
        }

        public IEnumerable<UserInfo> RemoveRolesToUser(string username, string[] roles)
        {
            Roles.RemoveUserFromRoles(username, roles);
            return this.GetAllUsersInfo();
        }

        public IEnumerable<string> GetAllRoles()
        {
            return Roles.GetAllRoles();
        }

        public IEnumerable<UserInfo> BlockUser(string username)
        {
            var usr = Membership.GetUser(username);
            usr.IsApproved = false;
            Membership.UpdateUser(usr);
            return this.GetAllUsersInfo();
        }

        public IEnumerable<UserInfo> AllowUser(string username)
        {
            var usr = Membership.GetUser(username);
            usr.IsApproved = true;
            Membership.UpdateUser(usr);
            return this.GetAllUsersInfo();
        }

        public IEnumerable<UserInfo> AddSitesToUser(string username, int[] sites)
        {
            parnamidb1Entities db = new parnamidb1Entities();
            foreach (var site in sites)
            {
                if (db.AllowedBUs.Any(x => x.LoginID == username && x.BUID == site))
                    continue;
                db.AllowedBUs.AddObject(new AllowedBU()
                {
                    LoginID = username,
                    BUID = site
                });
            }
            db.SaveChanges();
            return this.GetAllUsersInfo();
        }

        public IEnumerable<UserInfo> RemoveSitesToUser(string username, int[] sites)
        {
            parnamidb1Entities db = new parnamidb1Entities();
            foreach (var site in sites)
            {
                if (!db.AllowedBUs.Any(x => x.LoginID == username && x.BUID == site))
                    continue;
                db.AllowedBUs.DeleteObject(db.AllowedBUs.Single(x => x.LoginID == username && x.BUID == site));
            }
            db.SaveChanges();
            return this.GetAllUsersInfo();
        }

        public void CreateNewUser(string username, string password, string email)
        {
            var status = new MembershipCreateStatus();
            if (username == null)
                status = MembershipCreateStatus.InvalidUserName;
            if (password == null)
                status = MembershipCreateStatus.InvalidPassword;
            if (email == null)
                status = MembershipCreateStatus.InvalidEmail;

            manager.CreateUser(username, password, email, "NhiPata", "NhiPata", true, out status);
            if (status != MembershipCreateStatus.Success)
                throw new FaultException(status.ToString());
        }
    }
}
