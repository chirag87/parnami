﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using SIMS.MM.MODEL;
using SIMS.MM.BLL;
using System.Collections.ObjectModel;
using SIMS.Core;

namespace Contruction.Web.Services
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "MOService" in code, svc and config file together.
    [ServiceBehavior(MaxItemsInObjectGraph = 2147483647)]
    public class MOService : IMOService
    {
        IMOManager pm = new MOManager();
        IMODataProvider dp = new MODataProvider();

        public PaginatedData<MMOHeader> GetAllMOs(string username, int? pageindex, int? pagesize, string key, int? transiteid, int? reqsiteid, string ConType, bool? IsClosed)
        {
            var showAll = LoginService.CanShowAllSites(username);
            var data = (PaginatedData<MMOHeader>)dp.GetMOHeadersNew(username, pageindex, pagesize, key, transiteid, reqsiteid, ConType, IsClosed, showAll);
            return data;
        }

        public PaginatedData<MMOReleaseHeader> GetMOReleaseHeaders(string username, int? pageindex, int? pagesize, string Key, int? transiteid, int? reqsiteid, string ConType)
        {
            var showAll = LoginService.CanShowAllSites(username);
            var data = (PaginatedData<MMOReleaseHeader>)dp.GetMOReleaseHeaders(username, pageindex, pagesize, Key, transiteid, reqsiteid, ConType, showAll);
            return data;
        }

        public MMO CreateNewMO(MMO newMO)
        {
            return pm.CreateNewMO(newMO);
        }

        public MMO GetMObyID(int MOID)
        {
            return dp.GetMObyId(MOID);
        }

        //public string ReleaseMO(MReceiving mReceiving)
        //{
        //    var data = pm.CreateMORelease(mReceiving);
        //    return data;
        //}

        //public int ReceiveMO(MReceiving mReceiving)
        //{
        //    return pm.CreateMOReceive(mReceiving);
        //}

        public MReceiving GetMReceivingforMORelease(int MOID)
        {
            var data = dp.GetMReceivingforMoRelease(MOID);
            return data;
        }

        public MReceiving GetMReceivingforMOReceive(int MOID)
        {
            var data = dp.GetMReceivingforMoReceiving(MOID);
            return data;
        }

        public void CloseMO(int MOID)
        {
            pm.CloseMO(MOID);
        }

        public MMO SplitToMO(MMO NewMO, int OldPRID, ObservableCollection<int> OldPRLines)
        {
            var data = pm.SplitToMO(NewMO, OldPRID, OldPRLines);
            return data;
        }

        public MMO SplitToMOwithPartialQty(MMO NewMO, int OldPRID, ObservableCollection<MMOSplittedLine> MOSplittedLines)
        {
            var data = pm.SplitToMOwithPartialQty(NewMO, OldPRID, MOSplittedLines);
            return data;
        }

        public MMO CombineMO(int moid)
        {
            var data = pm.CombineMO(moid);
            return data;
        }

        public bool IsSimilarMOsAvailable(int moid)
        {
            var data = dp.IsSimilarMOsAvailable(moid);
            return data;
        }

        /********************************************************************/
        /*NEW METHODS after Double Entry System*/
        /*By: MADHUR*/

        public MMOReleasing GetMOReleaseTransactionByMOID(int moid)
        {
            var data = dp.GetMOReleaseTransactionByMOID(moid);
            return data;
        }

        public MMOReceiving GetMOReceiveTransactionByMOID(int moid)
        {
            var data = dp.GetMOReceiveTransactionByMOID(moid);
            return data;
        }

        public MMOReceiving GetMOReceivingAgainstMORelease(int moid, int relid)
        {
            var data = dp.GetMOReceivingAgainstMORelease(moid, relid);
            return data;
        }

        public MMOReleasing GetUniqueMOReleasingByID(int moid, int relid)
        {
            var data = dp.GetUniqueMOReleasingByID(moid, relid);
            return data;
        }

        public string MORelease(MMOReleasing mrec, string username)
        {
            var data = pm.MORelease(mrec, username);
            return data;
        }

        public int MOReceive(MMOReceiving mrec, string username)
        {
            var data = pm.MOReceive(mrec, username);
            return data;
        }

        public IEnumerable<MMOReleasing> GetMOReleasingsAgainstMO(int moid)
        {
            var data = dp.GetMOReleasingsAgainstMO(moid);
            return data;
        }

        public IEnumerable<MMOReceiving> GetMOReceivingsAgainstMO(int moid)
        {
            var data = dp.GetMOReceivingsAgainstMO(moid);
            return data;
        }

        /********************************************************************/
    }
}
