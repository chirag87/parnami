﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using SIMS.Core;
using SIMS.MM.MODEL;

namespace Contruction.Web.Services
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IReturnToVendorService" in both code and config file together.
    [ServiceContract]
    public interface IReturnToVendorService
    {
        [OperationContract]
        PaginatedData<MReturnToVendor> GetAllReturnToVendors(string username, int? pageindex, int? pagesize, string key, int? vendorid, int? siteid);

        [OperationContract]
        MReturnToVendor GetRTVByID(int ID);

        [OperationContract]
        MReturnToVendor CreateRTV(MReturnToVendor newRTV, string username);

        [OperationContract]
        MReturnToVendor UpdateRTV(MReturnToVendor rtv);

        [OperationContract]
        MReturnToVendor UpdateRTVReceivingInfo(MReturnToVendor rtv);

        [OperationContract]
        void DeleteRTVByID(int ID);

        [OperationContract]
        void DeleteRTV(MReturnToVendor rtv);

        [OperationContract]
        MReturnToVendor ChangeConfirmationStatus(int RTVID, bool status);
    }
}
