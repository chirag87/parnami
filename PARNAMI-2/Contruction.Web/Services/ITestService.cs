﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using SIMS.MM.MODEL;

namespace Contruction.Web.Services
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "ITestService" in both code and config file together.
    [ServiceContract]
    public interface ITestService
    {
        [OperationContract]
        IEnumerable<MItem> GetAllMItems();

        [OperationContract]
        MItem CreateMItem(MItem _MItem);

        [OperationContract]
        MItem UpdateMItem(MItem _MItem);

        [OperationContract]
        void DeleteMItem(int id);

        [OperationContract]
        MItem GetMItemByKey(int id);
    }
}
