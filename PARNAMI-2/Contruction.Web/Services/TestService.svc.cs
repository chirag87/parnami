﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using SIMS.MM.BLL;

namespace Contruction.Web.Services
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "TestService" in code, svc and config file together.
    public class TestService : ITestService
    {
        IMasterDataProvider dp = new MasterDataProvier();

        public IEnumerable<SIMS.MM.MODEL.MItem> GetAllMItems()
        {
            return dp.GetItemHeaders();
        }

        public SIMS.MM.MODEL.MItem CreateMItem(SIMS.MM.MODEL.MItem _MItem)
        {
            throw new NotImplementedException();
        }

        public SIMS.MM.MODEL.MItem UpdateMItem(SIMS.MM.MODEL.MItem _MItem)
        {
            throw new NotImplementedException();
        }

        public void DeleteMItem(int id)
        {
            throw new NotImplementedException();
        }

        public SIMS.MM.MODEL.MItem GetMItemByKey(int id)
        {
            throw new NotImplementedException();
        }
    }
}
