﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using SIMS.MM.MODEL;
using System.ServiceModel.Activation;
using System.IO;
using System.Web;
using System.Configuration;

namespace Contruction.Web.Services
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "FilesService" in code, svc and config file together.
    //[AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Required)]
    public class FilesService : IFilesService
    {
        public bool UploadPhoto(MItem picture)
        {
            FileStream fileStream = null;
            BinaryWriter writer = null;
            string filePath;
            try
            {
                filePath = HttpContext.Current.Server.MapPath(".") +
                ConfigurationManager.AppSettings["PictureUploadDirectory"] +
                picture.PictureName;

                if (picture.Photo != string.Empty)
                {
                    fileStream = File.Open(filePath, FileMode.Create);
                    writer = new BinaryWriter(fileStream);
                    writer.Write(picture.PictureStream);
                }
                return true;
            }

            catch (Exception)
            {
                return false;
            }

            finally
            {
                if (fileStream != null)
                    fileStream.Close();
                if (writer != null)
                    writer.Close();
            }
        }
    }
}
