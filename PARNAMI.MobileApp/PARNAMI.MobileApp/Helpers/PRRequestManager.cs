﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SIMS.MobileApprover.Helpers
{
    using Controllers;
    using SIMS.MM.MODEL;
    using Contruction.Web.Services;

    public class PRVerificationProvider : IApprovalRequestProvider
    {
        PurchasingService prService = new PurchasingService();

        public string Code
        {
            get { return "MI-Verify"; }
        }
        
        //public object GetSiteWiseStock(int _itemID,int? _size,int? _brand)        
        //{
        //    MM.DAL.MMReportsDBDataContext db = new MM.DAL.MMReportsDBDataContext(ConnectionProvider.ForLINQ2SQL());
        //    return db.GetDetailedStockReport(null,null,null,null,_itemID,_size,_brand);
        //}

        public List<ApprovalRequestModel> GetAll()
        {
            var data_mprs = prService.GetAllPRs("admin", 0, 20000, null, null, null, null);
            var data = data_mprs.Data
                .Where(x => x.IsVerified == false && x.IsReqApproved == false && x.IsApproved == false && x.PRState == "SubmittedForVerification")
                .Select(x =>
                new ApprovalRequestModel()
                {
                    ApprovalFor = "MI-Verify",
                    ApprovalForID = x.PRNo.ToString(),
                    ApprovalForRef = x.ReferenceNo,
                    DateTime = x.Date,
                    IsVerified = x.IsVerified,
                    IsReqApproved = x.IsReqApproved,
                    IsApproved = x.IsApproved,
                    IconPath = "~/Images/OrangeStar.png",
                    Title = "MI For: " + x.DisplaySite
                }).ToList();
            return data;
        }

        public object LocateObject(int id)
        {
            var pr = prService.GetPRbyID(id);
            foreach (var line in pr.PRLines)
            {
                var todispqty = GetTotalTODispacthQty(line.ID, line.LineID, pr.SiteID, line.ItemID, line.SizeID, line.BrandID, line.ConsumableType);
                line.TODispInfo = todispqty;
            }
            return pr;
        }

        private MTODispatchInfo GetTotalTODispacthQty(int? ID, int LineID, int SiteID, int ItemID, int? SizeID, int? BrandID, string ConType)
        {
            return prService.GetTotalTODispacthQty(ID, LineID, SiteID, ItemID, SizeID, BrandID, ConType);
        }

        public object Approve(int id, bool IsApproved, string By, string Comment = "No Comments")
        {
            //var pr = prService.GetPRbyID(id);
            //if(pr.IsVerified == true)
            //    return 
            if (IsApproved)
            {
                prService.VerifyPR(id, By, DateTime.UtcNow.AddHours(5.5), Comment);
                prService.ChangesStateofPR(id, "MIVerified");
                return prService.GetPRbyID(id);
            }
            else
            {
                //prService.ChangesStateofPR(id, "Draft");
                prService.RejectPR(id, Comment, "Draft");
                return prService.GetPRbyID(id);
            }
        }
    }

    public class PRReqApprovalProvider : IApprovalRequestProvider
    {
        PurchasingService prService = new PurchasingService();

        public string Code
        {
            get { return "MI-Approve"; }
        }

        public List<ApprovalRequestModel> GetAll()
        {
            var data_mprs = prService.GetAllPRs("admin", 0, 20000, null, null, null, null);
            var data = data_mprs.Data
                .Where(x => x.IsVerified == true && x.IsReqApproved == false && x.IsApproved == false && x.PRState == "SubmittedForMIApproval")
                .Select(x =>
                new ApprovalRequestModel()
                {
                    ApprovalFor = "MI-Approve",
                    ApprovalForID = x.PRNo.ToString(),
                    ApprovalForRef = x.ReferenceNo,
                    DateTime = x.Date,
                    IsVerified = x.IsVerified,
                    IsReqApproved = x.IsReqApproved,
                    IsApproved = x.IsApproved,
                    IconPath = "~/Images/PurpleStar.png",
                    Title = "MI For: " + x.DisplaySite
                }).ToList();
            return data;
        }

        public object LocateObject(int id)
        {
            var pr = prService.GetPRbyID(id);
            foreach (var line in pr.PRLines)
            {
                var todispqty = GetTotalTODispacthQty(line.ID, line.LineID, pr.SiteID, line.ItemID, line.SizeID, line.BrandID, line.ConsumableType);
                line.TODispInfo = todispqty;
            }
            return pr;
        }

        private MTODispatchInfo GetTotalTODispacthQty(int? ID, int LineID, int SiteID, int ItemID, int? SizeID, int? BrandID, string ConType)
        {
            return prService.GetTotalTODispacthQty(ID, LineID, SiteID, ItemID, SizeID, BrandID, ConType);
        }

        public object Approve(int id, bool IsApproved, string By, string Comment = "No Comments")
        {
            if (IsApproved)
            {
                prService.InitialApprovePR(id, By, DateTime.UtcNow.AddHours(5.5), Comment);
                prService.ChangesStateofPR(id, "MIApproved");
                return prService.GetPRbyID(id);
            }
            else
            {
                prService.RejectPR(id, Comment, "MIRejected");
                return prService.GetPRbyID(id);
            }
        }
    }

    public class PRFinalApprovalProvider : IApprovalRequestProvider
    {
        PurchasingService prService = new PurchasingService();
        
        public string Code
        {
            get { return "PO-Approve"; }
        }

        public List<ApprovalRequestModel> GetAll()
        {
            var data_mprs = prService.GetAllPRs("admin", 0, 20000, null, null, null, null);
            var data = data_mprs.Data
                .Where(x => x.IsVerified == true && x.IsReqApproved == true && x.IsApproved == false && x.PRState == "SubmittedForPOApproval")
                .Select(x =>
                new ApprovalRequestModel()
                {
                    ApprovalFor = "PO-Approve",
                    ApprovalForID = x.PRNo.ToString(),
                    ApprovalForRef = x.ReferenceNo,
                    DateTime = x.Date,
                    IsVerified = x.IsVerified,
                    IsReqApproved = x.IsReqApproved,
                    IsApproved = x.IsApproved,
                    IconPath = "~/Images/GreenStar.png",
                    Title = "MI For: " + x.DisplaySite
                }).ToList();
            return data;
        }

        public object LocateObject(int id)
        {
            var pr = prService.GetPRbyID(id);
            foreach (var line in pr.PRLines)
            {
                var todispqty = GetTotalTODispacthQty(line.ID, line.LineID, pr.SiteID, line.ItemID, line.SizeID, line.BrandID, line.ConsumableType);
                line.TODispInfo = todispqty;                 
            }
            return pr;
        }

        

        private MTODispatchInfo GetTotalTODispacthQty(int? ID, int LineID, int SiteID, int ItemID, int? SizeID, int? BrandID, string ConType)
        {
            
            return prService.GetTotalTODispacthQty(ID, LineID, SiteID, ItemID, SizeID, BrandID, ConType);
        }

        public object Approve(int id, bool IsApproved, string By, string Comment = "No Comments")
        {
            if (IsApproved)
            {
                prService.ApprovePR(id, By, DateTime.UtcNow.AddHours(5.5), Comment);
                prService.ChangesStateofPR(id, "POApproved");
                return prService.GetPRbyID(id);
            }
            else
            {
                //prService.ChangesStateofPR(id, "PORejected");
                prService.RejectPR(id, Comment, "PORejected");
                return prService.GetPRbyID(id);
            }
        }
    }
}