﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SIMS.MobileApprover.Controllers;

namespace SIMS.MobileApprover
{
    public interface IApprovalRequestProvider
    {
        string Code { get; }
        List<ApprovalRequestModel> GetAll();
        object LocateObject(int id);
        object Approve(int id, bool IsApproved, string By, string Comment = "No Comments");
    }
}