﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Linq;
using System.Web;
using System.Web.Security;
using System.Web.Mvc;
using SIMS.MM.DAL;

namespace SIMS.MobileApprover.Controllers
{
    public abstract class ApprovalControllerBase : Controller
    {
        public ApprovalControllerBase()
        {
            //RegisterAllProviders();
        }

        protected override void Initialize(System.Web.Routing.RequestContext requestContext)
        {
            base.Initialize(requestContext);
            RegisterAllProviders(null);
        }

        public virtual ActionResult Index()
        {
            return View("Index_MobileApprover", GetAll());
        }

        public abstract void RegisterAllProviders(string username);

        Dictionary<string, IApprovalRequestProvider> RequestProviders = new Dictionary<string, IApprovalRequestProvider>();
        Dictionary<string, string> RegisteredDetailedViews = new Dictionary<string, string>();

        public virtual void Register(string code, IApprovalRequestProvider provider, string DetailViewName = null)
        {
            RequestProviders.Add(code, provider);
            if (!String.IsNullOrWhiteSpace(DetailViewName))
                RegisteredDetailedViews.Add(code, DetailViewName);
        }

        /// <summary>
        /// Locate the Approval Request Providers
        /// </summary>
        /// <param name="code"></param>
        /// <returns></returns>
        public virtual IApprovalRequestProvider LocateProvider(string code)
        {
            return RequestProviders[code];
        }

        /// <summary>
        /// Locate The Object Which is Pending Approval
        /// </summary>
        /// <param name="code">Approval Code</param>
        /// <param name="id">Approval Item ID</param>
        /// <returns>Approving Object</returns>
        public virtual object LocateObject(string code, int id)
        {
            return LocateProvider(code).LocateObject(id);
        }

        public virtual string LocateDetailedParialViewName(string code)
        {
            if (RegisteredDetailedViews.ContainsKey(code))
                return RegisteredDetailedViews[code];
            return null;
        }

        public ActionResult GetSiteWiseStock(int id ,int siteid,int?size,int? brand )
        {            
            MMReportsDBDataContext db = new MMReportsDBDataContext(ConnectionProvider.ForLINQ2SQL());
            var data = db.GetDetailedStockReport(null, null, null, null, id, null, null);
            ViewBag.siteid = siteid;
            ViewBag.size = size;
            ViewBag.brand= brand;
            return View(data);
        }

        public virtual string GetDetailedViewName(string code)
        {
            ViewBag.UserName = Membership.GetUser().UserName;
            return "Details_MobileApprover";
        }

        public virtual string GetApprovalText(string code)
        { return "Approve"; }

        public virtual string GetRejectText(string code)
        { return "Reject"; }

        public virtual ActionResult eDetails(string For, int? ID, string key, string username)
        {
            if (string.IsNullOrWhiteSpace(key) || key != "927249E9-4489-49C6-B667-C76BBD6CEEFC")
                return Content("Unauthorized Access!");
            if (!Request.IsAuthenticated)
            {
                FormsAuthentication.SetAuthCookie(username, false);
                Response.Redirect(Request.RawUrl);
                return Content("redirecting ...");
            }
            var dataObject = LocateObject(For, ID.Value);
            ViewBag.PartialViewName = LocateDetailedParialViewName(For);
            ViewBag.approve = GetApprovalText(For);
            ViewBag.reject = GetRejectText(For);
            ViewBag.ApprovalForID = ID.Value;
            ViewBag.ApprovalFor = For;
            return View(GetDetailedViewName(For), dataObject);
        }

        public virtual ActionResult Details(string For, int ID)
        {
            var dataObject = LocateObject(For, ID);
            ViewBag.PartialViewName = LocateDetailedParialViewName(For);
            ViewBag.approve = GetApprovalText(For);
            ViewBag.reject = GetRejectText(For);
            ViewBag.ApprovalForID = ID;
            ViewBag.ApprovalFor = For;

            return View(GetDetailedViewName(For), dataObject);
        }

        public virtual ActionResult Approve(string For, int ID, string By, bool boolvalue, string Comment = "No Comments")
        {
            try
            {
                var InterfaceNameProvider = LocateProvider(For);
                InterfaceNameProvider.Approve(ID, boolvalue, By, Comment);
                var dataObject = LocateObject(For, ID);
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                var dataObject = LocateObject(For, ID);
                ViewBag.PartialViewName = LocateDetailedParialViewName(For);
                ViewBag.approve = GetApprovalText(For);
                ViewBag.reject = GetRejectText(For);
                ViewBag.ApprovalForID = ID;
                ViewBag.ApprovalFor = For;
                ViewBag.err = ex.Message;
                return View(GetDetailedViewName(For), dataObject);
            }
        }

        public virtual List<ApprovalRequestModel> GetAll()
        {
            List<ApprovalRequestModel> List = new List<ApprovalRequestModel>();
            foreach (var provider in RequestProviders.Values)
            {
                List.AddRange(provider.GetAll());
            }
            return List;
        }
    }
}