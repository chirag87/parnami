﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SIMS.MobileApprover
{
    public class ApprovalRequestModel
    {
        public string ApprovalFor { get; set; }
        public string ApprovalForID { get; set; }
        public string ApprovalForRef { get; set; }

        public bool IsVerified { get; set; }
        public bool IsReqApproved { get; set; }
        public bool IsApproved { get; set; }
        public string Status { get; set; }

        public string IconPath { get; set; }
        public DateTime DateTime { get; set; }
        public string Title { get; set; }
        public string ShortDetalils { get; set; }
        public string Details { get; set; }
        public string Comment { get; set; }
    }
}