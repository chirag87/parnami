﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.Mvc;
using SIMS.MM.MODEL;
using SIMS.MobileApprover.Helpers;

namespace SIMS.MobileApp.Controllers
{
    public class PRController : MobileApprover.Controllers.ApprovalControllerBase
    {

        public PRController() : base() { }

        public override void RegisterAllProviders(string username)
        {
            if (username == null)
            {
                if (Request == null) return;
                if (!Request.IsAuthenticated) return;
                var user = User.Identity.Name;
                if (user == null) return;
            }

                if (Roles.IsUserInRole("MI_CanVerify") || Roles.IsUserInRole("MI_AllRights")
                || Roles.IsUserInRole("admin") || Roles.IsUserInRole("dba"))
                this.Register("MI-Verify", new PRVerificationProvider());

            if (Roles.IsUserInRole("MI_CanApproveMI") || Roles.IsUserInRole("MI_AllRights")
                || Roles.IsUserInRole("admin") || Roles.IsUserInRole("dba"))
                this.Register("MI-Approve", new PRReqApprovalProvider());

            if (Roles.IsUserInRole("MI_CanApprovePO") || Roles.IsUserInRole("MI_AllRights")
                || Roles.IsUserInRole("admin") || Roles.IsUserInRole("dba"))
                this.Register("PO-Approve", new PRFinalApprovalProvider());
        }

        public override string GetApprovalText(string code)
        {
            if (code == "MI-Verify") return "Verify";
            if (code == "MI-Approve") return "Approve";
            if (code == "PO-Approve") return "Approve";
            return base.GetApprovalText(code);
        }
    }
}