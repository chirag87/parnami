﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;

namespace SIMS.MM.MODEL
{
    [DataContract]
    public class MPOReport : EntityModel
    {
        [DataMember]
        [Display(AutoGenerateField=false)]
        public int SNo { get; set; }

        [DataMember]
        public DateTime OrderDate { get; set; }

        [DataMember]
        [Display(Order = 1)]
        public int PONO { get; set; }

        [DataMember]
        [Display(Order = 2)]
        public int PRNO { get; set; }

        [DataMember]
        [Display(AutoGenerateField = false)]
        public string Vendor { get; set; }

        [DataMember]
        [Display(Name = "Vendor", Order=3)]
        public string DisplayVendor { get; set; }

        [DataMember]
        [Display(AutoGenerateField = false)]
        public string Site { get; set; }

        [DataMember]
        [Display(Name = "Site", Order = 4)]
        public string DisplaySite { get; set; }

        [DataMember]
        [Display(Order = 5)]
        public int CategoryID { get; set; }

        [DataMember]
        [Display(Order = 6)]
        public string MaterialCategory { get; set; }

        [DataMember]
        [Display(Order = 7)]
        public int MaterialID { get; set; }

        [DataMember]
        [Display(Order = 8)]
        public string Material { get; set; }

        [DataMember]
        [Display(Order = 9)]
        public string MU { get; set; }

        [DataMember]
        [Display(Order = 10)]
        public int SizeID { get; set; }

        [DataMember]
        [Display(Order = 11)]
        public string Size { get; set; }

        [DataMember]
        [Display(Order = 12)]
        public int BrandID { get; set; }

        [DataMember]
        [Display(Order = 13)]
        public string Brand { get; set; }
        
        [DataMember]
        [Display(Order = 14)]
        public double Qty { get; set; }

        [DataMember]
        [Display(Order = 15)]
        public double PerUnitPrice { get; set; }

        [DataMember]
        [Display(Order = 16)]
        public double TotalAmount { get; set; }
    }
}
