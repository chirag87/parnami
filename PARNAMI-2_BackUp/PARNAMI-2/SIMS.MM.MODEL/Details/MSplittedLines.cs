﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace SIMS.MM.MODEL
{
    [DataContract]
    public class MSplittedLines : EntityModel
    {
        int _LineID;
        [DataMember]
        public int LineID
        {
            get { return _LineID; }
            set
            {
                _LineID = value;
                Notify("LineID");
            }
        }

        double _OriginalQty;
        [DataMember]
        public double OriginalQty
        {
            get { return _OriginalQty; }
            set
            {
                _OriginalQty = value;
                Notify("OriginalQty");
            }
        }

        double _ConfirmedQty;
        [DataMember]
        public double ConfirmedQty
        {
            get { return _ConfirmedQty; }
            set
            {
                _ConfirmedQty = value;
                Notify("ConfirmedQty");
            }
        }

        [DataMember]
        public int ItemID { get; set; }

        [DataMember]
        public string Item { get; set; }

        [DataMember]
        public int? SizeID { get; set; }

        [DataMember]
        public string Size { get; set; }

        [DataMember]
        public int? BrandID { get; set; }

        [DataMember]
        public string Brand { get; set; }

        [DataMember]
        public string ConsumableType { get; set; }

        [DataMember]
        public bool IsPartial
        {
            get
            {
                if (OriginalQty - ConfirmedQty > 0)
                    return true;
                else
                    return false;
            }
            set { }
        }

        [DataMember]
        public double DiffQty
        {
            get { return OriginalQty - ConfirmedQty; }
            set { }
        }
    }
}
