﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace SIMS.MM.MODEL
{
    [DataContract]
    public class MMeasurementUnit
    {
        [DataMember]
        public string MU { get; set; }

        [DataMember]
        public string FormalName { get; set; }

        [DataMember]
        public string DataType { get; set; }

        [DataMember]
        public string Type { get; set; }

        [DataMember]
        public int? Variable { get; set; }
    }
}
