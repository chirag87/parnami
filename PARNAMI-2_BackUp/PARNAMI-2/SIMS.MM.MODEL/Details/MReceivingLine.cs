﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.ComponentModel;

namespace SIMS.MM.MODEL
{
    [DataContract]
    public class MReceivingLine : EntityModel
    {
        public MReceivingLine()
        {
            NQuantity = OrderedQty;
        }

        #region Core Properties

        [DataMember]
        public int TransactionID { get; set; }

        string _TransactionTypeID;
        [DataMember]
        public string TransactionTypeID
        {
            get { return _TransactionTypeID; }
            set
            {
                _TransactionTypeID = value;
                Notify("TransactionTypeID");
            }
        }

        int? _POID;
        [DataMember]
        public int? POID
        {
            get { return _POID; }
            set
            {
                _POID = value;
                Notify("POID");
            }
        }

        [DataMember]
        public int? PRID { get; set; }

        [DataMember]
        public int? PRLineID { get; set; }

        [DataMember]
        public int? MTRID { get; set; }

        [DataMember]
        public int? MTRLineID { get; set; }

        [DataMember]
        public int? MRID { get; set; }

        [DataMember]
        public int? MRLineID { get; set; }

        int _LineID;
        [DataMember]
        public int LineID
        {
            get { return _LineID; }
            set
            {
                _LineID = value;
                Notify("LineID");
            }
        }

        [DataMember]
        public MTruckParameter TruckParameters { get; set; }

        int _itemID { get; set; }
        [DataMember]
        public int ItemID
        {
            get { return _itemID; }
            set
            {
                _itemID = value;
                Notify("ItemID");
                RaiseItemIDChanged();
            }
        }

        [DataMember]
        public string Item { get; set; }

        int? _SizeID;
        [DataMember]
        public int? SizeID
        {
            get { return _SizeID; }
            set
            {
                _SizeID = value;
                Notify("SizeID");
                RaiseSizeIDChanged();
            }
        }
        
        [DataMember]
        public string Size { get; set; }

        int? _BrandID;
        [DataMember]
        public int? BrandID
        {
            get { return _BrandID; }
            set
            {
                _BrandID = value;
                Notify("BrandID");
                RaiseBrandIDChanged();
            }
        }

        [DataMember]
        public string Brand { get; set; }

        [DataMember]
        public string LineRemarks { get; set; }

        [DataMember]
        public string LineChallanNo { get; set; }

        double _Quantity;
        [DataMember]
        public double Quantity
        {
            get { return _Quantity; }
            set
            {
                if ((TransactionTypeID == "MOReceive" && value > (ReleasedQty - EarlierReceivedQty)) || (TransactionTypeID == "MOReceive" && value < 0))
                {
                    _Quantity = 0;
                }
                if ((TransactionTypeID == "Return" && value > (IssuedQty - EarlierReturnedQty)) || (TransactionTypeID == "Return" && value < 0))
                {
                    _Quantity = 0;
                }
                //if (TransactionTypeID == "GRN" && POID != null && value > (OrderedQty - EarlierReceivedQty))
                //{
                //    _Quantity = 0;
                //}
                else
                    _Quantity = value;

                Notify("Quantity");
                UpdateLinePrice();
            }
        }

        double _UnitPrice;
        [DataMember]
        public double UnitPrice
        {
            get { return _UnitPrice; }
            set
            {
                _UnitPrice = value;
                Notify("UnitPrice");
                UpdateLinePrice();
            }
        }

        [DataMember]
        public double LinePrice { get { return Quantity * UnitPrice; } set { } }

        [DataMember]
        public double NQuantity
        {
            get { return -1 * Quantity; }
            set
            {
                if ((TransactionTypeID == "MORelease" && value > (OrderedQty - EarlierReleasedQty)) || (TransactionTypeID == "MORelease" && value < 0))
                {
                    Quantity = 0;
                }
                if ((TransactionTypeID == "Issue" && value > (OrderedQty - EarlierIssuedQty)) || (TransactionTypeID == "Issue" && value < 0))
                {
                    _Quantity = 0;
                }
                else
                    Quantity = -1 * value;

                Notify("NQuantity");
                UpdateLinePrice();
            }
        }

        [DataMember]
        public double EQuantity
        {
            get
            {
                if (TransactionTypeID == "Consumption")
                    return NQuantity;
                return Quantity;
            }
            set { }
        }

        [DataMember]
        public double OrderedQty { get; set; }

        [DataMember]
        public double EarlierQty { get; set; }

        [DataMember]
        public double EarlierReleasedQty { get; set; }

        [DataMember]
        public double EarlierReceivedQty { get; set; }

        [DataMember]
        public double ReleasedQty { get; set; }

        [DataMember]
        public double NewPendingQty { get; set; }

        [DataMember]
        public double EarlierIssuedQty { get; set; }

        [DataMember]
        public double EarlierReturnedQty { get; set; }

        [DataMember]
        public double IssuedQty { get; set; }

        [DataMember]
        public string TruckNo { get; set; }

        [DataMember]
        public string TypeID { get; set; }

        #region For BILLING AND RECEIVING STATUS...

        [DataMember]
        public int? BillingID { get; set; }

        [DataMember]
        public int? BillingLineID { get; set; }

        [DataMember]
        public string BillNo { get; set; }

        [DataMember]
        public DateTime? BillDate { get; set; }

        [DataMember]
        public double? TotalbilingAmnt { get; set; }

        [DataMember]
        public double? TotalPaidAmnt { get; set; }

        [DataMember]
        public bool? IsBillClosed { get; set; }

        [DataMember]
        public string BillingStatus
        {
            get
            {
                if (IsBillClosed == true)
                    return "Closed";
                else if (HasBill == false && IsBillClosed == false)
                    return "UnBilled";
                else
                    return "Open";
            }
            set
            {
                Notify("IsBillClosed");
                Notify("BillingStatus");
            }
        }

        bool _HasBill;
        [DataMember]
        public bool HasBill
        {
            get { return _HasBill; }
            set
            {
                _HasBill = value;
                Notify("HasBill");
            }
        }

        #endregion

        #endregion

        #region Methods...

        public void UpdateLinePrice()
        {
            LinePrice = Quantity * UnitPrice;
            Notify("LinePrice");
        }

        #endregion

        #region Events...

        public event EventHandler ItemIDChanged;
        public void RaiseItemIDChanged()
        {
            if (ItemIDChanged != null)
                ItemIDChanged(this, new EventArgs());
        }

        public event EventHandler SizeIDChanged;
        public void RaiseSizeIDChanged()
        {
            if (SizeIDChanged != null)
                SizeIDChanged(this, new EventArgs());
        }

        public event EventHandler BrandIDChanged;
        public void RaiseBrandIDChanged()
        {
            if (BrandIDChanged != null)
                BrandIDChanged(this, new EventArgs());
        }

        #endregion

        #region For Billing
        /*For Billing*/


        [DataMember]
        public string ReferenceNo { get; set; }

        [DataMember]
        public int? VendorID { get; set; }

        [DataMember]
        public string Vendor { get; set; }

        [DataMember]
        public string ReceivingChallanNo { get; set; }

        [DataMember]
        public DateTime? CreationDate { get; set; }

        [DataMember]
        public int? SiteID { get; set; }

        [DataMember]
        public string Site { get; set; }

        #endregion

        #region For Adjustment

        //For Adjustment

        public double _CurrentStock = new double();
        [DataMember]
        public double CurrentStock
        {
            get { return _CurrentStock; }
            set
            {
                _CurrentStock = value;
                Notify("CurrentStock");
            }
        }

        public double _AdjustedStock = new double();
        [DataMember]
        public double AdjustedStock
        {
            get { return _AdjustedStock; }
            set
            {
                _AdjustedStock = value;
                Notify("AdjustedStock");
                UpdateQuantity();
            }
        }

        void UpdateQuantity()
        {
            Quantity = AdjustedStock - CurrentStock;
            Notify("Quantity");
        }

        #endregion
    }
}
