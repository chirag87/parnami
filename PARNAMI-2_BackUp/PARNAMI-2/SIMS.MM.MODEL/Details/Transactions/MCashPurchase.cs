﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.Collections.ObjectModel;

namespace SIMS.MM.MODEL
{
    [DataContract]
    public class MCashPurchase : EntityModel
    {
        public MCashPurchase()
        {
            ForDate = DateTime.UtcNow.AddHours(5.5);
            LastUpdatedOn = DateTime.UtcNow.AddHours(5.5);
            CreatedOn = DateTime.UtcNow.AddHours(5.5);
        }

        #region Events

        public event EventHandler SiteIDChanged;
        public void RaiseSiteIDChanged()
        {
            if (SiteIDChanged != null)
                SiteIDChanged(this, new EventArgs());
        }

        #endregion

        #region Model

        [DataMember]
        public int ID { get; set; }

        [DataMember]
        public string ReferenceNo { get; set; }

        [DataMember]
        public int? VendorID { get; set; }

        [DataMember]
        public string Vendor { get; set; }

        public int _SiteID = new int();
        [DataMember]
        public int SiteID
        {
            get { return _SiteID; }
            set
            {
                _SiteID = value;
                Notify("SiteID");
                RaiseSiteIDChanged();
            }
        }

        [DataMember]
        public string Site { get; set; }

        [DataMember]
        public string Remarks { get; set; }

        [DataMember]
        public string Requester { get; set; }

        DateTime _ForDate;
        [DataMember]
        public DateTime ForDate
        {
            get
            {
                if (_ForDate < DateTime.UtcNow.AddHours(5.5))
                    return _ForDate;
                else
                    return DateTime.UtcNow.AddHours(5.5);
            }
            set
            {
                _ForDate = value;
                Notify("ForDate");
            }
        }

        DateTime _CreatedOn;
        [DataMember]
        public DateTime CreatedOn
        {
            get
            {
                if (_CreatedOn < DateTime.UtcNow.AddHours(5.5))
                    return _CreatedOn;
                else
                    return DateTime.UtcNow.AddHours(5.5);
            }
            set
            {
                _CreatedOn = value;
                Notify("CreatedOn");
            }
        }

        [DataMember]
        public string CreatedBy { get; set; }

        DateTime _LastUpdatedOn;
        [DataMember]
        public DateTime LastUpdatedOn
        {
            get
            {
                if (_LastUpdatedOn < DateTime.UtcNow.AddHours(5.5))
                    return _LastUpdatedOn;
                else
                    return DateTime.UtcNow.AddHours(5.5);
            }
            set
            {
                _LastUpdatedOn = value;
                Notify("LastUpdatedOn");
            }
        }

        [DataMember]
        public string LastUpdatedBy { get; set; }

        [DataMember]
        public string InvoiceNo { get; set; }

        [DataMember]
        public string ChallanNo { get; set; }

        [DataMember]
        public double? GrossTotalofLines { get; set; }

        double? _TAX = 0;
        [DataMember]
        public double? TAX
        {
            get { return _TAX; }
            set
            {
                _TAX = value;
                Notify("TAX");
                UpdateTotalLinePrice();
            }
        }

        [DataMember]
        public double? TaxAmount
        {
            get { return (GrossTotalofLines * TAX) / 100; }
            set { }
        }

        double? _Freight = 0;
        [DataMember]
        public double? Freight
        {
            get { return _Freight; }
            set
            {
                _Freight = value;
                Notify("Freight");
                UpdateTotalLinePrice();
            }
        }

        [DataMember]
        public double? TotalLinePrice
        {
            get { return GrossTotalofLines + TaxAmount + Freight; }
            set { }
        }

        ObservableCollection<MCashPurchaseLine> _CashPurchaseLines = new ObservableCollection<MCashPurchaseLine>();
        [DataMember]
        public ObservableCollection<MCashPurchaseLine> CashPurchaseLines
        {
            get { return _CashPurchaseLines; }
            set
            {
                _CashPurchaseLines = value;
                Notify("CashPurchaseLines");
            }
        }

        #endregion

        #region Methods

        public MCashPurchaseLine AddNewLine()
        {
            if (this.CashPurchaseLines == null)
                this.CashPurchaseLines = new ObservableCollection<MCashPurchaseLine>();
            var line = new MCashPurchaseLine();
            this.CashPurchaseLines.Add(line);
            this.UpdateLineNumbers();
            return line;
        }

        public int UpdateLineNumbers()
        {
            if (this.CashPurchaseLines == null) return 0;
            int i = 1;
            foreach (var item in this.CashPurchaseLines)
            {
                item.LineID = i;
                i++;
            }
            return i - 1;
        }

        public void UpdateGrossLinePrice()
        {
            GrossTotalofLines = CashPurchaseLines.Sum(x => x.LinePrice);
        }

        public void UpdateTaxAmount()
        {
            TaxAmount = GrossTotalofLines * TAX / 100;
        }

        public void UpdateTotalLinePrice()
        {
            TotalLinePrice = GrossTotalofLines + TaxAmount + Freight;
        }

        #endregion
    }

    [DataContract]
    public class MCashPurchaseLine : EntityModel
    {
        #region Events...

        public event EventHandler ItemIDChanged;
        public void RaiseItemIDChanged()
        {
            if (ItemIDChanged != null)
                ItemIDChanged(this, new EventArgs());
        }

        #endregion

        #region Model

        [DataMember]
        public int HeaderID { get; set; }

        int _LineID;
        [DataMember]
        public int LineID
        {
            get { return _LineID; }
            set
            {
                _LineID = value;
                Notify("LineID");
            }
        }

        [DataMember]
        public MTruckParameter TruckParameters { get; set; }

        int? _itemID { get; set; }
        [DataMember]
        public int? ItemID
        {
            get { return _itemID; }
            set
            {
                _itemID = value;
                Notify("ItemID");
                RaiseItemIDChanged();
            }
        }

        [DataMember]
        public string Item { get; set; }

        int _SizeID;
        [DataMember]
        public int SizeID
        {
            get { return _SizeID; }
            set
            {
                _SizeID = value;
                Notify("SizeID");
            }
        }

        [DataMember]
        public string Size { get; set; }

        int _BrandID;
        [DataMember]
        public int BrandID
        {
            get { return _BrandID; }
            set
            {
                _BrandID = value;
                Notify("BrandID");
            }
        }

        [DataMember]
        public string Brand { get; set; }

        [DataMember]
        public string LineRemarks { get; set; }

        [DataMember]
        public string LineChallanNo { get; set; }

        double _Quantity;
        [DataMember]
        public double Quantity
        {
            get { return _Quantity; }
            set
            {
                _Quantity = value;
                Notify("Quantity");
                UpdateLinePrice();
            }
        }

        double _UnitPrice;
        [DataMember]
        public double UnitPrice
        {
            get { return _UnitPrice; }
            set
            {
                _UnitPrice = value;
                Notify("UnitPrice");
                UpdateLinePrice();
            }
        }

        [DataMember]
        public double LinePrice { get { return Quantity * UnitPrice; } set { } }

        [DataMember]
        public double ReimburseAmount { get; set; }

        [DataMember]
        public bool IsPaid { get; set; }

        [DataMember]
        public string TruckNo { get; set; }

        #endregion

        #region Methods...

        public void UpdateLinePrice()
        {
            LinePrice = Quantity * UnitPrice;
            Notify("LinePrice");
        }

        #endregion
    }
}