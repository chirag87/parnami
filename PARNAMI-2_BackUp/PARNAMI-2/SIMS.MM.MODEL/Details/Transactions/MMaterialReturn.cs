﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.Collections.ObjectModel;

namespace SIMS.MM.MODEL
{
    [DataContract]
    public class MMaterialReturn : EntityModel
    {
        public MMaterialReturn()
        {
            LastUpdatedOn = DateTime.UtcNow.AddHours(5.5);
            ReturnedOn = DateTime.UtcNow.AddHours(5.5);
        }

        #region Events

        public event EventHandler SiteIDChanged;
        public void RaiseSiteIDChanged()
        {
            if (SiteIDChanged != null)
                SiteIDChanged(this, new EventArgs());
        }

        #endregion

        #region Model

        [DataMember]
        public int ID { get; set; }

        [DataMember]
        public string ReferenceNo { get; set; }

        public int _SiteID = new int();
        [DataMember]
        public int SiteID
        {
            get { return _SiteID; }
            set
            {
                _SiteID = value;
                Notify("SiteID");
                RaiseSiteIDChanged();
            }
        }

        [DataMember]
        public string Site { get; set; }

        [DataMember]
        public string Requester { get; set; }

        [DataMember]
        public string Remarks { get; set; }

        [DataMember]
        public int? TranporterID { get; set; }

        [DataMember]
        public string Transporter { get; set; }

        [DataMember]
        public string ReturnedBy { get; set; }

        DateTime _ReturnedOn;
        [DataMember]
        public DateTime ReturnedOn
        {
            get
            {
                if (_ReturnedOn < DateTime.UtcNow.AddHours(5.5))
                    return _ReturnedOn;
                else
                    return DateTime.UtcNow.AddHours(5.5);
            }
            set
            {
                _ReturnedOn = value;
                Notify("ReturnedOn");
            }
        }

        DateTime _LastUpdatedOn;
        [DataMember]
        public DateTime LastUpdatedOn
        {
            get
            {
                if (_LastUpdatedOn < DateTime.UtcNow.AddHours(5.5))
                    return _LastUpdatedOn;
                else
                    return DateTime.UtcNow.AddHours(5.5);
            }
            set
            {
                _LastUpdatedOn = value;
                Notify("LastUpdatedOn");
            }
        }

        [DataMember]
        public string LastUpdatedBy { get; set; }

        ObservableCollection<MMaterialReturnLine> _MaterialReturnLines = new ObservableCollection<MMaterialReturnLine>();
        [DataMember]
        public ObservableCollection<MMaterialReturnLine> MaterialReturnLines
        {
            get { return _MaterialReturnLines; }
            set
            {
                _MaterialReturnLines = value;
                Notify("MaterialReturnLines");
            }
        }

        #endregion

        #region Methods

        public MMaterialReturnLine AddNewLine()
        {
            if (this.MaterialReturnLines == null)
                this.MaterialReturnLines = new ObservableCollection<MMaterialReturnLine>();
            var line = new MMaterialReturnLine();
            this.MaterialReturnLines.Add(line);
            this.UpdateLineNumbers();
            return line;
        }

        public int UpdateLineNumbers()
        {
            if (this.MaterialReturnLines == null) return 0;
            int i = 1;
            foreach (var item in this.MaterialReturnLines)
            {
                item.LineID = i;
                i++;
            }
            return i - 1;
        }

        #endregion
    }

    [DataContract]
    public class MMaterialReturnLine : EntityModel
    {
        #region Events...

        public event EventHandler ItemIDChanged;
        public void RaiseItemIDChanged()
        {
            if (ItemIDChanged != null)
                ItemIDChanged(this, new EventArgs());
        }

        #endregion

        #region Model

        [DataMember]
        public int HeaderID { get; set; }

        int _LineID;
        [DataMember]
        public int LineID
        {
            get { return _LineID; }
            set
            {
                _LineID = value;
                Notify("LineID");
            }
        }

        int _itemID { get; set; }
        [DataMember]
        public int ItemID
        {
            get { return _itemID; }
            set
            {
                _itemID = value;
                Notify("ItemID");
                RaiseItemIDChanged();
            }
        }

        [DataMember]
        public string Item { get; set; }

        int _SizeID;
        [DataMember]
        public int SizeID
        {
            get { return _SizeID; }
            set
            {
                _SizeID = value;
                Notify("SizeID");
            }
        }

        [DataMember]
        public string Size { get; set; }

        int _BrandID;
        [DataMember]
        public int BrandID
        {
            get { return _BrandID; }
            set
            {
                _BrandID = value;
                Notify("BrandID");
            }
        }

        [DataMember]
        public string Brand { get; set; }

        [DataMember]
        public string LineRemarks { get; set; }

        [DataMember]
        public string LineChallanNo { get; set; }

        double _Quantity;
        [DataMember]
        public double Quantity
        {
            get { return _Quantity; }
            set
            {
                if (value > (AleadyIssuedQty - EarlierReturnedQty))
                {
                    _Quantity = 0;
                }
                else
                {
                    _Quantity = value;
                }
                Notify("Quantity");
                UpdateLinePrice();
            }
        }

        [DataMember]
        public double OrderedQty { get; set; }

        [DataMember]
        public double EarlierReturnedQty { get; set; }

        [DataMember]
        public double AleadyIssuedQty { get; set; }

        [DataMember]
        public double PendingQty
        {
            get { return EarlierReturnedQty - Quantity; }
            set { }
        }

        double _UnitPrice;
        [DataMember]
        public double UnitPrice
        {
            get { return _UnitPrice; }
            set
            {
                _UnitPrice = value;
                Notify("UnitPrice");
                UpdateLinePrice();
            }
        }

        [DataMember]
        public double LinePrice { get { return Quantity * UnitPrice; } set { } }

        [DataMember]
        public string TruckNo { get; set; }

        [DataMember]
        public int MRID { get; set; }

        [DataMember]
        public int MRLineID { get; set; }

        #endregion

        #region Methods...

        public void UpdateLinePrice()
        {
            LinePrice = Quantity * UnitPrice;
            Notify("LinePrice");
        }

        #endregion
    }
}
