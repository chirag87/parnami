﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.Collections.ObjectModel;

namespace SIMS.MM.MODEL
{
    [DataContract]
    public class MMOReleasing : EntityModel
    {
        public MMOReleasing()
        {
            LastUpdatedOn = DateTime.UtcNow.AddHours(5.5);
            ReleasedOn = DateTime.UtcNow.AddHours(5.5);
        }

        #region Events

        public event EventHandler SiteIDChanged;
        public void RaiseSiteIDChanged()
        {
            if (SiteIDChanged != null)
                SiteIDChanged(this, new EventArgs());
        }

        #endregion

        #region Model

        [DataMember]
        public int ID { get; set; }

        [DataMember]
        public string ReferenceNo { get; set; }

        [DataMember]
        public string MOReferenceNo { get; set; }

        int _TransferringSiteID;
        [DataMember]
        public int TransferringSiteID
        {
            get { return _TransferringSiteID; }
            set
            {
                _TransferringSiteID = value;
                Notify("TransferringSiteID");
                RaiseSiteIDChanged();
            }
        }

        int _RequestingSiteID;
        [DataMember]
        public int RequestingSiteID
        {
            get { return _RequestingSiteID; }
            set
            {
                _RequestingSiteID = value;
                Notify("RequestingSiteID");
                RaiseSiteIDChanged();
            }
        }

        [DataMember]
        public string RequestingSite { get; set; }

        [DataMember]
        public string TransferringSite { get; set; }

        [DataMember]
        public string Remarks { get; set; }

        [DataMember]
        public int? TransporterID { get; set; }

        [DataMember]
        public string Transporter { get; set; }

        DateTime _ReleasedOn;
        [DataMember]
        public DateTime ReleasedOn
        {
            get
            {
                if (_ReleasedOn < DateTime.UtcNow.AddHours(5.5))
                    return _ReleasedOn;
                else
                    return DateTime.UtcNow.AddHours(5.5);
            }
            set
            {
                _ReleasedOn = value;
                Notify("ReleasedOn");
            }
        }

        [DataMember]
        public string ReleasedBy { get; set; }

        DateTime? _LastUpdatedOn;
        [DataMember]
        public DateTime? LastUpdatedOn
        {
            get
            {
                if (_LastUpdatedOn < DateTime.UtcNow.AddHours(5.5))
                    return _LastUpdatedOn;
                else
                    return DateTime.UtcNow.AddHours(5.5);
            }
            set
            {
                _LastUpdatedOn = value;
                Notify("LastUpdatedOn");
            }
        }

        [DataMember]
        public string LastUpdatedBy { get; set; }

        [DataMember]
        public string ChallanNo { get; set; }

        ObservableCollection<MMOReleasingLine> _MOReleasingLines = new ObservableCollection<MMOReleasingLine>();
        [DataMember]
        public ObservableCollection<MMOReleasingLine> MOReleasingLines
        {
            get { return _MOReleasingLines; }
            set
            {
                _MOReleasingLines = value;
                Notify("MOReleasingLines");
            }
        }

        [DataMember]
        public int? LinesCount { get; set; }

        [DataMember]
        public int MTRID { get; set; }

        [DataMember]
        public string ConsumableType { get; set; }

        #endregion

        #region Methods

        public MMOReleasingLine AddNewLine()
        {
            if (this.MOReleasingLines == null)
                this.MOReleasingLines = new ObservableCollection<MMOReleasingLine>();
            var line = new MMOReleasingLine();
            this.MOReleasingLines.Add(line);
            this.UpdateLineNumbers();
            return line;
        }

        public int UpdateLineNumbers()
        {
            if (this.MOReleasingLines == null) return 0;
            int i = 1;
            foreach (var item in this.MOReleasingLines)
            {
                item.LineID = i;
                i++;
            }
            return i - 1;
        }

        #endregion
    }

    [DataContract]
    public class MMOReleasingLine : EntityModel
    {
        #region Events...

        public event EventHandler ItemIDChanged;
        public void RaiseItemIDChanged()
        {
            if (ItemIDChanged != null)
                ItemIDChanged(this, new EventArgs());
        }

        #endregion

        #region Model

        [DataMember]
        public int HeaderID { get; set; }

        int _LineID;
        [DataMember]
        public int LineID
        {
            get { return _LineID; }
            set
            {
                _LineID = value;
                Notify("LineID");
            }
        }

        int _itemID { get; set; }
        [DataMember]
        public int ItemID
        {
            get { return _itemID; }
            set
            {
                _itemID = value;
                Notify("ItemID");
                RaiseItemIDChanged();
            }
        }

        [DataMember]
        public string Item { get; set; }

        int? _SizeID;
        [DataMember]
        public int? SizeID
        {
            get { return _SizeID; }
            set
            {
                _SizeID = value;
                Notify("SizeID");
            }
        }

        [DataMember]
        public string Size { get; set; }

        int? _BrandID;
        [DataMember]
        public int? BrandID
        {
            get { return _BrandID; }
            set
            {
                _BrandID = value;
                Notify("BrandID");
            }
        }

        [DataMember]
        public string Brand { get; set; }

        [DataMember]
        public string LineRemarks { get; set; }

        [DataMember]
        public string LineChallanNo { get; set; }

        double _Quantity;
        [DataMember]
        public double Quantity
        {
            get { return _Quantity; }
            set
            {
                if (value > (OrderedQty - EarlierReleasedQty))
                {
                    _Quantity = 0;
                }
                else
                {
                    _Quantity = value;
                }
                Notify("Quantity");
                UpdateLinePrice();
            }
        }

        [DataMember]
        public double OrderedQty { get; set; }

        [DataMember]
        public double EarlierReleasedQty { get; set; }

        [DataMember]
        public double PendingQty
        {
            get { return EarlierReleasedQty - Quantity; }
            set { }
        }

        double _UnitPrice;
        [DataMember]
        public double UnitPrice
        {
            get { return _UnitPrice; }
            set
            {
                _UnitPrice = value;
                Notify("UnitPrice");
                UpdateLinePrice();
            }
        }

        [DataMember]
        public double LinePrice { get { return Quantity * UnitPrice; } set { } }

        [DataMember]
        public string TruckNo { get; set; }

        [DataMember]
        public int MOID { get; set; }

        [DataMember]
        public int MOLineID { get; set; }

        #endregion

        #region Methods...

        public void UpdateLinePrice()
        {
            LinePrice = Quantity * UnitPrice;
            Notify("LinePrice");
        }

        #endregion
    }
}