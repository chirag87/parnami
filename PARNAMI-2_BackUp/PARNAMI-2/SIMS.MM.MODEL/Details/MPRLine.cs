﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.ComponentModel;

namespace SIMS.MM.MODEL
{
    [DataContract]
    public partial class MPRLine : EntityModel
    {
        public MPRLine()
        {
            DeliveryDate = DateTime.UtcNow.AddHours(5.5);
            ConsumableType = "Consumable";
        }

        [DataMember]
        public int PRID { get; set; }

        [DataMember]
        public int? POID { get; set; }

        int _LineID;
        [DataMember]
        public int LineID
        {
            get { return _LineID; }
            set
            {
                _LineID = value;
                Notify("LineID");
            }
        }

        bool _IsSelected = false;
        [DataMember]
        public bool IsSelected
        {
            get { return _IsSelected; }
            set
            {
                _IsSelected = value;
                Notify("IsSelected");
            }
        }

        double _Quantity;
        [DataMember]
        public double Quantity
        {
            get { return _Quantity; }
            set
            {
                _Quantity = value;
                UpdateLinePrice();
            }
        }

        double _SplittedQuantity;
        [DataMember]
        public double SplittedQuantity
        {
            get { return _SplittedQuantity; }
            set
            {
                _SplittedQuantity = value;
                Notify("SplittedQuantity");
            }
        }

        double _UnitPrice = 0;
        [DataMember]
        public double UnitPrice
        {
            get { return _UnitPrice; }
            set
            {
                _UnitPrice = value;
                UpdateLinePrice();
            }
        }

        [DataMember]
        public double LastPrice { get; set; }

        [DataMember]
        public int ItemID { get; set; }

        [DataMember]
        public string Item { get; set; }

        [DataMember]
        public int? SizeID { get; set; }

        [DataMember]
        public string Size { get; set; }

        [DataMember]
        public int? BrandID { get; set; }

        [DataMember]
        public string Brand { get; set; }

        [DataMember]
        public string ConsumableType { get; set; }

        DateTime? _DeliveryDate;
        [DataMember]
        public DateTime? DeliveryDate
        {
            get
            {
                //if (_DeliveryDate < DateTime.UtcNow.AddHours(5.5))
                //    return _DeliveryDate;
                //else
                //    return DateTime.UtcNow.AddHours(5.5);
                return _DeliveryDate;
            }
            set
            {
                _DeliveryDate = value;
                Notify("DeliveryDate");
            }
        }


        double _Discount = 0;
        [DataMember]
        public double Discount
        {
            get { return _Discount; }
            set
            {
                if (DiscountType == "%")
                {
                    _Discount = value;
                    DiscountAmount = (LinePrice * value) / 100;
                    UpdateTotalLinePriceOnPercent();
                    Notify("Discount");
                }
                else
                {
                    _Discount = value;
                    DiscountAmount = value;
                    UpdateTotalLinePrice();
                    Notify("Discount");
                }
            }
        }

        double _DiscountAmount = 0;
        public double DiscountAmount
        {
            get { return _DiscountAmount; }
            set
            {
                _DiscountAmount = value;
                Notify("DiscountAmount");
            }
        }

        string _DiscountType;
        [DataMember]
        public string DiscountType
        {
            get { return _DiscountType; }
            set
            {

                _DiscountType = value;
                Notify("DiscountType");
            }
        }

        [DataMember]
        public string LineRemarks { get; set; }

        [DataMember]
        public double LinePrice { get { return Quantity * UnitPrice; } set { } }

        [DataMember]
        public double TotalLinePrice { get { return LinePrice - DiscountAmount; } set { } }

        [DataMember]
        public string TruckNo { get; set; }

        [DataMember]
        public string MeasurementUnit { get; set; }

        [DataMember]
        public double? TotalReceivedQty { get; set; }

        public void UpdateLinePrice()
        {
            LinePrice = Quantity * UnitPrice;
            Notify("LinePrice");
        }

        public void UpdateTotalLinePrice()
        {
            TotalLinePrice = LinePrice - DiscountAmount;
            Notify("TotalLinePrice");
        }

        public void UpdateTotalLinePriceOnPercent()
        {
            var discount = (LinePrice * Discount) / 100;
            TotalLinePrice = LinePrice - discount;
            Notify("TotalLinePrice");
        }
    }
}
