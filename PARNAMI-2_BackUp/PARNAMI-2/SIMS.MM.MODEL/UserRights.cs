﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace SIMS.MM.MODEL
{
    [DataContract]
    public class UserRights : EntityModel
    {
        public UserRights()
        {
            IsValid = false;
        }

        public void SetAllCans(bool value)
        {
            foreach (var prop in this.GetType().GetProperties())
            {
                if (prop.Name.StartsWith("Can") && prop.PropertyType == typeof(bool))
                {
                    prop.SetValue(this, value, null);
                }
            }
        }

        [DataMember]
        public string Status { get; set; }

        [DataMember]
        public string Key { get; set; }

        [DataMember]
        public string UserName { get; set; }

        [DataMember]
        public bool IsValid { get; set; }

        [DataMember]
        public string[] Roles { get; set; }

        [DataMember]
        public bool CanManageTransporter { get; set; }

        [DataMember]
        public bool CanManageSites { get; set; }

        [DataMember]
        public bool CanManageVendors { get; set; }

        [DataMember]
        public bool CanManageItems { get; set; }

        [DataMember]
        public bool CanManageUsers { get; set; }

        [DataMember]
        public bool CanRaisePR { get; set; }

        [DataMember]
        public bool CanApprovePR { get; set; }

        [DataMember]
        public bool CanApprovePO { get; set; }

        [DataMember]
        public bool CanVerifyMI { get; set; }

        [DataMember]
        public bool CanRaisePO { get; set; }

        [DataMember]
        public bool CanEmailPR { get; set; }

        [DataMember]
        public bool CanReceive { get; set; }

        [DataMember]
        public bool CanApproveMTR { get; set; }

        [DataMember]
        public bool CanConsume { get; set; }

        [DataMember]
        public bool CanCreateMO { get; set; }

        [DataMember]
        public bool CanViewAllSites { get; set; }

        [DataMember]
        public int[] AllowedSiteIDs { get; set; }

        [DataMember]
        public bool CanViewReports { get; set; }

        [DataMember]
        public bool CanAdmin { get; set; }

        [DataMember]
        public bool CanApproveBill { get; set; }

        [DataMember]
        public bool CanCreateBill { get; set; }
    }
}
