﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SIMS.TM.BLL;
using SIMS.TM.Model;
 
namespace SIMS.TM.Services
{
    public class MBookService :IMBookService
    {
        IMBookDataProvider provider = new MBookDataProvider();
        IMBookManager manager = new MBookManager();

        public MMBook GetMBookDatails(int id)
        {
            throw new NotImplementedException();
        }

        public PaginatedData<MMBook> GetAllMBook(string username, int? pageindex, int? pagesize, string Key, DateTime? startDate, DateTime? endDate, int? sideid, bool showall = true)
        {
            return (PaginatedData<MMBook>)provider.GetMBookHeaders(username, pageindex, pagesize, Key,startDate,endDate, sideid, showall);
        }

        public MMBook GetMBookID(int MBookID)
        {
            throw new NotImplementedException();
        }

        public MMBook CreateMBook(MMBook _MBook)
        {
            throw new NotImplementedException();
        }

        public MMBook UpdateMBook(MMBook _MBook)
        {
            throw new NotImplementedException();
        }
    }
}
