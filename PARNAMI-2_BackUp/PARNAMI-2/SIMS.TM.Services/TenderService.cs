﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using SIMS.TM.Model;
using SIMS.TM.BLL;

namespace SIMS.TM.Services
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "TenderService" in both code and config file together.
    public class TenderService : ITenderService
    {
        ITenderDataProvider provider = new TenderDataProvider();
        ITenderManager manager = new TenderManager();
        public MTender GetTenderDatails(int id)
        {
            return MTender.GetTestMTender();
        }

        public PaginatedData<MTender> GetAllTenders(string username, int? pageindex, int? pagesize, string Key, int? sideid, bool showall = true)
        {
            return (PaginatedData<MTender>)provider.GetTenderHeaders(username, pageindex, pagesize,Key,sideid,showall);
        }

        public MTender GetTenderByID(int TenderID)
        {
            return provider.GetTenderByID(TenderID);
        }


        public MTender CreateNewTender(MTender _NewTender)
        {
           return manager.CreateNewTender(_NewTender);
        }

        public MTender UpdateTender(MTender _UpdatedTender)
        {
            return manager.UpdateTender(_UpdatedTender);
        }
    }
}
