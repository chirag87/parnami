﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace SIMS.TM.Model
{
    [DataContract]
    public class MMBook
    {
        [DataMember]
        public int ID { get; set; }

        [DataMember]
        public string Description { get; set; }

        [DataMember]
        public string Floor { get; set; }

        [DataMember]
        public string Site { get; set; }

        [DataMember]
        public string Remarks { get; set; }

        [DataMember]
        public string UpdatedBy { get; set; }

        [DataMember]
        public DateTime? UpdatedOn { get; set; }

        [DataMember]
        public bool IsBilled { get; set; }

        [DataMember]
        public int BillID { get; set; }

        [DataMember]
        public DateTime? CreatedOn { get; set; }
    }
}
