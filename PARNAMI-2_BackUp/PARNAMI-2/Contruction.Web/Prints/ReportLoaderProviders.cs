﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Contruction.Web
{
    using SIMS.ReportLoader;
    public class ReportLoaderProviders : IReportLoaderProvider
    {
        public ReportLoaderProviders()
        {

        }

        public IReportLoader GetReportLoader(string type)
        {
            string path = "~/Prints/" + type + ".rdlc";
            IReportLoader loader = new BlankReportLoader();
            switch (type)
            {
                case "VendorBill":
                    loader = new SIMS.MM.DAL.PrintReports.VendorBillPrintLoader();
                    // Change Path if Required
                    break;

                case "PO":
                    loader = new SIMS.MM.DAL.PrintReports.POPrintLoader();
                    // Change Path if Required
                    break;

                case "PR":
                    loader = new SIMS.MM.DAL.PrintReports.PRPrintLoader();
                    // Change Path if Required
                    break;

                case "GRN":
                    loader = new SIMS.MM.DAL.PrintReports.GRNPrintLoader();
                    // Change Path if Required
                    break;

                case "Cash":
                    loader = new SIMS.MM.DAL.PrintReports.CashPrintLoader();
                    // Change Path if Required
                    break;

                case "TO":
                    loader = new SIMS.MM.DAL.PrintReports.TOPrintLoader();
                    // Change Path if Required
                    break;

                case "TODispatchC":
                    loader = new SIMS.MM.DAL.PrintReports.TODispatchPrintLoader();
                    // Change Path if Required
                    break;

                case "TODispatchR":
                    loader = new SIMS.MM.DAL.PrintReports.TODispatchPrintLoader();
                    // Change Path if Required
                    break;

                case "TOReceiveC":
                    loader = new SIMS.MM.DAL.PrintReports.TOReceivePrintLoader();
                    // Change Path if Required
                    break;

                case "TOReceiveR":
                    loader = new SIMS.MM.DAL.PrintReports.TOReceivePrintLoader();
                    // Change Path if Required
                    break;

                default:
                    break;
            }
            if (loader != null)
            {
                loader.ReportPath = path;
                loader.DictionaryConverter = new QueryToDicConverter(type);
            }
            return loader;
        }
    }

    public class BlankReportLoader : ReportLoader
    {
        public override RDLCReportData GetReportData(ReportParameterDictionary Dic)
        {
            return new RDLCReportData();
        }
    }

    public class QueryToDicConverter : IDictionaryConverter
    {
        string _type;

        public QueryToDicConverter(string type) { _type = type; }

        public ReportParameterDictionary GetDic(object obj)
        {
            var Query = (System.Collections.Specialized.NameValueCollection)obj;
            ReportParameterDictionary dic = new ReportParameterDictionary();

            if (Query["id"] != null)
                dic.Add("id", Int32.Parse(Query["id"]));

            if (_type.Equals("VendorBill", StringComparison.OrdinalIgnoreCase))
            {
                return dic;
            }

            if (_type.Equals("PO", StringComparison.OrdinalIgnoreCase))
            {
                return dic;
            }

            if (_type.Equals("PR", StringComparison.OrdinalIgnoreCase))
            {
                return dic;
            }

            if (_type.Equals("GRN", StringComparison.OrdinalIgnoreCase))
            {
                return dic;
            }

            if (_type.Equals("TO", StringComparison.OrdinalIgnoreCase))
            {
                return dic;
            }

            if (_type.Equals("TODispatchC", StringComparison.OrdinalIgnoreCase))
            {
                return dic;
            }

            if (_type.Equals("TODispatchR", StringComparison.OrdinalIgnoreCase))
            {
                return dic;
            }

            if (_type.Equals("TOReceiveC", StringComparison.OrdinalIgnoreCase))
            {
                return dic;
            }

            if (_type.Equals("TOReceiveR", StringComparison.OrdinalIgnoreCase))
            {
                return dic;
            }

            return dic;
        }
    }
}