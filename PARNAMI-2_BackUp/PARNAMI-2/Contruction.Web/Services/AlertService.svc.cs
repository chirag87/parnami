﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using SIMS.MM.MODEL;
using System.Collections.ObjectModel;
using SIMS.MM.BLL;

namespace Contruction.Web.Services
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "AlertService" in code, svc and config file together.
    public class AlertService : IAlertService
    {
        IMasterDataProvider dp = new MasterDataProvier();

        public ObservableCollection<MAlerts> GetAlerts(string currentuser)
        {
            var data = dp.GetAlerts(currentuser);
            return data;

            #region TestData...
            //ObservableCollection<MAlerts> alerts = new ObservableCollection<MAlerts>();
            //alerts.Add(new MAlerts()
            //{
            //    Title = "PO Alerts",
            //    AlertType = "PO",
            //    Description = "New PO Raised",
            //});
            //alerts.Add(new MAlerts()
            //{
            //    Title = "Stock Alerts",
            //    AlertType = "Stock",
            //    Description = "Safety Stock for Items is Going below safety measures. Please Reorder in order to maintain",
            //});
            //alerts.Add(new MAlerts()
            //{
            //    Title = "PO Alerts",
            //    AlertType = "PO",
            //    Description = "wekuhdjxbefjekuysgbkuhvckuygfkrygskbxucgkvuygkuygkuyrgkxuycgkufygrkuycgx kuhfv kuyrg kuydkvxudyrkuxygrvkbdxkuy vkuhkuyxkv uvkuyrkuykcuhbkiyb kvhbkviybv kihkfiuvk rbxkidufvh kiubvk hcblifugnviu blijg b;ng",
            //});
            #endregion
        }
    }
}