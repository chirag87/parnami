﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using SIMS.MM.BLL.Services;
using SIMS.MM.MODEL;
using System.Web.Security;
using System.Web;
using SIMS.MM.DAL;

namespace Contruction.Web.Services
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "LoginService" in code, svc and config file together.
    public class LoginService : ILoginService
    {
        AuthenticationManager manager = new AuthenticationManager();
        string Username="";

        public SIMS.MM.MODEL.UserRights Login(string username, string password)
        {
            UserRights rights = new UserRights();
            rights.IsValid = manager.ValidateUser(username,password);
            if (rights.IsValid)
            {
                Username = username;
                rights.UserName = username;
                SetRightsForUser(rights);
            }
            else
            {
                rights.Status = "Username or password is invalid!";
                rights.SetAllCans(false);
            }
            return rights;
        }

        public static bool CanShowAllSites(string Username)
        {
            return Roles.IsUserInRole(Username, "dba") || Roles.IsUserInRole(Username, "Admin");
        }

        public static string CurrentUser { get { return HttpContext.Current.User.Identity.Name; } }
        public static bool Authenticate() { if (HttpContext.Current.User.Identity.IsAuthenticated) return true; throw new Exception("User Not Authenticated"); }
        public static bool IsUserInRole(string role) { return HttpContext.Current.User.IsInRole(role); }

        bool IsInRole(string roleName)
        {
            return Roles.IsUserInRole(Username, roleName);
        }

        void SetRightsForUser(UserRights rights)
        {
            if (Roles.IsUserInRole(Username, "dba"))
            {    
                rights.SetAllCans(true);
                return;
            }
            if (Roles.IsUserInRole(Username, "Admin"))
            {    
                rights.SetAllCans(true);
                rights.CanManageUsers = false;
                return;
            }
            rights.CanApprovePR = IsInRole("Purchasing Approver");
            rights.CanRaisePR = IsInRole("Purchaser") || IsInRole("Purchasing Approver") || IsInRole("Purchasing Manager");
            rights.CanRaisePO = IsInRole("Purchasing Manager");
            rights.CanManageItems = IsInRole("ItemsAdmin");
            rights.CanManageVendors = IsInRole("VendorsAdmin");
            rights.CanManageSites = IsInRole("SitesAdmin");
            rights.CanConsume = IsInRole("Store");
            rights.CanReceive = IsInRole("Store");
            rights.CanViewReports = IsInRole("ReportingUser");

            rights.CanManageTransporter = IsInRole("TransportersRight");

            rights.CanCreateMO = false;
            parnamidb1Entities db = new parnamidb1Entities();
            rights.AllowedSiteIDs = db.GetAllowedSiteIDs(Username, rights.CanViewAllSites).ToArray();
        }


        public IEnumerable<UserInfo> GetAllUsersInfo()
        {
            SIMS.MM.DAL.parnamidb1Entities db = new SIMS.MM.DAL.parnamidb1Entities();

            var usrs = Membership.GetAllUsers();
            List<UserInfo> list = new List<UserInfo>();
            foreach (var v in usrs)
            {
                var usr = (MembershipUser)v;
                UserInfo info = new UserInfo()
                {
                    Username = usr.UserName,
                    IsApproved = usr.IsApproved,
                    IsOnline = usr.IsOnline,
                    Roles = Roles.GetRolesForUser(usr.UserName)
                };
                info.AllowedSiteIDs = db.GetAllowedSiteIDs(usr.UserName).ToList();
                foreach (var siteid in info.AllowedSiteIDs)
                {
                    info.AllowedSites += db.Buses.Single(x => x.ID == siteid).Name+", ";
                }
                list.Add(info);
            }
            return list;
        }

        public bool ChangePasswordForced(string username, string newpassword)
        {
            throw new NotImplementedException();
        }

        public bool ChangePassword(string username, string oldPassword, string newpassword)
        {
            Membership.GetUser(username).ChangePassword(oldPassword, newpassword);
            return true;
        }

        public IEnumerable<UserInfo> AddRolesToUser(string username, string[] roles)
        {
            Roles.AddUserToRoles(username, roles);
            return this.GetAllUsersInfo();
        }

        public IEnumerable<UserInfo> RemoveRolesToUser(string username, string[] roles)
        {
            Roles.RemoveUserFromRoles(username, roles);
            return this.GetAllUsersInfo();
        }

        public IEnumerable<string> GetAllRoles()
        {
            return Roles.GetAllRoles();
        }

        public IEnumerable<UserInfo> BlockUser(string username)
        {
            var usr = Membership.GetUser(username);
            usr.IsApproved = false;
            Membership.UpdateUser(usr);
            return this.GetAllUsersInfo();
        }

        public IEnumerable<UserInfo> AllowUser(string username)
        {
            var usr = Membership.GetUser(username);
            usr.IsApproved = true;
            Membership.UpdateUser(usr);
            return this.GetAllUsersInfo();
        }


        public IEnumerable<UserInfo> AddSitesToUser(string username, int[] sites)
        {
            SIMS.MM.DAL.parnamidb1Entities db = new SIMS.MM.DAL.parnamidb1Entities();
            foreach(var site in sites)
            {
                if(db.AllowedBUs.Any(x=>x.LoginID == username && x.BUID == site))
                    continue;
                db.AllowedBUs.AddObject(new SIMS.MM.DAL.AllowedBU(){LoginID = username,
                BUID = site});
            }
            db.SaveChanges();
            return this.GetAllUsersInfo();
        }

        public IEnumerable<UserInfo> RemoveSitesToUser(string username, int[] sites)
        {
            SIMS.MM.DAL.parnamidb1Entities db = new SIMS.MM.DAL.parnamidb1Entities();
            foreach(var site in sites)
            {
                if(!db.AllowedBUs.Any(x=>x.LoginID == username && x.BUID == site))
                    continue;
                db.AllowedBUs.DeleteObject(db.AllowedBUs.Single(x=>x.LoginID == username && x.BUID == site));
            }
            db.SaveChanges();
            return this.GetAllUsersInfo();
        }

        public void CreateNewUser(string username, string password, string email)
        {
            var status = new MembershipCreateStatus();
            if (username == null)
                status = MembershipCreateStatus.InvalidUserName;
            if (password == null)
                status = MembershipCreateStatus.InvalidPassword;
            if (email == null)
                status = MembershipCreateStatus.InvalidEmail;

            manager.CreateUser(username, password, email, "NhiPata", "NhiPata", true, out status);
            if (status != MembershipCreateStatus.Success)
                throw new FaultException(status.ToString());
        }
    }
}
