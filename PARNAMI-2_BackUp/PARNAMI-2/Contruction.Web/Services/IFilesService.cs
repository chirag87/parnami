﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using SIMS.MM.MODEL;

namespace Contruction.Web.Services
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IFilesService" in both code and config file together.
    [ServiceContract]
    public interface IFilesService
    {
        [OperationContract]
        bool UploadPhoto(MItem picture);
    }
}
