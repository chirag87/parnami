﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using SIMS.Core;
using SIMS.MM.MODEL;
using SIMS.MM.BLL;
using Contruction.Web.Services;

namespace Contruction.Web
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "TransactionService" in code, svc and config file together.
    public class TransactionService : ITransactionService
    {
        ITransactionDataProvider dp = new TransactionDataProvider();
        ITransactionManager pm = new TransactionManager();

        public PaginatedData<MGRN> GetAllGRNs(string username, int? pageindex, int? pagesize, string key, int? vendorid, int? siteid)
        {
            var showAll = LoginService.CanShowAllSites(username);
            var data = (PaginatedData<MGRN>)dp.GetAllGRNs(username, pageindex, pagesize, key, vendorid, siteid, showAll);
            return data;
        }

        public PaginatedData<MCashPurchase> GetAllCashPurchases(string username, int? pageindex, int? pagesize, string key, int? vendorid, int? siteid)
        {
            var showAll = LoginService.CanShowAllSites(username);
            var data = (PaginatedData<MCashPurchase>)dp.GetAllCashPurchases(username, pageindex, pagesize, key, vendorid, siteid, showAll);
            return data;
        }

        public PaginatedData<MConsumption> GetAllConsumptions(string username, int? pageindex, int? pagesize, string key, int? siteid)
        {
            var showAll = LoginService.CanShowAllSites(username);
            var data = (PaginatedData<MConsumption>)dp.GetAllConsumptions(username, pageindex, pagesize, key, siteid, showAll);
            return data;
        }

        public MGRN GetGRNByID(int id)
        {
            var data = dp.GetGRNByID(id);
            return data;
        }

        public MCashPurchase GetCashPurchaseByID(int id)
        {
            var data = dp.GetCashPurchaseByID(id);
            return data;
        }

        public MConsumption GetConsumptionByID(int id)
        {
            var data = dp.GetConsumptionByID(id);
            return data;
        }

        public MGRN CreateNewGRN(MGRN NewGRN, string username)
        {
            var data = pm.CreateNewGRN(NewGRN, username);
            return data;
        }

        public MCashPurchase CreateNewCashPurchase(MCashPurchase newCP, string username)
        {
            var data = pm.CreateNewCashPurchase(newCP, username);
            return data;
        }

        public MConsumption CreateNewConsumption(MConsumption newCon, string username)
        {
            var data = pm.CreateNewConsumption(newCon, username);
            return data;
        }

        public IEnumerable<PendingPOInfo> GetPendingPOInfos(int vendorid, int siteid, int itemid, int? sizeid, int? brandid)
        {
            PurchasingManager pm = new PurchasingManager();
            var data = pm.GetPendingInfo(vendorid, siteid, itemid, sizeid, brandid);
            if (data.Count() == 0)
            {
                PendingPOInfo info = new PendingPOInfo()
                {
                    ItemID = itemid,
                    IsBlank = true
                };
                List<PendingPOInfo> list = new List<PendingPOInfo>();
                list.Add(info);
                return list;
            }
            return data;
        }

        public MGRN ConvertfromPO(int poid)
        {
            var data = pm.ConvertfromPO(poid);
            return data;
        }
    }
}
