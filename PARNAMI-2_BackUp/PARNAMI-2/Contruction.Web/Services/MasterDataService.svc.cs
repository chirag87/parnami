﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using Contruction.Web.Data;
using SIMS.MM.MODEL;
using SIMS.MM.BLL;
using System.Collections.ObjectModel;

namespace Contruction.Web.Services
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "MasterDataService" in code, svc and config file together.
    //[ServiceBehavior(MaxItemsInObjectGraph = 2147483647, IncludeExceptionDetailInFaults=true, TransactionTimeout = "00:20:00")]
    public class MasterDataService : IMasterDataService
    {
        IMasterManager pm = new MasterManager();
        IMasterDataProvider dp = new MasterDataProvier();


        //[OperationBehavior(TransactionScopeRequired=true)]
        public IEnumerable<MItem> GetAllItems()
        {
            //System.Threading.Thread.Sleep(1000*60*2);
            var data = dp.GetItemHeaders();
            return data;
        }

        public IEnumerable<MMeasurementUnit> GetAllMU()
        {
            var data = dp.GetAllMUs();
            return data;
        }

        public IEnumerable<string> GetAllMUTypes()
        {
            var data = dp.GetAllMUTypes();
            return data;
        }

        public IEnumerable<MItemCategory> GetAllItemCategories()
        {
            var data = dp.GetItemCategories();
            return data;
        }

        public IEnumerable<string> GetAllItemNames()
        {
            var data = dp.GetItemName();
            return data;
        }

        public IEnumerable<MVendor> GetAllVendors()
        {
            var data = dp.GetVendorHeaders();
            return data;
        }

        public IEnumerable<MSite> GetAllSites()
        {
            var data = dp.GetSiteHeaders();
            return data;
        }

        public IEnumerable<MTransporter> GetAllTransporters()
        {
            var data = dp.GetAllTransporters();
            return data;
        }

        public IEnumerable<MPriceMaster> GetAllPricing(string key, int? vendorid, int? siteid, int? itemid)
        {
            var data = dp.GetAllMasterPrice(key, vendorid, siteid, itemid);
            return data;
        }

        public IEnumerable<MAddress> GetAllAddresses()
        {
            var data = dp.GetAllAddresses();
            return data;
        }

        public IEnumerable<MDocument> GetAllDocuments()
        {
            var data = dp.GetAllDocuments();
            return data;
        }

        public IEnumerable<MContact> GetAllContacts()
        {
            var data = dp.GetAllContacts();
            return data;
        }
        
        public IEnumerable<MReceivingDetailsforMasters> GetAllReceivingsforMasters(int? vendorid, int? siteid)
        {
            var data = dp.GetAllReceivingsforMasters(vendorid, siteid);
            return data;
        }

        public IEnumerable<MAccounts> GetAllAccounts()
        {
            var data = dp.GetAllAccounts();
            return data;
        }

        public IEnumerable<MCompany> GetAllCompanies()
        {
            var data = dp.GetAllCompanies();
            return data;
        }

        public IEnumerable<MItemSize> GetAllItemSizes()
        {
            var data = dp.GetAllItemSizes();
            return data;
        }

        public IEnumerable<MItemBrand> GetAllItemBrands()
        {
            var data = dp.GetAllItemBrands();
            return data;
        }

        public IEnumerable<MClient> GetAllClients()
        {
            var data = dp.GetAllClients();
            return data;
        }






        public MVendor GetVendorbyID(int id)
        {
            var data = dp.GetVendorByID(id);
            return data;
        }

        public MSite GetSitebyID(int id)
        {
            var data = dp.GetSitebyID(id);
            return data;
        }

        public MItem GetItembyID(int id)
        {
            var data = dp.GetItembyID(id);
            return data;
        }

        public IEnumerable<MItem> GetItemsByCategoryID(int _CategoryID)
        {
            var data = dp.GetItemsByCategoryID(_CategoryID);
            return data;
        }

        public MAddress GetAddressbyID(int addressid)
        {
            var data = dp.GetAddressByID(addressid);
            return data;
        }

        public IEnumerable<MAddress> GetAddressbyForID(int ForID, string For)
        {
            var data = dp.GetAddressbyForID(ForID, For);
            return data;
        }

        public IEnumerable<MDocument> GetDocumentsbyForID(int ForID, string For)
        {
            var data = dp.GetDocumentbyForID(ForID, For);
            return data;
        }

        public MDocument GetDocumentbyID(int documentid)
        {
            var data = dp.GetDocumentbyID(documentid);
            return data;
        }

        public MContact GetContactbyID(int contactid)
        {
            var data = dp.GetContactbyID(contactid);
            return data;
        }

        public IEnumerable<MContact> GetContactsbyForID(int ForID, string For)
        {
            var data = dp.GetContactbyForID(ForID, For);
            return data;
        }

        public IEnumerable<MAccounts> GetAccountsbyForID(int ForID, string For)
        {
            var data = dp.GetAccountsbyForID(ForID, For);
            return data;
        }

        public MAccounts GetAccountsbyID(int _AcountsID)
        {
            var data = dp.GetAccountsbyID(_AcountsID);
            return data;
        }

        public List<MStockReport> GetStockReport(DateTime? sdate, DateTime? edate, int? siteid, int? itemid, int? sizeid, int? brandid)
        {
            var data = dp.GetStockSummary(sdate, edate, siteid, itemid, sizeid, brandid).ToList();
            return data;
        }

        public IEnumerable<MSafetyStock> GetSafetyStockbySiteID(int? siteid)
        {
            var data = (IEnumerable<MSafetyStock>)dp.GetSafetyStockbySiteID(siteid);
            return data;
        }

        public IEnumerable<MItemCategory> GetItemParents(int itemID)
        {
            var data = dp.GetItemParents(itemID);
            return data;
        }

        public IEnumerable<MPriceMaster> GetAllPricesByISV(int vendorid, int? siteid, int itemid)
        {
            var data = dp.GetAllPricesByISV(vendorid, siteid, itemid);
            return data;
        }

        public MCompany GetCompanyByID(int ID)
        {
            return dp.GetCompanyByID(ID);
        }







        public void CreateNewItem(MItem NewItem, string ItemCategory)
        {
            pm.CreateNewItem(NewItem, ItemCategory);
        }

        public MItem CreateItem(MItem newItem)
        {
            var data = pm.CreateNewItem(newItem);
            return data;
        }

        public void CreateMU(MMeasurementUnit newMU)
        {
            pm.CreateNewMU(newMU);
        }

        public void CreateNewSite(MSite NewSite)
        {
            pm.CreateNewSite(NewSite);
        }

        public void CreateNewVendor(MVendor NewVendor)
        {
            pm.CreateNewVendor(NewVendor);
        }

        public void CreateNewPricing(IEnumerable<MPriceMaster> pricemasterlist)
        {
            pm.CreateNewPricing(pricemasterlist);
        }

        public void CreateNewTransporter(MTransporter NewTransporter)
        {
            pm.CreateNewTransporter(NewTransporter);
        }

        public MItemCategory CreateNewItemCategory(MItemCategory _newItemCategory)
        {
            var data = pm.CreateNewItemCategory(_newItemCategory);
            return data;
        }

        public MAddress CreateNewAddress(MAddress NewAddress)
        {
            var data = pm.CreateNewAddress(NewAddress);
            return data;
        }

        public void SaveDocument(IEnumerable<MDocument> newDocument)
        {
            foreach (var item in newDocument)
            {
                pm.SaveDocument(item);
            }
        }

        public MDocument SaveDocuments(string FileName, string For, int? ForID, string DocType)
        {
            var data =pm.SaveDocument(FileName, For, ForID, DocType);
            return data;
        }

        public MContact CreateNewContact(MContact NewContact)
        {
            var data = pm.CreateNewContact(NewContact);
            return data;
        }

        public MAccounts CreateNewAccount(MAccounts _NewAccount)
        {
            var data = pm.CreateNewAccount(_NewAccount);
            return data;
        }

        public string NewPricing(MPriceMaster _NewPrice)
        {
            var data = pm.NewPricing(_NewPrice);
            return data;
        }

        public MCompany CreateNewCompany(MCompany NewCompany)
        {
            var data = pm.CreateNewCompany(NewCompany);
            return data;
        }

        public MItemSize CreateNewItemSize(MItemSize NewSize)
        {
            var data = pm.CreateNewItemSize(NewSize);
            return data;
        }

        public MItemBrand CreateNewItemBrand(MItemBrand NewBrand)
        {
            var data = pm.CreateNewItemBrand(NewBrand);
            return data;
        }

        public MClient CreateNewClient(MClient newClient)
        {
            var data = pm.CreateNewClient(newClient);
            return data;
        }







        public void DeleteItem(MItem DeletedItem)
        {
            pm.DeleteItem(DeletedItem.ID);
        }

        public void DeleteSite(MSite DeletedSite)
        {
            pm.DeleteSite(DeletedSite.ID);
        }

        public void DeleteVendor(MVendor DeletedVendor)
        {
            pm.DeleteVendor(DeletedVendor.ID);
        }

        public void DeleteTransporter(MTransporter DeletedTransporter)
        {
            pm.DeleteTransporter(DeletedTransporter.ID);
        }

        public void DeleteAddress(MAddress DeletedAddress)
        {
            pm.DeleteAddress(DeletedAddress.ID);
        }

        public void DeleteDocument(MDocument deletedDocument)
        {
            pm.DeleteDocument(deletedDocument.ID);
        }

        public void DeleteContact(int id)
        {
            pm.DeleteContact(id);
        }

        public void DeleteAccount(int _AccountID)
        {
            pm.DeleteAccount(_AccountID);
        }

        public void DeleteCompany(int ID)
        {
            pm.DeleteCompany(ID);
        }








        public void UpdateSites(IEnumerable<MSite> UpdatedSite)
        {
            foreach (var item in UpdatedSite)
            {
                pm.UpdateSite(item);
            }
        }

        public MClient UpdateClient(MClient updatedClient)
        {
            var data = pm.UpdateClient(updatedClient);
            return data;
        }

        public MSite UpdateSite(MSite UpdatedSite)
        {
            var data = pm.UpdateSite(UpdatedSite);
            return data;
        }

        public void UpdateVendors(IEnumerable<MVendor> UpdatedVendor)
        {
            foreach(var item in UpdatedVendor)
            {
                pm.UpdateVendor(item);
            }
        }

        public MVendor UpdateVendor(MVendor UpdatedVendor)
        {
            var data = pm.UpdateVendor(UpdatedVendor);
            return data;
        }

        public void UpdateTransporter(IEnumerable<MTransporter> UpdatedTransporter)
        {
            foreach (var item in UpdatedTransporter)
            {
                pm.UpdateTransporter(item);
            }
        }

        public void UpdateParentForItemCategory(int ID, int NewPID)
        {
            pm.UpdateParentForItemCategory(ID, NewPID);
        }

        public MItemCategory UpdateItemCategory(MItemCategory _updatedItemCategory)
        {
            var data = pm.UpdateItemCategory(_updatedItemCategory);
            return data;
        }

        public MAddress UpdateAddress(MAddress UpdatedAddress)
        {
            var data = pm.UpdateAddress(UpdatedAddress);
            return data;
        }

        public MContact UpdateContact(MContact UpdatedContact)
        {
            var data = pm.UpdateContact(UpdatedContact);
            return data;
        }

        public MAccounts UpdateAccount(MAccounts _updatedAccount)
        {
            var data = pm.UpdateAccount(_updatedAccount);
            return data;
        }

        public MItem UpdateItem(MItem UpdatedItem)
        {
            var data = pm.UpdateItem(UpdatedItem);
            return data;
        }

        public void SaveImage(string ImageName, int ItemID)
        {
            pm.SaveImagePath(ImageName, ItemID);
        }

        public void UpdatePrice(MPriceMaster _updatedPrice)
        {
            pm.UpdatePricing(_updatedPrice);
        }

        public MCompany UpdateCompany(MCompany company)
        {
            var data = pm.UpdateCompany(company);
            return data;
        }

        public IEnumerable<MItemCategory> GetItemCategoryLeaves(int CategoryID)
        {
            var data = dp.GetItemCategoryLeaves(CategoryID);
            return data;
        }

        public IEnumerable<MItem> GetVariationsforLeaf(int CategoryID)
        {
            var data = dp.GetVariationsforLeaf(CategoryID);
            return data;
        }

        public MItemSize UpdateItemSize(MItemSize updatedSize)
        {
            var data = pm.UpdateItemSize(updatedSize);
            return data;
        }
        
        public void UpdateItemSizes(IEnumerable<MItemSize> updatedSizes)
        {
            foreach (var item in updatedSizes)
            {
                pm.UpdateItemSize(item);
            }
        }

        public MItemBrand UpdateItemBrand(MItemBrand updatedBrand)
        {
            var data = pm.UpdateItemBrand(updatedBrand);
            return data;
        }

        public void UpdateItemBrands(IEnumerable<MItemBrand> updatedBrands)
        {
            foreach (var item in updatedBrands)
            {
                pm.UpdateItemBrand(item);
            }
        }
    }
}