﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using SIMS.MM.MODEL;

namespace Contruction.Web.Services
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IFeedbackService" in both code and config file together.
    [ServiceContract]
    public interface IFeedbackService
    {
        [OperationContract]
        void EmailFeedback(MFeedback feedback);

        [OperationContract]
        MFeedback CreateNewFeedback(MFeedback feedback);
    }
}
