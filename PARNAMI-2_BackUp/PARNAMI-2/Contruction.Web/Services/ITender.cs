﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using SIMS.MM.MODEL;
namespace Contruction.Web.Services
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "ITender" in both code and config file together.
    [ServiceContract]
    public interface ITender
    {
        [OperationContract]
        PaginatedData<MTender> GetAllTenders(string username, int? pageindex, int? pagesize, string Key, int? sideid, bool showall = true);
        
        [OperationContract]
        MTender GetTenderByID(int TenderID);
        [OperationContract]
        MTender CreateNewTender(MTender _NewTender);

        [OperationContract]
        MTender UpdateTender(MTender _UpdatedTender);
    }
}
