﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using SIMS.MM.DAL;
using SIMS.MM.BLL;
using System.Collections.ObjectModel;
using SIMS.MM.MODEL;
using SIMS.Core;

namespace Contruction.Web.Services
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "MessagingService" in code, svc and config file together.
    public class MessagingService : IMessagingService
    {
        IMessagingManager pm = new MessagingManager();
        IMessagingDataProvider dp = new MessagingDataProvider();

        public IEnumerable<MEmail> GetAllEmails()
        {
            var data = dp.GetAllEmails();
            return data;
        }

        public PaginatedData<MMessaging> GetAllMessages(string username, int? pageindex, int? pagesize, string key)
        {
            var data = (PaginatedData<MMessaging>)dp.GetAllMessages(username, pageindex, pagesize, key);
            return data;
        }

        public PaginatedData<MMessagingHeader> GetAllMessageHeaders(string username, int? pageindex, int? pagesize, string key)
        {
            var data = (PaginatedData<MMessagingHeader>)dp.GetAllMessageHeaders(username, pageindex, pagesize, key);
            return data;
        }

        public MMessaging GetMessageByID(int id)
        {
            var data = dp.GetMessageByID(id);
            return data;
        }




        public MMessaging SendNewMail(MMessaging NewEmail)
        {
            var data = pm.SendNewMail(NewEmail);
            return data;
        }

        public bool SetIsStar(int msgid, string user, bool IsStar)
        {
            var data = pm.SetIsStar(msgid, user, IsStar);
            return data;
        }

        public bool SetIsFlag(int msgid, string user, bool IsFlag)
        {
            var data = pm.SetIsFlag(msgid, user, IsFlag);
            return data;
        }

        public bool SetIsRead(int msgid, string user, bool IsRead)
        {
            var data = pm.SetIsRead(msgid, user, IsRead);
            return data;
        }

        public void DeleteMail(int Msgid, string user)
        {
            pm.DeleteMail(Msgid, user);
        }
    }
}
