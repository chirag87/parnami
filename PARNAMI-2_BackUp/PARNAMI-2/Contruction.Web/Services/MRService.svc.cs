﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using SIMS.MM.MODEL;
using SIMS.MM.BLL;
using SIMS.Core;

namespace Contruction.Web.Services
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "MRService" in code, svc and config file together.
    public class MRService : IMRService
    {
        IMRManager pm = new MRManager();
        IMRDataProvider dp = new MRDataProvider();

        public PaginatedData<MMRHeader> GetAllMRs(string username, int? pageindex, int? pagesize, string key, int? siteid)
        {
            var showAll = LoginService.CanShowAllSites(username);
            var data = (PaginatedData<MMRHeader>)dp.GetMRHeadersNew(username, pageindex, pagesize, key, siteid, showAll);
            return data;

            //var list = new List<MMRHeader>();
            //list.Add(new MMRHeader()
            //{
            //    ID = 1,
            //    RaiseOn = DateTime.UtcNow.AddHours(5.5),
            //    DisplaySite = "Site1"
            //});
            //list.Add(new MMRHeader()
            //{
            //    ID = 2,
            //    RaiseOn = DateTime.UtcNow.AddHours(5.5),
            //    DisplaySite = "Site2"
            //});

            //return (PaginatedData<MMRHeader>)list.ToPaginate(0, 20);
        }

        public MMR CreateNewMR(MMR NewMR)
        {
            var data = pm.CreateNewMR(NewMR);
            return data;
        }

        public MMR GetMRByID(int MRID)
        {
            var data = dp.GetMRByID(MRID);
            return data;
        }

        public int IssueMR(MReceiving mrec)
        {
            var data = pm.CreateMRIssue(mrec);
            return data;
        }

        public int ReturnMR(MReceiving mrec)
        {
            var data = pm.CreateMRReturn(mrec);
            return data;
        }

        public MReceiving GetMReceivingforMRIssue(int mrid)
        {
            var data = dp.GetMReceivingforMRIssue(mrid);
            return data;
        }

        public MReceiving GetMReceivingforMRReturn(int mrid)
        {
            var data = dp.GetMReceivingforMRReturn(mrid);
            return data;
        }

        public void CloseMR(int MRID)
        {
            pm.CloseMR(MRID);
        }

        /********************************************************************/
        /*NEW METHODS after Double Entry System*/
        /*By: MADHUR*/

        public MMaterialIssue GetMRIssueTransactionByMRID(int mrid)
        {
            var data = dp.GetMRIssueTransactionByMRID(mrid);
            return data;
        }

        public MMaterialReturn GetMRReturnTransactionByMRID(int mrid)
        {
            var data = dp.GetMRReturnTransactionByMRID(mrid);
            return data;
        }

        public int MRIssue(MMaterialIssue mrec, string username)
        {
            var data = pm.MRIssue(mrec, username);
            return data;
        }

        public int MRReturn(MMaterialReturn mrec, string username)
        {
            var data = pm.MRReturn(mrec, username);
            return data;
        }
        /********************************************************************/
    }
}
