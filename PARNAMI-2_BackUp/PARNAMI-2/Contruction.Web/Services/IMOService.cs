﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using SIMS.MM.MODEL;
using System.Collections.ObjectModel;
using SIMS.Core;

namespace Contruction.Web.Services
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IMOService" in both code and config file together.
    [ServiceContract]
    public interface IMOService
    {
        [OperationContract]
        PaginatedData<MMOHeader> GetAllMOs(string username, int? pageindex, int? pagesize, string key, int? transiteid, int? reqsiteid, string ConType, bool? IsClosed);

        [OperationContract]
        MMO CreateNewMO(MMO newMO);

        [OperationContract]
        MMO GetMObyID(int MOID);

        [OperationContract]
        string ReleaseMO(MReceiving mrec);

        [OperationContract]
        int ReceiveMO(MReceiving mrec);

        [OperationContract]
        MReceiving GetMReceivingforMORelease(int MOID);

        [OperationContract]
        MReceiving GetMReceivingforMOReceive(int MOID);

        [OperationContract]
        void CloseMO(int MOID);

        [OperationContract]
        MMO SplitToMO(MMO NewMO, int OldPRID, ObservableCollection<int> OldPRLines);

        [OperationContract]
        //MMO SplitToMOwithPartialQty(MMO NewMO, int OldPRID, ObservableCollection<MSplittedLines> SplittedLines);
        MMO SplitToMOwithPartialQty(MMO NewMO, int OldPRID, ObservableCollection<MMOSplittedLine> MOSplittedLines);

        [OperationContract]
        MMO CombineMO(int moid);

        [OperationContract]
        bool IsSimilarMOsAvailable(int moid);

        /*****************************************************************************/
        /****** New, By:Madhur******/

        [OperationContract]
        MMOReleasing GetMOReleaseTransactionByMOID(int moid);

        [OperationContract]
        MMOReceiving GetMOReceiveTransactionByMOID(int moid);

        [OperationContract]
        string MORelease(MMOReleasing mrec, string username);

        [OperationContract]
        int MOReceive(MMOReceiving mrec, string username);

        [OperationContract]
        IEnumerable<MMOReleasing> GetMOReleasingsAgainstMO(int moid);

        [OperationContract]
        IEnumerable<MMOReceiving> GetMOReceivingsAgainstMO(int moid);

        /*****************************************************************************/
    }
}
