﻿using System;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Collections;
using SIMS.MM.DAL;
using SIMS.MM.MODEL;
using System.Collections.Generic;

namespace Contruction.Web.Services
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IMasterReportingService" in both code and config file together.
    [ServiceContract]
    public interface IMasterReportingService
    {
        [OperationContract]
        List<MPOReport> GetPOReport(DateTime? startdate, DateTime? enddate, int? siteid, int? vendorid, int? itemid, int? itemcatid, int? sizeid, int? brandid);

        [OperationContract]
        List<MGRNReport> GetGRNReport(DateTime? sdate, DateTime? edate, int? siteid, int? vendorid, int? itemid, int? sizeid, int? brandid);

        [OperationContract]
        List<MCPReport> GetCPReport(DateTime? sdate, DateTime? edate, int? siteid, int? vendorid, int? itemid, int? sizeid, int? brandid);

        [OperationContract]
        List<MDPReport> GetDPReport(DateTime? sdate, DateTime? edate, int? siteid, int? vendorid, int? itemid, int? sizeid, int? brandid);

        [OperationContract]
        List<MDirectGRNReport> GetDirectGRNReport(DateTime? sdate, DateTime? edate, int? siteid, int? vendorid, int? itemid, int? sizeid, int? brandid);

        [OperationContract]
        List<MConsumptionReport> GetConsumptionReport(DateTime? sdate, DateTime? edate, int? siteid, int? vendorid, int? itemid, int? sizeid, int? brandid);

        [OperationContract]
        List<MMOReport> GetMMOReport(DateTime? sdate, DateTime? edate, int? siteid, int? vendorid, int? itemid, int? sizeid, int? brandid);

        [OperationContract]
        List<MBilledReceivings> GetBilledReceivings(DateTime? sdate, DateTime? edate, int? siteid, int? vendorid, int? itemid, int? sizeid, int? brandid);
        
        [OperationContract]
        List<MDirectGRNReport> GetUnBilledReceivings(DateTime? sdate, DateTime? edate, int? siteid, int? vendorid, int? itemid, int? sizeid, int? brandid);

        [OperationContract]
        List<MStockReport> GetStockReport(DateTime? sdate, DateTime? edate, int? siteid, int? itemid);

        [OperationContract]
        List<MMonthlyStockReport> GetMonthlyStockReport(DateTime? sdate, DateTime? edate, int? siteid, int? itemid);

        [OperationContract]
        List<MDailyStockReport> GetDailyStockReport(DateTime? sdate, DateTime? edate, int? siteid, int? itemid);
    }
}