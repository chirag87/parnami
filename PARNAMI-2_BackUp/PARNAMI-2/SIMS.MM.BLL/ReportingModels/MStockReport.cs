﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace SIMS.MM.BLL.ReportingModels
{
    [DataContract]
    public class MStockReport
    {
        [DataMember]
        public int RefNo { get; set; }

        [DataMember]
        public string Site { get; set; }

        [DataMember]
        public string Item { get; set; }

        [DataMember]
        public string MU { get; set; }

        [DataMember]
        public double OpeningBalance { get; set; }

        [DataMember]
        public double QtyIn  { get; set; }

        [DataMember]
        public double QtyOut { get; set; }

        [DataMember]
        public double ClosingBalance { get; set; }

        [DataMember]
        public string Remarks { get; set; }
    }
}
