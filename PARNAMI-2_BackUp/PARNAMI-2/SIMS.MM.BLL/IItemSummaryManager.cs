﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SIMS.MM.MODEL;
using System.ServiceModel;
using System.Collections.ObjectModel;
using SIMS.MM.MODEL;
using SIMS.Core;
using SIMS.MM.DAL;

namespace SIMS.MM.BLL
{
    [ServiceContract]
    public interface IItemSummaryManager
    {
        [OperationContract]
        IQueryable<MStockInINDENT> GetItemQtyInIndents(int? itemid, int? sizeid, int? brandid);

        [OperationContract]
        IQueryable<MStockInPO> GetItemQtyInPOs(int? itemid, int? sizeid, int? brandid);

        [OperationContract]
        IQueryable<MStockOnSites> GetItemQtyOnSites(int? itemid, int? sizeid, int? brandid);

        [OperationContract]
        IQueryable<MRateHistory> GetRateHistoryOfItem(int? itemid, int? sizeid, int? brandid);
    }

    public class ItemSummaryManager : IItemSummaryManager
    {
        MMReportsDBDataContext dc = new MMReportsDBDataContext();

        public IQueryable<MStockInINDENT> GetItemQtyInIndents(int? itemid, int? sizeid, int? brandid)
        {
            var data = dc.GetItemsQtyInPRs(itemid, sizeid, brandid)
                .OrderByDescending(x => x.Date)
            .Select(x => new MStockInINDENT
            {
                Date = x.Date,
                IsApproved = x.IsApproved,
                PRID = x.PRID,
                Qty = x.Qty,
                ReferenceNo = x.ReferenceNo,
                Site = x.SiteName,
                SiteID = x.SiteID
            }).AsQueryable();
            return data;
        }

        public IQueryable<MStockInPO> GetItemQtyInPOs(int? itemid, int? sizeid, int? brandid)
        {
            var data = dc.GetItemsQtyInPOs(itemid, sizeid, brandid)
                .OrderByDescending(x => x.Date)
                .Select(x => new MStockInPO
                {
                    Date = x.Date,
                    POID = x.POID,
                    Qty = x.Qty,
                    ReferenceNo = x.ReferenceNo,
                    Site = x.SiteName,
                    SiteID = x.SiteID
                }).AsQueryable();
            return data;
        }

        public IQueryable<MStockOnSites> GetItemQtyOnSites(int? itemid, int? sizeid, int? brandid)
        {
            var data = dc.GetItemClosingStockSitewise(itemid, sizeid, brandid)
                .OrderBy(x => x.SiteID)
                .Select(x => new MStockOnSites
                {
                    SiteID = x.SiteID.Value,
                    Site = x.SiteName,
                    Qty = x.Balance.Value
                }).AsQueryable();
            return data;
        }

        public IQueryable<MRateHistory> GetRateHistoryOfItem(int? itemid, int? sizeid, int? brandid)
        {
            var data = dc.GetItemsRateHistoryinPOs(itemid, sizeid, brandid)
                .OrderByDescending(x => x.Date)
                .Select(x => new MRateHistory
                {
                    Date = x.Date,
                    POID = x.POID,
                    Qty = x.Qty,
                    Rate = x.Rate,
                    ReferenceNo = x.ReferenceNo,
                    Vendor = x.VendorName,
                    VendorID = x.VendorID
                }).AsQueryable();
            return data;
        }
    }

}
