﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SIMS.MM.BLL;
using System.Collections.ObjectModel;
using SIMS.MM.BLL.Services;
namespace SIMS.MM.BLL
{    
    using DAL;
    using MODEL;    
    public class PurchasingManager : IPurchasingManger
    {
        parnamidb1Entities db = new parnamidb1Entities();
        PurchasingDataProvider provider = new PurchasingDataProvider();

        PRDetail ConvertToPRDetail(MPRLine _mprLine)
        {
            PRDetail detail = new PRDetail()
            {
                ID = _mprLine.LineID,
                DeliveryDate = _mprLine.DeliveryDate,
                ItemID = _mprLine.ItemID,
                SizeID = _mprLine.SizeID,
                BrandID = _mprLine.BrandID,
                PRID = _mprLine.PRID,
                Qty = _mprLine.Quantity,
                LinePrice = _mprLine.LinePrice,
                UnitPrice = _mprLine.UnitPrice,
                Remarks = _mprLine.LineRemarks,
                LastPrice = _mprLine.LastPrice,
                DiscountType = _mprLine.DiscountType,
                Discount = _mprLine.Discount,
                TotalLinePrice = _mprLine.TotalLinePrice,
                ConsumableType = _mprLine.ConsumableType,
                AdjustedQty = 0,
                IsClosed = false,
                NetRejectedQty = 0,
                TotalReceivedQty = 0
            };
            return detail;
        }

        MPRLine ConvertMPRLinesToPRDetails(PRDetail _detail)
        {
            MPRLine mprline = new MPRLine()
            {
                //DeliveryDate=_detail.DeliveryDate,
                LineID = _detail.ID,
                ItemID = _detail.ItemID,
                SizeID = _detail.SizeID,
                BrandID = _detail.BrandID,
                PRID = _detail.PRID,
                Quantity = _detail.Qty,
                UnitPrice = (double)_detail.UnitPrice,
                LinePrice = (double)_detail.LinePrice,
                LastPrice = _detail.LastPrice,
                Discount = _detail.Discount,
                DiscountType = _detail.DiscountType,
                TotalLinePrice = _detail.TotalLinePrice,
                ConsumableType = _detail.ConsumableType
            };
            return mprline;
        }

        PR ConvertToPR(MPR _mpr)
        {
            PR pr = new PR()
            {
                ID = _mpr.PRNumber,
                CurrentOwnerID = _mpr.CurrentOwnerID,
                VendorID = _mpr.VendorID,
                VerifiedBy = _mpr.VerifiedBy,
                VerifiedOn = _mpr.VerifiedOn,
                ReqApprovedOn = _mpr.ReqApprovedOn,
                ReqApprovedBy = _mpr.ReqApprovedBy,
                ApprovedBy = _mpr.ApprovedBy,
                ApprovedOn = _mpr.ApprovedOn,
                Frieght = _mpr.Frieght,
                SubTotal = _mpr.TotalValue,
                TotalPrice = _mpr.TotalValue + _mpr.TAX,
                Total = _mpr.GrandTotal,
                PCST = _mpr.pCST,
                PVat = _mpr.pVAT,
                Tax = _mpr.TAX,
                IsVerified = _mpr.IsVerified,
                IsReqApproved = _mpr.IsReqApproved,
                IsApproved = _mpr.IsApproved,
                SiteID = _mpr.SiteID,
                PurchasedForUserID = _mpr.Purchaser,
                PurchaseTypeID = _mpr.PurchaseType,
                Remarks = _mpr.PRRemarks,
                RaisedBy = _mpr.RaisedBy,
                RaisedOn = _mpr.RaisedOn,
                ConvertedTOPO = _mpr.ConvertedToPO,
                ContactPersonName = _mpr.ContactPerson,
                ContactPersonPhone = _mpr.ContactMobile,
                PaymentTerms = _mpr.PaymentTerms,
                TotalDiscount = _mpr.TotalDiscount,
                NetAmount = _mpr.NetAmount,
                Status = _mpr.PRStatus,
                IsClosed = _mpr.IsClosed
                //ReferenceNo = _mpr.ReferenceNo,
                //CostCenterID = _mpr.CostCenterID,
                //POID = _mpr.POID
            };
            if (_mpr.PRLines != null)
            {
                _mpr.PRLines
                    .ToList()
                    .ForEach(x => pr.PRDetails.Add(ConvertToPRDetail(x)));
            }
            return pr;
        }

        MPR ConvertPRToMPR(PR _pr)
        {
            MPR mpr = new MPR()
            {
                PRNumber = _pr.ID,
                VendorID = _pr.VendorID,
                IsVerified = _pr.IsVerified,
                IsReqApproved = _pr.IsReqApproved,
                IsApproved = _pr.IsApproved,
                SiteID = _pr.SiteID,
                Purchaser = _pr.PurchasedForUserID,
                PurchaseType = _pr.PurchaseTypeID,
                PRRemarks = _pr.Remarks,
                PaymentTerms = _pr.PaymentTerms,
                pCST = _pr.PCST,
                pVAT = _pr.PVat,
                TotalDiscount = _pr.TotalDiscount,
                NetAmount = _pr.NetAmount,
                IsClosed = _pr.IsClosed
            };
            return mpr;
        }

        public MPR CreatePR(MPR NewPR)
        {
            var year= DateTime.UtcNow.AddHours(5.5).Year;
            //NewPR.PRNumber = db.GetMaxPRID() + 1;
            NewPR.PRStatus = "Draft";
            NewPR.IsClosed = false;
            NewPR.UpdateLineNumbers();
            var pr = ConvertToPR(NewPR);
            pr.RaisedOn = DateTime.UtcNow.AddHours(5.5);
            pr.Year = year;
            pr.SiteID = NewPR.SiteID;
            pr.SiteCode = db.GetSiteCode(NewPR.SiteID);
            pr.RID = db.GetMaxPRsRefID(pr.SiteCode, pr.Year) + 1;
            pr.ReferenceNo = "MI/" + pr.Year + "/S-" + pr.SiteCode + "/" + pr.RID;
            pr.CurrentOwnerID = "dba";
            db.UpdateLastPrices(pr);
            db.PRs.AddObject(pr);
            db.SaveChanges();
            return provider.GetPRbyId(pr.ID);
        }

        public MPR SplitPR(MPR NewPR, int OldPRID, ObservableCollection<int> OldPRLines)
        {
            var year = DateTime.UtcNow.AddHours(5.5).Year;
            NewPR.UpdateLineNumbers();
            var pr = ConvertToPR(NewPR);
            pr.RaisedOn = DateTime.UtcNow.AddHours(5.5);
            pr.Year = year;
            pr.SiteID = NewPR.SiteID;
            pr.SiteCode = db.GetSiteCode(NewPR.SiteID);
            pr.RID = db.GetMaxPRsRefID(pr.SiteCode, pr.Year) + 1;
            pr.ReferenceNo = "MI/" + pr.Year + "/S-" + pr.SiteCode + "/" + pr.RID;
            pr.CurrentOwnerID = "dba";
            db.UpdateLastPrices(pr);
            db.PRs.AddObject(pr);
            db.SaveChanges();
            foreach (var LineID in OldPRLines)
            {
                var data = db.PRDetails.Single(x => x.PRID == OldPRID && x.ID == LineID);
                if (data != null)
                {
                    db.PRDetails.DeleteObject(data);
                    db.SaveChanges();
                }
            }
            return provider.GetPRbyId(pr.ID);
        }

        public PR CopyPRObject(PR x)
        {
            PR newpr = new PR()
            {
                ApprovedBy = x.ApprovedBy,
                ApprovedOn = x.ApprovedOn,
                ContactPersonName = x.ContactPersonName,
                ContactPersonPhone = x.ContactPersonPhone,
                ConvertedTOPO = x.ConvertedTOPO,
                CurrentOwnerID = x.CurrentOwnerID,
                Frieght = x.Frieght,
                ID = x.ID,
                IsApproved = x.IsApproved,
                IsReqApproved = x.IsReqApproved,
                IsVerified = x.IsVerified,
                NetAmount = x.NetAmount,
                PaymentTerms = x.PaymentTerms,
                PCST = x.PCST,
                POID = x.POID,
                PurchasedForUserID = x.PurchasedForUserID,
                PurchaseType = x.PurchaseType,
                PurchaseTypeID = x.PurchaseTypeID,
                PVat = x.PVat,
                RaisedBy = x.RaisedBy,
                RaisedOn = x.RaisedOn,
                Remarks = x.Remarks,
                ReqApprovedBy = x.ReqApprovedBy,
                ReqApprovedOn = x.ReqApprovedOn,
                SiteCode = x.SiteCode,
                SiteID = x.SiteID,
                SubTotal = x.SubTotal,
                Tax = x.Tax,
                Total = x.Total,
                TotalDiscount = x.TotalDiscount,
                TotalPrice = x.TotalPrice,
                VendorID = x.VendorID,
                VerifiedBy = x.VerifiedBy,
                VerifiedOn = x.VerifiedOn,
                Status = x.Status,
                IsClosed = x.IsClosed
            };
            return newpr;
        }

        public PRDetail CopyPRLineObject(PRDetail x)
        {
            PRDetail newprline = new PRDetail()
            {
                AdjustedQty = x.AdjustedQty,
                BrandID = x.BrandID,
                ClosingRemarks = x.ClosingRemarks,
                ConsumableType = x.ConsumableType,
                DeliveryDate = x.DeliveryDate,
                Discount = x.Discount,
                DiscountType = x.DiscountType,
                ID = x.ID,
                IsClosed = x.IsClosed,
                ItemID = x.ItemID,
                LastPrice = x.LastPrice,
                LinePrice = x.LinePrice,
                NetRejectedQty = x.NetRejectedQty,
                PRID = x.PRID,
                Qty = x.Qty,
                Remarks = x.Remarks,
                SizeID = x.SizeID,
                TotalLinePrice = x.TotalLinePrice,
                TotalReceivedQty = x.TotalReceivedQty,
                UnitPrice = x.UnitPrice
            };
            return newprline;
        }

        public MPR SplitPRwithPartialQty(int OldPRID, ObservableCollection<MSplittedLines> NewPRLines)
        {
            var year = DateTime.UtcNow.AddHours(5.5).Year;
            var oldpr = db.PRs.Single(x => x.ID == OldPRID);
            PR newpr = new PR();
            newpr = CopyPRObject(oldpr);

            var FullSplittedLines = NewPRLines.Where(x => !x.IsPartial).ToList();
            var PartialSplittedLines = NewPRLines.Where(x => x.IsPartial && x.ConfirmedQty > 0).ToList();

            if (FullSplittedLines.Count > 0)
            {
                foreach (var line in FullSplittedLines)
                {
                    var oldline = db.PRDetails.Single(x => x.ID == line.LineID && x.PRID == OldPRID);
                    PRDetail newline = new PRDetail();
                    newline = CopyPRLineObject(oldline);
                    newpr.PRDetails.Add(newline);
                    oldpr.PRDetails.Remove(oldline);
                }
            }
            if (PartialSplittedLines.Count > 0)
            {
                foreach (var line in PartialSplittedLines)
                {
                    var oldline = db.PRDetails.Single(x => x.ID == line.LineID && x.PRID == OldPRID);
                    oldline.Qty = line.DiffQty;
                    
                    PRDetail newline = new PRDetail();
                    newline = CopyPRLineObject(oldline);
                    newline.Qty = line.ConfirmedQty;
                    newpr.PRDetails.Add(newline);
                }
            }
            newpr.RaisedOn = DateTime.UtcNow.AddHours(5.5);
            newpr.Year = year;
            newpr.RID = db.GetMaxPRsRefID(newpr.SiteCode, newpr.Year) + 1;
            newpr.ReferenceNo = "MI/" + newpr.Year + "/S-" + newpr.SiteCode + "/" + newpr.RID;
            int i = 1;
            foreach (var item in newpr.PRDetails)
            {
                item.ID = i;
                i++;
            }
            db.UpdateLastPrices(newpr);
            db.PRs.AddObject(newpr);
            db.SaveChanges();
            return provider.GetPRbyId(newpr.ID);
        }

        public MPRLine ConvertfromSplittedLines(MSplittedLines x)
        {
            MPRLine ln = new MPRLine()
            {
                LineID = x.LineID,
                ItemID = x.ItemID,
                Item = x.Item,
                SizeID = x.SizeID,
                Size = x.Size,
                BrandID = x.BrandID,
                Brand = x.Brand,
                Quantity = x.ConfirmedQty
            };
            return ln;
        }

        public MPR UpdatePR(MPR UPR)
        {
            var pr = db.PRs.Single(x => x.ID == UPR.PRNumber);
            pr.VendorID = UPR.VendorID;
            pr.PVat = UPR.pVAT;
            pr.PCST = UPR.pCST;
            pr.Frieght = UPR.Frieght;
            pr.Remarks = UPR.PRRemarks;
            pr.Tax = UPR.TAX;
            pr.ContactPersonName = UPR.ContactPerson;
            pr.ContactPersonPhone = UPR.ContactMobile;
            pr.PaymentTerms = UPR.PaymentTerms;

            pr.SubTotal = UPR.TotalValue;
            pr.TotalPrice = UPR.TotalValue + UPR.TAX;
            pr.Total = UPR.GrandTotal;
            pr.TotalDiscount = UPR.TotalDiscount;
            pr.NetAmount = UPR.NetAmount;
            pr.IsClosed = UPR.IsClosed;

            foreach (var line in UPR.PRLines)
            {
                PRDetail prline;
                prline = pr.PRDetails.SingleOrDefault(x => x.ID == line.LineID);
                if (prline == null)     // If New Line is added
                {
                    prline = new PRDetail();
                    prline.DeliveryDate = DateTime.UtcNow.AddHours(5.5);
                    prline.TotalReceivedQty = 0;
                    prline.NetRejectedQty = 0;
                    prline.ID = line.LineID;
                    pr.PRDetails.Add(prline);
                }
                
                prline.Qty = line.Quantity;
                prline.UnitPrice = line.UnitPrice;
                prline.LinePrice = line.LinePrice;
                prline.ItemID = line.ItemID;
                prline.SizeID = line.SizeID;
                prline.BrandID = line.BrandID;
                prline.Remarks = line.LineRemarks;
                prline.Discount = line.Discount;
                prline.DiscountType = line.DiscountType;
                prline.TotalLinePrice = line.TotalLinePrice;
                prline.ConsumableType = line.ConsumableType;
            }

            //Removal Loops i.e. if any line is deleted
            List<PRDetail> prsToRemove = new List<PRDetail>();
            prsToRemove.AddRange(pr.PRDetails.Where(x => !UPR.PRLines.Select(y => y.LineID).Contains(x.ID)));
            prsToRemove.ForEach(x => pr.PRDetails.Remove(x));
            db.SaveChanges();

            //var obj = ConvertToPR(UPR);
            //db.PRs.Attach(obj);
            //db.ObjectStateManager.ChangeObjectState(obj, System.Data.EntityState.Modified);

            //db.UpdateLastPrices(obj, true);
            //db.SaveChanges();
            return UPR;
        }

        public void DeletePR(int prid)
        {
            var row = db.PRs.Single(x => x.ID == prid);
            db.PRs.DeleteObject(row);
            var v = db.PRDetails.Single(x=>x.PRID==prid);
            db.PRDetails.DeleteObject(v);
            db.SaveChanges();

        }             

        public void EmailPR(int PRID)
        {
            var emailid = Services.AppSettingsService.GetLatestAppSettings().Email;
            var pr = db.PRs.Single(x => x.ID == PRID);
            //if (!pr.IsApproved)
            //    throw new Exception("PR Not Approved! Operation not Allowed !");
            try
            {
                Emailer.Emailer.SMSPO(emailid, pr, !pr.IsApproved);
            }
            catch { }
            Emailer.Emailer.EmailPO(emailid, pr, !pr.IsApproved);
        }

        public string ConfirmEmail(int POID)
        {
            var pr = db.PRs.Single(x => x.POID == POID);
            string email = pr.Vendor.Email;
            return email;
        }

        public void EmailtoVendor(int POID)
        {
            var pr = db.PRs.Single(x => x.POID == POID);
            var emailid = pr.Vendor.Email;
            try
            {
                if (emailid == null)
                {
                    throw new Exception("This Vendor do not carry any Email Address!");
                }
                else
                {
                    Emailer.Emailer.SMSPO(emailid, pr, !pr.IsApproved);
                }
            }
            catch { }
            Emailer.Emailer.EmailPO(emailid, pr, !pr.IsApproved);
        }

        public void ApprovePR(int PRID, string ApprovedBy, DateTime?ApprovedOn)
        {
            var obj = db.PRs.Single(x => x.ID == PRID);
            if (obj.IsApproved)
                throw new Exception("Already Approved!");            
            obj.IsApproved = true;
            obj.ApprovedBy = ApprovedBy;
            obj.ApprovedOn = ApprovedOn;
            obj.Status = "POApproved";
            db.SaveChanges();
        }

        public void VerifyPR(int PRID, string VerifiedBy, DateTime? VerifiedOn)
        {
            var obj = db.PRs.Single(x => x.ID == PRID);
            if (obj.IsVerified)
                throw new Exception("Already Verified!");
            obj.IsVerified = true;
            obj.VerifiedBy = VerifiedBy;
            obj.VerifiedOn = VerifiedOn;
            obj.Status = "SubmittedForPRApproval";
            db.SaveChanges();
        }

        public void InitialApprovePR(int PRID, string ReqApprovedBy, DateTime? ReqApprovedOn)
        {
            var obj = db.PRs.Single(x => x.ID == PRID);
            if (obj.IsReqApproved)
                throw new Exception("Already Approved Initially!");
            obj.IsReqApproved = true;
            obj.ReqApprovedBy = ReqApprovedBy;
            obj.ReqApprovedOn = ReqApprovedOn;
            obj.Status = "PRApproved";
            db.SaveChanges();
        }

        public void ChangesStateofPR(int PRID, string State)
        {
            var obj = db.PRs.Single(x => x.ID == PRID);
            obj.Status = State;
            db.SaveChanges();
        }

        public void AcknowledgePO(int poid,string remark)
        {
            var po = db.POs.Single(x => x.ID == poid);
            po.AcknowledgedOn = DateTime.UtcNow.AddHours(5.5);
            po.IsAcknowledged = true;
            po.Ack_Remarks = remark;
            db.SaveChanges();
        }

        public int ConvertToPO(int prid)
        {
            var pr = db.PRs.Single(x => x.ID == prid);
            if (!pr.IsApproved)
                throw new Exception("MI Not Approved!");
            POs po = new POs();
            po.ID = db.GetMaxPOID() + 1;
            po.Year = pr.Year;
            po.SiteID = pr.SiteCode;
            po.RID = db.GetMaxPORID(po.SiteID,po.Year) + 1;
            po.Display = "PO/" + po.Year + "/S-" + pr.SiteCode+ "/" + po.RID;
            po.IsAcknowledged = false;
            po.Tax = pr.Tax;
            po.Frieght = (double)pr.Frieght;
            po.Total = (double)pr.Total;
            po.NetTotal = pr.SubTotal;
            po.Remarks = pr.Remarks;            
            po.CreatedOn = DateTime.UtcNow.AddHours(5.5);
            po.CreatedBy = pr.RaisedBy;
            po.ContactPersonName = pr.ContactPersonName;
            po.ContactPersonPhone = pr.ContactPersonPhone;
            po.TotalDiscount = pr.TotalDiscount;
            po.NetAmount = pr.NetAmount;
            pr.ConvertedTOPO = true;
            pr.POs = po;
            pr.Status = "ConvertedToPO";
            pr.IsClosed = true;
            db.SaveChanges();
            return po.ID;
        }

        public IEnumerable<PendingPOInfo> GetPendingInfo(int vendorid, int siteid, int itemid, int? sizeid, int? brandid)
        {
            var data = db.PRDetails.AsQueryable();
            var data2 = data.Where(x => x.PR.VendorID == vendorid && x.PR.SiteID == siteid
                && x.PR.POID != null
                && x.ItemID == itemid
                //&& (x.SizeID == null || x.SizeID == sizeid)
                //&& (x.BrandID == null || x.BrandID == brandid)
                && x.Qty > x.TotalReceivedQty).ToList();
            var list = new List<PendingPOInfo>();
            if (data2.Count != 0)
            {
                foreach (var item in data2)
                {
                    var info = new PendingPOInfo()
                    {
                        PRID = item.PRID,
                        ItemID = item.ItemID,
                        SizeID = item.SizeID,
                        BrandID = item.BrandID,
                        PRLineID = item.ID,
                        POID = item.PR.POID.Value,
                        OrderedQty = item.Qty,
                        UnitPrice = item.UnitPrice,
                        LinePrice = item.LinePrice
                    };
                    if (item.TotalReceivedQty.HasValue)
                    {
                        info.EarlierReceivedQty = item.TotalReceivedQty.Value;
                    }
                    list.Add(info);
                }
            }
            return list;
        }

        public IEnumerable<int> GetAllowedItemId(int vendorid, int siteid)
        {
            var data = db.PRDetails;
            var data2 = data.Where(x => x.PR.VendorID == vendorid
                && x.PR.SiteID == siteid && x.PR.POID != null
                && x.Qty > x.TotalReceivedQty)
                .Select(x => x.ItemID).Distinct()
                .ToList();
            return data2;
        }
    }

    public static class MPRExtentions
    {
        public static MPRLine AddNewLine(this MPR mpr)
        {
            if (mpr.PRLines == null)
                mpr.PRLines = new ObservableCollection<MPRLine>();
            var line = new MPRLine();
            mpr.PRLines.Add(line);
            mpr.UpdateLineNumbers();
            return line;
        }

        public static int UpdateLineNumbers(this MPR mpr)
        {
            if (mpr.PRLines == null) return 0;
            int i=1;
            foreach (var item in mpr.PRLines)
            {
                item.LineID = i;
                i++;
            }
            return i-1;
        }
    }
}

    
