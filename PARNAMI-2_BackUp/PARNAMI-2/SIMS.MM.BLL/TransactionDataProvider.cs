﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SIMS.MM.DAL;
using SIMS.Core;
using SIMS.MM.MODEL;
using System.Collections.ObjectModel;

namespace SIMS.MM.BLL
{
    public class TransactionDataProvider : ITransactionDataProvider
    {
        parnamidb1Entities db = new parnamidb1Entities();
        MMReportsDBDataContext db1 = new MMReportsDBDataContext();

        public IPaginated<MGRN> GetAllGRNs(string username, int? pageindex, int? pagesize, string key, int? vendorid, int? siteid, bool showAll = true)
        {
            var data = db1.GetDirectGRNHeaders(username, key, vendorid, siteid, null, showAll);
            return data.OrderByDescending(x => x.ID)
                .ToPaginate(pageindex, pagesize)
                .Select(x => new MGRN
                {
                    ID = x.ID,
                    ReferenceNo = x.ReferenceNo,
                    ForDate = x.ForDate,
                    Vendor = x.Vendor,
                    Site = x.Site,
                    LastUpdatedOn = x.LastUpdatedOn,
                    Remarks = x.Remarks,
                    ChallanNo = x.ChallanNo,
                    BillingStatus = x.BillingStatus,
                    TotalTrnLines = x.TotalTrnLines,
                    BilliedTrnLines = x.BilledTrnLines,
                    UnBilliedTrnLines = x.UnBilledTrnLines
                });
        }

        public IPaginated<MCashPurchase> GetAllCashPurchases(string username, int? pageindex, int? pagesize, string key, int? vendorid, int? siteid, bool showAll = true)
        {
            var data = db1.GetCPHeaders(username, key, vendorid, siteid, showAll);
            return data.OrderByDescending(x => x.ID)
                .ToPaginate(pageindex, pagesize)
                .Select(x => new MCashPurchase
                {
                    ID = x.ID,
                    ReferenceNo = x.ReferenceNo,
                    ForDate = x.ForDate,
                    Vendor = x.Vendor,
                    Site = x.Site,
                    LastUpdatedOn = x.LastUpdatedOn,
                    Remarks = x.Remarks,
                    ChallanNo = x.ChallanNo,
                    Freight = x.Freight,
                    TAX = x.TAX,
                    GrossTotalofLines = x.GrossAmount,
                    TotalLinePrice = x.TotalAmount
                });
        }

        public IPaginated<MConsumption> GetAllConsumptions(string username, int? pageindex, int? pagesize, string key, int? siteid, bool showAll = true)
        {
            var data = db1.GetConsumptionHeaders(username, key, siteid, showAll);
            return data.OrderByDescending(x => x.ID)
                .ToPaginate(pageindex, pagesize)
                .Select(x => new MConsumption
                {
                    ID = x.ID,
                    ReferenceNo = x.ReferenceNo,
                    ForDate = x.ForDate,
                    Site = x.Site,
                    LastUpdatedOn = x.LastUpdatedOn,
                    Remarks = x.Remarks
                });
        }

        public MGRN GetGRNByID(int id)
        {
            var data = db.GRNs.Single(x => x.ID == id);
            //var lines = db1.GRN_GETGRNLINES(id, "GRN");
            var lines = db1.GRN_GETGRNLINESNEW(id);
            var obj = ConvertToMGRN(data, true);
            foreach (var l in lines)
            {
                MGRNLine rline = new MGRNLine()
                {
                    LineID = l.LINEID,
                    ItemID = l.ITEMID.Value,
                    Item = l.ITEM,
                    SizeID = l.SIZEID.Value,
                    Size = l.SIZE,
                    BrandID = l.BRANDID.Value,
                    Brand = l.BRAND,
                    UnitPrice = l.UnitPrice.Value,
                    LinePrice = l.LinePrice.Value,
                    OrderedQty = l.OrderedQty ?? 0,
                    EarlierReceivedQty = l.EarlierRCVED ?? 0,
                    Quantity = l.Qty.Value,
                    LineChallanNo = l.ChallanNo,
                    LineRemarks = l.Remarks,
                    BillDate = l.BILLDate,
                    BillingID = l.BILLID,
                    BillingLineID = l.BILLINGLINEID,
                    BillNo = l.BILLNo,
                    IsBillClosed = l.BillClosed,
                    TotalbilingAmnt = l.BillAmt,
                    TotalPaidAmnt = l.BillPaid,
                    TruckNo = l.TRUCKNo
                };
                obj.GRNLines.Add(rline);
                if (l.BILLID == null)           //to check if Bill exists or not
                {
                    rline.HasBill = false;
                }
                else
                {
                    rline.HasBill = true;
                }
            }
            return obj;
        }

        public MCashPurchase GetCashPurchaseByID(int id)
        {
            var data = db.CashPurchases.Single(x => x.ID == id);
            return ConvertToMCashPurchase(data);
        }

        public MConsumption GetConsumptionByID(int id)
        {
            var data = db.Consumptions.Single(x => x.ID == id);
            return ConvertToMConsumption(data);
        }

        #region Convertion Methods...

        #region GRN

        public MGRN ConvertToMGRN(GRN data, bool HeaderOnly = false)
        {
            MGRN mrec = new MGRN()
            {
                ReferenceNo = data.ReferenceNo,
                ID = data.ID,
                SiteID = data.BUID,
                VendorID = data.VendorID,
                ChallanNo = data.ChallanNo,
                CreatedBy = data.CreatedBy,
                CreatedOn = data.CreatedOn,
                InvoiceNo = data.InvoiceNo,
                LastUpdatedOn = data.LastUpdatedOn,
                LastUpdatedBy = data.LastUpdatedBy,
                ForDate = data.ForDate,
                Requester = data.Requester,
                Remarks = data.Remarks,
                VehicleDetails = data.VehicleDetails
            };
            if (data.Vendor != null) mrec.Vendor = data.Vendor.DisplayVendor;
            if (data.BUs != null) mrec.Site = data.BUs.DisplaySite;
            if (HeaderOnly) return mrec;

            mrec.GRNLines = new ObservableCollection<MGRNLine>();
            data.GRNLines.Select(x => ConvertToMGRNLines(x)).ToList()
                .ForEach(x => mrec.GRNLines.Add(x));
            return mrec;
        }

        public MGRNLine ConvertToMGRNLines(GRNLine x)
        {
            MGRNLine mrline = new MGRNLine()
            {
                LineID = x.ID,
                ItemID = x.ItemID,
                SizeID = x.SizeID.Value,
                BrandID = x.BrandID.Value,
                POID = x.POID,
                PRID = x.PRID,
                PRLineID = x.PRLineID,
                HeaderID = x.HeaderID,
                Item = x.Item.DisplayName,
                LineRemarks = x.Remarks,
                Quantity = x.Qty,
                TruckNo = x.TruckNo,
                LineChallanNo = x.ChallanNo,
                DiscountType = x.DiscountType,
                Discount = x.Discount,
                TotalLinePrice = x.TotalLinePrice,
                UnitPrice = x.UnitPrice,
                LinePrice = x.LinePrice
            };
            if (x.ItemBrand != null)
                mrline.Brand = x.ItemBrand.Value;
            if (x.ItemSize != null)
                mrline.Size = x.ItemSize.Value;
            if (x.PRDetail != null)
            {
                mrline.OrderedQty = x.PRDetail.Qty;
                mrline.EarlierReceivedQty = x.PRDetail.TotalReceivedQty.Value;
            }
            if (x.TruckMeasurement != null)
            {
                if (mrline.TruckParameters == null) mrline.TruckParameters = new MTruckParameter();
                mrline.TruckParameters.ID = x.TruckMeasurement.ID;
                mrline.TruckParameters.Length = x.TruckMeasurement.Lth;
                mrline.TruckParameters.Breadth = x.TruckMeasurement.Bth;
                mrline.TruckParameters.Height = x.TruckMeasurement.Ht;
            }
            return mrline;
        }

        #endregion

        #region Cash Purchase

        public MCashPurchase ConvertToMCashPurchase(CashPurchas data)
        {
            MCashPurchase mrec = new MCashPurchase()
            {
                ReferenceNo = data.ReferenceNo,
                ID = data.ID,
                SiteID = data.BUID,
                VendorID = data.VendorID,
                ChallanNo = data.ChallanNo,
                CreatedBy = data.CreatedBy,
                CreatedOn = data.CreatedOn,
                InvoiceNo = data.InvoiceNo,
                LastUpdatedOn = data.LastUpdatedOn,
                LastUpdatedBy = data.LastUpdatedBy,
                ForDate = data.ForDate,
                Requester = data.Requester,
                Remarks = data.Remarks,
                GrossTotalofLines = data.GrossAmount,
                TAX = data.TAX,
                Freight = data.Freight,
                TotalLinePrice = data.TotalAmount,
            };
            if (data.Vendor != null) mrec.Vendor = data.Vendor.DisplayVendor;
            if (data.BUs != null) mrec.Site = data.BUs.DisplaySite;

            mrec.CashPurchaseLines = new ObservableCollection<MCashPurchaseLine>();
            data.CashPurchaseLines.Select(x => ConvertToMCashPurchaseLines(x)).ToList()
                .ForEach(x => mrec.CashPurchaseLines.Add(x));
            return mrec;
        }

        public MCashPurchaseLine ConvertToMCashPurchaseLines(CashPurchaseLine x)
        {
            MCashPurchaseLine line = new MCashPurchaseLine()
            {
                LineID = x.ID,
                ItemID = x.ItemID,
                SizeID = x.SizeID.Value,
                BrandID = x.BrandID.Value,
                HeaderID = x.HeaderID,
                Item = x.Item.DisplayName,
                LineRemarks = x.Remarks,
                Quantity = x.Qty,
                TruckNo = x.TruckNo,
                LineChallanNo = x.ChallanNo,
                UnitPrice = x.UnitPrice,
                LinePrice = x.LinePrice
            };
            if (x.ItemBrand != null)
                line.Brand = x.ItemBrand.Value;
            if (x.ItemSize != null)
                line.Size = x.ItemSize.Value;
            if (x.TruckMeasurement != null)
            {
                if (line.TruckParameters == null) line.TruckParameters = new MTruckParameter();
                line.TruckParameters.ID = x.TruckMeasurement.ID;
                line.TruckParameters.Length = x.TruckMeasurement.Lth;
                line.TruckParameters.Breadth = x.TruckMeasurement.Bth;
                line.TruckParameters.Height = x.TruckMeasurement.Ht;
            }
            return line;
        }

        #endregion

        #region Consumption

        public MConsumption ConvertToMConsumption(Consumption data)
        {
           MConsumption mrec = new MConsumption()
            {
                ReferenceNo = data.ReferenceNo,
                ID = data.ID,
                SiteID = data.BUID,
                CreatedBy = data.CreatedBy,
                CreatedOn = data.CreatedOn,
                LastUpdatedOn = data.LastUpdatedOn,
                LastUpdatedBy = data.LastUpdatedBy,
                ForDate = data.ForDate,
                ConsumedBy = data.ConsumedBy,
                Remarks = data.Remarks
            };
           if (data.BUs != null) mrec.Site = data.BUs.DisplaySite;

            mrec.ConsumptionLines = new ObservableCollection<MConsumptionLine>();
            data.ConsumptionLines.Select(x => ConvertToMConsumptionLines(x)).ToList()
                .ForEach(x => mrec.ConsumptionLines.Add(x));
            return mrec;
        }

        public MConsumptionLine ConvertToMConsumptionLines(ConsumptionLine x)
        {
            MConsumptionLine line = new MConsumptionLine()
            {
                LineID = x.ID,
                ItemID = x.ItemID,
                SizeID = x.SizeID.Value,
                BrandID = x.BrandID.Value,
                HeaderID = x.HeaderID,
                Item = x.Item.DisplayName,
                LineRemarks = x.Remarks,
                Quantity = x.Qty
            };
            if (x.ItemBrand != null)
                line.Brand = x.ItemBrand.Value;
            if (x.ItemSize != null)
                line.Size = x.ItemSize.Value;
            return line;
        }

        #endregion

        #endregion
    }
}
