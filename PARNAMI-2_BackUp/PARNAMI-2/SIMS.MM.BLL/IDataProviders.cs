﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SIMS.MM.MODEL;
using System.ServiceModel;
using System.Collections.ObjectModel;
using SIMS.MM.MODEL;
using SIMS.Core;

namespace SIMS.MM.BLL
{
    [ServiceContract]
    public interface IMasterDataProvider
    {
        [OperationContract]
        ObservableCollection<MAlerts> GetAlerts(string currentuser);


        [OperationContract]
        IQueryable<MVendor> GetVendorHeaders();
        [OperationContract]
        MVendor GetVendorByID(int _VendorID);
        [OperationContract]
        IQueryable<MReceivingDetailsforMasters> GetAllReceivingsforMasters(int? vendorID, int? siteID);



        [OperationContract]
        IQueryable<MAccounts> GetAllAccounts();
        [OperationContract]
        IEnumerable<MAccounts> GetAccountsbyForID(int ForID, string For);
        [OperationContract]
        MAccounts GetAccountsbyID(int _AcountsID);



        [OperationContract]
        MAddress GetAddressByID(int _AddressID);
        [OperationContract]
        IEnumerable<MAddress> GetAddressbyForID(int ForID, string For);
        [OperationContract]
        IQueryable<MAddress> GetAllAddresses();



        [OperationContract]
        IEnumerable<MDocument> GetDocumentbyForID(int ForID, string For);
        [OperationContract]
        IQueryable<MDocument> GetAllDocuments();
        [OperationContract]
        MDocument GetDocumentbyID(int ID);



        [OperationContract]
        IEnumerable<MContact> GetContactbyForID(int ForID, string For);
        [OperationContract]
        IQueryable<MContact> GetAllContacts();
        [OperationContract]
        MContact GetContactbyID(int ID);



        [OperationContract]
        IQueryable<MSite> GetSiteHeaders();
        [OperationContract]
        MSite GetSitebyID(int ID);
        [OperationContract]
        IQueryable<MSafetyStock> GetSafetyStockbySiteID(int? siteid);
        [OperationContract]
        IQueryable<MClient> GetAllClients();


        [OperationContract]
        IQueryable<MItem> GetItemHeaders();
        [OperationContract]
        IQueryable<MItemCategory> GetItemCategories();
        [OperationContract]
        IQueryable<MItem> GetItemsByCategoryID(int CategoryID);
        [OperationContract]
        IQueryable<string> GetItemName();
        [OperationContract]
        IQueryable<MMeasurementUnit> GetAllMUs();
        [OperationContract]
        IQueryable<string> GetAllMUTypes();
        [OperationContract]
        IQueryable<MItemSize> GetAllItemSizes();
        [OperationContract]
        IQueryable<MItemBrand> GetAllItemBrands();
        [OperationContract]
        IQueryable<MCompany> GetAllCompanies();
        [OperationContract]
        MCompany GetCompanyByID(int ID);
        [OperationContract]
        MItem GetItembyID(int ID);
        [OperationContract]
        IEnumerable<MStockReport> GetStockSummary(DateTime? startdate, DateTime? enddate, int? siteid, int? ItemID, int? sizeid, int? brandid);
        [OperationContract]
        IEnumerable<MItemCategory> GetItemParents(int itemID);
        [OperationContract]
        MItemCategory GetItemCategoryByID(int categoryid);
        [OperationContract]
        IQueryable<MItemCategory> GetItemCategoryLeaves(int CategoryID);   //CanHaveItem property must be rue For Given CategoryID
        [OperationContract]
        IQueryable<MItem> GetVariationsforLeaf(int CategoryID);    //IsItem Property must be true For Given CategoryID 


        [OperationContract]
        IQueryable<MTransporter> GetAllTransporters();


        [OperationContract]
        IQueryable<MPriceMaster> GetAllMasterPrice(string key, int? vendorid, int? siteid, int? itemid);


        [OperationContract]
        IQueryable<MPriceMaster> GetAllPricesByISV(int vendorid, int? siteid, int itemid);
    }

    [ServiceContract]
    public interface IPurchasingDataProvider
    {
        //[OperationContract]
        //IQueryable<MPOHeader> GetPOHeaders(int? pageindex, int? pagesize, string key, int? vendorid, int? siteid);

        //[OperationContract]
        //IQueryable<MPRHeader> GetPRHeaders(int? pageindex, int? pagesize, string key, int? vendorid, int? siteid);

        //[OperationContract]
        //IPaginated<MPOHeader> GetPOHeaders(string username, int? pageindex, int? pagesize, string key, int? vendorid, int? siteid, bool showall = true);

        [OperationContract]
        IPaginated<MPOHeader> GetPOHeadersNew(string username, int? pageindex, int? pagesize, string key, int? vendorid, int? siteid, bool showall = true);

        //[OperationContract]
        //IPaginated<MPRHeader> GetPRHeaders(string username, int? pageindex, int? pagesize, string key, int? vendorid, int? siteid, bool showall = true);

        [OperationContract]
        IPaginated<MPRHeader> GetPRHeadersNew(string username, int? pageindex, int? pagesize, string key, int? vendorid, int? siteid, string status, bool showall = true);//using Table-Valued Funtion

        [OperationContract]
        MPO GetPObyId(int poid);

        [OperationContract]
        MPR GetPRbyId(int prid);
    }

    [ServiceContract]
    public interface IReceivingDataProvider
    {
        //[OperationContract]
        //IQueryable<MReceivingHeader> GetCashPurachaseHeaders(int? pageindex,int? pagesize,string key,int? vendorid,int? siteid);
        //[OperationContract]
        //IQueryable<MReceivingHeader> GetDirectPurchaseHeaders(int? pageindex, int? pagesize, string key, int? vendorid, int? siteid);
        //[OperationContract]
        //IQueryable<MReceivingHeader> GetGRNHeaders(int? pageindex, int? pagesize, string key, int? vendorid, int? siteid);
        //[OperationContract]
        //IQueryable<MReceivingHeader> GetConsumptionHeaders(int? pageindex, int? pagesize, string key, int? siteid);
        [OperationContract]
        IPaginated<MReceivingHeader> GetConsumptionHeadersNew(string username, int? pageindex, int? pagesize, string key, int? siteid, bool showAll = true);
        //[OperationContract]
        //IPaginated<MReceivingHeader> GetCashPurachaseHeaders(string username, int? pageindex, int? pagesize, string key, int? vendorid, int? siteid, bool showAll=true);
        [OperationContract]
        IPaginated<MReceivingHeader> GetCashPurachaseHeadersNew(string username, int? pageindex, int? pagesize, string key, int? vendorid, int? siteid, bool showAll = true);
        [OperationContract]
        IPaginated<MReceivingHeader> GetDirectPurchaseHeaders(string username, int? pageindex, int? pagesize, string key, int? vendorid, int? siteid, bool showAll = true);
        //[OperationContract]
        //IPaginated<MReceivingHeader> GetAllDirectGRNs(string username, int? pageindex, int? pagesize, string key, int? vendorid, int? siteid, string Type, bool showAll = true);
        [OperationContract]
        IPaginated<MReceivingHeader> GetAllDirectGRNsNew(string username, int? pageindex, int? pagesize, string key, int? vendorid, int? siteid, string Type, bool showAll = true);
        //[OperationContract]
        //IPaginated<MReceivingHeader> GetGRNHeaders(string username, int? pageindex, int? pagesize, string key, int? vendorid, int? siteid, bool showAll = true);
        [OperationContract]
        IPaginated<MReceivingHeader> GetGRNHeadersNew(string username, int? pageindex, int? pagesize, string key, int? vendorid, int? siteid, bool showAll = true);
        //[OperationContract]
        //IPaginated<MReceivingHeader> GetConsumptionHeaders(string username, int? pageindex, int? pagesize, string key,int? siteid, bool showAll = true);

        [OperationContract]
        MReceiving GetCPbyId(int id);
        [OperationContract]
        MReceiving GetDPbyId(int id);
        [OperationContract]
        MReceiving GetGRNbyId(int id);
        [OperationContract]
        MReceiving GetConsbyId(int id);
    }

    [ServiceContract]
    public interface IPurchasingManger
    {
        MPR CreatePR(MPR NewPR);
        MPR UpdatePR(MPR UpdatedPR);
        void DeletePR(int prid);
        void EmailPR(int PRID);
        string ConfirmEmail(int POID);
        void EmailtoVendor(int POID);
        void ApprovePR(int PRID, string ApprovedBy, DateTime? ApprovedOn);
        void VerifyPR(int PRID, string VerifiedBy, DateTime? VerifiedOn);
        void ChangesStateofPR(int PRID, string State);
        void InitialApprovePR(int PRID, string ReqApprovedBy, DateTime? ReqApprovedOn);
        void AcknowledgePO(int poid, string remark);
        int ConvertToPO(int PRID);
        MPR SplitPR(MPR NewPR, int OldPRID, ObservableCollection<int> OldPRLines);
        MPR SplitPRwithPartialQty(int OldPRID, ObservableCollection<MSplittedLines> NewPRLines);
        IEnumerable<int> GetAllowedItemId(int vendorid,int siteid);
        IEnumerable<PendingPOInfo> GetPendingInfo(int vendorid, int siteid, int itemid, int? sizeid, int? brandid);
    }
    
    public interface IReceivingManager
    {
        MReceiving CreateNewReceiving(MReceiving newRcving, string username);
        MReceiving CreateNewCashPurchase(MReceiving newCP, string username);
        MReceiving CreateNewDirectPurchase(MReceiving newDP, string username);
        MReceiving CreateNewConsumption(MReceiving newDP, string username);
        MReceiving SaveAsDraft(int id);

        MReceiving ConvertfromPO(int poid); //For converting PO to Receivings

        MReceiving CreateNewRTV(MReceiving _newRTV, string username);
        MReceiving CreateNewSA(MReceiving _newSA, string username);//Stock Adjustment
    }
    
    public interface IMasterManager
    {             
        MVendor CreateNewVendor(MVendor newVendor);      
        MVendor UpdateVendor(MVendor updatedVendor);
        void DeleteVendor(int vendorId);
        //madhur

        MSite CreateNewSite(MSite newSite);
        MClient CreateNewClient(MClient newClient);
        MClient UpdateClient(MClient updatedClient);
        MSite UpdateSite(MSite updatedSite);
        void DeleteSite(int siteId);

        MItemSize CreateNewItemSize(MItemSize NewSize);
        MItemBrand CreateNewItemBrand(MItemBrand NewBrand);
        MCompany CreateNewCompany(MCompany NewCompany);
        MItemSize UpdateItemSize(MItemSize updatedSize);
        MItemBrand UpdateItemBrand(MItemBrand updatedBrand);
        MCompany UpdateCompany(MCompany company);
        void DeleteCompany(int ID);
        MItem CreateNewItem(MItem newItem, string categoryName);
        MItem CreateNewItem(MItem newItem);
        MItem UpdateItem(MItem updatedItem);
        void DeleteItem(int itemId);        
        //MItemType AddNewItemType(MItemType);
        void CreateNewMU(MMeasurementUnit value);


        MPriceMaster CreateNewPricing(MPriceMaster _mprice);
        void CreateNewPricing(IEnumerable<MPriceMaster> _mprice);
        string NewPricing(MPriceMaster _mprice);
        void UpdatePricing(MPriceMaster _mprice);

        void UpdateParentForItemCategory(int ID, int NewPID);
        MItemCategory CreateNewItemCategory(MItemCategory _newItemCategory);
        MItemCategory UpdateItemCategory(MItemCategory _updatedItemCategory);

        MTransporter CreateNewTransporter(MTransporter _NewTransporter);
        MTransporter UpdateTransporter(MTransporter _UpdatedVendor);
        void DeleteTransporter(int _TransporterID);

        MAddress CreateNewAddress(MAddress _NewAddress);
        MAddress UpdateAddress(MAddress _updatedAddress);
        void DeleteAddress(int _AddressID);


        MDocument SaveDocument(MDocument _NewDocument);
        MDocument SaveDocument(string FileName, string For, int? ForID, string DocType);
        void DeleteDocument(int DocumentID);
        void SaveImagePath(string ImageName, int ItemID);


        MContact CreateNewContact(MContact _NewContact);
        MContact UpdateContact(MContact _updatedContact);
        void DeleteContact(int _ContactID);


        MAccounts CreateNewAccount(MAccounts _NewAccount);
        MAccounts UpdateAccount(MAccounts _updatedAccount);
        void DeleteAccount(int _AccountID);
    }

    public interface IMTRDataProvider
    {
        //[OperationContract]
        //IQueryable<MMTRHeader>GetMTRHeaders(int? pageindex, int? pagesize, string Key, int? vendorid, int? sideid, int? sideid2);

        [OperationContract]
        IPaginated<MMTRHeader> GetMTRHeaders(int? pageindex, int? pagesize, string Key, int? vendorid, int? sideid, int? sideid2);

        [OperationContract]
        MMTR GetMTRbyId(int mtrid);
        
    }

    public interface IMTRManager
    {
        [OperationContract]
        MMTR CreateNewMTR(MMTR newMTR);
       //void DeleteMTR(int MTRId);
        void ApproveMTR(int MTRId);
        void ReleaseMTR(int MTRId);
        void ReceiveMTR(int MTRId);
    }
   
    [ServiceContract]
    public interface IMODataProvider
    {
        //[OperationContract]
        //IPaginated<MMOHeader> GetMOHeaders(string username,int? pageindex, int? pagesize, string Key, int? vendorid, int? sideid, int? sideid2,bool showall=true);

        [OperationContract]
        IPaginated<MMOHeader> GetMOHeadersNew(string username, int? pageindex, int? pagesize, string Key, int? transiteid, int? reqsiteid, string ConType, bool? IsClosed, bool showall = true);

        [OperationContract]
        MMO GetMObyId(int mtrid);

        [OperationContract]
        MReceiving GetMReceivingforMoRelease(int moid);

        [OperationContract]
        MReceiving GetMReceivingforMoReceiving(int moid);

        [OperationContract]
        bool IsSimilarMOsAvailable(int moid);

        /*****************************************************************************/
        /****** New******/
        [OperationContract]
        MMOReleasing GetMOReleaseTransactionByMOID(int moid);

        [OperationContract]
        MMOReceiving GetMOReceiveTransactionByMOID(int moid);

        [OperationContract]
        IQueryable<MMOReleasing> GetMOReleasingsAgainstMO(int moid);
        
        [OperationContract]
        IQueryable<MMOReceiving> GetMOReceivingsAgainstMO(int moid);
        /*****************************************************************************/
    }

    [ServiceContract]
    public interface IMOManager
    {
        [OperationContract]
        MMO CreateNewMO(MMO newMO);       

        //[OperationContract]
        //void ReleaseMO(int moid);

        [OperationContract]
        void ReceivedMO(int moid);

        [OperationContract]
        void CloseMO(int moid);

        [OperationContract]
        string CreateMORelease(MReceiving mrec);

        [OperationContract]
        int CreateMOReceive(MReceiving mrec);

        [OperationContract]
        MMO SplitToMO(MMO NewMO, int OldPRID, ObservableCollection<int> OldPRLines);

        //[OperationContract]
        //MMO SplitToMOwithPartialQty(MMO NewMO, int OldPRID, ObservableCollection<MSplittedLines> SplittedLines);

        [OperationContract]
        MMO SplitToMOwithPartialQty(MMO NewMO, int OldPRID, ObservableCollection<MMOSplittedLine> MOSplittedLines);

        [OperationContract]
        MMO CombineMO(int moid);

        /*****************************************************************************/
        /****** New******/
        [OperationContract]
        string MORelease(MMOReleasing mrec, string username);

        [OperationContract]
        int MOReceive(MMOReceiving mrec, string username);
        /*****************************************************************************/
    }

    [ServiceContract]
    public interface IMRManager
    {
        [OperationContract]
        MMR CreateNewMR(MMR _NewMR);

        [OperationContract]
        int CreateMRIssue(MReceiving _mrec);

        [OperationContract]
        int CreateMRReturn(MReceiving _mrec);

        [OperationContract]
        void CloseMR(int mrid);

        /*****************************************************************************/
        /****** New******/
        [OperationContract]
        int MRIssue(MMaterialIssue mrec, string username);

        [OperationContract]
        int MRReturn(MMaterialReturn mrec, string username);
        /*****************************************************************************/
    }

    [ServiceContract]
    public interface IMRDataProvider
    {
        //[OperationContract]
        //IPaginated<MMRHeader> GetMRHeaders(string username, int? pageindex, int? pagesize, string Key, int? sideid, bool showall = true);

        [OperationContract]
        IPaginated<MMRHeader> GetMRHeadersNew(string username, int? pageindex, int? pagesize, string Key, int? sideid, bool showall = true);

        [OperationContract]
        MMR GetMRByID(int MRID);

        [OperationContract]
        MReceiving GetMReceivingforMRIssue(int mrid);

        [OperationContract]
        MReceiving GetMReceivingforMRReturn(int mrid);

        /*****************************************************************************/
        /****** New******/
        [OperationContract]
        MMaterialIssue GetMRIssueTransactionByMRID(int mrid);

        [OperationContract]
        MMaterialReturn GetMRReturnTransactionByMRID(int mrid);
        /*****************************************************************************/
    }

    //[ServiceContract]
    //public interface ITenderDataProvider
    //{
    //    [OperationContract]
    //    IPaginated<MTender> GetTenderHeader(string username, int? pageindex, int? pagesize, string Key, int? sideid, bool showall = true);

    //    [OperationContract]
    //    MTender GetTenderByID(int TenderID);
    //}

    //[ServiceContract]
    //public interface ITenderManager
    //{
    //    [OperationContract]
    //    MTender CreateNewTender(MTender _NewTender);

    //    [OperationContract]
    //    MTender UpdateTender(MTender _UpdatedTender);
    //}

    [ServiceContract]
    public interface IBillingDataProvider
    {
        [OperationContract]
        IPaginated<MReceiving> GetReceivings(string username, int? pageindex, int? pagesize, string key, int? vendorid, int? siteid, bool showall = true);

        [OperationContract]
        MReceiving GetReceivingforBillByID(int ID, string Type);

        [OperationContract]
        MBilling GetBillbyId(int billid);

        //[OperationContract]
        //IPaginated<MBilling> GetAllBills(string username, int? pageindex, int? pagesize, string key, int? vendorid, int? siteid, string Filter);

        [OperationContract]
        IPaginated<MBilling> GetAllBillsNew(string username, int? pageindex, int? pagesize, string key, int? vendorid, int? siteid, string Filter);

        [OperationContract]
        IPaginated<MReceivingLine> GetReceivingLines(string username, int? pageindex, int? pagesize, string key, int? itemid, int? vendorid, int? siteid, DateTime? sdate, DateTime? edate, string challan);

        /*****************************************************************************/
        /****** New******/

        [OperationContract]
        IPaginated<MGRNLine> GetGRNLines(string username, int? pageindex, int? pagesize, string key, int? itemid, int? vendorid, int? siteid, DateTime? sdate, DateTime? edate, string challan);

        /*****************************************************************************/
    }

    [ServiceContract]
    public interface IBillingManager
    {
        [OperationContract]
        MBilling CreateNewBill(MBilling _NewBill);

        [OperationContract]
        MBilling UpdateBill(MBilling _Bill);

        [OperationContract]
        void VerifyBill(int BillID, string VerifiedBy);
    }

    [ServiceContract]
    public interface IMessagingDataProvider
    {
        [OperationContract]
        IQueryable<MEmail> GetAllEmails();

        [OperationContract]
        IPaginated<MMessaging> GetAllMessages(string username, int? pageindex, int? pagesize, string key);

        [OperationContract]
        IPaginated<MMessagingHeader> GetAllMessageHeaders(string username, int? pageindex, int? pagesize, string key);

        [OperationContract]
        MMessaging GetMessageByID(int id);
    }

    [ServiceContract]
    public interface IMessagingManager
    {
        [OperationContract]
        MMessaging SendNewMail(MMessaging NewEmail);

        [OperationContract]
        bool SetIsStar(int msgid, string user, bool IsStar);

        [OperationContract]
        bool SetIsFlag(int msgid, string user, bool IsFlag);

        [OperationContract]
        bool SetIsRead(int msgid, string user, bool IsRead);

        [OperationContract]
        void DeleteMail(int Msgid, string user);
    }


    /*****************************************************************************/
    /*New Interfaces after convertion in Double Entry System*/
    /*By: Madhur*/

    [ServiceContract]
    public interface ITransactionDataProvider
    {
        [OperationContract]
        IPaginated<MGRN> GetAllGRNs(string username, int? pageindex, int? pagesize, string key, int? vendorid, int? siteid, bool showAll = true);

        [OperationContract]
        IPaginated<MCashPurchase> GetAllCashPurchases(string username, int? pageindex, int? pagesize, string key, int? vendorid, int? siteid, bool showAll = true);

        [OperationContract]
        IPaginated<MConsumption> GetAllConsumptions(string username, int? pageindex, int? pagesize, string key, int? siteid, bool showAll = true);

        [OperationContract]
        MGRN GetGRNByID(int id);

        [OperationContract]
        MCashPurchase GetCashPurchaseByID(int id);

        [OperationContract]
        MConsumption GetConsumptionByID(int id);
    }

    [ServiceContract]
    public interface ITransactionManager
    {
        [OperationContract]
        MGRN CreateNewGRN(MGRN NewGRN, string username);

        [OperationContract]
        MCashPurchase CreateNewCashPurchase(MCashPurchase newCP, string username);

        [OperationContract]
        MConsumption CreateNewConsumption(MConsumption newCon, string username);

        [OperationContract]
        MGRN ConvertfromPO(int poid);
    }

    /*****************************************************************************/
}