﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace SIMS.MM.BLL.Emailer
{
    using DAL;
    public class Emailer
    {
        public static string from = "dkbuildcon@gmail.com";

        public static void SMSPO(string mobile, PR pr, bool isForApproval)
        {
            string EmailTxtFormat = "" +
@"
<header>
PO/PR: <poid>
Site: <site>
Vendor: <vendor>
Amount: <amount>
Raised by: <raisedby> on <raisedon>

Line Details:
<lines>

Remarks: <remarks>
";
            // Line Format
            string LineFormat =
                "Line: {0}\nItem: {1}\nQty: {2}\nUnit Price: {3}\nLine Price: {4}\nDelivery Date: {5}\nRemarks: {6}" + "\n\n";
            string LineHeader = String.Format(LineFormat,
                "Line", "Item", "Qty", "Unit Price", "LinePrice", "Delivery Date", "Remarks");
            string Lines = "";
            foreach (var line in pr.PRDetails)
            {
                Lines += String.Format(LineFormat,
                line.ID, String.Format("{0} ({1})", line.Item.ItemCategory.Name, line.Item.Name), line.Qty + " " + line.Item.MeasurementID, line.UnitPrice, line.LinePrice, line.DeliveryDate.Value.ToShortDateString(), line.Remarks);
            }

            string headerTxt = "";

            if (isForApproval)
            {
                headerTxt = "PR For Approval";
            }
            else
            {
                headerTxt = "PO Raised as";
            }

            Replacements r1 = new Replacements();
            r1.AddNew(new object[]{
                "poid", isForApproval?pr.ID:pr.POID,
                "vendor",pr.Vendor.Name,
                "site",pr.BUs.Name + " " +pr.BUs.Address + " "+pr.BUs.City,
                "amount",pr.TotalPrice,
                "raisedby",pr.RaisedBy,
                "raisedon",pr.RaisedOn,
                //"lineheader",LineHeader,
                "lines",Lines,
                "header",headerTxt,
                "remarks",pr.Remarks
            });

            string EmailTxt = r1.Execute(EmailTxtFormat);
            //SendSMS(Web.Services.AppSettings.Current.ClientMobile, EmailTxt);
        }

        public static void SendSMS(string mobile, string message)
        {
            //SMSModule.MVayooSMS sms = new SMSModule.MVayooSMS("INNOSOLS");
            //sms.sendSMS(mobile, message, 0, 0);
        }

        public static void EmailPO(string mailid, PR pr, bool isForApproval)
        {
            string EmailTxtFormat = "" +
@"
<header>
__________
PO/PR: <poid>
Site: <site>
Vendor: <vendor>
Amount: <amount>
Raised by: <raisedby> on <raisedon>

Line Details:
<lines>

PO/PR Remarks:
<remarks>

Terms and Conditions:
<terms>

<sendor>
";
            // Line Format
            string LineFormat =
                "Line: {0}\nItem: {1}\nQty: {2}\nUnit Price: {3}\nLine Price: {4}\nDelivery Date: {5}\nRemarks: {6}" + "\n\n";
            string LineHeader = String.Format(LineFormat,
                "Line", "Item", "Qty", "Unit Price", "LinePrice", "Delivery Date", "Remarks");
            string Lines = "";
            foreach (var line in pr.PRDetails)
            {
                Lines += String.Format(LineFormat,
                line.ID, String.Format("{0} ({1})", line.Item.ItemCategory.Name, line.Item.Name), line.Qty + " " + line.Item.MeasurementID, line.UnitPrice, line.LinePrice, line.DeliveryDate.Value.ToShortDateString(), line.Remarks);
            }

            string headerTxt = "";

            if (isForApproval)
            {
                headerTxt = "Dear Sir\nKindly Review and approve the PO";
            }
            else
            {
                headerTxt = "Dear Sir\nThis is for your kind information";
            }

            string terms = pr.PaymentTerms;

            Replacements r1 = new Replacements();
            r1.AddNew(new object[]{
                "poid", isForApproval?pr.ID:pr.POID,
                "vendor",pr.Vendor.Name,
                "site",pr.BUs.Name + " " +pr.BUs.Address + " "+pr.BUs.City,
                "amount",pr.TotalPrice,
                "raisedby",pr.RaisedBy,
                "raisedon",pr.RaisedOn,
                "lineheader",LineHeader,
                "lines",Lines,
                "header",headerTxt,
                "remarks",pr.Remarks,
                "terms",terms
               // "sendor",Web.Services.AppSettings.Current.Name
            });

            string EmailTxt = r1.Execute(EmailTxtFormat);
            var title = "PR/PO";
            if (pr.POs == null) title = pr.ReferenceNo;
            else title = pr.POs.Display;
            EmailSendor.SendTextEmail(mailid, from, title, EmailTxt);
        }
    }

    public class Replacement
    {
        public string oldvalue { get; set; }
        public object newvalue { get; set; }
        public Replacement(string _old, object _new)
        {
            oldvalue = _old;
            newvalue = _new;
        }
    }

    public class Replacements : List<Replacement>
    {
        public void AddNew(string _old, object _new)
        {
            this.Add(new Replacement(_old, _new));
        }

        public void AddNew(object[] list)
        {
            if (list.Count() % 2 != 0)
                return;
            for (int i = 0; i < list.Count(); i = i + 2)
            {
                this.AddNew(list[i].ToString(), list[i + 1]);
            }
        }

        public string Execute(string text)
        {
            foreach (var item in this)
            {
                string val = "";
                if (item.newvalue != null)
                    val = item.newvalue.ToString();
                text = text.Replace("<" + item.oldvalue + ">", val);
            }
            return text;
        }
    }
}
