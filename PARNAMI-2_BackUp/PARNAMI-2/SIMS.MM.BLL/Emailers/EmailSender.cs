﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Net.Mail;
using System.Net;

namespace SIMS.MM.BLL.Emailer
{
    public class EmailSendor
    {
        public static MailMessage SendTextEmail(string Tos, string From, string Subject, string Text)
        {
            return SendTextEmail(Tos, From, Subject, Text, null, false);
        }

        public static MailMessage SendTextEmail(string Tos, string From, string Subject, string Text, string[] Attachments, bool IsAsync)
        {
            var settings = Services.AppSettingsService.GetLatestAppSettings();
            MailMessage msg = new MailMessage();
            msg.To.Add(Tos);
            msg.From = new MailAddress(settings.mailfrom);
            msg.Subject = Subject;
            msg.Body = Text;
            if (Attachments != null)
            {
                foreach (var file in Attachments)
                {
                    msg.Attachments.Add(new Attachment(file));
                }
            }
            SmtpClient client = new SmtpClient(settings.mailserver, 25);
            NetworkCredential credential = new NetworkCredential(settings.mailserverusername, settings.mailserverpassword);
            client.Credentials = credential;
            if (IsAsync)
                client.SendAsync(msg, null);
            else
                client.Send(msg);
            return msg;
        }
    }
}
