﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SIMS.MM.BLL
{
    using DAL;
    using MODEL;
    using System.Collections.ObjectModel;
    public class MOManager : IMOManager
    {
        parnamidb1Entities db = new parnamidb1Entities();
        MMReportsDBDataContext dc = new MMReportsDBDataContext();
        MODataProvider provider = new MODataProvider();
        ReceivingDataProvider receivingDP = new ReceivingDataProvider();

        public MMO CreateNewMO(MMO newMO)
        {
            var qry = ConvertTOMO(newMO);
            qry.ID = db.GetMAX_MTR_ID() + 1;
            qry.CurrentOwnerID = "dba";
            qry.Year = DateTime.UtcNow.AddHours(5.5).Year;
            qry.SiteID = db.GetSiteCode(qry.RequestedSiteID);//???
            qry.RID = db.GetMaxMORID(qry.Year, qry.RequestedSiteID, qry.ConsumableType) + 1;
            if (qry.ConsumableType == "Consumable")
                qry.ReferenceNo = "TO/" + qry.Year + "/S-" + qry.SiteID + "/C-" + qry.RID;
            if (qry.ConsumableType == "Returnable")
                qry.ReferenceNo = "TO/" + qry.Year + "/S-" + qry.SiteID + "/R-" + qry.RID;
            db.MTRs.AddObject(qry);
            db.SaveChanges();
            return provider.GetMObyId(qry.ID);
        }

        public MMO SplitToMO(MMO NewMO, int OldPRID, ObservableCollection<int> OldPRLines)
        {
            var qry = ConvertTOMO(NewMO);
            NewMO.UpdateLineNumbers();
            qry.ID = db.GetMAX_MTR_ID() + 1;
            qry.CurrentOwnerID = "dba";
            qry.Year = DateTime.UtcNow.AddHours(5.5).Year;
            qry.SiteID = db.GetSiteCode(qry.RequestedSiteID);//???
            //qry.RID = db.GetMaxRRID("Consumption", qry.Year, qry.BUID) + 1;
            qry.ReferenceNo = "TO/" + qry.Year + "/S-" + qry.SiteID + "/" + qry.ID;
            db.MTRs.AddObject(qry);
            db.SaveChanges();

            foreach (var LineID in OldPRLines)
            {
                var data = db.PRDetails.Single(x => x.PRID == OldPRID && x.ID == LineID);
                if (data != null)
                {
                    db.PRDetails.DeleteObject(data);
                    db.SaveChanges();
                }
            }

            return provider.GetMObyId(qry.ID);
        }

        //public MMO SplitToMOwithPartialQty(MMO NewMO, int OldPRID, ObservableCollection<MSplittedLines> SplittedLines)
        //{
        //    NewMO.MOLines = new ObservableCollection<MMOLine>();
        //    var oldpr = db.PRs.Single(x => x.ID == OldPRID);
        //    MTR newmtr = new MTR();
        //    newmtr = ConvertTOMO(NewMO);

        //    var FullSplittedLines = SplittedLines.Where(x => !x.IsPartial).ToList();
        //    var PartialSplittedLines = SplittedLines.Where(x => x.IsPartial && x.ConfirmedQty > 0).ToList();

        //    if (FullSplittedLines.Count > 0)
        //    {
        //        foreach (var line in FullSplittedLines)
        //        {
        //            var oldline = db.PRDetails.Single(x => x.ID == line.LineID && x.PRID == OldPRID);
        //            MTRLine newline = new MTRLine();
        //            newline = ConvertfromPRLineToMOLine(oldline, NewMO.ID);
        //            newmtr.MTRLines.Add(newline);
        //            oldpr.PRDetails.Remove(oldline);
        //        }
        //    }


        //    if (PartialSplittedLines.Count > 0)
        //    {
        //        foreach (var line in PartialSplittedLines)
        //        {
        //            var oldline = db.PRDetails.Single(x => x.ID == line.LineID && x.PRID == OldPRID);
        //            oldline.Qty = line.DiffQty;

        //            MTRLine newline = new MTRLine();
        //            newline = ConvertfromPRLineToMOLine(oldline, NewMO.ID);
        //            newline.RequiredQty = line.ConfirmedQty;
        //            newmtr.MTRLines.Add(newline);
        //        }
        //    }

        //    if (oldpr.PRDetails.Count == 0)
        //    {
        //        // Close OLD Pr
        //        oldpr.IsClosed = true;
        //        oldpr.Status = "Closed";
        //    }
        //    newmtr.RaisedOn = DateTime.UtcNow.AddHours(5.5);
        //    newmtr.ID = db.GetMAX_MTR_ID() + 1;
        //    newmtr.CurrentOwnerID = "dba";
        //    newmtr.SiteID = db.GetSiteCode(newmtr.RequestedSiteID);//???
        //    newmtr.Year = DateTime.UtcNow.AddHours(5.5).Year;
        //    //newmtr.RID = db.GetMaxMORID(newmtr.Year, newmtr.RequestedSiteID) + 1;
        //    //newmtr.ReferenceNo = "TO/" + newmtr.Year + "/S-" + newmtr.SiteID + "/" + newmtr.RID;
        //    newmtr.RID = db.GetMaxMORID(newmtr.Year, newmtr.RequestedSiteID, newmtr.ConsumableType) + 1;
        //    if (newmtr.ConsumableType == "Consumable")
        //        newmtr.ReferenceNo = "TO/" + newmtr.Year + "/S-" + newmtr.SiteID + "/C-" + newmtr.RID;
        //    if (newmtr.ConsumableType == "Returnable")
        //        newmtr.ReferenceNo = "TO/" + newmtr.Year + "/S-" + newmtr.SiteID + "/R-" + newmtr.RID;
        //    int i = 1;
        //    foreach (var item in newmtr.MTRLines)
        //    {
        //        item.LineID = i;
        //        i++;
        //    }
        //    db.MTRs.AddObject(newmtr);
        //    db.SaveChanges();
        //    return provider.GetMObyId(newmtr.ID);
        //}

        public MMO SplitToMOwithPartialQty(MMO NewMO, int OldPRID, ObservableCollection<MMOSplittedLine> MOSplittedLines)
        {
            NewMO.MOLines = new ObservableCollection<MMOLine>();
            var oldpr = db.PRs.Single(x => x.ID == OldPRID);
            MTR newmtr = new MTR();
            newmtr = ConvertTOMO(NewMO);

            var FullSplittedLines = MOSplittedLines.Where(x => !x.IsPartial).ToList();
            var PartialSplittedLines = MOSplittedLines.Where(x => x.IsPartial && x.ConfirmedQty > 0).ToList();

            if (FullSplittedLines.Count > 0)
            {
                foreach (var line in FullSplittedLines)
                {
                    var oldline = db.PRDetails.Single(x => x.ID == line.LineID && x.PRID == OldPRID);
                    MTRLine newline = new MTRLine();
                    newline = ConvertfromPRLineToMOLine(oldline, NewMO.ID);
//                  newline = ConvertFromMMOSplittedLineToMOLine(line);
                    newline.ConsumableType = line.ConsumableType;
                    newmtr.MTRLines.Add(newline);
                    oldpr.PRDetails.Remove(oldline);
                }
            }

            if (PartialSplittedLines.Count > 0)
            {
                foreach (var line in PartialSplittedLines)
                {
                    var oldline = db.PRDetails.Single(x => x.ID == line.LineID && x.PRID == OldPRID);
                    oldline.Qty = line.DiffQty;

                    MTRLine newline = new MTRLine();
                    newline = ConvertfromPRLineToMOLine(oldline, NewMO.ID);
                    newline.RequiredQty = line.ConfirmedQty;
                    newline.ConsumableType = line.ConsumableType;
                    newmtr.MTRLines.Add(newline);
                }
            }

            if (oldpr.PRDetails.Count == 0)
            {
                // *** Close OLD PR ***
                oldpr.IsClosed = true;
                oldpr.Status = "Closed";
            }

            var mtrline = db.MTRs.First().GetMTRLine(NewMO.TransferringSiteID, NewMO.RequestingSiteID, NewMO.ConsumableType);

            if (mtrline != null)
            {
                List<MTRLine> tempList = new List<MTRLine>();
                
                MTR mtr = db.MTRs.Single(x => x.ID == mtrline.MTRID);
                var mtrlines = mtr.MTRLines;
                int count = mtrlines.Count();

                foreach (var line in newmtr.MTRLines)
                {
                    var item = mtrlines.SingleOrDefault(x => x.ItemID == line.ItemID && x.SizeID == line.SizeID && x.BrandID == line.BrandID);
                    if (item != null)
                    {
                        item.RequiredQty = item.RequiredQty + line.RequiredQty;
                    }
                    else
                    {
                        tempList.Add(line);
                    }
                }
               
                foreach (var line in tempList)
                {
                    line.LineID = ++count;
                    mtr.MTRLines.Add(line);
                }

                db.SaveChanges();
                return provider.GetMObyId(mtrline.MTR.ID);
            }
            else
            {
                newmtr.RaisedOn = DateTime.UtcNow.AddHours(5.5);
                newmtr.ID = db.GetMAX_MTR_ID() + 1;
                newmtr.CurrentOwnerID = "dba";
                newmtr.SiteID = db.GetSiteCode(newmtr.RequestedSiteID);//???
                newmtr.Year = DateTime.UtcNow.AddHours(5.5).Year;
                newmtr.RID = db.GetMaxMORID(newmtr.Year, newmtr.RequestedSiteID, newmtr.ConsumableType) + 1;
                if (newmtr.ConsumableType == "Consumable")
                    newmtr.ReferenceNo = "TO/" + newmtr.Year + "/S-" + newmtr.SiteID + "/C-" + newmtr.RID;
                if (newmtr.ConsumableType == "Returnable")
                    newmtr.ReferenceNo = "TO/" + newmtr.Year + "/S-" + newmtr.SiteID + "/R-" + newmtr.RID;
                int i = 1;
                foreach (var item in newmtr.MTRLines)
                {
                    item.LineID = i;
                    i++;
                }
                db.MTRs.AddObject(newmtr);
                db.SaveChanges();
                return provider.GetMObyId(newmtr.ID);
            }
        }

        public MTRLine ConvertfromPRLineToMOLine(PRDetail x, int mtrID)
        {
            MTRLine line = new MTRLine()
            {
                LineID = x.ID,
                MTRID = mtrID,
                ItemID = x.ItemID,
                SizeID = x.SizeID,
                BrandID = x.BrandID,
                ConsumableType = x.ConsumableType,
                RequiredQty = x.Qty,
                ApprovedQty = 0,
                ReleasedQty = 0,
                TransferredQty = 0,
                InTransitQty = 0,
                Remarks = x.Remarks
            };
            return line;
        }

        MTR ConvertTOMO(MMO _mmo)
        {
            MTR mo = new MTR()
            {
                ID = _mmo.ID,
                Remarks = _mmo.Remarks,
                RequestedBy = _mmo.Requester,
                RequestedSiteID = _mmo.RequestingSiteID,
                TransferringSiteID = _mmo.TransferringSiteID,
                TransferBefore = _mmo.TransferDate,
                RaisedBy = _mmo.RaisedBy,
                RaisedOn = _mmo.RaisedOn,
                ConsumableType = _mmo.ConsumableType
            };
            if (_mmo.MOLines != null) _mmo.MOLines.ToList()
                  .ForEach(x => mo.MTRLines.Add(ConvertToMOLine(x, _mmo.ConsumableType)));
            db.SaveChanges();
            return mo;
        }

        MTRLine ConvertToMOLine(MMOLine x, string ct)
        {
            MTRLine line = new MTRLine()
            {
                LineID = x.LineID,
                ItemID = x.ItemID,
                InTransitQty = 0,
                ReleasedQty = 0,
                TransferredQty = 0,
                SizeID = x.SizeID,
                BrandID = x.BrandID,
                Remarks = x.LineRemarks,
                RequiredQty = x.Quantity,
                ConsumableType = ct,
                MTRID = x.ID//?
            };
            return line;
        }

        //MTRLine ConvertFromMMOSplittedLineToMOLine(MMOSplittedLine x)
        //{
        //    MTRLine line = new MTRLine()
        //    {
        //        LineID = x.LineID,
        //        ItemID = x.ItemID,
        //        InTransitQty = 0,
        //        ReleasedQty = 0,
        //        TransferredQty = 0,
        //        SizeID = x.SizeID,
        //        BrandID = x.BrandID,
        //        Remarks = x.Remarks,
        //        RequiredQty = x.ConfirmedQty,
        //        ConsumableType = x.ConsumableType,
        //        //MTRID = x.MOID//?
        //    };
        //    return line;
        //}

        //public void ReleaseMO(int moid)
        //{
        //   var data= db.MTRs.Single(x=>x.ID==moid);
        //   data.IsReleased = true;
        //   data.ReleasedOn = DateTime.UtcNow.AddHours(5.5);
        //   data.ReleasedBy = "dba";
        //   db.SaveChanges();
        //}

        public void ReceivedMO(int moid)
        {
            throw new NotImplementedException();
        }

        public string CreateMORelease(MReceiving mrec)
        {
            ReceivingManager rm = new ReceivingManager();
            mrec.TransactionID = db.GetMaxMOId("MORelease") + 1;
            mrec.TransactionTypeID = "MORelease";
            mrec.CreatedBy = "dba";
            mrec.LastUpdatedBy = "dba";
            mrec.LastUpdaetdOn = DateTime.UtcNow.AddHours(5.5);
            mrec.CreatedOn = DateTime.UtcNow.AddHours(5.5);
            var rec = rm.ConvertToINTR(mrec);
            //rec.CreatedBy = mrec.Purchaser;
            //rec.LastUpdatedBy = mrec.Purchaser;
            rec.Year = DateTime.UtcNow.AddHours(5.5).Year;
            rec.SiteID = db.GetSiteCode(rec.BUID);//???-------------
            rec.RID = db.GetMaxRRID("MORelease", rec.Year, rec.BUID) + 1;
            rec.ReferenceNo = rec.TypeID + "/" + rec.Year + "/S-" + rec.SiteID + "/" + rec.RID;
            rec.TransitID = 1;
            db.InventoryTransactions.AddObject(rec);
            db.SaveChanges();
            return rec.ReferenceNo;
        }

        public int CreateMOReceive(MReceiving mrec)
        {
            ReceivingManager rm = new ReceivingManager();
            mrec.TransactionID = db.GetMaxMOId("MOReceive") + 1;
            mrec.TransactionTypeID = "MOReceive";
            mrec.CreatedBy = "dba";
            mrec.LastUpdatedBy = "dba";
            mrec.LastUpdaetdOn = DateTime.UtcNow.AddHours(5.5);
            mrec.CreatedOn = DateTime.UtcNow.AddHours(5.5);
            var rec = rm.ConvertToINTR(mrec);
            //rec.CreatedBy = mrec.Purchaser;
            // rec.LastUpdatedBy = mrec.Purchaser;
            rec.Year = DateTime.UtcNow.AddHours(5.5).Year;
            rec.SiteID = db.GetSiteCode(rec.BUID);//???-------------
            rec.RID = db.GetMaxRRID("MOReceive", rec.Year, rec.BUID) + 1;
            rec.ReferenceNo = rec.TypeID + "/" + rec.Year + "/S" + rec.SiteID + "/" + rec.RID;
            db.InventoryTransactions.AddObject(rec);
            rec.TransitID = 1;
            db.SaveChanges();
            db.CloseMO_IFRequired(mrec.MOID);
            return mrec.TransactionID;
        }

        public void CloseMO(int moid)
        {
            var data = db.MTRs.Single(x => x.ID == moid);
            data.IsClosed = true;
            data.ClosedBy = "dba";
            db.SaveChanges();
        }

        public MMO CombineMO(int moid)
        {
            dc.CombineMOs(moid);
            var data = provider.GetMObyId(moid);
            return data;
        }

        /********************************************************************/
        /*NEW METHODS after Double Entry System*/
        /*By: MADHUR*/

        public string MORelease(MMOReleasing mrec, string username)
        {
            mrec.ID = db.GetMaxMOReleaseID() + 1;
            mrec.LastUpdatedBy = username;
            mrec.LastUpdatedOn = DateTime.UtcNow.AddHours(5.5);

            var rec = ConvertToMOReleasing(mrec);

            rec.Year = DateTime.UtcNow.AddHours(5.5).Year;
            rec.SiteCode = db.GetSiteCode(rec.TransferringSiteID);
            rec.RID = db.GetMaxMOReleaseRID(rec.MTRID) + 1;
            //rec.RID = db.GetMaxMOReleaseRID(rec.Year, rec.TransferringSiteID) + 1;
            //rec.ReferenceNo = "TORelease/" + rec.Year + "/S-" + rec.SiteCode + "/" + rec.RID;
            rec.ReferenceNo = mrec.MOReferenceNo + "/Dis-" + rec.RID;
            rec.ChallanNo = rec.RID.ToString();    /*Copying RID as challan no.*/

            db.MOReleasings.AddObject(rec);
            db.SaveChanges();

            return rec.ReferenceNo;
        }

        public int MOReceive(MMOReceiving mrec, string username)
        {
            mrec.ID = db.GetMaxMOReceiveID() + 1;
            mrec.LastUpdatedBy = username;
            mrec.LastUpdatedOn = DateTime.UtcNow.AddHours(5.5);

            var rec = ConvertToMOReceiving(mrec);

            rec.Year = DateTime.UtcNow.AddHours(5.5).Year;
            rec.SiteCode = db.GetSiteCode(rec.RequestingSiteID);
            rec.RID = db.GetMaxMOReceiveRID(rec.MTRID) + 1;
            //rec.RID = db.GetMaxMOReceiveRID(rec.Year, rec.RequestingSiteID) + 1;
            //rec.ReferenceNo = "TOReceive/" + rec.Year + "/S-" + rec.SiteCode + "/" + rec.RID;
            rec.ReferenceNo = mrec.MOReleaseRefNo + "/Rec-" + rec.RID;
            rec.ChallanNo = rec.RID.ToString();    /*Copying RID as challan no.*/
            db.MOReceivings.AddObject(rec);
            db.SaveChanges();

            return rec.ID;
        }

        #region Convertion Methods

        #region MO Release

        public MOReleasing ConvertToMOReleasing(MMOReleasing data)
        {
            MOReleasing mrec = new MOReleasing()
            {
                ReferenceNo = data.ReferenceNo,
                ID = data.ID,
                TransferringSiteID = data.TransferringSiteID,
                ChallanNo = data.ChallanNo,
                ReleasedBy = data.ReleasedBy,
                ReleasedOn = data.ReleasedOn,
                LastUpdatedOn = data.LastUpdatedOn,
                LastUpdatedBy = data.LastUpdatedBy,
                Remarks = data.Remarks,
                TransitID = data.TransporterID,
                MTRID = data.MTRID,
                ConsumableType = data.ConsumableType
            };
            if (data.MOReleasingLines != null)
            {
                data.MOReleasingLines.ToList()
                    .ForEach(x => mrec.MOReleasingLines.Add(ConvertToMOReleasingLines(x)));
            }
            return mrec;
        }

        public MOReleasingLine ConvertToMOReleasingLines(MMOReleasingLine x)
        {
            MOReleasingLine mrline = new MOReleasingLine()
            {
                ID = x.LineID,
                ItemID = x.ItemID,
                SizeID = x.SizeID,
                BrandID = x.BrandID,
                HeaderID = x.HeaderID,
                Remarks = x.LineRemarks,
                Qty = x.Quantity,
                TruckNo = x.TruckNo,
                ChallanNo = x.LineChallanNo,
                OrderedQty = x.OrderedQty,
                EarlierReleasedQty = x.EarlierReleasedQty,
                MOID = x.MOID,
                MOLineID = x.MOLineID
            };
            mrline.IsReceived = false;
            mrline.ReceivingQty = 0;
            return mrline;
        }

        #endregion

        #region MO Receive

        public MOReceiving ConvertToMOReceiving(MMOReceiving data)
        {
            MOReceiving mrec = new MOReceiving()
            {
                ReferenceNo = data.ReferenceNo,
                ID = data.ID,
                RequestingSiteID = data.RequestingSiteID,
                ChallanNo = data.ChallanNo,
                ReceivedBy = data.ReceivedBy,
                ReceivedOn = data.ReceivedOn,
                LastUpdatedOn = data.LastUpdatedOn,
                LastUpdatedBy = data.LastUpdatedBy,
                Remarks = data.Remarks,
                TransitID = data.TranporterID,
                MTRID = data.MTRID,
                ConsumableType = data.ConsumableType
            };
            if (data.MOReceivingLines != null)
            {
                data.MOReceivingLines.ToList()
                    .ForEach(x => mrec.MOReceivingLines.Add(ConvertToMOReceivingLines(x)));
            }
            return mrec;
        }

        public MOReceivingLine ConvertToMOReceivingLines(MMOReceivingLine x)
        {
            MOReceivingLine mrline = new MOReceivingLine()
            {
                ID = x.LineID,
                ItemID = x.ItemID,
                SizeID = x.SizeID,
                BrandID = x.BrandID,
                HeaderID = x.HeaderID,
                Remarks = x.LineRemarks,
                Qty = x.Quantity,
                TruckNo = x.TruckNo,
                ChallanNo = x.LineChallanNo,
                OrderedQty = x.OrderedQty,
                EarlierReceivedQty = x.EarlierReceivedQty,
                MOID = x.MOID,
                MOLineID = x.MOLineID,
                ReleasingID = x.ReleasingID,
                ReleasingLineID = x.ReleasingLineID
            };
            return mrline;
        }

        #endregion

        #endregion

        /********************************************************************/
    }
}
