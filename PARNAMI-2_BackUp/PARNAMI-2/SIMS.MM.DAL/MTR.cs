﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SIMS.MM.DAL
{
    public partial class MTR
    {
        parnamidb1Entities db = new parnamidb1Entities();

        public string ApprovalStatus
        {
            get
            {
                if (this.IsApproved)
                    return "Approved";
                else
                    return "Pending Approval";
            }
        }

        //public string ReceivedStatus
        //{
        //    get
        //    {
        //        if (this.CanReceive)
        //            return "Received";
        //        else
        //            return "Not Received";
        //    }
        //}

        //public string ReleasedStatus
        //{
        //    get
        //    {
        //        if (this.CanRelease)
        //            return "Released";
        //        else
        //            return "Not Released";
        //    }
        //}

        public string ClosedStatus
        {
            get
            {
                if (this.IsClosed)
                    return "Closed!";
                else
                    return "Pending...";
            }
        }

        //public string TransporterName
        //{
        //    get
        //    {
        //        if (Transporter.Name != null)
        //            return this.transpo;
        //        else
        //            return "N/A";
        //    }
        //}

        public MTRLine GetMTRLine(int transSiteID, int ReqSiteID, string ConsumableType)
        {
            var data = db.MTRLines
                .Where(x =>
                    x.MTR.TransferringSiteID == transSiteID &&
                    x.MTR.RequestedSiteID == ReqSiteID &&
                    x.MTR.ConsumableType == ConsumableType &&
                    !x.MTR.IsClosed)
                .Where(x => x.ReleasedQty < x.RequiredQty)
            .OrderByDescending(x => x.MTRID)
            .FirstOrDefault();
            return data;
        }
    }
}
