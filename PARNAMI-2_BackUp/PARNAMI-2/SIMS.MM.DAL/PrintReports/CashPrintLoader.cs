﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SIMS.MM.DAL.PrintReports
{
    using SIMS.ReportLoader;

    public class CashPrintLoader : ReportLoader
    {
        public override RDLCReportData GetReportData(ReportParameterDictionary Dic)
        {
            RDLCReportData data = new RDLCReportData();

            parnamidb1Entities db = new parnamidb1Entities();
            MMReportsDBDataContext rdb = new MMReportsDBDataContext();

            int cid = (int)Dic["id"];

            var header = rdb.GetCPHeaders(null, null, null, null, null).Where(x => x.ID == cid).ToList();
            var detail = rdb.GetCPDetailsByUniqueID(cid);
            var vendor = rdb.GetVendorDetailsbyVendorID(header.First().VendorID);
            var site = rdb.GetSiteDetailsbySiteID(header.First().BUID);
            var app = db.AppSettings;

            data.Add("Headers", header);
            data.Add("Details", detail);
            data.Add("VendorDetails", vendor);
            data.Add("SiteDetails", site);
            data.Add("AppSettings", app);

            return data;
        }
    }
}
