﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SIMS.MM.DAL.PrintReports
{
    using SIMS.ReportLoader;

    public class PRPrintLoader : ReportLoader
    {
        public override RDLCReportData GetReportData(ReportParameterDictionary Dic)
        {
            RDLCReportData data = new RDLCReportData();

            parnamidb1Entities db = new parnamidb1Entities();
            MMReportsDBDataContext rdb = new MMReportsDBDataContext();

            int prid = (int)Dic["id"];

            var header = rdb.GetPRHeaders(null, null, null, null, null, null).Where(x => x.ID == prid).ToList();
            var detail = rdb.GetPRDetailsByUniqueID(prid);
            var vendor = rdb.GetVendorDetailsbyVendorID(header.First().VendorID);
            var site = rdb.GetSiteDetailsbySiteID(header.First().SiteID);
            var app = db.AppSettings;

            data.Add("Headers", header);
            data.Add("Details", detail);
            data.Add("VendorDetails", vendor);
            data.Add("SiteDetails", site);
            data.Add("AppSettings", app);

            return data;
        }
    }
}
