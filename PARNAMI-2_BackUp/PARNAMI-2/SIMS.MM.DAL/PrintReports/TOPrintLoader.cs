﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SIMS.MM.DAL.PrintReports
{
    using SIMS.ReportLoader;

    public class TOPrintLoader : ReportLoader
    {
        public override RDLCReportData GetReportData(ReportParameterDictionary Dic)
        {
            RDLCReportData data = new RDLCReportData();

            parnamidb1Entities db = new parnamidb1Entities();
            MMReportsDBDataContext rdb = new MMReportsDBDataContext();

            int moid = (int)Dic["id"];

            var header = rdb.GetMOHeaders(null, null, null, null, null, null, null).Where(x => x.ID == moid).ToList();
            var detail = rdb.GetMODetailsByUniqueID(moid);
            var Tsite = rdb.GetSiteDetailsbySiteID(header.First().TransferringSiteID);
            var Rsite = rdb.GetSiteDetailsbySiteID(header.First().RequestedSiteID);
            var app = db.AppSettings;

            data.Add("Headers", header);
            data.Add("Details", detail);
            data.Add("TransferringSiteDetails", Tsite);
            data.Add("RequestingSiteDetails", Rsite);
            data.Add("AppSettings", app);

            return data;
        }
    }
}
