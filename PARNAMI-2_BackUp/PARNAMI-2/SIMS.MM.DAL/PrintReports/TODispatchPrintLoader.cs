﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SIMS.MM.DAL.PrintReports
{
    using SIMS.ReportLoader;

    public class TODispatchPrintLoader : ReportLoader
    {
        public override RDLCReportData GetReportData(ReportParameterDictionary Dic)
        {
            RDLCReportData data = new RDLCReportData();

            parnamidb1Entities db = new parnamidb1Entities();
            MMReportsDBDataContext rdb = new MMReportsDBDataContext();

            int mo_rel_id = (int)Dic["id"];

            var header = rdb.GetMODispatchHeaders().Where(x => x.ID == mo_rel_id).ToList();
            var detail = rdb.GetMODispatchDetailsByUniqueID(mo_rel_id);
            var Tsite = rdb.GetSiteDetailsbySiteID(header.First().TransferringSiteID);
            var Rsite = rdb.GetSiteDetailsbySiteID(header.First().RequestingSiteID);
            var app = db.AppSettings;

            data.Add("Headers", header);
            data.Add("Details", detail);
            data.Add("TransferringSiteDetails", Tsite);
            data.Add("RequestingSiteDetails", Rsite);
            data.Add("AppSettings", app);

            return data;
        }
    }
}
