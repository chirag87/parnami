﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using SIMS.MM.MODEL;

namespace Contruction
{
    public partial class MTRView : UserControl
    {
        public NewMTRModel mtrModel
        {
            get { return (NewMTRModel)this.DataContext; }
        }
				

        public MTRView()
        {
            InitializeComponent();
        }

        private void Cancel_Clicked(object sender, RoutedEventArgs e)
        {

        }

        private void chkCanApprove_Checked(object sender, RoutedEventArgs e)
        {

        }

        private void textBlock_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            var textblock = sender as TextBlock;
            var line = (MMTRLine)textblock.DataContext;
            mtrModel.DeleteLine(line.LineID);
        }
    }
}
