﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace Contruction
{
    public partial class NewMessage : UserControl
    {
        public NewMessageModel Model
        {
            get { return (NewMessageModel)this.DataContext; }
        }

        public NewMessage()
        {
            InitializeComponent();
            receiversList.ItemsSource = Base.Current.UserNames;
            emailersList.ItemsSource = Base.Current.Emails;
        }

        private void BackImage_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            Base.Redirect("Inbox");
        }

        private void btnEMAIL_Click(object sender, RoutedEventArgs e)
        {
            EmailAddresses emails = new EmailAddresses();
            emails.Show();
        }

        private void btnTO_Click(object sender, RoutedEventArgs e)
        {
            UserNames users = new UserNames();
            users.Show();
            //users.Submitted += new EventHandler(users_Submitted);
        }

        //void users_Submitted(object sender, EventArgs e)
        //{
        //    foreach(var item in )
        //    {
        //        receiversList.SelectedItems.Add(item);
        //    }
        //}

        private void btnSend_Click(object sender, RoutedEventArgs e)
        {
            Model.NewMessage.ReceiversName = receiversList.SelectedItems;
            Model.NewMessage.EmailersName = emailersList.SelectedItems;
            Model.SendMail();
        }

        public void Reset()
        {
            receiversList = null;
            emailersList = null;
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            receiversList.Focus();
        }
    }
}
