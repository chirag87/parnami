﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace Contruction
{
    public partial class AllTransporters : UserControl
    {
        public AllTransporters()
        {
            InitializeComponent();
        }

        private void tbxKey_KeyUp(object sender, KeyEventArgs e)
        {
            var box = sender as TextBox;
            Base.Current.SearchTransporter = box.Text;
        }

        private void tbxKey_TextChanged(object sender, TextChangedEventArgs e)
        {
            var box = sender as TextBox;
            Base.Current.SearchTransporter = box.Text;
        }

        private void btnExport_Click(object sender, RoutedEventArgs e)
        {
            dgdAllTransporters.Export();
        }

        private void btnNew_Click(object sender, RoutedEventArgs e)
        {
            NewTransporter trans = new NewTransporter();
            trans.Show();
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            tbxKey.Focus();
        }
    }
}
