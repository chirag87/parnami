﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.ComponentModel;
using Contruction.MasterDBService;
using SIMS.MM.MODEL;

namespace Contruction
{
    public partial class Contacts : UserControl, INotifyPropertyChanged
    {
        public ContactsModel Model
        {
            get { return (ContactsModel)this.DataContext; }
        }
				
        public Contacts()
        {
            InitializeComponent();
        }

        public int ForID
        {
            get { return Model.ForID; }
            set
            {
                Model.ForID = value;
                Notify("ForID");
            }
        }

        public string For
        {
            get { return Model.For; }
            set
            {
                Model.For = value;
                Notify("For");
            }
        }

        private void btnEdit_Click(object sender, RoutedEventArgs e)
        {
            
        }

        private void btnAddNew_Click(object sender, RoutedEventArgs e)
        {
            EditContacts contact = new EditContacts(ForID);
            contact.Show();
        }

        private void btnDelete_Click(object sender, RoutedEventArgs e)
        {
            var btn = sender as Button;
            var data = (MContact)btn.DataContext;
            if (data == null) return;
            Model.DeleteContact(data.ID);
        }

        private void dataGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            //var line = sender as DataGrid;
            //var data = (MContact)line.SelectedItem;
            //Model.GetContact(data.ID);
        }

        private void hpbtnReload_Click(object sender, RoutedEventArgs e)
        {
            Model.Load();
        }

        private void dataGrid_CellEditEnded(object sender, DataGridCellEditEndedEventArgs e)
        {
            var line = sender as DataGrid;
            var data = (MContact)line.SelectedItem;
            if (data == null) return;
            Model.SelectedContact = data;
            Model.UpdateContact();
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void Notify(string propertyName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
