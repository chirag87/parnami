﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace Contruction
{
    public partial class ConfirmSplitMO : ChildWindow
    {
        public ViewPRDetailsModel Model
        {
            get { return (ViewPRDetailsModel)this.DataContext; }
        }

        public event EventHandler MOConfirm;
        public void RaiseMOConfirm()
        {
            if (MOConfirm != null)
                MOConfirm(this, new EventArgs());
        }

        public ConfirmSplitMO()
        {
            InitializeComponent();
        }

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
            RaiseMOConfirm();
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

        private void ChildWindow_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Escape)
            {
                this.DialogResult = false;
            }
        }
    }
}

