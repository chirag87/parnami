﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Collections.ObjectModel;
using SIMS.MM.MODEL.Headers;

namespace Contruction
{
	public partial class NewVendorPayment : UserControl
	{
        public NewVendorPaymentVM Model
        {
            get { return (NewVendorPaymentVM)this.DataContext; }
        }
		
		public NewVendorPayment()
		{
			InitializeComponent();
		}

        public NewVendorPayment(ObservableCollection<MVendorPaymentDetail> list)
            :this()
        {
            Model.loaddataInGrid(list);
        }

        private void btnCreate_Click(object sender, RoutedEventArgs e)
        {
            Model.SubmitToDB();
        }

        private void HyperAddNew_Click(object sender, RoutedEventArgs e)
        {
            var popup = new PendingBills();
            popup.CreatePopup();
        }

        private void textBlock_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            var textblock = sender as TextBlock;
            var line = (MVendorPaymentDetail)textblock.DataContext;
            Model.DeleteLine(line.LineID);
        }

        private void tbxTDS_percent_LostFocus(object sender, RoutedEventArgs e)
        {
            Model.UpdateTDS();
            Model.UpdateNetAmount();
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            VendorCombo.Focus();
        }
	}
}