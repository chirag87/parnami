﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace Contruction
{
    public partial class AddNewCompany : ChildWindow
    {
        public NewCompanyModel Model
        {
            get { return (NewCompanyModel)this.DataContext; }
        }

        public AddNewCompany()
        {
            InitializeComponent();
        }

        public AddNewCompany(int id)
            :this()
        {
            Model.GetCompanybyID(id);
        }

        public bool IsNew
        {
            get { return Model.IsNew; }
            set
            {
                Model.IsNew = value;
            }
        }

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            if (Model.IsNew == true)
                Model.SubmitToDB();
            if(Model.IsNew == false)
                Model.UpdateCompany();
            this.DialogResult = true;
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

        private void ChildWindow_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Escape)
            {
                this.DialogResult = false;
            }
        }
    }
}

