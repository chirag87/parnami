﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace Contruction
{
    public partial class LoginControl : UserControl
    {
        public LoginControl()
        {
            // Required to initialize variables
            InitializeComponent();
            //this.GotFocus += (s, e) => { txtUsername.Focus(); };
            Base.Current.LoginStatusChanged += new EventHandler(Current_LoginStatusChanged);
        }

        void Current_LoginStatusChanged(object sender, EventArgs e)
        {
            Status = Base.Current.LoginStatus;
            busy.IsBusy = false;
            if (Base.Current.IsLoggedIn)
                Clear();
        }

        public string Status
        {
            get { return txtStatus.Text; }
            set { txtStatus.Text = value; }
        }

        public string Username
        {
            get { return txtUsername.Text; }
            set { txtUsername.Text = value; }
        }

        public string Password
        {
            get { return txtPassword.Password; }
            set { txtPassword.Password = value; }
        }

        private void btn_Login_Click(object sender, RoutedEventArgs e)
        {
            Login();
        }

        public void Login()
        {
            busy.IsBusy = true;
            Status = "Logging ...";
            if (String.IsNullOrWhiteSpace(Username))
            { Status = "Invaid Username"; return; }
            if (String.IsNullOrWhiteSpace(Password))
            {
                Status = "Please Provide Password";
                return;
            }
            //            busy.IsBusy = true;
            Base.Current.Login(Username, Password);
            busy.IsBusy = false;
        }

        public void Clear()
        {
            Username = "";
            Password = "";
            Status = "";
        }

        private void txtPassword_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                Login();
            }
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            //if (App.Current.IsRunningOutOfBrowser)
            //{
            //    txtUsername.Focus();
            //}
            //else
            //{
            //    System.Windows.Browser.HtmlPage.Plugin.Focus();
                txtUsername.Focus();
            //}
        }
    }
}