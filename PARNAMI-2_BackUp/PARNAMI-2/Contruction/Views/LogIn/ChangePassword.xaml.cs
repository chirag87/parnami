﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace Contruction
{
    public partial class ChangePassword : ChildWindow
    {
        public Base Model
        {
            get { return (Base)this.DataContext; }
        }

        public ChangePassword()
        {
            InitializeComponent();
            tbxUserName.Text = Base.Current.UserName;
            Loaded += new RoutedEventHandler(ChangePassword_Loaded);
        }

        void ChangePassword_Loaded(object sender, RoutedEventArgs e)
        {
            OldPsswrdBox.Focus();
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            if (!Validate()) return;
            Model.ChangePassword(OldPsswrdBox.Password, NewPsswrdBox.Password);
            this.DialogResult = true;
        }

        private void ChildWindow_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Escape)
            {
                this.DialogResult = false;
            }
        }

        public bool Validate()
        {
            if (NewPsswrdBox.Password != ConfirmPsswrdBox.Password)
            {
                MessageBox.Show("Passwords doesn't match!");
                NewPsswrdBox.Password = "";
                ConfirmPsswrdBox.Password = "";
                return false;
            }
            return true;
        }

        private void ChildWindow_Loaded(object sender, RoutedEventArgs e)
        {
            OldPsswrdBox.Focus();
        }
    }
}

