﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Collections.Generic;

namespace Contruction
{
	public partial class MasterReportViewer : UserControl
	{
        public MasterReportModel Model
        {
            get { return (MasterReportModel)this.DataContext; }
        }
				
		public MasterReportViewer()
		{
			// Required to initialize variables
			InitializeComponent();
		}

        private void btnExport_Click(object sender, RoutedEventArgs e)
        {
            datagrid.Export();
        }

        private void hbtnClear_Click(object sender, RoutedEventArgs e)
        {
            reportTypeCombo.SelectedItem = null;
            rangeCombo.SelectedItem = null;
            dpStart.Text = null;
            dpEnd.Text = null;
            Model.Data = new List<object>();
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            reportTypeCombo.Focus();
        }
	}
}