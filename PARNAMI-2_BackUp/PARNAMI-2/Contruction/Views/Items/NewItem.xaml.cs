﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace Contruction
{
    public partial class NewItem : ChildWindow
    {
        public NewItemModel itemModel
        {
            get { return (NewItemModel)this.DataContext; }
        }

        public NewItem()
        {
            InitializeComponent();
        }

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            if (acbCategories.SelectedItem == null)
            {
                if (String.IsNullOrWhiteSpace(acbCategories.Text))
                {
                    MessageBox.Show("Please Input Item Category!");
                    return;
                }
                itemModel.ItemCategory = acbCategories.Text;
            }
            else
            {
                itemModel.NewItem.ItemCategoryID = (int)acbCategories.SelectedValue;
            }
            itemModel.SubmitToDB();
            this.DialogResult = true;
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

        private void ResetButton_Click(object sender, RoutedEventArgs e)
        {
            itemModel.Reset();
        }

        private void HyperlinkButton_Click(object sender, RoutedEventArgs e)
        {
            NewMU mu = new NewMU();
            mu.Show();
        }

        private void ChildWindow_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Escape)
            {
                this.DialogResult = false;
            }
        }

        private void ChildWindow_Loaded(object sender, RoutedEventArgs e)
        {
            acbCategories.Focus();
        }
    }
}

