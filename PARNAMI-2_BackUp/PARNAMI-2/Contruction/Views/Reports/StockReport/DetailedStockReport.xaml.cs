﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.IO;
using SIMS.MM.MODEL;

namespace Contruction
{
    public partial class DetailedStockReport : UserControl
    {
        public DetailedStockReportsModel Model
        {
            get { return (DetailedStockReportsModel)this.DataContext; }
        }
				
        public DetailedStockReport()
        {
            InitializeComponent();
        }

        private void tbxSearch_KeyDown(object sender, KeyEventArgs e)
        {

        }

        private void tbxSearch_TextChanged(object sender, TextChangedEventArgs e)
        {

        }

        private void btnSearchBox_Click(object sender, RoutedEventArgs e)
        {

        }

        private void imgSearchBox1_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            Model.SearchKey = null;
        }

        private void btnCategory_Click(object sender, RoutedEventArgs e)
        {
            //var hbtn = sender as HyperlinkButton;
            //var data = (MStockReport)hbtn.DataContext;
            //OpenNewTabItem(data);
        }

        private void btnVariations_Click(object sender, RoutedEventArgs e)
        {
            var btn = sender as Button;
            var data = (MStockReport)btn.DataContext;
            OpenNewTabItem(data);
        }

        public void OpenNewTabItem(MStockReport data)
        {
            if (data != null)
            {
                var tab = new CloseableTabItem();

                //DataTemplate headertemplate = this.Resources["TabHeaderTemplateForItems"] as DataTemplate;
                //tab.HeaderTemplate = headertemplate;

                tab.Header = data.Display;
                var uc= new MonthlyStockReport(data.SiteID, data.ItemID);
                tab.Content = uc;
                uc.TabControl = tabStockReport;
                tab.Name = data.Item + data.SiteID + " Details";

                string name = data.Item + data.SiteID + " Details";
                var tabitem = tabStockReport.Items.OfType<TabItem>().SingleOrDefault(x => x.Name == name);
                if (tabStockReport.Items.Contains(tabitem))
                {
                    tabStockReport.SelectedItem = tabitem;
                }
                else
                {
                    try
                    {
                        tabStockReport.Items.Add(tab);
                        tabStockReport.SelectedItem = tab;
                    }
                    catch
                    {
                    }
                }
            }
        }

        private void imgDeleteTab_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            var img = sender as Image;
            var data = (MStockReport)img.DataContext;
            var tabitem = tabStockReport.Items.OfType<TabItem>().SingleOrDefault(x => x.Name == data.Item + data.SiteID + " Details");
            if (tabitem != null)
                tabStockReport.Items.Remove(tabitem);
        }

        private void imgDeleteTab2_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            throw new NotImplementedException();
        }

        private void hpClear_Click(object sender, RoutedEventArgs e)
        {
            Model.Clear();
        }

        SaveFileDialog sfd;

        private void download_Click(object sender, RoutedEventArgs e)
        {
            sfd = new SaveFileDialog();
            if (Model.RType != null)
            {
                if (Model.RType == "Excel")
                    sfd.Filter = "Excel files (*.xls)|*.xls";
                else if (Model.RType == "pdf")
                    sfd.Filter = "PDF file format|*.pdf";
            }
            else
            {
                sfd.Filter = "PDF file format|*.pdf";
            }
            if ((bool)sfd.ShowDialog())
            {
                var hp = sender as HyperlinkButton;
                var data = (DetailedStockReportsModel)hp.DataContext;
                //HtmlPage.PopupWindow(new Uri("http://"+Base.Current.AppSettings.ServerPath"/GetReport.ashx?rtype=excel&type=GRNLines&sdate=" + Model.StartDate.ToString() + "&edate=" + Model.EndDate.ToString() + "&vendorid=" + Model.VendorID.ToString() + "&siteid=" + Model.SiteID.ToString() + "&itemid=" + Model.ItemID.ToString(), UriKind.Absolute));

                WebClient webClient = new WebClient();
                webClient.OpenReadCompleted += (s, args) =>
                {
                    StreamReader sr = new StreamReader(args.Result);
                    byte[] bytes = new byte[args.Result.Length];
                    // revisit Here
                    args.Result.Read(bytes, 0, (int)args.Result.Length);
                    Stream st = sfd.OpenFile();
                    st.Write(bytes, 0, (int)args.Result.Length);
                    st.Close();
                };
                //Uri uri = new Uri("http://localhost:18551" + "/GetReport.ashx?type=SiteWiseStockStatus&rtype=" + Model.RType + "&sdate=" + Model.sdate.ToString() + "&edate=" + Model.edate.ToString() + "&siteid=" + Model.SiteID.ToString() + "&itemid=" + Model.ItemID.ToString(), UriKind.Absolute);
                Uri uri = new Uri("http://" + Base.Current.AppSettings.ServerPath + "/GetReport.ashx?type=SiteWiseStockStatus&rtype=" + Model.RType + "&sdate=" + Model.sdate.ToString() + "&edate=" + Model.edate.ToString() + "&siteid=" + Model.SiteID.ToString() + "&itemid=" + Model.ItemID.ToString(), UriKind.Absolute);
                webClient.OpenReadAsync(uri);
            }
        }

        private void btnExport_Click(object sender, RoutedEventArgs e)
        {
            this.dgdStock.Export();
        }

        #region Property
        public DateTime? sdate
        {
            get { return Model.sdate; }
            set
            {
                Model.sdate = value;
            }
        }

        public DateTime? edate
        {
            get { return Model.edate; }
            set
            {
                Model.edate = value;
            }
        }

        public int? SiteID
        {
            get { return Model.SiteID; }
            set
            {
                Model.SiteID = value;
            }
        }

        public int? ItemID
        {
            get { return Model.ItemID; }
            set
            {
                Model.ItemID = value;
            }
        }
        #endregion
    }
}
