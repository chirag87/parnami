﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace Contruction
{
    public partial class CreateNewUser : ChildWindow
    {
        public UserManagementModel Model
        {
            get { return (UserManagementModel)this.DataContext; }
        }

        public CreateNewUser()
        {
            InitializeComponent();
        }

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            Model.SubmitToDB();
            Model.GetAll();
            this.DialogResult = true;
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

        private void ResetButton_Click(object sender, RoutedEventArgs e)
        {
            Model.Reset();
        }

        private void tbkConfirmPassword_LostFocus(object sender, RoutedEventArgs e)
        {
            string str1 = tbkPassword.Password;
            string str2 = tbkConfirmPassword.Password;
            bool check = Model.ConfirmPassword(str1, str2);
            if(!check)
                ResetPsswrd();
        }

        internal void ResetPsswrd()
        {
            tbkPassword.Password = "";
            tbkConfirmPassword.Password = "";
        }

        private void ChildWindow_Loaded(object sender, RoutedEventArgs e)
        {
            tbkUserName.Focus();
        }
    }
}

