﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using SIMS.MM.MODEL;
using System.Collections.ObjectModel;

namespace Contruction
{
    public partial class NewPricing : UserControl
    {
        public NewPricingModel Model
        {
            get { return (NewPricingModel)this.DataContext; }
        }
        
        public NewPricing()
        {
            InitializeComponent();
        }

        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            //Model.SubmittoDB();
            Model.UpdatePrice();
            Reset();
            Base.Current.LoadPricing();
        }

        private void btnAddNewLine_Click(object sender, RoutedEventArgs e)
        {
            Model.AddNewLine();
        }

        private void textBlock_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            
        }

        private void btnClear_Click(object sender, RoutedEventArgs e)
        {
            dgd.ItemsSource = null;
            Model.Reset();
        }

        private void btnResetForm_Click(object sender, RoutedEventArgs e)
        {
            Reset();
            Model.Reset();
        }

        public void Reset()
        {
            Model.NotifyCans();
            Model.SelectedItemID = 0;
            Model.SelectedSiteID = null;
            Model.SelectedVendorID = 0;
            dgd.ItemsSource = new ObservableCollection<MPriceMaster>();
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            SiteCombo_Copy.Focus();
        }
    }
}