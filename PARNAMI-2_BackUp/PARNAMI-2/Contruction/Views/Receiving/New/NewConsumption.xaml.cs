﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using SIMS.MM.MODEL;

namespace Contruction
{
    public partial class NewConsumption : UserControl
    {
        public NewConsumptionModel Model
        {
            get { return (NewConsumptionModel)this.DataContext; }
        }
				
        public NewConsumption()
        {
            InitializeComponent();
        }

        private void textBlock_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            var textblock = sender as TextBlock;
            //var line = (MReceivingLine)textblock.DataContext;
            var line = (MConsumptionLine)textblock.DataContext;
            Model.DeleteLine(line.LineID);
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            SiteCombo.Focus();
        }

        private void btnReset_Click(object sender, RoutedEventArgs e)
        {
            Model.Reset();
        }

        public void ResetForm()
        {
            this.status = null;
            this.SiteCombo.SelectedItem = null;
        }

        private void InfoIcon_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            var img = sender as Image;
            var data = (MConsumptionLine)img.DataContext;
            if (data.ItemID == 0) return;
            var popup = new ItemSummary(data.ItemID, data.SizeID, data.BrandID);
            popup.CreatePopup();
        }

        #region CreatePopUP Members
        public bool IsPopUpMode
        {
            get { return MyPopUP != null; }
        }

        public PopUp MyPopUP { get; set; }

        public PopUp CreatePopup()
        {
            MyPopUP = this.ToPopup(false);
            MyPopUP.SetAutoLayout();
            MyPopUP.Title = "Consumption Detailed View";
            return MyPopUP;
        }
        #endregion
    }
}
