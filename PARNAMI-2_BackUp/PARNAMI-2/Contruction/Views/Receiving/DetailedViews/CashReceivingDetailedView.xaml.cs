﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using SIMS.MM.MODEL;
using System.IO;

namespace Contruction
{
    public partial class CashReceivingDetailedView : UserControl
    {
        public ViewCashPurchaseDetailsModel Model
        {
            get { return (ViewCashPurchaseDetailsModel)this.DataContext; }
        }

        public CashReceivingDetailedView()
        {
            InitializeComponent();
        }

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
        }			

        internal void setID(int p)
        {
            Model.SetID(p);
        }

        private void InfoIcon_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            var img = sender as Image;
            var data = (MCashPurchaseLine)img.DataContext;
            if (data.ItemID == 0) return;
            var popup = new ItemSummary(data.ItemID, data.SizeID, data.BrandID);
            popup.CreatePopup();
        }

        SaveFileDialog sfd;

        private void download_Click(object sender, RoutedEventArgs e)
        {
            sfd = new SaveFileDialog();
            sfd.Filter = "PDF file format|*.pdf";
            if ((bool)sfd.ShowDialog())
            {
                var hp = sender as HyperlinkButton;
                var data = (ViewCashPurchaseDetailsModel)hp.DataContext;
                //HtmlPage.PopupWindow(new Uri("http://localhost:3385/GetReport.ashx?type=PO&id=" + data.NewGRN.TransactionID.ToString(), UriKind.Absolute), "_report", new HtmlPopupWindowOptions() { });

                WebClient webClient = new WebClient();
                webClient.OpenReadCompleted += (s, args) =>
                {
                    StreamReader sr = new StreamReader(args.Result);
                    byte[] bytes = new byte[args.Result.Length];
                    // revisit Here
                    args.Result.Read(bytes, 0, (int)args.Result.Length);
                    Stream st = sfd.OpenFile();
                    st.Write(bytes, 0, (int)args.Result.Length);
                    st.Close();
                };
                //webClient.OpenReadAsync(new Uri("http://localhost:3385/GetReport.ashx?type=GRN&id=" + data.NewGRN.ID.ToString(), UriKind.Absolute));
                Uri uri = new Uri("http://" + Base.Current.AppSettings.ServerPath + "/GetReport.ashx?rtype=pdf&type=Cash&id=" + data.NewTransaction.ID.ToString(), UriKind.Absolute);
                webClient.OpenReadAsync(uri);
            }
        }

        #region CreatePopUP Members
        public bool IsPopUpMode
        {
            get { return MyPopUP != null; }
        }

        public PopUp MyPopUP { get; set; }

        public PopUp CreatePopup()
        {
            MyPopUP = this.ToPopup(false);
            MyPopUP.SetAutoLayout();
            MyPopUP.Title = "Cash Receiving";
            return MyPopUP;
        }
        #endregion
    }
}

