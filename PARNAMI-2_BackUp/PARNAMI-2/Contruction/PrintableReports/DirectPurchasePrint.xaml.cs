﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using SIMS.MM.MODEL;
using System.Windows.Printing;

namespace Contruction
{
    public partial class DirectPurchasePrint : UserControl
    {
        public DirectPurchasePrint()
        {
            InitializeComponent();
        }

        public void SetDP(MReceiving _mreceiving)
        {
            this.DataContext = _mreceiving;
            if (_mreceiving != null)
                Print();
        }

        public void Print()
        {
            if (this.DataContext == null) return;
            PrintDocument doc = new PrintDocument();
            doc.PrintPage += new EventHandler<PrintPageEventArgs>(doc_PrintPage);
            doc.Print("DP");
        }

        void doc_PrintPage(object sender, PrintPageEventArgs e)
        {
            VisualStateManager.GoToState(this as Control, "Print", true);
            this.Width = e.PrintableArea.Width;
            this.Height = e.PrintableArea.Height;
            e.PageVisual = this;
        }
    }
}
