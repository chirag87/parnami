﻿#pragma checksum "D:\OFFICE-WORK\INNOSOLS\WORK\SILVERLIGHT\PARNAMI\PARNAMI-2\Contruction\Views\Address\Address.xaml" "{406ea660-64cf-4c82-b6f0-42d48172a799}" "D45003DF4F7D829FE7272437DFF9A088"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.296
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Automation.Peers;
using System.Windows.Automation.Provider;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Interop;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Resources;
using System.Windows.Shapes;
using System.Windows.Threading;


namespace Contruction {
    
    
    public partial class Address : System.Windows.Controls.ChildWindow {
        
        internal System.Windows.Controls.Grid LayoutRoot;
        
        internal System.Windows.Controls.TextBox tbxAddressName;
        
        internal System.Windows.Controls.TextBox tbxAddressline1;
        
        internal System.Windows.Controls.TextBox tbxAddressline2;
        
        internal System.Windows.Controls.TextBox tbxCity;
        
        internal System.Windows.Controls.TextBox tbxState;
        
        internal System.Windows.Controls.TextBox tbxCountry;
        
        internal System.Windows.Controls.TextBox tbxPincode;
        
        internal System.Windows.Controls.TextBox tbxPhone;
        
        internal System.Windows.Controls.TextBox tbxFax;
        
        internal System.Windows.Controls.TextBox tbxEmail;
        
        internal System.Windows.Controls.TextBox tbxWebsite;
        
        internal System.Windows.Controls.ComboBox comboAddressType;
        
        internal System.Windows.Controls.Button btnSave;
        
        internal System.Windows.Controls.Button CancelButton;
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Windows.Application.LoadComponent(this, new System.Uri("/Contruction;component/Views/Address/Address.xaml", System.UriKind.Relative));
            this.LayoutRoot = ((System.Windows.Controls.Grid)(this.FindName("LayoutRoot")));
            this.tbxAddressName = ((System.Windows.Controls.TextBox)(this.FindName("tbxAddressName")));
            this.tbxAddressline1 = ((System.Windows.Controls.TextBox)(this.FindName("tbxAddressline1")));
            this.tbxAddressline2 = ((System.Windows.Controls.TextBox)(this.FindName("tbxAddressline2")));
            this.tbxCity = ((System.Windows.Controls.TextBox)(this.FindName("tbxCity")));
            this.tbxState = ((System.Windows.Controls.TextBox)(this.FindName("tbxState")));
            this.tbxCountry = ((System.Windows.Controls.TextBox)(this.FindName("tbxCountry")));
            this.tbxPincode = ((System.Windows.Controls.TextBox)(this.FindName("tbxPincode")));
            this.tbxPhone = ((System.Windows.Controls.TextBox)(this.FindName("tbxPhone")));
            this.tbxFax = ((System.Windows.Controls.TextBox)(this.FindName("tbxFax")));
            this.tbxEmail = ((System.Windows.Controls.TextBox)(this.FindName("tbxEmail")));
            this.tbxWebsite = ((System.Windows.Controls.TextBox)(this.FindName("tbxWebsite")));
            this.comboAddressType = ((System.Windows.Controls.ComboBox)(this.FindName("comboAddressType")));
            this.btnSave = ((System.Windows.Controls.Button)(this.FindName("btnSave")));
            this.CancelButton = ((System.Windows.Controls.Button)(this.FindName("CancelButton")));
        }
    }
}

