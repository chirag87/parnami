﻿#pragma checksum "D:\OFFICE-WORK\INNOSOLS\WORK\SILVERLIGHT\Contruction\Contruction\UserControls\Purchasing\ViewPOs.xaml" "{406ea660-64cf-4c82-b6f0-42d48172a799}" "9FEA1B1B336FFEEE1ED8A36CA2DBDB5F"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.239
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using Innosols;
using System;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Automation.Peers;
using System.Windows.Automation.Provider;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Interop;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Resources;
using System.Windows.Shapes;
using System.Windows.Threading;


namespace Innosols {
    
    
    public partial class ViewPOs : System.Windows.Controls.UserControl {
        
        internal System.Windows.VisualStateGroup PreviewStates;
        
        internal System.Windows.VisualState Normal;
        
        internal System.Windows.VisualState HidePreview;
        
        internal System.Windows.Controls.Grid LayoutRoot;
        
        internal System.Windows.Controls.DomainDataSource domainDataSource;
        
        internal DataGrid dataGrid;
        
        internal System.Windows.Controls.StackPanel FilterPanel;
        
        internal System.Windows.Controls.TextBox textBox;
        
        internal AutoCompleteBox autoCompleteBox;
        
        internal AutoCompleteBox autoCompleteBox1;
        
        internal Innosols.POPreview BotPreview;
        
        internal Innosols.PRView PrView;
        
        internal System.Windows.Controls.Primitives.ToggleButton toggleButton;
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Windows.Application.LoadComponent(this, new System.Uri("/Contruction;component/UserControls/Purchasing/ViewPOs.xaml", System.UriKind.Relative));
            this.PreviewStates = ((System.Windows.VisualStateGroup)(this.FindName("PreviewStates")));
            this.Normal = ((System.Windows.VisualState)(this.FindName("Normal")));
            this.HidePreview = ((System.Windows.VisualState)(this.FindName("HidePreview")));
            this.LayoutRoot = ((System.Windows.Controls.Grid)(this.FindName("LayoutRoot")));
            this.domainDataSource = ((System.Windows.Controls.DomainDataSource)(this.FindName("domainDataSource")));
            this.dataGrid = ((DataGrid)(this.FindName("dataGrid")));
            this.FilterPanel = ((System.Windows.Controls.StackPanel)(this.FindName("FilterPanel")));
            this.textBox = ((System.Windows.Controls.TextBox)(this.FindName("textBox")));
            this.autoCompleteBox = ((AutoCompleteBox)(this.FindName("autoCompleteBox")));
            this.autoCompleteBox1 = ((AutoCompleteBox)(this.FindName("autoCompleteBox1")));
            this.BotPreview = ((Innosols.POPreview)(this.FindName("BotPreview")));
            this.PrView = ((Innosols.PRView)(this.FindName("PrView")));
            this.toggleButton = ((System.Windows.Controls.Primitives.ToggleButton)(this.FindName("toggleButton")));
        }
    }
}

