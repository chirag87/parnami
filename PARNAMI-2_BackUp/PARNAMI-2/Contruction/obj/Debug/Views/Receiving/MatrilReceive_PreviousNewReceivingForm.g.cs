﻿#pragma checksum "D:\OFFICE-WORK\INNOSOLS\WORK\SILVERLIGHT\PARNAMI\PARNAMI-2\Contruction\Views\Receiving\MatrilReceive_PreviousNewReceivingForm.xaml" "{406ea660-64cf-4c82-b6f0-42d48172a799}" "D4477F5B6647CA50993AAC941E6EB014"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.269
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using SyedMehrozAlam.CustomControls;
using System;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Automation.Peers;
using System.Windows.Automation.Provider;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Interop;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Resources;
using System.Windows.Shapes;
using System.Windows.Threading;


namespace Contruction {
    
    
    public partial class MatrilReceive_PreviousNewReceivingForm : System.Windows.Controls.UserControl {
        
        internal System.Windows.Controls.Grid LayoutRoot;
        
        internal System.Windows.Controls.RadioButton radioButton;
        
        internal System.Windows.Controls.TextBlock textBlock;
        
        internal SyedMehrozAlam.CustomControls.AutoCompleteComboBox VendorCombo;
        
        internal SyedMehrozAlam.CustomControls.AutoCompleteComboBox SiteCombo;
        
        internal System.Windows.Controls.StackPanel Remarks_Panel;
        
        internal System.Windows.Controls.TextBox textBox;
        
        internal System.Windows.Controls.StackPanel Remarks_Panel_Copy;
        
        internal System.Windows.Controls.TextBox textBox1;
        
        internal System.Windows.Controls.TextBox textBox2;
        
        internal System.Windows.Controls.StackPanel Remarks_Panel_Copy1;
        
        internal System.Windows.Controls.TextBox textBox3;
        
        internal System.Windows.Controls.StackPanel Btn_Panel;
        
        internal System.Windows.Controls.Button Confirm;
        
        internal System.Windows.Controls.Button SaveAsDraft;
        
        internal System.Windows.Controls.Button Cancel;
        
        internal System.Windows.Controls.HyperlinkButton hyperlinkButton;
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Windows.Application.LoadComponent(this, new System.Uri("/Contruction;component/Views/Receiving/MatrilReceive_PreviousNewReceivingForm.xam" +
                        "l", System.UriKind.Relative));
            this.LayoutRoot = ((System.Windows.Controls.Grid)(this.FindName("LayoutRoot")));
            this.radioButton = ((System.Windows.Controls.RadioButton)(this.FindName("radioButton")));
            this.textBlock = ((System.Windows.Controls.TextBlock)(this.FindName("textBlock")));
            this.VendorCombo = ((SyedMehrozAlam.CustomControls.AutoCompleteComboBox)(this.FindName("VendorCombo")));
            this.SiteCombo = ((SyedMehrozAlam.CustomControls.AutoCompleteComboBox)(this.FindName("SiteCombo")));
            this.Remarks_Panel = ((System.Windows.Controls.StackPanel)(this.FindName("Remarks_Panel")));
            this.textBox = ((System.Windows.Controls.TextBox)(this.FindName("textBox")));
            this.Remarks_Panel_Copy = ((System.Windows.Controls.StackPanel)(this.FindName("Remarks_Panel_Copy")));
            this.textBox1 = ((System.Windows.Controls.TextBox)(this.FindName("textBox1")));
            this.textBox2 = ((System.Windows.Controls.TextBox)(this.FindName("textBox2")));
            this.Remarks_Panel_Copy1 = ((System.Windows.Controls.StackPanel)(this.FindName("Remarks_Panel_Copy1")));
            this.textBox3 = ((System.Windows.Controls.TextBox)(this.FindName("textBox3")));
            this.Btn_Panel = ((System.Windows.Controls.StackPanel)(this.FindName("Btn_Panel")));
            this.Confirm = ((System.Windows.Controls.Button)(this.FindName("Confirm")));
            this.SaveAsDraft = ((System.Windows.Controls.Button)(this.FindName("SaveAsDraft")));
            this.Cancel = ((System.Windows.Controls.Button)(this.FindName("Cancel")));
            this.hyperlinkButton = ((System.Windows.Controls.HyperlinkButton)(this.FindName("hyperlinkButton")));
        }
    }
}

