﻿using System;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Collections.Generic;
using Contruction.Web;

namespace Contruction
{
    public partial class Base : ViewModel
    {
        public string CurrentUserName { get; set; }

        public User User
        {
            get { return null; }
            //get { return WebContext.Current.User; }
        }

        public bool IsAuthenticated
        {
            get { return IsLoggedIn; }
        }

        public void NotifyOnLogin()
        {
            string[] keys = new string[] { "IsAuthenticated", "IsLoggedIn" };
            Notify("IsLoggedIn");
            Notify("IsAuthenticated");
        }

        public bool CanEmailPO
        {
            get
            {
                return CanApprovePO;
            }
        }

        public bool CanApprovePO
        {
            get { return IsInAnyRole(new string[] { "dba", "admin", "Purchasing Manager", "POApprove" }); }
        }

        public bool CanApproveMI
        {
            get { return IsInAnyRole(new string[] { "dba", "admin", "Purchasing Manager", "MIApprove" }); }
        }

        public bool CanVerifyMI
        {
            get { return IsInAnyRole(new string[] { "dba", "admin", "Purchasing Manager", "MIVerify" }); }
        }

        public bool CanRaisePO
        {
            get { return IsInAnyRole(new string[] { "dba", "admin", "Purchasing Manager", "Purchaser" }); }
        }

        public bool CanManageUsers
        {
            get
            {
                return IsInAnyRole(new string[] { "dba" }) || UserName == "dba";
            }
        }

        public bool CanViewAllSites
        {
            get
            {
                return IsInAnyRole(new string[] { "dba", "admin", "store", "all sites" });
            }
        }

        public bool CanAdmin
        {
            get
            {
                return IsInAnyRole(new string[] { "dba", "admin" });
            }
        }

        List<int> _MySiteIDs = new List<int>();
        public List<int> MySiteIDs
        {
            get { return _MySiteIDs; }
            set
            {
                _MySiteIDs = value;
                SetMySites();
                Notify("MySiteIDs");
                Notify("MySites");
            }
        }

        void SetMySites()
        {
            MySites = new System.Collections.ObjectModel.ObservableCollection<SIMS.MM.MODEL.MSite>();
            Sites.Where(x => MySiteIDs.Contains(x.ID)).ToList().ForEach(x => MySites.Add(x));
        }

        //public int[] MySiteIDs
        //{
        //    get
        //    {
        //        if (Username == "sachin")
        //            return new int[] { 9,25,28, 29, 3, 37,2,39,47, 50 };
        //        if (Username == "nitish")
        //            return new int[] { 17, 18, 29,49 };
        //        if (Username == "rajeev")
        //            return new int[] { 6, 19, 29, 33, 51 };
        //        if (Username == "nitesh")
        //            return new int[] { 13, 10, 29, 36 };
        //        if (Username == "sushil")
        //            return new int[] { 22, 29, 43 };
        //        //if (Username == "verma")
        //        //    return new int[] { 8, 29 };
        //        if (Username == "kuldeep")
        //            return new int[] { 15,27, 29 };
        //        if (Username == "kuldeep.yadav")
        //            return new int[] { 17, 18, 29 };
        //        return new int[]{0};
        //    }
        //}
    }
}
