﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace Contruction
{
    public class PRRequestArgs : EventArgs
    {
        public int PRID { get; set; }
        public PRRequestArgs(int _PRID) { PRID = _PRID; }
    }

    public class PORequestedArgs : EventArgs
    {
        public int POID { get; set; }
        public PORequestedArgs(int _POID) { POID = _POID; }
    }

    public class MTRRequestedArgs : EventArgs
    {
        public int MTRID { get; set; }
        public MTRRequestedArgs(int _MTRID) { MTRID = _MTRID; }
    }

    public class MORequestedArgs : EventArgs
    {
        public int MOID { get; set; }
        public MORequestedArgs(int _MOID) { MOID = _MOID; }
    }

    public class MRRequestedArgs : EventArgs
    {
        public int MRID { get; set; }
        public MRRequestedArgs(int _MRID) { MRID = _MRID; }
    }

    public class ReceivingRequestedArgs : EventArgs
    {
        public int GRNID { get; set; }
        public ReceivingRequestedArgs(int _GRNID) { GRNID = _GRNID; }
    }

    public class CashPurchaseRequestedArgs : EventArgs
    {
        public int CashReceivingID { get; set; }
        public CashPurchaseRequestedArgs(int _CashReceivingID) { CashReceivingID = _CashReceivingID; }
    }

    public class DirectPurchaseRequestedArgs : EventArgs
    {
        public int DirectReceivingID { get; set; }
        public DirectPurchaseRequestedArgs(int _DirectReceivingID) { DirectReceivingID = _DirectReceivingID; }
    }

    public class ConsumptionRequestedArgs : EventArgs
    {
        public int ConsumptionID { get; set; }
        public ConsumptionRequestedArgs(int _ConsumptionID) { ConsumptionID = _ConsumptionID; }
    }

    public class AppEvents
    {
        static AppEvents _Current = new AppEvents();
        public static AppEvents Current
        {
            get { return _Current; }
            set
            {
                _Current = value;
            }
        }

        public AppEvents()
        {
            OnInit();
        }

        private void OnInit()
        {
            NewPRRaised += new EventHandler(AppEvents_NewPRRaised);
            NewReceivingAdded += new EventHandler(AppEvents_NewReceivingAdded);
            NewCashPurchaseAdded += new EventHandler(AppEvents_NewCashPurchaseAdded);
            NewDirectPurchaseAdded += new EventHandler(AppEvents_NewDirectPurchaseAdded);
            NewConsumptionAdded += new EventHandler(AppEvents_NewConsumptionAdded);
            NewMTRAdded += new EventHandler(AppEvents_NewMTRAdded);
            NewItemAdded += new EventHandler(AppEvents_NewItemAdded);
            NewSiteAdded += new EventHandler(AppEvents_NewSiteAdded);
            NewVendorAdded += new EventHandler(AppEvents_NewVendorAdded);
            NewPricingAdded += new EventHandler(AppEvents_NewPricingAdded);
            NewMOAdded += new EventHandler(AppEvents_NewMOAdded);
            NewMRAdded += new EventHandler(AppEvents_NewMRAdded);
            NewTransporterAdded += new EventHandler(AppEvents_NewTransporterAdded);
            POConverted += new EventHandler<PORequestedArgs>(AppEvents_POConverted);
            NewBillAdded += new EventHandler(AppEvents_NewBillAdded);
            NewVendorPaymentAdded += new EventHandler(AppEvents_NewVendorPaymentAdded);
            NewCompanyAdded += new EventHandler(AppEvents_NewCompanyAdded);
        }

        void AppEvents_NewCompanyAdded(object sender, EventArgs e)
        {
            RaiseCompanyDataChanged();
            Base.Redirect("");
        }

        void AppEvents_NewVendorPaymentAdded(object sender, EventArgs e)
        {
            RaiseVendorPaymentDataChanged();
            Base.Redirect("AllVendorPayments");
        }

        void AppEvents_NewBillAdded(object sender, EventArgs e)
        {
            RaiseBillingDataChanged();
            Base.Current.IsBusy = false;
            Base.Redirect("AllBillings");
        }

        void AppEvents_NewPricingAdded(object sender, EventArgs e)
        {
            RaisePricingDataChanged();
            Base.Redirect("AllPricings");
        }

        void AppEvents_NewMRAdded(object sender, EventArgs e)
        {
            RaiseMRDataChanged();
            Base.Redirect("AllMRs");
        }

        void AppEvents_NewTransporterAdded(object sender, EventArgs e)
        {
            RaiseTransporterdataChanged();
            Base.Redirect("AllTransporters");
        }

        void AppEvents_NewMOAdded(object sender, EventArgs e)
        {
            RaiseMODataChanged();
            Base.Redirect("AllMOs");
        }

        void AppEvents_POConverted(object sender, PORequestedArgs e)
        {
            RaisePRDataChanged();
            RaisePODataChanged();
        }

        void AppEvents_NewVendorAdded(object sender, EventArgs e)
        {
            RaiseVendorDataChanged();
            Base.Redirect("ShowVendors");
        }

        void AppEvents_NewSiteAdded(object sender, EventArgs e)
        {
            RaiseSiteDataChanged();
            //Base.Redirect("AllSites");
            Base.Redirect("SitesandClients");
        }

        void AppEvents_NewItemAdded(object sender, EventArgs e)
        {
            RaiseItemDataChanged();
            Base.Redirect("ViewItems");
        }

        void AppEvents_NewMTRAdded(object sender, EventArgs e)
        {
            RaiseMTRDataChanged();
            Base.Redirect("AllMTRs");
        }

        void AppEvents_NewConsumptionAdded(object sender, EventArgs e)
        {
            RaiseConsumptionDataChanged();
            Base.Redirect("AllConsumptions");
        }

        void AppEvents_NewDirectPurchaseAdded(object sender, EventArgs e)
        {
            RaiseDirectPurchaseDataChanged();
            Base.Redirect("ViewDirectPurchases");
        }

        void AppEvents_NewCashPurchaseAdded(object sender, EventArgs e)
        {
            RaiseCashPurchaseDataChanged();
            Base.Redirect("AllCashPurchases");
        }

        void AppEvents_NewReceivingAdded(object sender, EventArgs e)
        {
            RaiseReceivingDataChanged();
            Base.Redirect("AllReceivings");
        }

        void AppEvents_NewPRRaised(object sender, EventArgs e)
        {
            RaisePRDataChanged();
            Base.Redirect("ViewPRs");
        }




        public event EventHandler NewPRRaised;
        public void RaiseNewPRRaised()
        {
            if (NewPRRaised != null)
                NewPRRaised(this, new EventArgs());
        }

        public event EventHandler PRDataChanged;
        public void RaisePRDataChanged()
        {
            if (PRDataChanged != null)
                PRDataChanged(this, new EventArgs());
        }



        public event EventHandler NewMURaised;
        public void RaiseNewMURaised()
        {
            if (NewMURaised != null)
                NewMURaised(this, new EventArgs());
        }

        public event EventHandler MUDataChanged;
        public void RaiseMUDataChanged()
        {
            if (MUDataChanged != null)
                MUDataChanged(this, new EventArgs());
        }



        public event EventHandler<PORequestedArgs> POConverted;
        public void RaisePOConverted(int poid)
        {
            if (POConverted != null)
                POConverted(this, new PORequestedArgs(poid));
        }

        public event EventHandler PODataChanged;
        public void RaisePODataChanged()
        {
            if (PODataChanged != null)
                PODataChanged(this, new EventArgs());
        }






        public event EventHandler NewReceivingAdded;
        public void RaiseNewReceivingAdded()
        {
            if (NewReceivingAdded != null)
                NewReceivingAdded(this, new EventArgs());
        }

        public event EventHandler ReceivingDataChanged;
        public void RaiseReceivingDataChanged()
        {
            if (ReceivingDataChanged != null)
                ReceivingDataChanged(this, new EventArgs());
        }



        public event EventHandler NewCashPurchaseAdded;
        public void RaiseNewCashPurchaseAdded()
        {
            if (NewCashPurchaseAdded != null)
                NewCashPurchaseAdded(this, new EventArgs());
        }

        public event EventHandler CashPurchseDataChanged;
        public void RaiseCashPurchaseDataChanged()
        {
            if (CashPurchseDataChanged != null)
                CashPurchseDataChanged(this, new EventArgs());
        }



        public event EventHandler NewDirectPurchaseAdded;
        public void RaiseNewDirectPurchaseAdded()
        {
            if (NewDirectPurchaseAdded != null)
                NewDirectPurchaseAdded(this, new EventArgs());
        }

        public event EventHandler DirectPurchaseDataChanged;
        public void RaiseDirectPurchaseDataChanged()
        {
            if (DirectPurchaseDataChanged != null)
                DirectPurchaseDataChanged(this, new EventArgs());
        }



        public event EventHandler NewConsumptionAdded;
        public void RaiseNewConsumptionAdded()
        {
            if (NewConsumptionAdded != null)
                NewConsumptionAdded(this, new EventArgs());
        }

        public event EventHandler ConsumptionDataChanged;
        public void RaiseConsumptionDataChanged()
        {
            if (ConsumptionDataChanged != null)
                ConsumptionDataChanged(this, new EventArgs());
        }



        public event EventHandler NewMTRAdded;
        public void RaiseNewMTRAdded()
        {
            if (NewMTRAdded != null)
                NewMTRAdded(this, new EventArgs());
        }

        public event EventHandler MTRDataChanged;
        public void RaiseMTRDataChanged()
        {
            if (MTRDataChanged != null)
                MTRDataChanged(this, new EventArgs());
        }




        public event EventHandler NewMOAdded;
        public void RaiseNewMOAdded()
        {
            if (NewMOAdded != null)
                NewMOAdded(this, new EventArgs());
        }

        public event EventHandler MODataChanged;
        public void RaiseMODataChanged()
        {
            if (MODataChanged != null)
                MODataChanged(this, new EventArgs());
        }



        public event EventHandler NewMRAdded;
        public void RaiseNewMRAdded()
        {
            if (NewMRAdded != null)
                NewMRAdded(this, new EventArgs());
        }

        public event EventHandler MRDataChanged;
        public void RaiseMRDataChanged()
        {
            if (MRDataChanged != null)
                MRDataChanged(this, new EventArgs());
        }


        public event EventHandler NewVendorAdded;
        public void RaiseNewVendorAdded()
        {
            if (NewVendorAdded != null)
                NewVendorAdded(this, new EventArgs());
        }

        public event EventHandler VendorDataChanged;
        public void RaiseVendorDataChanged()
        {
            if (VendorDataChanged != null)
                VendorDataChanged(this, new EventArgs());
        }




        public event EventHandler NewSiteAdded;
        public void RaiseNewSiteAdded()
        {
            if (NewSiteAdded != null)
                NewSiteAdded(this, new EventArgs());
        }

        public event EventHandler SiteDataChanged;
        public void RaiseSiteDataChanged()
        {
            if (SiteDataChanged != null)
                SiteDataChanged(this, new EventArgs());
        }






        public event EventHandler NewBillAdded;
        public void RaiseNewBillAdded()
        {
            if (NewBillAdded != null)
                NewBillAdded(this, new EventArgs());
        }

        public event EventHandler BillingDataChanged;
        public void RaiseBillingDataChanged()
        {
            if (BillingDataChanged != null)
                BillingDataChanged(this, new EventArgs());
        }




        public event EventHandler NewVendorPaymentAdded;
        public void RaiseNewVendorPaymentAdded()
        {
            if (NewVendorPaymentAdded != null)
                NewVendorPaymentAdded(this, new EventArgs());
        }

        public event EventHandler VendorPaymentDataChanged;
        public void RaiseVendorPaymentDataChanged()
        {
            if (VendorPaymentDataChanged != null)
                VendorPaymentDataChanged(this, new EventArgs());
        }





        public event EventHandler NewItemAdded;
        public void RaiseNewItemAdded()
        {
            if (NewItemAdded != null)
                NewItemAdded(this, new EventArgs());
        }

        public event EventHandler ItemDataChanged;
        public void RaiseItemDataChanged()
        {
            if (ItemDataChanged != null)
                ItemDataChanged(this, new EventArgs());
        }
        




        public event EventHandler NewCompanyAdded;
        public void RaiseNewCompanyAdded()
        {
            if (NewCompanyAdded != null)
                NewCompanyAdded(this, new EventArgs());
        }

        public event EventHandler CompanyDataChanged;
        public void RaiseCompanyDataChanged()
        {
            if (CompanyDataChanged != null)
                CompanyDataChanged(this, new EventArgs());
        }






        public event EventHandler NewPricingAdded;
        public void RaiseNewPricingAdded()
        {
            if (NewPricingAdded != null)
                NewPricingAdded(this, new EventArgs());
        }

        public event EventHandler PricingDataChanged;
        public void RaisePricingDataChanged()
        {
            if (PricingDataChanged != null)
                PricingDataChanged(this, new EventArgs());
        }



        public event EventHandler NewTransporterAdded;
        public void RaiseNewTransporterAdded()
        {
            if (NewTransporterAdded != null)
                NewTransporterAdded(this, new EventArgs());
        }

        public event EventHandler TransporterdataChanged;
        public void RaiseTransporterdataChanged()
        {
            if (TransporterdataChanged != null)
                TransporterdataChanged(this, new EventArgs());
        }

        
        public event EventHandler<PRRequestArgs> PRRequested;
        public void RaisePRRequested(int prid)
        {
            if (PRRequested != null)
                PRRequested(this, new PRRequestArgs(prid));
        }

        public event EventHandler<PORequestedArgs> PORequested;
        public void RaisePORequested(int poid)
        {
            if (PORequested != null)
                PORequested(this, new PORequestedArgs(poid));
        }

        public event EventHandler<MTRRequestedArgs> MTRRequested;
        public void RaiseMTRRequested(int mtrid)
        {
            if (MTRRequested != null)
                MTRRequested(this, new MTRRequestedArgs(mtrid));
        }

        public event EventHandler<MORequestedArgs> MORequested;
        public void RaiseMORequested(int moid)
        {
            if (MORequested != null)
                MORequested(this, new MORequestedArgs(moid));
        }

        public event EventHandler<MRRequestedArgs> MRRequested;
        public void RaiseMRRequested(int mrid)
        {
            if (MRRequested != null)
                MRRequested(this, new MRRequestedArgs(mrid));
        }

        public event EventHandler<ReceivingRequestedArgs> GRNRequested;
        public void RaiseGRNRequested(int receivingid)
        {
            if (GRNRequested != null)
                GRNRequested(this, new ReceivingRequestedArgs(receivingid));
        }

        public event EventHandler<CashPurchaseRequestedArgs> CashPurchaseRequested;
        public void RaiseCashPurchaseRequested(int receivingid)
        {
            if (CashPurchaseRequested != null)
                CashPurchaseRequested(this, new CashPurchaseRequestedArgs(receivingid));
        }

        public event EventHandler<DirectPurchaseRequestedArgs> DirectPurchaseRequested;
        public void RaiseDirectPurchaseRequested(int receivingid)
        {
            if (DirectPurchaseRequested != null)
                DirectPurchaseRequested(this, new DirectPurchaseRequestedArgs(receivingid));
        }

        public event EventHandler<ConsumptionRequestedArgs> ConsumptionRequested;
        public void RaiseConsumptionRequested(int receivingid)
        {
            if (ConsumptionRequested != null)
                ConsumptionRequested(this, new ConsumptionRequestedArgs(receivingid));
        }
    }
}
