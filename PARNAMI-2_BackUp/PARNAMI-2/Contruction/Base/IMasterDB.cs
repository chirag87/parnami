﻿using System;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using SIMS.MM.MODEL;
using System.Collections.ObjectModel;
using Contruction.MasterDBService;
using System.Collections.Generic;

namespace Contruction
{
    #region interface IMasterDB

    public interface IMasterDB
    {
        ObservableCollection<MItem> Items { get; set; }

        ObservableCollection<MVendor> Vendors { get; set; }

        ObservableCollection<MSite> Sites { get; set; }

        ObservableCollection<MSite> MySites { get; set; }

        ObservableCollection<MTransporter> Transporters { get; set; }

        ObservableCollection<MPriceMaster> Pricing { get; set; }

        MVendor SelectedVendor { get; set; }

        MItem SelectedItem { get; set; }

        void LoadAll();
        void LoadItems();
        void LoadVendors();
        void LoadSites();
        void LoadMySites();
        void GetVendorByID(int id);
        void GetSiteByID(int id);
        void GetItemByID(int id);
        void Updatesites();
        void UpdateSite();
        void UpdateVendors();
        void UpdateVendor();
        void UpdateItem();
        void LoadTransporters();
        void LoadPricing();
        void LoadCompanies();
        void LoadItemBrands();
        void LoadItemSizes();
    }

    #endregion

    public partial class Base : IMasterDB
    {

        MasterDataServiceClient service = MyServiceFactory.GetMasterDBService();

        #region Events...

        public event EventHandler NewVendorAdded;
        public void RaiseNewVendorAdded()
        {
            if (NewVendorAdded != null)
                NewVendorAdded(this, new EventArgs());
        }

        public event EventHandler NewSiteAdded;
        public void RaiseNewSiteAdded()
        {
            if (NewSiteAdded != null)
                NewSiteAdded(this, new EventArgs());
        }

        public event EventHandler NewItemAdded;
        public void RaiseNewItemAdded()
        {
            if (NewItemAdded != null)
                NewItemAdded(this, new EventArgs());
        }

        public event EventHandler ItemUpdated;
        public void RaiseItemUpdated()
        {
            if (ItemUpdated != null)
                ItemUpdated(this, new EventArgs());
        }

        public event EventHandler VendorDetailUpdated;
        public void RaiseVendorDetailUpdated()
        {
            if (VendorDetailUpdated != null)
                VendorDetailUpdated(this, new EventArgs());
        }

        public event EventHandler SiteDetailUpdated;
        public void RaiseSiteDetailUpdated()
        {
            if (SiteDetailUpdated != null)
                SiteDetailUpdated(this, new EventArgs());
        }

        public event EventHandler DocumentDeleted;
        public void RaiseDocumentDeleted()
        {
            if (DocumentDeleted != null)
                DocumentDeleted(this, new EventArgs());
        }

        #endregion

        #region Properties...

        #region LoadAll Properties

        ObservableCollection<MItem> _Items = new ObservableCollection<MItem>();
        public ObservableCollection<MItem> Items
        {
            get
            {
                return _Items;
            }
            set
            {
                _Items = value;
                Notify("Items");
                Notify("ItemstoShow");
            }
        }

        ObservableCollection<MItemBrand> _ItemBrands = new ObservableCollection<MItemBrand>();
        public ObservableCollection<MItemBrand> ItemBrands
        {
            get { return _ItemBrands; }
            set
            {
                _ItemBrands = value;
                AttachItemBrandObject();
                Notify("ItemBrands");
                Notify("ItemBrandstoShow");
            }
        }

        ObservableCollection<MItemSize> _ItemSizes = new ObservableCollection<MItemSize>();
        public ObservableCollection<MItemSize> ItemSizes
        {
            get { return _ItemSizes; }
            set
            {
                _ItemSizes = value;
                AttachItemSizeObject();
                Notify("ItemSizes");
                Notify("ItemSizestoShow");
            }
        }

        ObservableCollection<MVendor> _Vendors = new ObservableCollection<MVendor>();
        public ObservableCollection<MVendor> Vendors
        {
            get
            {
                return _Vendors;
            }
            set
            {
                _Vendors = value;
                Notify("Vendors");
                Notify("VendorstoShow");
            }
        }

        ObservableCollection<MSite> _Sites = new ObservableCollection<MSite>();
        public ObservableCollection<MSite> Sites
        {
            get
            {
                return _Sites;
            }
            set
            {
                _Sites = value;
                Notify("Sites");
                Notify("SitestoShow");
            }
        }

        ObservableCollection<MClient> _Clients = new ObservableCollection<MClient>();
        public ObservableCollection<MClient> Clients
        {
            get { return _Clients; }
            set
            {
                _Clients = value;
                Notify("Clients");
                Notify("ClientstoShow");
            }
        }

        ObservableCollection<MSite> _MySites = new ObservableCollection<MSite>();
        public ObservableCollection<MSite> MySites
        {
            get
            {
                return _MySites;
            }
            set
            {
                _MySites = value;
            }
        }

        ObservableCollection<MTransporter> _Transporters = new ObservableCollection<MTransporter>();
        public ObservableCollection<MTransporter> Transporters
        {
            get { return _Transporters; }
            set
            {
                _Transporters = value;
                Notify("Transporters");
                Notify("TransporterstoShow");
            }
        }

        ObservableCollection<MPriceMaster> _Pricing = new ObservableCollection<MPriceMaster>();
        public ObservableCollection<MPriceMaster> Pricing
        {
            get { return _Pricing; }
            set
            {
                _Pricing = value;
                Notify("Pricing");
                Notify("PricestoShow");
            }
        }

        ObservableCollection<MMeasurementUnit> _GetAllMUs = new ObservableCollection<MMeasurementUnit>();
        public ObservableCollection<MMeasurementUnit> GetAllMUs
        {
            get { return _GetAllMUs; }
            set
            {
                _GetAllMUs = value;
                Notify("GetAllMUs");
                Notify("MUstoShow");
            }
        }

        ObservableCollection<string> _MUTypes = new ObservableCollection<string>();
        public ObservableCollection<string> MUTypes
        {
            get { return _MUTypes; }
            set
            {
                _MUTypes = value;
                Notify("MUTypes");
            }
        }

        ObservableCollection<MCompany> _Companies = new ObservableCollection<MCompany>();
        public ObservableCollection<MCompany> Companies
        {
            get { return _Companies; }
            set
            {
                _Companies = value;
                Notify("Companies");
                Notify("CompaniestoShow");
            }
        }

        ObservableCollection<MDocument> _DocumentsByForID = new ObservableCollection<MDocument>();
        public ObservableCollection<MDocument> DocumentsByForID
        {
            get { return _DocumentsByForID; }
            set
            {
                _DocumentsByForID = value;
                Notify("DocumentsByForID");
            }
        }

        #endregion

        #region DefaultType Properties

        IEnumerable<string> _PurchaseTypes;
        public IEnumerable<string> PurchaseTypes
        {
            get { return _PurchaseTypes; }
            set
            {
                _PurchaseTypes = value;
                Notify("PurchaseTypes");
            }
        }

        IEnumerable<string> _BUTypes;
        public IEnumerable<string> BUTypes
        {
            get { return _BUTypes; }
            set
            {
                _BUTypes = value;
                Notify("BUTypes");
            }
        }

        IEnumerable<string> _ConsumableTypes;
        public IEnumerable<string> ConsumableTypes
        {
            get { return _ConsumableTypes; }
            set
            {
                _ConsumableTypes = value;
                Notify("ConsumableTypes");
            }
        }

        IEnumerable<string> _AddressTypes;
        public IEnumerable<string> AddressTypes
        {
            get { return _AddressTypes; }
            set
            {
                _AddressTypes = value;
                Notify("AddressTypes");
            }
        }

        IEnumerable<string> _OwnerType;
        public IEnumerable<string> OwnerType
        {
            get { return _OwnerType; }
            set
            {
                _OwnerType = value;
                Notify("OwnerType");
            }
        }

        IEnumerable<string> _DocumentType;
        public IEnumerable<string> DocumentType
        {
            get { return _DocumentType; }
            set
            {
                _DocumentType = value;
                Notify("DocumentType");
            }
        }

        IEnumerable<string> _DealType;
        public IEnumerable<string> DealType
        {
            get { return _DealType; }
            set
            {
                _DealType = value;
                Notify("DealType");
            }
        }

        IEnumerable<string> _PaymentMode;
        public IEnumerable<string> PaymentMode
        {
            get { return _PaymentMode; }
            set
            {
                _PaymentMode = value;
                Notify("PaymentMode");
            }
        }

        IEnumerable<string> _DiscountType;
        public IEnumerable<string> DiscountType
        {
            get { return _DiscountType; }
            set
            {
                _DiscountType = value;
                Notify("DiscountType");
            }
        }

        IEnumerable<string> _ReceivingType;
        public IEnumerable<string> ReceivingType
        {
            get { return _ReceivingType; }
            set
            {
                _ReceivingType = value;
                Notify("ReceivingType");
            }
        }

        IEnumerable<string> _BillsFilter;
        public IEnumerable<string> BillsFilter
        {
            get { return _BillsFilter; }
            set
            {
                _BillsFilter = value;
                Notify("BillsFilter");
            }
        }

        #endregion

        #region Single Object Properties

        MVendor _SelectedVendor = new MVendor();
        public MVendor SelectedVendor
        {
            get { return _SelectedVendor; }
            set
            {
                _SelectedVendor = value;
                Notify("SelectedVendor");
            }
        }

        MItem _SelectedItem = new MItem();
        public MItem SelectedItem
        {
            get { return _SelectedItem; }
            set
            {
                _SelectedItem = value;
                Notify("SelectedItem");
            }
        }

        MSite _SelectedSite = new MSite();
        public MSite SelectedSite
        {
            get { return _SelectedSite; }
            set
            {
                _SelectedSite = value;
                Notify("SelectedSite");
            }
        }

        int? _VendorID;
        public int? VendorID
        {
            get { return _VendorID; }
            set
            {
                _VendorID = value;
                Notify("VendorID");
            }
        }

        int? _SiteID;
        public int? SiteID
        {
            get { return _SiteID; }
            set
            {
                _SiteID = value;
                Notify("SiteID");
            }
        }

        int? _ItemID;
        public int? ItemID
        {
            get { return _ItemID; }
            set
            {
                _ItemID = value;
                Notify("ItemID");
            }
        }

        #endregion

        #region Search Object Properties

        string _SearchVendor = "";
        public string SearchVendor
        {
            get { return _SearchVendor; }
            set
            {
                _SearchVendor = value;
                Notify("SearchVendor");
                Notify("VendorstoShow");
            }
        }

        string _SearchItem = "";
        public string SearchItem
        {
            get { return _SearchItem; }
            set
            {
                _SearchItem = value;
                Notify("SearchItem");
                Notify("ItemstoShow");
            }
        }

        string _SearchSite = "";
        public string SearchSite
        {
            get { return _SearchSite; }
            set
            {
                _SearchSite = value;
                Notify("SearchSite");
                Notify("SitestoShow");
            }
        }

        string _SearchClient = "";
        public string SearchClient
        {
            get { return _SearchClient; }
            set
            {
                _SearchClient = value;
                Notify("SearchClient");
                Notify("ClientstoShow");
            }
        }

        string _SearchTransporter = "";
        public string SearchTransporter
        {
            get { return _SearchTransporter; }
            set
            {
                _SearchTransporter = value;
                Notify("SearchTransporter");
                Notify("TransporterstoShow");
            }
        }

        string _SearchPricing = "";
        public string SearchPricing
        {
            get { return _SearchPricing; }
            set
            {
                _SearchPricing = value;
                Notify("SearchPricing");
                Notify("PricestoShow");
            }
        }

        string _SearchMU = "";
        public string SearchMU
        {
            get { return _SearchMU; }
            set
            {
                _SearchMU = value;
                Notify("SearchMU");
                Notify("MUstoShow");
            }
        }

        string _SearchItemSize = "";
        public string SearchItemSize
        {
            get { return _SearchItemSize; }
            set
            {
                _SearchItemSize = value;
                Notify("SearchItemSize");
                Notify("ItemSizestoShow");
            }
        }

        string _SearchItemBrand = "";
        public string SearchItemBrand
        {
            get { return _SearchItemBrand; }
            set
            {
                _SearchItemBrand = value;
                Notify("SearchItemBrand");
                Notify("ItemBrandstoShow");
            }
        }

        string _SearchCompany = "";
        public string SearchCompany
        {
            get { return _SearchCompany; }
            set
            {
                _SearchCompany = value;
                Notify("SearchCompany");
                Notify("CompaniestoShow");
            }
        }

        #endregion

        #region IsNew

        bool _IsNew = false;
        public bool IsNew
        {
            get { return _IsNew; }
            set
            {
                _IsNew = value;
                Notify("IsNew");
            }
        }

        #endregion

        #endregion

        #region Default Type Methods

        public void SetDocumentType()
        {
            DocumentType = (new string[] { "Agreement", "Contract", "Image File", "Related Document" });
        }

        public void SetBUType()
        {
            BUTypes = (new string[] { "Office", "Site", "Store"});
        }

        public void SetOwnerType()
        {
            OwnerType = (new string[] { "CEO", "MD", "OWNER" });
        }

        void SetAddressTypes()
        {
            AddressTypes = (new string[] { "Office Address", "Residential Address", "Billing Address", "Permanant Address", "Postal Address", "Registered Address", "Site Address", "Vendor Address" });
        }

        void SetPurchaseTypes()
        {
            PurchaseTypes = (new string[] { "BuildingMaterial", "Consumables", "SubContract", "Services"});
        }

        void SetConsumableTypes()
        {
            ConsumableTypes = (new string[] { "Consumable", "Returnable"});
        }

        void SetDealTypes()
        {
            DealType = (new string[] { "Fixed", "PerDeal" });
        }

        void SetPaymentMode()
        {
            PaymentMode = (new string[] { "Cash", "Cheque", "DD", "NEFT" });
        }

        void SetDiscountType()
        {
            DiscountType = (new string[] { "%", "Rs." });
        }

        void SetReceivingType()
        {
            ReceivingType = (new string[] { "Direct", "GRN", "ALL" });
        }

        void SetBillsFilter()
        {
            BillsFilter = (new string[] { "All", "Approved (Pending Payment)", "Draft", "Paid", "Verified" });
        }

        #endregion

        #region Constructor()

        public void OnInitForMasterDB()
        {
            service.GetAllItemsCompleted += new EventHandler<GetAllItemsCompletedEventArgs>(service_GetAllItemsCompleted);
            service.GetAllSitesCompleted += new EventHandler<GetAllSitesCompletedEventArgs>(service_GetAllSitesCompleted);
            service.GetAllVendorsCompleted += new EventHandler<GetAllVendorsCompletedEventArgs>(service_GetAllVendorsCompleted);
            service.GetAllTransportersCompleted += new EventHandler<GetAllTransportersCompletedEventArgs>(service_GetAllTransportersCompleted);
            service.GetAllPricingCompleted += new EventHandler<GetAllPricingCompletedEventArgs>(service_GetAllPricingCompleted);
            service.GetAllMUCompleted += new EventHandler<GetAllMUCompletedEventArgs>(service_GetAllMUCompleted);
            service.GetAllMUTypesCompleted += new EventHandler<GetAllMUTypesCompletedEventArgs>(service_GetAllMUTypesCompleted);
            service.GetAllCompaniesCompleted += new EventHandler<GetAllCompaniesCompletedEventArgs>(service_GetAllCompaniesCompleted);
            service.GetAllItemBrandsCompleted += new EventHandler<GetAllItemBrandsCompletedEventArgs>(service_GetAllItemBrandsCompleted);
            service.GetAllItemSizesCompleted += new EventHandler<GetAllItemSizesCompletedEventArgs>(service_GetAllItemSizesCompleted);
            service.GetAllClientsCompleted += new EventHandler<GetAllClientsCompletedEventArgs>(service_GetAllClientsCompleted);

            service.UpdateItemCompleted += new EventHandler<UpdateItemCompletedEventArgs>(service_UpdateItemCompleted);
            service.SaveImageCompleted += new EventHandler<System.ComponentModel.AsyncCompletedEventArgs>(service_SaveImageCompleted);
            service.UpdateSitesCompleted += new EventHandler<System.ComponentModel.AsyncCompletedEventArgs>(service_UpdateSitesCompleted);
            service.UpdateSiteCompleted += new EventHandler<UpdateSiteCompletedEventArgs>(service_UpdateSiteCompleted);
            service.UpdateVendorsCompleted += new EventHandler<System.ComponentModel.AsyncCompletedEventArgs>(service_UpdateVendorsCompleted);
            service.UpdateVendorCompleted += new EventHandler<UpdateVendorCompletedEventArgs>(service_UpdateVendorCompleted);
            service.UpdateTransporterCompleted += new EventHandler<System.ComponentModel.AsyncCompletedEventArgs>(service_UpdateTransporterCompleted);
            service.UpdateItemSizesCompleted += new EventHandler<System.ComponentModel.AsyncCompletedEventArgs>(service_UpdateItemSizesCompleted);
            service.UpdateItemBrandsCompleted += new EventHandler<System.ComponentModel.AsyncCompletedEventArgs>(service_UpdateItemBrandsCompleted);
            service.UpdateItemSizeCompleted += new EventHandler<UpdateItemSizeCompletedEventArgs>(service_UpdateItemSizeCompleted);
            service.UpdateItemBrandCompleted += new EventHandler<UpdateItemBrandCompletedEventArgs>(service_UpdateItemBrandCompleted);

            service.GetItembyIDCompleted += new EventHandler<GetItembyIDCompletedEventArgs>(service_GetItembyIDCompleted);
            service.GetVendorbyIDCompleted += new EventHandler<GetVendorbyIDCompletedEventArgs>(service_GetVendorbyIDCompleted);
            service.GetSitebyIDCompleted += new EventHandler<GetSitebyIDCompletedEventArgs>(service_GetSitebyIDCompleted);
            service.GetDocumentsbyForIDCompleted += new EventHandler<GetDocumentsbyForIDCompletedEventArgs>(service_GetDocumentsbyForIDCompleted);

            service.DeleteDocumentCompleted += new EventHandler<System.ComponentModel.AsyncCompletedEventArgs>(service_DeleteDocumentCompleted);

            SetPurchaseTypes();
            SetBUType();
            SetConsumableTypes();
            SetAddressTypes();
            SetOwnerType();
            SetDocumentType();
            SetDealTypes();
            SetPaymentMode();
            SetDiscountType();
            SetReceivingType();
            SetBillsFilter();
            LoadAll();

            Base.AppEvents.ItemDataChanged += new EventHandler(AppEvents_ItemDataChanged);
            Base.AppEvents.SiteDataChanged += new EventHandler(AppEvents_SiteDataChanged);
            Base.AppEvents.VendorDataChanged += new EventHandler(AppEvents_VendorDataChanged);
            Base.AppEvents.TransporterdataChanged += new EventHandler(AppEvents_TransporterdataChanged);
            Base.AppEvents.PricingDataChanged += new EventHandler(AppEvents_PricingDataChanged);
        }

        #endregion

        #region Completed Events...

        void service_GetAllClientsCompleted(object sender, GetAllClientsCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                Base.ShowError(e.Error);
                IsBusy = false;
            }
            else
            {
                Clients = e.Result;
                IsBusy = false;
            }
        }

        void service_GetAllItemSizesCompleted(object sender, GetAllItemSizesCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                Base.ShowError(e.Error);
                IsBusy = false;
            }
            else
            {
                ItemSizes = e.Result;
                IsBusy = false;
            }
        }

        void service_GetAllItemBrandsCompleted(object sender, GetAllItemBrandsCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                Base.ShowError(e.Error);
                IsBusy = false;
            }
            else
            {
                ItemBrands = e.Result;
                IsBusy = false;
            }
        }

        void service_GetAllCompaniesCompleted(object sender, GetAllCompaniesCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                Base.ShowError(e.Error);
                IsBusy = false;
            }
            else
            {
                Companies = e.Result;
                IsBusy = false;
            }
        }

        void service_DeleteDocumentCompleted(object sender, System.ComponentModel.AsyncCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                Base.ShowError(e.Error);
                IsBusy = false;
            }
            else
            {
                MessageBox.Show("Document Deleted!");
                RaiseDocumentDeleted();
                IsBusy = false;
            }
        }

        void service_GetDocumentsbyForIDCompleted(object sender, GetDocumentsbyForIDCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                Base.ShowError(e.Error);
                IsBusy = false;
            }
            else
            {
                DocumentsByForID = null;
                DocumentsByForID = e.Result;
                IsBusy = false;
            }
        }

        void service_SaveImageCompleted(object sender, System.ComponentModel.AsyncCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                Base.ShowError(e.Error);
            }
        }

        void service_UpdateItemCompleted(object sender, UpdateItemCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                Base.ShowError(e.Error);
                IsBusy = false;
            }
            else
            {
                IsBusy = false;
                MessageBox.Show("Updation Successful !");
                RaiseItemUpdated();
                LoadAll();
            }
        }

        void service_GetItembyIDCompleted(object sender, GetItembyIDCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                Base.ShowError(e.Error);
                IsBusy = false;
            }
            else
            {
                SelectedItem = e.Result;
                RaiseItemsLoaded();
                IsBusy = false;
            }
        }

        #region EventHandling for Item Category

        public class NewEventArgs : EventArgs
        {
            public MItem SelectedItem { get; set; }
            public NewEventArgs(MItem _SelectedItem) { SelectedItem = _SelectedItem; }
        }

        public event EventHandler<NewEventArgs> SelectedItemLoaded;
        public void RaiseSelectedItemLoaded(MItem item)
        {
            if (SelectedItemLoaded != null)
                SelectedItemLoaded(this, new NewEventArgs(item));
        }

        #endregion

        void service_GetVendorbyIDCompleted(object sender, GetVendorbyIDCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                Base.ShowError(e.Error);
                IsBusy = false;
            }
            else
            {
                SelectedVendor = e.Result;
                IsBusy = false;
            }
        }

        void service_GetSitebyIDCompleted(object sender, GetSitebyIDCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                Base.ShowError(e.Error);
                IsBusy = false;
            }
            else
            {
                SelectedSite = e.Result;
                IsBusy = false;
            }
        }

        void service_GetAllMUCompleted(object sender, GetAllMUCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                Base.ShowError(e.Error);
                IsBusy = false;
            }
            else
            {
                GetAllMUs = e.Result;
                IsBusy = false;
            }
        }

        void service_GetAllMUTypesCompleted(object sender, GetAllMUTypesCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                Base.ShowError(e.Error);
                IsBusy = false;
            }
            else
            {
                MUTypes = e.Result;
                IsBusy = false;
            }
        }

        void service_GetAllPricingCompleted(object sender, GetAllPricingCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                Base.ShowError(e.Error);
                IsBusy = false;
            }
            else
            {
                Pricing = e.Result;
                IsBusy = false;
            }
        }

        void service_GetAllTransportersCompleted(object sender, GetAllTransportersCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                Base.ShowError(e.Error);
                IsBusy = false;
            }
            else
            {
                Transporters = e.Result;
                IsBusy = false;
            }
        }

        void service_UpdateItemBrandCompleted(object sender, UpdateItemBrandCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                Base.ShowError(e.Error);
                IsBusy = false;
            }
            else
            {
                IsBusy = false;
                MessageBox.Show("Updation Successful !");
                LoadItemBrands();
                LoadAll();
            }
        }

        void service_UpdateItemSizeCompleted(object sender, UpdateItemSizeCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                Base.ShowError(e.Error);
                IsBusy = false;
            }
            else
            {
                IsBusy = false;
                MessageBox.Show("Updation Successful !");
                LoadItemSizes();
                LoadAll();
            }
        }

        void service_UpdateItemBrandsCompleted(object sender, System.ComponentModel.AsyncCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                Base.ShowError(e.Error);
                IsBusy = false;
            }
            else
            {
                IsBusy = false;
                //fMessageBox.Show("Makes Updation Successful !");
                LoadItemBrands();
                LoadAll();
            }
        }

        void service_UpdateItemSizesCompleted(object sender, System.ComponentModel.AsyncCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                Base.ShowError(e.Error);
                IsBusy = false;
            }
            else
            {
                IsBusy = false;
                //MessageBox.Show("Sizes Updation Successful !");
                LoadItemSizes();
                LoadAll();
            }
        }

        void service_UpdateVendorCompleted(object sender, UpdateVendorCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                Base.ShowError(e.Error);
                IsBusy = false;
            }
            else
            {
                IsBusy = false;
                MessageBox.Show("Updation Successful !");
                LoadVendors();
                LoadAll();
                RaiseVendorDetailUpdated();
            }
        }

        void service_UpdateVendorsCompleted(object sender, System.ComponentModel.AsyncCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                Base.ShowError(e.Error);
                IsBusy = false;
            }
            else
            {
                IsBusy = false;
                //MessageBox.Show("Vendor Updation Successful !");
                LoadVendors();
                LoadAll();
            }
        }

        void service_UpdateSitesCompleted(object sender, System.ComponentModel.AsyncCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                Base.ShowError(e.Error);
                IsBusy = false;
            }
            else
            {
                IsBusy = false;
                MessageBox.Show("Site Updation Successful !");
                LoadSites();
                LoadAll();
            }
        }

        void service_UpdateSiteCompleted(object sender, UpdateSiteCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                Base.ShowError(e.Error);
                IsBusy = false;
            }
            else
            {
                IsBusy = false;
                MessageBox.Show("Updation Successful !");
                LoadSites();
                LoadAll();
            }
        }

        void service_UpdateTransporterCompleted(object sender, System.ComponentModel.AsyncCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                Base.ShowError(e.Error);
                IsBusy = false;
            }
            else
            {
                IsBusy = false;
                LoadTransporters();
                LoadAll();
            }
        }

        void AppEvents_ItemDataChanged(object sender, EventArgs e)
        {
            LoadItems();
        }

        void AppEvents_VendorDataChanged(object sender, EventArgs e)
        {
            LoadVendors();
        }

        void AppEvents_SiteDataChanged(object sender, EventArgs e)
        {
            LoadSites();
        }

        void AppEvents_PricingdataChanged(object sender, EventArgs e)
        {
            LoadPricing();
        }

        void AppEvents_TransporterdataChanged(object sender, EventArgs e)
        {
            LoadTransporters();
        }

        void AppEvents_PricingDataChanged(object sender, EventArgs e)
        {
            LoadPricing();
        }

        void service_GetAllVendorsCompleted(object sender, GetAllVendorsCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                Base.ShowError(e.Error);
                IsBusy = false;
            }
            else
            {
                Vendors = e.Result;
                IsBusy = false;
            }
        }

        void service_GetAllSitesCompleted(object sender, GetAllSitesCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                Base.ShowError(e.Error);
                IsBusy = false;
            }
            else
            {
                Sites = e.Result;
                IsBusy = false;
            }
        }

        void service_GetAllItemsCompleted(object sender, GetAllItemsCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                Base.ShowError(e.Error);
                IsBusy = false;
            }
            else
            {
                Items = e.Result;
                IsBusy = false;
            }
        }

        #endregion
        
        #region Methods...

        #region Load()

        public void LoadAll()
        {
            LoadSites();
            LoadVendors();
            LoadTransporters();
            LoadItems();
            LoadPricing();
            LoadMU();
            LoadMUTypes();
            LoadCompanies();
            LoadItemBrands();
            LoadItemSizes();
            LoadClients();
        }

        public void LoadClients()
        {
            service.GetAllClientsAsync();
            IsBusy = true;
        }

        public void LoadItemBrands()
        {
            service.GetAllItemBrandsAsync();
            IsBusy = true;
        }

        public void LoadItemSizes()
        {
            service.GetAllItemSizesAsync();
            IsBusy = true;
        }

        public void LoadCompanies()
        {
            service.GetAllCompaniesAsync();
            IsBusy = true;
        }

        public void LoadMU()
        {
            service.GetAllMUAsync();
            IsBusy = true;
        }

        public void LoadMUTypes()
        {
            service.GetAllMUTypesAsync();
            IsBusy = true;
        }

        public void LoadSites()
        {
            service.GetAllSitesAsync();
            IsBusy = true;
        }

        public void LoadMySites()
        {
            service.GetAllSitesAsync();
            IsBusy = true;
        }

        public void LoadItems()
        {
            service.GetAllItemsAsync();
            IsBusy = true;
        }

        public void LoadVendors()
        {
            service.GetAllVendorsAsync();
            IsBusy = true;
        }

        public void LoadTransporters()
        {
            service.GetAllTransportersAsync();
            IsBusy = true;
        }

        public void LoadPricing()
        {
            service.GetAllPricingAsync(SearchPricing, VendorID, SiteID, ItemID);
            IsBusy = true;
        }

        #endregion

        #region Get()

        public void GetVendorByID(int id)
        {
            service.GetVendorbyIDAsync(id);
            IsBusy = true;
        }

        public void GetSiteByID(int id)
        {
            service.GetSitebyIDAsync(id);
            IsBusy = true;
        }

        public void GetItemByID(int id)
        {
            service.GetItembyIDAsync(id);
            IsBusy = true;
        }

        public void GetDocumentsByForID(int forid, string docfor)
        {
            service.GetDocumentsbyForIDAsync(forid, docfor);
            IsBusy = true;
        }

        public void DeleteDocument(MDocument doc)
        {
            service.DeleteDocumentAsync(doc);
            IsBusy = true;
        }

        #endregion

        #region Update()

        #region Update ItemBrand
        /*Another method By handling Event */
        private void AttachItemBrandObject()
        {
            foreach (var item in ItemBrands)
            {
                item.BrandChanged += new EventHandler(item_BrandChanged);
            }
        }

        void item_BrandChanged(object sender, EventArgs e)
        {
            var data = sender as MItemBrand;
            service.UpdateItemBrandAsync(data);
        }
        #endregion

        #region Update ItemSize
        /*Another method By handling Event */
        private void AttachItemSizeObject()
        {
            foreach (var item in ItemSizes)
            {
                item.SizeChanged += new EventHandler(item_SizeChanged);
            }
        }

        void item_SizeChanged(object sender, EventArgs e)
        {
            var data = sender as MItemSize;
            service.UpdateItemSizeAsync(data);
        }
        #endregion

        public void UpdateItemSize(MItemSize size)
        {
            service.UpdateItemSizeAsync(size);
            IsBusy = true;
        }

        public void UpdateItemSizes()
        {
            var n = Math.Ceiling(ItemSizes.Count / 50);
            ObservableCollection<MItemSize> list1 = new ObservableCollection<MItemSize>();
            for (int i = 0; i < n; i++)
            {
                list1.Clear();
                ItemSizes.Skip(i * 50).Take(50).ToList().ForEach(x => list1.Add(x));
                service.UpdateItemSizesAsync(list1);
            }
            IsBusy = true;
        }

        public void updateItemBrand(MItemBrand brand)
        {
            service.UpdateItemBrandAsync(brand);
            IsBusy = true;
        }

        public void UpdateItemBrands()
        {
            var n = Math.Ceiling(ItemBrands.Count / 50);
            ObservableCollection<MItemBrand> list1 = new ObservableCollection<MItemBrand>();
            for (int i = 0; i < n; i++)
            {
                list1.Clear();
                ItemBrands.Skip(i * 50).Take(50).ToList().ForEach(x => list1.Add(x));
                service.UpdateItemBrandsAsync(list1);
            }
            IsBusy = true;
        }

        public void UpdateVendors()
        {
            var n = Math.Ceiling(Vendors.Count / 50);
            ObservableCollection<MVendor> list1 = new ObservableCollection<MVendor>();
            for (int i = 0; i < n; i++)
            {
                list1.Clear();
                Vendors.Skip(i*50).Take(50).ToList().ForEach(x => list1.Add(x));
                service.UpdateVendorsAsync(list1);
            }
            IsBusy = true;
        }

        public void UpdateVendor()
        {
            service.UpdateVendorAsync(SelectedVendor);
            IsBusy = true;
        }

        public void UpdateItem()
        {
            service.UpdateItemAsync(SelectedItem);
            //service.SaveImageAsync(SelectedItem.PictureName, SelectedItem.ID);
            IsBusy = true;
        }

        public void UpdateItem(MItem selecteditem)
        {
            service.UpdateItemAsync(selecteditem);
            IsBusy = true;
        }

        public void UpdateVendor(MVendor vendor)
        {
            service.UpdateVendorAsync(vendor);
            IsBusy = true;
        }

        public void Updatesites()
        {
            service.UpdateSitesAsync(Sites);
            IsBusy = true;
        }

        public void UpdateSite()
        {
            service.UpdateSiteAsync(SelectedSite);
            IsBusy = true;
        }

        public void UpdateTransporter()
        {
            service.UpdateTransporterAsync(Transporters);
            IsBusy = true;
        }

        #endregion

        #region Undo()

        public void UndoVendor()
        {
            LoadVendors();
        }

        public void UndoSite()
        {
            LoadSites();
        }

        public void UndoTransporter()
        {
            LoadTransporters();
        }

        #endregion

        #region ToShow()

        public IEnumerable<MItem> ItemstoShow
        {
            get {
                if (Items == null) return  new List<MItem>();
                return Items.Where(x => x.Name.ToLower().Contains(SearchItem.ToLower())); }
        }

        public IEnumerable<MVendor> VendorstoShow
        {
            get
            {
                if (Vendors == null) return new List<MVendor>();
                return Vendors.Where(x => x.Name.ToLower().Contains(SearchVendor.ToLower())); //||
                                            //x.Address.ToLower().Contains(SearchVendor.ToLower()) ||
                                            //x.City.ToLower().Contains(SearchVendor.ToLower()) ||
                                            //x.State.ToLower().Contains(SearchVendor.ToLower()) ||
                                            //x.TINNo.ToLower().Contains(SearchVendor.ToLower()) ||
                                            //x.PANNo.ToLower().Contains(SearchVendor.ToLower()) ||
                                            //x.ServiceTaxNo.ToLower().Contains(SearchVendor.ToLower()) ||
                                            //x.Display.ToLower().Contains(SearchVendor.ToLower()));
            }
        }

        public IEnumerable<MSite> SitestoShow
        {
            get
            {
                if (Sites == null) return new List<MSite>(); 
                var tmp = Sites.Where(x => x.Name.ToLower().Contains(SearchSite.ToLower()) ||
                                            x.SiteCode.ToLower().Contains(SearchSite.ToLower())); // ||
                                            //x.Company.ToLower().Contains(SearchSite.ToLower()) ||
                                            //x.Address.ToLower().Contains(SearchSite.ToLower()) ||
                                            //x.City.ToLower().Contains(SearchSite.ToLower()) ||
                                            //x.Display.ToLower().Contains(SearchSite.ToLower()));
                return tmp; }
        }

        public IEnumerable<MClient> ClientstoShow
        {
            get
            {
                if (Clients == null) return new List<MClient>();
                var tmp = Clients.Where(x => x.Name.ToLower().Contains(SearchClient.ToLower())); //||
                                            //x.Address1.ToLower().Contains(SearchClient.ToLower()) ||
                                            //x.Address2.ToLower().Contains(SearchClient.ToLower()) ||
                                            //x.City.ToLower().Contains(SearchClient.ToLower()) ||
                                            //x.State.ToLower().Contains(SearchClient.ToLower()) ||
                                            //x.TINNo.ToLower().Contains(SearchClient.ToLower()) ||
                                            //x.PANNo.ToLower().Contains(SearchClient.ToLower()) ||
                                            //x.ServiceTaxNo.ToLower().Contains(SearchClient.ToLower()) ||
                                            //x.ContactPerson.ToLower().Contains(SearchClient.ToLower())); 
                return tmp;
            }
        }

        public IEnumerable<MTransporter> TransporterstoShow
        {
            get
            {
                if (Transporters == null) return new List<MTransporter>();
                var tmp = Transporters.Where(x => x.Name.ToLower().Contains(SearchTransporter.ToLower())); return tmp;
            }
        }

        public IEnumerable<MPriceMaster> PricestoShow
        {
            get
            {
                if (Pricing == null) return new List<MPriceMaster>();
                var tmp = Pricing.Where(x => x.Vendor.ToLower().Contains(SearchPricing.ToLower()) || 
                                            x.Site.ToLower().Contains(SearchPricing.ToLower()) || 
                                            x.Item.ToLower().Contains(SearchPricing.ToLower())); 
                return tmp;
            }
        }

        public IEnumerable<MMeasurementUnit> MUstoShow
        {
            get
            {
                if (GetAllMUs == null) return new List<MMeasurementUnit>();
                return GetAllMUs.Where(x => x.MU.ToLower().Contains(SearchMU.ToLower()) ||
                                            x.FormalName.ToLower().Contains(SearchMU.ToLower()) ||
                                            x.Type.ToLower().Contains(SearchMU.ToLower()));
            }
        }

        public IEnumerable<MItemSize> ItemSizestoShow
        {
            get
            {
                if (ItemSizes == null) return new List<MItemSize>();
                var tmp = ItemSizes.Where(x => x.Size.ToLower().Contains(SearchItemSize.ToLower())); 
                return tmp;
            }
        }

        public IEnumerable<MItemBrand> ItemBrandstoShow
        {
            get
            {
                if (ItemBrands == null) return new List<MItemBrand>();
                var tmp = ItemBrands.Where(x => x.BrandName.ToLower().Contains(SearchItemBrand.ToLower())); 
                return tmp;
            }
        }

        public IEnumerable<MCompany> CompaniestoShow
        {
            get
            {
                if (Companies == null) return new List<MCompany>();
                return Companies.Where(x => x.Name.ToLower().Contains(SearchCompany.ToLower()) ||
                                            x.Code.ToLower().Contains(SearchCompany.ToLower()) ||
                                            x.AddressLine1.ToLower().Contains(SearchCompany.ToLower())); // ||
                                            //x.AddressLine2.ToLower().Contains(SearchCompany.ToLower()) ||
                                            //x.AddressLine3.ToLower().Contains(SearchCompany.ToLower()) ||
                                            //x.City.ToLower().Contains(SearchCompany.ToLower()) ||
                                            //x.State.ToLower().Contains(SearchCompany.ToLower()) ||
                                            //x.Pincode.ToLower().Contains(SearchCompany.ToLower()) ||
                                            //x.Email.ToLower().Contains(SearchCompany.ToLower()));
            }
        }

        #endregion

        #region Clear()

        public void ClearItemSize()
        {
            SearchItemSize = "";
            LoadItemSizes();
        }

        public void ClearItemBrand()
        {
            SearchItemBrand = "";
            LoadItemBrands();
        }

        public void ClearCompany()
        {
            SearchCompany = "";
            LoadCompanies();
        }

        public void ClearClient()
        {
            SearchClient = "";
            LoadClients();
        }

        public void ClearVendor()
        {
            SearchVendor = "";
            LoadVendors();
        }

        public void ClearItem()
        {
            SearchItem = "";
            LoadItems();
        }

        public void ClearMU()
        {
            SearchMU = "";
            LoadMU();
        }

        public void ClearSite()
        {
            SearchSite = "";
            LoadSites();
        }

        public void ClearPricing()
        {
            SearchPricing = "";
            VendorID = null;
            SiteID = null;
            ItemID = null;
            LoadPricing();
        }

        public void ClearTransporter()
        {
            SearchTransporter = "";
            LoadTransporters();
        }

        #endregion

        #region New()

        public void AddNewVendor()
        {
            NewVendorModel newvendor = new NewVendorModel();
            RaiseNewVendorAdded();
        }

        public void AddNewSite()
        {
            NewSiteModel newsite = new NewSiteModel();
            RaiseNewSiteAdded();
        }

        public void AddNewItem()
        {
            NewItemModel newitem = new NewItemModel();
            RaiseNewItemAdded();
        }

        #endregion

        #endregion
    }
}