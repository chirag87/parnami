﻿using System;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Collections.Generic;

namespace Contruction
{
    public partial class Base
    {
        string _UserName;
        public string UserName
        {
            get { return _UserName; }
            set
            {
                _UserName = value;
                Notify("UserName");
            }
        }

        string[] _Roles;
        public string[] Roles
        {
            get { return _Roles; }
            set
            {
                _Roles = value;
                Notify("Roles");
            }
        }

        bool _IsLoggedIn = false;
        public bool IsLoggedIn
        {
            get { return _IsLoggedIn; }
            set
            {
                _IsLoggedIn = value;
                Notify("IsLoggedIn");
            }
        }

        public bool IsInRole(string role)
        {
            return false; 
            //return WebContext.Current.User.IsInRole(role);
        }

        public bool IsInAnyRole(IEnumerable<string> roles)
        {
            return roles.Any(x => IsInRole(x));
        }

        public bool IsInAnyRole(string[] roles)
        {
            return roles.Any(x => IsInRole(x));
        }
    }

    // Class for managing Roles
    public partial class Base
    {
        //public UserRegistrationContext userRegistrationContext = new UserRegistrationContext();

        //public IEnumerable<UserInfo> UserInfos
        //{
        //    get { return userRegistrationContext.UserInfos; }
        //}

        bool _IsDBA;
        public bool IsDBA
        {
            get { return _IsDBA; }
            set
            {
                _IsDBA = value;
                Notify("IsDBA");
            }
        }

        public void LoadRoles()
        {
            //var opr = userRegistrationContext.GetAllRoles();
            //opr.Completed += (s, e) =>
            //{
            //    if (!opr.HasError)
            //        Roles = opr.Value;
            //};
        }

        //public InvokeOperation CreateRole(string newRole)
        //{
        //    //var opr = userRegistrationContext.CreateRole(newRole);
        //    //opr.Completed += (s, e) =>
        //    //{
        //    //    if (!opr.HasError)
        //    //        Roles = opr.Value;
        //    //};
        //    //return opr;
        //    return null;
        //}

        public void LoadUserInfo()
        {
            LoadRoles();
            //userRegistrationContext.UserInfos.Clear();
            //userRegistrationContext.Load<UserInfo>(userRegistrationContext.GetAllUsersQuery());
        }
    }
    /*
    //// Modules Information
    //public partial class Base
    //{
    //    public ModuleManagementContext moduleManagement = new ModuleManagementContext();

    //    public EntitySet<Module> Modules
    //    {
    //        get { return moduleManagement.Modules; }
    //    }

    //    public LoadOperation<Module> LoadModules()
    //    {
    //        moduleManagement.Modules.Clear();
    //        return moduleManagement.Load<Module>(moduleManagement.GetModulesQuery());
    //    }
    //}

    //// for ModuleRights
    //public partial class Base
    //{
    //    public EntitySet<ModuleRight> ModuleRights
    //    {
    //        get { return moduleManagement.ModuleRights; }
    //    }

    //    public IEnumerable<string> ModuleAccessLevels
    //    {
    //        get { return moduleManagement.ModuleAccessLevels.Select(x => x.Value); }
    //    }

    //    public void InitModuleRights()
    //    {
    //        LoadModuleAccessLevels();
    //        LoadAllRights();
    //    }

    //    public void LoadAllRights()
    //    {
    //        LoadUserInfo();
    //        moduleManagement.ModuleRights.Clear();
    //        moduleManagement.Load<ModuleRight>(moduleManagement.GetModuleRightsQuery());
    //    }

    //    void LoadModuleAccessLevels()
    //    {
    //        moduleManagement.Load<ModuleAccessLevel>(moduleManagement.GetModuleAccessLevelsQuery());
    //    }

    //    public IEnumerable<string> MyRoles
    //    {
    //        get { return WebContext.Current.User.Roles; }
    //    }

    //    public IEnumerable<ModuleRight> MyModulesRights
    //    {
    //        get
    //        {
    //            return Current.ModuleRights.Where(x =>
    //                MyRoles.Count(role => role.Equals(x.Role)) > 0);
    //        }
    //    }

    //    public static AccessLevel CheckAccess(string Module)
    //    {
    //        if (!Base.Current.IsLoggedIn)
    //            return AccessLevel.None;
    //        if (Base.Current.Is_dba)
    //            return AccessLevel.Full;
    //        var list = Base.Current.ModuleRights.
    //             Where(x => x.Name.Equals(Module, StringComparison.OrdinalIgnoreCase));
    //        if (list.Any(x => x.AccessLevel.Equals("Full", StringComparison.OrdinalIgnoreCase)))
    //            return AccessLevel.Full;
    //        if (list.Any(x => x.AccessLevel.Equals("Partial", StringComparison.OrdinalIgnoreCase)))
    //            return AccessLevel.Partial;
    //        return AccessLevel.None;
    //    }

    //    public static bool CheckAccess(string Module, string FeatureName)
    //    {
    //        var list = Base.Current.ModuleRights.
    //             Where(x => x.Name.Equals(Module, StringComparison.OrdinalIgnoreCase));
    //        if (list.Any(x => x.AccessLevel.Equals("Full", StringComparison.OrdinalIgnoreCase)))
    //            return true;
    //        if (list.Any(x => x.AccessLevel.Equals("Partial", StringComparison.OrdinalIgnoreCase)))
    //            return list.Any(x => x.FeatureRights.Any(v => v.FeatureName == FeatureName
    //                && v.CanAccess));
    //        return false;
    //    }
    //}
    */
    public enum AccessLevel
    {
        Full, Partial, None
    }

    public static class StaticExtentions
    {
        //public static InvokeOperation ApproveUser(this UserInfo user)
        //{
        //    if (user.IsApproved)
        //        return null;
        //    return Base.Current.userRegistrationContext.AllowUser(user.Username);
        //}

        //public static InvokeOperation BlockUser(this UserInfo user)
        //{
        //    if (!user.IsApproved)
        //        return null;
        //    return Base.Current.userRegistrationContext.BlockUser(user.Username);
        //}
    }
}
