﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace Contruction
{

    public partial class Base
    {
        //public InventoryContext inventoryService { get; set; }

        public void OnCreatedForInventory()
        {
            //inventoryService = new InventoryContext();
            //inventoryService.PropertyChanged += new System.ComponentModel.PropertyChangedEventHandler(inventoryService_PropertyChanged);
            //inventoryService.AddReference(typeof(Item), ItemsDB);
            //inventoryService.AddReference(typeof(ItemCategory), ItemsDB);
            //inventoryService.AddReference(typeof(Vendor), ItemsDB);
            //inventoryService.AddReference(typeof(BUs), BUDB);
            /*//inventoryService.EntityContainer.AddReference(ItemsDB.Items);
            //inventoryService.AddReference(typeof(PR), POService);
            //inventoryService.AddReference(typeof(PRDetail), POService);*/
        }

        void inventoryService_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {

            if (e.PropertyName == "IsLoading" || e.PropertyName == "IsSubmitting")
                Notify("IsinventoryServiceBusy");
        }

        public bool IsinventoryServiceBusy
        {
            //get { return inventoryService.IsLoading || inventoryService.IsSubmitting; }
            get { return false; }
        }
    }
}
