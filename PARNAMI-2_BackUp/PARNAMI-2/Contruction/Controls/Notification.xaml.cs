﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Contruction.AlertService;
using SIMS.MM.MODEL;
using System.ComponentModel;
using System.Collections.ObjectModel;
using System.Threading;


namespace Contruction
{
    public partial class Notification : UserControl, INotifyPropertyChanged
    {
        public Notification()
        {
            InitializeComponent();
            Base.Current.AlertLoaded += new EventHandler(Current_AlertLoaded);
        }

        void Current_AlertLoaded(object sender, EventArgs e)
        {
            //NewNotifiactionSound.Stop();
            //NewNotifiactionSound.Source = new Uri("~/Media/WindowsExclamation.mp3", UriKind.Relative);
            //NewNotifiactionSound.Play();
            media.Play();
        }

        private void ImagenewNotification_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            Base.Current.IsOffline = true;
            border.Visibility = Visibility.Collapsed;
            Base.Current.IsAlert = false;
        }

        #region INotifyPropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;
        public void Notify(string PropertyName)
        {
            if (PropertyChanged != null) PropertyChanged(this, new PropertyChangedEventArgs(PropertyName));
        }

        #endregion

        private void NewNotifiactionSound_MediaOpened(object sender, RoutedEventArgs e)
        {
            //MediaElement m = (MediaElement)sender;
            //m.Play();
        }
    }
}
