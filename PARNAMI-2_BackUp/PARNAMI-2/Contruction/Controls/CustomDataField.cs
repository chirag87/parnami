﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Collections;
using System.Windows.Data;

namespace Contruction
{
    public class CustomDataField : DataField
    {
        public CustomDataField()
        {
            HorizontalAlignment = System.Windows.HorizontalAlignment.Stretch;
            VerticalAlignment = System.Windows.VerticalAlignment.Center;
        }

        public ContentTypes ContentType
        {
            get { return (ContentTypes)GetValue(ContentTypeProperty); }
            set
            {
                SetValue(ContentTypeProperty, value);
                ResetBinding();
            }
        }

        public IEnumerable ItemsSource
        {
            get
            {
                return (IEnumerable)GetValue(ItemsSourceProperty);
            }
            set
            {
                SetValue(ItemsSourceProperty, value);
                ResetBinding();
            }
        }

        public static readonly DependencyProperty ItemsSourceProperty = DependencyProperty.Register
            ("ItemsSource", typeof(IEnumerable), typeof(CustomDataField), new PropertyMetadata(null));

        object _SelectedObject;
        public object SelectedObject
        {
            get { return _SelectedObject; }
            set
            {
                _SelectedObject = value;
                ResetBinding();
            }
        }

        object _SelectedValue;
        public object SelectedValue
        {
            get { return _SelectedValue; }
            set
            {
                _SelectedValue = value;
                ResetBinding();
            }
        }

        string _SelectedValuePath;
        public string SelectedValuePath
        {
            get { return _SelectedValuePath; }
            set
            {
                _SelectedValuePath = value;
                ResetBinding();
            }
        }

        string _DisplayMemberPath;
        public string DisplayMemberPath
        {
            get { return _DisplayMemberPath; }
            set
            {
                _DisplayMemberPath = value;
                ResetBinding();
            }
        }

        public string LabelText
        {
            get { return this.Label.ToString(); }
            set { this.Label = value; }
        }

        string _ContentBinding;
        public string ContentBinding
        {
            get { return _ContentBinding; }
            set
            {
                _ContentBinding = value;
                ResetBinding();
            }
        }

        HorizontalAlignment ha = HorizontalAlignment.Stretch;
        VerticalAlignment va = VerticalAlignment.Center;

        void ResetBinding()
        {
            if (String.IsNullOrEmpty(ContentBinding))
                return;
            Binding binding = new Binding()
            {
                Path = new System.Windows.PropertyPath(ContentBinding),
                Mode = BindingMode.TwoWay,
                NotifyOnValidationError = true
            };
            switch (ContentType)
            {
                case ContentTypes.TextBox:
                    var obj1 = new TextBox()
                    {
                        HorizontalAlignment = ha,
                        VerticalAlignment = va
                    };
                    obj1.SetBinding(TextBox.TextProperty, binding);
                    this.Content = obj1;
                    break;

                case ContentTypes.ComboBox:
                    var obj2 = new ComboBox()
                    {
                        HorizontalAlignment = ha,
                        VerticalAlignment = va
                    };
                    if (!String.IsNullOrEmpty(SelectedValuePath))
                    {
                        obj2.SelectedValuePath = SelectedValuePath;
                        obj2.SetBinding(ComboBox.SelectedValueProperty, binding);
                    }
                    else
                        obj2.SetBinding(ComboBox.SelectedItemProperty, binding);
                    if (!String.IsNullOrEmpty(DisplayMemberPath))
                        obj2.DisplayMemberPath = DisplayMemberPath;
                    obj2.ItemsSource = ItemsSource;
                    this.Content = obj2;
                    break;

                default:
                    break;
            }
        }

        public static readonly DependencyProperty ContentTypeProperty = DependencyProperty.Register
        ("ContentType", typeof(ContentTypes), typeof(CustomDataField), new PropertyMetadata(ContentTypes.TextBox));
    }

    public enum ContentTypes
    {
        TextBox,
        PasswordBox,
        CheckBox,
        ComboBox,
        RadioButton,
        ListBox
    }
}
