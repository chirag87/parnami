﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace Contruction
{
    public static class StaticExtensions
    {
        public static T ToModel<T>(this UserControl userControl)
        {
            return (T)userControl.DataContext;
        }

        public static PopUp ToPopup(this UserControl uc)
        {
            return PopUp.New(uc);
        }

        public static PopUp ToPopup(this UserControl uc, bool showOkCancel)
        {
            return PopUp.New(uc, showOkCancel);
        }
    }
}
