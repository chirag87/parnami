﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace Contruction.Helpers
{
    public class NavigationButton:Button
    {
        public string Target
        {
            get { return (string)GetValue(TargetProperty); }
            set { SetValue(TargetProperty, value); }
        }
        public static DependencyProperty TargetProperty = DependencyProperty.Register
        ("Target", typeof(string), typeof(NavigationAction), new PropertyMetadata(""));

        public NavigationButton()
        {
            this.Click += new RoutedEventHandler(NavigationButton_Click);
        }

        void NavigationButton_Click(object sender, RoutedEventArgs e)
        {
            Base.Redirect(Target);
        }
    }
}
