﻿using System;
using System.Net;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Windows.Data;
using SIMS.MM.MODEL;
using System.Globalization;

namespace Contruction
{
    public class IDtoSiteConverter : IValueConverter
    {

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            string defval = "NA";
            if (parameter != null)
                defval = parameter.ToString();
            int SiteID = 0;
            if (value == null) return defval;
            if (value is int)
            {
                SiteID = (int)value;
            }
            else if (value is string)
            {
                if (!Int32.TryParse(value.ToString(), out SiteID))
                {
                    return defval;
                }
            }
            else { return defval; }

            try
            {
                MSite site = Base.Current.Sites.Single(x => x.ID == SiteID);
                return site.Name;
            }
            catch { }
            return defval;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
