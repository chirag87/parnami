﻿using System;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using SIMS.MM.MODEL;
using System.Collections.Generic;
using Contruction.ReceivingService;
using System.ComponentModel;
using System.Collections.ObjectModel;
using Contruction.TransactionService;

namespace Contruction
{
    public class NewReceivingModel : ViewModel
    {
        ReceivingServiceClient service = new ReceivingServiceClient();
        TransactionServiceClient newservice = new TransactionServiceClient();

        public NewReceivingModel()
        {
            if (!DesignerProperties.GetIsInDesignMode(Application.Current.RootVisual))
            {
                Reset();
                OnInit();
            }
        }

        #region Properties...

        MRecivingView _NewGRN = new MRecivingView();
        public MRecivingView NewGRN
        {
            get { return _NewGRN; }
            set
            {
                _NewGRN = value;
                Notify("NewGRN");
            }
        }

        //MReceiving _CreatedGRN;
        //public MReceiving CreatedGRN
        //{
        //    get { return _CreatedGRN; }
        //    set
        //    {
        //        _CreatedGRN = value;
        //        Notify("CreatedGRN");
        //    }
        //}

        MGRN _CreatedGRN = new MGRN();
        public MGRN CreatedGRN
        {
            get { return _CreatedGRN; }
            set
            {
                _CreatedGRN = value;
                Notify("CreatedGRN");
            }
        }

        IEnumerable<MItem> _MItems = new List<MItem>();
        public IEnumerable<MItem> MItems
        {
            get { return _MItems; }
            set
            {
                _MItems = value;
                Notify("MItems");
            }
        }

        string _Status;
        public string Status
        {
            get { return _Status; }
            set
            {
                _Status = value;
                Notify("Status");
            }
        }

        bool _IsLocked = true;
        public bool IsLocked
        {
            get
            {
                return _IsLocked;
            }
            set
            {
                _IsLocked = value;
                Notify("IsLocked");
            }
        }

        //ObservableCollection<PendingPOInfo> _BufferList = new ObservableCollection<PendingPOInfo>();
        //public ObservableCollection<PendingPOInfo> BufferList
        //{
        //    get { return _BufferList; }
        //    set
        //    {
        //        _BufferList = value;
        //        Notify("BufferList");
        //    }
        //}

        #endregion

        #region Completed Events...

        //void service_CreateNewReceivingCompleted(object sender, CreateNewReceivingCompletedEventArgs e)
        //{
        //    if (e.Error != null)
        //    {
        //        Base.ShowError(e.Error);
        //        IsBusy = false;
        //    }
        //    else
        //    {
        //        IsBusy = false;
        //        CreatedGRN = e.Result;
        //        MessageBox.Show("Reference No: " + CreatedGRN.ReferenceNo + "\n\nNew GRN Added Successfully !");
        //        Reset();
        //        Base.AppEvents.RaiseNewReceivingAdded();
        //    }
        //}

        void newservice_CreateNewGRNCompleted(object sender, CreateNewGRNCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                Base.ShowError(e.Error);
                IsBusy = false;
            }
            else
            {
                IsBusy = false;
                CreatedGRN = e.Result;
                MessageBox.Show("Reference No: " + CreatedGRN.ReferenceNo + "\n\nNew GRN Added Successfully !");
                Reset();
                Base.AppEvents.RaiseNewReceivingAdded();
            }
        }

        void service_GetAllowedItemIDsCompleted(object sender, GetAllowedItemIDsCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                Base.ShowError(e.Error);
                IsBusy = false;
            }
            else
            {
                MItems = Base.Current.Items.Where(x => e.Result.Contains(x.ID));
                foreach (var line in NewGRN.LinesViews)
                {
                    line.AllowedItems = MItems;
                }
                IsBusy = false;
            }
        }

        //void service_GetPendingPOInfosCompleted(object sender, GetPendingPOInfosCompletedEventArgs e)
        //{
        //    if (e.Error != null)
        //    {
        //        Base.ShowError(e.Error);
        //        IsBusy = false;
        //    }

        //    if (e.Result.Count == 0)
        //    {
        //        IsBusy = false;
        //        return;
        //    }
        //    BufferList = e.Result;  // temporarily storing the pending po info. Data
        //    var TempList = NewGRN.LinesViews.Where(x => x.ItemID == BufferList.First().ItemID);
        //    foreach (var item in TempList)
        //    {
        //        item.PendingInfos = new ObservableCollection<PendingPOInfo>();
        //    }
        //    IsBusy = false;
        //}

        #endregion

        #region Methods...

        public void OnInit()
        {
            //service.CreateNewReceivingCompleted += new EventHandler<CreateNewReceivingCompletedEventArgs>(service_CreateNewReceivingCompleted);
            newservice.CreateNewGRNCompleted += new EventHandler<CreateNewGRNCompletedEventArgs>(newservice_CreateNewGRNCompleted);
            service.GetAllowedItemIDsCompleted += new EventHandler<GetAllowedItemIDsCompletedEventArgs>(service_GetAllowedItemIDsCompleted);
        }

        public void AddNewLine()
        {
            //if (NewGRN.LinesViews.Count == 0)
            //    CallItems();
            var line = NewGRN.AddNewLineView();
            //line.AllowedItems = MItems;
            //line.ItemIDChanged += new EventHandler(line_ItemIDChanged);
            //line.SizeIDChanged += new EventHandler(line_SizeIDChanged);
            //line.BrandIDChanged += new EventHandler(line_BrandIDChanged);
            Notify("NewGRN");
            CanLock();
        }

        public void CallItems()
        {
            if (NewGRN.VendorID == null) return;
            if (NewGRN.SiteID == 0) return;
            service.GetAllowedItemIDsAsync(NewGRN.VendorID.Value, NewGRN.SiteID);
            IsBusy = true;
        }

        //void line_ItemIDChanged(object sender, EventArgs e)
        //{
        //    var item = sender as MReceivingLineView;
        //    service.GetPendingPOInfosCompleted += new EventHandler<GetPendingPOInfosCompletedEventArgs>(service_GetPendingPOInfosCompleted);
        //    if (item.ItemID != 0)
        //    {
        //        service.GetPendingPOInfosAsync(NewGRN.VendorID.Value, NewGRN.SiteID, item.ItemID, item.SizeID, item.BrandID);
        //        IsBusy = true;
        //    }
        //}

        //void line_SizeIDChanged(object sender, EventArgs e)
        //{
        //    var data = sender as MReceivingLineView;
        //    var id = data.SizeID;
        //    var templist = BufferList.Where(x => x.SizeID == id);
        //    BufferList = new ObservableCollection<PendingPOInfo>();
        //    foreach (var i in templist)
        //    {
        //        BufferList.Add(i);
        //    }
        //    //AssignPO(BufferList);
        //}

        //void line_BrandIDChanged(object sender, EventArgs e)
        //{
        //    var data = sender as MReceivingLineView;
        //    var id = data.BrandID;
        //    var templist = BufferList.Where(x => x.BrandID == id);
        //    BufferList = new ObservableCollection<PendingPOInfo>();
        //    foreach (var i in templist)
        //    {
        //        BufferList.Add(i);
        //    }
        //    var TempList = NewGRN.LinesViews.Where(x => x.ItemID == BufferList.First().ItemID);
        //    foreach (var item in TempList)
        //    {
        //        if (BufferList.Count == 1 && BufferList.First().IsBlank == true)
        //        {
        //            item.PendingInfos = new ObservableCollection<PendingPOInfo>();
        //            item.SelectedPOInfo = null;
        //            item.CanEditUnitPrice = true;
        //        }
        //        else
        //        {
        //            //if (BufferList.Distinct().Select(x => x.SizeID).Count() == 1)
        //            //{
        //            //    int? sizeid = BufferList.Select(x => x.SizeID).First();
        //            //    var list = BufferList.Where(x => x.SizeID == sizeid);
        //            //    BufferList = new ObservableCollection<PendingPOInfo>();
        //            //    foreach (var i in list)
        //            //    {
        //            //        BufferList.Add(i);
        //            //    }
        //            //}
        //            //if (BufferList.Distinct().Select(x => x.BrandID).Count() == 1)
        //            //{
        //            //    int? brandid = BufferList.Select(x => x.BrandID).First();
        //            //    var list = BufferList.Where(x => x.BrandID == brandid);
        //            //    BufferList = new ObservableCollection<PendingPOInfo>();
        //            //    foreach (var i in list)
        //            //    {
        //            //        BufferList.Add(i);
        //            //    }
        //            //}
        //            item.PendingInfos = BufferList;
        //            item.CanEditUnitPrice = false;
        //        }
        //    }
        //}

        public void DeleteLine(int lid)
        {
            try
            {
                var line = NewGRN.LinesViews.Single(x => x.LineID == lid);
                NewGRN.DeleteLineView(line);
            }
            catch { }
        }

        public void SubmitToDB()
        {
            if (!Validate()) return;
            //service.CreateNewReceivingAsync(NewGRN.GetMReceiving(), Base.Current.UserName);
            newservice.CreateNewGRNAsync(NewGRN.GetMReceiving(), Base.Current.UserName);
            IsBusy = true;
        }

        public void Reset()
        {
            NewGRN = new MRecivingView();
            //NewGRN.Purchaser = Base.Current.UserName;
            //NewGRN.PurchaseDate = DateTime.UtcNow.AddHours(5.5);
            NewGRN.Requester = Base.Current.UserName;
            NewGRN.ForDate = DateTime.UtcNow.AddHours(5.5);
            Notify("NewGRN");
            IsLocked = true;
        }

        public void CanLock()
        {
            if (NewGRN.VendorID.HasValue && NewGRN.SiteID != 0)
                IsLocked = false;
        }

        public bool Validate()
        {
            Status = "";
            if (!NewGRN.VendorID.HasValue) { Status = "Please Select Vendor"; return false; }
            if (NewGRN.SiteID == 0) { Status = "Please Select Site"; return false; }
            //if (String.IsNullOrWhiteSpace(NewGRN.Purchaser)) { Status = "Requester Cannot be Empty!"; return false; }
            if (String.IsNullOrWhiteSpace(NewGRN.Requester)) { Status = "Requester Cannot be Empty!"; return false; }
            if (NewGRN.LinesViews == null) { Status = "GRN should have alteast one item."; return false; }
            if (NewGRN.LinesViews.Count == 0) { Status = "GRN should have alteast one item."; return false; }
            if (NewGRN.LinesViews.Any(x => x.ItemID == 0)) { Status = "GRN Lines has some invaid/unselected Item/s!"; return false; }
            if (NewGRN.LinesViews.Any(x => x.SizeID == 0)) { Status = "GRN Lines has some invaid/unselected Size/s!"; return false; }
            if (NewGRN.LinesViews.Any(x => x.BrandID == 0)) { Status = "GRN Lines has some invaid/unselected Brand/s!"; return false; }
            return true;
        }

        #endregion
    }

    public class MReceivingLineView : MGRNLine  //MReceivingLine
    {
        TransactionServiceClient service = new TransactionServiceClient();

        public event EventHandler LineAdded;
        public void RaiseLineAdded()
        {
            if (LineAdded != null)
                LineAdded(this, new EventArgs());
        }

        public MReceivingLineView()
        {
            this.ItemIDChanged += new EventHandler(line_ItemIDChanged);
            this.SizeIDChanged += new EventHandler(line_SizeIDChanged);
            this.BrandIDChanged += new EventHandler(line_BrandIDChanged);
        }

        #region Properties

        PendingPOInfo _SelectedPOInfo = new PendingPOInfo();
        public PendingPOInfo SelectedPOInfo
        {
            get { return _SelectedPOInfo; }
            set
            {
                _SelectedPOInfo = value;
                if (value != null)
                {
                    this.POID = SelectedPOInfo.POID;
                    this.OrderedQty = SelectedPOInfo.OrderedQty;
                    this.EarlierReceivedQty = SelectedPOInfo.EarlierReceivedQty;
                    this.UnitPrice = SelectedPOInfo.UnitPrice;
                    this.LinePrice = SelectedPOInfo.LinePrice;
                    //this.TransactionTypeID = "GRN";
                    Notify("SelectedPOInfo");
                }
            }
        }

        ObservableCollection<PendingPOInfo> _PendingInfos = new ObservableCollection<PendingPOInfo>();
        public ObservableCollection<PendingPOInfo> PendingInfos
        {
            get { return _PendingInfos; }
            set
            {
                _PendingInfos = value;
                if (value.Count > 0)
                {
                    SelectedPOInfo = value.First();
                }
                else
                {
                    SelectedPOInfo = null;
                }
                Notify("PendingInfos");
            }
        }

        IEnumerable<MItem> _AllowedItems;
        public IEnumerable<MItem> AllowedItems
        {
            get { return _AllowedItems; }
            set
            {
                _AllowedItems = value;
                Notify("AllowedItems");
            }
        }

        bool _CanEditUnitPrice = true;
        public bool CanEditUnitPrice
        {
            get { return _CanEditUnitPrice; }
            set
            {
                _CanEditUnitPrice = value;
                Notify("CanEditUnitPrice");
            }
        }

        ObservableCollection<PendingPOInfo> _BufferList = new ObservableCollection<PendingPOInfo>();
        public ObservableCollection<PendingPOInfo> BufferList
        {
            get { return _BufferList; }
            set
            {
                _BufferList = value;
                Notify("BufferList");
            }
        }

        #endregion

        #region CompletedEvents...

        void service_GetPendingPOInfosCompleted(object sender, TransactionService.GetPendingPOInfosCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                Base.ShowError(e.Error);
                Base.Current.IsBusy = false;
            }

            if (e.Result.Count == 0)
            {
                Base.Current.IsBusy = false;
                return;
            }
            BufferList = e.Result;  // temporarily storing the pending po info. Data
            this.PendingInfos = new ObservableCollection<PendingPOInfo>();
            Base.Current.IsBusy = false;
        }

        void line_ItemIDChanged(object sender, EventArgs e)
        {
            var item = sender as MReceivingLineView;
            service.GetPendingPOInfosCompleted += new EventHandler<TransactionService.GetPendingPOInfosCompletedEventArgs>(service_GetPendingPOInfosCompleted);
            if (item.ItemID != 0 && VendorID.HasValue && SiteID.HasValue)
            {
                service.GetPendingPOInfosAsync(this.VendorID.Value, this.SiteID.Value, item.ItemID, item.SizeID, item.BrandID);
                Base.Current.IsBusy = true;
            }
        }

        void line_SizeIDChanged(object sender, EventArgs e)
        {
            var data = sender as MReceivingLineView;
            var id = data.SizeID;
            var templist = BufferList.Where(x => x.SizeID == id);
            BufferList = new ObservableCollection<PendingPOInfo>();
            foreach (var i in templist)
            {
                BufferList.Add(i);
            }
            //AssignPO(BufferList);
        }

        void line_BrandIDChanged(object sender, EventArgs e)
        {
            var data = sender as MReceivingLineView;
            var id = data.BrandID;
            var templist = BufferList.Where(x => x.BrandID == id);
            BufferList = new ObservableCollection<PendingPOInfo>();
            foreach (var i in templist)
            {
                BufferList.Add(i);
            }

            if (BufferList.Count == 1 && BufferList.First().IsBlank == true)
            {
                this.PendingInfos = new ObservableCollection<PendingPOInfo>();
                this.SelectedPOInfo = null;
                this.CanEditUnitPrice = true;
            }
            else
            {
                if (BufferList.Distinct().Select(x => x.SizeID).Count() == 1)
                {
                    int? sizeid = BufferList.Select(x => x.SizeID).First();
                    var list = BufferList.Where(x => x.SizeID == sizeid);
                    BufferList = new ObservableCollection<PendingPOInfo>();
                    foreach (var i in list)
                    {
                        BufferList.Add(i);
                    }
                }
                if (BufferList.Distinct().Select(x => x.BrandID).Count() == 1)
                {
                    int? brandid = BufferList.Select(x => x.BrandID).First();
                    var list = BufferList.Where(x => x.BrandID == brandid);
                    BufferList = new ObservableCollection<PendingPOInfo>();
                    foreach (var i in list)
                    {
                        BufferList.Add(i);
                    }
                }
                this.PendingInfos = BufferList;
                this.CanEditUnitPrice = false;
            }
        }

        #endregion
    }

    public class MRecivingView : MGRN   //MReceiving
    {
        public MRecivingView()
        {

        }

        public MRecivingView(MGRN rcv)
        {
            this.ChallanNo = rcv.ChallanNo;
            this.CreatedBy = rcv.CreatedBy;
            this.CreatedOn = rcv.CreatedOn;
            this.InvoiceNo = rcv.InvoiceNo;
            this.LastUpdatedOn = rcv.LastUpdatedOn;
            this.LastUpdatedBy = rcv.LastUpdatedBy;
            this.Requester = rcv.Requester;
            this.ForDate = rcv.ForDate;
            this.GRNLines = rcv.GRNLines;
            this.Remarks = rcv.Remarks;
            this.Site = rcv.Site;
            this.SiteID = rcv.SiteID;
            this.ID = rcv.ID;
            this.VehicleDetails = rcv.VehicleDetails;
            this.Vendor = rcv.Vendor;
            this.VendorID = rcv.VendorID;

            if (rcv.GRNLines == null)
                rcv.GRNLines = new ObservableCollection<MGRNLine>();
            foreach (var line in rcv.GRNLines)
            {
                MReceivingLineView newline = new MReceivingLineView();
                newline.EarlierReceivedQty = line.EarlierReceivedQty;
                newline.Item = line.Item;
                newline.ItemID = line.ItemID;
                newline.SizeID = line.SizeID;
                newline.BrandID = line.BrandID;
                newline.LineID = line.LineID;
                newline.LinePrice = line.LinePrice;
                newline.LineRemarks = line.LineRemarks;
                newline.OrderedQty = line.OrderedQty;
                newline.Quantity = line.Quantity;
                newline.HeaderID = line.HeaderID;
                newline.TruckNo = line.TruckNo;
                newline.UnitPrice = line.UnitPrice;
                newline.LineChallanNo = line.LineChallanNo;
                newline.POID = line.POID;
                newline.PRID = line.PRID;
                newline.PRLineID = line.PRLineID;

                newline.SelectedPOInfo.OrderedQty = line.OrderedQty;
                newline.SelectedPOInfo.EarlierReceivedQty = line.EarlierReceivedQty;

                //newline.SelectedPOInfo.POID = line.POID.Value;
                //newline.SelectedPOInfo.PRID = line.PRID.Value;
                //newline.SelectedPOInfo.PRLineID = line.PRLineID.Value;

                this.LinesViews.Add(newline);
            }
        }

        ObservableCollection<MReceivingLineView> _LinesViews = new ObservableCollection<MReceivingLineView>();
        public ObservableCollection<MReceivingLineView> LinesViews
        {
            get { return _LinesViews; }
            set
            {
                _LinesViews = value;
                Notify("LinesViews");
            }
        }

        public MReceivingLineView AddNewLineView()
        {
            MReceivingLineView line = new MReceivingLineView();
            line.VendorID = this.VendorID;
            line.SiteID = this.SiteID;
            LinesViews.Add(line);
            UpdateLineNumberViews();
            Notify("LinesViews");
            return line;
        }

        public int UpdateLineNumberViews()
        {
            if (LinesViews == null) return 0;
            int i = 1;
            foreach (var item in LinesViews)
            {
                item.LineID = i;
                i++;
            }
            return i - 1;
        }

        public MReceivingLineView DeleteLineView(MReceivingLineView line)
        {
            LinesViews.Remove(line);
            UpdateLineNumberViews();
            Notify("LinesViews");
            return line;
        }

        public MGRN GetMReceiving()
        {
            MGRN rcv = new MGRN();
            rcv.ChallanNo = this.ChallanNo;
            rcv.CreatedBy = this.CreatedBy;
            rcv.CreatedOn = this.CreatedOn;
            rcv.InvoiceNo = this.InvoiceNo;
            rcv.LastUpdatedOn = this.LastUpdatedOn;
            rcv.LastUpdatedBy = this.LastUpdatedBy;
            rcv.Requester = this.Requester;
            rcv.ForDate = this.ForDate;
            //rcv.ReceivingLines = this.ReceivingLines;
            rcv.Remarks = this.Remarks;
            rcv.Site = this.Site;
            rcv.SiteID = this.SiteID;
            rcv.ID = this.ID;
            rcv.VehicleDetails = this.VehicleDetails;
            rcv.Vendor = this.Vendor;
            rcv.VendorID = this.VendorID;

            if (rcv.GRNLines == null)
                rcv.GRNLines = new ObservableCollection<MGRNLine>();
            foreach (var line in this.LinesViews)
            {
                var newline = new MGRNLine();
                newline.EarlierReceivedQty = line.EarlierReceivedQty;
                newline.Item = line.Item;
                newline.ItemID = line.ItemID;
                newline.SizeID = line.SizeID;
                newline.BrandID = line.BrandID;
                newline.LineID = line.LineID;
                newline.LinePrice = line.LinePrice;
                newline.LineRemarks = line.LineRemarks;
                newline.OrderedQty = line.OrderedQty;
                newline.Quantity = line.Quantity;
                newline.HeaderID = line.HeaderID;
                newline.TruckNo = line.TruckNo;
                newline.UnitPrice = line.UnitPrice;
                newline.LineChallanNo = line.LineChallanNo;
                newline.POID = line.POID;
                newline.PRID = line.PRID;
                newline.PRLineID = line.PRLineID;

                if (line.SelectedPOInfo != null)
                {
                    newline.POID = line.SelectedPOInfo.POID;
                    newline.PRID = line.SelectedPOInfo.PRID;
                    newline.PRLineID = line.SelectedPOInfo.PRLineID;
                }
                //else
                //{
                //    newline.POID = null;
                //    newline.PRID = null;
                //    newline.PRLineID = null;
                //}
                rcv.GRNLines.Add(newline);
            }
            return rcv;
        }
    }
}
