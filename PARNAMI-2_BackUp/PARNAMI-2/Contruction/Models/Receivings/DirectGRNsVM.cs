﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Contruction.ReceivingService;
using System.ComponentModel;
using SIMS.MM.MODEL;
using SIMS.Core;

namespace Contruction
{
    public class DirectGRNsVM : ViewModel
    {
        ReceivingServiceClient service = new ReceivingServiceClient();

        public DirectGRNsVM()
        {
            if (!DesignerProperties.GetIsInDesignMode(Application.Current.RootVisual))
            {
                OnInit();
            }
        }

        public void OnInit()
        {
            service.GetAllDirectGrnsCompleted += new EventHandler<GetAllDirectGrnsCompletedEventArgs>(service_GetAllDirectGrnsCompleted);
            LoadDirectGRNs();
            Base.AppEvents.ReceivingDataChanged += new EventHandler(AppEvents_ReceivingDataChanged);
        }

        void service_GetAllDirectGrnsCompleted(object sender, GetAllDirectGrnsCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                Base.ShowError(e.Error);
                IsBusy = false;
            }
            else
            {
                DirectGRNs = e.Result;
                IsBusy = false;
            }
        }

        public event EventHandler DirectGRNChanged;
        public void RaiseDirectGRNChanged()
        {
            if (DirectGRNChanged != null)
                DirectGRNChanged(this, new EventArgs());
        }

        void AppEvents_ReceivingDataChanged(object sender, EventArgs e)
        {
            LoadDirectGRNs();
        }

        int _PageIndex = 0;
        public int PageIndex
        {
            get { return _PageIndex; }
            set
            {
                _PageIndex = value;
                Notify("PageIndex");
                LoadDirectGRNs();
            }
        }

        IPaginated<MReceivingHeader> _DirectGRNs;
        public IPaginated<MReceivingHeader> DirectGRNs
        {
            get { return _DirectGRNs; }
            set
            {
                _DirectGRNs = value;
                RaiseDirectGRNChanged();
                Notify("DirectGRNs");
            }
        }

        string _SearchKey;
        public string SearchKey
        {
            get { return _SearchKey; }
            set
            {
                _SearchKey = value;
                Notify("SearchKey");
            }
        }

        int? _VendorID;
        public int? VendorID
        {
            get { return _VendorID; }
            set
            {
                _VendorID = value;
                Notify("VendorID");
            }
        }

        int? _SiteID;
        public int? SiteID
        {
            get { return _SiteID; }
            set
            {
                _SiteID = value;
                Notify("SiteID");
            }
        }

        string _RecType;
        public string RecType
        {
            get { return _RecType; }
            set
            {
                _RecType = value;
                Notify("RecType");
            }
        }

        public void Clear()
        {
            SearchKey = null;
            VendorID = null;
            SiteID = null;
            LoadDirectGRNs();
        }

        public void LoadDirectGRNs()
        {
            service.GetAllDirectGrnsAsync(Base.Current.UserName, PageIndex, null, SearchKey, VendorID, SiteID, RecType);
            IsBusy = true;
        }

        internal void MoveToPage(int p)
        {
            PageIndex = p;
        }
    }
}