﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Contruction.MasterDBService;
using System.ComponentModel;
using System.Collections.ObjectModel;
using SIMS.MM.MODEL;
using System.Collections.Generic;
using System.Linq;

namespace Contruction
{
    public class StockBelowSafetyModel : ViewModel
    {
        MasterDataServiceClient service = new MasterDataServiceClient();

        public StockBelowSafetyModel()
        {
            if (!DesignerProperties.GetIsInDesignMode(Application.Current.RootVisual))
            {
                OnInit();
            }
        }

        public void OnInit()
        {
            service.GetSafetyStockbySiteIDCompleted += new EventHandler<GetSafetyStockbySiteIDCompletedEventArgs>(service_GetSafetyStockbySiteIDCompleted);
        }

        void service_GetSafetyStockbySiteIDCompleted(object sender, GetSafetyStockbySiteIDCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                Base.ShowError(e.Error);
                IsBusy = false;
            }
            else
            {
                AllItemsBeloweSafetyStock = e.Result;
                IsBusy = false;
            }
        }

        #region Model

        ObservableCollection<MSafetyStock> _AllItemsBeloweSafetyStock = new ObservableCollection<MSafetyStock>();
        public ObservableCollection<MSafetyStock> AllItemsBeloweSafetyStock
        {
            get { return _AllItemsBeloweSafetyStock; }
            set
            {
                _AllItemsBeloweSafetyStock = value;
                Notify("AllItemsBeloweSafetyStock");
            }
        }

        int? _siteid;
        public int? siteid
        {
            get { return _siteid; }
            set
            {
                _siteid = value;
                Notify("siteid");
            }
        }

        #endregion

        public void Load()
        {
            //service.GetSafetyStockbySiteIDAsync(siteid);
            //IsBusy=true;
        }
    }
}
