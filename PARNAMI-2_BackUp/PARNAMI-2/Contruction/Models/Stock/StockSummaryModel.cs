﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Contruction.MasterDBService;
using System.Collections.Generic;
using System.ComponentModel;
using SIMS.MM.MODEL;

namespace Contruction
{
    public class StockSummaryModel : ViewModel
    {
        MasterDataServiceClient service = new MasterDataServiceClient();

        public StockSummaryModel()
        {
            if (!DesignerProperties.GetIsInDesignMode(Application.Current.RootVisual))
            {
                OnInit();
            }
        }

        public void OnInit()
        {
            StartDate = null;
            EndDate = null;
            service.GetStockReportCompleted+=new EventHandler<GetStockReportCompletedEventArgs>(service_GetStockReportCompleted);
        }

        #region Model

        IEnumerable<MStockReport> _StockSummary;
        public IEnumerable<MStockReport> StockSummary
        {
            get { return _StockSummary; }
            set
            {
                _StockSummary = value;
                Notify("StockSummary");
            }
        }

        DateTime? _StartDate;
        public DateTime? StartDate
        {
            get { return _StartDate; }
            set
            {
                _StartDate = value;
                Notify("StartDate");
            }
        }

        DateTime? _EndDate;
        public DateTime? EndDate
        {
            get { return _EndDate; }
            set
            {
                _EndDate = value;
                Notify("EndDate");
            }
        }

        int? _SiteID;
        public int? SiteID
        {
            get { return _SiteID; }
            set
            {
                _SiteID = value;
                LoadStockReport();
                Notify("SiteID");
            }
        }

        int? _ItemID;
        public int? ItemID
        {
            get { return _ItemID; }
            set
            {
                _ItemID = value;
                LoadStockReport();
                Notify("ItemID");
            }
        }

        int? _BrandID;
        public int? BrandID
        {
            get { return _BrandID; }
            set
            {
                _BrandID = value;
                Notify("BrandID");
            }
        }

        int? _SizeID;
        public int? SizeID
        {
            get { return _SizeID; }
            set
            {
                _SizeID = value;
                Notify("SizeID");
            }
        }

        #endregion

        void service_GetStockReportCompleted(object sender, GetStockReportCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                Base.ShowError(e.Error);
                IsBusy = false;
            }
            else
            {
                StockSummary = e.Result;
                IsBusy = false;
            }
        }

        public void LoadStockReport()
        {
            service.GetStockReportAsync(StartDate, EndDate, SiteID, ItemID, SizeID, BrandID);
            IsBusy = true;
        }

        public IEnumerable<MStockReport> LoadStockReport(int? SiteID, int? ItemID, int? SizeID, int? BrandID)
        {
            service.GetStockReportAsync(null, null, SiteID, ItemID, SizeID, BrandID);
            service.GetStockReportCompleted+=(s,e)=>
            {
                if (e.Error != null)
                {
                    Base.ShowError(e.Error);
                    IsBusy = false;
                }
                else
                {
                    StockSummary = e.Result;
                    IsBusy = false;
                }
            };
            return StockSummary;
        }
    }
}
