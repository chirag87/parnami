﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.ComponentModel;
using Contruction.SSService;
using SIMS.EM.Model;
using Contruction.Views.Employee.SalaryStructure;

namespace Contruction.Models.Employee.SalaryStructure
{
    public class NewSSVM:ViewModel
    {
        SalaryStructureServiceClient proxy = new SalaryStructureServiceClient();
        public NewSSVM()
        {
            if (!DesignerProperties.GetIsInDesignMode(Application.Current.RootVisual))
            {
                OnInit();
            }
        }

        private void OnInit()
        {
            proxy.UpdateSalaryStructureCompleted += new EventHandler<UpdateSalaryStructureCompletedEventArgs>(proxy_UpdateSalaryStructureCompleted);
            proxy.GetSalaryStructureByIDCompleted += new EventHandler<GetSalaryStructureByIDCompletedEventArgs>(proxy_GetSalaryStructureByIDCompleted);
        }

        void proxy_GetSalaryStructureByIDCompleted(object sender, GetSalaryStructureByIDCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                IsBusy = false;
                Base.ShowError(e.Error);
            }
            else
            {
                IsBusy = false;
                NewSS = e.Result;
            }
        }

        void proxy_UpdateSalaryStructureCompleted(object sender, UpdateSalaryStructureCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                IsBusy = false;
                Base.ShowError(e.Error);
            }
            else
            {
                IsBusy = false;
                MessageBox.Show("                  EMP_ID: " + e.Result.EmpID + "\n" + "All Changes Successfullty Saved");
                Base.EmployeeEvents.RaiseSSUpdated(e.Result.EmpID);
            }
        }



        MSalaryStructure _NewSS=new MSalaryStructure();
        public MSalaryStructure NewSS
        {
            get { return _NewSS; }
            set
            {
                _NewSS = value;
                Notify("NewSS");
            }
        }

        string _Status;
        public string Status
        {
            get { return _Status; }
            set
            {
                _Status = value;
                Notify("Status");
            }
        }

        public void SubmitToDb(int EmpID)
        {
            if (!Validate()) return;
            Notify("NewSS");
            IsBusy = true;
            NewSS.EmpID = EmpID;
            proxy.UpdateSalaryStructureAsync(EmpID, NewSS);
        }

        private bool Validate()
        {
            Status = "";

            //if (String.IsNullOrWhiteSpace(NewEmployee.FirstName)) { Status = "Enter the First Name"; return false; }
            //if (NewEmployee.TypeID == 0) { Status = "Please specify Type of Labour!"; return false; }
            //if (NewEmployee.BUID == 0) { Status = "Please Select Site for Employee!"; return false; }
            //if (NewEmployee.CurrentRate == 0 || NewEmployee.CurrentRate == null) { Status = "Enter Current Rate For Labour"; return false; }
            //if (!Regex.IsMatch(NewLabour.LastName, @"^[a-zA-Z]+$", RegexOptions.IgnoreCase)) { Status = "Last Name should only contain alphabets!"; return false; }
            return true;
        }

        internal void GetSSForEmployee(int EmpID)
        {
            IsBusy = true;
            proxy.GetSalaryStructureByIDAsync(EmpID);
        }
    }
}
