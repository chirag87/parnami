﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Contruction.MessagingService;
using System.ComponentModel;
using SIMS.MM.MODEL;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace Contruction
{
    public class NewMessageModel : ViewModel
    {
        MessagingServiceClient service = new MessagingServiceClient();

        public NewMessageModel()
        {
            if (!DesignerProperties.GetIsInDesignMode(Application.Current.RootVisual))
            {
                OnInit();
            }
        }

        public void OnInit()
        {
            service.SendNewMailCompleted += new EventHandler<SendNewMailCompletedEventArgs>(service_SendNewMailCompleted);
        }

        void service_SendNewMailCompleted(object sender, SendNewMailCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                Base.ShowError(e.Error);
                IsBusy = false;
            }
            else
            {
                MessageBox.Show("Mail Sent Successfully!");
                NewMessage = e.Result;
                IsBusy = false;
                Reset();
                Base.Redirect("Inbox");
                Base.Current.LoadAllMessages();
            }
        }

        MMessaging _NewMessage = new MMessaging();
        public MMessaging NewMessage
        {
            get { return _NewMessage; }
            set
            {
                _NewMessage = value;
                Notify("NewMessage");
            }
        }

        public void SendMail()
        {
            NewMessage.Sender = Base.Current.UserName;
            NewMessage.SentDate = DateTime.UtcNow.AddHours(5.5);

            service.SendNewMailAsync(NewMessage);
            IsBusy = true;
        }

        public void Reset()
        {
            NewMessage = new MMessaging();
        }
    }
}
