﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Contruction.SrvVendorPayments;
using System.ComponentModel;
using SIMS.MM.MODEL.Headers;
using System.Collections.ObjectModel;
using SIMS.MM.MODEL;
using SIMS.Core;

namespace Contruction
{
    public class AllVendorPaymentVM : ViewModel
    {
        VendorPaymentsServiceClient service = new VendorPaymentsServiceClient();

        public event EventHandler VPDataChanged;
        public void RaiseVPDataChanged()
        {
            if (VPDataChanged != null)
                VPDataChanged(this, new EventArgs());
        }

        #region Core
        public AllVendorPaymentVM()
        {
            if (!DesignerProperties.GetIsInDesignMode(Application.Current.RootVisual))
            {
                OnInit();
            }
        }

        public void OnInit()
        {
            service.GetAllVendorPaymentsCompleted += new EventHandler<GetAllVendorPaymentsCompletedEventArgs>(service_GetAllVendorPaymentsCompleted);
            Load();
        }

        void service_GetAllVendorPaymentsCompleted(object sender, GetAllVendorPaymentsCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                Base.ShowError(e.Error);
                IsBusy = false;
            }
            else
            {
                AllPayments = e.Result;
                IsBusy = false;
            }
        }

        #endregion

        #region Properties

        IPaginated<MVendorPaymentHeader> _AllPayments;
        public IPaginated<MVendorPaymentHeader> AllPayments
        {
            get { return _AllPayments; }
            set
            {
                _AllPayments = value;
                Notify("AllPayments");
                RaiseVPDataChanged();
            }
        }

        int? _vendorid;
        public int? vendorid
        {
            get { return _vendorid; }
            set
            {
                _vendorid = value;
                Notify("vendorid");
            }
        }

        int? _siteid;
        public int? siteid
        {
            get { return _siteid; }
            set
            {
                _siteid = value;
                Notify("siteid");
            }
        }

        int? _pageindex = 0;
        public int? pageindex
        {
            get { return _pageindex; }
            set
            {
                _pageindex = value;
                Notify("pageindex");
            }
        }

        #endregion

        public void Load()
        {
            service.GetAllVendorPaymentsAsync(Base.Current.UserName, vendorid, siteid, pageindex, null);
            IsBusy = true;
        }

        public void Clear()
        {
            vendorid = null;
            siteid = null;
            Load();
        }
    }
}
