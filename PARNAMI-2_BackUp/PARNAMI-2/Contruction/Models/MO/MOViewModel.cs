﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.ComponentModel;
using Contruction.MOService;
using SIMS.MM.MODEL;
using SIMS.Core;
using System.Collections.ObjectModel;
using System.Collections.Generic;

namespace Contruction
{
    public class MOViewModel : ViewModel
    {
        MOServiceClient service = new MOServiceClient();

        public MOViewModel()
        {
            if (!DesignerProperties.GetIsInDesignMode(Application.Current.RootVisual))
            {
                OnInit();
            }
        }

        public event EventHandler MOChanged;
        public void RaiseMOChanged()
        {
            if (MOChanged != null)
                MOChanged(this, new EventArgs());
        }

        public void OnInit()
        {
            service.GetAllMOsCompleted += new EventHandler<GetAllMOsCompletedEventArgs>(service_GetAllMOsCompleted);
            LoadMOs();
            Base.AppEvents.MODataChanged += new EventHandler(AppEvents_MODataChanged);
            StatusTypes.AddRange(new string[] { "Pending", "Closed", "All" });
            ConsumableTypes.AddRange(new string[] { "Consumable", "Returnable", "All" });
        }

        void AppEvents_MODataChanged(object sender, EventArgs e)
        {
            LoadMOs();
        }

        void service_GetAllMOsCompleted(object sender, GetAllMOsCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                Base.ShowError(e.Error);
                IsBusy = false;
            }
            else
            {
                MOs = e.Result;
                IsBusy = false;
            }
        }

        #region Properties...

        IPaginated<MMOHeader> _MOs = new PaginatedData<MMOHeader>();
        public IPaginated<MMOHeader> MOs
        {
            get { return _MOs; }
            set
            {
                _MOs = value;
                Notify("MOs");
                RaiseMOChanged();
            }
        }

        int _PageIndex = 0;
        public int PageIndex
        {
            get { return _PageIndex; }
            set
            {
                _PageIndex = value;
                Notify("PageIndex");
                LoadMOs();
            }
        }

        string _SearchKey;
        public string SearchKey
        {
            get { return _SearchKey; }
            set
            {
                _SearchKey = value;
                Notify("SearchKey");
            }
        }

        int? _ReqSiteID;
        public int? ReqSiteID
        {
            get { return _ReqSiteID; }
            set
            {
                _ReqSiteID = value;
                Notify("ReqSiteID");
            }
        }

        int? _TransSiteID;
        public int? TransSiteID
        {
            get { return _TransSiteID; }
            set
            {
                _TransSiteID = value;
                Notify("TransSiteID");
            }
        }

        string _ConsumableType;
        public string ConsumableType
        {
            get { return _ConsumableType; }
            set
            {
                _ConsumableType = value;
                Notify("ConsumableType");
            }
        }

        public bool? IsClosed
        {
            get
            {
                if (Status == "Pending")
                    return false;
                else if (Status == "Closed")
                    return true;
                else
                    return null;
            }
        }

        List<string> _StatusTypes = new List<string>();
        public List<string> StatusTypes
        {
            get { return _StatusTypes; }
            set
            {
                _StatusTypes = value;
                Notify("StatusTypes");
            }
        }

        List<string> _ConsumableTypes = new List<string>();
        public List<string> ConsumableTypes
        {
            get { return _ConsumableTypes; }
            set
            {
                _ConsumableTypes = value;
                Notify("ConsumableTypes");
            }
        }

        string _Status;
        public string Status
        {
            get { return _Status; }
            set
            {
                _Status = value;
                Notify("Status");
            }
        }

        #endregion

        #region Methods...

        public void Clear()
        {
            SearchKey = null;
            ReqSiteID = null;
            TransSiteID = null;
            LoadMOs();
        }

        public void LoadMOs()
        {
            if (ConsumableType == "All") ConsumableType = null;
            service.GetAllMOsAsync(Base.Current.UserName, PageIndex, null, SearchKey, TransSiteID, ReqSiteID, ConsumableType, IsClosed);
            IsBusy = true;
        }

        internal void MoveToPage(int p)
        {
            PageIndex = p;
        }

        #endregion
    }
}
