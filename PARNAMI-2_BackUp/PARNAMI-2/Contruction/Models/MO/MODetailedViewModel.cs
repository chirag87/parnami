﻿using System;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Contruction.MOService;
using System.ComponentModel;
using SIMS.MM.MODEL;
using System.Collections.ObjectModel;

namespace Contruction
{
    public class MODetailedViewModel : ViewModel
    {
        MOServiceClient service = new MOServiceClient();

        public MODetailedViewModel()
        {
            if (!DesignerProperties.GetIsInDesignMode(Application.Current.RootVisual))
            {
                OnInit();
            }
        }

        #region Methods...

        public void OnInit()
        {
            AppEvents.Current.MORequested += new EventHandler<MORequestedArgs>(Current_MORequested);
            service.GetMObyIDCompleted += new EventHandler<GetMObyIDCompletedEventArgs>(service_GetMObyIDCompleted);
            service.CloseMOCompleted += new EventHandler<AsyncCompletedEventArgs>(service_CloseMOCompleted);
            service.IsSimilarMOsAvailableCompleted += new EventHandler<IsSimilarMOsAvailableCompletedEventArgs>(service_IsSimilarMOsAvailableCompleted);
            service.CombineMOCompleted += new EventHandler<CombineMOCompletedEventArgs>(service_CombineMOCompleted);
            service.GetMOReleasingsAgainstMOCompleted += new EventHandler<GetMOReleasingsAgainstMOCompletedEventArgs>(service_GetMOReleasingsAgainstMOCompleted);
            service.GetMOReceivingsAgainstMOCompleted +=new EventHandler<GetMOReceivingsAgainstMOCompletedEventArgs>(service_GetMOReceivingsAgainstMOCompleted);
        }

        public void GetMOReleasingsAgainstMO(int moid)
        {
            service.GetMOReleasingsAgainstMOAsync(moid);
            IsBusy = true;
        }

        public void GetMOReceivingsAgainstMO(int moid)
        {
            service.GetMOReceivingsAgainstMOAsync(moid);
            IsBusy = true;
        }

        public void GetMO(int moid)
        {
            service.GetMObyIDAsync(moid);
            IsBusy = true;
        }

        public void CombineMO(int moid)
        {
            service.CombineMOAsync(moid);
            IsBusy = true;
        }

        public void CheckMOAvailability(int moid)
        {
            service.IsSimilarMOsAvailableAsync(moid);
            IsBusy = true;
        }

        public void CloseMO(int id)
        {
            if (ConvertedMO.MOLines.All(x => x.PendingReceiveQty == null))
            {
                service.CloseMOAsync(id);
                IsBusy = true;
            }
            else
            {
                MessageBox.Show("Can't Close Transfer Order, as their are some Material Left to be received !");
            }
        }

        #endregion

        #region Completed Events...

        void service_GetMOReceivingsAgainstMOCompleted(object sender, GetMOReceivingsAgainstMOCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                Base.ShowError(e.Error);
                IsBusy = false;
            }
            else
            {
                MORec = e.Result;
                if (e.Result.Count() > 0)
                    HasReceivings = true;
                else
                    HasReceivings = false;
                IsBusy = false;
            }
        }

        void service_GetMOReleasingsAgainstMOCompleted(object sender, GetMOReleasingsAgainstMOCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                Base.ShowError(e.Error);
                IsBusy = false;
            }
            else
            {
                MORel = e.Result;
                if (e.Result.Count() > 0)
                    HasReleasings = true;
                else
                    HasReleasings = false;
                IsBusy = false;
            }
        }

        void service_IsSimilarMOsAvailableCompleted(object sender, IsSimilarMOsAvailableCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                Base.ShowError(e.Error);
                IsBusy = false;
            }
            else
            {
                IsMOAvailable = e.Result;
                IsBusy = false;
            }
        }

        void service_CombineMOCompleted(object sender, CombineMOCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                Base.ShowError(e.Error);
                IsBusy = false;
            }
            else
            {
                MessageBox.Show("Similar TOs Combined Successfully !");
                ConvertedMO = e.Result;
                Base.AppEvents.RaiseMODataChanged();
                IsBusy = false;
            }
        }

        void service_CloseMOCompleted(object sender, AsyncCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                Base.ShowError(e.Error);
                IsBusy = false;
            }
            else
            {
                IsBusy = false;
                MessageBox.Show("This Transfer Order is now Closed !");
                Base.AppEvents.RaiseNewMOAdded();
            }
        }

        void Current_MORequested(object sender, MORequestedArgs e)
        {
            int moid = e.MOID;
            CheckMOAvailability(moid);
            GetMO(moid);
        }

        void service_GetMObyIDCompleted(object sender, GetMObyIDCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                Base.ShowError(e.Error);
                IsBusy = false;
            }
            else
            {
                ConvertedMO = e.Result;
                IsBusy = false;
            }
        }

        #endregion

        #region Properties...

        MMO _ConvertedMO;
        public MMO ConvertedMO
        {
            get { return _ConvertedMO; }
            set
            {
                _ConvertedMO = value;
                Notify("ConvertedMO");
            }
        }

        bool _IsMOAvailable = false;
        public bool IsMOAvailable
        {
            get { return _IsMOAvailable; }
            set
            {
                _IsMOAvailable = value;
                Notify("IsMOAvailable");
            }
        }

        ObservableCollection<MMOReleasing> _MORel = new ObservableCollection<MMOReleasing>();
        public ObservableCollection<MMOReleasing> MORel
        {
            get { return _MORel; }
            set
            {
                _MORel = value;
                Notify("MORel");
            }
        }

        ObservableCollection<MMOReceiving> _MORec = new ObservableCollection<MMOReceiving>();
        public ObservableCollection<MMOReceiving> MORec
        {
            get { return _MORec; }
            set
            {
                _MORec = value;
                Notify("MORec");
            }
        }

        bool _HasReleasings = false;
        public bool HasReleasings
        {
            get { return _HasReleasings; }
            set
            {
                _HasReleasings = value;
                Notify("HasReleasings");
            }
        }

        bool _HasReceivings = false;
        public bool HasReceivings
        {
            get { return _HasReceivings; }
            set
            {
                _HasReceivings = value;
                Notify("HasReceivings");
            }
        }

        #endregion
    }
}
