﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Contruction.BillingService;
using System.ComponentModel;
using SIMS.MM.MODEL;
using SIMS.Core;

namespace Contruction
{
    public class AllBillingsViewModel : ViewModel
    {
        BillingServiceClient service = new BillingServiceClient();

        public AllBillingsViewModel()
        {
            if (!DesignerProperties.GetIsInDesignMode(Application.Current.RootVisual))
            {
                OnInit();
            }
        }

        public event EventHandler BillingDataChanged;
        public void RaiseBillingDataChanged()
        {
            if (BillingDataChanged != null)
                BillingDataChanged(this, new EventArgs());
        }

        public void OnInit()
        {
            service.GetAllBillsCompleted += new EventHandler<GetAllBillsCompletedEventArgs>(service_GetAllBillsCompleted);
            GetAllBills();
            Base.AppEvents.BillingDataChanged += new EventHandler(AppEvents_BillingDataChanged);
        }

        void AppEvents_BillingDataChanged(object sender, EventArgs e)
        {
            GetAllBills();
        }

        void service_GetAllBillsCompleted(object sender, GetAllBillsCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                Base.ShowError(e.Error);
                IsBusy = false;
            }
            else
            {
                AllBills = e.Result;
                IsBusy = false;
            }
        }

        #region Properties...

        IPaginated<MBilling> _AllBills;
        public IPaginated<MBilling> AllBills
        {
            get { return _AllBills; }
            set
            {
                _AllBills = value;
                Notify("AllBills");
                RaiseBillingDataChanged();
            }
        }

        int? _PageIndex = 0;
        public int? PageIndex
        {
            get { return _PageIndex; }
            set
            {
                _PageIndex = value;
                Notify("PageIndex");
            }
        }

        string _Key;
        public string Key
        {
            get { return _Key; }
            set
            {
                _Key = value;
                Notify("Key");
            }
        }

        int? _VendorID;
        public int? VendorID
        {
            get { return _VendorID; }
            set
            {
                _VendorID = value;
                Notify("VendorID");
            }
        }

        int? _SiteID;
        public int? SiteID
        {
            get { return _SiteID; }
            set
            {
                _SiteID = value;
                Notify("SiteID");
            }
        }

        string _SearchKey;
        public string SearchKey
        {
            get { return _SearchKey; }
            set
            {
                _SearchKey = value;
                Notify("SearchKey");
            }
        }

        string _Filter;
        public string Filter
        {
            get { return _Filter; }
            set
            {
                _Filter = value;
                Notify("Filter");
            }
        }

        #endregion

        public void GetAllBills()
        {
            service.GetAllBillsAsync(Base.Current.UserName, PageIndex, null, Key, VendorID, SiteID, Filter);
            IsBusy = true;
        }

        public void Clear()
        {
            SearchKey = null;
            Filter = null;
            VendorID = null;
            GetAllBills();
        }
    }
}
