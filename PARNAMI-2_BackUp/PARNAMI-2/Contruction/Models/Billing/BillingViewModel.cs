﻿using System;
using System.Net;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.ComponentModel;
using Contruction.BillingService;
using System.Collections.ObjectModel;

using System.Collections.Generic;
using SIMS.Core;
using SIMS.MM.MODEL;

namespace Contruction
{
    public class BillingViewModel : ViewModel
    {
        BillingServiceClient service = new BillingServiceClient();

        public BillingViewModel()
        {
            if (!DesignerProperties.GetIsInDesignMode(Application.Current.RootVisual))
            {
                OnInit();
            }
        }

        public void OnInit()
        {
            //service.GetReceivingLinesCompleted+=new EventHandler<GetReceivingLinesCompletedEventArgs>(service_GetReceivingLinesCompleted);
            service.GetGRNLinesCompleted += new EventHandler<GetGRNLinesCompletedEventArgs>(service_GetGRNLinesCompleted);
        }

        #region Events...

        public event EventHandler loadmydata;
        public void Raiseloadmydata()
        {
            if (loadmydata != null)
                loadmydata(this, new EventArgs());
        }

        #endregion

        #region Completed Events...

        void service_GetGRNLinesCompleted(object sender, GetGRNLinesCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                Base.ShowError(e.Error);

                IsBusy = false;
            }
            else
            {
                AllReceivingLines = null;
                AllReceivingLines = e.Result;
                IsBusy = false;
            }
        }

        //void service_GetReceivingLinesCompleted(object sender, GetReceivingLinesCompletedEventArgs e)
        //{
        //    if (e.Error != null)
        //    {
        //        Base.ShowError(e.Error);

        //        IsBusy = false;
        //    }
        //    else
        //    {
        //        AllReceivingLines = null;
        //        AllReceivingLines = e.Result;
        //        IsBusy = false;
        //    }
        //}

        #endregion

        #region Properties...

        //IPaginated<MReceivingLine> _AllReceivingLines = new PaginatedData<MReceivingLine>();
        //public IPaginated<MReceivingLine> AllReceivingLines
        //{
        //    get { return _AllReceivingLines; }
        //    set
        //    {
        //        _AllReceivingLines = value;
        //        NotifyAllReceivingLines();
        //        Raiseloadmydata();
        //    }
        //}

        //public List<MReceivingLine> AllReceivingLinesData
        //{
        //    get
        //    {
        //        if (AllReceivingLines == null) return null;
        //        return AllReceivingLines.Data;
        //    }
        //}

        //ObservableCollection<MReceivingLine> _ReceivingLinesList = new ObservableCollection<MReceivingLine>();
        //public ObservableCollection<MReceivingLine> ReceivingLinesList
        //{
        //    get { return _ReceivingLinesList; }
        //    set
        //    {
        //        _ReceivingLinesList = value;
        //        Notify("ReceivingLinesList");
        //    }
        //}

        IPaginated<MGRNLine> _AllReceivingLines = new PaginatedData<MGRNLine>();
        public IPaginated<MGRNLine> AllReceivingLines
        {
            get { return _AllReceivingLines; }
            set
            {
                _AllReceivingLines = value;
                Notify("AllReceivingLines");
                Raiseloadmydata();
            }

        }

        public List<MGRNLine> AllReceivingLinesData
        {
            get
            {
                if (AllReceivingLines == null) return null;
                return AllReceivingLines.Data;
            }
        }

        ObservableCollection<MGRNLine> _ReceivingLinesList = new ObservableCollection<MGRNLine>();
        public ObservableCollection<MGRNLine> ReceivingLinesList
        {
            get { return _ReceivingLinesList; }
            set
            {
                _ReceivingLinesList = value;
                Notify("ReceivingLinesList");
            }
        }

        public void NotifyAllReceivingLines()
        {
            Notify("AllReceivingLines");
            Notify("AllReceivingLinesData");
            Notify("CanEditHeader");
            Notify("CanEditDetails");
        }

        int? _PageIndex = 0;
        public int? PageIndex
        {
            get { return _PageIndex; }
            set
            {
                _PageIndex = value;
                Notify("PageIndex");
                Load();
            }
        }

        string _Key;
        public string Key
        {
            get { return _Key; }
            set
            {
                _Key = value;
                Notify("Key");
            }
        }

        int? _itemid;
        public int? itemid
        {
            get { return _itemid; }
            set
            {
                _itemid = value;
                Notify("itemid");
            }
        }

        int? _vendorid;
        public int? vendorid
        {
            get { return _vendorid; }
            set
            {
                _vendorid = value;
                Notify("vendorid");
            }
        }

        int? _siteid;
        public int? siteid
        {
            get { return _siteid; }
            set
            {
                _siteid = value;
                Notify("siteid");
            }
        }

        bool _IsChecked;
        public bool IsChecked
        {
            get { return _IsChecked; }
            set
            {
                _IsChecked = value;
                Notify("IsChecked");
            }
        }

        bool _IsSelected;
        public bool IsSelected
        {
            get { return _IsSelected; }
            set
            {
                _IsSelected = value;
                Notify("IsSelected");
            }
        }

        DateTime? _sdate;
        public DateTime? sdate
        {
            get { return _sdate; }
            set
            {
                _sdate = value;
                Notify("sdate");
            }
        }

        DateTime? _edate;
        public DateTime? edate
        {
            get { return _edate; }
            set
            {
                _edate = value;
                Notify("edate");
            }
        }

        string _challan;
        public string challan
        {
            get { return _challan; }
            set
            {
                _challan = value;
                Notify("challan");
            }
        }

        string _SelectedRangeType;
        public string SelectedRangeType
        {
            get { return _SelectedRangeType; }
            set
            {
                _SelectedRangeType = value;
                ComputeDate();
                Notify("SelectedRangeType");
            }
        }

        public string[] RangeTypes
        {
            get
            {
                return new string[]
                {
                    "Last 7 Days",
                    "Last Month",
                    "Last Quater",
                    "Last Year"
                };
            }
        }

        public void NotifyCans()
        {
            Notify("CanEditHeader");
            Notify("CanEditDetails");
        }

        public bool CanEditHeader
        {
            get { return (!vendorid.HasValue || !siteid.HasValue); }
        }

        public bool CanEditDetails
        {
            get { return !CanEditHeader; }
        }

        #endregion

        #region Methods...

        public void Clear()
        {
            Key = null;
            itemid = null;
            challan = null;
            Load();
        }

        public void StartCreateNew()
        {
            Notify("CanEditHeader");
            Notify("CanEditDetails");
        }

        public void Load()
        {
            if (vendorid.HasValue && siteid.HasValue)
            {
                service.GetGRNLinesAsync(Base.Current.UserName, PageIndex, null, Key, itemid, vendorid, siteid, sdate, edate, challan);
                //service.GetReceivingLinesAsync(Base.Current.UserName, PageIndex, null, Key, itemid, vendorid, siteid, sdate, edate, challan);
                IsBusy = true;
            }
            else
            {
                MessageBox.Show("Please Select Vendor and Site !", "Error", MessageBoxButton.OK);
            }
        }

        public void Reset()
        {
            vendorid = null;
            siteid = null;
            NotifyCans();
        }

        internal void MoveToPage(int p)
        {
            PageIndex = p;
        }

        public void ComputeDate()
        {
            switch (SelectedRangeType)
            {
                case "Last 7 Days":
                    edate = DateTime.Today;
                    sdate = edate.Value.AddDays(-7);
                    break;

                case "Last Month":
                    edate = DateTime.Today;
                    sdate = edate.Value.AddMonths(-1);
                    break;

                case "Last Quater":
                    edate = DateTime.Today;
                    sdate = edate.Value.AddMonths(-3);
                    break;

                case "Last Year":
                    edate = DateTime.Today;
                    sdate = edate.Value.AddYears(-1);
                    break;

                default:
                    edate = DateTime.Today;
                    sdate = null;
                    break;
            }
        }

        #endregion
    }
}
