﻿using System;
using System.Net;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.ComponentModel;
using Contruction.BillingService;
using SIMS.MM.MODEL;

namespace Contruction
{
    public class NewBillingModel : ViewModel
    {
        BillingServiceClient service = new BillingServiceClient();

        public override void OnStateChanged()
        {
            base.OnStateChanged();
            NotifyAllCans();
        }

        public NewBillingModel()
        {
            if (!DesignerProperties.GetIsInDesignMode(Application.Current.RootVisual))
            {
                OnInit();
            }
        }

        public void OnInit()
        {
            ControlState = ControlStates.Add;
            NotifyAllCans();
            service.CreateNewBillCompleted += new EventHandler<CreateNewBillCompletedEventArgs>(service_CreateNewBillCompleted);
            service.GetBillByIDCompleted += new EventHandler<GetBillByIDCompletedEventArgs>(service_GetBillByIDCompleted);
            service.UpdateBillCompleted += new EventHandler<UpdateBillCompletedEventArgs>(service_UpdateBillCompleted);
            service.ApproveBillCompleted += new EventHandler<ApproveBillCompletedEventArgs>(service_ApproveBillCompleted);
            service.VerifyBillCompleted += new EventHandler<VerifyBillCompletedEventArgs>(service_VerifyBillCompleted);
        }

        #region Events...

        public event EventHandler ResetForm;
        public void RaiseResetForm()
        {
            if (ResetForm != null)
                ResetForm(this, new EventArgs());
        }

        public event EventHandler ResetDatagrid;
        public void RaiseResetDatagrid()
        {
            if (ResetDatagrid != null)
                ResetDatagrid(this, new EventArgs());
        }

        #endregion

        #region Completed Events...

        void service_VerifyBillCompleted(object sender, VerifyBillCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                Base.ShowError(e.Error);
                IsBusy = false;
            }
            else
            {
                MessageBox.Show("Bill Verified !");
                NewBill = e.Result;
                Base.AppEvents.RaiseNewBillAdded();
                IsBusy = false;
            }
        }

        void service_ApproveBillCompleted(object sender, ApproveBillCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                Base.ShowError(e.Error);
                IsBusy = false;
            }
            else
            {
                MessageBox.Show("Bill Approved !");
                NewBill = e.Result;
                Base.AppEvents.RaiseNewBillAdded();
                IsBusy = false;
                ControlState = ControlStates.Detail;
            }
        }

        void service_UpdateBillCompleted(object sender, UpdateBillCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                Base.ShowError(e.Error);
                IsBusy = false;
            }
            else
            {
                MessageBox.Show("Bill Updated !");
                NewBill = e.Result;
                IsBusy = false;
            }
        }

        void service_GetBillByIDCompleted(object sender, GetBillByIDCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                Base.ShowError(e.Error);
                IsBusy = false;
            }
            else
            {
                NewBill = e.Result;
                BillDetailforPrint = e.Result;

                if (NewBill.BillingLines != null)              //For setting SrNo. in detail view
                {
                    int i = 0;
                    foreach (var item in NewBill.BillingLines)
                    {
                        i++;
                        item.SrNo = i;
                    }
                }

                IsBusy = false;
                Reset();
                ControlState = ControlStates.Edit;
            }
        }

        void service_CreateNewBillCompleted(object sender, CreateNewBillCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                Base.ShowError(e.Error);
                IsBusy = false;
            }
            else
            {
                MessageBox.Show("Bills Added !");
                Base.AppEvents.RaiseNewBillAdded();
                IsBusy = false;
            }
        }

        #endregion

        #region Properties...

        MBilling _NewBill = new MBilling();
        public MBilling NewBill
        {
            get { return _NewBill; }
            set
            {
                _NewBill = value;
                Notify("NewBill");
                NotifyAllCans();
            }
        }

        MBilling _BillDetailforPrint = new MBilling();
        public MBilling BillDetailforPrint
        {
            get { return _BillDetailforPrint; }
            set
            {
                _BillDetailforPrint = value;
                Notify("BillDetailforPrint");
            }
        }

        public bool CanApprove
        {
            get 
            { 
                return !NewBill.IsApproved.Value && Base.Current.Rights.CanApproveBill; 
            }
        }

        public bool CanEdit
        {
            get
            {
                return !NewBill.IsApproved.Value && Base.Current.Rights.CanApproveBill;
            }
        }

        public bool CanPrint
        {
            get
            {
                return !Base.Current.IsInsertMode;
            }
        }

        string _ValidationStatus;
        public string ValidationStatus
        {
            get { return _ValidationStatus; }
            set
            {
                _ValidationStatus = value;
                Notify("ValidationStatus");
            }
        }

        public bool IsResetForm { get; set; }

        #endregion

        #region Methods...

        public void NotifyAllCans()
        {
            foreach (var prop in this.GetType().GetProperties())
            {
                if (prop.Name.StartsWith("can", StringComparison.OrdinalIgnoreCase))
                    Notify(prop.Name);
            }
        }
        
        public void SubmittoDB()
        {
            if (Validate())
            {
                service.CreateNewBillAsync(NewBill);
                IsBusy = true;
                IsResetForm = true;
            }
            else
            {
                IsResetForm = false;
            }
        }

        public void Reset()
        {
            NewBill = new MBilling();
            Notify("NewBill");
        }

        public void UpdateGrandTotal()
        {
            NewBill.UpdateGrossAmount();
            Notify("NewBill");
        }

        public void UpdateNetAmount()
        {
            NewBill.UpdateNetAmount();
            Notify("NewBill");
        }

        public void UpdateTotalAmount()
        {
            NewBill.UpdateTotalAmount();
            Notify("NewBill");
        }

        public void UpdatePayNotBefore()
        {
            NewBill.UpdatePayNotBefore();
            Notify("NewBill");
        }

        public void GetBillbyID(int id)
        {
            service.GetBillByIDAsync(id);
            IsBusy = true;
        }

        public void UpdateBill()
        {
            service.UpdateBillAsync(NewBill);
            IsBusy = true;
        }

        public void ApproveBill()
        {
            NewBill.ApprovedOn = DateTime.UtcNow.AddHours(5.5);
            NewBill.ApprovedBy = Base.Current.UserName;
            NewBill.IsApproved = true;
            service.ApproveBillAsync(NewBill);
            IsBusy = true;
        }

        public void VerifyBill()
        {
            service.VerifyBillAsync(NewBill, Base.Current.UserName);
            IsBusy = true;
        }

        public void DeleteLine(int id)
        {
            NewBill.DeleteLine(id);
            Notify("NewBill");
        }

        public bool Validate()
        {
            ValidationStatus = "";
            if (!NewBill.VendorID.HasValue) { ValidationStatus = "Please Select Vendor"; return false; }
            if (NewBill.SiteID == 0) { ValidationStatus = "Please Select Site"; return false; }
            if (String.IsNullOrWhiteSpace(NewBill.BillNo)) { ValidationStatus = "Bill No. Cannot be Empty!"; return false; }
            if (NewBill.BillingLines == null) { ValidationStatus = "Bill should have alteast one item!"; return false; }
            if (NewBill.BillingLines.Count == 0) { ValidationStatus = "Bill should have alteast one item!"; return false; }
            if (NewBill.BillingLines.Any(x => x.ActualQty == 0)) { ValidationStatus = "Billing Lines has some invaid/unselected Quantity!"; return false; }
            if (NewBill.BillingLines.Any(x => x.ActualPrice == 0)) { ValidationStatus = "Billing Lines has some invaid/unselected Price!"; return false; }
            return true;
        }

        #endregion
    }
}