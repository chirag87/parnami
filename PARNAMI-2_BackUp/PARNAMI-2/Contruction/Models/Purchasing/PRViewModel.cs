﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Contruction.PurchasingService;
using System.Collections.ObjectModel;
using SIMS.MM.MODEL;
using System.ComponentModel;
using System.Windows.Data;
using SIMS.Core;
using System.Collections.Generic;

namespace Contruction
{
    public class PRViewModel : ViewModel
    {
        PurchasingServiceClient service = new PurchasingServiceClient();
        
        public PRViewModel()
        {
            if (!DesignerProperties.GetIsInDesignMode(Application.Current.RootVisual))
            {
                OnInit();
            }
        }

        public event EventHandler PRChanged;
        public void RaisePRChanged()
        {
            if (PRChanged != null)
                PRChanged(this, new EventArgs());
        }

        IPaginated<MPRHeader> _PRs = new PaginatedData<MPRHeader>();
        public IPaginated<MPRHeader> PRs
        {
            get { return _PRs; }
            set
            {
                _PRs = value;
                Notify("PRs");
                RaisePRChanged();
            }
        }

        public void OnInit()
        {
            service.GetAllPRsCompleted +=new EventHandler<GetAllPRsCompletedEventArgs>(service_GetAllPRsCompleted);
            LoadPRs();
            service.DeletePRCompleted += new EventHandler<System.ComponentModel.AsyncCompletedEventArgs>(service_DeletePRCompleted);
            Base.AppEvents.PRDataChanged += new EventHandler(AppEvents_PRDataChanged);
            StatusList = (new string[] { "Draft", "SubmittedForVerification", "SubmittedForPRApproval", "PRApproved", "SubmittedForPOApproval", "POApproved", "Closed", "All" });
        }

        void service_DeletePRCompleted(object sender, System.ComponentModel.AsyncCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                Base.ShowError(e.Error);
                IsBusy = false;
            }
            else
            {
                IsBusy = false;
                MessageBox.Show("Deletion Successfull !");
                LoadPRs();
            }
        }

        void AppEvents_PRDataChanged(object sender, EventArgs e)
        {
            LoadPRs();
        }

        void service_GetAllPRsCompleted(object sender, GetAllPRsCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                Base.ShowError(e.Error);
                IsBusy = false;
            }
            else
            {
                PRs = e.Result;
                IsBusy = false;
            }
        }

        int _PageIndex = 0;
        public int PageIndex
        {
            get { return _PageIndex; }
            set
            {
                _PageIndex = value;
                Notify("PageIndex");
                LoadPRs();
            }
        }

        string _SearchKey;
        public string SearchKey
        {
            get { return _SearchKey; }
            set
            {
                _SearchKey = value;
                Notify("SearchKey");
            }
        }

        int? _VendorID;
        public int? VendorID
        {
            get { return _VendorID; }
            set
            {
                _VendorID = value;
                Notify("VendorID");
            }
        }

        int? _SiteID;
        public int? SiteID
        {
            get { return _SiteID; }
            set
            {
                _SiteID = value;
                Notify("SiteID");
            }
        }

        string _Status;
        public string Status
        {
            get { return _Status; }
            set
            {
                _Status = value;
                Notify("Status");
            }
        }

        IEnumerable<string> _StatusList = new List<string>();
        public IEnumerable<string> StatusList
        {
            get { return _StatusList; }
            set
            {
                _StatusList = value;
                Notify("StatusList");
            }
        }

        public void Clear()
        {
            SearchKey = null;
            VendorID = null;
            SiteID = null;
            Status = null;
            LoadPRs();
        }

        public void LoadPRs()
        {
            if (Status == " " || Status == "All") Status = null;
            service.GetAllPRsAsync(Base.Current.UserName, PageIndex, null, SearchKey, VendorID, SiteID, Status);
            IsBusy = true;
        }

        public void DeletePR(MPRHeader DeleteRow)
        {
            if (DeleteRow.ApprovalStatus == "Pending Approval")
            {
                var Result = MessageBox.Show("Sure about Deleting this record?", "Warning", MessageBoxButton.OKCancel);
                if (Result == MessageBoxResult.OK)
                {
                    service.DeletePRAsync(DeleteRow.PRNo);
                    IsBusy = true;
                }
                else
                {
                    MessageBox.Show("Row Not Deleted");
                }
            }
            else
            {
                MessageBox.Show("Can Not Delete Approved Purchase Requisition !");
            }
        }

        internal void MoveToPage(int p)
        {
            PageIndex = p;
        }
    }
}
