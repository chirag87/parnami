﻿using System;
using System.Net;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Contruction.PurchasingService;
using System.Collections.ObjectModel;
using SIMS.MM.MODEL;
using System.ComponentModel;
using Contruction.MOService;

namespace Contruction
{
    public class ViewPRDetailsModel : ViewModel
    {
        PurchasingServiceClient service = new PurchasingServiceClient();
        MOServiceClient moservice = new MOServiceClient();

        public ViewPRDetailsModel()
        {
            if (!DesignerProperties.GetIsInDesignMode(Application.Current.RootVisual))
            {
                OnInit();
            }
        }

        #region EVENTS...

        public event EventHandler POConverted;
        public void RaisePOConverted()
        {
            if (POConverted != null)
                POConverted(this, new EventArgs());
        }

        #endregion

        #region METHODS...

        public void OnInit()
        {
            service.GetPRbyIDCompleted += new EventHandler<GetPRbyIDCompletedEventArgs>(service_GetPRbyIDCompleted);
            service.ApprovePRCompleted += new EventHandler<AsyncCompletedEventArgs>(service_ApprovePRCompleted);
            service.ConvertToPOCompleted += new EventHandler<ConvertToPOCompletedEventArgs>(service_ConvertToPOCompleted);
            service.VerifyPRCompleted += new EventHandler<AsyncCompletedEventArgs>(service_VerifyPRCompleted);
            service.InitialApprovePRCompleted += new EventHandler<AsyncCompletedEventArgs>(service_InitialApprovePRCompleted);
            service.UpdatePRCompleted += new EventHandler<UpdatePRCompletedEventArgs>(service_UpdatePRCompleted);
            service.SaveandApproveCompleted += new EventHandler<SaveandApproveCompletedEventArgs>(service_SaveandApproveCompleted);
            service.SaveandInitialApproveCompleted += new EventHandler<SaveandInitialApproveCompletedEventArgs>(service_SaveandInitialApproveCompleted);
            service.SaveandVerifyCompleted += new EventHandler<SaveandVerifyCompletedEventArgs>(service_SaveandVerifyCompleted);
            service.EmailPRCompleted += new EventHandler<AsyncCompletedEventArgs>(service_EmailPRCompleted);
            service.ChangesStateofPRCompleted += new EventHandler<AsyncCompletedEventArgs>(service_ChangesStateofPRCompleted);
        }

        public void ChangeState(int prid, string state)
        {
            service.ChangesStateofPRAsync(prid, state);
            IsBusy = true;
        }

        public void GetPR(int prid)
        {
            service.GetPRbyIDAsync(prid);
            IsBusy = true;
            IsLoading = true;
        }

        public void VerifyPR()
        {
            Status = "Verifying ...";
            if (!Validate()) return;
            //service.VerifyPRAsync(ConvertedPR.PRNumber, Base.Current.UserName, DateTime.UtcNow.AddHours(5.5));
            service.SaveandVerifyAsync(ConvertedPR, Base.Current.UserName);
            IsBusy = true;
            IsLoading = true;
        }

        public void InitiallyApprovePR()
        {
            Status = "Approving ...";
            if (!Validate()) return;
            //service.InitialApprovePRAsync(ConvertedPR.PRNumber, Base.Current.UserName, DateTime.UtcNow.AddHours(5.5));
            service.SaveandInitialApproveAsync(ConvertedPR, Base.Current.UserName);
            IsBusy = true;
            IsLoading = true;
        }

        public void ApprovePR()
        {
            Status = "Approving ...";
            if (!Validate()) return;
            service.SaveandApproveAsync(ConvertedPR, Base.Current.UserName);
            IsBusy = true;
            IsLoading = true;
        }

        public void convertToPO()
        {
            service.ConvertToPOAsync(ConvertedPR);
            IsBusy = true;
            IsLoading = true;
        }

        public void Email()
        {
            //if(ConvertedPR.IsApproved)
            //{
            service.EmailPRAsync(ConvertedPR.PRNumber);
            IsBusy = true;
            //}
            //else
            //{
            //    MessageBox.Show("PR is not Approved! Operation not Allowed!");
            //}
        }

        public void UpdatePR()
        {
            Status = "Updating ...";
            if (!Validate()) return;
            service.UpdatePRAsync(ConvertedPR);
            IsBusy = true;
            IsLoading = true;
        }

        public void UpdatePRafterSplit(MPR convertedPR)
        {
            Status = "Updating ...";
            service.UpdatePRAsync(convertedPR);
            IsBusy = true;
            IsLoading = true;
        }

        public void NotifyAllCans()
        {
            foreach (var prop in this.GetType().GetProperties())
            {
                if (prop.Name.StartsWith("can", StringComparison.OrdinalIgnoreCase))
                    Notify(prop.Name);
            }
        }

        public void UpdateTotal()
        {
            ConvertedPR.UpdateTotalValue();
            Notify("ConvertedPR");
        }

        public void UpdateNetAmount()
        {
            ConvertedPR.UpdateNetAmount();
            Notify("ConvertedPR");
        }

        public void UpdateTotalDiscount()
        {
            ConvertedPR.UpdateTotalDiscount();
            Notify("ConvertedPR");
        }

        internal void SetID(int p)
        {
            GetPR(p);
            IsLoading = true;
        }

        public void AddNewLine()
        {
            ConvertedPR.AddNewLine();
            Notify("ConvertedPR");
        }

        public void DeleteLine(int lid)
        {
            try
            {
                var line = ConvertedPR.PRLines.Single(x => x.LineID == lid);
                ConvertedPR.PRLines.Remove(line);
                UpdateTotal();
                ConvertedPR.UpdateLineNumbers();
            }
            catch { }
        }

        public bool Validate()
        {
            if (ConvertedPR.VendorID == 0) { Status = "Please Select Vendor"; return false; }
            if (ConvertedPR.SiteID == 0) { Status = "Please Select Site"; return false; }
            if (String.IsNullOrWhiteSpace(ConvertedPR.Purchaser)) { Status = "Purchaser Cannot be Empty!"; return false; }
            if (String.IsNullOrWhiteSpace(ConvertedPR.PurchaseType)) { Status = "Please Select Purchase Type"; return false; }
            if (ConvertedPR.PRLines == null) { Status = "Material Indent should have alteast one item."; return false; }
            if (ConvertedPR.PRLines.Count == 0) { Status = "Material Indent should have alteast one item."; return false; }
            if (ConvertedPR.PRLines.Any(x => x.ItemID == 0)) { Status = "Indent Lines has some invaid/unselected Items!"; return false; }
            return true;
        }

        #endregion

        #region COMPLETED EVENTS...

        void service_ChangesStateofPRCompleted(object sender, AsyncCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                Base.ShowError(e.Error);
                IsBusy = false;
            }
            else
            {
                if (!Validate()) return;
                Base.AppEvents.RaisePRDataChanged();
                Notify("ConvertedPR");
                IsBusy = false;
            }
            IsLoading = false;
        }

        void service_InitialApprovePRCompleted(object sender, AsyncCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                Base.ShowError(e.Error);
                IsBusy = false;
            }
            else
            {
                if (!Validate()) return;
                MessageBox.Show("Material Indent Initially Approved successfully !");
                Base.AppEvents.RaisePRDataChanged();
                Status = "Initially Approved!";
                IsBusy = false;
            }
            IsLoading = false;
        }

        void service_VerifyPRCompleted(object sender, AsyncCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                Base.ShowError(e.Error);
                IsBusy = false;
            }
            else
            {
                if (!Validate()) return;
                MessageBox.Show("Material Indent Verified successfully !");
                Base.AppEvents.RaisePRDataChanged();
                Status = "Verified!";
                IsBusy = false;
            }
            IsLoading = false;
        }

        void service_ConvertToPOCompleted(object sender, ConvertToPOCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                Base.ShowError(e.Error);
                IsBusy = false;
            }
            else
            {
                IsBusy = false;
                MessageBox.Show("Successfully Converted to Purchase Order !");
                Base.AppEvents.RaisePOConverted(e.Result);
            }
            IsLoading = false;
        }

        void service_EmailPRCompleted(object sender, AsyncCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                Base.ShowError(e.Error);
                IsBusy = false;
            }
            else
            {
                IsBusy = false;
                MessageBox.Show("Email Sent Successfully!");
            }
        }

        void service_SaveandVerifyCompleted(object sender, SaveandVerifyCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                Base.ShowError(e.Error);
                IsBusy = false;
            }
            else
            {
                if (!Validate()) return;
                ConvertedPR = e.Result;
                Base.AppEvents.RaisePRDataChanged();
                Status = "Verified!";
                IsBusy = false;
            }
            IsLoading = false;
        }

        void service_SaveandInitialApproveCompleted(object sender, SaveandInitialApproveCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                Base.ShowError(e.Error);
                IsBusy = false;
            }
            else
            {
                if (!Validate()) return;
                ConvertedPR = e.Result;
                Base.AppEvents.RaisePRDataChanged();
                Status = "Initially Approved!";
                IsBusy = false;
            }
            IsLoading = false;
        }

        void service_SaveandApproveCompleted(object sender, SaveandApproveCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                Base.ShowError(e.Error);
                IsBusy = false;
            }
            else
            {
                if (!Validate()) return;
                ConvertedPR = e.Result;
                Base.AppEvents.RaisePRDataChanged();
                Status = "Approved!";
                IsBusy = false;
            }
            IsLoading = false;
        }

        void service_UpdatePRCompleted(object sender, UpdatePRCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                Base.ShowError(e.Error);
                IsBusy = false;
            }
            else
            {
                if (!Validate()) return;
                ConvertedPR = e.Result;
                Base.AppEvents.RaisePRDataChanged();
                Status = "Saved!";
                IsBusy = false;
            }
            IsLoading = false;
        }

        void service_ApprovePRCompleted(object sender, AsyncCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                Base.ShowError(e.Error);
                IsBusy = false;
            }
            else
            {
                if (!Validate()) return;
                MessageBox.Show("Material Indent Approved successfully !");
                Status = "Approved!";
                IsBusy = false;
            }
            IsLoading = false;
        }

        void service_GetPRbyIDCompleted(object sender, GetPRbyIDCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                Base.ShowError(e.Error);
                IsBusy = false;
            }
            else
            {
                ConvertedPR = e.Result;
                IsBusy = false;
            }
            IsLoading = false;
        }

        #endregion

        #region PROPERTIES...

        ObservableCollection<MSplittedLines> _SplittedLines = new ObservableCollection<MSplittedLines>();
        public ObservableCollection<MSplittedLines> SplittedLines
        {
            get { return _SplittedLines; }
            set
            {
                _SplittedLines = value;
                Notify("SplittedLines");
            }
        }

        MMOSplittedHeader _MOSplit = new MMOSplittedHeader();
        public MMOSplittedHeader MOSplit
        {
            get { return _MOSplit; }
            set
            {
                _MOSplit = value;
                Notify("MOSplit");
            }
        }

        MPR _ConvertedPR = new MPR();
        public MPR ConvertedPR
        {
            get { return _ConvertedPR; }
            set
            {
                _ConvertedPR = value;
                Notify("ConvertedPR");
                NotifyAllCans();
            }
        }

        bool _IsPR = false;
        public bool IsPR
        {
            get { return _IsPR; }
            set
            {
                _IsPR = value;
                Notify("IsPR");
            }
        }

        bool _IsPO = false;
        public bool IsPO
        {
            get { return _IsPO; }
            set
            {
                _IsPO = value;
                Notify("IsPO");
            }
        }

        string _Status;
        public string Status
        {
            get { return _Status; }
            set
            {
                _Status = value;
                Notify("Status");
            }
        }

        public override bool CanEdit
        {
            get { return !ConvertedPR.IsApproved && Base.Current.Rights.CanApprovePR; }
        }

        public bool CanClosed
        {
            get
            {
                return !ConvertedPR.IsClosed;
            }
        }

        public bool CanEditItems
        {
            get { return !ConvertedPR.IsVerified && Base.Current.Rights.CanApprovePR; }
        }

        public bool CanSubmitToVerify
        {
            get { return !ConvertedPR.IsVerified && (ConvertedPR.PRStatus == "Draft" || ConvertedPR.PRStatus == "PRRejected") && Base.Current.Rights.CanApprovePR; }
        }

        public bool CanVerify
        {
            get { return !ConvertedPR.IsVerified && ConvertedPR.PRStatus == "SubmittedForVerification" && Base.Current.Rights.CanApprovePR; }
        }

        public bool CanInitialApprove
        {
            get { return ConvertedPR.IsVerified && ConvertedPR.PRStatus == "SubmittedForPRApproval" && !ConvertedPR.IsReqApproved && Base.Current.Rights.CanApprovePR; }
        }

        public bool CanSubmitToPOApprove
        {
            get { return ConvertedPR.IsVerified && ConvertedPR.IsReqApproved && (ConvertedPR.PRStatus == "PRApproved" || ConvertedPR.PRStatus == "PORejected") && Base.Current.Rights.CanApprovePR; }
        }

        public bool CanApprove
        {
            get { return ConvertedPR.IsVerified && ConvertedPR.IsReqApproved && ConvertedPR.PRStatus == "SubmittedForPOApproval" && !ConvertedPR.IsApproved && Base.Current.Rights.CanApprovePR; }
        }

        public bool CanSave
        {
            get { return !ConvertedPR.IsApproved && Base.Current.Rights.CanRaisePR; }
        }

        public bool CanAcknowledge
        {
            get { return ConvertedPR.ConvertedToPO && Base.Current.Rights.CanRaisePO; }
        }

        public bool CanConvertToPO
        {
            get { return ConvertedPR.IsApproved && ConvertedPR.PRStatus == "POApproved" && !ConvertedPR.ConvertedToPO && Base.Current.Rights.CanRaisePO; }
        }

        bool _ApprovalStatus = false;
        public bool ApprovalStatus
        {
            get { return _ApprovalStatus; }
            set
            {
                _ApprovalStatus = value;
                Notify("ApprovalStatus");
            }
        }

        bool _IsLoading = false;
        public bool IsLoading
        {
            get { return _IsLoading; }
            set
            {
                _IsLoading = value;
                Notify("IsLoading");
            }
        }

        #endregion
    }
}
