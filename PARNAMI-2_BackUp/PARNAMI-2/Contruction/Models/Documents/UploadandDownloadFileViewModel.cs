﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.ComponentModel;
using Contruction.FileService;
using SIMS.MM.MODEL;

namespace Contruction
{
    public class UploadandDownloadFileViewModel : ViewModel
    {
        FilesServiceClient service = new FilesServiceClient();

        public UploadandDownloadFileViewModel()
        {
            if (!DesignerProperties.GetIsInDesignMode(Application.Current.RootVisual))
            {
                OnInit();
            }
        }

        public void OnInit()
        {
            service.UploadPhotoCompleted += new EventHandler<UploadPhotoCompletedEventArgs>(service_UploadPhotoCompleted);
            Upload();
        }

        void service_UploadPhotoCompleted(object sender, UploadPhotoCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                Base.ShowError(e.Error);
                IsBusy = false;
            }
            else
            {
                MessageBox.Show("Upload Succeed !");
                IsBusy = false;
            }
        }

        MItem _PictureFile = new MItem();
        public MItem PictureFile
        {
            get { return _PictureFile; }
            set
            {
                _PictureFile = value;
                Notify("PictureFile");
            }
        }

        byte[] _PictureStream;
        public byte[] PictureStream
        {
            get { return _PictureStream; }
            set
            {
                _PictureStream = value;
                Notify("PictureStream");
            }
        }

        public void Upload()
        {
            service.UploadPhotoAsync(PictureFile);
            IsBusy = true;
        }
    }
}
