﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Contruction.MasterReportingService;
using SIMS.MM.MODEL;

namespace Contruction
{
    public class DetailedStockReportsModel : ViewModel
    {
        MasterReportingServiceClient service = new MasterReportingServiceClient();

        public DetailedStockReportsModel()
        {
            if (!DesignerProperties.GetIsInDesignMode(Application.Current.RootVisual))
            {
                OnInit();
                AddRTypes();
            }
        }

        #region Methods...

        public void OnInit()
        {
            service.GetStockReportCompleted += new EventHandler<GetStockReportCompletedEventArgs>(service_GetStockReportCompleted);
            Load();
        }

        public void AddRTypes()
        {
            RTypes.Add("pdf");
            RTypes.Add("Excel");
        }

        public void Load()
        {
            service.GetStockReportAsync(sdate, edate, SiteID, ItemID);
            IsBusy = true;
        }

        public void Clear()
        {
            SiteID = null;
            ItemID = null;
            Load();
        }

        #endregion

        #region Completed Events...

        void service_GetStockReportCompleted(object sender, GetStockReportCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                Base.ShowError(e.Error);
                IsBusy = false;
            }
            else
            {
                Data = e.Result;
                IsBusy = false;
            }
        }

        #endregion

        #region Properties...

        string _SearchKey;
        public string SearchKey
        {
            get { return _SearchKey; }
            set
            {
                _SearchKey = value;
                Notify("SearchKey");
            }
        }

        ObservableCollection<MStockReport> _Data = new ObservableCollection<MStockReport>();
        public ObservableCollection<MStockReport> Data
        {
            get { return _Data; }
            set
            {
                _Data = value;
                Notify("Data");
            }
        }

        DateTime? _sdate;
        public DateTime? sdate
        {
            get { return _sdate; }
            set
            {
                _sdate = value;
                Notify("sdate");
            }
        }

        DateTime? _edate;
        public DateTime? edate
        {
            get { return _edate; }
            set
            {
                _edate = value;
                Notify("edate");
            }
        }

        int? _SiteID;
        public int? SiteID
        {
            get { return _SiteID; }
            set
            {
                _SiteID = value;
                Notify("SiteID");
                Load();
            }
        }

        int? _ItemID;
        public int? ItemID
        {
            get { return _ItemID; }
            set
            {
                _ItemID = value;
                Notify("ItemID");
                Load();
            }
        }

        int? _catid;
        public int? catid
        {
            get { return _catid; }
            set
            {
                _catid = value;
                Notify("catid");
            }
        }

        string _RType;
        public string RType
        {
            get { return _RType; }
            set
            {
                _RType = value;
                Notify("RType");
            }
        }

        List<string> _RTypes = new List<string>();
        public List<string> RTypes
        {
            get { return _RTypes; }
            set
            {
                _RTypes = value;
                Notify("RTypes");
            }
        }

        #endregion
    }
}
