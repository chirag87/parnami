﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.ComponentModel;
using Contruction.MasterDBService;
using System.Collections.ObjectModel;
using SIMS.MM.MODEL;

namespace Contruction
{
    public class ContactsModel : ViewModel
    {
        MasterDataServiceClient service = new MasterDataServiceClient();

        public ContactsModel()
        {
            if (!DesignerProperties.GetIsInDesignMode(Application.Current.RootVisual))
            {
                OnInit();
            }
        }

        public void OnInit()
        {
            service.GetContactsbyForIDCompleted += new EventHandler<GetContactsbyForIDCompletedEventArgs>(service_GetContactsbyForIDCompleted);
            service.DeleteContactCompleted += new EventHandler<AsyncCompletedEventArgs>(service_DeleteContactCompleted);
            service.GetContactbyIDCompleted += new EventHandler<GetContactbyIDCompletedEventArgs>(service_GetContactbyIDCompleted);
            service.UpdateContactCompleted += new EventHandler<UpdateContactCompletedEventArgs>(service_UpdateContactCompleted);
            service.CreateNewContactCompleted += new EventHandler<CreateNewContactCompletedEventArgs>(service_CreateNewContactCompleted);
        }

        void service_CreateNewContactCompleted(object sender, CreateNewContactCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                Base.ShowError(e.Error);
                IsBusy = false;
            }
            else
            {
                MessageBox.Show("Contact created Successfully!");
                IsBusy = false;
                Load();
                Base.Current.RaiseVendorDetailUpdated();
            }
        }

        void service_UpdateContactCompleted(object sender, UpdateContactCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                Base.ShowError(e.Error);
                IsBusy = false;
            }
            else
            {
                SelectedContact = e.Result;
                IsBusy = false;
                Notify("SelectedContact");
                Load();
            }
        }

        void service_GetContactbyIDCompleted(object sender, GetContactbyIDCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                Base.ShowError(e.Error);
                IsBusy = false;
            }
            else
            {
                SelectedContact = e.Result;
                IsBusy = false;
                Notify("SelectedContact");
            }
        }

        void service_DeleteContactCompleted(object sender, AsyncCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                Base.ShowError(e.Error);
                IsBusy = false;
            }
            else
            {
                MessageBox.Show("Contact Deleted!");
                IsBusy = false;
                Load();
            }
        }

        void service_GetContactsbyForIDCompleted(object sender, GetContactsbyForIDCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                Base.ShowError(e.Error);
                IsBusy = false;
            }
            else
            {
                Contacts = e.Result;
                IsBusy = false;
                Notify("Contacts");
            }
        }


        #region Model

        ObservableCollection<MContact> _Contacts = new ObservableCollection<MContact>();
        public ObservableCollection<MContact> Contacts
        {
            get { return _Contacts; }
            set
            {
                _Contacts = value;
                Notify("Contacts");
            }
        }

        MContact _SelectedContact = new MContact();
        public MContact SelectedContact
        {
            get { return _SelectedContact; }
            set
            {
                _SelectedContact = value;
                Notify("SelectedContact");
            }
        }

        string _For;
        public string For
        {
            get { return _For; }
            set
            {
                _For = value;
                Load();
                Notify("For");
            }
        }

        int _ForID;
        public int ForID
        {
            get { return _ForID; }
            set
            {
                _ForID = value;
                Load();
                Notify("ForID");
            }
        }

        #endregion

        public void Load()
        {
            Contacts.Clear();
            if (String.IsNullOrWhiteSpace(For)) return;
            if (ForID == 0) return;
            service.GetContactsbyForIDAsync(ForID, For);
            IsBusy = true;
        }

        public void DeleteContact(int id)
        {
            var response = MessageBox.Show("Are You sure you want to delete this Contact ?", "Warning", MessageBoxButton.OKCancel);
            if (response.ToString() == "OK")
            {
                service.DeleteContactAsync(id);
                IsBusy = true;
            }
            else
            {
                MessageBox.Show("Contact NOT Deleted!");
            }
        }

        public void GetContact(int id)
        {
            service.GetContactbyIDAsync(id);
            IsBusy = true;
        }

        public void UpdateContact()
        {
            service.UpdateContactAsync(SelectedContact);
            IsBusy = true;
        }

        public void CreateContact()
        {
            service.CreateNewContactAsync(SelectedContact);
            IsBusy = true;
        }
    }
}
