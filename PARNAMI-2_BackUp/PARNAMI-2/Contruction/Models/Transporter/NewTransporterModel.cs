﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.ComponentModel;
using Contruction.MasterDBService;
using SIMS.MM.MODEL;

namespace Contruction
{
    public class NewTransporterModel : ViewModel
    {
        MasterDataServiceClient service = new MasterDataServiceClient();

        public NewTransporterModel()
        {
            if (!DesignerProperties.GetIsInDesignMode(Application.Current.RootVisual))
            {
                Reset();
                OnInit();
            }
        }

        MTransporter _NewTransporter;
        public MTransporter NewTransporter
        {
            get { return _NewTransporter; }
            set
            {
                _NewTransporter = value;
                Notify("NewTransporter");
            }
        }

        public void OnInit()
        {
            service.CreateNewTransporterCompleted += new EventHandler<AsyncCompletedEventArgs>(service_CreateNewTransporterCompleted);
        }

        void service_CreateNewTransporterCompleted(object sender, AsyncCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                Base.ShowError(e.Error);
                IsBusy = false;
            }
            else
            {
                IsBusy = false;
                MessageBox.Show("New Transporter Added Successfully !");
                Base.AppEvents.RaiseNewTransporterAdded();
            }
        }

        public void Reset()
        {
            NewTransporter = new MTransporter();
        }

        public void SubmitToDB()
        {
            service.CreateNewTransporterAsync(NewTransporter);
            IsBusy = true;
        }
    }
}
