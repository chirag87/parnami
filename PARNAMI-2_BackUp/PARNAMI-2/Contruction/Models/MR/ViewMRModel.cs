﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.ComponentModel;
using SIMS.MM.MODEL;
using Contruction.MRService;
using SIMS.Core;

namespace Contruction
{
    public class ViewMRModel : ViewModel
    {
        MRServiceClient service = new MRServiceClient();

        public event EventHandler MRChanged;
        public void RaiseMRChanged()
        {
            if (MRChanged != null)
                MRChanged(this, new EventArgs());
        }

        public ViewMRModel()
        {
            if (!DesignerProperties.GetIsInDesignMode(Application.Current.RootVisual))
            {
                OnInit();
            }
        }

        public void OnInit()
        {
            service.GetAllMRsCompleted += new EventHandler<GetAllMRsCompletedEventArgs>(service_GetAllMRsCompleted);
            LoadMRs();
            Base.AppEvents.MRDataChanged += new EventHandler(AppEvents_MRDataChanged);
        }

        void AppEvents_MRDataChanged(object sender, EventArgs e)
        {
            LoadMRs();
        }

        void service_GetAllMRsCompleted(object sender, GetAllMRsCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                Base.ShowError(e.Error);
                IsBusy = false;
            }
            else
            {
                MRs = e.Result;
                IsBusy = false;
            }
        }

        IPaginated<MMRHeader> _MRs = new PaginatedData<MMRHeader>();
        public IPaginated<MMRHeader> MRs
        {
            get { return _MRs; }
            set
            {
                _MRs = value;
                Notify("MRs");
                RaiseMRChanged();
            }
        }

        int _PageIndex = 0;
        public int PageIndex
        {
            get { return _PageIndex; }
            set
            {
                _PageIndex = value;
                Notify("PageIndex");
                LoadMRs();
            }
        }

        string _SearchKey;
        public string SearchKey
        {
            get { return _SearchKey; }
            set
            {
                _SearchKey = value;
                Notify("SearchKey");
            }
        }

        int? _SiteID;
        public int? SiteID
        {
            get { return _SiteID; }
            set
            {
                _SiteID = value;
                Notify("SiteID");
            }
        }

        public void Clear()
        {
            SearchKey = null;
            SiteID = null;
            LoadMRs();
        }

        public void LoadMRs()
        {
            service.GetAllMRsAsync(Base.Current.UserName, PageIndex, null, SearchKey, SiteID);
            IsBusy = true;
        }

        internal void MoveToPage(int p)
        {
            PageIndex = p;
        }
    }
}
