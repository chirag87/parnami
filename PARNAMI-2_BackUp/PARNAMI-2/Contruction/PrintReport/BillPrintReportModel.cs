﻿using System;
using System.Net;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using SIMS.MM.MODEL;

namespace Contruction
{
    public class BillPrintReportModel : ViewModel
    {
        public BillPrintReportModel()
        {

        }

        MBilling _SelectedBill = new MBilling();
        public MBilling SelectedBill
        {
            get { return _SelectedBill; }
            set
            {
                _SelectedBill = value;
                OnBillSet();
                Notify("SelectedBill");
            }
        }

        private void OnBillSet()
        {
            if (SelectedBill == null) return;
            Vendor = Base.Current.Vendors.Single(x => x.ID == SelectedBill.VendorID);
            Site = Base.Current.Sites.Single(x => x.ID == SelectedBill.SiteID);
            TotalInWords = "Rs. " + NumberToEnglish.changeCurrencyToWords(SelectedBill.TotalAmount.Value);
        }

        MVendor _Vendor = new MVendor();
        public MVendor Vendor
        {
            get { return _Vendor; }
            set
            {
                _Vendor = value;
                Notify("Vendor");
            }
        }

        MSite _Site = new MSite();
        public MSite Site
        {
            get { return _Site; }
            set
            {
                _Site = value;
                Notify("Site");
            }
        }

        string _TotalInWords;
        public string TotalInWords
        {
            get { return _TotalInWords; }
            set
            {
                _TotalInWords = value;
                Notify("TotalInWords");
            }
        }
    }
}
