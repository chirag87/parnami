﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;

namespace SIMS.VendorPayments
{
    public static class StaticDBExtensions
    {
        public static IQueryable<VendorPayment> FilterByVendor(this IQueryable<VendorPayment> qry, int? vendorid)
        {
            if (vendorid.HasValue)
                return qry.Where(x => x.VendorID == vendorid);
            return qry;
        }

        public static IQueryable<VendorPayment> FilterBySite(this IQueryable<VendorPayment> qry, int? siteid)
        {
            if (siteid.HasValue)
                return qry.Where(x => x.SiteID == siteid.Value);
            return qry;
        }
    }

    public partial class ConnectDBDataContext
    {
        public int GetMaxPaymentID()
        {
            if (VendorPayments.Count() == 0) return 0;
            return VendorPayments.Max(x => x.ID);
        }
    }
}
