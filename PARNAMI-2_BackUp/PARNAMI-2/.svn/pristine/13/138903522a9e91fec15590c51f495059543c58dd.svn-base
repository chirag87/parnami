﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace SIMS.MM.MODEL
{
    [DataContract]
    public partial class MPR : EntityModel
    {
        public MPR()
        {
            PRLines = new ObservableCollection<MPRLine>();
        }

        [DataMember]
        [Display(AutoGenerateField = false)]
        public int PRNumber { get; set; }

        [DataMember]
        public string ReferenceNo { get; set; }

        //[DataMember]
        //public DateTime RaisedOn { get; set; }
        DateTime _RaisedOn;
        [DataMember]
        public DateTime RaisedOn
        {
            get
            {
                if (_RaisedOn < DateTime.UtcNow.AddHours(5.5))
                    return _RaisedOn;
                else
                    return DateTime.UtcNow.AddHours(5.5);
            }
            set
            {
                _RaisedOn = value;
                Notify("RaisedOn");
            }
        }

        [DataMember]
        public string RaisedBy { get; set; }

        [DataMember]
        public string CurrentOwnerID 
        {
            get { return "DBA"; }
            set { }
        }

        [DataMember]
        public string ApprovedBy { get; set; }

        [DataMember]
        public int VendorID { get; set; }

        [DataMember]
        public string Vendor { get; set; }

        [DataMember]
        public int SiteID { get; set; }

        [DataMember]
        public string ContactPerson { get; set; }

        [DataMember]
        public string ContactMobile { get; set; }

        [DataMember]
        public string Site { get; set; }

        [DataMember]
        public string Purchaser { get; set; }

        [DataMember]
        public string PurchaseType { get; set; }

        [DataMember]
        public string PRRemarks { get; set; }

        [DataMember]
        public bool IsApproved { get; set; }

        [DataMember]
        public string ApprovalStatus { get; set; }

        [DataMember]
        public bool ConvertedToPO { get; set; }

        [DataMember]
        public double TotalValue { get; set; }

        double _TotalDiscount = 0;
        [DataMember]
        public double TotalDiscount
        {
            get { return _TotalDiscount; }
            set
            {
                _TotalDiscount = value;
                Notify("TotalDiscount");
            }
        }

        public event EventHandler OnpVATChanged;
        public void RaiseOnpVATChanged()
        {
            if (OnpVATChanged != null)
                OnpVATChanged(this, new EventArgs());
        }

        double _pVAT;
        [DataMember]
        public double pVAT 
        {
            get { return _pVAT; }
            set {
                if (_pCST > 0)
                {
                    _pVAT = 0;
                    return;
                }
                _pVAT = value; 
                RaiseOnpVATChanged(); 
            } 
        }

        public event EventHandler OnpCSTChanged;
        public void RaiseOnpCSTChanged()
        {
            if (OnpCSTChanged != null)
                OnpCSTChanged(this, new EventArgs());
        }

        double _pCST;
        [DataMember]
        public double pCST 
        {
            get { return _pCST; }
            set {
                if (_pVAT > 0)
                {
                    _pCST = 0;
                    return;
                }
                _pCST = value; 
                RaiseOnpCSTChanged(); 
            } 
        }

        [DataMember]
        public string PaymentTerms { get; set; }

        [DataMember]
        public double TAX { get; set; }

        [DataMember]
        public double Frieght { get; set; }

		[DataMember]
        public double NetAmount
		{
			get { return GrandTotal - TotalDiscount; }
			set { }
		}

        [DataMember]
        public double GrandTotal 
        {
            get
            {
                return ((100 + pVAT + pCST)/100 * TotalValue) + Frieght;
            }
            set { }
        }

        [DataMember]
        public ObservableCollection<MPRLine> PRLines 
        { 
            get; 
            set; 
        }

        public void UpdateTotalValue()
        {
            if (!Validate()) return;
            //PRLines.ToList().ForEach(x => x.UpdateLinePrice());
            TotalValue = PRLines.Sum(x => x.TotalLinePrice);
        }

        public void UpdateNetAmount()
        {
            NetAmount = GrandTotal - TotalDiscount;
        }

        public void UpdateTotalDiscount()
        {
            TotalDiscount = PRLines.Sum(x => x.Discount);
        }

        public MPRLine AddNewLine()
        {
            if (this.PRLines == null)
                this.PRLines = new ObservableCollection<MPRLine>();
            var line = new MPRLine();
            this.PRLines.Add(line);
            this.UpdateLineNumbers();
            Notify("PRLines");
            return line;
        }

        public bool Validate()
        {
            if (_pCST > 100 || _pCST < 0) return false;
            if (_pVAT > 100 || _pVAT < 0) return false;
            return true;
        }

        public int UpdateLineNumbers()
        {
            if (this.PRLines == null) return 0;
            int i = 1;
            foreach (var item in this.PRLines)
            {
                item.LineID = i;
                i++;
            }
            return i - 1;
        }
    }
}
