﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SIMS.TM.Model;
using System.ServiceModel;

namespace SIMS.TM.Services
{
    [ServiceContract]
    public interface IMBookService
    {
        [OperationContract]
        MMBook GetMBookDatails(int id);

        [OperationContract]
        PaginatedData<MMBook> GetAllMBook(string username, int? pageindex, int? pagesize, string Key, DateTime? startDate, DateTime? endDate, int? sideid, bool showall = true);

        [OperationContract]
        MMBook GetMBookID(int MBookID);

        [OperationContract]
        MMBook CreateMBook(MMBook _MBook);

        [OperationContract]
        MMBook UpdateMBook(MMBook _MBook);
    }
}
