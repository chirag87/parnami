﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SIMS.MM.MODEL.Headers;
using SIMS.MM.MODEL;

namespace SIMS.VendorPayments
{

    public abstract class Converter<T1, T2>
    {
        public abstract T2 ConvertTo(T1 _T1);
        public abstract T1 ConvertFrom(T2 _T1);
    }

    public class PaymentDetailConverter : Converter<MVendorPaymentDetail, VendorPaymentDetail>
    {
        public override VendorPaymentDetail ConvertTo(MVendorPaymentDetail _T1)
        {
            return new VendorPaymentDetail()
            {
                BillDate = _T1.BillDate,
                BillID = _T1.BillID,
                BillNo = _T1.BillNo,
                HeaderID = _T1.HeaderID,
                ID = _T1.LineID,
                Paid = _T1.Paid,
                Payment = _T1.Payment,
                Remarks = _T1.Remarks,
                Total = _T1.Total,
            };
        }

        public override MVendorPaymentDetail ConvertFrom(VendorPaymentDetail _T1)
        {
            return new MVendorPaymentDetail()
            {
                BillDate = _T1.BillDate,
                BillID = _T1.BillID,
                BillNo = _T1.BillNo,
                HeaderID = _T1.HeaderID,
                LineID = _T1.ID,
                Paid = _T1.Paid,
                Payment = _T1.Payment,
                Remarks = _T1.Remarks,
                Total = _T1.Total
            };
        }
    }

    public class PaymentConverter : Converter<MVendorPayment, VendorPayment>
    {
        internal ConnectDBDataContext db;
        PaymentDetailConverter detailConverter = new PaymentDetailConverter();

        public override VendorPayment ConvertTo(MVendorPayment _T1)
        {
            var obj = new VendorPayment()
            {
                SiteID = _T1.SiteID,
                TDS = _T1.TDS,
                Mode = _T1.Mode,
                GivenTo = _T1.GivenTo,
                ID = _T1.ID,
                NetPayment = _T1.NetPayment,
                PayedBy = _T1.PayedBy,
                PayeeName = _T1.PayeeName,
                PaymentDate = _T1.PaymentDate,
                RefNo = _T1.RefNo,
                Remarks = _T1.Remarks,
                Total = _T1.Total,
                VendorID = _T1.VendorID,
            };
            _T1.Details.ToList().ForEach(x => obj.VendorPaymentDetails.Add(detailConverter.ConvertTo(x)));
            return obj;
        }

        public override MVendorPayment ConvertFrom(VendorPayment _T1)
        {
            var obj = new MVendorPayment()
            {
                SiteID = _T1.SiteID,
                TDS = _T1.TDS,
                Mode = _T1.Mode,
                GivenTo = _T1.GivenTo,
                ID = _T1.ID,
                NetPayment = _T1.NetPayment,
                PayedBy = _T1.PayedBy,
                PayeeName = _T1.PayeeName,
                PaymentDate = _T1.PaymentDate,
                RefNo = _T1.RefNo,
                Remarks = _T1.Remarks,
                Total = _T1.Total,
                VendorID = _T1.VendorID,
                Vendor = db.GetVendorName(_T1.VendorID),
                Site = db.GetSiteName(_T1.SiteID)
            };

            _T1.VendorPaymentDetails.ToList().ForEach(x=> obj.Details.Add(detailConverter.ConvertFrom(x)));

            return obj;
        }
    }

    public class PendingBillDetailsConverter : Converter<vw_PendingBill, MPendingBillDetail>
    {
        public override MPendingBillDetail ConvertTo(vw_PendingBill _T1)
        {
            return new MPendingBillDetail()
            {
                ID = _T1.ID,
                DueDate = _T1.PaymentNotBefore,
                BillDate = _T1.BillDate,
                BillNo = _T1.BillNo,
                SiteID = _T1.SiteID??0,
                Site = _T1.Site,
                VendorID = _T1.VendorID??0,
                Vendor = _T1.Vendor,
                Total = _T1.TotalAmount??0,
                Pending = _T1.Pending??0,
                Remarks = _T1.Remarks,
            };
        }

        public override vw_PendingBill ConvertFrom(MPendingBillDetail _T1)
        {
            return new vw_PendingBill()
            {
                ID = _T1.ID,
                PaymentNotBefore = _T1.DueDate,
                BillDate = _T1.BillDate,
                BillNo = _T1.BillNo,
                SiteID = _T1.SiteID,
                Site = _T1.Site,
                VendorID = _T1.VendorID,
                Vendor = _T1.Vendor,
                TotalAmount = _T1.Total,
                Pending = _T1.Pending,
                Remarks = _T1.Remarks,
            };
        }
    }

    public partial class VendorPaymentManager
    {
        ConnectDBDataContext db = new ConnectDBDataContext();
    }

    public partial class VendorPaymentManager
    {
        public IQueryable<vw_PendingBill> GetPendingBills(int? VendorID, int? SiteID)
        {        
            var data = db.vw_PendingBills.Where(x =>
                //(!VendorID.HasValue || x.VendorID == VendorID)
                (x.VendorID == VendorID) && (x.SiteID == SiteID)
                //&& (!SiteID.HasValue || x.SiteID == SiteID)
                );
            return data;
        }

        public IEnumerable<MPendingBillDetail> GetPendingBillDetails(int? VendorID, int? SiteID)
        {
            PendingBillDetailsConverter ct = new PendingBillDetailsConverter();
            return GetPendingBills(VendorID, SiteID).Select(x => ct.ConvertTo(x)).ToList();
        }

        public MVendorPayment CreateNewVendorPayment(MVendorPayment _MPayment)
        {
            PaymentConverter converter = new PaymentConverter(){db = this.db};
            _MPayment.ID = db.GetMaxPaymentID() + 1;
            var Payment = converter.ConvertTo(_MPayment);
            db.VendorPayments.InsertOnSubmit(Payment);
            db.SubmitChanges();
            return converter.ConvertFrom(Payment);
        }

        public IPaginated<MVendorPaymentHeader> GetAllPayments(string username, int? pageindex, int? pagesize, int? vendorid, int? siteid)
        {
            var payments = db.VendorPayments
               .FilterByVendor(vendorid)
               .FilterBySite(siteid)
               .OrderByDescending(x => x.ID).AsQueryable();

            return payments.ToList()
               .ToPaginate(pageindex, pagesize).Select(x => new MVendorPaymentHeader()
            {
                GivenTo = x.GivenTo,
                ID = x.ID,
                Mode = x.Mode,
                NetPayment = x.NetPayment,
                PayedBy = x.PayedBy,
                PayeeName = x.PayeeName,
                RefNo = x.RefNo,
                Remarks = x.Remarks,
                PaymentDate = x.PaymentDate,
                SiteID = x.SiteID,
                TDS = x.TDS,
                Total = x.Total,
                VendorID = x.VendorID,
                Vendor = x.Vendor.DisplayVendor,
                Site = x.BUs.DisplaySite,
            });
        }
    }
}