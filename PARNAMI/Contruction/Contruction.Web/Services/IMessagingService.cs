﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using SIMS.MM.DAL;
using SIMS.MM.BLL;
using System.Collections.ObjectModel;
using SIMS.MM.MODEL;

namespace Contruction.Web.Services
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IMessagingService" in both code and config file together.
    [ServiceContract]
    public interface IMessagingService
    {
        [OperationContract]
        IEnumerable<MEmail> GetAllEmails();

        [OperationContract]
        PaginatedData<MMessaging> GetAllMessages(string username, int? pageindex, int? pagesize, string key);

        [OperationContract]
        PaginatedData<MMessagingHeader> GetAllMessageHeaders(string username, int? pageindex, int? pagesize, string key);

        [OperationContract]
        MMessaging GetMessageByID(int id);




        
        [OperationContract]
        MMessaging SendNewMail(MMessaging NewEmail);

        [OperationContract]
        bool SetIsStar(int msgid, string user, bool IsStar);

        [OperationContract]
        bool SetIsFlag(int msgid, string user, bool IsFlag);

        [OperationContract]
        bool SetIsRead(int msgid, string user, bool IsRead);

        [OperationContract]
        void DeleteMail(int Msgid, string user);
    }
}
