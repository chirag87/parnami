﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using SIMS.MM.MODEL.Headers;
using SIMS.MM.MODEL;

namespace Contruction.Web.Services
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IVendorPaymentsService" in both code and config file together.
    [ServiceContract]
    public interface IVendorPaymentsService
    {
        [OperationContract]
        IEnumerable<MPendingBillDetail> GetPendingBillDetails(int? VendorID, int? SiteID, int? pageIndex, int? PageSize);

        [OperationContract]
        MVendorPayment CreateNewVendorPayment(MVendorPayment _NewPayment);

        [OperationContract]
        PaginatedData<MVendorPaymentHeader> GetAllVendorPayments(string username, int? VendorID, int? SiteID, int? pageIndex, int? PageSize);
    }
}
