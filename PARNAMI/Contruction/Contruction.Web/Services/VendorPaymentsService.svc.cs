﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using SIMS.VendorPayments;
using SIMS.MM.MODEL.Headers;
using SIMS.MM.MODEL;

namespace Contruction.Web.Services
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "VendorPaymentsService" in code, svc and config file together.
    public class VendorPaymentsService : IVendorPaymentsService
    {
        VendorPaymentManager manager = new VendorPaymentManager();

        public IEnumerable<MPendingBillDetail> GetPendingBillDetails(int? VendorID, int? SiteID, int? pageIndex, int? PageSize)
        {
            var data = manager.GetPendingBillDetails(VendorID,SiteID);
            return data;
        }

        public MVendorPayment CreateNewVendorPayment(MVendorPayment _NewPayment)
        {
            var data = manager.CreateNewVendorPayment(_NewPayment);
            return data;
        }

        public PaginatedData<MVendorPaymentHeader> GetAllVendorPayments(string username, int? VendorID, int? SiteID, int? pageIndex, int? PageSize)
        {
            var data = (PaginatedData<MVendorPaymentHeader>)manager.GetAllPayments(username, pageIndex, PageSize, VendorID, SiteID);
            return data;
        }
    }
}
