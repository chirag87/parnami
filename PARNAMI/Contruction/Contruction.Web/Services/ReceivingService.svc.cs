﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using SIMS.MM.BLL;
using SIMS.MM.MODEL;

namespace Contruction.Web.Services
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "ReceivingService" in code, svc and config file together.
    [ServiceBehavior(MaxItemsInObjectGraph = 2147483647)]
    public class ReceivingService : IReceivingService
    {
        IReceivingManager pm = new ReceivingManager();
        IReceivingDataProvider dp = new ReceivingDataProvider();
        //IReceivingDataProvider dp = new TestReceivingDataProvider();

        public PaginatedData<MReceivingHeader> GetAllReceivings(string username, int? pageindex, int? pagesize, string key, int? vendorid, int? siteid)
        {
            var showAll = LoginService.CanShowAllSites(username);
            var data = (PaginatedData<MReceivingHeader>)dp.GetGRNHeadersNew(username, pageindex, pagesize, key, vendorid, siteid, showAll);
            return data;
        }

        public PaginatedData<MReceivingHeader> GetAllCashPurchases(string username, int? pageindex, int? pagesize, string key, int? vendorid, int? siteid)
        {
            var showAll = LoginService.CanShowAllSites(username);
            var data = (PaginatedData<MReceivingHeader>)dp.GetCashPurachaseHeadersNew(username, pageindex, pagesize, key, vendorid, siteid, showAll);
            return data;
        }

        public PaginatedData<MReceivingHeader> GetAllDirectPurchases(string username, int? pageindex, int? pagesize, string key, int? vendorid, int? siteid)
        {
            var showAll = LoginService.CanShowAllSites(username);
            var data = (PaginatedData<MReceivingHeader>)dp.GetDirectPurchaseHeaders(username, pageindex, pagesize, key, vendorid, siteid, showAll);
            return data;
        }

        public PaginatedData<MReceivingHeader> GetAllDirectGrns(string username, int? pageindex, int? pagesize, string key, int? vendorid, int? siteid, string type)
        {
            var showAll = LoginService.CanShowAllSites(username);
            var data = (PaginatedData<MReceivingHeader>)dp.GetAllDirectGRNsNew(username, pageindex, pagesize, key, vendorid, siteid, type, showAll);
            return data;
        }

        public PaginatedData<MReceivingHeader> GetAllConsumptions(string username, int? pageindex, int? pagesize, string key, int? siteid)
        {
            var showAll = LoginService.CanShowAllSites(username);
            var data = (PaginatedData<MReceivingHeader>)dp.GetConsumptionHeadersNew(username, pageindex, pagesize, key, siteid, showAll);
            return data;
        }

        public MReceiving CreateNewReceiving(MReceiving NewReceiving, string username)
        {
            return pm.CreateNewReceiving(NewReceiving, username);
        }

        public MReceiving CreateNewCashPurchase(MReceiving NewCashPurchase, string username)
        {
            return pm.CreateNewCashPurchase(NewCashPurchase, username);
        }

        public MReceiving CreateNewDirectPurchase(MReceiving NewDirectPurchase, string username)
        {
            var data = pm.CreateNewDirectPurchase(NewDirectPurchase, username);
            return data;
        }

        public MReceiving CreateNewConsumption(MReceiving NewConsumption, string username)
        {
            return pm.CreateNewConsumption(NewConsumption, username);
        }

        public MReceiving CreateNewRTV(MReceiving _newRTV, string username)
        {
            var data = pm.CreateNewRTV(_newRTV, username);
            return data;
        }

        public MReceiving CreateNewSA(MReceiving _newSA, string username)
        {
            var data = pm.CreateNewSA(_newSA, username);
            return data;
        }

        public MReceiving GetGRNByID(int receivingID)
        {
            var data = dp.GetGRNbyId(receivingID);
            return data;
        }

        public MReceiving GetCashPurchaseByID(int receivingID)
        {
            return dp.GetCPbyId(receivingID);
        }

        public MReceiving GetDirectPurchaseByID(int receivingID)
        {
            var data = dp.GetDPbyId(receivingID);
            return data;
        }

        public MReceiving GetConsumptionByID(int receivingID)
        {
            return dp.GetConsbyId(receivingID);
        }

        public IEnumerable<int> GetAllowedItemIDs(int vendorid, int siteid)
        {

            PurchasingManager pm = new PurchasingManager();
            //return new int[] { 1001, 1002, 1003};
            var data = pm.GetAllowedItemId(vendorid, siteid);
            return data;
        }

        public IEnumerable<PendingPOInfo> GetPendingPOInfos(int vendorid, int siteid, int itemid)
        {
            PurchasingManager pm = new PurchasingManager();
            var data = pm.GetPendingInfo(vendorid, siteid, itemid);
            if (data.Count() == 0)
            {
                PendingPOInfo info = new PendingPOInfo()
                {
                    ItemID = itemid,
                    IsBlank = true
                };
                List<PendingPOInfo> list = new List<PendingPOInfo>();
                list.Add(info);
                return list;
            }
            return data;
        }

        public MReceiving ConvertfromPO(int poid)
        {
            var data = pm.ConvertfromPO(poid);
            return data;
        }
    }
}

    //public class TestReceivingDataProvider : IReceivingDataProvider
    //{
    //    public IQueryable<MReceivingHeader> GetCashPurachaseHeaders(int? pageindex, int? pagesize, string key, int? vendorid, int? siteid)
    //    {
    //        List<MReceivingHeader> CashList = new List<MReceivingHeader>();
    //        CashList.Add(new MReceivingHeader()
    //        {
    //            ID = 1001,
    //            ReferenceNo = "CP-1001",
    //            ForDate = (new DateTime(2012, 06, 12)),
    //            ChallanNo = "A-12298",
    //            Site = "Site-1",
    //            Vendor = "Vendor-1",
    //            Remarks = "Test Data",
    //            UpdateOn = (new DateTime(2012, 07, 26)),
    //        });
    //        CashList.Add(new MReceivingHeader()
    //        {
    //            ID = 1002,
    //            ReferenceNo = "CP-1002",
    //            ForDate = (new DateTime(2012, 01, 11)),
    //            ChallanNo = "A-125528",
    //            Site = "Site-2",
    //            Vendor = "Vendor-2",
    //            Remarks = "Test Data",
    //            UpdateOn = (new DateTime(2012, 12, 25)),
    //        });

    //        return CashList.AsQueryable();
    //    }

    //    public IQueryable<MReceivingHeader> GetDirectPurchaseHeaders(int? pageindex, int? pagesize, string key, int? vendorid, int? siteid)
    //    {
    //        List<MReceivingHeader> DirectPurList = new List<MReceivingHeader>();
    //        DirectPurList.Add(new MReceivingHeader()
    //        {
    //            ID = 1101,
    //            ReferenceNo = "DP-1001",
    //            ForDate = (new DateTime(2012, 06, 12)),
    //            ChallanNo = "B-12298",
    //            Site = "Site-01",
    //            Vendor = "Vendor-01",
    //            Remarks = "Test Data",
    //            UpdateOn = (new DateTime(2012, 07, 26)),
    //        });
    //        DirectPurList.Add(new MReceivingHeader()
    //        {
    //            ID = 1102,
    //            ReferenceNo = "DP-1002",
    //            ForDate = (new DateTime(2012, 01, 11)),
    //            ChallanNo = "B-125528",
    //            Site = "Site-02",
    //            Vendor = "Vendor-02",
    //            Remarks = "Test Data",
    //            UpdateOn = (new DateTime(2012, 12, 25)),
    //        });

    //        return DirectPurList.AsQueryable();
    //    }

    //    public IQueryable<MReceivingHeader> GetGRNHeaders(int? pageindex, int? pagesize, string key, int? vendorid, int? siteid)
    //    {
    //        List<MReceivingHeader> GRNList = new List<MReceivingHeader>();
    //        GRNList.Add(new MReceivingHeader()
    //        {
    //            ID = 1201,
    //            ReferenceNo = "GRN-1001",
    //            ForDate = (new DateTime(2012, 06, 12)),
    //            ChallanNo = "C-12298",
    //            Site = "Site-101",
    //            Vendor = "Vendor-101",
    //            Remarks = "Test Data",
    //            UpdateOn = (new DateTime(2012, 07, 26)),
    //        });
    //        GRNList.Add(new MReceivingHeader()
    //        {
    //            ID = 1202,
    //            ReferenceNo = "GRN-1002",
    //            ForDate = (new DateTime(2012, 01, 11)),
    //            ChallanNo = "C-125528",
    //            Site = "Site-102",
    //            Vendor = "Vendor-102",
    //            Remarks = "Test Data",
    //            UpdateOn = (new DateTime(2012, 12, 25)),
    //        });

    //        return GRNList.AsQueryable();
    //    }

    //    public IQueryable<MReceivingHeader> GetConsumptionHeaders(int? pageindex, int? pagesize, string key, int? siteid)
    //    {
    //        throw new NotImplementedException();
    //    }

    //    public MReceiving GetCPbyId(int id)
    //    {
    //        throw new NotImplementedException();
    //    }

    //    public MReceiving GetConsbyId(int id)
    //    {
    //        throw new NotImplementedException();
    //    }

    //    public MReceiving GetDPbyId(int id)
    //    {
    //        throw new NotImplementedException();
    //    }

    //    public MReceiving GetGRNbyId(int id)
    //    {
    //        throw new NotImplementedException();
    //    }


    //    IPaginated<MReceivingHeader> IReceivingDataProvider.GetCashPurachaseHeaders(int? pageindex, int? pagesize, string key, int? vendorid, int? siteid)
    //    {
    //        throw new NotImplementedException();
    //    }

    //    IPaginated<MReceivingHeader> IReceivingDataProvider.GetConsumptionHeaders(int? pageindex, int? pagesize, string key, int? siteid)
    //    {
    //        throw new NotImplementedException();
    //    }

    //    IPaginated<MReceivingHeader> IReceivingDataProvider.GetDirectPurchaseHeaders(int? pageindex, int? pagesize, string key, int? vendorid, int? siteid)
    //    {
    //        throw new NotImplementedException();
    //    }

    //    IPaginated<MReceivingHeader> IReceivingDataProvider.GetGRNHeaders(int? pageindex, int? pagesize, string key, int? vendorid, int? siteid)
    //    {
    //        throw new NotImplementedException();
    //    }
    //}
