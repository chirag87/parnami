﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using SIMS.MM.BLL;
using SIMS.MM.MODEL;

namespace Contruction.Web.Services
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IMTRService" in both code and config file together.
    [ServiceContract]
    public interface IMTRService
    {
        [OperationContract]
        PaginatedData<MMTRHeader> GetAllMTRs(int? pageindex, int? pagesize, string key, int? siteid);

        [OperationContract]
        void CreateNewMTR(MMTR NewMTR);

        [OperationContract]
        MMTR GetMTRbyID(int MTRID);

        [OperationContract]
        void ApproveMTR(int MTRID);

        [OperationContract]
        void ReleaseMTR(int MTRID);

        [OperationContract]
        void ReceiveMTR(int MTRID);
    }
}
