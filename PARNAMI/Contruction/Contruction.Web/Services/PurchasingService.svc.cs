﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using SIMS.MM.BLL;
using SIMS.MM.MODEL;
using Contruction.Web.Data;
using System.Collections.ObjectModel;

namespace Contruction.Web.Services
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "PurchasingService" in code, svc and config file together.
    [ServiceBehavior(MaxItemsInObjectGraph = 2147483647)]
    public class PurchasingService : IPurchasingService
    {
        //IPurchasingDataProvider dp = new TestPurchasingDataProvider();
        IPurchasingManger pm = new PurchasingManager();
        IPurchasingDataProvider dp = new PurchasingDataProvider();

        public PaginatedData<MPRHeader> GetAllPRs(string username, int? pageindex,int? pagesize, string key, int? vendorid, int? siteid)
        {
            var showAll = LoginService.CanShowAllSites(username);
            var data = (PaginatedData<MPRHeader>)dp.GetPRHeadersNew(username, pageindex, pagesize, key, vendorid, siteid, showAll);
            return data;
        }

        public PaginatedData<MPOHeader> GetAllPOs(string username, int? pageindex, int? pagesize, string key, int? vendorid, int? siteid)
        {
            var showAll = LoginService.CanShowAllSites(username);
            var data = (PaginatedData<MPOHeader>)dp.GetPOHeadersNew(username, pageindex, pagesize, key, vendorid, siteid, showAll);
            return data;
        }

        public MPR GetPRbyID(int PRID)
        {
            return dp.GetPRbyId(PRID);
        }

        public MPO GetPObyID(int POID)
        {
            var data = dp.GetPObyId(POID);
            return data;
        }

        public MPR CreateNewPR(MPR NewPR)
        {
            return pm.CreatePR(NewPR);
        }

        public MPR SplitPR(MPR NewPR, int OldPRID, ObservableCollection<int> OldPRLines)
        {
            var data = pm.SplitPR(NewPR, OldPRID, OldPRLines);
            return data;
        }

        public void DeletePR(int PRID)
        {
            pm.DeletePR(PRID);
        }

        public MPR UpdatePR(MPR UpdatedPR)
        {
            MPR updatedpr = pm.UpdatePR(UpdatedPR);
            return updatedpr;
        }

        public void EmailPR(int PRID)
        {
            pm.EmailPR(PRID);
        }

        public string ConfirmVendorEmail(int POID)
        {
            return pm.ConfirmEmail(POID);
        }

        public void EmailtoVendor(int POID)
        {
            try
            {
                pm.EmailtoVendor(POID);
            }
            catch
            {
                throw new FaultException("Not Successful. Please check if Email Address is configured for the Vendor!");
            }
        }

        public void AcknowledgePO(int POID, string Remarks)
        {
            pm.AcknowledgePO(POID, Remarks);;
        }

        public void ApprovePR(MPR ApprovedPR, string User)
        {
            pm.ApprovePR(ApprovedPR.PRNumber, User);
        }

        public int ConvertToPO(MPR PR)
        {
            return pm.ConvertToPO(PR.PRNumber);
        }

        public MPR SaveandApprove(MPR mpr, string User)
        {
            pm.UpdatePR(mpr);
            pm.ApprovePR(mpr.PRNumber, User);
            return dp.GetPRbyId(mpr.PRNumber);
        }
    }

    //public class TestPurchasingDataProvider : IPurchasingDataProvider
    //{
    //    public IQueryable<MPOHeader> GetPOHeaders(int? pageindex, int? pagesize, string key, int? vendorid, int? siteid)
    //    {
    //        List<MPOHeader> POList = new List<MPOHeader>();
    //        POList.Add(new MPOHeader()
    //        {
    //            POID = 1,
    //            Site = "Site1",
    //            Date = (new DateTime(2012, 04, 28)),
    //            Price = 12050.00,
    //            PRNo = 1003,
    //            Vendor = "Vendor1",
    //            ApprovalStatus = "Approved"
    //        });
    //        POList.Add(new MPOHeader()
    //        {
    //            POID = 2,
    //            Site = "Site2",
    //            Date = (new DateTime(2012, 12, 24)),
    //            Price = 15000.00,
    //            PRNo = 1011,
    //            Vendor = "Vendor2",
    //            ApprovalStatus = "Not Approved"
    //        });
    //        POList.Add(new MPOHeader()
    //        {
    //            POID = 3,
    //            Site = "Site3",
    //            Date = (new DateTime(2012, 06, 12)),
    //            Price = 14900.00,
    //            PRNo = 1010,
    //            Vendor = "Vendor1",
    //            ApprovalStatus = "Approved"
    //        });

    //        return POList.AsQueryable();
    //    }

    //    public IQueryable<MPRHeader> GetPRHeaders(int? pageindex, int? pagesize, string key, int? vendorid, int? siteid)
    //    {
    //        List<MPRHeader> PRList = new List<MPRHeader>();
    //        PRList.Add(new MPRHeader()
    //        {
    //            PRNo = 1003,
    //            Site = "Site1",
    //            Date = (new DateTime(2012, 04, 28)),
    //            Price = 12050.00,
    //            Vendor = "Vendor1",
    //            ApprovalStatus = "Approved"
    //        });
    //        PRList.Add(new MPRHeader()
    //        {
    //            PRNo = 1011,
    //            Site = "Site2",
    //            Date = (new DateTime(2012, 12, 24)),
    //            Price = 15000.00,
    //            Vendor = "Vendor2",
    //            ApprovalStatus = "Not Approved"
    //        });
    //        PRList.Add(new MPRHeader()
    //        {
    //            PRNo = 1010,
    //            Site = "Site3",
    //            Date = (new DateTime(2012, 06, 12)),
    //            Price = 14900.00,
    //            Vendor = "Vendor1",
    //            ApprovalStatus = "Approved"
    //        });

    //        return PRList.AsQueryable();
    //    }

    //    MPR IPurchasingDataProvider.GetPRbyId(int prid)
    //    {
    //        throw new NotImplementedException();
    //    }


    //    MPO IPurchasingDataProvider.GetPObyId(int poid)
    //    {
    //        throw new NotImplementedException();
    //    }

    //    IPaginated<MPRHeader> IPurchasingDataProvider.GetPRHeaders(int? pageindex, int? pagesize, string key, int? vendorid, int? siteid)
    //    {
    //        throw new NotImplementedException();
    //    }

    //    IPaginated<MPOHeader> IPurchasingDataProvider.GetPOHeaders(int? pageindex, int? pagesize, string key, int? vendorid, int? siteid)
    //    {
    //        throw new NotImplementedException();
    //    }
    //}
}
