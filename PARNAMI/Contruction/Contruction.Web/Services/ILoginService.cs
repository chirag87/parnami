﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using SIMS.MM.MODEL;

namespace Contruction.Web.Services
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "ILoginService" in both code and config file together.
    [ServiceContract]
    public interface ILoginService
    {
        [OperationContract]
        UserRights Login(string username, string password);

        [OperationContract]
        IEnumerable<UserInfo> GetAllUsersInfo();

        [OperationContract]
        bool ChangePasswordForced(string username, string newpassword);

        [OperationContract]
        bool ChangePassword(string username, string oldPassword, string newpassword);

        [OperationContract]
        IEnumerable<UserInfo> AddRolesToUser(string username, string[] roles);

        [OperationContract]
        IEnumerable<UserInfo> RemoveRolesToUser(string username, string[] roles);

        [OperationContract]
        IEnumerable<UserInfo> AddSitesToUser(string username, int[] sites);

        [OperationContract]
        IEnumerable<UserInfo> RemoveSitesToUser(string username, int[] sites);

        [OperationContract]
        IEnumerable<string> GetAllRoles();

        [OperationContract]
        IEnumerable<UserInfo> BlockUser(string username);

        [OperationContract]
        IEnumerable<UserInfo> AllowUser(string username);

        [OperationContract]
        void CreateNewUser(string username, string password, string email);
    }
}
