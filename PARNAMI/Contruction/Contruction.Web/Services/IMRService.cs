﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using SIMS.MM.MODEL;

namespace Contruction.Web.Services
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IMRService" in both code and config file together.
    [ServiceContract]
    public interface IMRService
    {
        [OperationContract]
        PaginatedData<MMRHeader> GetAllMRs(string username, int? pageindex, int? pagesize, string key, int? siteid);

        [OperationContract]
        MMR CreateNewMR(MMR NewMR);

        [OperationContract]
        MMR GetMRByID(int MRID);

        [OperationContract]
        int IssueMR(MReceiving mrec);

        [OperationContract]
        int ReturnMR(MReceiving mrec);

        [OperationContract]
        MReceiving GetMReceivingforMRIssue(int mrid);

        [OperationContract]
        MReceiving GetMReceivingforMRReturn(int mrid);

        [OperationContract]
        void CloseMR(int MRID);
    }
}
