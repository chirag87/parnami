﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using SIMS.MM.BLL.Reports;
using SIMS.MM.DAL;
using SIMS.MM.MODEL.Reports;
using SIMS.MM.MODEL;

namespace Contruction.Web.Services
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "MasterReportingService" in code, svc and config file together.
    [ServiceBehavior(MaxItemsInObjectGraph = 2147483647)]
    public class MasterReportingService : IMasterReportingService
    {
        ReportProvider pr = new ReportProvider();

        public List<MPOReport> GetPOReport(DateTime? startdate, DateTime? enddate, int? siteid, int? vendorid, int? itemid, int? itemcatid, int? sizeid, int? brandid)
        {
            var data = pr.GetPODetailsByLine(startdate, enddate, siteid, vendorid, itemid, itemcatid, sizeid, brandid).ToList();
            return data;
        }

        public List<MGRNReport> GetGRNReport(DateTime? sdate, DateTime? edate, int? siteid, int? vendorid, int? itemid, int? sizeid, int? brandid)
        {
            var data = pr.GetGRNDetails(sdate, edate, siteid, vendorid, itemid, sizeid, brandid).ToList();
            return data;
        }

        public List<MCPReport> GetCPReport(DateTime? sdate, DateTime? edate, int? siteid, int? vendorid, int? itemid, int? sizeid, int? brandid)
        {
            var data = pr.GetCPdetails(sdate, edate, siteid, vendorid, itemid, sizeid, brandid).ToList();
            return data;
        }

        public List<MDPReport> GetDPReport(DateTime? sdate, DateTime? edate, int? siteid, int? vendorid, int? itemid, int? sizeid, int? brandid)
        {
            var data = pr.GetDPDetails(sdate, edate, siteid, vendorid, itemid, sizeid, brandid).ToList();
            return data;
        }

        public List<MDirectGRNReport> GetDirectGRNReport(DateTime? sdate, DateTime? edate, int? siteid, int? vendorid, int? itemid, int? sizeid, int? brandid)
        {
            var data = pr.GetDirectGRNReport(sdate, edate, siteid, vendorid, itemid, sizeid, brandid).ToList();
            return data;
        }

        public List<MStockReport> GetStockReport(DateTime? sdate, DateTime? edate, int? siteid, int? itemid, int? sizeid, int? brandid)
        {
            var data = pr.GetDetailedStocReport(sdate, edate, siteid, itemid, sizeid, brandid).ToList();
            return data;
        }

        public List<MConsumptionReport> GetConsumptionReport(DateTime? sdate, DateTime? edate, int? siteid, int? vendorid, int? itemid, int? sizeid, int? brandid)
        {
            var data = pr.GetConsumptionDetails(sdate, edate, siteid, vendorid, itemid, sizeid, brandid).ToList();
            return data;
        }

        public List<MMOReport> GetMMOReport(DateTime? sdate, DateTime? edate, int? siteid, int? vendorid, int? itemid, int? sizeid, int? brandid)
        {
            var data = pr.GetMOdetails(sdate, edate, siteid, itemid, sizeid, brandid).ToList();
            return data;
        }

        public List<MBilledReceivings> GetBilledReceivings(DateTime? sdate, DateTime? edate, int? siteid, int? vendorid, int? itemid, int? sizeid, int? brandid)
        {
            var data = pr.GetBilledReceivings(sdate, edate, siteid, vendorid, itemid, sizeid, brandid).ToList();
            return data;
        }

        public List<MDirectGRNReport> GetUnBilledReceivings(DateTime? sdate, DateTime? edate, int? siteid, int? vendorid, int? itemid, int? sizeid, int? brandid)
        {
            var data = pr.GetUnBilledReceivings(sdate, edate, siteid, vendorid, itemid, sizeid, brandid).ToList();
            return data;
        }
    }
}
