﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using Contruction.Web.Data;
using SIMS.MM.BLL;
using SIMS.MM.MODEL;
using System.Collections.ObjectModel;

namespace Contruction.Web.Services
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IPurchasingService" in both code and config file together.
    [ServiceContract]
    //[ServiceBehavior(
    public interface IPurchasingService
    {
        [OperationContract]
        PaginatedData<MPRHeader> GetAllPRs(string username, int? pageindex, int? pagesize, string key, int? vendorid, int? siteid);

        [OperationContract]
        PaginatedData<MPOHeader> GetAllPOs(string username, int? pageindex, int? pagesize, string key, int? vendorid, int? siteid);

        [OperationContract]
        MPR GetPRbyID(int PRID);

        [OperationContract]
        MPO GetPObyID(int POID);

        [OperationContract]
        MPR CreateNewPR(MPR NewPR);

        [OperationContract]
        MPR SplitPR(MPR NewPR, int OldPRID, ObservableCollection<int> OldPRLines);

        [OperationContract]
        void DeletePR(int PRID);

        [OperationContract]
        MPR UpdatePR(MPR UpdatedPR);

        [OperationContract]
        void EmailPR(int PRID);

        [OperationContract]
        string ConfirmVendorEmail(int POID);

        [OperationContract]
        void EmailtoVendor(int PRID);

        [OperationContract]
        void AcknowledgePO(int POID, string Remarks);

        [OperationContract]
        void ApprovePR(MPR ApprovedPR, string User);

        [OperationContract]
        int ConvertToPO(MPR PR);

        [OperationContract]
        MPR SaveandApprove(MPR mpr, string User);
    }
}
