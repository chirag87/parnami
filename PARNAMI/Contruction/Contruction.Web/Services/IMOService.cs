﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using SIMS.MM.MODEL;
using System.Collections.ObjectModel;

namespace Contruction.Web.Services
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IMOService" in both code and config file together.
    [ServiceContract]
    public interface IMOService
    {
        [OperationContract]
        PaginatedData<MMOHeader> GetAllMOs(string username, int? pageindex, int? pagesize, string key, int? siteid);

        [OperationContract]
        MMO CreateNewMO(MMO newMO);

        [OperationContract]
        MMO GetMObyID(int MOID);

        [OperationContract]
        string ReleaseMO(MReceiving mrec);

        [OperationContract]
        int ReceiveMO(MReceiving mrec);

        [OperationContract]
        MReceiving GetMReceivingforMORelease(int MOID);

        [OperationContract]
        MReceiving GetMReceivingforMOReceive(int MOID);

        [OperationContract]
        void CloseMO(int MOID);

        [OperationContract]
        MMO SplitToMO(MMO NewMO, int OldPRID, ObservableCollection<int> OldPRLines);
    }
}
