﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Contruction.Web
{
    using SIMS.ReportLoader;
    public class ReportLoaderProviders:IReportLoaderProvider
    {
        public ReportLoaderProviders()
        {

        }

        public IReportLoader GetReportLoader(string type)
        {
            string path = "~/Prints/"+type+".rdlc";
            IReportLoader loader = new BlankReportLoader();
            switch (type)
            {
                case "VendorBill":
                    loader = new SIMS.MM.DAL.PrintReports.VendorBillPrintLoader();
                    // Change Path if Required
                    break;

                case "PO":
                    loader = new SIMS.MM.DAL.PrintReports.POPrintLoader();
                    // Change Path if Required
                    break;

                case "GRN":
                    loader = new SIMS.MM.DAL.PrintReports.GRNPrintLoader();
                    // Change Path if Required
                    break;

                case "DP":
                    loader = new SIMS.MM.DAL.PrintReports.DPPrintLoader();
                    // Change Path if Required
                    break;

                default:
                    break;
            }
            if (loader != null)
            {
                loader.ReportPath = path;
                loader.DictionaryConverter = new QueryToDicConverter(type);
            }
            return loader;
        }
    }

    public class BlankReportLoader:ReportLoader
    {
        public override RDLCReportData GetReportData(ReportParameterDictionary Dic)
        {
            return new RDLCReportData();
        }
    }

    public class QueryToDicConverter : IDictionaryConverter
    {
        string _type;
        
        public QueryToDicConverter(string type) { _type = type; }

        public ReportParameterDictionary GetDic(object obj)
        {
            var Query = (System.Collections.Specialized.NameValueCollection)obj;
            ReportParameterDictionary dic = new ReportParameterDictionary();

            if (Query["id"] != null)
                dic.Add("id", Int32.Parse(Query["id"]));

            if (_type.Equals("VendorBill", StringComparison.OrdinalIgnoreCase))
            {
                return dic;
            }

            if (_type.Equals("PO", StringComparison.OrdinalIgnoreCase))
            {
                return dic;
            }

            if (_type.Equals("GRN", StringComparison.OrdinalIgnoreCase))
            {
                return dic;
            }

            if (_type.Equals("DP", StringComparison.OrdinalIgnoreCase))
            {
                return dic;
            }

            return dic;
        }
    }
}