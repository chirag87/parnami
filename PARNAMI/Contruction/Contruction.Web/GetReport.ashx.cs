﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.Reporting.WebForms;
using SIMS.MM.DAL;
using SIMS;

namespace Contruction.Web
{
    using SIMS.ReportLoader;
    /// <summary>
    /// Summary description for GetReport
    /// </summary>
    public class GetReport : IHttpHandler
    {
        public void ProcessRequest(HttpContext context)
        {
            context.Response.Clear();
            IReportLoaderProvider provider = new ReportLoaderProviders();
            IReportLoader loader = provider.GetReportLoader(context.Request.QueryString["type"]);
            context.PushRDLC_PDFReport(loader.ReportPath, loader.GetReportData(context.Request.QueryString).DataSources);
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}