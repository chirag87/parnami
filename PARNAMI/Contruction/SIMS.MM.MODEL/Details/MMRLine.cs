﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.ComponentModel;

namespace SIMS.MM.MODEL
{
    [DataContract]
    public class MMRLine : EntityModel
    {
        public MMRLine()
        {
            IssuedOn = DateTime.UtcNow.AddHours(5.5);
            ReturnedOn = DateTime.UtcNow.AddHours(5.5);
            ConsumableType = "Consumable";
        }

        [DataMember]
        public int MRID { get; set; }

        int _LineID;
        [DataMember]
        public int LineID 
        {
            get { return _LineID; }
            set { _LineID = value;
            Notify("LineID");
            }
        }

        double _Quantity;
        [DataMember]
        public double Quantity 
        {
            get { return _Quantity; }
            set 
            {
                _Quantity = value;
            }
        }

        [DataMember]
        public double? IssuedQty { get; set; }

        [DataMember]
        public double? ReturnedQty { get; set; }

        [DataMember]
        public double? PendingIssuedQty
        {
            get { return Quantity - IssuedQty; }
            set { }
        }

        [DataMember]
        public double? PendingReturnedQty
        {
            get { return IssuedQty - ReturnedQty; }
            set { }
        }

        [DataMember]
        public int ItemID { get; set; }

        [DataMember]
        public string Item { get; set; }

        [DataMember]
        public int? SizeID { get; set; }

        [DataMember]
        public string Size { get; set; }

        [DataMember]
        public int? BrandID { get; set; }

        [DataMember]
        public string Brand { get; set; }

        DateTime? _IssuedOn;
        [DataMember]
        public DateTime? IssuedOn
        {
            get
            {
                if (_IssuedOn < DateTime.UtcNow.AddHours(5.5))
                    return _IssuedOn;
                else
                    return DateTime.UtcNow.AddHours(5.5);
            }
            set
            {
                _IssuedOn = value;
                Notify("IssuedOn");
            }
        }

        DateTime? _ReturnedOn;
        [DataMember]
        public DateTime? ReturnedOn
        {
            get
            {
                if (_ReturnedOn < DateTime.UtcNow.AddHours(5.5))
                    return _ReturnedOn;
                else
                    return DateTime.UtcNow.AddHours(5.5);
            }
            set
            {
                _ReturnedOn = value;
                Notify("ReturnedOn");
            }
        }

        [DataMember]
        public string LineRemarks { get; set; }

        [DataMember]
        public string MeasurementUnit { get; set; }

        [DataMember]
        public string ConsumableType { get; set; }
    }
}