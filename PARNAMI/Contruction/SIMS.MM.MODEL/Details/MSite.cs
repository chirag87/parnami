﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;

namespace SIMS.MM.MODEL
{
    [DataContract]
    public class MSite : EntityModel
    {
        [DataMember]
        [Display(AutoGenerateField = false)]
        public int ID { get; set; }

        [DataMember]
        [Display(AutoGenerateField = false)]
        public string Display
        {
            get
            {
                return Name + " (ID: " + ID + "," + " Code: " + SiteCode + "," + " Address: " + Address + ", " + City + ")";
            }
            set { }
        }

        [DataMember]
        [Display(AutoGenerateField = false)]
        public int? ParentID { get; set; }

        [DataMember]
        [Required]
        public string Name { get; set; }

        [DataMember]
        [Display(Name = "Type")]
        [Required]
        public string TypeID
        {
            get { return "Site"; }
            set { }
        }

        [DataMember]
        [Required]
        public string SiteCode { get; set; }

        [DataMember]
        public int CompanyID { get; set; }

        [DataMember]
        public string Company { get; set; }

        [DataMember]
        public int ClientID { get; set; }

        [DataMember]
        public string Client { get; set; }

        [DataMember]
        public string Address { get; set; }

        [DataMember]
        public string City { get; set; }

        [DataMember]
        public string State { get; set; }

        [DataMember]
        public string Country { get; set; }

        [DataMember]
        public string PinCode { get; set; }

        [DataMember]
        public string Phone { get; set; }

        [DataMember]
        public string ContactPerson { get; set; }

        [DataMember]
        [Display(Name = "Contact Person Mobile1")]
        public string KeyContactMobile1 { get; set; }

        [DataMember]
        [Display(Name = "Contact Person Mobile2")]
        public string KeyContactMobile2 { get; set; }

        [DataMember]
        [Display(Name = "Contact Person Email")]
        public string KeyContactEmail { get; set; }
    }
}
