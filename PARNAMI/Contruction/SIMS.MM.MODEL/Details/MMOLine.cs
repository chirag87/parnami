﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.ComponentModel;

namespace SIMS.MM.MODEL
{
    [DataContract]
    public class MMOLine : EntityModel
    {
        public MMOLine()
        {
            ReceivedQty = PendingReceiveQty;
            ReleasedQty = PendingReleaseQty;
        }

        [DataMember]
        public int ID { get; set; }

        int _LineID;
        [DataMember]
        public int LineID
        {
            get { return _LineID; }
            set
            {
                _LineID = value;
                Notify("LineID");
            }
        }

        [DataMember]
        public double Quantity { get; set; }

        double? _ReleasedQty;
        [DataMember]
        public double? ReleasedQty
        {
            get { return _ReleasedQty; }
            set
            {
                _ReleasedQty = value;
                Notify("ReleasedQty");
            }
        }

        double? _ReceivedQty;
        [DataMember]
        public double? ReceivedQty
        {
            get { return _ReceivedQty; }
            set
            {
                _ReceivedQty = value;
                Notify("ReceivedQty");
            }
        }

        [DataMember]
        public double? PendingReleaseQty 
        {
            get { return Quantity - ReleasedQty; }
            set { }
        }

        [DataMember]
        public double? PendingReceiveQty 
        {
            get { return ReleasedQty - ReceivedQty; }
            set { }
        }

        [DataMember]
        public double? TransferredQty { get; set; }

        [DataMember]
        public double? InTransitQty { get; set; }

        [DataMember]
        public int ItemID { get; set; }

        [DataMember]
        public string Item { get; set; }
        
        [DataMember]
        public int? SizeID { get; set; }

        [DataMember]
        public string Size { get; set; }

        [DataMember]
        public int? BrandID { get; set; }

        [DataMember]
        public string Brand { get; set; }

        [DataMember]
        public string LineRemarks { get; set; }

        [DataMember]
        public string LineChallanNo { get; set; }

        public bool ValidateQty()
        {
            if (ReceivedQty > ReleasedQty) return false;
            return true;
        }
    }
}
