﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.Collections.ObjectModel;

namespace SIMS.MM.MODEL
{
    [DataContract]
    public class MMO : EntityModel
    {
        public MMO()
        {
            RaisedOn = DateTime.UtcNow.AddHours(5.5);
            ReceivedOn = DateTime.UtcNow.AddHours(5.5);
            ReleasedOn = DateTime.UtcNow.AddHours(5.5);
            ApprovedOn = DateTime.UtcNow.AddHours(5.5);
        }

        [DataMember]
        public int ID { get; set; }

        [DataMember]
        public string ReferenceNo { get; set; }

        [DataMember]
        public bool IsApproved { get; set; }

        [DataMember]
        public bool IsReleased { get; set; }

        //[DataMember]
        //public DateTime? ReleasedOn { get; set; }
        DateTime? _ReleasedOn;
        [DataMember]
        public DateTime? ReleasedOn
        {
            get
            {
                if (_ReleasedOn < DateTime.UtcNow.AddHours(5.5))
                    return _ReleasedOn;
                else
                    return DateTime.UtcNow.AddHours(5.5);
            }
            set
            {
                _ReleasedOn = value;
                Notify("ReleasedOn");
            }
        }

        [DataMember]
        public string ReleasedBy { get; set; }

        [DataMember]
        public bool IsReceived { get; set; }

        //[DataMember]
        //public DateTime? ReceivedOn { get; set; }
        DateTime? _ReceivedOn;
        [DataMember]
        public DateTime? ReceivedOn
        {
            get
            {
                if (_ReceivedOn < DateTime.UtcNow.AddHours(5.5))
                    return _ReceivedOn;
                else
                    return DateTime.UtcNow.AddHours(5.5);
            }
            set
            {
                _ReceivedOn = value;
                Notify("ReceivedOn");
            }
        }

        [DataMember]
        public string ReceivedBy { get; set; }

        [DataMember]
        public string Requester { get; set; }

        //[DataMember]
        //public DateTime RaisedOn { get; set; }
        DateTime _RaisedOn;
        [DataMember]
        public DateTime RaisedOn
        {
            get
            {
                if (_RaisedOn < DateTime.UtcNow.AddHours(5.5))
                    return _RaisedOn;
                else
                    return DateTime.UtcNow.AddHours(5.5);
            }
            set
            {
                _RaisedOn = value;
                Notify("RaisedOn");
            }
        }

        [DataMember]
        public string RaisedBy { get; set; }

        //[DataMember]
        //public DateTime ApprovedOn { get; set; }
        DateTime _ApprovedOn;
        [DataMember]
        public DateTime ApprovedOn
        {
            get
            {
                if (_ApprovedOn < DateTime.UtcNow.AddHours(5.5))
                    return _ApprovedOn;
                else
                    return DateTime.UtcNow.AddHours(5.5);
            }
            set
            {
                _ApprovedOn = value;
                Notify("ApprovedOn");
            }
        }

        [DataMember]
        public string ApprovedBy { get; set; }

        [DataMember]
        public int RequestingSiteID { get; set; }

        [DataMember]
        public string RequestingSite { get; set; }

        [DataMember]
        public int TransferringSiteID { get; set; }

        [DataMember]
        public string TransferringSite { get; set; }

        [DataMember]
        public int? TransporterID { get; set; }

        [DataMember]
        public string Transporter { get; set; }

        [DataMember]
        public DateTime? TransferDate { get; set; }

        [DataMember]
        public string Remarks { get; set; }

        [DataMember]
        public bool PendingRelease { get; set; }

        [DataMember]
        public bool PendingReceive { get; set; }

        [DataMember]
        public bool IsClosed { get; set; }

        [DataMember]
        public string Status { get; set; }

        [DataMember]
        public string ChallanNo { get; set; }

        [DataMember]
        public ObservableCollection<MMOLine> MOLines
        {
            get;
            set;
        }

        public MMOLine AddNewLine()
        {
            if (this.MOLines == null)
                this.MOLines = new ObservableCollection<MMOLine>();
            var line = new MMOLine();
            this.MOLines.Add(line);
            this.UpdateLineNumbers();
            return line;
        }

        public int UpdateLineNumbers()
        {
            if (this.MOLines == null) return 0;
            int i = 1;
            foreach (var item in this.MOLines)
            {
                item.LineID = i;
                i++;
            }
            return i - 1;
        }
    }
}
