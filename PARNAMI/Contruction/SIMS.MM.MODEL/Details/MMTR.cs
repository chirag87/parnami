﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.Collections.ObjectModel;

namespace SIMS.MM.MODEL
{
    [DataContract]
    public class MMTR : EntityModel
    {
        [DataMember]
        public int MTRNo { get; set; }

        [DataMember]
        public int RequestingSiteID { get; set; }

        [DataMember]
        public string RequestingSite { get; set; }

        [DataMember]
        public int TransferringSiteID { get; set; }

        [DataMember]
        public string TransferringSite { get; set; }

        [DataMember]
        public string Requester { get; set; }

        [DataMember]
        public DateTime? TransferDate { get; set; }

        [DataMember]
        public string MTRRemarks { get; set; }

        [DataMember]
        public bool IsReleased { get; set; }

        //[DataMember]
        //public DateTime? ReleasedOn { get; set; }
        DateTime _ReleasedOn;
        [DataMember]
        public DateTime ReleasedOn
        {
            get
            {
                if (_ReleasedOn < DateTime.UtcNow.AddHours(5.5))
                    return _ReleasedOn;
                else
                    return DateTime.UtcNow.AddHours(5.5);
            }
            set
            {
                _ReleasedOn = value;
                Notify("ReleasedOn");
            }
        }

        [DataMember]
        public string ReleasedBy { get; set; }

        [DataMember]
        public bool IsReceived { get; set; }

        //[DataMember]
        //public DateTime? ReceivedOn { get; set; }
        DateTime _ReceivedOn;
        [DataMember]
        public DateTime ReceivedOn
        {
            get
            {
                if (_ReceivedOn < DateTime.UtcNow.AddHours(5.5))
                    return _ReceivedOn;
                else
                    return DateTime.UtcNow.AddHours(5.5);
            }
            set
            {
                _ReceivedOn = value;
                Notify("ReceivedOn");
            }
        }

        [DataMember]
        public string ReceivedBy { get; set; }

        [DataMember]
        public ObservableCollection<MMTRLine> MTRLines
        {
            get;
            set;
        }

        public MMTRLine AddNewLine()
        {
            if (this.MTRLines == null)
                this.MTRLines = new ObservableCollection<MMTRLine>();
            var line = new MMTRLine();
            this.MTRLines.Add(line);
            this.UpdateLineNumbers();
            return line;
        }

        public int UpdateLineNumbers()
        {
            if (this.MTRLines == null) return 0;
            int i = 1;
            foreach (var item in this.MTRLines)
            {
                item.LineID = i;
                i++;
            }
            return i - 1;
        }
    }
}
