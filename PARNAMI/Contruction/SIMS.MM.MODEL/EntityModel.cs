﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;

namespace SIMS.MM.MODEL
{
    [DataContract]
    public class EntityModel : INotifyPropertyChanged
    {
        bool _HasChanges = false;
        [DataMember]
        [Display(AutoGenerateField = false)]
        public bool HasChanges
        {
            get { return _HasChanges; }
            set
            {
                _HasChanges = value;
                Notify("HasChanges");
            }
        }

        bool _IsBlocked = false;
        [DataMember]
        [Display(AutoGenerateField = false)]
        public bool IsBlocked
        {
            get { return _IsBlocked; }
            set
            {
                _IsBlocked = value;
                Notify("IsBlocked");
            }
        }

        #region INotifyPropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;
        public void Notify(string PropertyName)
        {
            if (PropertyChanged != null) 
                PropertyChanged(this, new PropertyChangedEventArgs(PropertyName));
        }

        #endregion
    }
}
