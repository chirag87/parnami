﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;

namespace SIMS.MM.MODEL.Headers
{
    [DataContract]
    public partial class MPendingBillDetail : EntityModel
    {
        [DataMember]
        [Display(Order = 0, Name = "Bill ID")]
        public int ID { get; set; }

        [DataMember]
        [Display(AutoGenerateField = false)]
        public DateTime BillDate { get; set; }

        [DataMember]
        [Display(Order = 1, Name = "Bill Date")]
        public string BillDateDisplay { get { return BillDate.ToString("MMM dd, yyyy"); } set { } }

        [DataMember]
        [Display(Order = 2, Name = "Bill No")]
        public string BillNo { get; set; }

        [DataMember]
        [Display(Order = 3)]
        public string Vendor { get; set; }

        [DataMember]
        [Display(Order = 4)]
        public string Site { get; set; }

        [DataMember]
        [Display(Order = 5)]
        public double Total { get; set; }

        [DataMember]
        [Display(Order = 6)]
        public double Pending { get; set; }

        [DataMember]
        [Display(Order = 7, Name = "Due Date")]
        [DisplayFormat(DataFormatString = "MMM dd, yyyy")]
        public DateTime DueDate { get; set; }

        [DataMember]
        [Display(Order = 8)]
        public string Remarks { get; set; }

        [DataMember]
        [Display(AutoGenerateField = false)]
        public int VendorID { get; set; }

        [DataMember]
        [Display(AutoGenerateField = false)]
        public int SiteID { get; set; }
    }

    [DataContract]
    public partial class MVendorPayment : INotifyPropertyChanged
    {
        [DataMember]
        public int ID { get; set; }

        [DataMember]
        public int VendorID { get; set; }

        [DataMember]
        public string Vendor { get; set; }

        [DataMember]
        public int SiteID { get; set; }

        [DataMember]
        public string Site { get; set; }

        [DataMember]
        public DateTime PaymentDate { get; set; }

        [DataMember]
        public string PayedBy { get; set; }

        [DataMember]
        public string Mode { get; set; }

        [DataMember]
        public string PayeeName { get; set; }

        [DataMember]
        public string GivenTo { get; set; }

        double _Total;
        [DataMember]
        public double Total
        {
            get { return _Total; }
            set
            {
                if (Details != null)
                    _Total = Details.Sum(x => x.Payment);
                Notify("Total");
            }
        }

        [DataMember]
        public double NetPayment
        {
            get { return Total - TDS; }
            set { }
        }

        [DataMember]
        public double TDS { get { return ((Total * TDSPercent) / 100); } set { } }

        double _TDSPercent;
        [DataMember]
        public double TDSPercent
        {
            get { return _TDSPercent; }
            set
            {
                _TDSPercent = value;
                //UpdateTDS();
                Notify("TDSPercent");
            }
        }

        [DataMember]
        public string Remarks { get; set; }

        [DataMember]
        public string RefNo { get; set; }

        [DataMember]
        public ObservableCollection<MVendorPaymentDetail> Details { get; set; }

        public MVendorPayment()
        {
            Details = new ObservableCollection<MVendorPaymentDetail>();
            PaymentDate = DateTime.UtcNow.AddHours(5.5);
        }

        public void UpdateTotal()
        {
            if (this.Details != null)
                Total = Details.Sum(x => x.Total);
        }

        public void UpdateNetAmount()
        {
            if (this.Details != null)
                NetPayment = Total - TDS;
        }

        public void UpdateTDS()
        {
            if (this.Details != null)
                TDS = (Total * TDSPercent) / 100;
        }

        public void DeleteLine(int lid)
        {
            try
            {
                var line = Details.Single(x => x.LineID == lid);
                Details.Remove(line);
            }
            catch { }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void Notify(string propertyName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }
    }

    [DataContract]
    public partial class MVendorPaymentDetail : INotifyPropertyChanged
    {
        [DataMember]
        public int HeaderID { get; set; }

        [DataMember]
        public int LineID { get; set; }

        [DataMember]
        public int BillID { get; set; }

        [DataMember]
        public string BillNo { get; set; }

        [DataMember]
        public DateTime BillDate { get; set; }

        [DataMember]
        public double Total { get; set; }

        [DataMember]
        public double Paid { get; set; }

        double _Payment;
        [DataMember]
        public double Payment
        {
            get { return _Payment; }
            set
            {
                //if (CheckforPayment())
                    _Payment = value;
                //else
                 //   _Payment = 0;
                Notify("Payment");
            }
        }

        [DataMember]
        public string Remarks { get; set; }

        public bool CheckforPayment()
        {
            if ((Payment > Total || Payment > (Total - Paid)))
            {
                return false;
            }
            else
                return true;
        }

        public void UpdatePaid()
        {
            this.Paid = Paid + Payment;
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void Notify(string propertyName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }
    }


    [DataContract]
    public partial class MVendorPaymentHeader : INotifyPropertyChanged
    {
        [DataMember]
        public int ID { get; set; }

        [DataMember]
        public int VendorID { get; set; }

        [DataMember]
        public string Vendor { get; set; }

        [DataMember]
        public int SiteID { get; set; }

        [DataMember]
        public string Site { get; set; }

        [DataMember]
        public DateTime PaymentDate { get; set; }

        [DataMember]
        public string PayedBy { get; set; }

        [DataMember]
        public string Mode { get; set; }

        [DataMember]
        public string PayeeName { get; set; }

        [DataMember]
        public string GivenTo { get; set; }

        [DataMember]
		public double Total { get; set;}

        [DataMember]
        public double NetPayment { get; set; }

        [DataMember]
        public double TDS { get; set; }

        [DataMember]
        public string Remarks { get; set; }

        [DataMember]
        public string RefNo { get; set; }

        public event PropertyChangedEventHandler PropertyChanged;
        public void Notify(string propertyName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}