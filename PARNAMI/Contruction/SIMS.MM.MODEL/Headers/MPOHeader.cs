﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace SIMS.MM.MODEL
{
    [DataContract]
    public class MPOHeader : MPRHeader
    {
        [DataMember]
        public int POID { get; set; }

        [DataMember]
        public string POReferenceNo { get; set; }

        bool _IsClosed = false;
        [DataMember]
        public bool IsClosed
        {
            get { return _IsClosed; }
            set
            {
                _IsClosed = value;
                Notify("IsClosed");
            }
        }

        [DataMember]
        public string Status
        {
            get 
            { 
                if(IsClosed == true)
                    return "Closed !";
                return "";
            }
            set { }
        }
    }
}