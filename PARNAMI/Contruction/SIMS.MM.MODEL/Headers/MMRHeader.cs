﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace SIMS.MM.MODEL
{
    [DataContract]
    public class MMRHeader : EntityModel
    {
        [DataMember]
        public int ID { get; set; }

        [DataMember]
        public string ReferenceNo { get; set; }

        [DataMember]
        public DateTime RaiseOn { get; set; }

        [DataMember]
        [Display(AutoGenerateField = false)]
        public string Site { get; set; }

        [DataMember]
        [Display(Name = "Site")]
        public string DisplaySite { get; set; }

        [DataMember]
        public string Status { get; set; }

        [DataMember]
        public bool CanIssue { get; set; }

        [DataMember]
        public bool CanReturn { get; set; }

        [DataMember]
        public bool IsClosed { get; set; }

        [DataMember]
        public string ClosedBy { get; set; }

        [DataMember]
        public string Remarks { get; set; }
    }
}
