﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;

namespace SIMS.MM.MODEL.Reports
{
    [DataContract]
    public class MMTRReport : EntityModel
    {
        [DataMember]
        public int ID { get; set; }

        [DataMember]
        public int RefNo { get; set; }

        [DataMember]
        public DateTime Date { get; set; }

        [DataMember]
        [Display(AutoGenerateField = false)]
        public string TransferringSite { get; set; }

        [DataMember]
        [Display(Name = "TransferringSite")]
        public string DisplayTransferringSite { get; set; }

        [DataMember]
        [Display(AutoGenerateField = false)]
        public string ReceivingSite { get; set; }

        [DataMember]
        [Display(Name = "ReceivingSite")]
        public string DisplayReceivingSite { get; set; }

        [DataMember]
        public string item { get; set; }

        [DataMember]
        public double RequestingQty { get; set; }

        [DataMember]
        public double ApprovedQty { get; set; }

        [DataMember]
        public double PendingQty { get; set; }

        [DataMember]
        public string MU { get; set; }

        [DataMember]
        public string Remarks { get; set; }
    }
}
