﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;

namespace SIMS.MM.MODEL.Reports
{
    [DataContract]
    public class MGRNReport : EntityModel
    {
        [DataMember]
        public int ID { get; set; }

        [DataMember]
        public string RefNo { get; set; }

        [DataMember]
        [Display(AutoGenerateField = false)]
        public string Vendor { get; set; }

        [DataMember]
        [Display(Name = "Vendor")]
        public string DisplayVendor { get; set; }

        [DataMember]
        [Display(AutoGenerateField = false)]
        public string Site { get; set; }

        [DataMember]
        [Display(Name = "Site")]
        public string DisplaySite { get; set; }

        [DataMember]
        public DateTime DateofReceipt { get; set; }

        [DataMember]
        public string Purchaser { get; set; }

        [DataMember]
        public int ItemID { get; set; }

        [DataMember]
        public string Item { get; set; }

        [DataMember]
        public int CategoryID { get; set; }

        [DataMember]
        public string ItemCategory { get; set; }

        [DataMember]
        public int SizeID { get; set; }

        [DataMember]
        public string Size { get; set; }

        [DataMember]
        public int BrandID { get; set; }

        [DataMember]
        public string Brand { get; set; }

        [DataMember]
        public string MU { get; set; }

        [DataMember]
        public double QtyReceived { get; set; }

        [DataMember]
        public double PerUnitPrice { get; set; }

        [DataMember]
        public double TotalAmount { get; set; }

        [DataMember]
        public string InvoiceNo { get; set; }

        [DataMember]
        public string ChallanNo { get; set; }

        [DataMember]
        public int? PRNO { get; set; }

        [DataMember]
        public int? PONO { get; set; }

        [DataMember]
        public string Remarks { get; set; }
    }
}
