﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SIMS.MM.DAL.PrintReports
{
    using SIMS.ReportLoader;

    public class DPPrintLoader : ReportLoader
    {
        public override RDLCReportData GetReportData(ReportParameterDictionary Dic)
        {
            RDLCReportData data = new RDLCReportData();

            parnamidb1Entities db = new parnamidb1Entities();
            MMReportsDBDataContext rdb = new MMReportsDBDataContext();

            int dpid = (int)Dic["id"];

            var header = rdb.GetDPHeaders(null, null, null, null, null).Where(x => x.ID == dpid).ToList();
            var detail = rdb.GetDPDetailsByUniqueID(dpid);
            var vendor = rdb.GetVendorDetailsbyVendorID(header.First().VendorID);
            var site = rdb.GetSiteDetailsbySiteID(header.First().BUID);

            data.Add("Headers", header);
            data.Add("Details", detail);
            data.Add("VendorDetails", vendor);
            data.Add("SiteDetails", site);

            return data;
        }
    }
}
