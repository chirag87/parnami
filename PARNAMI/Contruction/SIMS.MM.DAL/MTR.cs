﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SIMS.MM.DAL
{
    public partial class MTR
    {
        public string ApprovalStatus
        {
            get
            {
                if (this.IsApproved)
                    return "Approved";
                else
                    return "Pending Approval";
            }
        }

        //public string ReceivedStatus
        //{
        //    get
        //    {
        //        if (this.CanReceive)
        //            return "Received";
        //        else
        //            return "Not Received";
        //    }
        //}

        //public string ReleasedStatus
        //{
        //    get
        //    {
        //        if (this.CanRelease)
        //            return "Released";
        //        else
        //            return "Not Released";
        //    }
        //}
        
        public string ClosedStatus
        {
            get
            {
                if (this.IsClosed)
                    return "Closed!";
                else
                    return "Pending...";
            }
        }

        //public string TransporterName
        //{
        //    get
        //    {
        //        if (Transporter.Name != null)
        //            return this.transpo;
        //        else
        //            return "N/A";
        //    }
        //}
    }
}
