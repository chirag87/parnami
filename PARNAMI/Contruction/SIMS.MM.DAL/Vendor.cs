﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SIMS.MM.DAL
{
    public partial class Vendor
    {
        public string DisplayVendor
        {
            get 
            {
                return Name + " (ID: " + ID + ", Address: " + RegisteredAddress + ", " + City+")";
            }
        }
    }
}
