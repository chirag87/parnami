﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace Contruction
{
    public partial class MainPage : UserControl
    {
        public MainPage()
        {
            InitializeComponent();
            Base.Current.InitForNavigation(this.ContentBorder);
            Base.Current.MainTitleBlock = this.tblkMainHeader;
            //LoginPage p = new LoginPage();
            //p.Visibility = Visibility.Visible;
            ContentBorder.Child= new LoginPage();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            //ErrorWindow err = new ErrorWindow();
            //err.Show();

            //RegistrationForm rf = new RegistrationForm();
            //ContentBorder.Child = rf;

            //AllSites ass = new AllSites();
            //ContentBorder.Child = ass;

            //PopUp pu = new PopUp();
            //pu.Show();

            //LoginForm lf = new LoginForm();
            //lf.Show();

            //CFTCalculatorPopup cc = new CFTCalculatorPopup();
            //cc.Show();

            //AddNewItem an = new AddNewItem();
            //ContentBorder.Child = an;

            //ViewItems vi = new ViewItems();
            //ContentBorder.Child = vi;

            //NewConsumption nc = new NewConsumption();
            //ContentBorder.Child = nc;

            //ItemWiseReporting ir = new ItemWiseReporting();
            //ContentBorder.Child = ir;

            //SiteWiseReporting sr = new SiteWiseReporting();
            //ContentBorder.Child = sr;

            //VendorWiseReporting vr = new VendorWiseReporting();
            //ContentBorder.Child = vr;

            //MTRView mv = new MTRView();
            //ContentBorder.Child = mv;

            //MTRReleaseDetails md = new MTRReleaseDetails();
            //ContentBorder.Child = md;

            //AllMTRs am = new AllMTRs();
            //ContentBorder.Child = am;

            //ReceiveMTR rm = new ReceiveMTR();
            //ContentBorder.Child = rm;

            //ReleaseMTR rlm = new ReleaseMTR();
            //ContentBorder.Child = rlm;

            //POView pv = new POView();
            //ContentBorder.Child = pv;

            //NewPR np = new NewPR();
            //ContentBorder.Child = np;

            //PRView pv = new PRView();
            //ContentBorder.Child = pv;

            //ViewPRs VP = new ViewPRs();
            //ContentBorder.Child = VP;

            //AllCashPurchases acp = new AllCashPurchases();
            //ContentBorder.Child = acp;

            //AllReceivings ar = new AllReceivings();
            //ContentBorder.Child = ar;

            //NewCashPurchase cp = new NewCashPurchase();
            //ContentBorder.Child = cp;

            //NewReceiving mr = new NewReceiving();
            //ContentBorder.Child = mr;

            //Cash_Report cr = new Cash_Report();
            //ContentBorder.Child = cr;

            //DirectPurchase_Report dpr = new DirectPurchase_Report();
            //ContentBorder.Child = dpr;

            //ItemWiseReport iwr = new ItemWiseReport();
            //ContentBorder.Child = iwr;

            //MTR_Report mr = new MTR_Report();
            //ContentBorder.Child = mr;

            //ReportsMaster rm = new ReportsMaster();
            //ContentBorder.Child = rm;
        }

        private void hpBtnLogin_Click(object sender, RoutedEventArgs e)
        {
            LoginForm lf = new LoginForm();
            lf.Show();
        }

        private void hpBtnChngPsswrd_Click(object sender, RoutedEventArgs e)
        {
            ChangePassword cp = new ChangePassword();
            cp.Show();
        }

        private void hpBtnFeedback_Click(object sender, RoutedEventArgs e)
        {
            Feedback fb = new Feedback();
            //ContentBorder.Child = fb;
            ContentBorder.Child = fb;
        }
    }
}
