﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace Contruction.Hyperlinks
{
    public class IDBasedHyperLink : HyperlinkButton
    {
        public IDBasedHyperLink()
        {
            this.Content = "Print";
        }

        public int ID
        {
            get { return (int)GetValue(IDProperty); }
            set
            {
                SetValue(IDProperty, value);
                SetURI();
            }
        }

        // Using a DependencyProperty as the backing store for ID.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty IDProperty =
            DependencyProperty.Register("ID", typeof(int), typeof(VendorPrintLink), new PropertyMetadata(1));


        public string Type { get; set; }

        internal void SetURI()
        {
            this.NavigateUri = new Uri(String.Format("/GetReport.ashx?type={1}&id={0}", ID,Type), UriKind.Relative);
        }
    }

    public class VendorPrintLink:IDBasedHyperLink
    {
        public VendorPrintLink()
        {
            OnInit();
        }

        void OnInit()
        {
            this.Type = "VendorBill";
        }

        
    }
}
