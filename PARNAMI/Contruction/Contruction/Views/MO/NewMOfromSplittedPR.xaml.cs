﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Collections.ObjectModel;

namespace Contruction
{
    public partial class NewMOfromSplittedPR : UserControl
    {
        public NewMOfromSplittedPRModel Model
        {
            get { return (NewMOfromSplittedPRModel)this.DataContext; }
        }
        
        public NewMOfromSplittedPR()
        {
            InitializeComponent();
        }

        int _OldPRID;
        public int OldPRID
        {
            get { return _OldPRID; }
            set
            {
                _OldPRID = value;
                Model.Notify("OldPRID");
            }
        }

        ObservableCollection<int> _OldPRLineIDs = new ObservableCollection<int>();
        public ObservableCollection<int> OldPRLineIDs
        {
            get { return _OldPRLineIDs; }
            set
            {
                _OldPRLineIDs = value;
                Model.Notify("OldPRLineIDs");
            }
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            this.MyPopUP.Close();
        }

        private void btnCreateMO_Click(object sender, RoutedEventArgs e)
        {
            Model.CreateNewSplittedMO(OldPRID, OldPRLineIDs);
        }

        #region CreatePopUP Members
        public bool IsPopUpMode
        {
            get { return MyPopUP != null; }
        }

        public PopUp MyPopUP { get; set; }

        public PopUp CreatePopup()
        {
            MyPopUP = this.ToPopup(false);
            MyPopUP.SetAutoLayout();
            MyPopUP.Title = "New Move Order";
            return MyPopUP;
        }
        #endregion

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            transferringSiteCombo.Focus();
        }
    }
}
