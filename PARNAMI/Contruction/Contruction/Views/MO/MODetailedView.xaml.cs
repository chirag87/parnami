﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using SIMS.MM.MODEL;

namespace Contruction
{
    public partial class MODetailedView : ChildWindow
    {
        public MODetailedViewModel Model
        {
            get { return (MODetailedViewModel)this.DataContext; }
        }

        public MODetailedView()
        {
            InitializeComponent();
        }

        private void btnConfirm_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

        private void btnClose_Click(object sender, RoutedEventArgs e)
        {
            var data = sender as Button;
            var mo = (MODetailedViewModel)data.DataContext;
            if (mo == null) return;
            Model.CloseMO(mo.ConvertedMO.ID);
            this.DialogResult = false;
        }

        private void ChildWindow_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Escape)
            {
                this.DialogResult = false;
            }
        }
    }
}

