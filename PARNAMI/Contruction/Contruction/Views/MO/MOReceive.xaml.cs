﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace Contruction.Views.MO
{
    public partial class MOReceive : ChildWindow
    {
        public MOReceiveModel Model
        {
            get { return (MOReceiveModel)this.DataContext; }
        }

        public MOReceive(int id)
        {
            InitializeComponent();
            Model.GetReceivingforMOReceive(id);
            Model.Received += new EventHandler(Model_Received);
        }

        void Model_Received(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

        private void textBlock_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {

        }

        private void btnReceive_Click(object sender, RoutedEventArgs e)
        {
            if (Model.IsReceived)
                this.DialogResult = true;
        }

        private void ChildWindow_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Escape)
            {
                this.DialogResult = false;
            }
        }

        private void ChildWindow_Loaded(object sender, RoutedEventArgs e)
        {
            requestingSiteCombo.Focus();
        }
    }
}

