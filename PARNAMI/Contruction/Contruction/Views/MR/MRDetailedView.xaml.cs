﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace Contruction
{
    public partial class MRDetailedView : ChildWindow
    {
        public MRDetailedViewModel Model
        {
            get { return (MRDetailedViewModel)this.DataContext; }
        }

        public MRDetailedView(int id)
        {
            InitializeComponent();
            Model.SetID(id);
        }

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

        private void ChildWindow_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Escape)
            {
                this.DialogResult = false;
            }
        }

        private void ChildWindow_Loaded(object sender, RoutedEventArgs e)
        {
            requestingSiteCombo.Focus();
        }
    }
}

