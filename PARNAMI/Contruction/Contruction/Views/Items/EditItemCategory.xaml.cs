﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using SIMS.MM.MODEL;

namespace Contruction
{

    public class UpdateItemCategoryArgs : EventArgs
    {
        public MItemCategory Category { get; private set; }
        public UpdateItemCategoryArgs(MItemCategory _Category) { Category = _Category; }
    }

    public partial class EditItemCategory : ChildWindow
    {
        public event EventHandler<UpdateItemCategoryArgs> Updated;
        public void RaiseUpdated(MItemCategory _ItemCategory)
        {
            if (Updated != null)
                Updated(this, new UpdateItemCategoryArgs(_ItemCategory));
        }

        public event EventHandler Cancelled;
        public void RaiseCancelled()
        {
            if (Cancelled != null)
                Cancelled(this, new EventArgs());
        }

        public EditItemCategory()
        {
            InitializeComponent();
            Model.Updated += new EventHandler<UpdateItemCategoryArgs>(Model_Updated);
            this.Closed += new EventHandler(EditItemCategory_Closed);            
        }

        void EditItemCategory_Closed(object sender, EventArgs e)
        {
            RaiseCancelled();
        }

        void Model_Updated(object sender, UpdateItemCategoryArgs e)
        {
            RaiseUpdated(e.Category);
        }

        public EditItemCategoryVM Model
        {
            get { return (EditItemCategoryVM)this.DataContext; }
        }

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
            RaiseCancelled();
        }

        public MItemCategory ItemCategory
        {
            get { return Model.ItemCategory; }
            set
            {
                Model.ItemCategory = value;
            }
        }

        private void LayoutRoot_KeyDown(object sender, KeyEventArgs e)
        {

        }

        private void ChildWindow_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Escape)
                this.DialogResult = false;
        }

        private void ChildWindow_Loaded(object sender, RoutedEventArgs e)
        {
            tbxName.Focus();
        }
    }
}

