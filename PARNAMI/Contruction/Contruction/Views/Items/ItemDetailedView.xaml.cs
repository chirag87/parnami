﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.ComponentModel;

namespace Contruction
{
    public partial class ItemDetailedView : UserControl, INotifyPropertyChanged
    {
        public ItemDetailedView()
        {
            InitializeComponent();
        }

        public NewItemModel Model
        {
            get { return (NewItemModel)this.DataContext; }
        }
				
        public void LoadData(int id)
        {
            Base.Current.GetItemByID(id);
        }

        int _id;
        public int id
        {
            get { return _id; }
            set
            {
                _id = value;
                Notify("id");
            }
        }

        private void btnAddNewDoc_Click(object sender, RoutedEventArgs e)
        {
            FileUploader upload = new FileUploader();
            upload.Show();
        }

        private void btnUploadFile_Click(object sender, RoutedEventArgs e)
        {

        }

        private void btnClear_Click(object sender, RoutedEventArgs e)
        {

        }

        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            if (Model.IsNew == false)
            {
                Model.UpdateItemDetails();
            }
            else
            {
                Model.AddNewItem();
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void Notify(string propertyName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }

        private void addnewMU_Click(object sender, RoutedEventArgs e)
        {
            NewMU mu = new NewMU();
            mu.Show();
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            tbxItemName.Focus();
        }
    }
}
