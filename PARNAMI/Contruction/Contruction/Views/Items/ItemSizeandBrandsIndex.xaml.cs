﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace Contruction
{
    public partial class ItemSizeandBrandsIndex : UserControl
    {
        public ItemSizeandBrandsIndex()
        {
            InitializeComponent();
        }

        private void btnNewBrand_Click(object sender, RoutedEventArgs e)
        {
            AddNewItemBrand brand = new AddNewItemBrand();
            brand.Show();
        }

        private void btnNewSize_Click(object sender, RoutedEventArgs e)
        {
            AddNewItemSize size = new AddNewItemSize();
            size.Show();
        }

        private void tbxsizeKey_TextChanged(object sender, TextChangedEventArgs e)
        {
            var box = sender as TextBox;
            Base.Current.SearchItemSize = box.Text;
        }

        private void tbxBrandKey_TextChanged(object sender, TextChangedEventArgs e)
        {
            var box = sender as TextBox;
            Base.Current.SearchItemBrand = box.Text;
        }

        private void btnExportBrands_Click(object sender, RoutedEventArgs e)
        {
            dgdBrand.Export();
        }

        private void btnExportSizes_Click(object sender, RoutedEventArgs e)
        {
            dgdSize.Export();
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            tbxsizeKey.Focus();
        }
    }
}
