﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using SIMS.MM.MODEL;
using System.Collections.ObjectModel;
using System.ComponentModel;
using Contruction.MasterDBService;
namespace Contruction
{
    public class Node : INotifyPropertyChanged
    {
        private String text;

        private ObservableCollection<Node> children;

        public event PropertyChangedEventHandler PropertyChanged;

        public ObservableCollection<Node> Children
        {
            get { return children; }
            set { children = value; }
        }

        public String Text
        {
            get { return text; }
            set { text = value; }
        }

        public Node(String text)
        {
            Children = new ObservableCollection<Node>();
            Text = text;
        }

        public void Add(Node node)
        {
            children.Add(node);
            NotifyPropertyChanged("Children");
        }

        public void Delete(Node node)
        {
            children.Remove(node);
            NotifyPropertyChanged("Children");
        }

        private void NotifyPropertyChanged(String info)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(info));
        }
    }

    public class ItemCategoryNode : ViewModel
    {
        MItemCategory _Category;
        public MItemCategory Category
        {
            get { return _Category; }
            set
            {
                _Category = value;
                Notify("Category");
            }
        }

        public ItemCategoryNode ParentNode { get; set; }

        public ObservableCollection<ItemCategoryNode> ChildNodes { get; private set; }

        public bool HasParent { get { return ParentNode != null; } }

        ItemCategoryNode()
        {
            Category = new MItemCategory();
            ChildNodes = new ObservableCollection<ItemCategoryNode>();

        }

        public ItemCategoryNode(MItemCategory category)
            : this()
        {
            Category = category;
        }

        public void AddChildNode(ItemCategoryNode _node)
        {
            ChildNodes.Add(_node);
            _node.ParentNode = this;
        }

        public ItemCategoryNode AddChildNode(MItemCategory _category)
        {
            var _node = new ItemCategoryNode(_category);
            ChildNodes.Add(_node);
            _node.ParentNode = this;
            return _node;
        }

        public override string ToString()
        {
            return Category.Name;
        }

        public void AddChildNodes(IEnumerable<MItemCategory> categories)
        {
            try
            {
                int? matchWith = null;
                if (this.Category.ID != 0) matchWith = this.Category.ID;

                var listToAdd = categories.Where(x => x.ParentID == matchWith);
                foreach (var item in listToAdd)
                {
                    var node = this.AddChildNode(item);
                    node.AddChildNodes(categories);
                }
            }
            catch
            {

            }
        }

        public static ObservableCollection<ItemCategoryNode> GenerateNodes(IEnumerable<MItemCategory> categories)
        {
            ItemCategoryNode fakeNode = new ItemCategoryNode();
            fakeNode.AddChildNodes(categories);
            return fakeNode.ChildNodes;
        }
    }

    public class ItemCategoryTreeViewItem : TreeViewItem
    {

        public static event EventHandler<MouseButtonEventArgs> ContextMenuRequested;
        public void RaiseContextMenuRequested(MouseButtonEventArgs e)
        {
            if (ContextMenuRequested != null)
                ContextMenuRequested(this, e);
        }

        public static event EventHandler MouseLeftButtonDownEvent;
        public void RaiseMouseLeftButtonDown()
        {
            if (MouseLeftButtonDownEvent != null)
                MouseLeftButtonDownEvent(this, new EventArgs());
        }

        public ItemCategoryTreeViewItem(ItemCategoryNode Node)
        {
            this.Header = Node;
            this.DataContext = Node.Category;
            foreach (var childNode in Node.ChildNodes)
            {
                this.Items.Add(new ItemCategoryTreeViewItem(childNode));
            }
            this.MouseRightButtonUp += new MouseButtonEventHandler(ItemCategoryTreeViewItem_MouseRightButtonUp);
            this.MouseLeftButtonUp += new MouseButtonEventHandler(ItemCategoryTreeViewItem_MouseLeftButtonDown);
        }

        void ItemCategoryTreeViewItem_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            RaiseMouseLeftButtonDown();
        }

        public override string ToString()
        {
            if (this.DataContext != null)
                return ((MItemCategory)this.DataContext).Name;
            return "NULL";

        }

        void ItemCategoryTreeViewItem_MouseRightButtonUp(object sender, MouseButtonEventArgs e)
        {
            RaiseContextMenuRequested(e);
        }
    }

    public partial class ItemCategoryTree : UserControl
    {
        MasterDataServiceClient service = new MasterDataServiceClient();
        ObservableCollection<Node> objectTree = new ObservableCollection<Node>();
        Node selectedNode;
        public TreeViewItem draggedItem { get; set; }

        #region Events

        public event EventHandler NodesSetting;
        public void RaiseNodesSetting()
        {
            if (NodesSetting != null)
                NodesSetting(this, new EventArgs());
        }

        public event EventHandler NodesSet;
        public void RaiseNodesSet()
        {
            if (NodesSet != null)
                NodesSet(this, new EventArgs());
        }
        #endregion

        #region Constructors

        public ItemCategoryTree()
        {
            InitializeComponent();
            Model.ItemCategoriesLoaded += new EventHandler(Model_ItemCategoriesLoaded);
            service.UpdateParentForItemCategoryCompleted += new EventHandler<AsyncCompletedEventArgs>(service_UpdateParentForItemCategoryCompleted);
            ItemCategoryTreeViewItem.ContextMenuRequested += new EventHandler<MouseButtonEventArgs>(ItemCategoryTreeViewItem_ContextMenuRequested);
            ItemCategoryTreeViewItem.MouseLeftButtonDownEvent += new EventHandler(ItemCategoryTreeViewItem_MouseLeftButtonDownEvent);
        }

        void ItemCategoryTreeViewItem_MouseLeftButtonDownEvent(object sender, EventArgs e)
        {
            Model.ItemsByCategory = new ObservableCollection<MItem>();
            var node = sender as ItemCategoryTreeViewItem;
            LoadLeafCategories(node);
            //LoadItems(node);
        }

        private void LoadLeafCategories(ItemCategoryTreeViewItem node)
        {
            var data = (MItemCategory)(node).DataContext;
            if (data.CanHaveItem == true)
            {
                Model.LoadAllLeafCategories(data.ID);
                Model.ParentItemCategory = data;////////////////////////////////////////////////////////////////////
                Model.SingleCategoryName = data.Name;
            }
            else
            {
                //MessageBox.Show("Doesn't have Items!");
            }

            Model.LeafAdded += (s, e) =>
            {
                var data1 = (MItemCategory)(node).DataContext;
                if (data1.CanHaveItem == true)
                {
                    Model.LoadAllLeafCategories(data1.ID);
                    Model.ParentItemCategory = data1;////////////////////////////////////////////////////////////////////
                }
                else
                {
                    //MessageBox.Show("Doesn't have Items!");
                }
            };
        }

        //void LoadItems(ItemCategoryTreeViewItem node)
        //{
        //    Model.ItemCategory2 = (MItemCategory)(node).DataContext;
        //    Model.LoadItemsForCategory();
        //}

        void ItemCategoryTreeViewItem_ContextMenuRequested(object sender, MouseButtonEventArgs e)
        {
            var node = sender as ItemCategoryTreeViewItem;
            ShowContextMenu(e, node);
        }

        void service_UpdateParentForItemCategoryCompleted(object sender, AsyncCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                Base.ShowError(e.Error);
            }
            else
            {
                //MessageBox.Show("Shifted Successfully");
            }
        }

        void Model_ItemCategoriesLoaded(object sender, EventArgs e)
        {
            this.ItemCategories = null;
            this.ItemCategories = Model.AllItemCategories;/////////////////////////////////////////
        }

        public ItemCategoryTree(ObservableCollection<ItemCategoryNode> _Nodes)
            : this()
        {
            this.Nodes = _Nodes;
        }

        public ItemCategoryTree(IEnumerable<MItemCategory> Categories)
            : this()
        {
            SetCategories(Categories);
        }
        #endregion

        #region ViewModel

        public NewItemModel Model
        {
            get { return (NewItemModel)this.DataContext; }
        }

        #endregion

        void SetCategories(IEnumerable<MItemCategory> Categories)
        {
            this.Nodes = ItemCategoryNode.GenerateNodes(Categories);
        }

        public IEnumerable<MItemCategory> ItemCategories
        {
            get { return (IEnumerable<MItemCategory>)GetValue(ItemCategoriesProperty); }
            set
            {
                SetValue(ItemCategoriesProperty, value);
                SetCategories(value);
            }
        }

        // Using a DependencyProperty as the backing store for ItemCategories.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ItemCategoriesProperty =
            DependencyProperty.Register("ItemCategories", typeof(IEnumerable<MItemCategory>), typeof(ItemCategoryTree), new PropertyMetadata(new List<MItemCategory>()));


        ObservableCollection<ItemCategoryNode> _Nodes;
        public ObservableCollection<ItemCategoryNode> Nodes
        {
            get { return _Nodes; }
            set
            {
                RaiseNodesSetting();
                OnSettingNodes();
                _Nodes = value;
                OnNodesSet();
                RaiseNodesSet();
            }
        }

        public virtual void OnSettingNodes() { }

        public virtual void OnNodesSet()
        {
            ResetNodes();
        }

        void ResetNodes()
        {
            if (Nodes == null) return;
            TVIAllItems.Items.Clear();
            foreach (var node in Nodes)
            {
                TVIAllItems.Items.Add(new ItemCategoryTreeViewItem(node));
            }
        }

        private void ItemsTree_Drop(object sender, DragEventArgs e)
        {
            //MessageBox.Show("Would you Like to drop"+selectedItem.Header.ToString()+"into this node","", MessageBoxButton.OKCancel);
            MessageBox.Show("Drop");
        }

        private void ItemsTree_DragOver(object sender, DragEventArgs e)
        {
            MessageBox.Show("DragOver");
        }

        #region /////////////////ContextMenuButton Clicks///////////////////////////////
        private void AddButton_Click(object sender, RoutedEventArgs e)
        {

            var hbtn = sender as Button;
            AddNewItemCategory c = new AddNewItemCategory();

            try
            {
                var pnode = (ItemCategoryTreeViewItem)hbtn.DataContext;
                c.ParentCategory = (MItemCategory)(pnode).DataContext;
                c.Added += (s, e1) =>
                {
                    var itemNode = (ItemCategoryNode)pnode.Header;
                    itemNode.AddChildNode(e1.Category);
                    pnode.Items.Add(new ItemCategoryTreeViewItem(new ItemCategoryNode(e1.Category)));
                    pnode.IsExpanded = true;
                };
            }
            catch
            {
                c.Added += (s, e1) =>
                {
                    TVIAllItems.Items.Add(new ItemCategoryTreeViewItem(new ItemCategoryNode(e1.Category)));
                    TVIAllItems.IsExpanded = true;
                };
            }
            c.Show();
            HideContextMenu();
        }

        private void EditButton_Click(object sender, RoutedEventArgs e)
        {
            var hbtn = sender as Button;
            EditItemCategory category = new EditItemCategory();
            var pnode = (ItemCategoryTreeViewItem)hbtn.DataContext;
            var itemNode = (ItemCategoryNode)pnode.Header;
            category.ItemCategory = (MItemCategory)(pnode).DataContext;
            pnode.Header = "Updating ...";
            category.Updated += (s, e1) =>
                {
                    itemNode.Category = e1.Category;
                    pnode.Header = itemNode;
                    pnode.DataContext = e1.Category;
                };
            category.Cancelled += (s, e1) =>
                {
                    pnode.Header = itemNode;
                };
            category.Show();
            HideContextMenu();
        }

        private void DeleteButton_Click(object sender, RoutedEventArgs e)
        {
            TreeViewItem selectedTreeViewItem =
               TreeViewExtensions.GetContainerFromItem(ItemsTree, selectedNode);
            if (selectedTreeViewItem != null)
            {
                TreeViewItem selectedTreeViewItemParent =
                    TreeViewExtensions.GetParentTreeViewItem(selectedTreeViewItem);
                if (selectedTreeViewItemParent != null)
                {
                    Node seleactedParentNode =
            (Node)selectedTreeViewItemParent.DataContext;
                    seleactedParentNode.Delete(selectedNode);
                }
                else
                {
                    objectTree.Remove(selectedNode);
                }
            }
            HideContextMenu();
        }

        private void ExitButton_Click(object sender, RoutedEventArgs e)
        {
            HideContextMenu();
        }
        #endregion

        private void ShowContextMenu(MouseButtonEventArgs e, object ParentCategory)
        {
            e.Handled = true;
            Point p = e.GetPosition(this);
            ContextMenu.Visibility = Visibility.Visible;
            ContextMenu.DataContext = ParentCategory;
            ContextMenu.IsOpen = true;
            ContextMenu.SetValue(Canvas.LeftProperty, (double)p.X);
            ContextMenu.SetValue(Canvas.TopProperty, (double)p.Y);
        }

        private void HideContextMenu()
        {
            ContextMenu.Visibility = Visibility.Collapsed;
            ContextMenu.IsOpen = false;
        }

        private void EnalbleEditForSelectedItem()
        {
            if (selectedNode != null)
            {
                SetTemplateForSelectedItem("TreeViewMainEditTemplate");
            }
        }

        private void DisableEditForSelectedItem()
        {
            if (selectedNode != null)
            {
                SetTemplateForSelectedItem("TreeViewMainReadTemplate");
                selectedNode = null;
            }
        }

        private void SetTemplateForSelectedItem(String templateName)
        {
            HierarchicalDataTemplate hdt = (HierarchicalDataTemplate)Resources[templateName];
            TreeViewItem selectedTreeViewItem = TreeViewExtensions.GetContainerFromItem(ItemsTree, selectedNode);
            if (selectedTreeViewItem != null)
                selectedTreeViewItem.HeaderTemplate = hdt;
        }

        private void ItemsTree_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            HideContextMenu();
            //Point _lastMouseDown = e.GetPosition(ItemsTree);
            //DisableEditForSelectedItem();

            //var sitem = sender as TreeView;
            //Model.ItemCategory2 = (MItemCategory)((TreeViewItem)sitem.SelectedItem).DataContext;
            //Model.LoadItemsForCategory(Model.ItemCategory2.ID);
        }

        private void ItemsTree_MouseRightButtonDown(object sender, MouseButtonEventArgs e)
        {
            DisableEditForSelectedItem();
            e.Handled = true;
        }

        private void ItemsTree_MouseRightButtonUp(object sender, MouseButtonEventArgs e)
        {
            //    DisableEditForSelectedItem();
            //    if (sender is TextBlock)
            //    {
            //        selectedNode = (Node)((sender as TextBlock).DataContext);
            //    }
            //    else
            //    {
            //        selectedNode = null;
            //    }
            //    ShowContextMenu(e);
        }

        private void newFixedTreeViewDragDropTarget_Drop(object sender, DragEventArgs e)
        {
            MessageBox.Show("Droped");
        }

        private void newFixedTreeViewDragDropTarget_ItemDroppedOnTarget(object sender, ItemDragEventArgs e)
        {
            try
            {
                var node = (ItemCategoryTreeViewItem)((SelectionCollection)e.Data).First().Item;
                var data = (MItemCategory)node.DataContext;

                var newParentNode = node.Parent as ItemCategoryTreeViewItem;
                if (newParentNode != null)
                {
                    var Parentdata = newParentNode.DataContext as MItemCategory;
                    service.UpdateParentForItemCategoryAsync(data.ID, Parentdata.ID);
                }
            }
            catch
            {
                MessageBox.Show("Not Possible");
            }
        }

        private void newFixedTreeViewDragDropTarget_ItemDroppedOnSource(object sender, Microsoft.Windows.DragEventArgs e)
        {
            //MessageBox.Show(e.OriginalSource.ToString());
            //MessageBox.Show(e.Data.ToString());
            //MessageBox.Show(sender.ToString());
            MessageBox.Show("Source and Destination are Same");
        }

        private void TVIAllItems_MouseRightButtonUp(object sender, MouseButtonEventArgs e)
        {
            DisableEditForSelectedItem();
            if (sender is TextBlock)
            {
                selectedNode = (Node)((sender as TextBlock).DataContext);
            }
            else
            {
                selectedNode = null;
            }
            ShowContextMenu(e, new MItemCategory());
        }

        private void TVIAllItems_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            MessageBox.Show("hello:" + e);
            HideContextMenu();
        }

        private void ContextMenu_MouseLeave(object sender, MouseEventArgs e)
        {
            HideContextMenu();
        }
    }
}
