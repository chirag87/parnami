﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using SIMS.MM.MODEL;

namespace Contruction
{
    public partial class ViewPRs : UserControl
    {
        public PRViewModel PRModel
        {
            get { return (PRViewModel)this.DataContext; }
        }
				
        public ViewPRs()
        {
            InitializeComponent();
            PRModel.PRChanged += new EventHandler(PRModel_PRChanged);
        }

        void PRModel_PRChanged(object sender, EventArgs e)
        {
            myPager.PagedData = (IPagination)PRModel.PRs;
        }

        private void dataGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var SelectedRow = (MPRHeader)(sender as DataGrid).SelectedItem;
            if (SelectedRow == null) return;
            prView.SetID(SelectedRow.PRNo);
        }

        private void Image_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            var img = sender as Image;
            var prheader = (MPRHeader)img.DataContext;
            if (prheader == null) return;
            var popup = new PRView();
            popup.SetID(prheader.PRNo);
            popup.CreatePopup();
        }

        private void MyPager_MoveToPage(object sender, Controls.MyPageChangedEventArgs e)
        {
            PRModel.PageIndex = e.RequestedPage;
        }

        private void btnDelete_Click(object sender, RoutedEventArgs e)
        {
            PRModel.DeletePR((MPRHeader)(sender as HyperlinkButton).DataContext);
        }

        private void SearchBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                PRModel.SearchKey = SearchBox.Text;
                PRModel.LoadPRs();
            }
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            SearchBox.Focus();
        }
    }
}