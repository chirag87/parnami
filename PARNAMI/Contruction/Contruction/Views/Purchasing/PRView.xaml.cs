﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using SIMS.MM.MODEL;
using System.Collections.ObjectModel;

namespace Contruction
{
    public partial class PRView : UserControl
    {
        public ViewPRDetailsModel Model
        {
            get { return (ViewPRDetailsModel)this.DataContext; }
        }

        public PRView()
        {
            InitializeComponent();
            Base.AppEvents.POConverted +=new EventHandler<PORequestedArgs>(AppEvents_POConverted);
        }

        int? poid;

        void AppEvents_POConverted(object sender, PORequestedArgs e)
        {
            if (IsPopUpMode)
            {
                poid = e.POID;
                this.MyPopUP.Closed += new EventHandler(MyPopUP_Closed);
                this.MyPopUP.Close();
            }
        }

        void MyPopUP_Closed(object sender, EventArgs e)
        {
            if (!poid.HasValue) return;
            var win = new PODetailView();
            win.SetID(poid.Value);
            win.CreatePopup();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {

        }

        private void Email_Clicked(object sender, RoutedEventArgs e)
        {

        }

        private void ConvertToPO_Click(object sender, RoutedEventArgs e)
        {
            //PRDetailedView pr = new PRDetailedView();
            //pr.DialogResult = true;
        }

        private void EmailToVendor_Clicked(object sender, RoutedEventArgs e)
        {

        }

        internal void SetID(int p)
        {
            Model.SetID(p);
        }

        private void dataGrid_CellEditEnded(object sender, DataGridCellEditEndedEventArgs e)
        {
            Model.UpdateNetAmount();
            Model.UpdateTotal();
            Model.UpdateTotalDiscount();
        }

        private void btnToPR_Click(object sender, RoutedEventArgs e)
        {
            var btn = sender as Button;
            var data = (ViewPRDetailsModel)btn.DataContext;
            if (data.ConvertedPR.PRLines.Any(x => x.IsSelected == true))
            {
                ObservableCollection<MPRLine> SelectedLines = new ObservableCollection<MPRLine>();
                data.ConvertedPR.PRLines.Where(x => x.IsSelected == true).ToList().ForEach(x=> SelectedLines.Add(x));

                if (data.ConvertedPR.PRLines.Count() == SelectedLines.Count())
                {
                    MessageBox.Show("All Lines Can not be Splitted!");
                }
                else
                {
                    ObservableCollection<int> selectedLineIDs = new ObservableCollection<int>();    //Getting Line IDs of Selected Lines
                    foreach (var lineid in SelectedLines)
                    {
                        selectedLineIDs.Add(lineid.LineID);
                    }

                    CreateNewSplittedPR(ConvertToMPR(data, SelectedLines), data.ConvertedPR.PRNumber, selectedLineIDs);
                }
            }
            else
            {
                MessageBox.Show("No Row Selected!");
            }
        }

        public void CreateNewSplittedPR(MPR newpr, int OldPRID, ObservableCollection<int> lineIDs)
        {
            NewPRModel pr = new NewPRModel();
            pr.CreateNewSplittedPR(newpr, OldPRID, lineIDs);
            pr.SaveSplitPR += new EventHandler<NewPRModel.SplittedPRRequestedArgs>(pr_SaveSplitPR);
        }

        void pr_SaveSplitPR(object sender, NewPRModel.SplittedPRRequestedArgs e)
        {
            var popup = new PRView();
            popup.Model.ConvertedPR = e.mpr;
            popup.CreatePopup();
        }

        public MPR ConvertToMPR(ViewPRDetailsModel data, ObservableCollection<MPRLine> SelectedLines)
        {
            MPR mpr = new MPR()
            {
                ApprovalStatus = data.ConvertedPR.ApprovalStatus,
                ApprovedBy = data.ConvertedPR.ApprovedBy,
                ContactMobile = data.ConvertedPR.ContactMobile,
                ContactPerson = data.ConvertedPR.ContactPerson,
                ConvertedToPO = data.ConvertedPR.ConvertedToPO,
                CurrentOwnerID = data.ConvertedPR.CurrentOwnerID,
                Frieght = data.ConvertedPR.Frieght,
                GrandTotal = data.ConvertedPR.GrandTotal,
                HasChanges = data.ConvertedPR.HasChanges,
                IsApproved = data.ConvertedPR.IsApproved,
                IsBlocked = data.ConvertedPR.IsBlocked,
                NetAmount = data.ConvertedPR.NetAmount,
                PaymentTerms = data.ConvertedPR.PaymentTerms,
                pCST = data.ConvertedPR.pCST,
                PRNumber = data.ConvertedPR.PRNumber,
                PRRemarks = data.ConvertedPR.PRRemarks,
                Purchaser = data.ConvertedPR.Purchaser,
                PurchaseType = data.ConvertedPR.PurchaseType,
                pVAT = data.ConvertedPR.pVAT,
                RaisedBy = data.ConvertedPR.RaisedBy,
                RaisedOn = data.ConvertedPR.RaisedOn,
                ReferenceNo = data.ConvertedPR.ReferenceNo,
                Site = data.ConvertedPR.Site,
                SiteID = data.ConvertedPR.SiteID,
                TAX = data.ConvertedPR.TAX,
                TotalDiscount = data.ConvertedPR.TotalDiscount,
                TotalValue = data.ConvertedPR.TotalValue,
                Vendor = data.ConvertedPR.Vendor,
                VendorID = data.ConvertedPR.VendorID,
                PRLines = SelectedLines
            };

            return mpr;
        }

        private void btnToMO_Click(object sender, RoutedEventArgs e)
        {
            var btn = sender as Button;
            var data = (ViewPRDetailsModel)btn.DataContext;
            if (data.ConvertedPR.PRLines.Any(x => x.IsSelected == true))
            {
                ObservableCollection<MMOLine> SelectedLines = new ObservableCollection<MMOLine>();
                data.ConvertedPR.PRLines.Where(x => x.IsSelected == true).ToList().ForEach(x => SelectedLines.Add(ConvertToMOLines(x)));

                if (data.ConvertedPR.PRLines.Count() == SelectedLines.Count())
                {
                    MessageBox.Show("All Lines Can not be Splitted!");
                }
                else
                {
                    ObservableCollection<int> selectedLineIDs = new ObservableCollection<int>();    //Getting Line IDs of Selected Lines
                    foreach (var lineid in SelectedLines)
                    {
                        selectedLineIDs.Add(lineid.LineID);
                    }

                    CreateNewSplittedMO(ConvertToMMO(data, SelectedLines), data.ConvertedPR.PRNumber, selectedLineIDs);
                }
            }
            else
            {
                MessageBox.Show("No Row Selected!");
            }
        }

        public void CreateNewSplittedMO(MMO newMO, int OldPRID, ObservableCollection<int> lineIDs)
        {
            NewMOfromSplittedPRModel mo = new NewMOfromSplittedPRModel();
            var popup = new NewMOfromSplittedPR();
            popup.Model.NewMO = newMO;
            popup.OldPRID = OldPRID;
            popup.OldPRLineIDs = lineIDs;
            popup.CreatePopup();
        }

        public MMOLine ConvertToMOLines(MPRLine x)
        {
            MMOLine line = new MMOLine()
            {
                LineID = x.LineID,
                Item = x.Item,
                ItemID = x.ItemID,
                Quantity = x.Quantity,
            };
            return line;
        }

        public MMO ConvertToMMO(ViewPRDetailsModel data, ObservableCollection<MMOLine> SelectedLines)
        {
            MMO mo = new MMO()
            {
                ApprovedBy = data.ConvertedPR.ApprovedBy,
                HasChanges = data.ConvertedPR.HasChanges,
                IsApproved = data.ConvertedPR.IsApproved,
                IsBlocked = data.ConvertedPR.IsBlocked,
                RaisedBy = data.ConvertedPR.RaisedBy,
                RaisedOn = data.ConvertedPR.RaisedOn,
                ReferenceNo = data.ConvertedPR.ReferenceNo,
                RequestingSiteID = data.ConvertedPR.SiteID,
                Requester = data.ConvertedPR.Purchaser,
                MOLines = SelectedLines
            };
            return mo;
        }

        #region CreatePopUP Members
        public bool IsPopUpMode
        {
            get { return MyPopUP != null; }
        }

        public PopUp MyPopUP { get; set; }

        public PopUp CreatePopup()
        {
            MyPopUP = this.ToPopup(false);
            MyPopUP.SetAutoLayout();
            MyPopUP.Title = "PR View";
            return MyPopUP;
        }
        #endregion

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            VendorCombo.Focus();
        }
    }
}
