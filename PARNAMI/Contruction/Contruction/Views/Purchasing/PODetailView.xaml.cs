﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.IO;

namespace Contruction
{
    public partial class PODetailView : UserControl
    {
        public PODetailView()
        {
            InitializeComponent();
        }

        public ViewPODetailsModel Model
        {
            get { return (ViewPODetailsModel)this.DataContext; }
        }

        #region CreatePopUP Members
        public bool IsPopUpMode
        {
            get { return MyPopUP != null; }
        }

        public PopUp MyPopUP { get; set; }

        public PopUp CreatePopup()
        {
            MyPopUP = this.ToPopup(false);
            MyPopUP.SetAutoLayout();
            return MyPopUP;
        }
        #endregion

        internal void SetID(int p)
        {
            Model.SetID(p);
        }

        private void btn_print_Click(object sender, RoutedEventArgs e)
        {
            var data = this.Model.ConvertedPO;
            PrintFactory.PrintPO(data);
        }

        SaveFileDialog sfd;

        private void download_Click(object sender, RoutedEventArgs e)
        {
            sfd = new SaveFileDialog();
            sfd.Filter = "PDF file format|*.pdf";
            if ((bool)sfd.ShowDialog())
            {
                var hp = sender as HyperlinkButton;
                var data = (ViewPODetailsModel)hp.DataContext;
                //HtmlPage.PopupWindow(new Uri("http://localhost:3385/GetReport.ashx?type=PO&id=" + data.ConvertedPO.POID.ToString(), UriKind.Absolute), "_report", new HtmlPopupWindowOptions() { });

                WebClient webClient = new WebClient();
                webClient.OpenReadCompleted += (s, args) =>
                {
                    StreamReader sr = new StreamReader(args.Result);
                    byte[] bytes = new byte[args.Result.Length];
                    // revisit Here
                    args.Result.Read(bytes, 0, (int)args.Result.Length);
                    Stream st = sfd.OpenFile();
                    st.Write(bytes, 0, (int)args.Result.Length);
                    st.Close();
                };
                webClient.OpenReadAsync(new Uri("http://localhost:3385/GetReport.ashx?type=PO&id=" + data.ConvertedPO.POID.ToString(), UriKind.Absolute));
            }
        }
    }
}
