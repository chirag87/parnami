﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using SIMS.MM.MODEL;

namespace Contruction
{
    public partial class POView : UserControl
    {
        public POViewModel Model
        {
            get { return (POViewModel)this.DataContext; }
        }

        public POMode Mode
        {
            get { return (POMode)GetValue(ModeProperty); }
            set { SetValue(ModeProperty, value);
            SetPOVisibility();
            }
        }

        // Using a DependencyProperty as the backing store for Mode.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ModeProperty =
            DependencyProperty.Register("Mode", typeof(POMode), typeof(POView), new PropertyMetadata(POMode.Default));


        public int? RefID
        {
            get { return (int?)GetValue(RefIDProperty); }
            set { SetValue(RefIDProperty, value);
            SetPOVisibility();
            }
        }

        // Using a DependencyProperty as the backing store for RefID.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty RefIDProperty =
            DependencyProperty.Register("RefID", typeof(int?), typeof(POView), new PropertyMetadata(null));

        
				
        public POView()
        {
            InitializeComponent();
            //Model.LoadPOs(); 
            SetPOVisibility();
            Model.POChanged += new EventHandler(Model_POChanged);
        }

        void Model_POChanged(object sender, EventArgs e)
        {
            myPager.PagedData = (IPagination)Model.POs;
        }

        private void dataGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var SelectedRow = (MPOHeader)(sender as DataGrid).SelectedItem;
            if (SelectedRow == null) return;
            poDetail.SetID(SelectedRow.POID);
        }

        private void Image_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            var img = sender as Image;
            var poheader = (MPOHeader)img.DataContext;
            if (poheader == null) return;
            var pdv = new PODetailView();
            pdv.SetID(poheader.POID);
            pdv.CreatePopup();
        }

        private void myPager_MoveToPage(object sender, Controls.MyPageChangedEventArgs e)
        {
            Model.PageIndex = e.RequestedPage;
        }      

        public void SetPOVisibility()
        {
            VendorCombo.Visibility = System.Windows.Visibility.Visible;
            SiteCombo.Visibility = System.Windows.Visibility.Visible;

            //toggleButton.Visibility = System.Windows.Visibility.Visible;
            poborder.Visibility = System.Windows.Visibility.Visible;

            vendortext.Visibility = System.Windows.Visibility.Visible;
            sitetext.Visibility = System.Windows.Visibility.Visible;

            Model.DefaultSiteID = null;
            Model.DefaultVendorID = null;

            if (Mode == POMode.Vendor && RefID.HasValue)
            {
                VendorCombo.Visibility = System.Windows.Visibility.Collapsed;
                vendortext.Visibility = System.Windows.Visibility.Collapsed;
                //toggleButton.Visibility = System.Windows.Visibility.Collapsed;
                poborder.Visibility = System.Windows.Visibility.Collapsed;

                Model.DefaultVendorID = RefID;
                Model.VendorID = Model.DefaultVendorID;
                Model.LoadPOs();
            }
            else if (Mode == POMode.Site && RefID.HasValue)
            {
                SiteCombo.Visibility = System.Windows.Visibility.Collapsed;
                sitetext.Visibility = System.Windows.Visibility.Collapsed;
                //toggleButton.Visibility = System.Windows.Visibility.Collapsed;
                poborder.Visibility = System.Windows.Visibility.Collapsed;

                Model.DefaultSiteID = RefID;
                Model.SiteID = Model.DefaultSiteID;
                Model.LoadPOs();
            }
            else
            {
                Model.LoadPOs();
            }
        }

        private void btnReceive_Click(object sender, RoutedEventArgs e)
        {
            var btn = sender as Button;
            var data = (MPOHeader)btn.DataContext;
            if (data == null) return;

            POReceiving rec = new POReceiving(data.POID);
            rec.Show();

            //var popup = new NewReceiving(data.POID);
            //popup.VendorCombo.IsEnabled = false;
            //popup.SiteCombo.IsEnabled = false;
            //popup.CreatePopup();
        }

        private void SearchBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                Model.SearchKey = SearchBox.Text;
                Model.LoadPOs();
            }
        }

        private void SearchBox_TextChanged(object sender, TextChangedEventArgs e)
        {

        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            SearchBox.Focus();
        }
    }

    public enum POMode
    {
        Default,
        Vendor,
        Site,
        Item
    }
}
