﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.IO;
using System.ComponentModel;
using SIMS.MM.MODEL;

namespace Contruction
{
    public partial class VendorDetailView : UserControl, INotifyPropertyChanged
    {
        public VendorDetailView()
        {
            InitializeComponent();
            addressWindow.For = "Vendor";
            contacts.For = "Vendor";
            accountDetails.For = "Vendor";
        }

        public VendorDetailView(int id)
            :this()
        {
            Base.Current.IsNew = true;
            Base.Current.GetVendorByID(id);
            Base.Current.GetDocumentsByForID(id, "Vendor");
            this.id = id;
            addressWindow.ForID = id;
            contacts.ForID = id;
            POs.RefID = id;
            //receivingsForVendor.VendorID = id;
            accountDetails.ForID = id;
            Base.Current.VendorDetailUpdated += (s, e) => 
            {
                VendorDetailView view = new VendorDetailView(id);
            };
        }

        public VendorDetailView(int? id)
            : this()
        {
            Base.Current.IsNew = false;
            Base.Current.SelectedVendor = new MVendor();
        }

        private void btnBack_Click(object sender, RoutedEventArgs e)
        {
            Base.Redirect(new ShowVendors());
        }

        private void btnAddNewDoc_Click(object sender, RoutedEventArgs e)
        {
            DocumentWindow doc = new DocumentWindow(id, "Vendor");
            doc.Show();
        }

        int _id = 0;
        public int id
        {
            get { return _id; }
            set
            {
                _id = value;
                Notify("id");
            }
        }

        MVendor _SelectedVendor = new MVendor();
        public MVendor SelectedVendor
        {
            get { return _SelectedVendor; }
            set
            {
                _SelectedVendor = value;
                Notify("SelectedVendor");
            }
        }

        private void btnEdit_Click(object sender, RoutedEventArgs e)
        {
            EditAddress address = new EditAddress(id);
            address.Show();
        }

        private void delete_Click(object sender, RoutedEventArgs e)
        {
            var btn = sender as HyperlinkButton;
            var data = (MDocument)btn.DataContext;
            Base.Current.DeleteDocument(data);
            Base.Current.DocumentDeleted += new EventHandler(Current_DocumentDeleted);
        }

        void Current_DocumentDeleted(object sender, EventArgs e)
        {
            Base.Current.GetDocumentsByForID(id, "Vendor");
        }

        private void btnSaveVendorInfo_Click(object sender, RoutedEventArgs e)
        {

            if (Base.Current.IsNew == true)
            {
                Base.Current.UpdateVendor();
            }
            else
            {
                if (Base.Current.SelectedVendor.Name != null)
                {
                    Base.Current.AddNewVendor();
                }
                else
                {
                    MessageBox.Show("Invalid Name of Vendor !");
                }
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void Notify(string propertyName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            tbxName.Focus();
        }
    }
}