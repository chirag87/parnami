﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Collections;
using SIMS.TM.Model;

namespace Contruction
{
    public partial class AllTenders : UserControl
    {
        public TenderViewModel model
        {
            get { return (TenderViewModel)this.DataContext; }
        }

        public AllTenders()
        {
            InitializeComponent();
        }

        private void Image_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            var img = sender as Image;
            var data=(MTender)img.DataContext;
            if (data == null) return;
            TenderDetailView view = new TenderDetailView(data.ID);
            //view.Visibility = Visibility.Visible;
            Base.Redirect(view);
        }

    }
}
