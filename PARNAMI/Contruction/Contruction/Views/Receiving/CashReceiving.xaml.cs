﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using SIMS.MM.MODEL;

namespace Contruction
{
    public partial class CashReceiving : UserControl
    {
        public CashReceiving()
        {
            InitializeComponent();
        }

        public event EventHandler DeleteRequested;
        public void RaiseDeleteRequested()
        {
            if (DeleteRequested != null)
                DeleteRequested(this, new EventArgs());
        }

        public int DeleteLineID { get; set; }

        private void textBlock_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            var textblock = sender as TextBlock;
            var line = (MReceivingLine)textblock.DataContext;
            DeleteLineID = line.LineID;
            RaiseDeleteRequested();
        }

        #region CreatePopUP Members
        public bool IsPopUpMode
        {
            get { return MyPopUP != null; }
        }

        public PopUp MyPopUP { get; set; }

        public PopUp CreatePopup()
        {
            MyPopUP = this.ToPopup(false);
            MyPopUP.SetAutoLayout();
            MyPopUP.Title = "Cash Receiving";
            return MyPopUP;
        }
        #endregion

        private void btnPrint_Click(object sender, RoutedEventArgs e)
        {
            var btn = sender as Button;
            var data = (MReceiving)btn.DataContext;
            PrintFactory.PrintReceiving(data);
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            VendorCombo.Focus();
        }
    }
}
