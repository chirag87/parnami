﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace Contruction
{
    public partial class NewDirectPurchase : UserControl
    {
        public NewDirectPurchaseModel DirectModel
        {
            get { return (NewDirectPurchaseModel)this.DataContext; }
        }
				
        public NewDirectPurchase()
        {
            InitializeComponent();
        }

        private void CashReceiving_DeleteRequested(object sender, EventArgs e)
        {
            var line = sender as CashReceiving;
            var LineID = line.DeleteLineID;
            DirectModel.DeleteLine(LineID);
        }
    }
}
