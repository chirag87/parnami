﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using SIMS.MM.MODEL;

namespace Contruction
{
    public partial class NewCashPurchase : UserControl
    {
        public NewCashPurchaseModel cashModel
        {
            get { return (NewCashPurchaseModel)this.DataContext; }
        }
				
        public NewCashPurchase()
        {
            InitializeComponent();
        }

        private void CashReceiving_DeleteRequested(object sender, EventArgs e)
        {
            var line = sender as CashReceiving;
            var LineID = line.DeleteLineID;
            cashModel.DeleteLine(LineID);
        }
    }
}
