﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using SIMS.MM.MODEL;
using Contruction.Printing.Reporting;
using System.IO;

namespace Contruction
{
    public partial class GRNsDetailedView : UserControl
    {
        public GRNsDetailedView()
        {
            InitializeComponent();
        }

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
        }

        #region CreatePopUP Members
        public bool IsPopUpMode
        {
            get { return MyPopUP != null; }
        }

        public PopUp MyPopUP { get; set; }

        public PopUp CreatePopup()
        {
            MyPopUP = this.ToPopup(false);
            MyPopUP.SetAutoLayout();
            MyPopUP.Title = "GRN Receiving";
            return MyPopUP;
        }
        #endregion

        public ViewReceivingDetailsModel Model
        {
            get { return (ViewReceivingDetailsModel)this.DataContext; }
        }
				

        internal void SetID(int p)
        {
            Model.SetID(p);
        }

        private void textBlock_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {

        }

        private void btnPrint_Click(object sender, RoutedEventArgs e)
        {
            var btn = sender as Button;
            var data = (MReceiving)btn.DataContext;
            PrintFactory.PrintReceiving(data);
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            VendorCombo.Focus();
        }

        SaveFileDialog sfd;

        private void download_Click(object sender, RoutedEventArgs e)
        {
            sfd = new SaveFileDialog();
            sfd.Filter = "PDF file format|*.pdf";
            if ((bool)sfd.ShowDialog())
            {
                var hp = sender as HyperlinkButton;
                var data = (ViewReceivingDetailsModel)hp.DataContext;
                //HtmlPage.PopupWindow(new Uri("http://localhost:3385/GetReport.ashx?type=PO&id=" + data.NewGRN.TransactionID.ToString(), UriKind.Absolute), "_report", new HtmlPopupWindowOptions() { });

                WebClient webClient = new WebClient();
                webClient.OpenReadCompleted += (s, args) =>
                {
                    StreamReader sr = new StreamReader(args.Result);
                    byte[] bytes = new byte[args.Result.Length];
                    // revisit Here
                    args.Result.Read(bytes, 0, (int)args.Result.Length);
                    Stream st = sfd.OpenFile();
                    st.Write(bytes, 0, (int)args.Result.Length);
                    st.Close();
                };
                webClient.OpenReadAsync(new Uri("http://localhost:3385/GetReport.ashx?type=GRN&id=" + data.NewGRN.TransactionID.ToString(), UriKind.Absolute));
            }
        }
    }
}

