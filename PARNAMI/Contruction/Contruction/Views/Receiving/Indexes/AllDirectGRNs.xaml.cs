﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using SIMS.MM.MODEL;

namespace Contruction
{
    public partial class AllDirectGRNs : UserControl
    {
        public DirectGRNsVM Model
        {
            get { return (DirectGRNsVM)this.DataContext; }
        }
		
        public AllDirectGRNs()
        {
            InitializeComponent();
            Model.DirectGRNChanged += new EventHandler(Model_DirectGRNChanged);
        }

        void Model_DirectGRNChanged(object sender, EventArgs e)
        {
            myPager.PagedData = (IPagination)Model.DirectGRNs;
        }

        private void HyperlinkButton_Click(object sender, RoutedEventArgs e)
        {
            
        }

        private void myPager_MoveToPage(object sender, Controls.MyPageChangedEventArgs e)
        {
            Model.PageIndex = e.RequestedPage;
        }

        private void dataGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var SelectedRow = (MReceivingHeader)(sender as DataGrid).SelectedItem;
            if (SelectedRow == null) return;
        }

        private void Image_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            var img = sender as Image;
            var recheader = (MReceivingHeader)img.DataContext;
            if (recheader == null) return;
            Base.AppEvents.RaiseGRNRequested(recheader.ID);
            if (recheader.Type == "GRN")
            {
                var popup = new GRNsDetailedView();
                popup.SetID(recheader.ID);
                popup.CreatePopup();
            }
            if (recheader.Type == "Direct")
            {
                var popup = new DirectPurchaseDetailedView();
                popup.SetID(recheader.ID);
                popup.CreatePopup();
            }
        }

        private void SearchKey_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                Model.SearchKey = SearchKey.Text;
                Model.LoadDirectGRNs();
            }
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            SearchKey.Focus();
        }
    }
}
