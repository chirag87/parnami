﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using SIMS.MM.MODEL;

namespace Contruction
{
    public partial class ViewDirectPurchases : UserControl
    {
        public DirectPurchaseViewModel Model
        {
            get { return (DirectPurchaseViewModel)this.DataContext; }
        }
				
        public ViewDirectPurchases()
        {
            InitializeComponent();
            Model.DPChanged += new EventHandler(Model_DPChanged);
        }

        void Model_DPChanged(object sender, EventArgs e)
        {
            myPager.PagedData = (IPagination)Model.DirectPurchases;
        }

        private void Image_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            var img = sender as Image;
            var dirheader = (MReceivingHeader)img.DataContext;
            if (dirheader == null) return;
            var popup = new DirectPurchaseDetailedView();
            popup.SetID(dirheader.ID);
            popup.CreatePopup();
        }

        private void dataGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var SelectedRow = (MReceivingHeader)(sender as DataGrid).SelectedItem;
            if (SelectedRow == null) return;
            Base.AppEvents.RaiseDirectPurchaseRequested(SelectedRow.ID);
        }

        private void myPager_MoveToPage(object sender, Controls.MyPageChangedEventArgs e)
        {
            Model.PageIndex = e.RequestedPage;
        }

        private void SearchKey_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                Model.SearchKey = SearchKey.Text;
                Model.LoadDirectPurchases();
            }
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            SearchKey.Focus();
        }
    }
}
