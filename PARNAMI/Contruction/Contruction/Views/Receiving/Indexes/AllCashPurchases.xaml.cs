﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using SIMS.MM.MODEL;

namespace Contruction
{
    public partial class AllCashPurchases : UserControl
    {
        public CashPurchaseViewModel CPModel
        {
            get { return (CashPurchaseViewModel)this.DataContext; }
        }
				
        public AllCashPurchases()
        {
            InitializeComponent();
            CPModel.LoadCashPurchases();
            CPModel.CPChanged += new EventHandler(CPModel_CPChanged);
        }

        void CPModel_CPChanged(object sender, EventArgs e)
        {
            myPager.PagedData = (IPagination)CPModel.CashPurchases;
        }

        private void HyperlinkButton_Click(object sender, RoutedEventArgs e)
        {

        }

        private void Image_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            var img = sender as Image;
            var cpheader = (MReceivingHeader)img.DataContext;
            if (cpheader == null) return;
            Base.AppEvents.RaiseCashPurchaseRequested(cpheader.ID);
            var popup = new CashReceivingDetailedView();
            popup.setID(cpheader.ID);
            popup.CreatePopup();
            //CashReceivingDetailedView cdv = new CashReceivingDetailedView();
            //cdv.Show();
        }

        private void dataGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var SelectedRow = (MReceivingHeader)(sender as DataGrid).SelectedItem;
            if (SelectedRow == null) return;
            Base.AppEvents.RaiseCashPurchaseRequested(SelectedRow.ID);
        }

        private void myPager_MoveToPage(object sender, Controls.MyPageChangedEventArgs e)
        {
            CPModel.PageIndex = e.RequestedPage;
        }

        private void SearchKey_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                CPModel.SearchKey = SearchKey.Text;
                CPModel.LoadCashPurchases();
            }
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            SearchKey.Focus();
        }
    }
}
