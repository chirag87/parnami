﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using SIMS.MM.MODEL;

namespace Contruction
{
    public partial class AllConsumptions : UserControl
    {
        public ConsumptionViewModel Model
        {
            get { return (ConsumptionViewModel)this.DataContext; }
        }

        public AllConsumptions()
        {
            InitializeComponent();
            Model.ConsumptionChanged += new EventHandler(Model_ConsumptionChanged);
        }

        void Model_ConsumptionChanged(object sender, EventArgs e)
        {
            myPager.PagedData = (IPagination)Model.Consumptions;
        }

        private void Image_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            var img = sender as Image;
            var conheader = (MReceivingHeader)img.DataContext;
            if (conheader == null) return;
            var popup = new ConsumptionsDetailedView();
            popup.SetID(conheader.ID);
            popup.CreatePopup();
        }

        private void dataGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var SelectedRow = (MReceivingHeader)(sender as DataGrid).SelectedItem;
            if (SelectedRow == null) return;
            Base.AppEvents.RaiseConsumptionRequested(SelectedRow.ID);
        }

        private void searchKey_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                Model.SearchKey = searchKey.Text;
                Model.LoadConsumptions();
            }
        }

        private void myPager_MoveToPage(object sender, Controls.MyPageChangedEventArgs e)
        {
            Model.PageIndex = e.RequestedPage;
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            searchKey.Focus();
        }
    }
}
