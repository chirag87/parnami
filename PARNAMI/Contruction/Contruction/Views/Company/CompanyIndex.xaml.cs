﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace Contruction
{
    public partial class CompanyIndex : UserControl
    {
        public CompanyIndex()
        {
            InitializeComponent();
        }

        private void tbxsizeKey_TextChanged(object sender, TextChangedEventArgs e)
        {
            var box = sender as TextBox;
            Base.Current.SearchCompany = box.Text;
        }

        private void btnNewCompany_Click(object sender, RoutedEventArgs e)
        {
            AddNewCompany company = new AddNewCompany();
            company.Show();
        }

        private void btnExport_Click(object sender, RoutedEventArgs e)
        {
            dgdCompany.Export();
        }
    }
}
