﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using SIMS.MM.MODEL;
using System.Collections.ObjectModel;
using System.Windows.Printing;
using System.Windows.Navigation;
using System.Windows.Browser;
using System.IO;

namespace Contruction
{
    public partial class BillingDetail : UserControl
    {
        public NewBillingModel Model
        {
            get { return (NewBillingModel)this.DataContext; }
        }

        public BillingDetail()
        {
            InitializeComponent();
        }

        #region Events...

        public event EventHandler<TransactionRequestedArgs> Linedeleted;
        public void RaiseLinedeleted(int lineid, int transactionid)
        {
            if (Linedeleted != null)
                Linedeleted(this, new TransactionRequestedArgs(lineid, transactionid));
        }

        public class TransactionRequestedArgs : EventArgs
        {
            public int lineid { get; set; }
            public int transactionID { get; set; }
            public TransactionRequestedArgs(int _lineid, int _transactionID)
            {
                lineid = _lineid;
                transactionID = _transactionID;
            }
        }

        #endregion

        #region Properties...

        public int? vendorid
        {
            get { return Model.NewBill.VendorID; }
            set
            {
                Model.NewBill.VendorID = value;
                Model.NewBill.PANNo = Base.Current.Vendors.SingleOrDefault(x => x.ID == vendorid).PANNo;
                Model.NewBill.VendorIDChanged += new EventHandler(NewBill_VendorIDChanged);
            }
        }

        public int? siteid
        {
            get { return Model.NewBill.SiteID; }
            set
            {
                Model.NewBill.SiteID = value;
                Model.NewBill.SiteIDChanged += new EventHandler(NewBill_SiteIDChanged);
            }
        }

        #endregion

        #region Completed EVents...

        void NewBill_VendorIDChanged(object sender, EventArgs e)
        {
            Model.Notify("VendorID");
        }

        void NewBill_SiteIDChanged(object sender, EventArgs e)
        {
            Model.Notify("SiteID");
        }

        #endregion

        #region Methods

        public void SetID(int id)
        {
            Model.GetBillbyID(id);
        }

        #endregion

        #region DependencyPropertyEvents...

        private void btnVerify_Click(object sender, RoutedEventArgs e)
        {
            Model.VerifyBill();
        }

        private void btnApprove_Click(object sender, RoutedEventArgs e)
        {
            Model.ApproveBill();
            this.MyPopUP.Close();
        }

        private void btnCreate_Click(object sender, RoutedEventArgs e)
        {
            Model.SubmittoDB();
            if (Model.IsResetForm == true)
            {
                Base.Current.IsBusy = true;
                Model.RaiseResetForm();
            }
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            Model.Reset();
            Model.RaiseResetDatagrid();
        }

        private void dgd_Loaded(object sender, RoutedEventArgs e)
        {
            Model.Notify("NewBill");
        }

        private void textBlock_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            var textblock = sender as TextBlock;
            var line = (MBillingLines)textblock.DataContext;
            Model.DeleteLine(line.LineID);
            RaiseLinedeleted(line.LineID, line.TransactionID);
        }

        #endregion

        #region CreatePopUP Members
        public bool IsPopUpMode
        {
            get { return MyPopUP != null; }
        }

        public PopUp MyPopUP { get; set; }

        public PopUp CreatePopup()
        {
            MyPopUP = this.ToPopup(false);
            MyPopUP.SetAutoLayout();
            MyPopUP.Title = "Billing Details";
            return MyPopUP;
        }
        #endregion

        private void btn_print_Click(object sender, RoutedEventArgs e)
        {
            PrintFactory.PrintBill(Model.BillDetailforPrint);
        }

        SaveFileDialog sfd;

        private void download_Click(object sender, RoutedEventArgs e)
        {
            sfd = new SaveFileDialog();
            sfd.Filter = "PDF file format|*.pdf";
            if ((bool)sfd.ShowDialog())
            {
                var hp = sender as HyperlinkButton;
                var data = (NewBillingModel)hp.DataContext;
                //HtmlPage.PopupWindow(new Uri("http://localhost:3385/GetReport.ashx?type=VendorBill&id=" + data.NewBill.ID.ToString(), UriKind.Absolute), "_report", new HtmlPopupWindowOptions() { });

                WebClient webClient = new WebClient();
                webClient.OpenReadCompleted += (s, args) =>
                    {
                        StreamReader sr = new StreamReader(args.Result);
                        byte[] bytes = new byte[args.Result.Length];
                        // revisit Here
                        args.Result.Read(bytes,0,(int)args.Result.Length);
                        Stream st = sfd.OpenFile();
                        st.Write(bytes, 0, (int)args.Result.Length);
                        st.Close();
                    };
                webClient.OpenReadAsync(new Uri("http://localhost:3385/GetReport.ashx?type=VendorBill&id=" + data.NewBill.ID.ToString(), UriKind.Absolute));
            }
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            VendorCombo.Focus();
        }
    }
}