﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using SIMS.MM.MODEL;

namespace Contruction
{
    public partial class AllBillings : UserControl
    {
        public AllBillingsViewModel Model
        {
            get { return (AllBillingsViewModel)this.DataContext; }
        }
        
        public AllBillings()
        {
            InitializeComponent();
            Model.BillingDataChanged += new EventHandler(Model_BillingDataChanged);
        }

        void Model_BillingDataChanged(object sender, EventArgs e)
        {
            myPager.PagedData = (IPagination)Model.AllBills;
        }

        private void dataGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        private void textBox_TextChanged(object sender, TextChangedEventArgs e)
        {

        }

        private void myPager_MoveToPage(object sender, Controls.MyPageChangedEventArgs e)
        {
            Model.PageIndex = e.RequestedPage;
        }

        private void Image_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            var img = sender as Image;
            var data = (MBilling)img.DataContext;
            if (data == null) return;
            var popup = new BillingDetail();
            popup.SetID(data.ID);
            popup.CreatePopup();
        }

        private void btnExport_Click(object sender, RoutedEventArgs e)
        {
            dataGrid.Export();
        }

        private void SearchBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                Model.SearchKey = SearchBox.Text;
                Model.GetAllBills();
            }
        }

        private void Border_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            var bdr = sender as Border;
            var data = (MBilling)bdr.DataContext;
            if (data == null) return;
            if (data.IsPaid == true)
            {
                var popup = new BillingDetail();
                popup.SetID(data.ID);
                popup.CreatePopup();
            }
        }
    }
}