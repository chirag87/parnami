﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using SIMS.MM.MODEL;
using System.Collections.ObjectModel;
using System.Collections;
using SyedMehrozAlam.CustomControls;
using System.ComponentModel;

namespace Contruction
{
    public partial class PurchaseInvoice : UserControl, INotifyPropertyChanged
    {
        public BillingViewModel Model
        {
            get { return (BillingViewModel)this.DataContext; }
        }

        public PurchaseInvoice()
        {
            InitializeComponent();
            billingDetail.Linedeleted += new EventHandler<BillingDetail.TransactionRequestedArgs>(billingDetail_Linedeleted);
            billingDetail.Model.ResetForm += new EventHandler(Model_ResetForm);
            billingDetail.Model.ResetDatagrid += new EventHandler(Model_ResetDatagrid);
            Model.loadmydata += new EventHandler(Model_loadmydata);
        }

        void Model_loadmydata(object sender, EventArgs e)
        {
            if (Model.AllReceivingLines != null)
            {
                myPager.PagedData = (IPagination)Model.AllReceivingLines;
            }
        }

        void Model_ResetDatagrid(object sender, EventArgs e)
        {
            Model.ReceivingLinesList = new ObservableCollection<MReceivingLine>();
        }

        void Model_ResetForm(object sender, EventArgs e)
        {
            Model.Reset();
            Model.ReceivingLinesList = new ObservableCollection<MReceivingLine>();
        }

        void billingDetail_Linedeleted(object sender, BillingDetail.TransactionRequestedArgs e)
        {
            this.DeleteLine(e.lineid, e.transactionID);
        }

        ObservableCollection<MBillingLines> _FilteredData;
        public ObservableCollection<MBillingLines> FilteredData
        {
            get { return _FilteredData; }
            set
            {
                _FilteredData = value;
                Notify("FilteredData");
            }
        }

        private void myPager_MoveToPage(object sender, Controls.MyPageChangedEventArgs e)
        {
            Model.PageIndex = e.RequestedPage;
        }

        public void DeleteLine(int lineid, int transactionID)
        {
            var data = Model.ReceivingLinesList.Single(x => x.LineID == lineid && x.TransactionID == transactionID);
            if (data != null)
            {
                Model.ReceivingLinesList.Remove(data);
                Model.AllReceivingLines.OData.Add(data);
                Model.Notify("AllReceivingLines");
            }
            billingDetail.dgd.ItemsSource = ConverttoBillingLines(Model.ReceivingLinesList);
            //Model.Load();
        }

        public ObservableCollection<MBillingLines> ConverttoBillingLines(ObservableCollection<MReceivingLine> _mrecieving)
        {
            ObservableCollection<MBillingLines> billinglist = new ObservableCollection<MBillingLines>();
            int i=0;
            
            foreach (var _mrec in _mrecieving)
            {
                i++;
                MBillingLines bill = new MBillingLines()
                {
                    SrNo = i,
                    LineID = _mrec.LineID,
                    ItemID = _mrec.ItemID,
                    ActualQty = _mrec.Quantity,
                    SuggestedQty = _mrec.Quantity,
                    ActualPrice = _mrec.UnitPrice,
                    SuggestedPrice = _mrec.UnitPrice,
                    LinePrice = _mrec.LinePrice,
                    LineRemarks = _mrec.LineRemarks,
                    TransactionType = _mrec.TransactionTypeID,
                    TransactionID = _mrec.TransactionID,
                    TransactionDetailsID = _mrec.LineID,
                    LineChallanNo = _mrec.LineChallanNo,
                    ChallanNo = _mrec.ReceivingChallanNo,
                    ReferenceNo = _mrec.ReferenceNo,
                    Date = _mrec.CreationDate,
                    VendorID = _mrec.VendorID,
                    Item = _mrec.Item,
                    Vendor = _mrec.Vendor,
                };
                billinglist.Add(bill);
            }
            return billinglist;
        }

        private void btnLoad_Click(object sender, RoutedEventArgs e)
        {
            Model.StartCreateNew();
            /*Hiding Buttons*/
            billingDetail.btnApprove.Visibility = Visibility.Collapsed;
            billingDetail.btnSave.Visibility = Visibility.Collapsed;
            billingDetail.btnVerify.Visibility = Visibility.Collapsed;
            billingDetail.btn_print.Visibility = Visibility.Collapsed;
            billingDetail.download.Visibility = Visibility.Collapsed;
            /*Hiding Verified By and Approved By*/
            billingDetail.verifiedBy.Visibility = Visibility.Collapsed;
            billingDetail.showArrow.Visibility = Visibility.Collapsed;
            billingDetail.approvedBy.Visibility = Visibility.Collapsed;
            billingDetail.tbxApprovedBy.Visibility = Visibility.Collapsed;
            /*Hiding Payent Details*/
            billingDetail.Paymentdetails.Visibility = Visibility.Collapsed;
            billingDetail.Img1.Visibility = Visibility.Collapsed;
            billingDetail.Img2.Visibility = Visibility.Collapsed;
            billingDetail.Paymentdetails2.Visibility = Visibility.Collapsed;
        }

        private void btnResetHeaderControls_Click(object sender, RoutedEventArgs e)
        {
            Model.Reset();
            billingDetail.Model.Reset();
            Model.ReceivingLinesList = new ObservableCollection<MReceivingLine>();
        }

        private void VendorCombo_Copy_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var line = sender as AutoCompleteComboBox;
            if (line.SelectedItem != null)
            {
                var data = (MVendor)line.SelectedItem;
                billingDetail.vendorid = data.ID;
            }
        }

        private void SiteCombo_Copy_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var line = sender as AutoCompleteComboBox;
            if (line.SelectedItem != null)
            {
                var data = (MSite)line.SelectedItem;
                billingDetail.siteid = data.ID;
            }
        }

        private void btnAdd_Click(object sender, RoutedEventArgs e)
        {
            billingDetail.dgd.ItemsSource = ConverttoBillingLines(Model.ReceivingLinesList);
            billingDetail.Model.UpdateGrandTotal();
            billingDetail.Model.UpdateTotalAmount();
        }

        private void CheckBox_Checked(object sender, RoutedEventArgs e)
        {
            var chk = sender as CheckBox;
            if (chk.DataContext == null) return;
            var data = (MReceivingLine)chk.DataContext;
            var id = Model.ReceivingLinesList.SingleOrDefault(x => x.LineID == data.LineID && x.TransactionID == data.TransactionID && x.ItemID == data.ItemID);
            if (id == null)
            {
                Model.ReceivingLinesList.Add((MReceivingLine)data);
                Model.AllReceivingLines.OData.Remove(data);
                Model.Notify("AllReceivingLines");
            }
            else
            {
                MessageBox.Show("Record already there!");
            }
        }

        private void CheckBox_Unchecked(object sender, RoutedEventArgs e)
        {
            var chk = sender as CheckBox;
            if (chk.DataContext == null) return;
            var data = (MReceivingLine)chk.DataContext;
            if (Model.ReceivingLinesList.Contains(data))
                Model.ReceivingLinesList.Remove(data);
        }

        private void DownArrow_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            var chk = sender as Image;
            if (chk.DataContext == null) return;
            var data = (MReceivingLine)chk.DataContext;
            var id = Model.ReceivingLinesList.SingleOrDefault(x => x.LineID == data.LineID && x.TransactionID == data.TransactionID && x.Item == data.Item && x.ItemID == x.ItemID);
            if (id == null)
            {
                Model.ReceivingLinesList.Add((MReceivingLine)data);
                Model.AllReceivingLines.OData.Remove(data);
                Model.Notify("AllReceivingLines");
            }
            else
            {
                MessageBox.Show("Record already there!");
            }
        }

        private void ImageDelete_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            var chk = sender as Image;
            if (chk.DataContext == null) return;
            var data = (MReceivingLine)chk.DataContext;
            if (Model.ReceivingLinesList.Contains(data))
            {
                Model.ReceivingLinesList.Remove(data);
                Model.AllReceivingLines.OData.Add(data);
                Model.NotifyAllReceivingLines();
            }
        }

        private void selectAll_Click(object sender, RoutedEventArgs e)
        {
            foreach(var item in Model.AllReceivingLines.OData)
            {
                Model.ReceivingLinesList.Add(item);
            }
            Model.AllReceivingLines = new PaginatedData<MReceivingLine>();
        }

        private void checkAll_Checked(object sender, RoutedEventArgs e)
        {

        }

        private void checkAll_Unchecked(object sender, RoutedEventArgs e)
        {

        }

        private void hpbtnClear_Click(object sender, RoutedEventArgs e)
        {
            Model.Clear();
            rangeCombo.SelectedItem = null;
            dpStart.Text = "";
            dpEnd.Text = "";
        }

        private void hpbtnClearline2_Click(object sender, RoutedEventArgs e)
        {
            Model.challan = null;
            rangeCombo.SelectedItem = null;
            dpStart.Text = "";
            dpEnd.Text = "";
            Model.Load();
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void Notify(string propertyName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            VendorCombo_Copy.Focus();
        }
    }
}
