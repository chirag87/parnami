﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using SIMS.MM.MODEL;
using System.ComponentModel;

namespace Contruction
{
    public partial class SiteDetailedView : UserControl, INotifyPropertyChanged
    {
        public SiteDetailedView()
        {
            InitializeComponent();
        }

        public SiteDetailedView(int id)
            : this()
        {
            Base.Current.IsNew = true;
            Base.Current.GetSiteByID(id);
            Base.Current.GetDocumentsByForID(id, "Site");
            this.id = id;
            stock.siteid = id;
            stock.Model.Load();
        }

        public SiteDetailedView(int? id)
            : this()
        {
            Base.Current.IsNew = false;
            Base.Current.SelectedSite = new MSite();
        }

        MSite _SelectedSite;
        public MSite SelectedSite
        {
            get { return _SelectedSite; }
            set
            {
                _SelectedSite = value;
                Notify("SelectedSite");
            }
        }

        int _id = 0;
        public int id
        {
            get { return _id; }
            set
            {
                _id = value;
                Notify("id");
            }
        }

        private void btnEdit_Click(object sender, RoutedEventArgs e)
        {

        }

        private void btnSaveSiteInfo_Click(object sender, RoutedEventArgs e)
        {

        }

        private void btnAddNewDoc_Click(object sender, RoutedEventArgs e)
        {
            DocumentWindow doc = new DocumentWindow(id, "Site");
            doc.Show();
        }

        private void btnBack_Click(object sender, RoutedEventArgs e)
        {
            Base.Redirect(new SitesandClients());
        }

        private void delete_Click(object sender, RoutedEventArgs e)
        {
            var btn = sender as HyperlinkButton;
            var data = (MDocument)btn.DataContext;
            Base.Current.DeleteDocument(data);
            Base.Current.DocumentDeleted += new EventHandler(Current_DocumentDeleted);
        }

        void Current_DocumentDeleted(object sender, EventArgs e)
        {
            Base.Current.GetDocumentsByForID(id, "Site");
        }

        private void btnSaveInfo_Click(object sender, RoutedEventArgs e)
        {
            if (Base.Current.IsNew == true)
            {
                Base.Current.UpdateSite();
            }
            else
            {
                if (String.IsNullOrWhiteSpace(Base.Current.SelectedSite.Name))
                {
                    MessageBox.Show("Please Select a Valid Name !");
                }
                if (String.IsNullOrWhiteSpace(Base.Current.SelectedSite.SiteCode))
                {
                    MessageBox.Show("Please Select a Valid Site Code !");
                }
                if (String.IsNullOrWhiteSpace(Base.Current.SelectedSite.TypeID))
                {
                    MessageBox.Show("Please Select a Valid Type !");
                }
                if (Base.Current.SelectedSite.CompanyID == 0)
                {
                    MessageBox.Show("Invalid Company !");
                }
                if (Base.Current.SelectedSite.ClientID == 0)
                {
                    MessageBox.Show("Invalid Client !");
                }
                else
                {
                    Base.Current.AddNewSite(); 
                }
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void Notify(string propertyName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            tbxName.Focus();
        }
    }
}