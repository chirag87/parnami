﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using SIMS.MM.MODEL;

namespace Contruction.Views.Usermanagement
{
    public partial class SelectSites : UserControl
    {
        public event EventHandler Selected;
        public void RaiseSelected()
        {
            if (Selected != null)
                Selected(this, new EventArgs());
        }

        public event EventHandler Cancelled;
        public void RaiseCancelled()
        {
            if (Cancelled != null)
                Cancelled(this, new EventArgs());
        }

        public SelectSites()
        {
            InitializeComponent();
        }

        public IEnumerable<int> SelectedIDs
        {
            get
            {
                try
                {
                    return this.lstSites.SelectedItems
                        .Cast<MSite>().Select(x => x.ID);
                }
                catch
                {
                    return new List<int>();
                }
            }
        }

        public void SetSiteIDs(IEnumerable<int> list)
        {
            if (list == null) return;
            var Sites = Base.Current.Sites.Where(x => list.Contains(x.ID));
            lstSites.ItemsSource = Sites;
        }

        public void SetSiteIDsExcept(IEnumerable<int> list)
        {
            if (list == null) list = new List<int>();
            var Sites = Base.Current.Sites.Where(x => !list.Contains(x.ID));
            lstSites.ItemsSource = Sites;
        }

        private void btnOK_Click(object sender, RoutedEventArgs e)
        {
            RaiseSelected();
            if (IsPopUpMode)
                MyPopUP.Close();
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            RaiseCancelled();
            if(IsPopUpMode)
                MyPopUP.Cancel();
        }

        #region CreatePopUP Members
        public bool IsPopUpMode
        {
            get { return MyPopUP != null; }
        }

        public PopUp MyPopUP { get; set; }

        public PopUp CreatePopup()
        {
            MyPopUP = this.ToPopup();
            MyPopUP.SetAutoLayout();
            MyPopUP.ShowOkCancel = false;
            MyPopUP.Title = "Select Sites";
            return MyPopUP;
        }
        #endregion
    }
}
