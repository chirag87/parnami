﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Contruction.SrvLogin;

namespace Innosols
{
    public partial class CreateNewRole : ChildWindow
    {
        public LoginServiceClient service { get; set; }
        public CreateNewRole()
        {
            InitializeComponent();
        }

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            AddNewRole();
        }

        void AddNewRole()
        {
            var newrole = TxtNewRole.Text;
            if (String.IsNullOrWhiteSpace(newrole))
                return;
            // if (Base.Current.Roles.Select(x=>x.ToLower()).Contains(newrole.ToLower()))
            //    return;
            //var opr = Base.Current.CreateRole(newrole);
            //opr.Completed += (s, e) => { this.DialogResult = true; };
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

        private void ChildWindow_Loaded(object sender, RoutedEventArgs e)
        {
            TxtNewRole.Focus();
        }
    }
}

