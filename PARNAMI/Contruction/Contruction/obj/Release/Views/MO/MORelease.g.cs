﻿#pragma checksum "D:\OFFICE-WORK\INNOSOLS\WORK\SILVERLIGHT\DK\DKBuildcon2.1\Contruction\Contruction\Views\MO\MORelease.xaml" "{406ea660-64cf-4c82-b6f0-42d48172a799}" "4052E80B6E953F067D1782281175F971"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.261
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using SyedMehrozAlam.CustomControls;
using System;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Automation.Peers;
using System.Windows.Automation.Provider;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Interop;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Resources;
using System.Windows.Shapes;
using System.Windows.Threading;


namespace Contruction.Views.MO {
    
    
    public partial class MORelease : System.Windows.Controls.ChildWindow {
        
        internal System.Windows.Controls.Grid LayoutRoot;
        
        internal System.Windows.Controls.TextBlock Status;
        
        internal SyedMehrozAlam.CustomControls.AutoCompleteComboBox requestingSiteCombo;
        
        internal SyedMehrozAlam.CustomControls.AutoCompleteComboBox transferringSiteCombo;
        
        internal SyedMehrozAlam.CustomControls.AutoCompleteComboBox transporterCombo;
        
        internal System.Windows.Controls.DatePicker datePicker;
        
        internal System.Windows.Controls.DataGrid datagrid;
        
        internal System.Windows.Controls.Button btnRelease;
        
        internal System.Windows.Controls.Button btnCancel;
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Windows.Application.LoadComponent(this, new System.Uri("/Contruction;component/Views/MO/MORelease.xaml", System.UriKind.Relative));
            this.LayoutRoot = ((System.Windows.Controls.Grid)(this.FindName("LayoutRoot")));
            this.Status = ((System.Windows.Controls.TextBlock)(this.FindName("Status")));
            this.requestingSiteCombo = ((SyedMehrozAlam.CustomControls.AutoCompleteComboBox)(this.FindName("requestingSiteCombo")));
            this.transferringSiteCombo = ((SyedMehrozAlam.CustomControls.AutoCompleteComboBox)(this.FindName("transferringSiteCombo")));
            this.transporterCombo = ((SyedMehrozAlam.CustomControls.AutoCompleteComboBox)(this.FindName("transporterCombo")));
            this.datePicker = ((System.Windows.Controls.DatePicker)(this.FindName("datePicker")));
            this.datagrid = ((System.Windows.Controls.DataGrid)(this.FindName("datagrid")));
            this.btnRelease = ((System.Windows.Controls.Button)(this.FindName("btnRelease")));
            this.btnCancel = ((System.Windows.Controls.Button)(this.FindName("btnCancel")));
        }
    }
}

