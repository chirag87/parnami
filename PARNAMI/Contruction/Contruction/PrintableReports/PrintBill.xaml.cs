﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Windows.Printing;
using SIMS.MM.MODEL;

namespace Contruction
{
    public partial class PrintBill : UserControl
    {
        public BillPrintReportModel Model
        {
            get { return (BillPrintReportModel)this.DataContext; }
        }
				
        public PrintBill()
        {
            InitializeComponent();
        }

        public void Print()
        {
            if (Model.SelectedBill == null) return;
            PrintDocument doc = new PrintDocument();
            doc.PrintPage += new EventHandler<PrintPageEventArgs>(doc_PrintPage);
            doc.Print("BILL " + Model.SelectedBill.ID);
        }

        void doc_PrintPage(object sender, PrintPageEventArgs e)
        {
            VisualStateManager.GoToState(this as Control, "Print", true);
            this.Width = e.PrintableArea.Width;
            this.Height = e.PrintableArea.Height;
            e.PageVisual = this;
        }

        public void PrintingBill(MBilling bill)
        {
            Model.SelectedBill = bill;
            if (bill != null)
                Print();
        }
    }
}
