﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using SIMS.MM.MODEL;

namespace Contruction.Controls
{
    public partial class MyPager : UserControl
    {
        public MyPager()
        {
            InitializeComponent();
        }

        public int RequestedPage { get; set; }

        public event EventHandler<MyPageChangedEventArgs> MoveToPage;
        public void RaiseMoveToPage(int page)
        {
            if (MoveToPage != null)
                MoveToPage(this, new MyPageChangedEventArgs() { RequestedPage = page});
        }

        public MyPagerViewModel Model
        {
            get { return (MyPagerViewModel)this.DataContext; }
        }


        public IPagination PagedData
        {
            get {
                return Model.PagedData;
            }
            set
            {
                Model.PagedData = value;
            }
        }

        //public void SyncPagedData() { Model.PagedData = PagedData; }

        // Using a DependencyProperty as the backing store for PagedData.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty PagedDataProperty =
            DependencyProperty.Register("PagedData", typeof(IPagination), typeof(MyPager), new PropertyMetadata(null));

        

        private void btn_Next_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (!Model.CanNext) return;
            RequestedPage =  Model.CurrentPage + 1;
            RaiseMoveToPage(RequestedPage);
        }

        private void btn_last_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (!Model.CanNext) return;
            RequestedPage = Model.PagedData.TotalPages-1;
            RaiseMoveToPage(RequestedPage);
        }

        private void btn_Previous_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (!Model.CanPrevious) return;
            RequestedPage = Model.CurrentPage - 1;
            RaiseMoveToPage(RequestedPage);
        }

        private void btn_First_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (!Model.CanPrevious) return;
            RequestedPage = 0;
            RaiseMoveToPage(RequestedPage);
        }

    }

    public class MyPageChangedEventArgs : EventArgs
    {
        public int RequestedPage { get; set; }
    }
}
