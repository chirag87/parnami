﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.ComponentModel;
using SIMS.MM.MODEL;

namespace Contruction
{
    public partial class CFTCalculator : UserControl, INotifyPropertyChanged
    {
        public event EventHandler ValueChanged;
        public void RaiseValueChanged()
        {
            OnValueChanged();
            if (ValueChanged != null)
                ValueChanged(this, new EventArgs());
        }

        void CFTCalculator_Loaded(object sender, RoutedEventArgs e)
        {
            var line = (MReceivingLine)this.DataContext;
            if (line == null) return;
            this.Value = line.Quantity;
        }

        public CFTCalculator()
        {
            InitializeComponent();
            this.Loaded += new RoutedEventHandler(CFTCalculator_Loaded);
        }

        void OnValueChanged()
        {
            var line = (MReceivingLine)this.DataContext;
            if (line == null) return;
            if (this.SetThroughMeasurement)
            {
                line.Quantity = this.Value;
                line.TruckParameters = this.Measurements;
            }
            else
            {
                line.Quantity = this.Value;
                line.TruckParameters = null;
            }
        }

        bool _SetThroughMeasurement = false;
        public bool SetThroughMeasurement
        {
            get { return _SetThroughMeasurement; }
            set
            {
                _SetThroughMeasurement = value;
                Notify("SetThroughMeasurement");
            }
        }

        double _Value;
        public double Value
        {
            get { return _Value; }
            set
            {
                _Value = value;
                text.Text = value.ToString();
                Notify("Value"); }
        }


        private void Image_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            CFTCalculatorPopup cft = new CFTCalculatorPopup();
            cft.Show();
            cft.Submitted += new EventHandler(cft_Submitted);
        }

        void cft_Submitted(object sender, EventArgs e)
        {
            var data = sender as CFTCalculatorPopup;
            Measurements.LFeet = double.Parse(data.FtextL.Text);
            Measurements.LInches = double.Parse(data.ItextL.Text);
            Measurements.BFeet = double.Parse(data.FtextB.Text);
            Measurements.BInches = double.Parse(data.ItextB.Text);
            Measurements.HFeet = double.Parse(data.FtextH.Text);
            Measurements.HInches = double.Parse(data.ItextH.Text);

            Value = Measurements.Volume;
            text.Text = Value.ToString();
            SetThroughMeasurement = true;
            RaiseValueChanged();
        }

        MTruckParameter _Measurements = new MTruckParameter();
        public MTruckParameter Measurements
        {
            get { return _Measurements; }
            set
            {
                _Measurements = value;
                Notify("Measurements");
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void Notify(string propertyName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }

        private void text_KeyUp(object sender, KeyEventArgs e)
        {
            
        }

        private void text_LostFocus(object sender, RoutedEventArgs e)
        {
            double val = this.Value;
            if (!Double.TryParse(text.Text, out val))
            {
                text.Text = "0";
                Value = 0;
            }
            else
            {
                Value = val;
            }
            RaiseValueChanged();
            SetThroughMeasurement = false;
        }

        private void text_GotFocus(object sender, RoutedEventArgs e)
        {
            if (text.Text == "0")
                text.Text = "";
        }
    }
}
