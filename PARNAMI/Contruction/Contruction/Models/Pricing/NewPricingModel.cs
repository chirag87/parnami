﻿using System;
using System.Net;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Contruction.MasterDBService;
using System.ComponentModel;
using SIMS.MM.MODEL;
using System.Collections.ObjectModel;
using System.Collections.Generic;

namespace Contruction
{
    public class NewPricingModel : ViewModel
    {
        MasterDataServiceClient service = new MasterDataServiceClient();

        public NewPricingModel()
        {
            if (!DesignerProperties.GetIsInDesignMode(Application.Current.RootVisual))
            {
                //Reset();
                OnInit();
            }
        }

        public void OnInit()
        {
            service.UpdatePriceCompleted += new EventHandler<AsyncCompletedEventArgs>(service_UpdatePriceCompleted);
            service.GetAllPricesByISVCompleted += new EventHandler<GetAllPricesByISVCompletedEventArgs>(service_GetAllPricesByISVCompleted);
        }

        void service_GetAllPricesByISVCompleted(object sender, GetAllPricesByISVCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                Base.ShowError(e.Error);
                IsBusy = false;
            }
            else
            {
                OldPriceList = e.Result;
                IsBusy = false;
            }
        }

        void service_UpdatePriceCompleted(object sender, AsyncCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                Base.ShowError(e.Error);
                IsBusy = false;
            }
            else
            {
                MessageBox.Show("Updation Successful !");
                IsBusy = false;
                Base.AppEvents.RaiseNewPricingAdded();
            }
        }

        #region Model

        ObservableCollection<MPriceMaster> _PriceLine = new ObservableCollection<MPriceMaster>();
        public ObservableCollection<MPriceMaster> PriceLine
        {
            get { return _PriceLine; }
            set
            {
                _PriceLine = value;
                Notify("PriceLine");
                Notify("CanEditHeader");
                Notify("CanEditDetails");
            }
        }

        ObservableCollection<MPriceMaster> _OldPriceList = new ObservableCollection<MPriceMaster>();
        public ObservableCollection<MPriceMaster> OldPriceList
        {
            get { return _OldPriceList; }
            set
            {
                _OldPriceList = value;
                Notify("OldPriceList");
                Notify("CanEditHeader");
                Notify("CanEditDetails");
            }
        }

        int _SelectedVendorID = new int();
        public int SelectedVendorID
        {
            get { return _SelectedVendorID; }
            set
            {
                _SelectedVendorID = value;
                NotifyCans();
                Notify("SelectedVendorID");
            }
        }

        int? _SelectedSiteID;
        public int? SelectedSiteID
        {
            get { return _SelectedSiteID; }
            set
            {
                _SelectedSiteID = value;
                NotifyCans();
                Notify("SelectedSiteID");
            }
        }

        int _SelectedItemID = new int();
        public int SelectedItemID
        {
            get { return _SelectedItemID; }
            set
            {
                _SelectedItemID = value;
                NotifyCans();
                Notify("SelectedItemID");
                MItem item = new MItem();
                if (SelectedItemID != 0)
                {
                    item = Base.Current.Items.Single(x => x.ID == SelectedItemID);
                    MU = item.MeasurementUnitID;
                }
                TryGettingData();
            }
        }

        string _MU;
        public string MU
        {
            get { return _MU; }
            set
            {
                _MU = value;
                Notify("MU");
            }
        }

        #endregion

        public void Reset()
        {
            SelectedItemID = 0;
            SelectedSiteID = null;
            SelectedVendorID = 0;
            NotifyCans();
        }

        public void UpdatePrice()
        {
            foreach (var line in PriceLine)
            {
                line.UM = MU;
                service.UpdatePriceAsync(line);
                IsBusy = true;
            }
        }

        public void TryGettingData()
        {
            if (SelectedVendorID != 0 && SelectedItemID != 0)
            {
                service.GetAllPricesByISVAsync(SelectedVendorID, SelectedSiteID, SelectedItemID);
                IsBusy = true;
            }
        }

        public void NotifyCans()
        {
            Notify("CanEditHeader");
            Notify("CanEditDetails");
        }

        public bool CanEditHeader
        {
            get { return (SelectedVendorID == default(int)) || SelectedItemID == default(int); }
        }

        public bool CanEditDetails
        {
            get { return !CanEditHeader; }
        }

        public MPriceMaster AddNewLine()
        {
            if (this.PriceLine == null)
                this.PriceLine = new ObservableCollection<MPriceMaster>();
            if (SelectedVendorID != 0 && SelectedItemID != 0)
            {
                var line = new MPriceMaster();

                line.VendorID = SelectedVendorID;
                line.ItemID = SelectedItemID;
                line.SiteID = SelectedSiteID;
                this.PriceLine.Add(line);

                this.OldPriceList.Add(line);
                this.UpdateLineNumbers();
                line.EffectiveTill = null;
                line.CreatedBy = Base.Current.UserName;
                line.CreatedOn = DateTime.UtcNow.AddHours(5.5);
                line.UpdatedBy = Base.Current.UserName;
                line.UpdatedOn = DateTime.UtcNow.AddHours(5.5);

                Notify("SelectedPrice");
                return line;
            }
            else
                return null;
        }

        public int UpdateLineNumbers()
        {
            if (this.PriceLine == null) return 0;
            int i = 1;
            foreach (var item in this.PriceLine)
            {
                item.LineID = i;
                i++;
            }
            return i - 1;
        }
    }
}