﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Contruction.SrvTenders;
using System.ComponentModel;
using SIMS.TM.Model;
namespace Contruction
{
    public class NewTenderModel:ViewModel
    {
        TenderServiceClient proxy = new TenderServiceClient();
        public NewTenderModel()
        {
            if (!DesignerProperties.GetIsInDesignMode(Application.Current.RootVisual))
            {
                //Reset();
                OnInit();
            }
        }

        private void OnInit()
        {
            proxy.CreateNewTenderCompleted += new EventHandler<CreateNewTenderCompletedEventArgs>(proxy_CreateNewTenderCompleted);
        }

        void proxy_CreateNewTenderCompleted(object sender, CreateNewTenderCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                Base.ShowError(e.Error);
                IsBusy = false;
            }
            else
            {
                CreatedTender = e.Result;
                IsBusy = false;
                MessageBox.Show("ID: " + CreatedTender.ID + "New Tender Added Successfully !");
                //Reset();
                //Base.AppEvents.RaiseNewPRRaised();
            }
        }


        MTender _NewTender = new MTender();
        public MTender NewTender
        {
            get { return _NewTender; }
            set
            {
                _NewTender = value;
                Notify("NewTender");
            }
        }

        MTender _CreatedTender;
        public MTender CreatedTender
        {
            get { return _CreatedTender; }
            set
            {
                _CreatedTender = value;
                Notify("CreatedTender");
            }
        }

        public void SubmitToDB()
        {
            if (!Validate()) return;
            proxy.CreateNewTenderAsync(NewTender);
            IsBusy = true;
        }

        private bool Validate()
        {
            Status = "";
            if (String.IsNullOrWhiteSpace(NewTender.Title)) { Status = "Title Cannot be Empty!"; return false; }
            //if (NewPR.VendorID == 0) { Status = "Please Select Vendor"; return false; }
            //if (NewPR.SiteID == 0) { Status = "Please Select Site"; return false; }
            //if (String.IsNullOrWhiteSpace(NewPR.Purchaser)) { Status = "Purchaser Cannot be Empty!"; return false; }
            //if (String.IsNullOrWhiteSpace(NewPR.PurchaseType)) { Status = "Please Select Purchase Type"; return false; }
            //if (NewPR.PRLines == null) { Status = "Purchase Request should have alteast one item."; return false; }
            //if (NewPR.PRLines.Count == 0) { Status = "Purchase Request should have alteast one item."; return false; }
            //if (NewPR.PRLines.Any(x => x.ItemID == 0)) { Status = "Purchase Lines has some invaid/unselected Items!"; return false; }
            return true;
        }

        string _Status;
        public string Status 
        {
            get { return _Status; }
            set
            {
                _Status = value;
                Notify("Status");
            }
        }
    }
}
