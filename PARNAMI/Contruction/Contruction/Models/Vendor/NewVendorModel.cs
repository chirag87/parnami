﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Collections.ObjectModel;
using Contruction.PurchasingService;
using SIMS.MM.MODEL;
using System.Collections.Generic;
using Contruction.MasterDBService;
using System.ComponentModel;

namespace Contruction
{
    public class NewVendorModel : ViewModel
    {
        MasterDataServiceClient service = new MasterDataServiceClient();

        public NewVendorModel()
        {
            if (!DesignerProperties.GetIsInDesignMode(Application.Current.RootVisual))
            {
                Reset();
                OnInit();
            }
        }

        //MVendor _NewVendor;
        //public MVendor NewVendor
        //{
        //    get { return _NewVendor; }
        //    set
        //    {
        //        _NewVendor = value;
        //        Notify("NewVendor");
        //    }
        //}

        public void OnInit()
        {
            service.CreateNewVendorCompleted += new EventHandler<System.ComponentModel.AsyncCompletedEventArgs>                         (service_CreateNewVendorCompleted);
            Base.Current.NewVendorAdded += new EventHandler(Current_NewVendorAdded);
        }

        void Current_NewVendorAdded(object sender, EventArgs e)
        {
            if (Base.Current.SelectedVendor.IsBlocked == false)
                service.CreateNewVendorAsync(Base.Current.SelectedVendor);
            IsBusy = true;
            Base.Current.SelectedVendor.IsBlocked = true;
        }

        void service_CreateNewVendorCompleted(object sender, System.ComponentModel.AsyncCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                Base.ShowError(e.Error);
                IsBusy = false;
                Base.Current.SelectedVendor.IsBlocked = false;
            }
            else
            {
                Base.Current.SelectedVendor.IsBlocked = true;
                IsBusy = false;
                MessageBox.Show("New Vendor Added Successfully !");
                Base.AppEvents.RaiseNewVendorAdded();
            }
        }

        public void SubmitToDB()
        {
            //MessageBox.Show("Before service Call, Details: "+"Data:"+DateTime.UtcNow.AddHours(5.5)+" Is Blocked Status:"+NewVendor.IsBlocked+"");
            //if(NewVendor.IsBlocked==false)
            //    service.CreateNewVendorAsync(NewVendor);
            //NewVendor.IsBlocked = true;
            //MessageBox.Show("service Call Details: "+"Data:"+DateTime.UtcNow.AddHours(5.5)+" Is Blocked Status:"+NewVendor.IsBlocked+"");
            //IsBusy = true;
        }

        public void Reset()
        {
            //NewVendor = new MVendor();
        }
    }
}
