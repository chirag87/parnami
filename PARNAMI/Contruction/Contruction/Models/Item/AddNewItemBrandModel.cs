﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Contruction.MasterDBService;
using System.ComponentModel;
using SIMS.MM.MODEL;

namespace Contruction
{
    public class AddNewItemBrandModel : ViewModel
    {
        MasterDataServiceClient service = new MasterDataServiceClient();

        public AddNewItemBrandModel()
        {
            if (!DesignerProperties.GetIsInDesignMode(Application.Current.RootVisual))
            {
                OnInit();
            }
        }

        public void OnInit()
        {
            service.CreateNewItemBrandCompleted += new EventHandler<CreateNewItemBrandCompletedEventArgs>(service_CreateNewItemBrandCompleted);
        }

        void service_CreateNewItemBrandCompleted(object sender, CreateNewItemBrandCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                Base.ShowError(e.Error);
                IsBusy = false;
            }
            else
            {
                NewBrand = e.Result;
                MessageBox.Show("New Brand Added Successfully !");
                Base.Current.LoadItemBrands();
                IsBusy = false;
            }
        }

        MItemBrand _NewBrand = new MItemBrand();
        public MItemBrand NewBrand
        {
            get { return _NewBrand; }
            set
            {
                _NewBrand = value;
                Notify("NewBrand");
            }
        }

        public void SubmitToDB()
        {
            service.CreateNewItemBrandAsync(NewBrand);
            IsBusy = true;
        }
    }
}