﻿using System;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Collections.Generic;
using SIMS.MM.MODEL;
using System.ComponentModel;
using Contruction.MasterDBService;
using System.Collections.ObjectModel;

namespace Contruction
{
    public class ShowItemParentsVM:ViewModel
    {
        public event EventHandler DataLoaded;
        public void RaiseDataLoaded()
        {
            if (DataLoaded != null)
                DataLoaded(this, new EventArgs());
        }

        MasterDataServiceClient service = new MasterDataServiceClient();
        
        public ShowItemParentsVM()
        {
            if (!DesignerProperties.GetIsInDesignMode(Application.Current.RootVisual))
            {
                OnInit();
            }
        }

        public void OnInit()
        {
            service.GetItemParentsCompleted += new EventHandler<GetItemParentsCompletedEventArgs>(service_GetItemParentsCompleted);
        }

        void service_GetItemParentsCompleted(object sender, GetItemParentsCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                Base.ShowError(e.Error);
                IsBusy = false;
            }
            else
            {
                Parents = e.Result;
                IsBusy = false;
            }
        }

        int _itemid = 0;
        public int itemid
        {
            get { return _itemid; }
            set
            {
                _itemid = value;
                Load();
                Notify("itemid");
            }
        }

        public void Load()
        {
            service.GetItemParentsAsync(itemid);
            IsBusy = true;
        }

        IEnumerable<MItemCategory> _Parents = new List<MItemCategory>();
        public IEnumerable<MItemCategory> Parents
        {
            get { return _Parents; }
            set
            {
                _Parents = value;
                MakeToOrder(null);
                Notify("Parents");
                Notify("OrderedParents");
            }
        }

        ObservableCollection<ItemParentModel> _OrderedParents = new ObservableCollection<ItemParentModel>();
        public ObservableCollection<ItemParentModel> OrderedParents
		{
			get
            {
                return _OrderedParents;
            }
            set
            {
                _OrderedParents = value;
			    Notify("OrderedParents");
			}
		}

        List<ItemParentModel> tmp;
        int _Level = 0;
        void MakeToOrder(int? parentID)
        {
            if(!parentID.HasValue)
            {
                _Level = 0;
                OrderedParents = null;
                tmp = new List<ItemParentModel>();
            }
            else
            {
                _Level++;
            }

            if(Parents.Count(x => x.ParentID == parentID) == 1)
            {
                var current = Parents.Single(x => x.ParentID == parentID);
                tmp.Add(new ItemParentModel(_Level,current.ID,current.ParentID,current.Name));
                MakeToOrder(current.ID);
            }
            else
            {

                var tmp2 = new ObservableCollection<ItemParentModel>();
                tmp.ForEach(x => tmp2.Add(x));
                OrderedParents = null;
                OrderedParents = tmp2;
                RaiseDataLoaded();
            }
        }
    }

    public class ItemParentModel:ViewModel
    {
        int _ID;
        public int ID
        {
            get { return _ID; }
            set
            {
                _ID = value;
                Notify("ID");
            }
        }

        int? _ParentID;
        public int? ParentID
        {
            get { return _ParentID; }
            set
            {
                _ParentID = value;
                Notify("ParentID");
            }
        }

        string _Name;
        public string Name
        {
            get { return _Name; }
            set
            {
                _Name = value;
                Notify("Name");
            }
        }

        int _Level;
        public int Level
        {
            get { return _Level; }
            set
            {
                _Level = value;
                Notify("Level");
            }
        }
        //public string URL { get; set; }

        public string StartingSpace
        {
            get
            {
                string str = "";
                for (int i = 0; i < Level; i++)
                {
                    str += "  ";
                }
                return str;
            }
        }
        public ItemParentModel(int _Level, int _ID, int? _ParentID, string _Name)
        {
            ID = _ID;
            ParentID = _ParentID;
            Level = _Level;
            Name = _Name;
        }
    }
}
