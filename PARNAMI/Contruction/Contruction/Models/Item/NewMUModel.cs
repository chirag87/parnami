﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.ComponentModel;
using System.Collections.ObjectModel;
using Contruction.PurchasingService;
using SIMS.MM.MODEL;
using System.Collections.Generic;
using Contruction.MasterDBService;

namespace Contruction
{
    public class NewMUModel : ViewModel
    {
        MasterDataServiceClient service = new MasterDataServiceClient();

        public NewMUModel()
        {
            if (!DesignerProperties.GetIsInDesignMode(Application.Current.RootVisual))
            {
                Reset();
                OnInit();
            }
        }

        private void OnInit()
        {
            service.CreateMUCompleted+=new EventHandler<AsyncCompletedEventArgs>(service_CreateMUCompleted);
        }

        void service_CreateMUCompleted(object sender, AsyncCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                Base.ShowError(e.Error);
                IsBusy = false;
            }
            else
            {
                IsBusy = false;
                MessageBox.Show("New Measurement Unit Added Successfully !");
                Base.Current.LoadMU();
                Base.AppEvents.RaiseNewMURaised();
            }
        }

        MMeasurementUnit _NewMU = new MMeasurementUnit();
        public MMeasurementUnit NewMU
        {
            get { return _NewMU; }
            set
            {
                _NewMU = value;
                Notify("NewMU");
            }
        }

        public void SubmitToDB()
        {
            service.CreateMUAsync(NewMU);
            IsBusy = true;
        }

        public void Reset()
        {
            NewMU = new MMeasurementUnit();
        }
    }
}