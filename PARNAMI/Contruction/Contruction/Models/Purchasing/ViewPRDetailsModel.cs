﻿using System;
using System.Net;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Contruction.PurchasingService;
using System.Collections.ObjectModel;
using SIMS.MM.MODEL;
using System.ComponentModel;

namespace Contruction
{
    public class ViewPRDetailsModel : ViewModel
    {
        PurchasingServiceClient service = new PurchasingServiceClient();

        public event EventHandler POConverted;
        public void RaisePOConverted()
        {
            if (POConverted != null)
                POConverted(this, new EventArgs());
        }

        public ViewPRDetailsModel()
        {
            if (!DesignerProperties.GetIsInDesignMode(Application.Current.RootVisual))
            {
                OnInit();
            }
        }

        public void OnInit()
        {
            service.GetPRbyIDCompleted += new EventHandler<GetPRbyIDCompletedEventArgs>(service_GetPRbyIDCompleted);
            service.ApprovePRCompleted += new EventHandler<AsyncCompletedEventArgs>(service_ApprovePRCompleted);
            service.ConvertToPOCompleted += new EventHandler<ConvertToPOCompletedEventArgs>(service_ConvertToPOCompleted);
            service.AcknowledgePOCompleted += new EventHandler<AsyncCompletedEventArgs>(service_AcknowledgePOCompleted);
            service.UpdatePRCompleted += new EventHandler<UpdatePRCompletedEventArgs>(service_UpdatePRCompleted);
            service.SaveandApproveCompleted += new EventHandler<SaveandApproveCompletedEventArgs>(service_SaveandApproveCompleted);
            service.EmailPRCompleted += new EventHandler<AsyncCompletedEventArgs>(service_EmailPRCompleted);
        }

        void service_ConvertToPOCompleted(object sender, ConvertToPOCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                Base.ShowError(e.Error);
                IsBusy = false;
            }
            else
            {
                IsBusy = false;
                MessageBox.Show("Successfully Converted to Purchase Order !");
                Base.AppEvents.RaisePOConverted(e.Result);
            }
            IsLoading = false;
        }

        void service_EmailPRCompleted(object sender, AsyncCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                Base.ShowError(e.Error);
                IsBusy = false;
            }
            else
            {
                IsBusy = false;
                MessageBox.Show("Email Sent Successfully!");
            }
        }

        void service_SaveandApproveCompleted(object sender, SaveandApproveCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                Base.ShowError(e.Error);
                IsBusy = false;
            }
            else
            {
                if (!Validate()) return;
                ConvertedPR = e.Result;
                Base.AppEvents.RaisePRDataChanged();
                Status = "Approved!";
                IsBusy = false;
            }
            IsLoading = false;
        }

        void service_UpdatePRCompleted(object sender, UpdatePRCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                Base.ShowError(e.Error);
                IsBusy = false;
            }
            else
            {
                if (!Validate()) return;
                ConvertedPR = e.Result;
                Base.AppEvents.RaisePRDataChanged();
                Status = "Saved!";
                IsBusy = false;
            }
            IsLoading = false;
        }

        void service_AcknowledgePOCompleted(object sender, AsyncCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                Base.ShowError(e.Error);
                IsBusy = false;
            }
            else
            {
                IsBusy = false;
                MessageBox.Show("Acknowledged !");
            }
            IsLoading = false;
        }

        void service_ApprovePRCompleted(object sender, AsyncCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                Base.ShowError(e.Error);
                IsBusy = false;
            }
            else
            {
                if (!Validate()) return;
                MessageBox.Show("Purchase Requisition Approved successfully !");
                Status = "Approved!";
                IsBusy = false;
            }
            IsLoading = false;
        }

        void service_GetPRbyIDCompleted(object sender, GetPRbyIDCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                Base.ShowError(e.Error);
                IsBusy = false;
            }
            else
            {
                ConvertedPR = e.Result;
                IsBusy = false;
            }
            IsLoading = false;
        }

        public void GetPR(int prid)
        {
            service.GetPRbyIDAsync(prid);
            IsBusy = true;
            IsLoading = true;
        }

        public void ApprovePR()
        {
            Status = "Approving ...";
            if (!Validate()) return;
            service.SaveandApproveAsync(ConvertedPR, Base.Current.UserName);
            IsBusy = true;
            IsLoading = true;
        }

        public void convertToPO()
        {
            service.ConvertToPOAsync(ConvertedPR);
            IsBusy = true;
            IsLoading = true;
        }

        public void Acknowledge()
        {
            //service.AcknowledgePOAsync(
        }

        public void Email()
        {
            //if(ConvertedPR.IsApproved)
            //{
                service.EmailPRAsync(ConvertedPR.PRNumber);
                IsBusy = true;
            //}
            //else
            //{
            //    MessageBox.Show("PR is not Approved! Operation not Allowed!");
            //}
        }

        public void UpdatePR()
        {
            Status = "Updating ...";
            if(!Validate()) return;
            service.UpdatePRAsync(ConvertedPR);
            IsBusy = true;
            IsLoading = true;
        }

        public void UpdatePRafterSplit(MPR convertedPR)
        {
            Status = "Updating ...";
            service.UpdatePRAsync(convertedPR);
            IsBusy = true;
            IsLoading = true;
        }

        MPR _ConvertedPR = new MPR();
        public MPR ConvertedPR
        {
            get { return _ConvertedPR; }
            set
            {
                _ConvertedPR = value;
                Notify("ConvertedPR");
                NotifyAllCans();
            }
        }

        bool _IsPR = false;
        public bool IsPR
        {
            get { return _IsPR; }
            set
            {
                _IsPR = value;
                Notify("IsPR");
            }
        }

        bool _IsPO = false;
        public bool IsPO
        {
            get { return _IsPO; }
            set
            {
                _IsPO = value;
                Notify("IsPO");
            }
        }

        string _Status;
        public string Status
        {
            get { return _Status; }
            set
            {
                _Status = value;
                Notify("Status");
            }
        }

        public bool Validate()
        {
            if (ConvertedPR.VendorID == 0) { Status = "Please Select Vendor"; return false; }
            if (ConvertedPR.SiteID == 0) { Status = "Please Select Site"; return false; }
            if (String.IsNullOrWhiteSpace(ConvertedPR.Purchaser)) { Status = "Purchaser Cannot be Empty!"; return false; }
            if (String.IsNullOrWhiteSpace(ConvertedPR.PurchaseType)) { Status = "Please Select Purchase Type"; return false; }
            if (ConvertedPR.PRLines == null) { Status = "Purchase Request should have alteast one item."; return false; }
            if (ConvertedPR.PRLines.Count == 0) { Status = "Purchase Request should have alteast one item."; return false; }
            if (ConvertedPR.PRLines.Any(x => x.ItemID == 0)) { Status = "Purchase Lines has some invaid/unselected Items!"; return false; }
            return true;
        }

        public override bool CanEdit
        {
            get { return !ConvertedPR.IsApproved && Base.Current.Rights.CanApprovePR; }
        }

        public bool CanApprove
        {
            get { return !ConvertedPR.IsApproved && Base.Current.Rights.CanApprovePR; }
        }

        public bool CanSave
        {
            get { return !ConvertedPR.IsApproved && Base.Current.Rights.CanRaisePR; }
        }

        public bool CanAcknowledge
        {
            get { return ConvertedPR.ConvertedToPO && Base.Current.Rights.CanRaisePO; }
        }

        public bool CanConvertToPO
        {
            get { return ConvertedPR.IsApproved && !ConvertedPR.ConvertedToPO && Base.Current.Rights.CanRaisePO; }
        }

        public void NotifyAllCans()
        {
            foreach (var prop in this.GetType().GetProperties())
            {
                if (prop.Name.StartsWith("can", StringComparison.OrdinalIgnoreCase))
                    Notify(prop.Name);
            }
        }

        bool _ApprovalStatus = false;
        public bool ApprovalStatus
        {
            get { return _ApprovalStatus; }
            set
            {
                _ApprovalStatus = value;
                Notify("ApprovalStatus");
            }
        }

        public void UpdateTotal()
        {
            ConvertedPR.UpdateTotalValue();
            Notify("ConvertedPR");
        }

        public void UpdateNetAmount()
        {
            ConvertedPR.UpdateNetAmount();
            Notify("ConvertedPR");
        }

        public void UpdateTotalDiscount()
        {
            ConvertedPR.UpdateTotalDiscount();
            Notify("ConvertedPR");
        }

        bool _IsLoading=false;
        public bool IsLoading
        {
            get { return _IsLoading; }
            set
            {
                _IsLoading = value;
                Notify("IsLoading");
            }
        }

        internal void SetID(int p)
        {
            GetPR(p);
            IsLoading = true;
        }
    }
}
