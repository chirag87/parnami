﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Contruction.ReceivingService;
using System.ComponentModel;
using SIMS.MM.MODEL;

namespace Contruction
{
    public class POReceivingViewModel : ViewModel
    {
        ReceivingServiceClient service = new ReceivingServiceClient();

        public POReceivingViewModel()
        {
            if (!DesignerProperties.GetIsInDesignMode(Application.Current.RootVisual))
            {
                OnInit();
            }
        }

        #region Methods...

        public void OnInit()
        {
            service.ConvertfromPOCompleted += new EventHandler<ConvertfromPOCompletedEventArgs>(service_ConvertfromPOCompleted);
            service.CreateNewReceivingCompleted += new EventHandler<CreateNewReceivingCompletedEventArgs>(service_CreateNewReceivingCompleted);
        }

        public void GetConvertedGRN(int id)
        {
            service.ConvertfromPOAsync(id);
            IsBusy = true;
        }

        public void CreateGRN()
        {
            service.CreateNewReceivingAsync(NewGRN, Base.Current.UserName);
            IsBusy = true;
        }

        #endregion

        #region Completed Events...

        void service_CreateNewReceivingCompleted(object sender, CreateNewReceivingCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                Base.ShowError(e.Error);
                IsBusy = false;
            }
            else
            {
                IsBusy = false;
                CreatedGRN = e.Result;
                MessageBox.Show("Reference No: " + CreatedGRN.ReferenceNo + "\n\nNew GRN Added Successfully !");
                //Reset();
                //Base.AppEvents.RaiseNewReceivingAdded();
                Base.AppEvents.RaisePODataChanged();
            }
        }

        void service_ConvertfromPOCompleted(object sender, ConvertfromPOCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                Base.ShowError(e.Error);
                IsBusy = false;
            }
            else
            {
                NewGRN.Purchaser = Base.Current.UserName;
                NewGRN = e.Result;
                IsBusy = false;
            }
        }

        #endregion

        #region Properties...

        MReceiving _CreatedGRN = new MReceiving();
        public MReceiving CreatedGRN
        {
            get { return _CreatedGRN; }
            set
            {
                _CreatedGRN = value;
                Notify("CreatedGRN");
            }
        }

        MReceiving _NewGRN = new MReceiving();
        public MReceiving NewGRN
        {
            get { return _NewGRN; }
            set
            {
                _NewGRN = value;
                Notify("NewGRN");
            }
        }

        #endregion
    }
}