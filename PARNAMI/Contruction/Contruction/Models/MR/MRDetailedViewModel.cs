﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.ComponentModel;
using Contruction.MRService;
using SIMS.MM.MODEL;

namespace Contruction
{
    public class MRDetailedViewModel : ViewModel
    {
        MRServiceClient service = new MRServiceClient();

        public MRDetailedViewModel()
        {
            if (!DesignerProperties.GetIsInDesignMode(Application.Current.RootVisual))
            {
                OnInit();
            }
        }

        public void OnInit()
        {
            service.GetMRByIDCompleted += new EventHandler<GetMRByIDCompletedEventArgs>(service_GetMRByIDCompleted);
        }

        void service_GetMRByIDCompleted(object sender, GetMRByIDCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                Base.ShowError(e.Error);
                IsBusy = false;
            }
            else
            {
                SelectedMR = e.Result;
                IsBusy = false;
            }
        }

        MMR _SelectedMR = new MMR();
        public MMR SelectedMR
        {
            get { return _SelectedMR; }
            set
            {
                _SelectedMR = value;
                Notify("SelectedMR");
            }
        }

        internal void SetID(int p)
        {
            service.GetMRByIDAsync(p);
            IsBusy = true;
        }
    }
}
