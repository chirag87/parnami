﻿using System;
using System.Net;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.ComponentModel;
using Contruction.MRService;
using SIMS.MM.MODEL;

namespace Contruction
{
    public class NewMRModel : ViewModel
    {
        MRServiceClient service = new MRServiceClient();

        public NewMRModel()
        {
            if (!DesignerProperties.GetIsInDesignMode(Application.Current.RootVisual))
            {
                Reset();
                OnInit();
            }
        }

        public void OnInit()
        {
            service.CreateNewMRCompleted += new EventHandler<CreateNewMRCompletedEventArgs>(service_CreateNewMRCompleted);
        }

        void service_CreateNewMRCompleted(object sender, CreateNewMRCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                Base.ShowError(e.Error);
                IsBusy = false;
            }
            else
            {
                IsBusy = false;
                CreateMR = e.Result;
                MessageBox.Show("Reference No: " + CreateMR.ReferenceNo + "\n\nNew MR Created Successfully !");
                Reset();
                Base.AppEvents.RaiseNewMRAdded();
            }
        }

        MMR _NewMR = new MMR();
        public MMR NewMR
        {
            get { return _NewMR; }
            set
            {
                _NewMR = value;
                Notify("NewMR");
            }
        }

        MMR _CreateMR = new MMR();
        public MMR CreateMR
        {
            get { return _CreateMR; }
            set
            {
                _CreateMR = value;
                Notify("CreateMR");
            }
        }

        public void AddNewLine()
        {
            NewMR.AddNewLine();
            Notify("NewMR");
        }

        public void SubmitToDB()
        {
            if (!Validate()) return;
            service.CreateNewMRAsync(NewMR);
            IsBusy = true;
        }

        public void Reset()
        {
            NewMR = new MMR();
            NewMR.RequestedBy = Base.Current.UserName;
            NewMR.RaisedBy = Base.Current.UserName;
            NewMR.RaisedOn = DateTime.UtcNow.AddHours(5.5);
            Notify("NewMR");
        }

        public void DeleteLine(int lid)
        {
            try
            {
                var line = NewMR.MRLines.Single(x => x.LineID == lid);
                NewMR.MRLines.Remove(line);
            }
            catch { }
        }

        string _Status;
        public string Status
        {
            get { return _Status; }
            set
            {
                _Status = value;
                Notify("Status");
            }
        }

        public bool Validate()
        {
            Status = "";
            if (NewMR.SiteID == 0) { Status = "Please Select Site"; return false; }
            if (String.IsNullOrWhiteSpace(NewMR.RequestedBy)) { Status = "Requester Cannot be Empty!"; return false; }
            if (NewMR.MRLines == null) { Status = "MR should have alteast one item."; return false; }
            if (NewMR.MRLines.Count == 0) { Status = "MR should have alteast one item."; return false; }
            if (NewMR.MRLines.Any(x => x.ItemID == 0)) { Status = "MR Lines has some invaid/unselected Items!"; return false; }
            if (NewMR.MRLines.Any(x => x.ConsumableType == null)) { Status = "MR Lines has some invaid/unselected Consumable Type!"; return false; }
            return true;
        }
    }
}
