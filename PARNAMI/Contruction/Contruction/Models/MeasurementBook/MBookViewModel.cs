﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Contruction.SrvMBook;
using SIMS.TM.Model;
using System.ComponentModel;
namespace Contruction
{
    public class MBookViewModel:ViewModel
    {
        MBookServiceClient proxy = new MBookServiceClient();
        public MBookViewModel()
        {
            if (!DesignerProperties.GetIsInDesignMode(Application.Current.RootVisual))
            {
                OnInit();
            }
        }

        private void OnInit()
        {
            proxy.GetAllMBookCompleted += new EventHandler<GetAllMBookCompletedEventArgs>(proxy_GetAllMBookCompleted);
            LoadMBooks();
        }

        void proxy_GetAllMBookCompleted(object sender, GetAllMBookCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                Base.ShowError(e.Error);
                IsBusy = false;
            }
            else
            {
                MBooks = e.Result;
                IsBusy = false;
            }
        }

        IPaginated<MMBook> _MBooks;
        public IPaginated<MMBook> MBooks
        {
            get { return _MBooks; }
            set
            {
                _MBooks = value;
                Notify("MBooks");
                
            }
        }

        DateTime? _StartDate;
        public DateTime? StartDate
        {
            get { return _StartDate; }
            set
            {
                _StartDate = value;
                Notify("StartDate");
            }
        }

        DateTime? _EndDate;
        public DateTime? EndDate
        {
            get { return _EndDate; }
            set
            {
                _EndDate = value;
                Notify("EndDate");
            }
        }


        int _PageIndex = 0;
        public int PageIndex
        {
            get { return _PageIndex; }
            set
            {
                _PageIndex = value;
                Notify("PageIndex");
                LoadMBooks();
            }
        }

        string _SearchKey;
        public string SearchKey
        {
            get { return _SearchKey; }
            set
            {
                _SearchKey = value;
                Notify("SearchKey");
            }
        }

        int? _SiteID;
        public int? SiteID
        {
            get { return _SiteID; }
            set
            {
                _SiteID = value;
                Notify("SiteID");
            }
        }

        public void LoadMBooks()
        {
            proxy.GetAllMBookAsync(Base.Current.UserName, PageIndex, null, SearchKey, StartDate, EndDate, SiteID, true);
            IsBusy = true;
        }

        public void ClearAll()
        {
            SiteID = null;            
            SearchKey = null;
            LoadMBooks();
        }
    }
}
