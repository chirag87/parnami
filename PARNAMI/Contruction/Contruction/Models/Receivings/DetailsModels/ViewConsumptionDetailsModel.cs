﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Contruction.ReceivingService;
using System.Collections.ObjectModel;
using SIMS.MM.MODEL;
using System.ComponentModel;

namespace Contruction
{
    public class ViewConsumptionDetailsModel : ViewModel
    {
        ReceivingServiceClient service = new ReceivingServiceClient();

        public ViewConsumptionDetailsModel()
        {
            if (!DesignerProperties.GetIsInDesignMode(Application.Current.RootVisual))
            {
                OnInit();
            }
        }

        public void OnInit()
        {
            service.GetConsumptionByIDCompleted +=new EventHandler<GetConsumptionByIDCompletedEventArgs>(service_GetConsumptionByIDCompleted);
        }

        void service_GetConsumptionByIDCompleted(object sender, GetConsumptionByIDCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                Base.ShowError(e.Error);
                IsBusy = false;
            }
            else
            {
                NewConsumption = e.Result;
                IsBusy = false;
                IsOld = true;
            }
        }

        void Current_ConsumptionRequested(object sender, ConsumptionRequestedArgs e)
        {
            int receivingid = e.ConsumptionID;
            SetID(receivingid);
        }

        MReceiving _NewConsumption;
        public MReceiving NewConsumption
        {
            get { return _NewConsumption; }
            set
            {
                _NewConsumption = value;
                Notify("NewConsumption");
            }
        }

        bool _IsOld = false;
        public bool IsOld
        {
            get { return _IsOld; }
            set
            {
                _IsOld = value;
                Notify("IsOld");
                Notify("CanEdit");
            }
        }

        public override bool CanEdit
        {
            get
            {
                return !IsOld;
            }
        }

        public bool CanPrint
        {
            get
            {
                return true;
            }
        }

        internal void SetID(int p)
        {
            service.GetConsumptionByIDAsync(p);
            IsBusy = true;
        }
    }
}
