﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Contruction.ReceivingService;
using System.Collections.ObjectModel;
using SIMS.MM.MODEL;
using System.ComponentModel;

namespace Contruction
{
    public class ViewDirectPurchaseDetailsModel : ViewModel
    {
        ReceivingServiceClient service = new ReceivingServiceClient();

        public ViewDirectPurchaseDetailsModel()
        {
            if (!DesignerProperties.GetIsInDesignMode(Application.Current.RootVisual))
            {
                OnInit();
            }
        }

        public void OnInit()
        {
            service.GetDirectPurchaseByIDCompleted += new EventHandler<GetDirectPurchaseByIDCompletedEventArgs>(service_GetDirectPurchaseByIDCompleted);
        }

        void service_GetDirectPurchaseByIDCompleted(object sender, GetDirectPurchaseByIDCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                Base.ShowError(e.Error);
                IsBusy = false;
            }
            else
            {
                NewTransaction = e.Result;
                IsBusy = false;
                IsOld = true;
            }
        }

        MReceiving _NewTransaction;
        public MReceiving NewTransaction
        {
            get { return _NewTransaction; }
            set
            {
                _NewTransaction = value;
                Notify("NewTransaction");
            }
        }

        public void SetID(int recevingid)
        {
            service.GetDirectPurchaseByIDAsync(recevingid);
            IsBusy = true;
        }

        bool _IsOld = false;
        public bool IsOld
        {
            get { return _IsOld; }
            set
            {
                _IsOld = value;
                Notify("IsOld");
                Notify("CanEdit");
            }
        }

        public override bool CanEdit
        {
            get
            {
                return !IsOld;
            }
        }

        public bool CanPrint
        {
            get
            {
                return true;
            }
        }
    }
}
