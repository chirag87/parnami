﻿using System;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Collections.ObjectModel;
using SIMS.MM.MODEL;
using Contruction.ReceivingService;
using System.ComponentModel;

namespace Contruction
{
    public class NewDirectPurchaseModel : ViewModel
    {
        ReceivingServiceClient service = new ReceivingServiceClient();

        public NewDirectPurchaseModel()
        {
            if (!DesignerProperties.GetIsInDesignMode(Application.Current.RootVisual))
            {
                Reset();
                OnInit();
            }
        }

        MReceiving _NewTransaction;
        public MReceiving NewTransaction
        {
            get { return _NewTransaction; }
            set
            {
                _NewTransaction = value;
                Notify("NewTransaction");
            }
        }

        MReceiving _CreatedTransaction;
        public MReceiving CreatedTransaction
        {
            get { return _CreatedTransaction; }
            set
            {
                _CreatedTransaction = value;
                Notify("CreatedTransaction");
            }
        }

        public void OnInit()
        {
            service.CreateNewDirectPurchaseCompleted += new EventHandler<CreateNewDirectPurchaseCompletedEventArgs>(service_CreateNewDirectPurchaseCompleted);
        }

        void service_CreateNewDirectPurchaseCompleted(object sender, CreateNewDirectPurchaseCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                Base.ShowError(e.Error);
                IsBusy = false;
            }
            else
            {
                IsBusy = false;
                CreatedTransaction = e.Result;
                MessageBox.Show("Reference No: " + CreatedTransaction.ReferenceNo + "\n\nNew Direct Purchase Added Successfully !");
                Reset();
                Base.AppEvents.RaiseNewDirectPurchaseAdded();
            }
        }

        public void AddNewLine()
        {
            NewTransaction.AddNewLine();
            Notify("NewTransaction");
        }

        public void SubmitToDB()
        {
            if (!Validate()) return;
            service.CreateNewDirectPurchaseAsync(NewTransaction, Base.Current.UserName);
            IsBusy = true;
        }

        public void Reset()
        {
            NewTransaction = new MReceiving();
            NewTransaction.Purchaser = Base.Current.UserName;
            NewTransaction.PurchaseDate = DateTime.UtcNow.AddHours(5.5);
            Notify("NewTransaction");
        }

        public void DeleteLine(int lid)
        {
            try
            {
                var line = NewTransaction.ReceivingLines.Single(x => x.LineID == lid);
                NewTransaction.ReceivingLines.Remove(line);
            }
            catch { }
        }

        string _Status;
        public string Status
        {
            get { return _Status; }
            set
            {
                _Status = value;
                Notify("Status");
            }
        }

        public bool Validate()
        {
            Status = "";
            if (!NewTransaction.VendorID.HasValue) { Status = "Please Select Vendor"; return false; }
            if (NewTransaction.SiteID == 0) { Status = "Please Select Site"; return false; }
            if (String.IsNullOrWhiteSpace(NewTransaction.Purchaser)) { Status = "Requester Cannot be Empty!"; return false; }
            if (NewTransaction.ReceivingLines == null) { Status = "Direch Purchase should have alteast one item."; return false; }
            if (NewTransaction.ReceivingLines.Count == 0) { Status = "Direch Purchase should have alteast one item."; return false; }
            if (NewTransaction.ReceivingLines.Any(x => x.ItemID == 0)) { Status = "Direch Purchase Lines has some invaid/unselected Items!"; return false; }
            return true;
        }
    }
}
