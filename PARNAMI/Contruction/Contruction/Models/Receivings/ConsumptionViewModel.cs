﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using SIMS.MM.MODEL;
using System.Collections.Generic;
using Contruction.ReceivingService;
using System.Collections.ObjectModel;
using System.ComponentModel;

namespace Contruction
{
    public class ConsumptionViewModel : ViewModel   
    {
        ReceivingServiceClient service = new ReceivingServiceClient();

        public ConsumptionViewModel()
        {
            if (!DesignerProperties.GetIsInDesignMode(Application.Current.RootVisual))
            {
                OnInit();
            }
        }

        public event EventHandler ConsumptionChanged;
        public void RaiseConsumptionChanged()
        {
            if (ConsumptionChanged != null)
                ConsumptionChanged(this, new EventArgs());
        }

        int _PageIndex = 0;
        public int PageIndex
        {
            get { return _PageIndex; }
            set
            {
                _PageIndex = value;
                Notify("PageIndex");
                LoadConsumptions();
            }
        }

        public void OnInit()
        {
            service.GetAllConsumptionsCompleted += new EventHandler<GetAllConsumptionsCompletedEventArgs>(service_GetAllConsumptionsCompleted);
            LoadConsumptions();
            Base.AppEvents.ConsumptionDataChanged += new EventHandler(AppEvents_ConsumptionDataChanged);
        }

        void AppEvents_ConsumptionDataChanged(object sender, EventArgs e)
        {
            LoadConsumptions();
        }

        void service_GetAllConsumptionsCompleted(object sender, GetAllConsumptionsCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                Base.ShowError(e.Error);
                IsBusy = false;
            }
            else
            {
                Consumptions = e.Result;
                IsBusy = false;
            }
        }

        IPaginated<MReceivingHeader> _Consumptions;
        public IPaginated<MReceivingHeader> Consumptions
        {
            get { return _Consumptions; }
            set
            {
                _Consumptions = value;
                Notify("Consumptions");
                RaiseConsumptionChanged();
            }
        }

        string _SearchKey;
        public string SearchKey
        {
            get { return _SearchKey; }
            set
            {
                _SearchKey = value;
                Notify("SearchKey");
            }
        }

        int? _SiteID;
        public int? SiteID
        {
            get { return _SiteID; }
            set
            {
                _SiteID = value;
                Notify("SiteID");
            }
        }

        public void Clear()
        {
            SearchKey = null;
            SiteID = null;
            LoadConsumptions();
        }

        public void LoadConsumptions()
        {
            service.GetAllConsumptionsAsync(Base.Current.UserName, PageIndex, null, SearchKey, SiteID);
            IsBusy = true;
        }

        internal void MoveToPage(int p)
        {
            PageIndex = p;
        }
    }
}
