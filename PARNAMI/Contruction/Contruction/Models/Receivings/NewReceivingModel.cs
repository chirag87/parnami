﻿using System;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using SIMS.MM.MODEL;
using System.Collections.Generic;
using Contruction.ReceivingService;
using System.ComponentModel;
using System.Collections.ObjectModel;

namespace Contruction
{
    public class NewReceivingModel : ViewModel
    {
        ReceivingServiceClient service = new ReceivingServiceClient();

        public NewReceivingModel()
        {
            if (!DesignerProperties.GetIsInDesignMode(Application.Current.RootVisual))
            {
                Reset();
                OnInit();
            }
        }

        #region Properties...

        MRecivingView _NewGRN;
        public MRecivingView NewGRN
        {
            get { return _NewGRN; }
            set
            {
                _NewGRN = value;
                Notify("NewGRN");
            }
        }

        MReceiving _CreatedGRN;
        public MReceiving CreatedGRN
        {
            get { return _CreatedGRN; }
            set
            {
                _CreatedGRN = value;
                Notify("CreatedGRN");
            }
        }

        IEnumerable<MItem> _MItems = new List<MItem>();
        public IEnumerable<MItem> MItems
        {
            get { return _MItems; }
            set
            {
                _MItems = value;
                Notify("MItems");
            }
        }

        string _Status;
        public string Status
        {
            get { return _Status; }
            set
            {
                _Status = value;
                Notify("Status");
            }
        }

        bool _IsLocked = true;
        public bool IsLocked
        {
            get
            {
                return _IsLocked;
            }
            set
            {
                _IsLocked = value;
                Notify("IsLocked");
            }
        }

        #endregion

        #region Completed Events...

        void service_CreateNewReceivingCompleted(object sender, CreateNewReceivingCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                Base.ShowError(e.Error);
                IsBusy = false;
            }
            else
            {
                IsBusy = false;
                CreatedGRN = e.Result;
                MessageBox.Show("Reference No: " + CreatedGRN.ReferenceNo + "\n\nNew GRN Added Successfully !");
                Reset();
                Base.AppEvents.RaiseNewReceivingAdded();
            }
        }

        void service_GetAllowedItemIDsCompleted(object sender, GetAllowedItemIDsCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                Base.ShowError(e.Error);
                IsBusy = false;
            }
            else
            {
                MItems = Base.Current.Items.Where(x => e.Result.Contains(x.ID));
                foreach (var line in NewGRN.LinesViews)
                {
                    line.AllowedItems = MItems;
                }
                IsBusy = false;
            }
        }

        void line_ItemIDChanged(object sender, EventArgs e)
        {
            var item = sender as MReceivingLine;
            service.GetPendingPOInfosCompleted += new EventHandler<GetPendingPOInfosCompletedEventArgs>(service_GetPendingPOInfosCompleted);
            service.GetPendingPOInfosAsync(NewGRN.VendorID.Value, NewGRN.SiteID, item.ItemID);
            IsBusy = true;
        }

        void service_GetPendingPOInfosCompleted(object sender, GetPendingPOInfosCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                Base.ShowError(e.Error);
                IsBusy = false;
            }

            if (e.Result.Count == 0)
            {
                IsBusy = false;
                return;
            }

            var rline = NewGRN.LinesViews.Where(x => x.ItemID == e.Result.First().ItemID);
            foreach (var item in rline)
            {
                if (e.Result.Count == 1 && e.Result.First().IsBlank == true)
                {
                    item.PendingInfos = new ObservableCollection<PendingPOInfo>();
                    item.SelectedPOInfo = null;
                    item.CanEditUnitPrice = true;
                }
                else
                {
                    item.PendingInfos = e.Result;
                    item.CanEditUnitPrice = false;
                }
            }
            IsBusy = false;
        }

        #endregion

        #region Methods...

        public void OnInit()
        {
            service.CreateNewReceivingCompleted += new EventHandler<CreateNewReceivingCompletedEventArgs>(service_CreateNewReceivingCompleted);
            service.GetAllowedItemIDsCompleted += new EventHandler<GetAllowedItemIDsCompletedEventArgs>(service_GetAllowedItemIDsCompleted);
        }

        public void CallItems()
        {
            if (NewGRN.VendorID == null) return;
            if (NewGRN.SiteID == 0) return;
            service.GetAllowedItemIDsAsync(NewGRN.VendorID.Value, NewGRN.SiteID);
            IsBusy = true;
        }

        public void AddNewLine()
        {
            if (NewGRN.LinesViews.Count == 0)
                CallItems();
            var line = NewGRN.AddNewLineView();
            line.AllowedItems = MItems;
            line.ItemIDChanged += new EventHandler(line_ItemIDChanged);
            Notify("NewGRN");
            CanLock();
        }

        public void DeleteLine(int lid)
        {
            try
            {
                var line = NewGRN.LinesViews.Single(x => x.LineID == lid);
                NewGRN.DeleteLineView(line);
            }
            catch { }
        }

        public void SubmitToDB()
        {
            if (!Validate()) return;
            service.CreateNewReceivingAsync(NewGRN.GetMReceiving(), Base.Current.UserName);
            IsBusy = true;
        }

        public void Reset()
        {
            NewGRN = new MRecivingView();
            NewGRN.Purchaser = Base.Current.UserName;
            NewGRN.PurchaseDate = DateTime.UtcNow.AddHours(5.5);
            Notify("NewGRN");
            IsLocked = true;
        }

        public void CanLock()
        {
            if (NewGRN.VendorID.HasValue && NewGRN.SiteID != 0)
                IsLocked = false;
        }

        public bool Validate()
        {
            Status = "";
            if (!NewGRN.VendorID.HasValue) { Status = "Please Select Vendor"; return false; }
            if (NewGRN.SiteID == 0) { Status = "Please Select Site"; return false; }
            if (String.IsNullOrWhiteSpace(NewGRN.Purchaser)) { Status = "Requester Cannot be Empty!"; return false; }
            if (NewGRN.LinesViews == null) { Status = "GRN should have alteast one item."; return false; }
            if (NewGRN.LinesViews.Count == 0) { Status = "GRN should have alteast one item."; return false; }
            if (NewGRN.LinesViews.Any(x => x.ItemID == 0)) { Status = "GRN Lines has some invaid/unselected Items!"; return false; }
            return true;
        }

        #endregion
    }

    public class MReceivingLineView : MReceivingLine
    {
        PendingPOInfo _SelectedPOInfo = new PendingPOInfo();
        public PendingPOInfo SelectedPOInfo
        {
            get { return _SelectedPOInfo; }
            set
            {
                _SelectedPOInfo = value;
                if (value != null)
                {
                    this.POID = SelectedPOInfo.POID;
                    this.OrderedQty = SelectedPOInfo.OrderedQty;
                    this.EarlierReceivedQty = SelectedPOInfo.EarlierReceivedQty;
                    this.TransactionTypeID = "GRN";
                    Notify("SelectedPOInfo");
                }
            }
        }

        ObservableCollection<PendingPOInfo> _PendingInfos = new ObservableCollection<PendingPOInfo>();
        public ObservableCollection<PendingPOInfo> PendingInfos
        {
            get { return _PendingInfos; }
            set
            {
                _PendingInfos = value;
                if (value.Count > 0)
                {
                    SelectedPOInfo = value.First();
                }
                else
                {
                    SelectedPOInfo = null;
                }
                Notify("PendingInfos");
            }
        }

        IEnumerable<MItem> _AllowedItems;
        public IEnumerable<MItem> AllowedItems
        {
            get { return _AllowedItems; }
            set
            {
                _AllowedItems = value;
                Notify("AllowedItems");
            }
        }

        bool _CanEditUnitPrice = true;
        public bool CanEditUnitPrice
        {
            get { return _CanEditUnitPrice; }
            set
            {
                _CanEditUnitPrice = value;
                Notify("CanEditUnitPrice");
            }
        }
    }        

    public class MRecivingView : MReceiving
    {
        public MRecivingView()
        {
        }

        public MRecivingView(MReceiving rcv)
        {
            this.ChallanNo = rcv.ChallanNo;
            this.CreatedBy = rcv.CreatedBy;
            this.CreatedOn = rcv.CreatedOn;
            this.InvoiceNo = rcv.InvoiceNo;
            this.LastUpdaetdOn = rcv.LastUpdaetdOn;
            this.LastUpdatedBy = rcv.LastUpdatedBy;
            this.Purchaser = rcv.Purchaser;
            this.PurchaseDate = rcv.PurchaseDate;
            this.ReceivingLines = rcv.ReceivingLines;
            this.ReceivingRemarks = rcv.ReceivingRemarks;
            this.Site = rcv.Site;
            this.SiteID = rcv.SiteID;
            this.TransactionID = rcv.TransactionID;
            this.TransactionTypeID = rcv.TransactionTypeID;
            this.TransporterID = rcv.TransporterID;
            this.VehicleDetails = rcv.VehicleDetails;
            this.Vendor = rcv.Vendor;
            this.VendorID = rcv.VendorID;

            if (rcv.ReceivingLines == null)
                rcv.ReceivingLines = new ObservableCollection<MReceivingLine>();
            foreach (var line in rcv.ReceivingLines)
            {
                MReceivingLineView newline = new MReceivingLineView();
                newline.EarlierQty = line.EarlierQty;
                newline.Item = line.Item;
                newline.ItemID = line.ItemID;
                newline.LineID = line.LineID;
                newline.LinePrice = line.LinePrice;
                newline.LineRemarks = line.LineRemarks;
                newline.MTRID = line.MTRID;
                newline.MTRLineID = line.MTRLineID;
                newline.NewPendingQty = line.NewPendingQty;
                newline.NQuantity = line.NQuantity;
                newline.OrderedQty = line.OrderedQty;
                newline.Quantity = line.Quantity;
                newline.TransactionID = line.TransactionID;
                newline.TransactionTypeID = line.TransactionTypeID;
                newline.TruckNo = line.TruckNo;
                newline.UnitPrice = line.UnitPrice;
                newline.LineChallanNo = line.LineChallanNo;
                newline.POID = line.POID;
                newline.PRID = line.PRID;
                newline.PRLineID = line.PRLineID;

                newline.SelectedPOInfo.OrderedQty = line.OrderedQty;
                newline.SelectedPOInfo.EarlierReceivedQty = line.EarlierReceivedQty;

                //newline.SelectedPOInfo.POID = line.POID.Value;
                //newline.SelectedPOInfo.PRID = line.PRID.Value;
                //newline.SelectedPOInfo.PRLineID = line.PRLineID.Value;

                this.LinesViews.Add(newline);
            }
        }

        ObservableCollection<MReceivingLineView> _LinesViews = new ObservableCollection<MReceivingLineView>();
        public ObservableCollection<MReceivingLineView> LinesViews
        {
            get { return _LinesViews; }
            set
            {
                _LinesViews = value;
                Notify("LinesViews");

            }
        }

        public MReceivingLineView AddNewLineView()
        {
            MReceivingLineView line = new MReceivingLineView();
            LinesViews.Add(line);
            UpdateLineNumberViews();
            Notify("LinesViews");
            return line;
        }

        public int UpdateLineNumberViews()
        {
            if (LinesViews == null) return 0;
            int i = 1;
            foreach (var item in LinesViews)
            {
                item.LineID = i;
                i++;
            }
            return i - 1;
        }

        public MReceivingLineView DeleteLineView(MReceivingLineView line)
        {
            LinesViews.Remove(line);
            UpdateLineNumberViews();
            Notify("LinesViews");
            return line;
        }

        public MReceiving GetMReceiving()
        {
            MReceiving rcv = new MReceiving();
            rcv.ChallanNo = this.ChallanNo;
            rcv.CreatedBy = this.CreatedBy;
            rcv.CreatedOn = this.CreatedOn;
            rcv.InvoiceNo = this.InvoiceNo;
            rcv.LastUpdaetdOn = this.LastUpdaetdOn;
            rcv.LastUpdatedBy = this.LastUpdatedBy;
            rcv.Purchaser = this.Purchaser;
            rcv.PurchaseDate = this.PurchaseDate;
            //rcv.ReceivingLines = this.ReceivingLines;
            rcv.ReceivingRemarks = this.ReceivingRemarks;
            rcv.Site = this.Site;
            rcv.SiteID = this.SiteID;
            rcv.TransactionID = this.TransactionID;
            rcv.TransactionTypeID = this.TransactionTypeID;
            rcv.TransporterID = this.TransporterID;
            rcv.VehicleDetails = this.VehicleDetails;
            rcv.Vendor = this.Vendor;
            rcv.VendorID = this.VendorID;

            if (rcv.ReceivingLines == null)
                rcv.ReceivingLines = new ObservableCollection<MReceivingLine>();
            foreach (var line in this.LinesViews)
            {
                var newline = new MReceivingLine();
                newline.EarlierQty = line.EarlierQty;
                newline.Item = line.Item;
                newline.ItemID = line.ItemID;
                newline.LineID = line.LineID;
                newline.LinePrice = line.LinePrice;
                newline.LineRemarks = line.LineRemarks;
                newline.MTRID = line.MTRID;
                newline.MTRLineID = line.MTRLineID;
                newline.NewPendingQty = line.NewPendingQty;
                newline.NQuantity = line.NQuantity;
                newline.OrderedQty = line.OrderedQty;
                newline.Quantity = line.Quantity;
                newline.TransactionID = line.TransactionID;
                newline.TransactionTypeID = line.TransactionTypeID;
                newline.TruckNo = line.TruckNo;
                newline.UnitPrice = line.UnitPrice;
                newline.LineChallanNo = line.LineChallanNo;
                newline.POID = line.POID;
                newline.PRID = line.PRID;
                newline.PRLineID = line.PRLineID;

                if (line.SelectedPOInfo != null)
                {
                    newline.POID = line.SelectedPOInfo.POID;
                    newline.PRID = line.SelectedPOInfo.PRID;
                    newline.PRLineID = line.SelectedPOInfo.PRLineID;
                }
                //else
                //{
                //    newline.POID = null;
                //    newline.PRID = null;
                //    newline.PRLineID = null;
                //}
                rcv.ReceivingLines.Add(newline);
            }
            return rcv;
        }
    }
}
