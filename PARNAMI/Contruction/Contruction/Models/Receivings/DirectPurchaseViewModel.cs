﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Collections.ObjectModel;
using SIMS.MM.MODEL;
using Contruction.ReceivingService;
using System.ComponentModel;

namespace Contruction
{
    public class DirectPurchaseViewModel : ViewModel
    {
        ReceivingServiceClient service = new ReceivingServiceClient();

        public DirectPurchaseViewModel()
        {
            if (!DesignerProperties.GetIsInDesignMode(Application.Current.RootVisual))
            {
                OnInit();
            }
        }

        public event EventHandler DPChanged;
        public void RaiseDPChanged()
        {
            if (DPChanged != null)
                DPChanged(this, new EventArgs());
        }

        int _PageIndex = 0;
        public int PageIndex
        {
            get { return _PageIndex; }
            set
            {
                _PageIndex = value;
                Notify("PageIndex");
                LoadDirectPurchases();
            }
        }

        public void OnInit()
        {
            service.GetAllDirectPurchasesCompleted += new EventHandler<GetAllDirectPurchasesCompletedEventArgs>(service_GetAllDirectPurchasesCompleted);
            LoadDirectPurchases();
            Base.AppEvents.DirectPurchaseDataChanged += new EventHandler(AppEvents_DirectPurchaseDataChanged);
        }

        void AppEvents_DirectPurchaseDataChanged(object sender, EventArgs e)
        {
            LoadDirectPurchases();
        }

        void service_GetAllDirectPurchasesCompleted(object sender, GetAllDirectPurchasesCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                Base.ShowError(e.Error);
                IsBusy = false;
            }
            else
            {
                DirectPurchases = e.Result;
                IsBusy = false;
            }
        }

        IPaginated<MReceivingHeader> _DirectPurchases;
        public IPaginated<MReceivingHeader> DirectPurchases
        {
            get { return _DirectPurchases; }
            set
            {
                _DirectPurchases = value;
                Notify("DirectPurchases");
                RaiseDPChanged();
            }
        }

        string _SearchKey;
        public string SearchKey
        {
            get { return _SearchKey; }
            set
            {
                _SearchKey = value;
                Notify("SearchKey");
            }
        }

        int? _VendorID;
        public int? VendorID
        {
            get { return _VendorID; }
            set
            {
                _VendorID = value;
                Notify("VendorID");
            }
        }

        int? _SiteID;
        public int? SiteID
        {
            get { return _SiteID; }
            set
            {
                _SiteID = value;
                Notify("SiteID");
            }
        }

        public void Clear()
        {
            SearchKey = null;
            VendorID = null;
            SiteID = null;
            LoadDirectPurchases();
        }

        public void LoadDirectPurchases()
        {
            service.GetAllDirectPurchasesAsync(Base.Current.UserName, PageIndex, null, SearchKey, VendorID, SiteID);
            IsBusy = true;
        }

        internal void MoveToPage(int p)
        {
            PageIndex = p;
        }
    }
}
