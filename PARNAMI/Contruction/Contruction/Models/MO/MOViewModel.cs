﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.ComponentModel;
using Contruction.MOService;
using SIMS.MM.MODEL;

namespace Contruction
{
    public class MOViewModel : ViewModel
    {
        MOServiceClient service = new MOServiceClient();

        public MOViewModel()
        {
            if (!DesignerProperties.GetIsInDesignMode(Application.Current.RootVisual))
            {
                OnInit();
            }
        }

        public event EventHandler MOChanged;
        public void RaiseMOChanged()
        {
            if (MOChanged != null)
                MOChanged(this, new EventArgs());
        }

        public void OnInit()
        {
            service.GetAllMOsCompleted+=new EventHandler<GetAllMOsCompletedEventArgs>(service_GetAllMOsCompleted);
            LoadMOs();
            Base.AppEvents.MODataChanged += new EventHandler(AppEvents_MODataChanged);
        }

        void AppEvents_MODataChanged(object sender, EventArgs e)
        {
            LoadMOs();
        }

        void service_GetAllMOsCompleted(object sender, GetAllMOsCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                Base.ShowError(e.Error);
                IsBusy = false;
            }
            else
            {
                MOs = e.Result;
                IsBusy = false;
            }
        }

        IPaginated<MMOHeader> _MOs = new PaginatedData<MMOHeader>();
        public IPaginated<MMOHeader> MOs
        {
            get { return _MOs; }
            set
            {
                _MOs = value;
                Notify("MOs");
                RaiseMOChanged();
            }
        }

        int _PageIndex = 0;
        public int PageIndex
        {
            get { return _PageIndex; }
            set
            {
                _PageIndex = value;
                Notify("PageIndex");
                LoadMOs();
            }
        }

        string _SearchKey;
        public string SearchKey
        {
            get { return _SearchKey; }
            set
            {
                _SearchKey = value;
                Notify("SearchKey");
            }
        }

        int? _SiteID;
        public int? SiteID
        {
            get { return _SiteID; }
            set
            {
                _SiteID = value;
                Notify("SiteID");
            }
        }

        public void Clear()
        {
            SearchKey = null;
            SiteID = null;
            LoadMOs();
        }

        public void LoadMOs()
        {
            service.GetAllMOsAsync(Base.Current.UserName, PageIndex, null, SearchKey, SiteID);
            IsBusy = true;
        }

        internal void MoveToPage(int p)
        {
            PageIndex = p;
        }
    }
}
