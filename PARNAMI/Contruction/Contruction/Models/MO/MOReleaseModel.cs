﻿using System;
using System.Net;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.ComponentModel;
using SIMS.MM.MODEL;
using Contruction.MOService;

namespace Contruction
{
    public class MOReleaseModel : ViewModel
    {
        public event EventHandler Released;
        public void RaiseReleased()
        {
            if (Released != null)
                Released(this, new EventArgs());
        }

        MOServiceClient service = new MOServiceClient();

        public MOReleaseModel()
        {
            if (!DesignerProperties.GetIsInDesignMode(Application.Current.RootVisual))
            {
                OnInit();
            }
        }

        public void OnInit()
        {
            service.GetMReceivingforMOReleaseCompleted += new EventHandler<GetMReceivingforMOReleaseCompletedEventArgs>(service_GetMReceivingforMOReleaseCompleted);
            service.ReleaseMOCompleted += new EventHandler<ReleaseMOCompletedEventArgs>(service_ReleaseMOCompleted);
            ReceivingforRelease.ReleasedBy = Base.Current.UserName;
            ReceivingforRelease.CreatedBy = Base.Current.UserName;
        }

        void service_ReleaseMOCompleted(object sender, ReleaseMOCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                Base.ShowError(e.Error);
                IsBusy = false;
            }
            else
            {
                IsBusy = false;
                IsReleased = true;
                ReferenceNo = e.Result;
                MessageBox.Show("Reference No:" + ReferenceNo + "\n\nMO Released Successfully to " + ReceivingforRelease.Site);
                Base.AppEvents.RaiseNewMOAdded();
                RaiseReleased();
            }
        }

        void service_GetMReceivingforMOReleaseCompleted(object sender, GetMReceivingforMOReleaseCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                Base.ShowError(e.Error);
                IsBusy = false;
            }
            else
            {
                ReceivingforRelease = e.Result;
                ReceivingforRelease.Purchaser = Base.Current.UserName;
                var lines = ReceivingforRelease.ReceivingLines.ToList();
                foreach (var line in lines)
                {
                    line.NQuantity = line.OrderedQty;
                }
                IsBusy = false;
            }
        }

        int _TransactionID;
        public int TransactionID
        {
            get { return _TransactionID; }
            set
            {
                _TransactionID = value;
                Notify("TransactionID");
            }
        }

        string _ReferenceNo;
        public string ReferenceNo
        {
            get { return _ReferenceNo; }
            set
            {
                _ReferenceNo = value;
                Notify("ReferenceNo");
            }
        }

        MReceiving _ReceivingforRelease = new MReceiving();
        public MReceiving ReceivingforRelease
        {
            get { return _ReceivingforRelease; }
            set
            {
                _ReceivingforRelease = value;
                Notify("ReceivingforRelease");
            }
        }

        public void GetReceivingforMORelease(int moid)
        {
            service.GetMReceivingforMOReleaseAsync(moid);
            IsBusy = true;
        }

        public void MORelease()
        {
            if (!Validate())
            {
                IsReleased = false;
                return;
            }
            service.ReleaseMOAsync(ReceivingforRelease);
            IsBusy = true;            
        }

        bool _IsReleased = false;
        public bool IsReleased
        {
            get { return _IsReleased; }
            set
            {
                _IsReleased = value;
                Notify("IsReleased");
            }
        }

        string _Status;
        public string Status
        {
            get { return _Status; }
            set
            {
                _Status = value;
                Notify("Status");
            }
        }

        public bool Validate()
        {
            Status = "";
            if (ReceivingforRelease.TransporterID == null) { Status = "Please Select Transporter!"; return false; }
            if (ReceivingforRelease.ReceivingLines.All(x => x.NQuantity == 0.0)) { Status = "Move Order has some invaid/unselected Quantity!"; return false; }
            if (ReceivingforRelease.ReceivingLines.All(x => x.LineChallanNo == "" || x.LineChallanNo == null)) { Status = "Move Order has some invaid/unselected Challan No.!"; return false; }
            return true;
        }
    }
}
