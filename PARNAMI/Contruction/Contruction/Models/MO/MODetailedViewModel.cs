﻿using System;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Contruction.MOService;
using System.ComponentModel;
using SIMS.MM.MODEL;

namespace Contruction
{
    public class MODetailedViewModel : ViewModel
    {
        MOServiceClient service = new MOServiceClient();

        public MODetailedViewModel()
        {
            if (!DesignerProperties.GetIsInDesignMode(Application.Current.RootVisual))
            {
                OnInit();
            }
        }

        public void OnInit()
        {
            AppEvents.Current.MORequested += new EventHandler<MORequestedArgs>(Current_MORequested);
            service.GetMObyIDCompleted += new EventHandler<GetMObyIDCompletedEventArgs>(service_GetMObyIDCompleted);
            service.CloseMOCompleted += new EventHandler<AsyncCompletedEventArgs>(service_CloseMOCompleted);
        }

        void service_CloseMOCompleted(object sender, AsyncCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                Base.ShowError(e.Error);
                IsBusy = false;
            }
            else
            {
                IsBusy = false;
                MessageBox.Show("This Move Order is now Closed !");
                Base.AppEvents.RaiseNewMOAdded();
            }
        }

        void Current_MORequested(object sender, MORequestedArgs e)
        {
            int moid = e.MOID;
            GetMO(moid);
        }

        void service_GetMObyIDCompleted(object sender, GetMObyIDCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                Base.ShowError(e.Error);
                IsBusy = false;
            }
            else
            {
                ConvertedMO = e.Result;
                IsBusy = false;
            }
        }

        MMO _ConvertedMO;
        public MMO ConvertedMO
        {
            get { return _ConvertedMO; }
            set
            {
                _ConvertedMO = value;
                Notify("ConvertedMO");
            }
        }

        public void GetMO(int moid)
        {
            service.GetMObyIDAsync(moid);
            IsBusy = true;
        }

        public void CloseMO(int id)
        {
            if (ConvertedMO.MOLines.All(x => x.PendingReceiveQty == null))
            {
                service.CloseMOAsync(id);
                IsBusy = true;
            }
            else
            {
                MessageBox.Show("Can't Close MO, as their are some Material Left to be received !");
            }
        }
    }
}
