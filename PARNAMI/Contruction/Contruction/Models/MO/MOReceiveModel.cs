﻿using System;
using System.Net;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Contruction.MOService;
using SIMS.MM.MODEL;
using System.ComponentModel;

namespace Contruction
{
    public class MOReceiveModel : ViewModel
    {
        public event EventHandler Received;
        private void RaiseReceive()
        {
            if (Received != null)
                Received(this, new EventArgs());
        }
        MOServiceClient service = new MOServiceClient();

        public MOReceiveModel()
        {
            if (!DesignerProperties.GetIsInDesignMode(Application.Current.RootVisual))
            {
                OnInit();
            }
        }

        public void OnInit()
        {
            service.GetMReceivingforMOReceiveCompleted += new EventHandler<GetMReceivingforMOReceiveCompletedEventArgs>(service_GetMReceivingforMOReceiveCompleted);
            service.ReceiveMOCompleted += new EventHandler<ReceiveMOCompletedEventArgs>(service_ReceiveMOCompleted);
            ReceivingforRelease.ReceivedBy = Base.Current.UserName;
        }

        void service_ReceiveMOCompleted(object sender, ReceiveMOCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                Base.ShowError(e.Error);
                IsBusy = false;
            }
            else
            {
                IsBusy = false;
                IsReceived = true;
                TransactionID = e.Result;
                MessageBox.Show("MO Received Successfully from " + ReceivingforRelease.TransferSite);
                Base.AppEvents.RaiseNewMOAdded();
                RaiseReceive();
            }
        }

        void service_GetMReceivingforMOReceiveCompleted(object sender, GetMReceivingforMOReceiveCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                Base.ShowError(e.Error);
                IsBusy = false;
            }
            else
            {
                ReceivingforRelease = e.Result;
                ReceivingforRelease.Purchaser = Base.Current.UserName;
                IsBusy = false;
            }
        }

        int _TransactionID;
        public int TransactionID
        {
            get { return _TransactionID; }
            set
            {
                _TransactionID = value;
                Notify("TransactionID");
            }
        }

        MReceiving _ReceivingforRelease = new MReceiving();
        public MReceiving ReceivingforRelease
        {
            get { return _ReceivingforRelease; }
            set
            {
                _ReceivingforRelease = value;
                Notify("ReceivingforRelease");
            }
        }

        public void GetReceivingforMOReceive(int moid)
        {
            service.GetMReceivingforMOReceiveAsync(moid);
            IsBusy = true;
        }

        public void MOReceive()
        {
            if (!Validate())
            {
                IsReceived = false;
                return;
            }
            service.ReceiveMOAsync(ReceivingforRelease);
            IsBusy = true;
        }

        string _Status;
        public string Status
        {
            get { return _Status; }
            set
            {
                _Status = value;
                Notify("Status");
            }
        }

        bool _IsReceived = false;
        public bool IsReceived
        {
            get { return _IsReceived; }
            set
            {
                _IsReceived = value;
                Notify("IsReceived");
            }
        }

        public bool Validate()
        {
            Status = "";
            if (ReceivingforRelease.ReceivingLines.All(x => x.Quantity == 0.0)) { Status = "Move Order has some invaid/unselected Quantity!"; return false; }
            return true;
        }
    }
}
