﻿using System;
using System.Net;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Contruction.MOService;
using System.ComponentModel;
using SIMS.MM.MODEL;
using System.Collections.ObjectModel;

namespace Contruction
{
    public class NewMOfromSplittedPRModel : ViewModel
    {
         MOServiceClient service = new MOServiceClient();

         public NewMOfromSplittedPRModel()
        {
            if (!DesignerProperties.GetIsInDesignMode(Application.Current.RootVisual))
            {
                OnInit();
            }
        }

        public class SplittedMORequestedArgs : EventArgs
        {
            public MMO mo { get; set; }
            public SplittedMORequestedArgs(MMO _mo) { mo = _mo; }
        }

        public event EventHandler<SplittedMORequestedArgs> SaveSplittedMO;
        public void RaiseSaveSplittedMO(MMO newMO)
        {
            if (SaveSplittedMO != null)
                SaveSplittedMO(this, new SplittedMORequestedArgs(newMO));
        }

        MMO _NewMO;
        public MMO NewMO
        {
            get { return _NewMO; }
            set
            {
                _NewMO = value;
                Notify("NewMO");
            }
        }

        string _Status;
        public string Status
        {
            get { return _Status; }
            set
            {
                _Status = value;
                Notify("Status");
            }
        }

        public void OnInit()
        {
            service.SplitToMOCompleted += new EventHandler<SplitToMOCompletedEventArgs>(service_SplitToMOCompleted);
        }

        void service_SplitToMOCompleted(object sender, SplitToMOCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                Base.ShowError(e.Error);
                IsBusy = false;
            }
            else
            {
                IsBusy = false;
                CreatedMO = e.Result;
                MessageBox.Show("Reference No: " + CreatedMO.ReferenceNo + "\n\nNew Move Order created successfully!");
                //Base.AppEvents.RaiseNewMOAdded();
            }
        }

        public void CreateNewSplittedMO(int OldPRID, ObservableCollection<int> lineIDs)
        {
            if (!Validate()) return;
            service.SplitToMOAsync(NewMO, OldPRID, lineIDs);
            IsBusy = true;
        }

        MMO _CreatedMO;
        public MMO CreatedMO
        {
            get { return _CreatedMO; }
            set
            {
                _CreatedMO = value;
                Notify("CreatedMO");
            }
        }

        public bool Validate()
        {
            Status = "";
            if (NewMO.RequestingSiteID == 0) { Status = "Please Select Requesting Site"; return false; }
            if (NewMO.TransferringSiteID == 0) { Status = "Please Select Transferring Site"; return false; }
            if (String.IsNullOrWhiteSpace(NewMO.Requester)) { Status = "Requester Cannot be Empty!"; return false; }
            if (NewMO.MOLines == null) { Status = "Move Order should have alteast one item."; return false; }
            if (NewMO.MOLines.Count == 0) { Status = "Move Order should have alteast one item."; return false; }
            if (NewMO.MOLines.Any(x => x.ItemID == 0)) { Status = "Move Order Lines has some invaid/unselected Items!"; return false; }
            if (NewMO.MOLines.Any(X => X.Quantity == 0)) { Status = "Move Order Lines must have some Quantity of Item!"; return false; }
            return true;
        }
    }
}
