﻿using System;
using System.Net;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.ComponentModel;
using Contruction.MOService;
using SIMS.MM.MODEL;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace Contruction
{
    public class NewMOModel : ViewModel
    {
        MOServiceClient service = new MOServiceClient();

        public NewMOModel()
        {
            if (!DesignerProperties.GetIsInDesignMode(Application.Current.RootVisual))
            {
                Reset();
                OnInit();
            }
        }

        MMO _NewMO;
        public MMO NewMO
        {
            get { return _NewMO; }
            set
            {
                _NewMO = value;
                Notify("NewMO");
            }
        }

        string _Status;
        public string Status
        {
            get { return _Status; }
            set
            {
                _Status = value;
                Notify("Status");
            }
        }

        public void OnInit()
        {
            service.CreateNewMOCompleted += new EventHandler<CreateNewMOCompletedEventArgs>(service_CreateNewMOCompleted);
        }

        void service_CreateNewMOCompleted(object sender, CreateNewMOCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                Base.ShowError(e.Error);
                IsBusy = false;
            }
            else
            {
                IsBusy = false;
                CreatedMO = e.Result;
                //MessageBox.Show("Reference No: " + CreatedMO.ReferenceNo + "\n\nNew Move Order created successfully!");
                MessageBox.Show("Please Now Release the MO !");
                Reset();
                Base.AppEvents.RaiseNewMOAdded();
                if (RequireRelease)
                {
                    Contruction.Views.MO.MORelease win = new Views.MO.MORelease(CreatedMO.ID);
                    win.Show();
                }
                RequireRelease = false;
            }
        }

        public bool RequireRelease = false;
        public void SubmitToDB()
        {
            if (!Validate()) return;
            service.CreateNewMOAsync(NewMO);
            IsBusy = true;
        }

        MMO _CreatedMO;
        public MMO CreatedMO
        {
            get { return _CreatedMO; }
            set
            {
                _CreatedMO = value;
                Notify("CreatedMO");
            }
        }

        public void Reset()
        {
            NewMO = new MMO();
            NewMO.Requester = Base.Current.UserName;
            NewMO.RaisedBy = Base.Current.UserName;
            NewMO.TransferDate = DateTime.UtcNow.AddHours(5.5);
            Notify("NewMO");
        }

        public void AddNewLine()
        {
            NewMO.AddNewLine();
            Notify("NewMO");
        }

        public void DeleteLine(int lid)
        {
            try
            {
                var line = NewMO.MOLines.Single(x => x.LineID == lid);
                NewMO.MOLines.Remove(line);
            }
            catch { }
        }

        public bool Validate()
        {
            Status = "";
            if (NewMO.RequestingSiteID == 0) { Status = "Please Select Requesting Site"; return false; }
            if (NewMO.TransferringSiteID == 0) { Status = "Please Select Transferring Site"; return false; }
            if (String.IsNullOrWhiteSpace(NewMO.Requester)) { Status = "Requester Cannot be Empty!"; return false; }
            if (NewMO.MOLines == null) { Status = "Move Order should have alteast one item."; return false; }
            if (NewMO.MOLines.Count == 0) { Status = "Move Order should have alteast one item."; return false; }
            if (NewMO.MOLines.Any(x => x.ItemID == 0)) { Status = "Move Order Lines has some invaid/unselected Items!"; return false; }
            if (NewMO.MOLines.Any(X => X.Quantity == 0)) { Status = "Move Order Lines must have some Quantity of Item!"; return false; }
            return true;
        }
    }
}
