﻿using System;
using System.Net;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Contruction.MasterDBService;
using System.ComponentModel;
using System.Collections.ObjectModel;
using SIMS.MM.MODEL;

namespace Contruction
{
    public class AccountsWindowViewModel : ViewModel
    {
        MasterDataServiceClient service = new MasterDataServiceClient();

        public AccountsWindowViewModel()
        {
            if (!DesignerProperties.GetIsInDesignMode(Application.Current.RootVisual))
            {
                OnInit();
            }
        }

        public void OnInit()
        {
            service.GetAccountsbyForIDCompleted += new EventHandler<GetAccountsbyForIDCompletedEventArgs>(service_GetAccountsbyForIDCompleted);
            service.UpdateAccountCompleted += new EventHandler<UpdateAccountCompletedEventArgs>(service_UpdateAccountCompleted);
            service.CreateNewAccountCompleted += new EventHandler<CreateNewAccountCompletedEventArgs>(service_CreateNewAccountCompleted);
        }

        void service_CreateNewAccountCompleted(object sender, CreateNewAccountCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                Base.ShowError(e.Error);
                IsBusy = false;
            }
            else
            {
                MessageBox.Show("Account Created Successfully !");
                IsBusy = false;
                Load();
                Notify("AllAccountDetails");
                Base.Current.RaiseVendorDetailUpdated();
            }
        }

        void service_UpdateAccountCompleted(object sender, UpdateAccountCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                Base.ShowError(e.Error);
                IsBusy = false;
            }
            else
            {
                SelectedAccount = e.Result;
                MessageBox.Show("Updation Successful !");
                Load();
                Notify("SelectedAccount");
                Base.Current.RaiseVendorDetailUpdated();
            }
        }

        void service_GetAccountsbyForIDCompleted(object sender, GetAccountsbyForIDCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                Base.ShowError(e.Error);
                IsBusy = false;
            }
            else
            {
                AllAccountDetails = e.Result;
                IsBusy = false;
                Notify("AllAccountDetails");
            }
        }

        #region Models

        ObservableCollection<MAccounts> _AllAccountDetails = new ObservableCollection<MAccounts>();
        public ObservableCollection<MAccounts> AllAccountDetails
        {
            get { return _AllAccountDetails; }
            set
            {
                _AllAccountDetails = value;
                Notify("AllAccountDetails");
                if (value.Count > 0)
                {
                    SelectedAccount = value.First();
                }
            }
        }

        MAccounts _SelectedAccount = new MAccounts();
        public MAccounts SelectedAccount
        {
            get { return _SelectedAccount; }
            set
            {
                _SelectedAccount = value;
                Notify("SelectedAccount");
            }
        }

        #endregion

        string _For;
        public string For
        {
            get { return _For; }
            set
            {
                _For = value;
                Load();
                Notify("For");
            }
        }

        int _ForID;
        public int ForID
        {
            get { return _ForID; }
            set
            {
                _ForID = value;
                Load();
                Notify("ForID");
            }
        }

        public void Load()
        {
            AllAccountDetails.Clear();
            if (String.IsNullOrWhiteSpace(For)) return;
            if (ForID == 0) return;
            service.GetAccountsbyForIDAsync(ForID, For);
            IsBusy = true;
        }

        public void UpdateAccount()
        {
            service.UpdateAccountAsync(SelectedAccount);
            IsBusy = true;
        }

        public void CreateAccount()
        {
            service.CreateNewAccountAsync(SelectedAccount);
            IsBusy = true;
        }
    }
}