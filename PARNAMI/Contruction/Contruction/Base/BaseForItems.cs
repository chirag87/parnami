﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Collections.Generic;
using Innosols.Web.Services;
using Contruction;
using System.Collections.ObjectModel;
using Contruction.MasterDBService;

namespace Contruction
{
    public partial class Base : ViewModel
    {
        ItemsContext _ItemsDB;
        public ItemsContext ItemsDB
        {
            get { return _ItemsDB; }
        }

        //IEnumerable<Item> _Items = new List<Item>();
        //public ObservableCollection<Item> Items
        //{
        //    get
        //    {
        //        if (_Items.Count() == 0 && ItemsDB != null)
        //        {
        //            _Items = ItemsDB.Items.ToList();
        //        }
        //        return _Items;
        //    }
        //    set
        //    {
        //        _Items = value;
        //        Notify("Items");
        //    }
        //}

        void OnCreatedForItems()
        {
            _ItemsDB = new ItemsContext();
            //ItemsDB.PropertyChanged += new System.ComponentModel.PropertyChangedEventHandler(ItemsDB_PropertyChanged);
            InitForItems();
        }

        void ItemsDB_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "IsLoading" || e.PropertyName == "IsSubmitting")
                Notify("IsItemsDBBusy");
        }

        public bool IsItemsDBBusy
        {
            //get { return ItemsDB.IsLoading || ItemsDB.IsSubmitting; }
            get { return false; }
        }

        public event EventHandler ItemsLoaded;
        public void RaiseItemsLoaded()
        {
            if (ItemsLoaded != null)
                ItemsLoaded(this, new EventArgs());
        }

        public void InitForItems()
        {
            //LoadAllOprs(0);
            //LoadMeasurementUnits();
            //LoadDealTypes();
            //LoadItemsCategories();
            //LoadItems();
            //LoadVendors();
            //LoadTransporters();
        }

        //public LoadOperation CurrentLoadOprForItems { get; set; }
        //public int TotalLoadOprForItems = 6;
        //public LoadOperation LoadOprForItems(int ind)
        //{
            //if (ind == 0)
            //{
            //    Status = "Loading Measurement Units ...";
            //    return ItemsDB.Load(ItemsDB.GetMeasurementUnitsQuery());
            //}
            //if (ind == 1)
            //{
            //    Status = "Loading Deal Types ...";
            //    return ItemsDB.Load(ItemsDB.GetDealTypesQuery());
            //}
            //if (ind == 2)
            //{
            //    Status = "Loading Item Categories ...";
            //    return ItemsDB.Load(ItemsDB.GetItemCategoriesQuery());
            //}
            //if (ind == 3)
            //{
            //    Status = "Loading Items ...";
            //    return ItemsDB.Load(ItemsDB.GetItemsQuery());
            //}
            //if (ind == 4)
            //{
            //    Status = "Loading Vendors ...";
            //    return ItemsDB.Load(ItemsDB.GetVendorsQuery());
            //}
            //if (ind == 5)
            //{
            //    Status = "Loading Transporters ...";
            //    return ItemsDB.Load(ItemsDB.GetTransportersQuery());
            //}
        //    return null;
        //}

        //public void LoadAllOprs(int? i)
        //{
        //    i = i ?? 0;
        //    CurrentLoadOprForItems = LoadOprForItems(i.Value);
        //    CurrentLoadOprForItems.Completed += (s, e) =>
        //    {
        //        if (!CurrentLoadOprForItems.HasError)
        //        {
        //            Status = "";
        //            if (i.Value < TotalLoadOprForItems - 1)
        //            {
        //                LoadAllOprs(i + 1);
        //            }
        //        }
        //        else
        //        {
        //            Status = CurrentLoadOprForItems.Error.Message;
        //        }
        //    };
        //}

        //public void LoadTransporters()
        //{
        //    //LoadOperation opr = ItemsDB.Load<Transporter>(ItemsDB.GetTransportersQuery());
        //}

        public void LoadMeasurementUnits()
        {
            //ItemsDB.Load<MeasurementUnit>(ItemsDB.GetMeasurementUnitsQuery());
        }

        public void LoadDealTypes()
        {
            //ItemsDB.Load<DealType>(ItemsDB.GetDealTypesQuery());
        }

        //public void LoadItems()
        //{
        //    //var opr = ItemsDB.Load<Item>(ItemsDB.GetItemsQuery());
        //    //opr.Completed += (s, e) =>
        //    //{
        //    //    Items = opr.Entities;
        //    //};
        //}

        public void LoadItemsCategories()
        {
            //ItemsDB.Load<ItemCategory>(ItemsDB.GetItemCategoriesQuery());
        }

        //public IEnumerable<ItemCategory> GeneralItems
        //{
        //    get
        //    {
        //        return ItemsDB.ItemCategories.Where(x => x.IsItem);
        //    }
        //}

        //public IEnumerable<ItemCategory> GeneralCategories
        //{
        //    get
        //    {
        //        return ItemsDB.ItemCategories.Where(x => !x.IsItem);
        //    }
        //}

        //public void LoadVendors()
        //{
        //    //ItemsDB.Load<Vendor>(ItemsDB.GetVendorsQuery().Where(x => x.IsActive));
        //}

        //public SubmitOperation AddVendor(/*Vendor vendor*/)
        //{
            //ItemsDB.Vendors.Add(vendor);
            //var opr = ItemsDB.SubmitChanges();
            //opr.Completed += (s, e) =>
            //{
            //    if (opr.HasError)
            //        ItemsDB.RejectChanges();
            //};
            //return opr;
            //return null;
        //}

        //public SubmitOperation AddNewItem(int catID, string name, string mu)
        //{
            //var cat = ItemsDB.ItemCategories.Single(x => x.ID == catID);
            //cat.HasVariations = true;
            //cat.Items.Add(new Item()
            //{
            //    Name = name,
            //    MeasurementID = mu,
            //    DealTypeID = "PerDeal"
            //});
            //var opr = ItemsDB.SubmitChanges();
            //opr.Completed += (s, e) =>
            //{
            //    if (opr.HasError)
            //    {
            //        ItemsDB.RejectChanges();
            //    }
            //};
            //return opr;
        //    return null;
        //}

        //public SubmitOperation AddNewCat(string catName, string name, string mu)
        //{
        //    ItemCategory cat = new ItemCategory()
        //    {
        //        Name = catName,
        //        IsItem = true,
        //        HasVariations = true,
        //        ItemType = "BuildingMaterial"
        //    };
        //    cat.Items.Add(new Item()
        //    {
        //        Name = name,
        //        MeasurementID = mu,
        //        DealTypeID = "PerDeal"
        //    });
        //    ItemsDB.ItemCategories.Add(cat);
        //    var opr = ItemsDB.SubmitChanges();
        //    opr.Completed += (s, e) =>
        //    {
        //        if (opr.HasError)
        //        {
        //            ItemsDB.RejectChanges();
        //        }
        //    };
        //    return opr;
        //    return null;
        //}
    }
}

namespace Innosols.Web.Services
{
    public partial class ItemsContext
    {
        //partial void OnCreated()
        //{
        //    WcfTimeoutUtility.ChangeWcfSendTimeout(this, 5);
        //}

        //public override LoadOperation Load(EntityQuery query, LoadBehavior loadBehavior, Action<LoadOperation> callback, object userState)
        //{
        //    //var opr = base.Load(query, loadBehavior, callback, userState);
        //    //opr.Completed += (s, e) =>
        //    //{
        //    //    Base.Current.RaiseItemsLoaded();
        //    //};
        //    //return opr;
        //    return null;
        //}
    }
}
