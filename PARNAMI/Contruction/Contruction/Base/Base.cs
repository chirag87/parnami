﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Collections.Generic;
using System.ComponentModel;

namespace Contruction
{
    public partial class Base : ViewModel
    {
        public Base()
        {
            if(!DesignerProperties.GetIsInDesignMode(Application.Current.RootVisual))
                Init();
            //if (IsRealTime)
            //{
            //    OnCreatedForItems();
            //    OnCreatedForPO();
            //    OnCreatedForInventory();
            //    InitSettings();
            //}
        }

        public void Init()
        {
            //InitForItems();
            //InitForBUs();
            //InitForPOs();
            OnInitForMessaging();
            OnInitForAppSettings();
            OnInitForMasterDB();
            OnInitNotificationModel();
        }

        public static Base Current
        {
            get { return (Base)Application.Current.Resources["base"]; }
        }

        public static AppEvents AppEvents
        {
            get { return AppEvents.Current; }
        }

        public static void ShowError(Exception ex)
        {
            ErrorWindow err = new ErrorWindow();
            err.tblkIntroductoryText.Text = "Request not found ! Please check the Internet connection or Retry !";
            err.LabelText.Text = ex.Message;
            err.ErrorTextBox.Text = ex.ToString();
            err.Show();
            //MessageBox.Show(ex.ToString());
        }

        string _Status;
        public string Status
        {
            get
            {
                return _Status;
            }
            set
            {
                _Status = value;
                Notify("Status");
                Notify("HasStatus");
            }
        }

        public bool HasStatus { get { return !String.IsNullOrWhiteSpace(Status); } }

        void InitSettings()
        {
            //var opr1 = settingscontext.LoadAppSettings();
            //opr1.Completed += (s, e) =>
            //{
            //    Settings = opr1.Value;
            //};

            //var opr = settingscontext.GetClientName();
            //opr.Completed += (s, e) => { ClientName = opr.Value; };
            //var opr2 = settingscontext.ShowClientLogo();
            //opr2.Completed += (s, e) => { ShowClinetLogo = opr2.Value; };
        }
    }
}
