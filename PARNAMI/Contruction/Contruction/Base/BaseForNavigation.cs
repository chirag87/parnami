﻿using System;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Windows.Interactivity;
using System.Collections.Generic;


namespace Contruction
{
    public class BaseForNavigation
    {

    }

    public partial class Base
    {
        /// <summary>
        /// Updated on Feb 11,11
        /// </summary>
        void RegisterAll()
        {
            AddNew<Home>("Home");
            AddNew<LoginPage>("LoginPage","Login");
            //AddNew<Test>("About", "About Us");
            AddNew<Feedback>("Feedback", "Feedback");

            AddNew<NewVendor>("NewVendor", "New Vendor");
            AddNew<ShowVendors>("ShowVendors", "Vendors");
            AddNew<VendorDetailView>("VendorDetailView", "Vendor Detailed View");

            AddNew<ViewPRs>("ViewPRs", "Purchase Requisitions");
            AddNew<NewPR>("NewPR", "New Purchase Requisition");
            AddNew<ChangePassword>("ChangePassword", "Change Password");
            AddNew<POView>("POView", "Purchase Orders");
            AddNew<PODetailedView>("PODetailedView");

            AddNew<NewReceiving>("NewReceiving", "New Receiving");
            AddNew<AllReceivings>("AllReceivings", "Goods Receipt Notes (GRNs)");
            AddNew<NewCashPurchase>("NewCashPurchase", "New Cash Purchase");
            AddNew<AllCashPurchases>("AllCashPurchases", "Cash Purchases");
            AddNew<NewDirectPurchase>("NewDirectPurchase", "New Direct Purchase");
            AddNew<ViewDirectPurchases>("ViewDirectPurchases", "Direct Purchases");
            AddNew<AllConsumptions>("AllConsumptions", "Consumptions");
            AddNew<NewConsumption>("NewConsumption", "New Consumption");
            AddNew<AllDirectGRNs>("AllDirectGRNs", "Receivings");

            //AddNew<AllMTRs>("AllMTRs", "Material Transerfer Requsitions");
            //AddNew<MTRView>("MTRView", "Material Transfer Request");

            AddNew<AllPricings>("AllPricings", "Pricing");
            AddNew<AddNewPricing>("AddNewPricing", "New Pricing");
            AddNew<NewPricing>("NewPricing", "New Pricing");

            AddNew<AllBillings>("AllBillings", "Bills");
            AddNew<BillingDetail>("BillingDetail", "Billing Details");
            AddNew<PurchaseInvoice>("PurchaseInvoice", "Purchase Invoice");

            AddNew<NewVendorPayment>("NewVendorPayment", "New Vendor Payment");
            AddNew<PendingBills>("PendingBills", "Pending Bills");
            AddNew<AllVendorPayments>("AllVendorPayments", "All Vendor Payments");
            AddNew<VendorDetailView>("VendorDetailView", "");

            AddNew<AllMOs>("AllMOs", "Move Order");
            AddNew<NewMO>("NewMO", "New Move Order");

            AddNew<AllMRs>("AllMRs", "Material Requisitions");
            AddNew<NewMR>("NewMR", "New Material Requisition");

            AddNew<ViewItems>("ViewItems", "Items");
            AddNew<NewItem>("NewItem", "New Item");
            AddNew<ItemDetails>("ItemDetails", "Item Details");
            AddNew<ItemCategory>("ItemCategory", "Item Category Master");
            AddNew<ItemSizeandBrandsIndex>("ItemSizeandBrandsIndex", "Item Sizes & Brands Master");

            AddNew<AllTransporters>("AllTransporters", "Transporters");
            AddNew<NewTransporter>("NewTransporter", "New Transporter");

            AddNew<MasterReportViewer>("ReportsMaster", "Reports");
            AddNew<ItemWiseReport>("ItemWiseReport");
            AddNew<MTR_Report>("MTR_Report", "MTR Report");
            AddNew<Cash_Report>("Cash_Report", "Cash Report");
            AddNew<DirectPurchase_Report>("DirectPurchase_Report", "Direct Purchase Report");

            AddNew<Innosols.UserManagement>("UserManagement", "Users' Management");

            AddNew<CompanyIndex>("CompanyIndex", "All Companies");
            
            AddNew<AllSites>("AllSites", "All Sites");
            AddNew<SiteDetailedView>("SiteDetailedView", "Site Details");
            AddNew<SitesandClients>("SitesandClients", "All Sites & Clients");

            AddNew<SafetyStock>("SafetyStock", "Stock Alert");

            AddNew<ItemWiseReporting>("ItemWiseReporting", "Item Wise Report");
            AddNew<SiteWiseReporting>("SiteWiseReporting", "Site Wise Report");
            AddNew<VendorWiseReporting>("VendorWiseReporting", "Vendor Wise Report");
            AddNew<ManageAllowedSites>("ManageAllowedSites", "Manage Site Wise Allocations");

            AddNew<AllTenders>("AllTenders", "Tenders");
            AddNew<NewTender>("NewTender","New Tender");
            AddNew<TenderDetailView>("TenderDetailView","Tender Detail View");

            AddNew<MBook>("MBook","Measurement Book");
            AddNew<StockAdjustments>("StockAdjustments", "Stock Adjustment", "Create New Stock Adjustment");
            DefaultPage = "Home";

            AddNew<Inbox>("Inbox", "Inbox");
            AddNew<NewMessage>("NewMessage", "New Message");
        }

        public void AtLoginChanged()
        {
            if (Base.Current._IsLoggedIn)
            {
                Base.Redirect(DefaultPage);
            }
            else
            {
                Base.Redirect("LoginPage");
            }
        }

        public void InitForNavigation(Border ContentBorder)
        {
            ContentVisual = ContentBorder;
            RegisterAll();
            Base.Redirect("LoginPage");
            //WebContext.Current.Authentication.LoggedIn += new EventHandler<System.ServiceModel.DomainServices.Client.ApplicationServices.AuthenticationEventArgs>(Authentication_LoggedIn);
            //WebContext.Current.Authentication.LoggedOut += new EventHandler<System.ServiceModel.DomainServices.Client.ApplicationServices.AuthenticationEventArgs>(Authentication_LoggedOut);
            //Base.Redirect(new FileUpload());
        }

        void NotifyAllCans()
        {
            foreach (var p in this.GetType().GetProperties().Where(x => x.Name.StartsWith("Can")))
            {
                Notify(p.Name);
            }
        }

        //void Authentication_LoggedIn(object sender, System.ServiceModel.DomainServices.Client.ApplicationServices.AuthenticationEventArgs e)
        //{
        //    //LoadAllowedSiteIDs();
        //    NotifyAllCans();
        //}

        //void Authentication_LoggedOut(object sender, System.ServiceModel.DomainServices.Client.ApplicationServices.AuthenticationEventArgs e)
        //{
        //    //ClearAllowedSiteIDs();
        //    Redirect(DefaultPage);
        //    NotifyAllCans();
        //}

        public string DefaultPage = "Home";

        public Border ContentVisual { get; set; }
        public TextBlock MainTitleBlock = new TextBlock();

        public string MainTitle
        {
            get { return MainTitleBlock.Text; }
            set { MainTitleBlock.Text = value; }
        }

        public static void Redirect(UIElement Control)
        {
            Current.ContentVisual.Child = Control;
        }

        public static void Redirect(NavigationDetail _detail)
        {
            Current.ContentVisual.Child = _detail.Object;
            Current.MainTitle = _detail.MainTitle;
        }

        public static void Redirect(UIElement Control, string _MainTitle)
        {
            Current.ContentVisual.Child = Control;
            Current.MainTitle = _MainTitle;
        }

        public static void Redirect(UIElement Control, string _MainTitle, string _SubTitle)
        {
            Current.ContentVisual.Child = Control;
            Current.MainTitle = _MainTitle;
        }

        Dictionary<string, NavigationDetail> dic = new Dictionary<string, NavigationDetail>();

        public void GoBack()
        {
            //if (CanGoBack)
            //Redirect(BackPage);
        }

        public void AddNew<T>(string key)
        {
            if(!dic.ContainsKey(key))
                dic.Add(key, new NavigationDetail() { type = typeof(T), MainTitle = key });
        }

        public void AddNew<T>(string key, string title)
        {
            if (!dic.ContainsKey(key))
                dic.Add(key, new NavigationDetail() { type = typeof(T), MainTitle = title });
        }

        public void AddNew<T>(string key, string title, string subtitle)
        {
            if (!dic.ContainsKey(key))
                dic.Add(key, new NavigationDetail() { type = typeof(T), MainTitle = title, SubTitle = subtitle });
        }

        public bool CanGoBack
        {
            get { return String.IsNullOrWhiteSpace(BackPage); }
        }

        public string BackPage { get; set; }
        public string CurrentPage { get; set; }

        public static void Redirect(string page)
        {
            if (String.IsNullOrWhiteSpace(page))
                return;
            Current.BackPage = Current.CurrentPage;
            Current.CurrentPage = page;
            NavigationDetail detail;
            try
            {
                detail = Current.dic[page];
            }
            catch
            {
                if (page.Equals("Back", StringComparison.OrdinalIgnoreCase))
                    Base.Current.GoBack();
                if (!page.Equals("PageNotFound", StringComparison.OrdinalIgnoreCase))
                    Redirect("PageNotFound");
                return;
            }
            try
            {
                if (detail.Object == null) detail.Object = New(detail.type);
                Redirect(detail);
            }
            catch (Exception ex)
            {
               // ErrorWindow.(ex);
            }
        }


        public static UIElement New(Type t)
        {
            try
            {
                return (UIElement)t.GetConstructor(new Type[] { }).Invoke(new object[] { });
            }
            catch
            {
                return null;
            }
        }

    }

    public class NavigationDetail
    {
        public Type type { get; set; }
        public UIElement Object { get; set; }
        public string MainTitle { get; set; }
        public string SubTitle { get; set; }
    }

    public class NavigationLink : HyperlinkButton
    {
        public NavigationLink()
        {
            try
            {
                this.Foreground = (Brush)App.Current.Resources["HyperLinkForeground"];
            }
            catch { }
            this.Click += new RoutedEventHandler(NavigationLink_Click);
        }

        void NavigationLink_Click(object sender, RoutedEventArgs e)
        {
            //Base.Redirect(TargetName);
        }
    }

    //
    // If you want your Action to target elements other than its parent, extend your class
    // from TargetedTriggerAction instead of from TriggerAction
    //
    public class NavigationAction : TriggerAction<DependencyObject>
    {
        public NavigationAction()
        {
            // Insert code required on object creation below this point.
        }

        public string Target
        {
            get { return (string)GetValue(TargetProperty); }
            set { SetValue(TargetProperty, value); }
        }
        public static DependencyProperty TargetProperty = DependencyProperty.Register
        ("Target", typeof(string), typeof(NavigationAction), new PropertyMetadata(""));

        protected override void Invoke(object o)
        {
            // Insert code that defines what the Action will do when triggered/invoked.
            Base.Redirect(Target);
        }
    }
}