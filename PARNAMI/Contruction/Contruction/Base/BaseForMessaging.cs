﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Contruction.MessagingService;
using System.Collections.ObjectModel;
using SIMS.MM.MODEL;

namespace Contruction
{
    public partial class Base
    {
        MessagingServiceClient msgservice = new MessagingServiceClient();
        UserManagementModel um = new UserManagementModel();

        #region Events...

        public event EventHandler MessegesDataChanged;
        public void RaiseMessegesDataChanged()
        {
            if (MessegesDataChanged != null)
                MessegesDataChanged(this, new EventArgs());
        }

        #endregion

        public void OnInitForMessaging()
        {
            msgservice.GetAllEmailsCompleted += new EventHandler<GetAllEmailsCompletedEventArgs>(msgservice_GetAllEmailsCompleted);
            msgservice.GetAllMessagesCompleted += new EventHandler<GetAllMessagesCompletedEventArgs>(msgservice_GetAllMessagesCompleted);
            msgservice.GetAllMessageHeadersCompleted += new EventHandler<GetAllMessageHeadersCompletedEventArgs>(msgservice_GetAllMessageHeadersCompleted);
            msgservice.GetMessageByIDCompleted += new EventHandler<GetMessageByIDCompletedEventArgs>(msgservice_GetMessageByIDCompleted);
            msgservice.SetIsFlagCompleted += new EventHandler<SetIsFlagCompletedEventArgs>(msgservice_SetIsFlagCompleted);
            msgservice.SetIsStarCompleted += new EventHandler<SetIsStarCompletedEventArgs>(msgservice_SetIsStarCompleted);
            msgservice.SetIsReadCompleted += new EventHandler<SetIsReadCompletedEventArgs>(msgservice_SetIsReadCompleted);
            msgservice.DeleteMailCompleted += new EventHandler<System.ComponentModel.AsyncCompletedEventArgs>(msgservice_DeleteMailCompleted);
            LoadAllEmails();    //Loading Emails
            um.GetAll();        //Loading Users
        }

        #region CompletedEvents...

        void msgservice_GetAllEmailsCompleted(object sender, GetAllEmailsCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                ShowError(e.Error);
                IsBusy = false;
            }
            else
            {
                AllEmailAddresses = e.Result;
                foreach (var email in AllEmailAddresses)    //For ExtAutoCompleteBox as it accepts only String Type
                {
                    Emails.Add(email.EmailAddress);
                }
                IsBusy = false;
            }
        }

        void msgservice_GetAllMessagesCompleted(object sender, GetAllMessagesCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                ShowError(e.Error);
                IsBusy = false;
            }
            else
            {
                AllMessages = e.Result;
                TotalMailsCount = AllMessages.Count;
                IsBusy = false;
            }
        }

        void msgservice_GetAllMessageHeadersCompleted(object sender, GetAllMessageHeadersCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                ShowError(e.Error);
                IsBusy = false;
            }
            else
            {
                AllMessageHeaders = e.Result;
                TotalMailsCount = AllMessages.Count;
                IsBusy = false;
            }
        }

        void msgservice_GetMessageByIDCompleted(object sender, GetMessageByIDCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                ShowError(e.Error);
                IsBusy = false;
            }
            else
            {
                //SelectedMessage = e.Result;
                IsBusy = false;
            }
        }

        void msgservice_SetIsFlagCompleted(object sender, SetIsFlagCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                ShowError(e.Error);
            }
            else
            {
                IsFlag = e.Result;
            }
        }

        void msgservice_SetIsStarCompleted(object sender, SetIsStarCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                ShowError(e.Error);
            }
            else
            {
                IsStar = e.Result;
            }
        }

        void msgservice_SetIsReadCompleted(object sender, SetIsReadCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                ShowError(e.Error);
            }
            else
            {
                IsReadMsg = e.Result;
            }
        }

        void msgservice_DeleteMailCompleted(object sender, System.ComponentModel.AsyncCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                ShowError(e.Error);
            }
            else
            {
                MessageBox.Show("Deleted !");
                LoadAllMessages();
            }
        }

        #endregion


        #region Properties...

        ObservableCollection<MEmail> _AllEmailAddresses = new ObservableCollection<MEmail>();
        public ObservableCollection<MEmail> AllEmailAddresses
        {
            get { return _AllEmailAddresses; }
            set
            {
                _AllEmailAddresses = value;
                Notify("AllEmailAddresses");
            }
        }

        ObservableCollection<string> _Emails = new ObservableCollection<string>();
        public ObservableCollection<string> Emails
        {
            get { return _Emails; }
            set
            {
                _Emails = value;
                Notify("Emails");
            }
        }

        ObservableCollection<string> _UserNames = new ObservableCollection<string>();
        public ObservableCollection<string> UserNames
        {
            get { return _UserNames; }
            set
            {
                _UserNames = value;
                Notify("UserNames");
            }
        }

        PaginatedData<MMessaging> _AllMessages = new PaginatedData<MMessaging>();
        public PaginatedData<MMessaging> AllMessages
        {
            get { return _AllMessages; }
            set
            {
                _AllMessages = value;
                Notify("AllMessages");
            }
        }

        PaginatedData<MMessagingHeader> _AllMessageHeaders = new PaginatedData<MMessagingHeader>();
        public PaginatedData<MMessagingHeader> AllMessageHeaders
        {
            get { return _AllMessageHeaders; }
            set
            {
                _AllMessageHeaders = value;
                Notify("AllMessageHeaders");
            }
        }

        MMessaging _SelectedMessage = new MMessaging();
        public MMessaging SelectedMessage
        {
            get { return _SelectedMessage; }
            set
            {
                _SelectedMessage = value;
                Notify("SelectedMessage");
            }
        }

        int _PageIndex = 0;
        public int PageIndex
        {
            get { return _PageIndex; }
            set
            {
                _PageIndex = value;
                Notify("PageIndex");
                LoadAllMessages();
            }
        }

        string _SearchKey;
        public string SearchKey
        {
            get { return _SearchKey; }
            set
            {
                _SearchKey = value;
                Notify("SearchKey");
            }
        }

        bool _IsReadMsg = false;
        public bool IsReadMsg
        {
            get { return _IsReadMsg; }
            set
            {
                _IsReadMsg = value;
                Notify("IsReadMsg");
            }
        }

        bool _IsFlag = false;
        public bool IsFlag
        {
            get { return _IsFlag; }
            set
            {
                _IsFlag = value;
                Notify("IsFlag");
            }
        }

        bool _IsStar = false;
        public bool IsStar
        {
            get { return _IsStar; }
            set
            {
                _IsStar = value;
                Notify("IsStar");
            }
        }

        bool _IsDeleted = false;
        public bool IsDeleted
        {
            get { return _IsDeleted; }
            set
            {
                _IsDeleted = value;
                Notify("IsDeleted");
            }
        }

        int _TotalMailsCount = 0;
        public int TotalMailsCount
        {
            get { return _TotalMailsCount; }
            set
            {
                _TotalMailsCount = value;
                Notify("TotalMailsCount");
            }
        }

        #endregion


        #region Methods...

        public void LoadAllEmails()
        {
            msgservice.GetAllEmailsAsync();
            IsBusy = true;
        }

        public void LoadAllMessages()
        {
            msgservice.GetAllMessagesAsync(Base.Current.UserName, PageIndex, null, SearchKey);
            IsBusy = true;
        }

        public void LoadAllMessageHeaders()
        {
            msgservice.GetAllMessageHeadersAsync(Base.Current.UserName, PageIndex, null, SearchKey);
            IsBusy = true;
        }

        public void GetMessage(int id)
        {
            msgservice.GetMessageByIDAsync(id);
            IsBusy = true;
        }

        public void SetIsStar(int msgid)
        {
            msgservice.SetIsStarAsync(msgid, Base.Current.UserName, IsStar);
        }

        public void SetIsFlag(int msgid)
        {
            msgservice.SetIsFlagAsync(msgid, Base.Current.UserName, IsFlag);
        }

        public void SetIsRead(int msgid)
        {
            msgservice.SetIsReadAsync(msgid, Base.Current.UserName, IsReadMsg);
        }

        public void DeleteMail(int msgid)
        {
            msgservice.DeleteMailAsync(msgid, Base.Current.UserName);
        }

        #endregion
    }
}
