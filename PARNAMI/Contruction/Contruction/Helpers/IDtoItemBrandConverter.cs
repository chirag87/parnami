﻿using System;
using System.Net;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Windows.Data;
using SIMS.MM.MODEL;

namespace Contruction
{
    public class IDtoItemBrandConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            string defval = "NA";
            if (parameter != null)
                defval = parameter.ToString();
            int ID = 0;
            if (value == null) return defval;
            if (value is int)
            {
                ID = (int)value;
            }
            else if (value is string)
            {
                if (!Int32.TryParse(value.ToString(), out ID))
                {
                    return defval;
                }
            }
            else { return defval; }

            try
            {
                MItemBrand brand = Base.Current.ItemBrands.Single(x => x.ID == ID);
                return brand.BrandName;
            }
            catch { }
            return defval; ;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
