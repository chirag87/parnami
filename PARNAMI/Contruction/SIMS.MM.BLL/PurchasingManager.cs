﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SIMS.MM.BLL;
using System.Collections.ObjectModel;
using SIMS.MM.BLL.Services;
namespace SIMS.MM.BLL
{    
    using DAL;
    using MODEL;    
    public class PurchasingManager : IPurchasingManger
    {
        parnamidb1Entities db = new parnamidb1Entities();
        PurchasingDataProvider provider = new PurchasingDataProvider();

        PRDetail ConvertToPRDetail(MPRLine _mprLine)
        {
            PRDetail detail = new PRDetail()
            {
                ID = _mprLine.LineID,
                DeliveryDate = _mprLine.DeliveryDate,
                ItemID = _mprLine.ItemID,
                SizeID = _mprLine.SizeID,
                BrandID = _mprLine.BrandID,
                PRID = _mprLine.PRID,
                Qty = _mprLine.Quantity,
                LinePrice = _mprLine.LinePrice,
                UnitPrice = _mprLine.UnitPrice,
                Remarks = _mprLine.LineRemarks,
                LastPrice = _mprLine.LastPrice,
                DiscountType = _mprLine.DiscountType,
                Discount = _mprLine.Discount,
                TotalLinePrice = _mprLine.TotalLinePrice
            };
            return detail;
        }

        MPRLine ConvertMPRLinesToPRDetails(PRDetail _detail)
        {
            MPRLine mprline = new MPRLine()
            {
                //DeliveryDate=_detail.DeliveryDate,
                LineID = _detail.ID,
                ItemID = _detail.ItemID,
                SizeID = _detail.SizeID,
                BrandID = _detail.BrandID,
                PRID = _detail.PRID,
                Quantity = _detail.Qty,
                UnitPrice = (double)_detail.UnitPrice,
                LinePrice = (double)_detail.LinePrice,
                LastPrice = _detail.LastPrice,
                Discount = _detail.Discount,
                DiscountType = _detail.DiscountType,
                TotalLinePrice = _detail.TotalLinePrice
            };
            return mprline;
        }

        PR ConvertToPR(MPR _mpr)
        {
            PR pr = new PR()
            {
                ID = _mpr.PRNumber,
                CurrentOwnerID = _mpr.CurrentOwnerID,
                //CostCenterID=_mpr.
                VendorID = _mpr.VendorID,
                ApprovedBy = _mpr.ApprovedBy,
                Frieght = _mpr.Frieght,
                SubTotal = _mpr.TotalValue,
                TotalPrice = _mpr.TotalValue + _mpr.TAX,
                Total = _mpr.GrandTotal,
                PCST = _mpr.pCST,
                PVat = _mpr.pVAT,
                Tax = _mpr.TAX,
                //TotalValue = (decimal)_mpr.TotalValue,
                IsApproved = _mpr.IsApproved,
                SiteID = _mpr.SiteID,
                PurchasedForUserID = _mpr.Purchaser,
                PurchaseTypeID = _mpr.PurchaseType,
                Remarks = _mpr.PRRemarks,
                RaisedBy = _mpr.RaisedBy,
                RaisedOn = _mpr.RaisedOn,
                ConvertedTOPO = _mpr.ConvertedToPO,
                ContactPersonName = _mpr.ContactPerson,
                ContactPersonPhone = _mpr.ContactMobile,
                PaymentTerms = _mpr.PaymentTerms,
                TotalDiscount = _mpr.TotalDiscount,
                NetAmount = _mpr.NetAmount
                // SiteCode=_mpr
            };
            if (_mpr.PRLines != null)
            {
                _mpr.PRLines
                    .ToList()
                    .ForEach(x => pr.PRDetails.Add(ConvertToPRDetail(x)));
            }
            return pr;
        }

        MPR ConvertPRToMPR(PR _pr)
        {
            MPR mpr = new MPR()
            {
                PRNumber = _pr.ID,
                VendorID = _pr.VendorID,
                //TotalValue = (double)_pr.TotalValue,
                IsApproved = _pr.IsApproved,
                SiteID = _pr.SiteID,
                Purchaser = _pr.PurchasedForUserID,
                PurchaseType = _pr.PurchaseTypeID,
                PRRemarks = _pr.Remarks,
                PaymentTerms = _pr.PaymentTerms,
                pCST = _pr.PCST,
                pVAT = _pr.PVat,
                TotalDiscount = _pr.TotalDiscount,
                NetAmount = _pr.NetAmount
            };
            return mpr;
        }

        public MPR CreatePR(MPR NewPR)
        {
            var year= DateTime.UtcNow.AddHours(5.5).Year;
            //NewPR.PRNumber = db.GetMaxPRID() + 1;              
            NewPR.UpdateLineNumbers();
            var pr = ConvertToPR(NewPR);
            pr.RaisedOn = DateTime.UtcNow.AddHours(5.5);
            pr.Year = year;
            pr.SiteID = NewPR.SiteID;
            pr.SiteCode = db.GetSiteCode(NewPR.SiteID);
            pr.RID = db.GetMaxPRsRefID(pr.SiteCode, pr.Year) + 1;
            pr.ReferenceNo = "PR/" + pr.Year + "/S-" + pr.SiteCode + "/" + pr.RID;
            pr.CurrentOwnerID = "dba";
            db.UpdateLastPrices(pr);
            db.PRs.AddObject(pr);
            db.SaveChanges();
            return provider.GetPRbyId(pr.ID);
        }

        public MPR SplitPR(MPR NewPR, int OldPRID, ObservableCollection<int> OldPRLines)
        {
            var year = DateTime.UtcNow.AddHours(5.5).Year;
            NewPR.UpdateLineNumbers();
            var pr = ConvertToPR(NewPR);
            pr.RaisedOn = DateTime.UtcNow.AddHours(5.5);
            pr.Year = year;
            pr.SiteID = NewPR.SiteID;
            pr.SiteCode = db.GetSiteCode(NewPR.SiteID);
            pr.RID = db.GetMaxPRsRefID(pr.SiteCode, pr.Year) + 1;
            pr.ReferenceNo = "PR/" + pr.Year + "/S-" + pr.SiteCode + "/" + pr.RID;
            pr.CurrentOwnerID = "dba";
            db.UpdateLastPrices(pr);
            db.PRs.AddObject(pr);
            db.SaveChanges();
            foreach( var LineID in OldPRLines)
            {
                var data = db.PRDetails.Single(x => x.PRID == OldPRID && x.ID == LineID);
                if (data != null)
                {
                    db.PRDetails.DeleteObject(data);
                    db.SaveChanges();
                }
            }
            return provider.GetPRbyId(pr.ID);
        }

        public MPR UpdatePR(MPR UPR)
        {
            var pr = db.PRs.Single(x=> x.ID == UPR.PRNumber);
            pr.VendorID = UPR.VendorID;
            pr.PVat = UPR.pVAT;
            pr.PCST = UPR.pCST;
            pr.Frieght = UPR.Frieght;
            pr.Remarks = UPR.PRRemarks;
            pr.Tax = UPR.TAX;
            pr.ContactPersonName = UPR.ContactPerson;
            pr.ContactPersonPhone = UPR.ContactMobile;
            pr.PaymentTerms = UPR.PaymentTerms;

            pr.SubTotal=UPR.TotalValue;
            pr.TotalPrice = UPR.TotalValue+UPR.TAX;
            pr.Total = UPR.GrandTotal;
            pr.TotalDiscount = UPR.TotalDiscount;
            pr.NetAmount = UPR.NetAmount;
           
            foreach (var line in UPR.PRLines)
            {
                var prline = pr.PRDetails.Single(x => x.ID == line.LineID);
                prline.Qty = line.Quantity;
                prline.UnitPrice = line.UnitPrice;
                prline.LinePrice = line.LinePrice;
                prline.ItemID = line.ItemID;
                prline.SizeID = line.SizeID;
                prline.BrandID = line.BrandID;
                prline.Remarks = line.LineRemarks;
                prline.Discount = line.Discount;
                prline.DiscountType = line.DiscountType;
                prline.TotalLinePrice = line.TotalLinePrice;
            }
            db.SaveChanges();

            //var obj = ConvertToPR(UPR);
            //db.PRs.Attach(obj);
            //db.ObjectStateManager.ChangeObjectState(obj, System.Data.EntityState.Modified);
            
            //db.UpdateLastPrices(obj,true);
            //db.SaveChanges();
            return UPR;
        }

        public void DeletePR(int prid)
        {
            var row = db.PRs.Single(x => x.ID == prid);
            db.PRs.DeleteObject(row);
            var v = db.PRDetails.Single(x=>x.PRID==prid);
            db.PRDetails.DeleteObject(v);
            db.SaveChanges();

        }             

        public void EmailPR(int PRID)
        {
            var emailid = Services.AppSettingsService.GetLatestAppSettings().Email;
            var pr = db.PRs.Single(x => x.ID == PRID);
            //if (!pr.IsApproved)
            //    throw new Exception("PR Not Approved! Operation not Allowed !");
            try
            {
                Emailer.Emailer.SMSPO(emailid, pr, !pr.IsApproved);
            }
            catch { }
            Emailer.Emailer.EmailPO(emailid, pr, !pr.IsApproved);
        }

        public string ConfirmEmail(int POID)
        {
            var pr = db.PRs.Single(x => x.POID == POID);
            string email = pr.Vendor.Email;
            return email;
        }

        public void EmailtoVendor(int POID)
        {
            var pr = db.PRs.Single(x => x.POID == POID);
            var emailid = pr.Vendor.Email;
            try
            {
                if (emailid == null)
                {
                    throw new Exception("This Vendor do not carry any Email Address!");
                }
                else
                {
                    Emailer.Emailer.SMSPO(emailid, pr, !pr.IsApproved);
                }
            }
            catch { }
            Emailer.Emailer.EmailPO(emailid, pr, !pr.IsApproved);
        }

        public void ApprovePR(int PRID, string ApprovedBy)
        {
            var obj = db.PRs.Single(x => x.ID == PRID);
            if (obj.IsApproved)
                throw new Exception("Already Approved!");            
            obj.IsApproved = true;
            obj.ApprovedBy = ApprovedBy;           
            db.SaveChanges();
        }

        public void AcknowledgePO(int poid,string remark)
        {
            var po = db.POs.Single(x => x.ID == poid);
            po.AcknowledgedOn = DateTime.UtcNow.AddHours(5.5);
            po.IsAcknowledged = true;
            po.Ack_Remarks = remark;
            db.SaveChanges();
        }

        public int ConvertToPO(int prid)
        {
            var pr = db.PRs.Single(x => x.ID == prid);
            if (!pr.IsApproved)
                throw new Exception("PR Not Approved!");
            POs po = new POs();
            po.ID = db.GetMaxPOID() + 1;
            po.Year = pr.Year;
            po.SiteID = pr.SiteCode;
            po.RID = db.GetMaxPORID(po.SiteID,po.Year) + 1;
            po.Display = "PO/" + po.Year + "/S-" + pr.SiteCode+ "/" + po.RID;
            po.IsAcknowledged = false;
            po.Tax = pr.Tax;
            po.Frieght = (double)pr.Frieght;
            po.Total = (double)pr.Total;
            po.NetTotal = pr.SubTotal;
            po.Remarks = pr.Remarks;            
            po.CreatedOn = DateTime.UtcNow.AddHours(5.5);
            po.CreatedBy = "dba";
            po.ContactPersonName = pr.ContactPersonName;
            po.ContactPersonPhone = pr.ContactPersonPhone;
            po.TotalDiscount = pr.TotalDiscount;
            po.NetAmount = pr.NetAmount;
           // po.Payment=pr.
           // po.Transportation
            pr.ConvertedTOPO = true;
            pr.POs = po;
            db.SaveChanges();
            return po.ID;
        }

        public IEnumerable<PendingPOInfo> GetPendingInfo(int vendorid, int siteid, int itemid)
        {
            var data = db.PRDetails.AsQueryable();
            var data2 = data.Where(x => x.PR.VendorID == vendorid && x.PR.SiteID == siteid 
                && x.PR.POID != null
                && x.ItemID == itemid
                && x.Qty > x.TotalReceivedQty
                )
                .ToList();
            var list = new List<PendingPOInfo>();
            if (data2.Count != 0)
            {
                foreach (var item in data2)
                {
                    var info = new PendingPOInfo()
                    {
                        PRID = item.PRID,
                        ItemID = item.ItemID,
                        SizeID = item.SizeID,
                        BrandID = item.BrandID,
                        PRLineID = item.ID,
                        POID = item.PR.POID.Value,
                        OrderedQty = item.Qty,
                        UnitPrice = item.UnitPrice,
                        LinePrice = item.LinePrice
                    };
                    if (item.TotalReceivedQty.HasValue)
                    {
                        info.EarlierReceivedQty = item.TotalReceivedQty.Value;
                    }
                    list.Add(info);
                }
            }
            return list;
        }

        public IEnumerable<int> GetAllowedItemId(int vendorid, int siteid)
        {
            var data = db.PRDetails;
            var data2 = data.Where(x => x.PR.VendorID == vendorid 
                && x.PR.SiteID == siteid && x.PR.POID!= null
                && x.Qty > x.TotalReceivedQty
                )
                .Select(x => x.ItemID).Distinct()
                .ToList();
            return data2;
        }
    }

    public static class MPRExtentions
    {
        public static MPRLine AddNewLine(this MPR mpr)
        {
            if (mpr.PRLines == null)
                mpr.PRLines = new ObservableCollection<MPRLine>();
            var line = new MPRLine();
            mpr.PRLines.Add(line);
            mpr.UpdateLineNumbers();
            return line;
        }

        public static int UpdateLineNumbers(this MPR mpr)
        {
            if (mpr.PRLines == null) return 0;
            int i=1;
            foreach (var item in mpr.PRLines)
            {
                item.LineID = i;
                i++;
            }
            return i-1;
        }
    }
}

    
