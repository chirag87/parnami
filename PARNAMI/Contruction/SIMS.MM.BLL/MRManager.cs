﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SIMS.MM.BLL
{
    using MODEL;
    using DAL;
    public class MRManager:IMRManager
    {
        parnamidb1Entities db = new parnamidb1Entities();
        public MMR CreateNewMR(MMR _NewMR)
        {
            MRDataProvider pr = new MRDataProvider();
            var MR = ConvertToMR(_NewMR);            
            MR.ID = db.GetMax_MR_ID() + 1;
            MR.Year = DateTime.UtcNow.AddHours(5.5).Year;
            MR.SiteCode = db.GetSiteCode(MR.SiteID);
            MR.ReferenceNo = "MR/" + MR.Year + "/S-" + MR.SiteCode+ "/" + MR.ID;           
            db.MRs.AddObject(MR);
            db.SaveChanges();
            return pr.GetMRByID(MR.ID);
        }

        private MR ConvertToMR(MMR _NewMR)
        {
            MR _mr = new MR()
            {
                ID = _NewMR.ID,
                CurrentOwnerID = _NewMR.CurrentOwnerID,
                ConactPersonPhone = _NewMR.ContactPerson,
                IsApproved = _NewMR.IsApproved,
                ContactPersonName = _NewMR.ContactPerson,
                RaisedBy = _NewMR.RaisedBy,
                RaisedOn = _NewMR.RaisedOn,
                Remarks = _NewMR.Remarks,
                RequestedBy = _NewMR.RequestedBy,
                SiteID=_NewMR.SiteID                
            };
            if (_NewMR.MRLines != null)
                _NewMR.MRLines.ToList()
                    .ForEach(x=> _mr.MRLines.Add(ConvertToMRLIne(x)));
            db.SaveChanges();
            return _mr;   
        }

        private MRLine ConvertToMRLIne(MMRLine x)
        {
            MRLine line = new MRLine()
            {
                ID = x.LineID,
                ItemID = x.ItemID,
                SizeID = x.SizeID,
                BrandID = x.BrandID,
                Remarks = x.LineRemarks,
                //IssuedQty=x.IssuedQty,
                //ReturnedQty=x.ReturnedQty,
                MRID = x.MRID,
                Qty = x.Quantity,
                ConsumableType = x.ConsumableType
            };
            return line;
        }

        public int CreateMRIssue(MReceiving _mrec)
        {
            ReceivingManager rm=new ReceivingManager();
            _mrec.TransactionID = db.GetMaxMOId("Issue") + 1;
            _mrec.TransactionTypeID = "Issue";
            _mrec.CreatedBy = "dba";
            _mrec.LastUpdatedBy = "dba";
            _mrec.LastUpdaetdOn = DateTime.UtcNow.AddHours(5.5);
            _mrec.CreatedOn = DateTime.UtcNow.AddHours(5.5);
            var rec=rm.ConvertToINTR(_mrec);
            rec.Year = DateTime.UtcNow.AddHours(5.5).Year;
            rec.SiteID = db.GetSiteCode(rec.BUID);//???-------------
            rec.RID = db.GetMaxRRID("Issue", rec.Year, rec.BUID) + 1;
            rec.ReferenceNo = rec.TypeID + "/" + rec.Year + "/S-" + rec.SiteID + "/" + rec.RID;
            rec.TransitID = 1; 
            db.InventoryTransactions.AddObject(rec);
            db.SaveChanges();
            return _mrec.TransactionID;            
        }

        public int CreateMRReturn(MReceiving _mrec)
        {
            ReceivingManager rm = new ReceivingManager();
            _mrec.TransactionID = db.GetMaxMOId("Return") + 1;
            _mrec.TransactionTypeID = "Return";
            _mrec.CreatedBy = "dba";
            _mrec.LastUpdatedBy = "dba";
            _mrec.LastUpdaetdOn = DateTime.UtcNow.AddHours(5.5);
            _mrec.CreatedOn = DateTime.UtcNow.AddHours(5.5);
            var rec = rm.ConvertToINTR(_mrec);
            rec.Year = DateTime.UtcNow.AddHours(5.5).Year;
            rec.SiteID = db.GetSiteCode(rec.BUID);//???-------------
            rec.RID = db.GetMaxRRID("Return", rec.Year, rec.BUID) + 1;
            rec.ReferenceNo = rec.TypeID + "/" + rec.Year + "/S-" + rec.SiteID + "/" + rec.RID;
            rec.TransitID = 1;
            db.InventoryTransactions.AddObject(rec);
            db.SaveChanges();
            db.CloseMR_IFRequired(_mrec.MRID);
            return _mrec.TransactionID; 
        }

        public void CloseMR(int mrid)
        {
            var mr = db.MRs.Single(x => x.ID == mrid);
            mr.IsClosed = true;
            mr.ClosedBy = "dba";
            db.SaveChanges();
        }
    }
}
