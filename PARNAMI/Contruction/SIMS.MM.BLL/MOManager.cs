﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SIMS.MM.BLL
{
    using DAL;
    using MODEL;
    public class MOManager:IMOManager
    {
        parnamidb1Entities db = new parnamidb1Entities();
        MODataProvider provider = new MODataProvider();
        ReceivingDataProvider receivingDP = new ReceivingDataProvider();

        public MMO CreateNewMO(MMO newMO)
        {
            var qry = ConvertTOMO(newMO);
            qry.ID = db.GetMAX_MTR_ID() + 1;
            //qry.RaisedBy = "dba";
            qry.CurrentOwnerID = "dba";
            //qry.RaisedOn = DateTime.UtcNow.AddHours(5.5);            
            qry.Year = DateTime.UtcNow.AddHours(5.5).Year;
            qry.SiteID = db.GetSiteCode(qry.RequestedSiteID);//???
            //qry.RID = db.GetMaxRRID("Consumption", qry.Year, qry.BUID) + 1;
            qry.ReferenceNo ="MO/" + qry.Year + "/S-" + qry.SiteID + "/" + qry.ID;           
            db.MTRs.AddObject(qry);
            db.SaveChanges();
            return provider.GetMObyId(qry.ID);
        }

        public MMO SplitToMO(MMO NewMO, int OldPRID, System.Collections.ObjectModel.ObservableCollection<int> OldPRLines)
        {
            var qry = ConvertTOMO(NewMO);
            NewMO.UpdateLineNumbers();
            qry.ID = db.GetMAX_MTR_ID() + 1;
            qry.CurrentOwnerID = "dba";
            qry.Year = DateTime.UtcNow.AddHours(5.5).Year;
            qry.SiteID = db.GetSiteCode(qry.RequestedSiteID);//???
            //qry.RID = db.GetMaxRRID("Consumption", qry.Year, qry.BUID) + 1;
            qry.ReferenceNo = "MO/" + qry.Year + "/S-" + qry.SiteID + "/" + qry.ID;
            db.MTRs.AddObject(qry);
            db.SaveChanges();

            foreach (var LineID in OldPRLines)
            {
                var data = db.PRDetails.Single(x => x.PRID == OldPRID && x.ID == LineID);
                if (data != null)
                {
                    db.PRDetails.DeleteObject(data);
                    db.SaveChanges();
                }
            }

            return provider.GetMObyId(qry.ID);
        }

        MTR ConvertTOMO(MMO _mmo)
        {
            MTR mo = new MTR()
            {
                ID = _mmo.ID,
                Remarks = _mmo.Remarks,
                RequestedBy = _mmo.Requester,
                RequestedSiteID = _mmo.RequestingSiteID,
                TransferringSiteID = _mmo.TransferringSiteID,
                TransferBefore = _mmo.TransferDate,
                RaisedBy=_mmo.RaisedBy,    
                RaisedOn=_mmo.RaisedOn
            };
            if (_mmo.MOLines != null) _mmo.MOLines.ToList()
                  .ForEach(x => mo.MTRLines.Add(ConvertToMOLine(x)));
            db.SaveChanges();
            return mo;
        }

        MTRLine ConvertToMOLine(MMOLine x)
        {
            MTRLine line = new MTRLine()
            {
                LineID = x.LineID,
                ItemID = x.ItemID,               
               // InTransitQty=x.InTransitQty,
               // ReleasedQty=x.ReleasedQty,
               // TransferredQty=x.TransferredQty,
               SizeID = x.SizeID,
               BrandID = x.BrandID,
                Remarks=x.LineRemarks,
                RequiredQty=x.Quantity,
                MTRID=x.ID//?
            };
            return line;
        }

        //public void ReleaseMO(int moid)
        //{
        //   var data= db.MTRs.Single(x=>x.ID==moid);
        //   data.IsReleased = true;
        //   data.ReleasedOn = DateTime.UtcNow.AddHours(5.5);
        //   data.ReleasedBy = "dba";
        //   db.SaveChanges();
        //}

        public void ReceivedMO(int moid)
        {
            throw new NotImplementedException();
        }

        public string CreateMORelease(MReceiving mrec)
        {
            ReceivingManager rm = new ReceivingManager();
            mrec.TransactionID = db.GetMaxMOId("MORelease") + 1;
            mrec.TransactionTypeID = "MORelease";            
            mrec.CreatedBy = "dba";
            mrec.LastUpdatedBy = "dba";            
            mrec.LastUpdaetdOn = DateTime.UtcNow.AddHours(5.5);
            mrec.CreatedOn = DateTime.UtcNow.AddHours(5.5);
            var rec = rm.ConvertToINTR(mrec);
            //rec.CreatedBy = mrec.Purchaser;
            //rec.LastUpdatedBy = mrec.Purchaser;
            rec.Year = DateTime.UtcNow.AddHours(5.5).Year;
            rec.SiteID = db.GetSiteCode(rec.BUID);//???-------------
            rec.RID = db.GetMaxRRID("MORelease", rec.Year, rec.BUID) + 1;
            rec.ReferenceNo = rec.TypeID + "/" + rec.Year + "/S-" + rec.SiteID + "/" + rec.RID;
            rec.TransitID = 1;             
            db.InventoryTransactions.AddObject(rec);            
            db.SaveChanges();
            return rec.ReferenceNo;
        }

        public int CreateMOReceive(MReceiving mrec)
        {
            ReceivingManager rm = new ReceivingManager();
            mrec.TransactionID = db.GetMaxMOId("MOReceive") + 1;
            mrec.TransactionTypeID = "MOReceive";
            mrec.CreatedBy = "dba";            
            mrec.LastUpdatedBy = "dba";
            mrec.LastUpdaetdOn = DateTime.UtcNow.AddHours(5.5);
            mrec.CreatedOn = DateTime.UtcNow.AddHours(5.5);
            var rec = rm.ConvertToINTR(mrec);
            //rec.CreatedBy = mrec.Purchaser;
           // rec.LastUpdatedBy = mrec.Purchaser;
            rec.Year = DateTime.UtcNow.AddHours(5.5).Year;
            rec.SiteID = db.GetSiteCode(rec.BUID);//???-------------
            rec.RID = db.GetMaxRRID("MOReceive", rec.Year, rec.BUID) + 1;
            rec.ReferenceNo = rec.TypeID + "/" + rec.Year + "/S" + rec.SiteID + "/" + rec.RID;
            db.InventoryTransactions.AddObject(rec);
            rec.TransitID = 1;
            db.SaveChanges();
            db.CloseMO_IFRequired(mrec.MOID);
            return mrec.TransactionID;
        }

        public void CloseMO(int moid)
        {
            var data=db.MTRs.Single(x => x.ID == moid);           
            data.IsClosed = true;
            data.ClosedBy = "dba";
            db.SaveChanges();
        }
    }
}
