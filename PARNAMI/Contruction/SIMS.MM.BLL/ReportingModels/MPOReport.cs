﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace SIMS.MM.BLL.ReportingModels
{    
        [DataContract]
        public class MPOReport
        {
            [DataMember]
            public int SNo { get; set; }

            [DataMember]
            public DateTime OrderDate { get; set; }

            [DataMember]
            public int PRNO  { get; set; }

            [DataMember]
            public int PONO { get; set; }

            [DataMember]
            public string Vendor { get; set; }

            [DataMember]
            public string Site { get; set; }

            [DataMember]
            public string MaterialCategory { get; set; }

            [DataMember]
            public string Material { get; set; }

            [DataMember]
            public double Qty { get; set; }

            [DataMember]
            public string MU { get; set; }

            [DataMember]
            public double PerUnitPrice { get; set; }

            [DataMember]
            public double TotalAmount { get; set; }
      
    }
}
