﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SIMS.MM.BLL
{
    using BLL;
    using DAL;
    using MODEL;
    public class MasterManager : IMasterManager
    {
        parnamidb1Entities db = new parnamidb1Entities();
        MMReportsDBDataContext dc = new MMReportsDBDataContext();
        MasterDataProvier provider = new MasterDataProvier();

        public MVendor CreateNewVendor(MVendor newVendor)
        {
            var dt = ConvertToVendor(newVendor);
            dt.ID = db.GetMaxVID() + 1;
            db.Vendors.AddObject(dt);
            db.SaveChanges();
            return newVendor;
        }

        public MVendor UpdateVendor(MVendor updatedVendor)
        {
            var UV = ConvertToVendor(updatedVendor);
            db.Vendors.Attach(UV);
            db.ObjectStateManager.ChangeObjectState(UV, System.Data.EntityState.Modified);
            db.SaveChanges();
            return updatedVendor;
        }

        public void DeleteVendor(int VendorId)
        {
            var obj = db.Vendors.Single(x => x.ID == VendorId);
            db.Vendors.DeleteObject(obj);
            db.SaveChanges();
        }

        public MSite CreateNewSite(MSite newSite)
        {
            var qry = ConvertToBu(newSite);
            qry.ID = db.getmaxSiteID() + 1;
            db.Buses.AddObject(qry);
            db.SaveChanges();
            return newSite;
        }

        public MClient CreateNewClient(MClient newClient)
        {
            var qry = ConvertToClient(newClient);
            db.Clients.AddObject(qry);
            db.SaveChanges();
            return newClient;
        }

        public Client ConvertToClient(MClient newClient)
        {
            Client data = new Client()
            {
                ID = newClient.ID,
                AddressLine1 = newClient.Address1,
                AddressLine2 = newClient.Address2,
                City = newClient.City,
                Country = newClient.Country,
                Name = newClient.Name,
                PANNo = newClient.PANNo,
                Pincode = newClient.Pincode,
                ServiceTaxNo = newClient.ServiceTaxNo,
                State = newClient.State,
                TINNo = newClient.TINNo
            };
            return data;
        }

        public MSite UpdateSite(MSite updatedSite)
        {
            var updatedBU = ConvertToBu(updatedSite);
            db.Buses.Attach(updatedBU);
            db.ObjectStateManager.ChangeObjectState(updatedBU, System.Data.EntityState.Modified);
            db.SaveChanges();
            return updatedSite;
        }

        public MClient UpdateClient(MClient updatedClient)
        {
            var update = ConvertToClient(updatedClient);
            db.Clients.Attach(update);
            db.ObjectStateManager.ChangeObjectState(update, System.Data.EntityState.Modified);
            db.SaveChanges();
            return updatedClient;
        }

        public void DeleteSite(int siteID)
        {
            var obj = db.Buses.Single(x => x.ID == siteID);
            db.Buses.DeleteObject(obj);
            db.SaveChanges();
        }

        Vendor ConvertToVendor(MVendor _mv)
        {
            Vendor vr = new Vendor()
            {
                ID = _mv.ID,
                RegisteredAddress = _mv.Address,
                City = _mv.City,
                Country = _mv.Country,
                Phone = _mv.Phone,
                Logo = _mv.Logo,
                Website = _mv.Website,
                Fax = _mv.Fax,
                OwnerEmail = _mv.OwnerEmail,
                CompanyOwner = _mv.CompanyOwner,
                CompanyType = _mv.CompanyType,
                OwnerMobile = _mv.OwnerMobile,
                Rating = _mv.Rating,
                Remarks = _mv.Remarks,
                TermsandConditions = _mv.TermsandConditions,
                DefaultPaymentDays = _mv.DefaultPaymentDays,
                CST = _mv.CST,
                Email = _mv.Email,
                Name = _mv.Name,
                PanNo = _mv.PANNo,
                Pincode = _mv.PinCode,
                ServiceTaxNo = _mv.ServiceTaxNo,
                State = _mv.State,
                TINNo = _mv.TINNo,
                IsActive = _mv.IsActive,
                OwnerDesignation = _mv.OwnerDesignation
                //Address2=_mv.Address2,
                //City2=_mv.City2,
                //Pincode2=_mv.PinCode2,
                //State2=_mv.State2,
                //ContactMobile=_mv.ContactMobile,
                //ContactName=_mv.ContactName
            };
            return vr;
        }

        BUs ConvertToBu(MSite _site)
        {
            BUs bu = new BUs()
            {
                ID = _site.ID,
                CompanyID = _site.CompanyID,
                Address = _site.Address,
                City = _site.City,
                KeyContactName = _site.ContactPerson,
                Country = _site.Country,
                Name = _site.Name,
                Phone = _site.Phone,
                PinCode = _site.PinCode,
                State = _site.State,
                TypeID = _site.TypeID,
                KeyContactMobile1 = _site.KeyContactMobile1,
                KeyContactMobile2 = _site.KeyContactMobile2,
                KeyContactEmail = _site.KeyContactEmail,
                Code = _site.SiteCode,
                ClientID = _site.ClientID
            };
            return bu;
        }

        public void CreateNewMU(MMeasurementUnit newMU)
        {
            var qry = new MeasurementUnit()
            {
                Value = newMU.MU,
                DataType = "Double",//newMU.DataType,
                FormalName = newMU.FormalName,
                Type = newMU.Type,
                Variable = newMU.Variable,
            };
            db.MeasurementUnits.AddObject(qry);
            db.SaveChanges();
        }

        public MPriceMaster CreateNewPricing(MPriceMaster _mprice)
        {
            _mprice.ID = db.GetMAX_PriceMID() + 1;
            var qry = ConvertTOMasterPrice(_mprice);
            db.PriceMasters.AddObject(qry);
            db.SaveChanges();
            return _mprice;
        }

        public void CreateNewPricing(IEnumerable<MPriceMaster> _mprice)
        {
            _mprice.ToList()
                .ForEach(x => CreateNewPricing(_mprice));
        }

        private PriceMaster ConvertTOMasterPrice(MPriceMaster _mprice)
        {
            PriceMaster pm = new PriceMaster()
            {
                ID = _mprice.ID,
                VendorID = _mprice.VendorID,
                ItemID = _mprice.ItemID,
                SiteID = _mprice.SiteID,
                UM = _mprice.UM,
                CreatedBy = _mprice.CreatedBy,
                CreatedOn = _mprice.CreatedOn,
                EffectiveFrom = _mprice.EffectiveFrom,
                EffectiveTill = _mprice.EffectiveTill,
                QtyFor = _mprice.QtyFor,
                TotalPrice = _mprice.TotalPrice,
                UpdatedBy = _mprice.UpdatedBy,
                UpdatedOn = _mprice.UpdatedOn,
                UnitPrice = _mprice.UnitPrice
            };
            return pm;
        }

        public string NewPricing(MPriceMaster _mprice)
        {
            var Id = db.PriceMasters.Where(x => x.ItemID == _mprice.ItemID && x.VendorID == _mprice.VendorID && x.SiteID == _mprice.SiteID).Select(x => x.ID);
            if (Id.Count() == 0)
            {
                var data = ConvertToPricing(_mprice);
                db.PriceMasters.AddObject(data);
                db.SaveChanges();
                return "New Price Added !";
            }
            return "Record Already Exists !";
        }

        public PriceMaster ConvertToPricing(MPriceMaster _mprice)
        {
            PriceMaster master = new PriceMaster()
            {
                ID = db.GetMaxPriceID() + 1,
                ItemID = _mprice.ItemID,
                VendorID = _mprice.VendorID,
                SiteID = _mprice.SiteID,
                UM = _mprice.UM,
                CreatedBy = _mprice.CreatedBy,
                CreatedOn = _mprice.CreatedOn,
                EffectiveFrom = _mprice.EffectiveFrom,
                EffectiveTill = _mprice.EffectiveTill,
                QtyFor = _mprice.QtyFor,
                UnitPrice = _mprice.UnitPrice,
                TotalPrice = _mprice.TotalPrice,
                UpdatedBy = _mprice.UpdatedBy,
                UpdatedOn = _mprice.UpdatedOn
            };
            return master;
        }

        public void UpdatePricing(MPriceMaster _mprice)
        {
            dc.UpdatePricing(
                _mprice.VendorID,
                _mprice.SiteID,
                _mprice.ItemID,
                _mprice.UM,
                _mprice.UnitPrice,
                _mprice.TotalPrice,
                _mprice.QtyFor,
                _mprice.EffectiveFrom,
                _mprice.EffectiveTill,
                _mprice.CreatedBy,
                _mprice.CreatedOn,
                _mprice.UpdatedBy,
                _mprice.UpdatedOn);
        }

        public MTransporter CreateNewTransporter(MTransporter _NewTransporter)
        {
            //throw new NotImplementedException();
            var transporter = ConvertToTransporter(_NewTransporter);
            transporter.ID = db.GetMAX_TransporterID() + 1;
            db.Transporters.AddObject(transporter);
            db.SaveChanges();
            return _NewTransporter;
        }

        private Transporter ConvertToTransporter(MTransporter _NewTransporter)
        {
            Transporter transporter = new Transporter()
            {
                ID = _NewTransporter.ID,
                Name = _NewTransporter.Name,
                Address = _NewTransporter.Address,
                City = _NewTransporter.City,
                CST = _NewTransporter.CST,
                Email = _NewTransporter.Email,
                IsActive = _NewTransporter.IsActive,
                KeyContactEmail = _NewTransporter.ContactEmail,
                KeyContactName = _NewTransporter.ContactName,
                KeyContactPhone1 = _NewTransporter.ContactMobile1,
                KeyContactPhone2 = _NewTransporter.ContactMobile2,
                PanNO = _NewTransporter.PANNo,
                ServiceTaxNo = _NewTransporter.ServiceTaxNo,
                State = _NewTransporter.State,
                Pincode = _NewTransporter.PinCode,
                TINNo = _NewTransporter.TINNo,
            };
            return transporter;
        }

        public MTransporter UpdateTransporter(MTransporter _UpdateTransporter)
        {
            var transporter = ConvertToTransporter(_UpdateTransporter);
            db.Transporters.Attach(transporter);
            db.ObjectStateManager.ChangeObjectState(transporter, System.Data.EntityState.Modified);
            db.SaveChanges();
            return _UpdateTransporter;
        }

        public void DeleteTransporter(int _TransporterID)
        {
            var transporter = db.Transporters.Single(x => x.ID == _TransporterID);
            db.Transporters.DeleteObject(transporter);
            db.SaveChanges();
        }

        public MAddress CreateNewAddress(MAddress _NewAddress)
        {
            var dt = ConvertToAddress(_NewAddress);
            dt.ID = db.GetMaxAddressID() + 1;
            db.Addresses.AddObject(dt);
            db.SaveChanges();
            return _NewAddress;
        }

        public Address ConvertToAddress(MAddress _NewAddress)
        {
            Address add = new Address()
            {
                ID = _NewAddress.ID,
                Title = _NewAddress.Title,
                AddressFor = _NewAddress.AddressFor,
                AddressForID = _NewAddress.AddressForID,
                AddressLine1 = _NewAddress.AddressLine1,
                AddressLine2 = _NewAddress.AddressLine2,
                AddressType = _NewAddress.AddressType,
                City = _NewAddress.City,
                State = _NewAddress.State,
                Country = _NewAddress.Country,
                Pincode = _NewAddress.Pincode,
                Email = _NewAddress.Email,
                Fax = _NewAddress.Fax,
                Phone = _NewAddress.Phone,
                Website = _NewAddress.Website
            };
            return add;
        }

        public MAddress UpdateAddress(MAddress _updatedAddress)
        {
            var UA = ConvertToAddress(_updatedAddress);
            db.Addresses.Attach(UA);
            db.ObjectStateManager.ChangeObjectState(UA, System.Data.EntityState.Modified);
            db.SaveChanges();
            return _updatedAddress;
        }

        public void DeleteAddress(int _AddressID)
        {
            var obj = db.Addresses.Single(x => x.ID == _AddressID);
            db.Addresses.DeleteObject(obj);
            db.SaveChanges();
        }

        public MDocument SaveDocument(string FileName, string For, int? ForID, string DocType)
        {
            db.Documents.AddObject(new Document()
            {
                //ID = db.GetMaxDocumentID() + 1,
                DocPath = FileName,
                DocumentFor = For,
                DocumentForID = ForID,
                DocumentType = DocType
            });
            db.SaveChanges();
            return provider.GetDocumentbyID(db.GetMaxDocumentID());
        }

        public MDocument SaveDocument(MDocument _NewDocument)
        {
            var dt = ConvertToDocument(_NewDocument);
            //dt.ID = db.GetMaxDocumentID() + 1;
            db.Documents.AddObject(dt);
            db.SaveChanges();
            return _NewDocument;
        }

        public Document ConvertToDocument(MDocument _NewDocument)
        {
            Document doc = new Document()
            {
                ID = _NewDocument.ID,
                DocumentFor = _NewDocument.DocumentFor,
                DocumentForID = _NewDocument.DocumentForID,
                DocumentType = _NewDocument.DocumentType,
                DocPath = _NewDocument.DocPath
            };
            return doc;
        }

        public void DeleteDocument(int DocumentID)
        {
            var obj = db.Documents.Single(X => X.ID == DocumentID);
            db.Documents.DeleteObject(obj);
            db.SaveChanges();
        }

        public MContact CreateNewContact(MContact _NewContact)
        {
            var dt = ConvertToContacts(_NewContact);
            dt.ID = db.GetMaxContactID() + 1;
            db.Contacts.AddObject(dt);
            db.SaveChanges();
            return _NewContact;
        }

        public Contact ConvertToContacts(MContact _NewContact)
        {
            Contact con = new Contact()
            {
                ID = _NewContact.ID,
                Title = _NewContact.Title,
                ContactFor = _NewContact.ContactFor,
                ContactForID = _NewContact.ContactForID,
                Name = _NewContact.Name,
                Mobile1 = _NewContact.Mobile1,
                Mobile2 = _NewContact.Mobile2,
                Email1 = _NewContact.Email1,
                Email2 = _NewContact.Email2,
                Fax = _NewContact.Fax,
                Phone = _NewContact.Phone,
            };
            return con;
        }

        public MContact UpdateContact(MContact _updatedContact)
        {
            var UC = ConvertToContacts(_updatedContact);
            db.Contacts.Attach(UC);
            db.ObjectStateManager.ChangeObjectState(UC, System.Data.EntityState.Modified);
            db.SaveChanges();
            return _updatedContact;
        }

        public void DeleteContact(int _ContactID)
        {
            var obj = db.Contacts.Single(x => x.ID == _ContactID);
            db.Contacts.DeleteObject(obj);
            db.SaveChanges();
        }

        public MAccounts CreateNewAccount(MAccounts _NewAccount)
        {
            var dt = ConvertToAccount(_NewAccount);
            dt.ID = db.GetMaxAccountID() + 1;
            db.AccountDetails.AddObject(dt);
            db.SaveChanges();
            return _NewAccount;
        }

        public AccountDetail ConvertToAccount(MAccounts _NewAccount)
        {
            AccountDetail acc = new AccountDetail()
            {
                ID = _NewAccount.ID,
                AccountFor = _NewAccount.AccountFor,
                AccountForID = _NewAccount.AccountForID,
                AccountNo = _NewAccount.AccountNo,
                BankEmail = _NewAccount.BankEmail,
                BankName = _NewAccount.BankName,
                BankPhone = _NewAccount.BankPhone,
                BankWebSite = _NewAccount.BankWebSite,
                BranchAddress = _NewAccount.BranchAddress,
                BranchCity = _NewAccount.BranchCity,
                BranchCountry = _NewAccount.BranchCountry,
                BranchPinCode = _NewAccount.BranchPinCode,
                BranchState = _NewAccount.BranchState,
                ChequePayableTo = _NewAccount.ChequePayableTo,
                IFSCCode = _NewAccount.IFSCCode
            };
            return acc;
        }

        public MAccounts UpdateAccount(MAccounts _updatedAccount)
        {
            var UAcc = ConvertToAccount(_updatedAccount);
            db.AccountDetails.Attach(UAcc);
            db.ObjectStateManager.ChangeObjectState(UAcc, System.Data.EntityState.Modified);
            db.SaveChanges();
            return _updatedAccount;
        }

        public void DeleteAccount(int _AccountID)
        {
            var obj = db.AccountDetails.Single(x => x.ID == _AccountID);
            db.AccountDetails.DeleteObject(obj);
            db.SaveChanges();
        }

        public void SaveImagePath(string ImageName, int ItemID)
        {
            var item = db.Items.Single(x => x.ID == ItemID);
            item.Photo = ImageName;
            db.SaveChanges();
        }

        #region Item Manager///////////////////////////////////////////////////   Item Manager  ///////////

        ItemCategory GetCategory(int id, string name)
        {
            if (id != 0)
            {
                return db.ItemCategories.Single(x => x.ID == id);
            }
            var qry = db.ItemCategories.Where(x => x.Name.ToLower() == name.ToLower());
            if (qry.Count() > 0)
                return qry.ToList().Last();
            ItemCategory cat = new ItemCategory()
            {
                Name = name,
                IsConsumable = false,
            };
            db.ItemCategories.AddObject(cat);
            db.SaveChanges();
            return cat;
        }

        public MItem CreateNewItem(MItem newItem, string categoryName)
        {
            newItem.ItemCategoryID = GetCategory(newItem.ItemCategoryID, categoryName).ID;
            newItem.ID = db.GetMaxItemID() + 1;
            newItem.DealTypeID = "Fixed";
            newItem.ConsumableType = "Consumable";
            var qry = ConvertToItem(newItem);
            if (qry.ConsumableType == null)
            {
                qry.ConsumableType = "Consumable";
            }
            db.Items.AddObject(qry);
            db.SaveChanges();
            return newItem;
        }

        public MItem CreateNewItem(MItem newItem)
        {
            //newItem.ItemCategoryID = GetCategory(newItem.ItemCategoryID, categoryName).ID;
            newItem.ID = db.GetMaxItemID() + 1;
            newItem.DealTypeID = "Fixed";
            //newItem.ConsumableType = "Consumable";
            var qry = ConvertToItem(newItem);
            if (qry.ConsumableType == null)
            {
                qry.ConsumableType = "Consumable";
            }
            db.Items.AddObject(qry);
            db.SaveChanges();
            return newItem;
        }

        public MItemSize CreateNewItemSize(MItemSize NewSize)
        {
            var qry = ConvertToItemSize(NewSize);
            db.ItemSizes.AddObject(qry);
            db.SaveChanges();
            //return ConvertToMItemSize(db.ItemSizes.OrderByDescending(x=>x.ID).First());
            return ConvertToMItemSize(db.ItemSizes.Single(x => x.ID == qry.ID));
        }

        public MItemSize ConvertToMItemSize(ItemSize data)
        {
            MItemSize size = new MItemSize()
            {
                Size = data.Value,
            };
            return size;
        }

        public ItemSize ConvertToItemSize(MItemSize NewSize)
        {
            ItemSize size = new ItemSize()
            {
                Value = NewSize.Size,
            };
            return size;
        }

        public MItemBrand CreateNewItemBrand(MItemBrand NewBrand)
        {
            var qry = ConvertToItemBrand(NewBrand);
            db.ItemBrands.AddObject(qry);
            db.SaveChanges();
            //return ConvertToMItemBrand(db.ItemBrands.OrderByDescending(x=>x.ID).First());
            return ConvertToMItemBrand(db.ItemBrands.Single(x => x.ID == qry.ID));
        }

        public MItemBrand ConvertToMItemBrand(ItemBrand data)
        {
            MItemBrand brand = new MItemBrand()
            {
                BrandName = data.Value,
            };
            return brand;
        }

        public ItemBrand ConvertToItemBrand(MItemBrand NewBrand)
        {
            ItemBrand brand = new ItemBrand()
            {
                Value = NewBrand.BrandName,
            };
            return brand;
        }

        public MCompany CreateNewCompany(MCompany NewCompany)
        {
            var qry = ConvertToCompany(NewCompany);
            db.Companies.AddObject(qry);
            db.SaveChanges();
            //return ConvertToMCompany(db.Companies.OrderByDescending(x => x.ID).First());
            return ConvertToMCompany(db.Companies.Single(x => x.ID == qry.ID));
        }

        public MCompany ConvertToMCompany(Company data)
        {
            MCompany company = new MCompany()
            {
                ID = data.ID,
                Name = data.Name,
                AddressLine1 = data.AddressLine1,
                AddressLine2 = data.AddressLine2,
                AddressLine3 = data.AddressLine3,
                City = data.City,
                State = data.State,
                Country = data.Country,
                Pincode = data.Pincode,
                Fax = data.Fax,
                Phone = data.Phone,
                Mobile1 = data.Mobile1,
                Mobile2 = data.Mobile2,
                Email = data.Email,
                Website = data.Website,
            };
            return company;
        }

        public Company ConvertToCompany(MCompany NewCompany)
        {
            Company company = new Company()
            {
                ID = NewCompany.ID,
                Name = NewCompany.Name,
                AddressLine1 = NewCompany.AddressLine1,
                AddressLine2 = NewCompany.AddressLine2,
                AddressLine3 = NewCompany.AddressLine3,
                City = NewCompany.City,
                State = NewCompany.State,
                Country = NewCompany.Country,
                Pincode = NewCompany.Pincode,
                Fax = NewCompany.Fax,
                Phone = NewCompany.Phone,
                Mobile1 = NewCompany.Mobile1,
                Mobile2 = NewCompany.Mobile2,
                Email = NewCompany.Email,
                Website = NewCompany.Website,
            };
            return company;
        }


        public MItem UpdateItem(MItem updatedItem)
        {
            //var data = db.Items.Single(x => x.ID == updatedItem.ID);
            //data.Name = updatedItem.Name;
            //data.DealTypeID = updatedItem.DealTypeID;
            //data.Description = updatedItem.Description;
            //data.IsDeleted = updatedItem.IsDeleted;
            //data.CategoryID = updatedItem.ItemCategoryID;
            //data.MeasurementID = updatedItem.MeasurementUnitID;
            //data.Name = updatedItem.Name;
            //data.Remarks = updatedItem.Remarks;
            //data.DefaultVendorID = updatedItem.VendorID;
            //data.ConsumableType = updatedItem.ConsumableType;
            //data.InventoryCount = updatedItem.InventoryCount;
            //data.Photo = updatedItem.Photo;

            var qry = ConvertToItem(updatedItem);
            db.Items.Attach(qry);
            db.ObjectStateManager.ChangeObjectState(qry, System.Data.EntityState.Modified);
            db.SaveChanges();
            return updatedItem;
        }

        public void DeleteItem(int itemId)
        {
            var obj = db.Items.Single(x => x.ID == itemId);
            db.DeleteObject(obj);
            db.SaveChanges();
        }

        public void UpdateParentForItemCategory(int ID, int NewPID)
        {
            var data = db.ItemCategories.Single(x => x.ID == ID);
            data.ParentID = NewPID;
            db.SaveChanges();
        }

        public MItemCategory CreateNewItemCategory(MItemCategory _newItemCategory)
        {
            var data = ConvertTOItemCategory(_newItemCategory);
            db.ItemCategories.AddObject(data);
            db.SaveChanges();
            int ID = db.ItemCategories.Max(x => x.ID);
            return provider.GetItemCategoryByID(ID);
        }

        private ItemCategory ConvertTOItemCategory(MItemCategory _newItemCategory)
        {
            ItemCategory itemcat = new ItemCategory()
            {
                ID = _newItemCategory.ID,
                Name = _newItemCategory.Name,
                ParentID = _newItemCategory.ParentID,
                IsConsumable = _newItemCategory.IsConsumable,
                IsItem = _newItemCategory.IsItem,
                CanHaveItems = _newItemCategory.CanHaveItem,
                HasVariations = _newItemCategory.HasVariations,
                Description = _newItemCategory.Description,
                IsStockable = _newItemCategory.IsStockable,
                IsSystemDefined = _newItemCategory.IsSystemDefined,
                ItemType = _newItemCategory.ItemType
            };
            return itemcat;
        }


        public MItemCategory UpdateItemCategory(MItemCategory _updatedItemCategory)
        {
            var qry = db.ItemCategories.Single(x => x.ID == _updatedItemCategory.ID);
            qry.Name = _updatedItemCategory.Name;
            db.SaveChanges();
            return _updatedItemCategory;
        }

        Item ConvertToItem(MItem _item)
        {
            Item item = new Item()
            {
                ID = _item.ID,
                Code = _item.ItemCode,
                DealTypeID = _item.DealTypeID,
                Description = _item.Description,
                IsDeleted = _item.IsDeleted,
                CategoryID = _item.ItemCategoryID,
                MeasurementID = _item.MeasurementUnitID,
                Name = _item.Name,
                Remarks = _item.Remarks,
                DefaultVendorID = _item.VendorID,
                ConsumableType = _item.ConsumableType,
                InventoryCount = _item.InventoryCount,
                Photo = _item.Photo,
                IsStockable = _item.IsStockable,
            };
            return item;
        }

        #endregion
    }
}
