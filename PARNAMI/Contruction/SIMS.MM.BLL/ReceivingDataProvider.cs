﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SIMS.MM.BLL
{
    using DAL;
    using MODEL;
    using System.Collections.ObjectModel;
    public class ReceivingDataProvider : IReceivingDataProvider
    {
        parnamidb1Entities db = new parnamidb1Entities();
        MMReportsDBDataContext db1 = new MMReportsDBDataContext();

        public IPaginated<MReceivingHeader> GetCashPurachaseHeadersNew(string username, int? pageindex, int? pagesize, string key, int? vendorid, int? siteid, bool showAll = true)
        {
            var data = db1.GetCPHeaders(username, key, vendorid, siteid, showAll);
            return data.OrderByDescending(x => x.ID)
                .ToPaginate(pageindex, pagesize)
                .Select(x => new MReceivingHeader
                {
                    ID = x.ID,
                    ReferenceNo = x.ReferenceNo,
                    ForDate = x.ForDate,
                    //Site = x.BUs.Name,
                    //Vendor = x.Vendor_Name,
                    DisplayVendor = x.Vendor_Name,
                    DisplaySite = x.Site_Name,
                    UpdateOn = x.LastUpdatedOn,
                    Remarks = x.Remarks,
                    ChallanNo = x.ChallanNo
                });
        }

        public IPaginated<MReceivingHeader> GetDirectPurchaseHeaders(string username, int? pageindex, int? pagesize, string key, int? vendorid, int? siteid, bool showall = true)
        {
            var qry = db.DirectPurchases
               .FilterByVendor(vendorid)
               .FilterBySite(siteid)
               .OrderByDescending(x => x.ID).AsQueryable();
            if (!showall)
            {
                var ids = db.GetAllowedSiteIDs(username, showall);
                qry = qry.Where(x => ids.Contains(x.BUID));

            }
            return qry.ToList()
              .ToPaginate(pageindex, pagesize)
              .Select(x => new MReceivingHeader()
              {
                  ID = x.ID,
                  ReferenceNo = x.ReferenceNo,
                  ForDate = x.ForDate,
                  Site = x.BUs.Name,
                  Vendor = x.Vendor.Name,
                  DisplayVendor = x.Vendor.DisplayVendor,
                  DisplaySite = x.BUs.DisplaySite,
                  UpdateOn = x.LastUpdatedOn,
                  Remarks = x.Remarks,
                  ChallanNo = x.ChallanNo
              });
        }

        public IPaginated<MReceivingHeader> GetConsumptionHeadersNew(string username, int? pageindex, int? pagesize, string key, int? siteid, bool showAll = true)
        {
            var data = db1.GetConsumptionHeaders(username, key, siteid, showAll);
            return data.OrderByDescending(x => x.ID)
                .ToPaginate(pageindex, pagesize)
                .Select(x => new MReceivingHeader
                {
                    ID = x.ID,
                    ReferenceNo = x.ReferenceNo,
                    ForDate = x.ForDate,
                    //Site = x.BUs.Name,
                    //Vendor = x.Vendor_Name,
                    //DisplayVendor = x.Vendor_Name,
                    DisplaySite = x.Site_Name,
                    UpdateOn = x.LastUpdatedOn,
                    Remarks = x.Remarks,
                    ChallanNo = x.ChallanNo
                });
        }

        public IPaginated<MReceivingHeader> GetGRNHeadersNew(string username, int? pageindex, int? pagesize, string key, int? vendorid, int? siteid, bool showAll = true)
        {
            var data = db1.GetGRNHeaders(username, key, vendorid, siteid, showAll);
            return data.OrderByDescending(x => x.ID)
                .ToPaginate(pageindex, pagesize)
                .Select(x => new MReceivingHeader
                {
                    ID = x.ID,
                    ReferenceNo = x.ReferenceNo,
                    ForDate = x.ForDate,
                    //Site = x.BUs.Name,
                    //Vendor = x.Vendor_Name,
                    DisplayVendor = x.Vendor_Name,
                    DisplaySite = x.Site_Name,
                    UpdateOn = x.LastUpdatedOn,
                    Remarks = x.Remarks,
                    ChallanNo = x.ChallanNo
                });
        }

        public MReceiving GetCPbyId(int id)
        {
            var data = db.CashPurchases.Single(x => x.ID == id);
            return ConvertToMReceiving(data);
        }

        public MReceiving ConvertToMReceiving(InventoryTransaction _it, bool HeaderOnly=false)
        {
            MReceiving mrec = new MReceiving()
            {
                ReferenceNo = _it.ReferenceNo,
                TransactionID = _it.ID,
                TransactionTypeID = _it.TypeID,
                SiteID = _it.BUID,
                VendorID = _it.VendorID,
                TransporterID = _it.TransitID,
                ChallanNo = _it.ChallanNo,
                CreatedBy = _it.CreatedBy,
                CreatedOn = _it.CreatedOn,
                InvoiceNo = _it.InvoiceNo,
                LastUpdaetdOn = _it.LastUpdatedOn,
                LastUpdatedBy = _it.LastUpdatedBy,
                PurchaseDate = _it.ForDate,
                Purchaser = _it.Requester,
                ReceivingRemarks = _it.Remarks,
                Site = _it.BUs.Name,
                VehicleDetails = _it.VehicleDetails,
                //Vendor=_it.Vendor.Name
            };
            if (_it.Vendor != null) mrec.Vendor = _it.Vendor.Name;
            if (HeaderOnly) return mrec;

            mrec.ReceivingLines = new ObservableCollection<MReceivingLine>();
            _it.InventoryTransactionDetails.Select(x => ConvertToRLines(x)).ToList()
                .ForEach(x => mrec.ReceivingLines.Add(x));
            return mrec;
        }

        public MReceivingLine ConvertToRLines(InventoryTransactionDetail _itd)
        {
            MReceivingLine mrline = new MReceivingLine()
            {
                LineID = _itd.ID,
                ItemID = _itd.ItemID,
                SizeID = _itd.SizeID,
                BrandID = _itd.BrandID,
                TransactionTypeID = _itd.TypeID,
                MTRID = _itd.MTRID,
                MTRLineID = _itd.MTRLineID,
                POID = _itd.POID,
                PRID = _itd.PRID,
                PRLineID = _itd.PRLineID,
                TransactionID = _itd.HeaderID,
                Item = _itd.Item.DisplayName,
                LineRemarks = _itd.Remarks,
                Quantity = _itd.Qty,
                TruckNo = _itd.TruckNo,
                LineChallanNo = _itd.ChallanNo,
            };
            if (_itd.ItemBrand != null)
                mrline.Brand = _itd.ItemBrand.Value;
            if (_itd.ItemSize != null)
                mrline.Size = _itd.ItemSize.Value;
            if (_itd.PRDetail != null)
            {
                mrline.OrderedQty = _itd.PRDetail.Qty;
                mrline.EarlierReceivedQty = _itd.PRDetail.TotalReceivedQty.Value;
            }
            if (_itd.CashPurchaseDetail != null)
            {
                mrline.UnitPrice = (double)_itd.CashPurchaseDetail.UnitPrice;
                mrline.LinePrice = (double)_itd.CashPurchaseDetail.LinePrice;
            }
            if (_itd.TruckMeasurement != null)
            {
                if (mrline.TruckParameters == null) mrline.TruckParameters = new MTruckParameter();
                mrline.TruckParameters.ID = _itd.TruckMeasurement.ID;
                mrline.TruckParameters.Length = _itd.TruckMeasurement.Lth;
                mrline.TruckParameters.Breadth = _itd.TruckMeasurement.Bth;
                mrline.TruckParameters.Height = _itd.TruckMeasurement.Ht;
            }
            return mrline;
        }

        public MReceiving GetDPbyId(int id)
        {
            var data = db.DirectPurchases.Single(x => x.ID == id);
            var lines = db1.GRN_GETGRNLINES(id, "Direct");
            var obj = ConvertToMReceiving(data, true);
            foreach (var l in lines)
            {
                MReceivingLine rline = new MReceivingLine()
                {
                    LineID = l.LINEID,
                    ItemID = l.ITEMID.Value,
                    Item = l.ITEM,
                    //SizeID = l.SizeID,
                    //Size = l.Size,
                    //BrandID = l.BrandID,
                    //Brand = l.Brand,
                    Quantity = l.Qty.Value,
                    UnitPrice = l.UnitPrice.Value,
                    LinePrice = l.LinePrice.Value,
                    LineChallanNo = l.ChallanNo,
                    LineRemarks = l.Remarks,
                    BillDate = l.BILLDate,
                    BillingID = l.BILLID,
                    BillingLineID = l.BILLINGLINEID,
                    BillNo = l.BILLNo,
                    IsBillClosed = l.BillClosed,
                    TotalbilingAmnt = l.BillAmt,
                    TotalPaidAmnt = l.BillPaid
                };
                obj.ReceivingLines.Add(rline);
            }
            return obj;
        }

        public MReceiving GetGRNbyId(int id)
        {
            var data = db.AllGRNs.Single(x => x.ID == id);
            var lines = db1.GRN_GETGRNLINES(id, "GRN");
            var obj = ConvertToMReceiving(data,true);
            foreach (var l in lines)
            {
                MReceivingLine rline = new MReceivingLine()
                {
                    LineID = l.LINEID,
                    ItemID = l.ITEMID.Value,
                    Item = l.ITEM,
                    SizeID = l.SIZEID,
                    Size = l.SIZE,
                    BrandID = l.BRANDID,
                    Brand = l.BRAND,
                    UnitPrice = l.UnitPrice.Value,
                    LinePrice = l.LinePrice.Value,
                    OrderedQty = l.OrderedQty??0,
                    EarlierReceivedQty = l.EarlierRCVED??0,
                    Quantity = l.Qty.Value,
                    LineChallanNo = l.ChallanNo,
                    LineRemarks = l.Remarks,
                    BillDate = l.BILLDate,
                    BillingID = l.BILLID,
                    BillingLineID = l.BILLINGLINEID,
                    BillNo = l.BILLNo,
                    IsBillClosed = l.BillClosed,
                    TotalbilingAmnt = l.BillAmt,
                    TotalPaidAmnt = l.BillPaid,
                    TruckNo = l.TRUCKNo
                };
                obj.ReceivingLines.Add(rline);
                if (l.BILLID == null)           //to check if Bill exists or not
                {
                    rline.HasBill = false;
                }
                else
                {
                    rline.HasBill = true;
                }
            }
            return obj;
        }

        public MReceiving GetConsbyId(int id)
        {
            var data = db.AllConsumptions.Single(x => x.ID == id);
            return ConvertToMReceiving(data);
        }

        public IPaginated<MReceivingHeader> GetAllDirectGRNsNew(string username, int? pageindex, int? pagesize, string key, int? vendorid, int? siteid, string Type, bool showAll = true)
        {
            var qry = db1.GetDirectGRNHeaders(username, key, vendorid, siteid, Type, showAll)
              .OrderByDescending(x => x.ForDate)
              .AsQueryable();
            //if (!showAll)
            //{
            //    var ids = db.GetAllowedSiteIDs(username, showAll);
            //    qry = qry.Where(x => ids.Contains(x.BUID));
            //}
            var data = qry
                //.ToList()
              .ToPaginate(pageindex, pagesize)
              .Select(x => new MReceivingHeader()
              {
                  ID = x.ID,
                  ReferenceNo = x.ReferenceNo,
                  ForDate = x.ForDate,
                  //Site = x.Site_Name,
                  //Vendor = x.Vendor_Name,
                  DisplayVendor = x.Vendor_Name,
                  DisplaySite = x.Site_Name,
                  UpdateOn = x.LastUpdatedOn,
                  Remarks = x.Remarks,
                  ChallanNo = x.ChallanNo,
                  Type = x.TypeID,
                  TotalTrnLines = x.TotalTrnLines,
                  BilliedTrnLines = x.BilledTrnLines,
                  UnBilliedTrnLines = x.UnBilledTrnLines,
                  BillingStatus = x.BillingStatus
              });
            return data;
        }




        //public IQueryable<MODEL.MReceivingHeader> GetCashPurachaseHeaders(int? pageindex, int? pagesize, string key, int? vendorid, int? siteid)
        //{                             
        //    return db.CashPurchases
        //        .FilterByVendor(vendorid)
        //        .FilterBySite(siteid)
        //        .OrderBy(x => x.ID)
        //        .Paginate(pageindex, pagesize)    
        //        .ToList()
        //        .Select(x => new MReceivingHeader()
        //        {
        //            ReferenceNo=x.ReferenceNo,
        //            ForDate=x.ForDate,
        //            Site = x.BUs.Name,
        //            Vendor=x.Vendor.Name,
        //            UpdateOn=x.LastUpdatedOn,
        //            Remarks=x.Remarks,
        //            ChallanNo=x.ChallanNo

        //        }).ToList().AsQueryable();
        //}

        //public IPaginated<MReceivingHeader> GetCashPurachaseHeaders(string username, int? pageindex, int? pagesize, string key, int? vendorid, int? siteid, bool showall = true)
        //{
        //    var qry = db.CashPurchases
        //       .FilterByVendor(vendorid)
        //       .FilterBySite(siteid)
        //       .OrderByDescending(x => x.ID).AsQueryable();
        //    if (!showall)
        //    {
        //        var ids = db.GetAllowedSiteIDs(username, showall);
        //        qry = qry.Where(x => ids.Contains(x.BUID));

        //    }
        //    return qry.ToList()
        //      .ToPaginate(pageindex, pagesize)
        //      .Select(x => new MReceivingHeader()
        //      {
        //          ID = x.ID,
        //          ReferenceNo = x.ReferenceNo,
        //          ForDate = x.ForDate,
        //          Site = x.BUs.Name,
        //          Vendor = x.Vendor.Name,
        //          DisplayVendor = x.Vendor.DisplayVendor,
        //          DisplaySite = x.BUs.DisplaySite,
        //          UpdateOn = x.LastUpdatedOn,
        //          Remarks = x.Remarks,
        //          ChallanNo = x.ChallanNo
        //      });
        //}

        //public IQueryable<MODEL.MReceivingHeader> GetDirectPurchaseHeaders(int? pageindex, int? pagesize, string key, int? vendorid, int? siteid)
        //{
        //    return db.DirectPurchases
        //        .FilterByVendor(vendorid)
        //        .FilterBySite(siteid)
        //        .OrderBy(x => x.ID)
        //        .Paginate(pageindex,pagesize)
        //        .ToList()
        //        .Select(x => new MReceivingHeader()
        //        {
        //            ReferenceNo =x.ReferenceNo,
        //            ForDate = x.ForDate,
        //            Site = x.BUs.Name,
        //            Vendor = x.Vendor.Name,
        //            UpdateOn = x.LastUpdatedOn,
        //            Remarks = x.Remarks,
        //            ChallanNo = x.ChallanNo

        //        }).AsQueryable();
        //}


        //public IQueryable<MODEL.MReceivingHeader> GetConsumptionHeaders(int? pageindex, int? pagesize, string key, int? siteid)
        //{
        //    return db.AllConsumptions                
        //        .FilterBySite(siteid)
        //        .OrderBy(x => x.ID)
        //        .Paginate(pageindex, pagesize)
        //        .ToList()
        //        .Select(x => new MReceivingHeader()
        //        {
        //            ReferenceNo = x.ReferenceNo,
        //            ForDate = x.ForDate,
        //            Site = x.BUs.Name,                    
        //            UpdateOn = x.LastUpdatedOn,
        //            Remarks = x.Remarks,
        //            ChallanNo = x.ChallanNo

        //        }).AsQueryable();
        //}

        //public IPaginated<MReceivingHeader> GetConsumptionHeaders(string username, int? pageindex, int? pagesize, string key, int? siteid, bool showall = true)
        //{
        //    var qry = db.AllConsumptions
        //         .OrderByDescending(x => x.ID)
        //         .FilterBySite(siteid)
        //         .OrderByDescending(x => x.ID).AsQueryable();
        //    if (!showall)
        //    {
        //        var ids = db.GetAllowedSiteIDs(username, showall);
        //        qry = qry.Where(x => ids.Contains(x.BUID));

        //    }
        //    return qry
        //        .ToList()
        //        .ToPaginate(pageindex, pagesize)
        //        .Select(x => new MReceivingHeader()
        //        {
        //            ID = x.ID,
        //            ReferenceNo = x.ReferenceNo,
        //            ForDate = x.ForDate,
        //            Site = x.BUs.Name,
        //            //DisplayVendor = x.Vendor.DisplayVendor,
        //            DisplaySite = x.BUs.DisplaySite,
        //            UpdateOn = x.LastUpdatedOn,
        //            Remarks = x.Remarks,
        //            ChallanNo = x.ChallanNo

        //        });
        //}

        //public IQueryable<MODEL.MReceivingHeader> GetGRNHeaders(int? pageindex, int? pagesize, string key, int? vendorid, int? siteid)
        //{
        //    var data = db.AllGRNs
        //         .FilterByVendor(vendorid)
        //         .FilterBySite(siteid)
        //         .OrderBy(x => x.ID)
        //         .Paginate(pageindex, pagesize)
        //         .ToList()
        //         .Select(x => new MReceivingHeader()
        //         {
        //             ReferenceNo = x.ReferenceNo,
        //             ForDate = x.ForDate,
        //             Site = x.BUs.Name,
        //             Vendor = x.Vendor.Name,
        //             UpdateOn = x.LastUpdatedOn,
        //             Remarks = x.Remarks,
        //             ChallanNo = x.ChallanNo
        //         }).AsQueryable();
        //   return data;
        //}

        //public IPaginated<MReceivingHeader> GetAllDirectGRNs(string username, int? pageindex, int? pagesize, string key, int? vendorid, int? siteid, string Type, bool showAll = true)
        //{
        //    var qry = db.Receivings
        //      .FilterByVendor(vendorid)
        //      .FilterBySite(siteid)
        //      .FilterByType(Type)
        //      .OrderByDescending(x => x.ForDate)
        //      //.OrderByDescending(x => x.ID)
        //      .AsQueryable();
        //    if (!showAll)
        //    {
        //        var ids = db.GetAllowedSiteIDs(username, showAll);
        //        qry = qry.Where(x => ids.Contains(x.BUID));

        //    }
        //    var data = qry.ToList()
        //      .ToPaginate(pageindex, pagesize)
        //      .Select(x => new MReceivingHeader()
        //      {
        //          ID = x.ID,
        //          ReferenceNo = x.ReferenceNo,
        //          ForDate = x.ForDate,
        //          Site = x.BUs.Name,
        //          Vendor = x.Vendor.Name,
        //          DisplayVendor = x.Vendor.DisplayVendor,
        //          DisplaySite = x.BUs.DisplaySite,
        //          UpdateOn = x.LastUpdatedOn,
        //          Remarks = x.Remarks,
        //          ChallanNo = x.ChallanNo,
        //          Type = x.TypeID
        //      });
        //    return data;
        //}
    }
}
