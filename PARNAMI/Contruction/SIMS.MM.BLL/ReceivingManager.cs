﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SIMS.MM.BLL
{
    using MODEL;
    using DAL;
    using System.Collections.ObjectModel;

    public class ReceivingManager : IReceivingManager
    {
        parnamidb1Entities db = new parnamidb1Entities();
        ReceivingDataProvider provier1 = new ReceivingDataProvider();
        PurchasingDataProvider ppro = new PurchasingDataProvider();

        public InventoryTransaction ConvertToINTR(MReceiving _mrec)
        {
            InventoryTransaction IT = new InventoryTransaction()
            {
                ID = _mrec.TransactionID,
                ChallanNo = _mrec.ChallanNo,
                CreatedBy = _mrec.CreatedBy,
                CreatedOn = _mrec.CreatedOn,
                ForDate = _mrec.PurchaseDate,
                InvoiceNo = _mrec.InvoiceNo,
                LastUpdatedOn = _mrec.LastUpdaetdOn,
                LastUpdatedBy = _mrec.LastUpdatedBy,
                Remarks = _mrec.ReceivingRemarks,
                BUID = _mrec.SiteID,
                TypeID = _mrec.TransactionTypeID,
                VendorID = _mrec.VendorID,
                Requester = _mrec.Purchaser,
                VehicleDetails = _mrec.VehicleDetails
            };
            if (_mrec.ReceivingLines != null)
            {
                _mrec.ReceivingLines.ToList()
                    .ForEach(x => IT.InventoryTransactionDetails.Add(ConvertToINTRD(x, IT.TypeID)));
            }
            return IT;
        }

        public InventoryTransactionDetail ConvertToINTRD(MReceivingLine _details, string type)
        {
            InventoryTransactionDetail detail = new InventoryTransactionDetail()
            {
                ID = _details.LineID,
                ItemID = _details.ItemID,
                SizeID = _details.SizeID,
                BrandID = _details.BrandID,
                //LinePrice=_details.LinePrice,
                Remarks = _details.LineRemarks,
                MTRID = _details.MTRID,
                MTRLineID = _details.MTRLineID,
                POID = _details.POID,
                PRID = _details.PRID,
                PRLineID = _details.PRLineID,
                Qty = _details.Quantity,
                //UncheckedQty=_details.OrderedQty,//??
                HeaderID = _details.TransactionID,
                TruckNo = _details.TruckNo,
                TypeID = type,
                MRID = _details.MRID,
                MRLineID = _details.MRLineID,
                ChallanNo = _details.LineChallanNo
            };

            if (_details.TruckParameters != null)
            {
                TruckMeasurement tm = new TruckMeasurement();
                tm.Lth = _details.TruckParameters.Length;
                tm.Bth = _details.TruckParameters.Breadth;
                tm.Ht = _details.TruckParameters.Height;
                detail.TruckMeasurement = tm;
            }
            if (detail.TypeID == "Cash" || detail.TypeID == "Direct" || detail.TypeID == "GRN")
            {
                CashPurchaseDetail cdetail = new CashPurchaseDetail();
                cdetail.TypeID = detail.TypeID;
                cdetail.HeaderID = detail.HeaderID;
                cdetail.ID = detail.ID;
                cdetail.UnitPrice = (decimal)_details.UnitPrice;
                cdetail.LinePrice = (decimal)_details.LinePrice;
                detail.CashPurchaseDetail = cdetail;
            }

            if (detail.TypeID == "Adjust")
            {
                StockAdjustment sa = new StockAdjustment();
                sa.ID = detail.ID;
                sa.HeaderID = detail.HeaderID;
                sa.TypeID = detail.TypeID;
                sa.CurrentStock = _details.CurrentStock;
                sa.AdjustedStock = _details.AdjustedStock;
                detail.StockAdjustment = sa;
            }
            //db.CloseMO_IFRequired(detail.MTRID);
            return detail;

        }
        public MReceiving CreateNewReceiving(MReceiving newRcving, string username)
        {
            newRcving.TransactionID = db.GetMaxRID("GRN") + 1;
            newRcving.TransactionTypeID = "GRN";
            newRcving.CreatedBy = username;
            newRcving.LastUpdatedBy = username;
            newRcving.Purchaser = username;
            newRcving.LastUpdaetdOn = DateTime.UtcNow.AddHours(5.5);
            newRcving.CreatedOn = DateTime.UtcNow.AddHours(5.5);
            var rec = ConvertToINTR(newRcving);
            rec.Requester = username;
            rec.Year = DateTime.UtcNow.AddHours(5.5).Year;
            rec.SiteID = db.GetSiteCode(rec.BUID);//???-------------
            rec.RID = db.GetMaxRRID("GRN", rec.Year, rec.BUID) + 1;
            rec.ReferenceNo = rec.TypeID + "/" + rec.Year + "/S-" + rec.SiteID + "/" + rec.RID;
            db.InventoryTransactions.AddObject(rec);
            db.SaveChanges();
            return provier1.GetGRNbyId(rec.ID);
        }

        public MReceiving CreateNewCashPurchase(MReceiving newCP, string username)
        {
            newCP.TransactionID = db.GetMaxRID("Cash") + 1;
            newCP.TransactionTypeID = "Cash";
            newCP.CreatedBy = username;
            newCP.LastUpdatedBy = username;
            newCP.LastUpdaetdOn = DateTime.UtcNow.AddHours(5.5);
            newCP.CreatedOn = DateTime.UtcNow.AddHours(5.5);
            var rec = ConvertToINTR(newCP);
            rec.Requester = username;
            rec.Year = DateTime.UtcNow.AddHours(5.5).Year;
            rec.SiteID = db.GetSiteCode(rec.BUID);//???
            rec.RID = db.GetMaxRRID("Cash", rec.Year, rec.BUID) + 1;
            rec.ReferenceNo = rec.TypeID + "/" + rec.Year + "/S-" + rec.SiteID + "/" + rec.RID;
            db.InventoryTransactions.AddObject(rec);
            db.SaveChanges();
            return provier1.GetCPbyId(rec.ID);
        }

        public MReceiving CreateNewDirectPurchase(MReceiving newDP, string username)
        {
            newDP.TransactionID = db.GetMaxRID("Direct") + 1;
            newDP.TransactionTypeID = "Direct";
            newDP.CreatedBy = username;
            newDP.LastUpdatedBy = username;
            newDP.CreatedOn = DateTime.UtcNow.AddHours(5.5);
            newDP.LastUpdaetdOn = DateTime.UtcNow.AddHours(5.5);
            var rec = ConvertToINTR(newDP);
            rec.Year = DateTime.UtcNow.AddHours(5.5).Year;
            rec.SiteID = db.GetSiteCode(rec.BUID);//???
            rec.RID = db.GetMaxRRID("Direct", rec.Year, rec.BUID) + 1;
            rec.ReferenceNo = rec.TypeID + "/" + rec.Year + "/S-" + rec.SiteID + "/" + rec.RID;
            db.InventoryTransactions.AddObject(rec);
            db.SaveChanges();
            return provier1.GetDPbyId(rec.ID);
        }

        public MReceiving CreateNewConsumption(MReceiving newC, string username)
        {
            newC.TransactionID = db.GetMaxRID("Consumption") + 1;
            newC.LastUpdaetdOn = DateTime.UtcNow.AddHours(5.5);
            newC.TransactionTypeID = "Consumption";
            newC.CreatedBy = username;
            newC.LastUpdatedBy = username;
            var rec = ConvertToINTR(newC);
            //rec.CreatedBy = "dba";
            //rec.LastUpdatedBy = "dba";
            rec.CreatedOn = DateTime.UtcNow.AddHours(5.5);
            rec.Year = DateTime.UtcNow.AddHours(5.5).Year;
            rec.SiteID = db.GetSiteCode(rec.BUID);//???
            rec.RID = db.GetMaxRRID("Consumption", rec.Year, rec.BUID) + 1;
            rec.ReferenceNo = rec.TypeID + "/" + rec.Year + "/S-" + rec.SiteID + "/" + rec.RID;
            db.InventoryTransactions.AddObject(rec);
            db.SaveChanges();
            return provier1.GetConsbyId(rec.ID);
        }

        public MReceiving SaveAsDraft(int id)
        {
            throw new NotImplementedException();
        }

        public MReceiving CreateNewRTV(MReceiving _newRTV, string username)
        {
            _newRTV.TransactionID = db.GetMaxRID("RTV") + 1;
            _newRTV.TransactionTypeID = "RTV";
            _newRTV.CreatedBy = username;
            _newRTV.LastUpdatedBy = username;
            _newRTV.CreatedOn = DateTime.UtcNow.AddHours(5.5);
            _newRTV.LastUpdaetdOn = DateTime.UtcNow.AddHours(5.5);
            var rec = ConvertToINTR(_newRTV);
            rec.Year = DateTime.UtcNow.AddHours(5.5).Year;
            rec.SiteID = db.GetSiteCode(rec.BUID);//???
            rec.RID = db.GetMaxRRID("RTV", rec.Year, rec.BUID) + 1;
            rec.ReferenceNo = rec.TypeID + "/" + rec.Year + "/S-" + rec.SiteID + "/" + rec.RID;
            db.InventoryTransactions.AddObject(rec);
            db.SaveChanges();
            return _newRTV;
        }

        public MReceiving CreateNewSA(MReceiving _newSA, string username)
        {
            _newSA.TransactionID = db.GetMaxRID("Adjust") + 1;
            _newSA.TransactionTypeID = "Adjust";
            _newSA.CreatedBy = username;
            _newSA.LastUpdatedBy = username;
            _newSA.Purchaser = username;
            _newSA.CreatedOn = DateTime.UtcNow.AddHours(5.5);
            _newSA.LastUpdaetdOn = DateTime.UtcNow.AddHours(5.5);
            var rec = ConvertToINTR(_newSA);
            rec.Requester = username;
            rec.Year = DateTime.UtcNow.AddHours(5.5).Year;
            rec.SiteID = db.GetSiteCode(rec.BUID);//???
            rec.RID = db.GetMaxRRID("Adjust", rec.Year, rec.BUID) + 1;
            rec.ReferenceNo = rec.TypeID + "/" + rec.Year + "/S-" + rec.SiteID + "/" + rec.RID;
            db.InventoryTransactions.AddObject(rec);
            db.SaveChanges();
            // return provier1.GetDPbyId(rec.ID);
            return _newSA;
        }

        public MReceiving ConvertfromPO(int poid)
        {
            var po = ppro.GetPObyId(poid);

            MReceiving rec = new MReceiving()
            {
                VendorID = po.VendorID,
                Vendor = po.Vendor,
                SiteID = po.SiteID,
                Site = po.Site,
                PurchaseDate = po.RaisedOn,
            };
            if (po.PRLines != null)
            {
                foreach (var item in po.PRLines)
                {
                    rec.ReceivingLines.Add(ConvertToReceivingLines(item, poid));
                }
            }
            return rec;
        }

        public MReceivingLine ConvertToReceivingLines(MPRLine lines, int poid)
        {
            MReceivingLine line = new MReceivingLine()
            {
                LineID = lines.LineID,
                ItemID = lines.ItemID,
                Item = lines.Item,
                SizeID = lines.SizeID,
                Size = lines.Size,
                BrandID = lines.BrandID,
                Brand = lines.Brand,
                LinePrice = lines.LinePrice,
                OrderedQty = lines.Quantity,
                EarlierReceivedQty = lines.TotalReceivedQty.Value,
                UnitPrice = lines.UnitPrice,
                PRID = lines.PRID,
                PRLineID = lines.LineID,
                POID = poid,
            };
            return line;
        }
    }

    public static class MReceivingExt
    {
        public static MReceivingLine AddNewLine(this MReceiving mr)
        {
            if (mr.ReceivingLines == null)
                mr.ReceivingLines = new ObservableCollection<MReceivingLine>();
            var line = new MReceivingLine();
            mr.ReceivingLines.Add(line);
            mr.UpdateLineNumber();
            return line;
        }

        public static int UpdateLineNumber(this MReceiving mr)
        {
            if (mr.ReceivingLines == null) return 0;
            int i = 1;
            foreach (var item in mr.ReceivingLines)
            {
                item.LineID = i;
                i++;
            }
            return i - 1;
        }
    }
}
