﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Security;
using System.Web;
using System.Web.Security;
namespace SIMS.MM.BLL.Services
{
   public  class AuthenticationManager:IAuthenticationManager
    {
       public bool ValidateUser(string username, string password)
       {
         return  Membership.ValidateUser(username,password);
       }

       public bool HasUser(string username)
       {
           return Membership.GetUser(username) != null;
       }

       public MembershipUser GetUser(string username)
       {
            return Membership.GetUser(username);           
       }

       public string[] GetAllUserNames()
       {
           List<string> list = new List<string>();
           foreach(MembershipUser userinfo in Membership.GetAllUsers())
           {
               list.Add(userinfo.UserName);
           }
           return list.ToArray();
       }

       public void CreateUser(string username, string password, string email, string passwordQuestion, string passwordAnswer, bool isApproved, out MembershipCreateStatus status)
       {
           Membership.CreateUser(username, password, email, passwordQuestion, passwordAnswer, isApproved, out status);
       }
    }
}
