﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SIMS.MM.DAL;
using SIMS.MM.BLL;
using System.Collections.ObjectModel;
using SIMS.MM.MODEL;

namespace SIMS.MM.BLL
{
    public class MessagingDataProvider : IMessagingDataProvider
    {
        parnamidb1Entities db = new parnamidb1Entities();

        public IQueryable<MEmail> GetAllEmails()
        {
            var data = db.Emails
            .OrderBy(x => x.EmailAddresses)
            .Select(x => new MEmail()
            {
                ID = x.ID,
                EmailAddress = x.EmailAddresses
            }).AsQueryable();
            return data;
        }

        public IPaginated<MMessaging> GetAllMessages(string username, int? pageindex, int? pagesize, string key)
        {
            var data = db.MessageReceivers.Where(x => !x.IsDeleted && x.ReceivingUser.ToLower() == username.ToLower())
                //.Where(x=> x.Message.SendingUser.Trim().Equals(username.Trim()))
                //.Select(x => x.Message)
                .ToList();

            var qry = data.OrderByDescending(x => x.Message.SentDate).AsQueryable();

            return qry.ToList()
                .ToPaginate(pageindex, pagesize)
                .Select(x => new MMessaging()
                {
                    ID = x.Message.ID,
                    HasAttachment = x.Message.HasAttachment,
                    Message = x.Message.MainMessage,
                    Sender = x.Message.SendingUser,
                    SentDate = x.Message.SentDate,
                    Subject = x.Message.Subject,
                    IsFlag = x.IsFlag,
                    IsStar = x.IsStar,
                    IsRead = x.IsRead
                });
        }

        public IPaginated<MMessagingHeader> GetAllMessageHeaders(string username, int? pageindex, int? pagesize, string key)
        {
            var data = db.vw_Messages.Where(x => !x.IsDeleted && x.Receiver.ToLower() == username.ToLower()).ToList();

            var qry = data
                .OrderByDescending(x => x.SentDate).AsQueryable()
                .ToPaginate(pageindex, pagesize)
                .Select(x => new MMessagingHeader()
                {
                    MsgID = x.MsgID,
                    EmailerID = x.EmailerID,
                    IsDeleted = x.IsDeleted,
                    IsFlag = x.IsFlag,
                    IsRead = x.IsRead,
                    ReadOn = x.ReadOn.Value,
                    HasAttachment = x.HasAttachment,
                    Message = x.Message,
                    Sender = x.Sender,
                    SentDate = x.SentDate,
                    Subject = x.Subject,
                    IsStar = x.IsStar,
                    //EmailID = x.EmailID,
                    MsgEmailerID = x.MsgEmailerID,
                    MsgReceiverID = x.MsgReceiverID,
                    Receiver = x.Receiver,
                    ReceiverID = x.ReceiverID
                });

            return qry;
        }

        public MMessaging GetMessageByID(int id)
        {
            var msg = db.Messages.Single(x => x.ID == id);
            return ConvertToMMessaging(msg);
        }

        public MMessaging ConvertToMMessaging(Message mssg)
        {
            MMessaging msg = new MMessaging()
            {
                ID = mssg.ID,
                HasAttachment = mssg.HasAttachment,
                Message = mssg.MainMessage,
                Sender = mssg.SendingUser,
                SentDate = mssg.SentDate,
                Subject = mssg.Subject,
            };

            if (mssg.MessageReceivers != null)
            {
                mssg.MessageReceivers.ToList()
                    .ForEach(x => msg.Receivers.Add(ConvertToMMessageReceiver(x)));
            }

            if (mssg.MessageEmailers != null)
            {
                mssg.MessageEmailers.ToList()
                    .ForEach(x => msg.Emailers.Add(ConvertToMMessageEmailers(x)));
            }
            return msg;
        }

        public MMessageEmailer ConvertToMMessageEmailers(MessageEmailer x)
        {
            MMessageEmailer msgEmail = new MMessageEmailer()
            {
                MessageID = x.MessageID,
                EmailAddress = x.EmailAddress
            };
            return msgEmail;
        }

        public MMessageReceiver ConvertToMMessageReceiver(MessageReceiver x)
        {
            MMessageReceiver msgRec = new MMessageReceiver()
            {
                IsDeleted = x.IsDeleted,
                IsFlag = x.IsFlag,
                IsRead = x.IsRead,
                IsStar = x.IsStar,
                ReadOn = x.ReadOn,
                Receiver = x.ReceivingUser
            };
            return msgRec;
        }
    }
}
