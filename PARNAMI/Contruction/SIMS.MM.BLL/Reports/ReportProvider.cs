﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SIMS.MM.BLL.Reports
{
    using DAL;
    using MODEL;
    using System.Collections;
    using SIMS.MM.MODEL.Reports;

   public class ReportProvider:IReport
    {
       //estrodb2Entities1 db = new estrodb2Entities1();
       MMReportsDBDataContext dc = new MMReportsDBDataContext();
       public IEnumerable<MPOReport> GetPODetailsByLine(DateTime? startdate, DateTime? enddate, int? siteid, int? vendorid, int? itemid, int? itemcatid, int? sizeid, int? brandid)
       {
           return dc.GetPODetailsByLine(startdate, enddate, siteid, vendorid, itemid, itemcatid, sizeid, brandid)
               .Select(x =>
                   new MPOReport()
                   {
                       PRNO = x.PRID,
                       Site = x.Site,
                       OrderDate = x.CreatedOn,
                       MaterialCategory = x.ItemCategory,
                       Material = x.Item,
                       MaterialID = x.ItemID,
                       CategoryID = x.CategoryID,
                       MU = x.MU,
                       PerUnitPrice = (double)x.UnitPrice,
                       PONO = x.POID,
                       Qty = x.Qty,
                       SizeID = x.SizeID,
                       Size = x.Size,
                       BrandID = x.BrandID,
                       Brand = x.Brand,
                       TotalAmount = (double)x.LinePrice,
                       Vendor = x.Vendor,
                       DisplaySite = x.DisplaySite,
                       DisplayVendor = x.DisplayVendor
                   })
               .ToList();
       }

       public IEnumerable<MGRNReport> GetGRNDetails(DateTime? sdate, DateTime? edate, int? siteid, int? vendorid, int? itemid, int? sizeid, int? brandid)
       {
           return dc.GetGRNDetails(sdate, edate, siteid, vendorid, itemid, sizeid, brandid)
               .Select(x => new MGRNReport()
               {
                   ID = x.ID,
                   CategoryID = x.CategoryID,
                   RefNo = x.ReferenceNo,
                   DateofReceipt = x.ForDate,
                   ItemID = x.ItemID,
                   Item = x.ItemDetail,
                   ItemCategory = x.Item,
                   MU = x.Unit,
                   PerUnitPrice = x.UnitPrice.Value,
                   PONO = x.POID,
                   PRNO = x.PRID,
                   QtyReceived = x.Qty,
                   Remarks = x.Remarks,
                   Site = x.Site,
                   SizeID = x.SizeID,
                   Size = x.Size,
                   BrandID = x.BrandID,
                   Brand = x.Brand,
                   TotalAmount = x.Qty * x.UnitPrice.Value,
                   Vendor = x.Vendor,
                   InvoiceNo = x.InvoiceNo,
                   ChallanNo = x.ChallanNo,
                   DisplaySite = x.DisplaySite,
                   DisplayVendor = x.DisplayVendor,
                   Purchaser = x.CreatedBy
               })
               .ToList();
       }

       public IEnumerable<MDPReport> GetDPDetails(DateTime? sdate, DateTime? edate, int? siteid, int? vendorid, int? itemid, int? sizeid, int? brandid)
       {
           var data1 = dc.GetDirectPurchaseDetails(sdate, edate, siteid, vendorid, itemid, sizeid, brandid).ToList();
           var data = data1
               .Select(x => new MDPReport()
               {
                   ID = x.ID,
                   RefNo = x.ReferenceNo,
                   Date = x.ForDate,
                   InvoiceNo = x.InvoiceNo,
                   ChallanNo = x.challanNo,
                   LineChallanNo = x.LineChallanNo,
                   ItemID = x.ItemID,
                   Item = x.ItemDetail,
                   ItemCategory = x.Item,
                   CategoryID = x.CategoryID,
                   SizeID = x.SizeID,
                   Size = x.Size,
                   BrandID = x.BrandID,
                   Brand = x.Brand,
                   MU = x.Unit,
                   PerUnitPrice = x.UnitPrice,
                   Purchaser = x.CreatedBy,
                   Quantity = x.Qty,
                   Remarks = x.Remarks,
                   TotalAmount = x.Qty * x.UnitPrice,
                   Vendor = x.Vendor,
                   Site = x.Site,
                   DisplaySite = x.DisplaySite,
                   DisplayVendor = x.DisplayVendor,
                   LastUpdatedOn = x.LastUpdatedOn,
               }).ToList();
           return data;
       }

       public IEnumerable<MDirectGRNReport> GetDirectGRNReport(DateTime? sdate, DateTime? edate, int? siteid, int? vendorid, int? itemid, int? sizeid, int? brandid)
       {
           var data1 = dc.GetDirectGRNReport(sdate, edate, siteid, vendorid, itemid, sizeid, brandid).ToList();
           var data = data1
               .Select(x => new MDirectGRNReport()
               {
                   ID = x.ID,
                   RefNo = x.ReferenceNo,
                   InvoiceNo = x.InvoiceNo,
                   ChallanNo = x.ChallanNo,
                   LineChallanNo = x.LineChallanNo,
                   ItemID = x.ItemID,
                   Item = x.ItemDetail,
                   ItemCategory = x.Item,
                   CategoryID = x.CategoryID,
                   SizeID = x.SizeID,
                   Size = x.Size,
                   BrandID = x.BrandID,
                   Brand = x.Brand,
                   MU = x.Unit,
                   PerUnitPrice = x.UnitPrice.Value,
                   Purchaser = x.CreatedBy,
                   Remarks = x.Remarks,
                   TotalAmount = x.Qty * x.UnitPrice.Value,
                   Vendor = x.Vendor,
                   Site = x.Site,
                   DisplaySite = x.DisplaySite,
                   DisplayVendor = x.DisplayVendor,
                   LastUpdatedOn = x.LastUpdatedOn,
                   PONO = x.POID,
                   PRNO = x.PRID,
                   QtyReceived = x.Qty,
                   DateofReceipt = x.ForDate
               }).ToList();
           return data;
       }

       public IEnumerable<MCPReport> GetCPdetails(DateTime? sdate, DateTime? edate, int? siteid, int? vendorid, int? itemid, int? sizeid, int? brandid)
       {
           var data = dc.GetCashPurchaseDetails(sdate, edate, siteid, vendorid, itemid, sizeid, brandid)
                .Select(x => new MCPReport()
                {
                    Date = x.ForDate,
                    InvoiceNo = x.InvoiceNo,
                    ChallanNo = x.ChallanNo,
                    ItemID = x.ItemID,
                    ItemCategory = x.Item,
                    Item = x.ItemDetail,
                    SizeID = x.SizeID,
                    Size = x.Size,
                    BrandID = x.BrandID,
                    Brand = x.Brand,
                    MU = x.Unit,
                    PerUnitPrice = x.UnitPrice,
                    Purchaser = x.CreatedBy,
                    Quantity = x.Qty,
                    ID = x.ID,
                    CategoryID = x.CategoryID,
                    RefNo = x.ReferenceNo,
                    Remarks = x.Remarks,
                    Site = x.Site,
                    TotalAmount = x.Qty * x.UnitPrice,
                    Vendor = x.vendor,
                    DisplaySite = x.DisplaySite,
                    DisplayVendor = x.DisplayVendor
                });
          return data;
           //throw new NotImplementedException();
       }

       public IEnumerable<MConsumptionReport> GetConsumptionDetails(DateTime? sdate, DateTime? edate, int? siteid, int? vendorid, int? itemid, int? sizeid, int? brandid)
       {
           var data = dc.GetConsumptionDetails(sdate, edate, siteid, vendorid, itemid, sizeid, brandid)
               .Select(x => new MConsumptionReport()
               {
                   CategoryID = x.CategoryID,
                   ForDate = x.ForDate,
                   ID = x.ID,
                   RefNo = x.ReferenceNo,
                   Item = x.ItemDetail,
                   ItemCategory = x.Item,
                   ItemID = x.ItemID,
                   SizeID = x.SizeID,
                   Size = x.Size,
                   BrandID = x.BrandID,
                   Brand = x.Brand,
                   LineID = x.LineID,
                   Qty = x.Qty,
                   Remarks = x.Remarks,
                   Site = x.Site,
                   Type = x.Type,
                   Unit = x.Unit,
                   DisplaySite = x.DisplaySite,
                   CreatedBy = x.CreatedBy
               });
           return data;
       }

       public IEnumerable<MStockReport> GetDetailedStocReport(DateTime? sdate, DateTime? edate, int? siteid, int? itemid, int? sizeid, int? brandid)
       {
           var data1 = dc.GetDetailedStockReport(sdate, edate, siteid, itemid, sizeid, brandid).ToArray();
           var data = data1.Select(x => new MStockReport()
               {
                   ClosingBalance = x.ClosingBalance,
                   OpeningBalance = x.OpeningBalance,
                   ItemCategory = x.Item,
                   Item = x.Name,
                   MU = x.MeasurementID,
                   QtyIn = x.QtyIn,
                   QtyOut = x.QtyOut,
                   ItemID = x.ItemID,
                   SizeID = x.SizeID,
                   Size = x.Size,
                   BrandID = x.BrandID,
                   Brand = x.Brand,
                   CategoryID = x.CategoryID,
                   DisplaySite = x.DisplaySite,
               });
           return data.AsQueryable();
       }

       public IEnumerable<MConsumptionReport> TotalReceiveByDates(DateTime? date1, DateTime? date2)
       {
           throw new NotImplementedException();
       }

       public IEnumerable<MMTRReport> TotalReceiveBySiteAndDates(int? siteid, DateTime? date1, DateTime? date2)
       {
           throw new NotImplementedException();
       }

       public IEnumerable<MMOReport> GetMOdetails(DateTime? sdate, DateTime? edate, int? siteid, int? itemid, int? sizeid, int? brandid)
       {
           var data = dc.GetMODetails(sdate, edate, siteid, itemid, sizeid, brandid)
               .OrderByDescending(x => x.MTRID)
               .Select(x => new MMOReport()
               {
                   ID = x.MTRID,
                   DisplayReceivingSite = x.Requesting_Site,
                   DisplayTransferringSite = x.Transferring_Site,
                   RefNo = x.ReferenceNo,
                   ItemID = x.ItemID,
                   ItemCategory = x.Category_Name,
                   Item = x.Item_Name,
                   SizeID = x.SizeID,
                   Size = x.Size,
                   BrandID = x.BrandID,
                   Brand = x.Brand,
                   RequestingQty = x.RequiredQty,
                   ApprovedQty = x.ApprovedQty,
                   PendingQty = x.InTransitQty,
                   ReleasedQty = x.RequiredQty,
                   TransferedQty = x.TransferredQty,
                   Remarks = x.Remarks,
                   Date = x.TransferBefore,
                   Status = x.Status,
                   MU = x.MU
               });
           return data;
       }

       public IEnumerable<MBilledReceivings> GetBilledReceivings(DateTime? sdate, DateTime? edate, int? siteid, int? vendorid, int? itemid, int? sizeid, int? brandid)
       {
           var data1 = dc.GetBilledReceivingDetails(sdate, edate, siteid, vendorid, itemid, sizeid, brandid).ToList();
           var data = data1
               .Select(x => new MBilledReceivings()
               {
                   ID = x.ID,
                   RefNo = x.ReferenceNo,
                   InvoiceNo = x.InvoiceNo,
                   ChallanNo = x.ChallanNo,
                   LineChallanNo = x.LineChallanNo,
                   ItemID = x.ItemID,
                   Item = x.ItemDetail,
                   ItemCategory = x.Item,
                   CategoryID = x.CategoryID,
                   SizeID = x.SizeID,
                   Size = x.Size,
                   BrandID = x.BrandID,
                   Brand = x.Brand,
                   MU = x.Unit,
                   PerUnitPrice = x.UnitPrice.Value,
                   Purchaser = x.CreatedBy,
                   Remarks = x.Remarks,
                   TotalAmount = x.Qty * x.UnitPrice.Value,
                   Vendor = x.Vendor,
                   Site = x.Site,
                   DisplaySite = x.DisplaySite,
                   DisplayVendor = x.DisplayVendor,
                   LastUpdatedOn = x.LastUpdatedOn,
                   QtyReceived = x.Qty,
                   DateofReceipt = x.ForDate,
                   ApprovedBy = x.ApprovedBy,
                   ApprovedOn = x.ApprovedOn,
                   BillDate = x.BillDate,
                   BillingID = x.BillingID,
                   BillingLineID = x.BillingLineID,
                   BillingRemarks = x.BillingRemarks,
                   BillNo = x.BillNo,
                   BillRefNo = x.BillRefNo,
                   Cartage = x.Cartage,
                   Deductions = x.Deductions,
                   DefaultPaymentDays = x.DefaultPaymentDays,
                   GrossAmount = x.GrossAmount,
                   NetAmount = x.NetAmount,
                   PaymentNotBefore = x.PaymentNotBefore,
                   Surcharge = x.Surcharge,
                   TotalBillAmount = x.TotalAmount,
                   VAT = x.VAT
               }).ToList();
           return data;
       }

       public IEnumerable<MDirectGRNReport> GetUnBilledReceivings(DateTime? sdate, DateTime? edate, int? siteid, int? vendorid, int? itemid, int? sizeid, int? brandid)
       {
           var data1 = dc.GetUnBilledReceivingDetails(sdate, edate, siteid, vendorid, itemid, sizeid, brandid).ToList();
           var data = data1
               .Select(x => new MDirectGRNReport()
               {
                   ID = x.ID,
                   RefNo = x.ReferenceNo,
                   InvoiceNo = x.InvoiceNo,
                   ChallanNo = x.ChallanNo,
                   LineChallanNo = x.LineChallanNo,
                   ItemID = x.ItemID,
                   Item = x.ItemDetail,
                   ItemCategory = x.Item,
                   CategoryID = x.CategoryID,
                   SizeID = x.SizeID,
                   Size = x.Size,
                   BrandID = x.BrandID,
                   Brand = x.Brand,
                   MU = x.Unit,
                   PerUnitPrice = x.UnitPrice.Value,
                   Purchaser = x.CreatedBy,
                   Remarks = x.Remarks,
                   TotalAmount = x.Qty * x.UnitPrice.Value,
                   Vendor = x.Vendor,
                   Site = x.Site,
                   DisplaySite = x.DisplaySite,
                   DisplayVendor = x.DisplayVendor,
                   LastUpdatedOn = x.LastUpdatedOn,
                   PONO = x.POID,
                   PRNO = x.PRID,
                   QtyReceived = x.Qty,
                   DateofReceipt = x.ForDate
               }).ToList();
           return data;
       }
    }
}
