﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SIMS.MM.MODEL;
using SIMS.MM.DAL;
using System.Collections.ObjectModel;

namespace SIMS.MM.BLL
{
    public class BillingDataProvider : IBillingDataProvider
    {
        parnamidb1Entities db = new parnamidb1Entities();
        MMReportsDBDataContext db1 = new MMReportsDBDataContext();

        public IPaginated<MReceiving> GetReceivings(string username, int? pageindex, int? pagesize, string key, int? vendorid, int? siteid, bool showall = true)
        {
            var qry = db.Receivings
               .FilterByVendor(vendorid)
               .FilterBySite(siteid)
               .OrderByDescending(x => x.ID).AsQueryable();
            if (!showall)
            {
                var ids = db.GetAllowedSiteIDs(username, showall);
                qry = qry.Where(x => ids.Contains(x.BUID));

            }
            return qry.ToList()
              .ToPaginate(pageindex, pagesize)
              .Select(x => new MReceiving()
              {
                  TransactionID = x.ID,
                  ReferenceNo = x.ReferenceNo,
                  PurchaseDate = x.ForDate,
                  Vendor = x.Vendor.DisplayVendor,
                  Site = x.BUs.DisplaySite,
                  Remarks = x.Remarks,
                  ChallanNo = x.ChallanNo,
                  TransactionTypeID = x.TypeID,
                  CreatedBy = x.CreatedBy,
                  CreatedOn = x.CreatedOn,
                  LastUpdatedBy = x.LastUpdatedBy,
                  LastUpdaetdOn = x.LastUpdatedOn,
                  InvoiceNo = x.InvoiceNo,
              });
        }

        public MReceiving GetReceivingforBillByID(int id, string Type)
        {
            if (Type == "Direct")
            {
                var data = db.DirectPurchases.Single(x => x.ID == id);
                return ConvertToMReceiving(data);
            }

            var grndata = db.AllGRNs.Single(x => x.ID == id);
            return ConvertToMReceiving(grndata);
        }

        public MReceiving ConvertToMReceiving(InventoryTransaction _it)
        {
            MReceiving mrec = new MReceiving()
            {
                ReferenceNo = _it.ReferenceNo,
                TransactionID = _it.ID,
                TransactionTypeID = _it.TypeID,
                SiteID = _it.BUID,
                VendorID = _it.VendorID,
                TransporterID = _it.TransitID,
                ChallanNo = _it.ChallanNo,
                CreatedBy = _it.CreatedBy,
                CreatedOn = _it.CreatedOn,
                InvoiceNo = _it.InvoiceNo,
                LastUpdaetdOn = _it.LastUpdatedOn,
                LastUpdatedBy = _it.LastUpdatedBy,
                PurchaseDate = _it.ForDate,
                Purchaser = _it.Requester,
                ReceivingRemarks = _it.Remarks,
                Site = _it.BUs.Name,
                VehicleDetails = _it.VehicleDetails,
                //Vendor=_it.Vendor.Name
            };
            if (_it.Vendor != null) mrec.Vendor = _it.Vendor.Name;
            mrec.ReceivingLines = new ObservableCollection<MReceivingLine>();
            _it.InventoryTransactionDetails.Select(x => ConvertToRLines(x)).ToList()
                .ForEach(x => mrec.ReceivingLines.Add(x));
            return mrec;
        }

        public MReceivingLine ConvertToRLines(InventoryTransactionDetail _itd)
        {
            MReceivingLine mrline = new MReceivingLine()
            {
                LineID = _itd.ID,
                ItemID = _itd.ItemID,
                TransactionTypeID = _itd.TypeID,
                MTRID = _itd.MTRID,
                MTRLineID = _itd.MTRLineID,
                POID = _itd.POID,
                PRID = _itd.PRID,
                PRLineID = _itd.PRLineID,
                TransactionID = _itd.HeaderID,
                Item = _itd.Item.DisplayName,
                LineRemarks = _itd.Remarks,
                Quantity = _itd.Qty,
                TruckNo = _itd.TruckNo,
                LineChallanNo = _itd.ChallanNo
            };
            if (_itd.PRDetail != null)
            {
                mrline.OrderedQty = _itd.Qty;
            }
            if (_itd.CashPurchaseDetail != null)
            {
                mrline.UnitPrice = (double)_itd.CashPurchaseDetail.UnitPrice;
                mrline.LinePrice = (double)_itd.CashPurchaseDetail.LinePrice;
            }
            if (_itd.TruckMeasurement != null)
            {
                if (mrline.TruckParameters == null) mrline.TruckParameters = new MTruckParameter();
                mrline.TruckParameters.ID = _itd.TruckMeasurement.ID;
                mrline.TruckParameters.Length = _itd.TruckMeasurement.Lth;
                mrline.TruckParameters.Breadth = _itd.TruckMeasurement.Bth;
                mrline.TruckParameters.Height = _itd.TruckMeasurement.Ht;
            }
            return mrline;
        }

        public MBilling GetBillbyId(int billid)
        {
            var bill = db.Billings.Single(x => x.ID == billid);
            var paymentdetails = db1.GetVendorPaymentDetailsagainstBill(billid);
            GetVendorPaymentDetailsagainstBillResult item = new GetVendorPaymentDetailsagainstBillResult();
            if(paymentdetails.Count() != 0)
                item = paymentdetails.First();
            var obj = ConvertToMBill(bill);
            if (bill.PaidAmount != 0)
            {
                obj.PaymentDate = item.PaymentDate;
                obj.CurrentPayment = item.CurrentPayment;
                obj.PaymentRefno = item.RefNo;
                obj.TotalPaymentAmount = item.Total;
                obj.TotalPaidAmount = item.Paid;
                obj.PaymentStatus = item.Status;
                obj.IsPaid = true;
            }
            return obj;
        }

        public MBilling ConvertToMBill(Billing bill)
        {
            MBilling mbill = new MBilling()
            {
                ID = bill.ID,
                ApprovedBy = bill.ApprovedBy,
                ApprovedOn = bill.ApprovedOn,
                BillDate = bill.BillDate,
                BillNo = bill.BillNo,
                Deductions = bill.Deductions,
                DefaultPaymentDays = bill.DefaultPaymentDays,
                PANNo = bill.PANNo,
                PaymentNotBefore = bill.PaymentNotBefore,
                Remarks = bill.Remarks,
                TotalAmount = bill.TotalAmount,
                VendorID = bill.VendorID,
                Status = bill.Status,
                IsApproved = bill.IsApproved,
                SiteID = bill.SiteID,
                Vendor = bill.Vendor.DisplayVendor,
                Site = bill.BUs.DisplaySite,
                GrossAmount = bill.GrossAmount,
                Cartage = bill.Cartage,
                SurchargePercent = bill.Surcharge,
                VATPercent = bill.VAT,
                NetAmount = bill.NetAmount,
            };

            mbill.BillingLines = new ObservableCollection<MBillingLines>();
            bill.BillingDetails.Select(x => ConvertToMBillDetails(x)).ToList()
                .ForEach(x => mbill.BillingLines.Add(x));
            mbill.BillingVerifications = new ObservableCollection<MBillingVerified>();
            bill.BillingVerifications.Select(x => ConvertToMBillVerification(x)).ToList()
                .ForEach(x => mbill.BillingVerifications.Add(x));
            return mbill;
        }

        private MBillingVerified ConvertToMBillVerification(BillingVerification x)
        {
            MBillingVerified bilver = new MBillingVerified()
            {
                ID = x.ID,
                BillingID = x.BillingID,
                //VerificationCounter = x.VerificationCounter.Value,
                VerifiedBy = x.VerifiedBy,
                VerifiedOn = x.VerifiedOn,
            };
            return bilver;
        }

        public MBillingLines ConvertToMBillDetails(BillingDetail _details)
        {
            MBillingLines billline = new MBillingLines()
            {
                ID = _details.ID,
                LineID = _details.LineID,
                ItemID = _details.ItemID,
                SizeID = _details.SizeID,
                BrandID = _details.BrandID,
                BillingID = _details.BillingID,
                LinePrice = _details.LinePrice,
                LineRemarks = _details.LineRemarks,
                SuggestedPrice = _details.SuggestedPrice,
                SuggestedQty = _details.SuggestedQty,
                ActualPrice = _details.ActualPrice,
                ActualQty = _details.ActualQty,
                TransactionType = _details.Type,
                TransactionDetailsID = _details.TransactionDetailsID,
                TransactionID = _details.TransactionID,
                ChallanNo = _details.ChallanNo,
                LineChallanNo = _details.LineChallanNo,
                Date = _details.Date,
                ReferenceNo = _details.ReferenceNo,
                Item = _details.Item.DisplayName,
            };
            if (_details.ItemBrand != null)
                billline.Brand = _details.ItemBrand.Value;
            if (_details.ItemSize != null)
                billline.Size = _details.ItemSize.Value;
            return billline;
        }

        //public IPaginated<MBilling> GetAllBills(string username, int? pageindex, int? pagesize, string key, int? vendorid, int? siteid, string Filter)
        //{
        //    var qry = db.Billings
        //       .FilterByVendor(vendorid)
        //       .FilterBySite(siteid)
        //       .FilterByfilter(Filter)
        //       .OrderByDescending(x => x.ID).AsQueryable();

        //    return qry.ToList()
        //      .ToPaginate(pageindex, pagesize)
        //      .Select(x => new MBilling()B
        //      {
        //          ID = x.ID,
        //          ApprovedBy = x.ApprovedBy,
        //          ApprovedOn = x.ApprovedOn,
        //          BillDate = x.BillDate,
        //          BillNo = x.BillNo,
        //          Deductions = x.Deductions,
        //          DefaultPaymentDays = x.DefaultPaymentDays,
        //          PANNo = x.PANNo,
        //          PaymentNotBefore = x.PaymentNotBefore,
        //          Remarks = x.Remarks,
        //          VendorID = x.VendorID,
        //          Status = x.Status,
        //          IsApproved = x.IsApproved,
        //          SiteID = x.SiteID,
        //          Site = x.BUs.DisplaySite,
        //          Vendor = x.Vendor.DisplayVendor,
        //          Cartage = x.Cartage,
        //          SurchargePercent = x.Surcharge,
        //          VATPercent = x.VAT,
        //          TotalAmnt = x.TotalAmount,
        //          NetAmnt = x.NetAmount,
        //          GrossAmnt = x.GrossAmount
        //      });
        //}

        public IPaginated<MBilling> GetAllBillsNew(string username, int? pageindex, int? pagesize, string key, int? vendorid, int? siteid, string Filter)
        {
            var qry = db1.GetBillingHeaders(username, key, vendorid, siteid, Filter)
               .OrderByDescending(x => x.BillDate).AsQueryable();

            return qry.ToList()
              .ToPaginate(pageindex, pagesize)
              .Select(x => new MBilling()
              {
                  ID = x.ID,
                  ApprovedBy = x.ApprovedBy,
                  ApprovedOn = x.ApprovedOn,
                  BillDate = x.BillDate,
                  BillNo = x.BillNo,
                  Deductions = x.Deductions,
                  DefaultPaymentDays = x.DefaultPaymentDays,
                  PANNo = x.PANNo,
                  PaymentNotBefore = x.PaymentNotBefore,
                  Remarks = x.Remarks,
                  VendorID = x.VendorID,
                  Status = x.Status,
                  IsApproved = x.IsApproved,
                  SiteID = x.SiteID,
                  Site = x.Site,
                  Vendor = x.Vendor,
                  Cartage = x.Cartage,
                  SurchargePercent = x.Surcharge,
                  VATPercent = x.VAT,
                  TotalAmnt = x.TotalAmount,
                  NetAmnt = x.NetAmount,
                  GrossAmnt = x.GrossAmount,
                  PaymentStatus = x.PaymentStatus,
                  TotalBillingLines = x.TotalBillingLines,
                  PaidBillingLines = x.PaidBillingLines,
                  UnpaidBillignLines = x.UnPaidBillingLines,
                  TotalPaymentAmount = x.TotalPayment,
                  PaymentDate = x.PaymentDate,
                  CurrentPayment = x.CurrentPayment,
                  PaymentRefno = x.RefNo,
                  TotalPaidAmount = x.Paid
              });
        }


        public IPaginated<MReceivingLine> GetReceivingLines(string username, int? pageindex, int? pagesize, string key, int? itemid, int? vendorid, int? siteid, DateTime? sdate, DateTime? edate, string challan)
        {
            var qry = db1.GetPendingReceivingsForBilling(key, vendorid, siteid, itemid, sdate, edate, challan)
               .OrderByDescending(x => x.ID).AsQueryable();

            var data = qry
              .ToPaginate(pageindex, pagesize)
              .Select(x => new MReceivingLine()
              {
                  LineID = x.LineID,
                  TransactionID = x.HeaderID,
                  Quantity = x.Qty,
                  LineRemarks = x.Remarks,
                  ItemID = x.ItemID,
                  Item = x.Item,
                  UnitPrice = (double)x.UnitPrice,
                  LinePrice = (double)x.LinePrice,
                  LineChallanNo = x.LineChallanNo,
                  TruckNo = x.TruckNo,
                  TransactionTypeID = x.TypeID,
                  TypeID = x.TypeID,
                  ReceivingChallanNo = x.ChallanNo,
                  ReferenceNo = x.ReferenceNo,
                  VendorID = x.VendorID,
                  CreationDate = x.CreatedOn,
                  Site = x.Site,
                  SiteID = x.BUID,
                  Vendor = x.Vendor,
              });
            return data;
        }
    }
}
