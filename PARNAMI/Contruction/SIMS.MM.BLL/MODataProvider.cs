﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SIMS.MM.BLL
{
    using DAL;
    using SIMS.MM.MODEL;
    using System.Collections.ObjectModel;    
    public class MODataProvider:IMODataProvider
    {
        parnamidb1Entities db = new parnamidb1Entities();
        MMReportsDBDataContext db1 = new MMReportsDBDataContext();
        //public IPaginated<MMOHeader> GetMOHeaders(string username, int? pageindex, int? pagesize, string Key, int? vendorid, int? sideid, int? sideid2, bool showall = true)
        //{
        //    var qry= db.MTRs
        //        .FilterBySite(sideid,sideid2)
        //        .OrderByDescending(x => x.ID)
        //        .AsQueryable();
        //    if (!showall)
        //    {
        //        var ids = db.GetAllowedSiteIDs(username, showall);
        //        qry = qry.Where(x => ids.Contains(x.TransferringSiteID) 
        //            || ids.Contains(x.RequestedSiteID));

        //    }
        //     return qry.ToList()
        //        .ToPaginate(pageindex, pagesize)                
        //        .Select(x => new MMOHeader()
        //        {
        //            Date=x.TransferBefore,
        //            ID=x.ID,          
        //            DisplayRequestedBySite=x.BUs.DisplaySite,
        //            DisplayTransferringSite=x.BUs1.DisplaySite,
        //            Remarks=x.Remarks,
        //            RequestedBy=x.RequestedBy,
        //            RequestedBySite=x.BUs.Name,
        //            TransferringSite=x.BUs1.Name,                    
        //            CanRelease=x.MTRLines.Any(y=>y.RequiredQty> y.ReleasedQty)&&!x.IsClosed,                    
        //            CanReceive = x.MTRLines.Any(y => y.InTransitQty>0&&!x.IsClosed),
        //            IsClosed=x.IsClosed,
        //            Status=x.ClosedStatus,
        //            ClosedBy=x.ClosedBy,
        //            ReferenceNo=x.ReferenceNo,      
        //            //Transporter=x.TransporterName
        //        });
             
        //}

        public IPaginated<MMOHeader> GetMOHeadersNew(string username, int? pageindex, int? pagesize, string Key, int? sideid, int? sideid2, bool showall = true)//Using Table Valued Funtion Functions
        {
            var MOs = db1.GetMOHeaders(username, Key, sideid, sideid2, showall);
            return MOs.OrderByDescending(x => x.ID)
                .ToPaginate(pageindex, pagesize)
                .Select(x => new MMOHeader()
                {
                    Date = x.TransferBefore,
                    ID = x.ID,
                    DisplayRequestedBySite = x.DisplayReqSite,
                    DisplayTransferringSite = x.DisplayTransSite,
                    Remarks = x.Remarks,
                    RequestedBy = x.RequestedBy,
                    CanRelease = x.CanRelease.Value,
                    CanReceive = x.CanRecieve.Value,
                    IsClosed = x.IsClosed,
                    Status = x.Status,
                    ClosedBy = x.ClosedBy,
                    ReferenceNo = x.ReferenceNo,
                    ChallanNOs = x.ChallanNOs
                });


        }

        public MMO GetMObyId(int mtrid)
        {
            var mo = db.MTRs.Single(x => x.ID == mtrid);
            return ConvertToMMO(mo);
        }

        private MMO ConvertToMMO(MTR _mo)
        {
           MMO mmo= new MMO()
            {
                ID = _mo.ID,
                RequestingSite = _mo.BUs.Name,
                RequestingSiteID = _mo.RequestedSiteID,
                Requester = _mo.RequestedBy,
                TransferringSite = _mo.BUs1.Name,
                TransferringSiteID = _mo.TransferringSiteID,
                TransferDate = _mo.TransferBefore,
                Remarks = _mo.Remarks,
                Status=_mo.ClosedStatus,
                IsClosed=_mo.IsClosed,
                ReferenceNo=_mo.ReferenceNo,
                TransporterID=_mo.TransporterID,
                RaisedBy=_mo.RaisedBy,
                RaisedOn=_mo.RaisedOn,
            };
           mmo.MOLines = new ObservableCollection<MMOLine>();
           _mo.MTRLines.Select(x => ConvertToMOLines(x)).ToList()
            .ForEach(x=>mmo.MOLines.Add(x));
           return mmo;
        }

        private MMOLine ConvertToMOLines(MTRLine _mtrline)
        {
            MMOLine mmo_line = new MMOLine()
            {
                ID = _mtrline.MTRID,
                LineID = _mtrline.LineID,
                ItemID = _mtrline.ItemID,
                Item = _mtrline.Item.DisplayName,
                SizeID = _mtrline.SizeID,
                BrandID = _mtrline.BrandID,
                LineRemarks = _mtrline.Remarks,
                InTransitQty = _mtrline.InTransitQty,
                Quantity = _mtrline.RequiredQty,
                ReleasedQty = _mtrline.ReleasedQty,
                ReceivedQty = _mtrline.TransferredQty,
                LineChallanNo = _mtrline.ChallanNo
            };
            if (_mtrline.ItemBrand != null)
                mmo_line.Brand = _mtrline.ItemBrand.Value;
            if (_mtrline.ItemSize != null)
                mmo_line.Size = _mtrline.ItemSize.Value;
            return mmo_line;
        }


        public MReceiving GetMReceivingforMoRelease(int moid)
        {
            var data = db.MTRs.Single(x => x.ID == moid);
            return ConvertMTR_TO_MReleasing(data);            
        }

        private MReceiving ConvertMTR_TO_MReleasing(MTR _it)
        {
            MReceiving mrec = new MReceiving()
            {
                TransactionID = _it.ID,
                SiteID = _it.RequestedSiteID,
                TranferSiteID = _it.TransferringSiteID,
                CreatedBy = _it.ReleasedBy,
                CreatedOn = DateTime.UtcNow.AddHours(5.5),
                ReceivingRemarks = _it.Remarks,
                Site = _it.BUs.Name,
                TransferSite = _it.BUs1.Name,
                TransporterID = _it.TransporterID,
                TransactionTypeID = "MORelease",
            };
            
            mrec.ReceivingLines = new ObservableCollection<MReceivingLine>();
            _it.MTRLines.Select(x => ConvertToRLines(x)).ToList()
                .ForEach(x => mrec.ReceivingLines.Add(x));
            return mrec;
        }

        public MReceivingLine ConvertToRLines(MTRLine _itd)
        {
            MReceivingLine mrline = new MReceivingLine()
            {
                LineID = _itd.LineID,
                Item = _itd.Item.DisplayName,
                ItemID = _itd.ItemID,
                SizeID = _itd.SizeID,
                BrandID = _itd.BrandID,
                MTRID = _itd.MTRID,
                MTRLineID = _itd.LineID,
                LineRemarks = _itd.Remarks,
                Quantity = 0,
                EarlierReleasedQty=_itd.ReleasedQty??0,
                OrderedQty = _itd.RequiredQty,
                TransactionTypeID = "MORelease",
                LineChallanNo = _itd.ChallanNo
            };
            if (_itd.ItemBrand != null)
                mrline.Brand = _itd.ItemBrand.Value;
            if (_itd.ItemSize != null)
                mrline.Size = _itd.ItemSize.Value;
            return mrline;
        }

        public MReceiving GetMReceivingforMoReceiving(int moid)
        {
            var data = db.MTRs.Single(x => x.ID == moid);
            return ConvertMTR_TO_MReceiving(data); 
        }

        private MReceiving ConvertMTR_TO_MReceiving(MTR _it)
        {
            MReceiving mrec = new MReceiving()
            {

                TransactionID = _it.ID,
                SiteID = _it.RequestedSiteID,
                CreatedBy = _it.ReleasedBy,
                CreatedOn = DateTime.UtcNow.AddHours(5.5),
                ReceivingRemarks = _it.Remarks,
                Site = _it.BUs.Name,
                MOID=_it.ID,
                TransferSite=_it.BUs1.Name,
                TranferSiteID=_it.TransferringSiteID,
                TransactionTypeID = "MOReceive",
                ReceivedBy = _it.ReceivedBy,
                ReceivedOn=DateTime.UtcNow.AddHours(5.5),
                ReleaseOn=DateTime.UtcNow.AddHours(5.5),
                ReleasedBy=_it.ReleasedBy
            };

            mrec.ReceivingLines = new ObservableCollection<MReceivingLine>();
            _it.MTRLines.Select(x => ConvertToRLines2(x)).ToList()
                .ForEach(x => mrec.ReceivingLines.Add(x));
            return mrec;
        }

        public MReceivingLine ConvertToRLines2(MTRLine _itd)
        {
            MReceivingLine mrline = new MReceivingLine()
            {
                LineID = _itd.LineID,
                Item = _itd.Item.DisplayName,
                ItemID = _itd.ItemID,
                SizeID = _itd.SizeID,
                BrandID = _itd.BrandID,
                MTRID = _itd.MTRID,
                MTRLineID = _itd.LineID,
                LineRemarks = _itd.Remarks,
                Quantity = 0,
                EarlierReceivedQty = _itd.TransferredQty?? 0,
                OrderedQty = _itd.RequiredQty,
                ReleasedQty=_itd.ReleasedQty.Value,
                TransactionTypeID="MOReceive",
                LineChallanNo = _itd.ChallanNo
            };
            if (_itd.ItemBrand != null)
                mrline.Brand = _itd.ItemBrand.Value;
            if (_itd.ItemSize != null)
                mrline.Size = _itd.ItemSize.Value;
            return mrline;
        }
    }
}
