﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SIMS.TM.Model;
namespace SIMS.TM.BLL
{
    public interface ITenderDataProvider
    {
        IPaginated<MTender> GetTenderHeaders(string username, int? pageindex, int? pagesize, string Key, int? sideid, bool showall = true);
        MTender GetTenderByID(int TenderID);
    }

    public interface ITenderManager
    {
        MTender CreateNewTender(MTender _NewTender);
        MTender UpdateTender(MTender _UpdatedTender);
    }

    public interface IMBookDataProvider
    {
        IPaginated<MMBook> GetMBookHeaders(string username, int? pageindex, int? pagesize, string Key, DateTime? startDate,DateTime? endDate, int? sideid, bool showall = true);
        MMBook GetMBookByID(int MBookID);
    }

    public interface IMBookManager
    {
        MMBook CreateNewMBook(MMBook _NewMBook);
        MMBook UpdateMBook(MMBook _UpdatedMBook);
    }
}
