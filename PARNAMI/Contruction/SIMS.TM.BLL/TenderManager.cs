﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SIMS.TM.Model;
using SIMS.TM.DAL;
namespace SIMS.TM.BLL
{
    public class TenderManager:ITenderManager
    {
        TMDBDataContext db = new TMDBDataContext();
        public MTender CreateNewTender(MTender _NewTender)
        {
            _NewTender.ID = db.GetMaxTenderID() + 1;            
            var tender = ConvertToTender(_NewTender);
            db.Tenders.InsertOnSubmit(tender);
            db.SubmitChanges();
            return _NewTender;
        }

        private Tender ConvertToTender(MTender _NewTender)
        {
            Tender tender = new Tender()
            {
                ID = _NewTender.ID,
                Title = _NewTender.Title,
                ClientName = _NewTender.ClientName,
                ClientAddress = _NewTender.ClientAddress,
                CurrentOwner = _NewTender.CurrentOwner,
                DueOn = _NewTender.DueOn,
                ExpectedTotal = _NewTender.ExpectedTotal,
                ExpEndDate = _NewTender.ExpEndDate,
                ExpStartDate = _NewTender.ExpStartDate,
                FirstSubmittedOn = _NewTender.FirstSubmittedOn,
                IsWon = _NewTender.IsWon,
                ProjectID = _NewTender.ProjectID,
                SiteAddress = _NewTender.SiteAddress,
                SiteCity = _NewTender.SiteCity,
                SiteLocation = _NewTender.SiteLocation,
                SitePincode = _NewTender.SitePincode,
                SiteState = _NewTender.SiteState,
                Taxes = _NewTender.Taxes,
                TotalAfterTax = _NewTender.TotalAfterTax
            };
            return tender;
        }

        public MTender UpdateTender(MTender _UpdatedTender)
        {
            throw new NotImplementedException();
        }
    }
}
