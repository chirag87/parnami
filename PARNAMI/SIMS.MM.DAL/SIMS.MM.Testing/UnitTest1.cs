﻿using System;
using System.IO;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace SIMS.MM.Testing
{
    using BLL;
    using MM.DAL;
    using SIMS.MM.MODEL;
    using BLL.Reports;
    using SIMS.MM.BLL.Services;
    using SIMS.MM.MODEL.Reports;
    [TestClass]
    public class PurchasingTest
    {
        parnamidb1Entities db = new parnamidb1Entities();  
        [TestMethod]
        public void Receiving_CashPurchase_From_Database()
        {
            ReceivingDataProvider provider = new ReceivingDataProvider();
            var data = provider.GetCashPurachaseHeadersNew(null, null, null, null, null, null, true);
            Assert.IsTrue(data.Count > 0);
        }

        [TestMethod]
        public void Receiving_DirectPurchase_From_Database()
        {
            ReceivingDataProvider provider = new ReceivingDataProvider();
            var data = provider.GetDirectPurchaseHeaders(null, null, null, null, null, null, true);
            Assert.IsTrue(data.Count > 0);

        }

        [TestMethod]
        public void Receiving_Consumption_From_Database()
        {
            ReceivingDataProvider provider = new ReceivingDataProvider();
            var data = provider.GetConsumptionHeadersNew(null, null, null, null, null, true);
            Assert.IsTrue(data.Count > 0);

        }

        [TestMethod]
        public void receiving_grn_from_database()
        {
            ReceivingDataProvider provider = new ReceivingDataProvider();
            var data = provider.GetGRNHeadersNew(null, null, null, null, null, null, true);
            Assert.IsTrue(data.Count > 0);

        }

        [TestMethod]
        public void Receiving_MTR_From_Database()
        {
            MTRDataProvider provider = new MTRDataProvider();
            var data = provider.GetMTRHeaders(null, null, null, null, null, null);
            Console.WriteLine(data.Count + " entries found!");
            Console.WriteLine();
            Assert.IsTrue(data.Count > 0);
        }

        [TestMethod]
        public void Receiving_PRs_From_Database()
        {
            PurchasingDataProvider provider = new PurchasingDataProvider();
            var data = provider.GetPRHeadersNew(null, null, null, null, null, null, true);
            Assert.IsTrue(data.Count > 0);
        }

        [TestMethod]
        public void Receiving_MOs_From_Database()
        {
            MODataProvider provider = new MODataProvider();
            var data = provider.GetMOHeadersNew("sanjay",null, null, null, null, null, false);
            Assert.IsTrue(data.Count > 0);
        }

        [TestMethod]
        public void Receiving_POs_From_Database()
        {
            PurchasingDataProvider provider = new PurchasingDataProvider();
            var data = provider.GetPOHeadersNew("dba", 0, null, null, null, null, true);
            Assert.IsTrue(data.Count > 0);
        }

        [TestMethod]
        public void Receiving_BUs_From_Database()
        {
            MasterDataProvier MP = new MasterDataProvier();
            var data = MP.GetSiteHeaders().ToList();
            Assert.IsTrue(data.Count() > 0);
        }

        [TestMethod]
        public void Receiving_Vendors_From_Database()
        {
            MasterDataProvier mp = new MasterDataProvier();
            var data = mp.GetVendorHeaders().ToList();
            Assert.IsTrue(data.Count > 0);
        }

        [TestMethod]
        public void Receiving_Items_From_Database()
        {
            MasterDataProvier mp = new MasterDataProvier();
            var data = mp.GetItemHeaders().ToList();
            Assert.IsTrue(data.Count > 0);
        }
    
        [TestMethod]
        public void CreatingNewPRTest()
        {
            PurchasingManager manager = new PurchasingManager();
            MPR newpr = new MPR()
            {
                IsApproved = false,
                PRRemarks = "wait for a short period ",
                VendorID = 1,
                Purchaser = "dba",
                PurchaseType = "BuildingMaterial",
                SiteID = 1,
                TotalValue = 19000,
                ApprovalStatus = " After 3 days "
            };
            var line1 = newpr.AddNewLine();
            line1.ItemID = 1025;
            line1.Quantity = 10;
            line1.DeliveryDate = DateTime.Today;
            manager.CreatePR(newpr);
        }

        [TestMethod]
        public void DeletePR_test()
        {
            PurchasingManager m = new PurchasingManager();
            m.DeletePR(1);
        }

        [TestMethod]
        public void CreateNewReceivingTest()
        {
            for (int i = 0; i < 2; i++)
            {
                ReceivingManager manager = new ReceivingManager();
                MReceiving newmr = new MReceiving()
                {
                    //ForDate = DateTime.UtcNow.AddHours(5.5),
                    CreatedBy = "dinesh",
                    LastUpdatedBy = "dba",
                    LastUpdaetdOn = DateTime.UtcNow.AddHours(5.5),
                    SiteID = 1,
                    VendorID = 1,
                    Purchaser = "Dinesh kr."

                };
                var line1 = newmr.AddNewLine();
                line1.ItemID = 1001;
                line1.Quantity = 4;
                manager.CreateNewReceiving(newmr, "dba");
            }

        }

        [TestMethod]
        public void CreateNewCashPurchaseTest()
        {
            for (int i = 0; i < 1; i++)
            {
                ReceivingManager manager = new ReceivingManager();
                MReceiving newmr = new MReceiving()
                {
                    //ForDate = DateTime.UtcNow.AddHours(5.5),
                    CreatedBy = "dinesh",
                    LastUpdatedBy = "dba",
                    LastUpdaetdOn = DateTime.UtcNow.AddHours(5.5),
                    SiteID = 1,
                    VendorID = 1,
                    Purchaser = "Dinesh Kr."

                };
                var line1 = newmr.AddNewLine();
                line1.ItemID = 1001;
                line1.Quantity = 4;
                manager.CreateNewCashPurchase(newmr, "dba");
            }
        }

        [TestMethod]
        public void CreateNewDirectPurchaseTest()
        {
            for (int i = 0; i < 1; i++)
            {
                ReceivingManager manager = new ReceivingManager();
                MReceiving newmr = new MReceiving()
                {
                    //ForDate = DateTime.UtcNow.AddHours(5.5),
                    CreatedBy = "dinesh",
                    LastUpdatedBy = "dba",
                    LastUpdaetdOn = DateTime.UtcNow.AddHours(5.5),
                    SiteID = 1,
                    VendorID = 1,
                    Purchaser = "Dinesh Kr."

                };
                var line1 = newmr.AddNewLine();
                line1.ItemID = 1001;
                line1.Quantity = 4;
                manager.CreateNewDirectPurchase(newmr, "dba");
            }
        }
        [TestMethod]
        public void CreateNewConsumptionTest()
        {
            for (int i = 0; i < 1; i++)
            {
                ReceivingManager manager = new ReceivingManager();
                MReceiving newmr = new MReceiving()
                {
                    //ForDate = DateTime.UtcNow.AddHours(5.5),
                    CreatedBy = "dinesh",
                    LastUpdatedBy = "dba",
                    LastUpdaetdOn = DateTime.UtcNow.AddHours(5.5),
                    SiteID = 1,
                    VendorID = 1,
                    Purchaser = "Dinesh Kr."

                };
                var line1 = newmr.AddNewLine();
                line1.ItemID = 1001;
                line1.Quantity = 4;
                manager.CreateNewConsumption(newmr, "dba");
            }
        }

        [TestMethod]
        public void CreateNewVendorTest()
        {
            MasterManager manager = new MasterManager();
            MVendor mv = new MVendor()
            {
                Name = "TestingVendor",
                Email="dk@gmail.com"

            };
            manager.CreateNewVendor(mv);
        }

        [TestMethod]
        public void UpdatingVendorTest()
        {
            MasterManager m = new MasterManager();
            MVendor s = new MVendor()
            {
                ID = 1,
                Name = "Vendor101",
            };
            m.UpdateVendor(s);

        }

        [TestMethod]
        public void UpdatingTransporterTest()
        {
            MasterManager m = new MasterManager();
            MTransporter s = new MTransporter()
            {
                ID = 2,
                Address = "Faridabad",
                City = "Ballabagrh"
            };
            m.UpdateTransporter(s);

        }

        [TestMethod]
        public void CreateSiteTest()
        {
            MasterManager manager = new MasterManager();
            MSite s = new MSite()
            {
                Name = "IT Department",
                TypeID = "Office"

            };
            manager.CreateNewSite(s);
        }

        [TestMethod]
        public void UpdatingSiteTest()
        {
            MasterManager m = new MasterManager();
            MSite s = new MSite()
            {
                ID = 2,
                Name = "CSE2 Department",
                TypeID = "Site"
            };
            m.UpdateSite(s);

        }

        [TestMethod]
        public void UpdatingPRTest()
        {
            PurchasingManager mp = new PurchasingManager();
            PurchasingDataProvider pd = new PurchasingDataProvider();
            var mpr = pd.GetPRbyId(1);
            mpr.Frieght += 1;
            mp.UpdatePR(mpr);
        }

        [TestMethod]
        public void CreateNewItemTest()
        {
            MasterManager m = new MasterManager();
            MItem mit = new MItem()
            {
                Name = "RAOne1",
                MeasurementUnitID = "Kg",
            };
            string newt = "CategoryTest1234";
            m.CreateNewItem(mit, newt);
        }

        [TestMethod]
        public void CreateNewMTR_Test()
        {
            for (int i = 0; i < 2; i++)
            {
                MTRManager manager = new MTRManager();
                MMTR m = new MMTR()
                {
                    Requester = "Dinesh",
                    RequestingSiteID = 1,
                    TransferringSiteID = 2,
                    TransferDate = DateTime.UtcNow.AddHours(5.5)
                };
                var line = m.AddNewLine();
                line.ItemID = 1056;
                line.Quantity = 34;
                manager.CreateNewMTR(m);
            }
        }

        [TestMethod]
        public void GETPR_BY_ID()
        {
            PurchasingDataProvider m = new PurchasingDataProvider();
            var data = m.GetPRbyId(1);
        }

        [TestMethod]
        public void ApprovePR_test()
        {
            PurchasingManager m = new PurchasingManager();
            m.ApprovePR(83, " dba");
        }

        [TestMethod]
        public void Convert_To_PO_test()
        {
            PurchasingManager m = new PurchasingManager();
            m.ConvertToPO(124);

        }

        [TestMethod]
        public void Get_POs_From_Database()
        {
            PurchasingDataProvider provider = new PurchasingDataProvider();
            var data = provider.GetPOHeadersNew(null, null, null,"2925",null,null,true);
            Assert.IsTrue(data.Count> 0);
        }     
       

        //[TestMethod]
        //public void GETPO_BY_ID()
        //{
        //    PurchasingDataProvider m = new PurchasingDataProvider();
        //    var data = m.GetPObyId(2);
        //}

        //[TestMethod]
        //public void GETMTR_BY_ID()
        //{
        //    MTRDataProvider m = new MTRDataProvider();
        //    var data = m.GetMTRbyId(1);
        //}

        //[TestMethod]
        //public void GetCPbyId_test()
        //{
        //    ReceivingDataProvider provider = new ReceivingDataProvider();
        //    for (int i = 1; i < 10; i++)
        //    {
        //        var data = provider.GetCPbyId(i);
        //        int a = i;
        //    }

        //}

        [TestMethod]
        public void Get_DPbyId_test()
        {
            ReceivingDataProvider provider = new ReceivingDataProvider();
            var data = provider.GetDPbyId(7);
        }

        //[TestMethod]
        //public void GetGRNbyId_test()
        //{
        //    for (int i = 1; i < 20; i++)
        //    {
        //        ReceivingDataProvider provider = new ReceivingDataProvider();
        //        var data = provider.GetGRNbyId(i);
        //    }
        //}

        //[TestMethod]
        //public void GetConsbyId_test()
        //{
        //    ReceivingDataProvider provider = new ReceivingDataProvider();
        //    var data = provider.GetConsbyId(6);
        ////}

       

        #region               ------Reports Test-------
        #region               ------Reports Test-------
        //[TestMethod]
        //public void GetPODetailsByLine_test()
        //{
        //    ReportProvider rp = new ReportProvider();
        //    var data = rp.GetPODetailsByLine(null, null, null, null, null, null);
        //}

        //[TestMethod]
        //public void GetSiteWiseReceiving()
        //{
        //    ReportProvider rp = new ReportProvider();
        //    var data = rp.GetSiteWiseReceiving(null, null, null, null);
        //}

        //[TestMethod]
        //public void GetVendorWiseReceiving()
        //{
        //    ReportProvider rp = new ReportProvider();
        //    var data = rp.GetVendorWiseReceiving(null, null, null, null);
        //}

        //[TestMethod]
        //public void InventoryDetailsBySiteID()
        //{
        //    ReportProvider rp = new ReportProvider();
        //    var data = rp.InventoryDetailsBySiteID(null);
        //}

        //[TestMethod]
        //public void InventoryListforAllSites()
        //{
        //    ReportProvider rp = new ReportProvider();
        //    var data = rp.InventoryListforAllSites(null, null);
        //}

        //[TestMethod]
        //public void TotalReceiveByDates()
        //{
        //    ReportProvider rp = new ReportProvider();
        //    var data = rp.TotalReceiveByDates(null, null);
        //}

        //[TestMethod]
        //public void TotalReceiveBySiteAndDates()
        //{
        //    ReportProvider rp = new ReportProvider();
        //    var data = rp.TotalReceiveBySiteAndDates(null, null, null);
        //}
        #endregion
        #endregion

        //[TestMethod]
        //public void validate_User_Test()
        //{
        //    AuthenticationManager m = new AuthenticationManager();
        //    bool result = m.ValidateUser("dba", "hitech8*");
        //    Assert.IsTrue(result);
        //}

        //[TestMethod]
        //public void HasUserInDatabase()
        //{
        //    AuthenticationManager m = new AuthenticationManager();
        //    bool result = m.HasUser("dba");
        //    Assert.IsTrue(result);
        //}

        //[TestMethod]
        //public void HasUsersInDatabase()
        //{
        //    AuthenticationManager m = new AuthenticationManager();
        //    var data = m.GetAllUserNames();
        //    Assert.IsTrue(data.Count() > 0);
        //}

        [TestMethod]
        public void GetCPReports_test()
        {
            ReportProvider rp = new ReportProvider();
            var data = rp.GetCPdetails(new DateTime(2012, 01, 22), new DateTime(2012, 01, 29), null, 154, null);
        }

        [TestMethod]
        public void GetGetPODetailsByLine_test()
        {
            ReportProvider rp = new ReportProvider();
            var data = rp.GetPODetailsByLine(null, null, null, null, null, null);
        }

        [TestMethod]
        public void GetDPReports_test()
        {
            ReportProvider rp = new ReportProvider();
            var data = rp.GetDPDetails(null, null, null, null, null);
        }

        [TestMethod]
        public void GetStockReports_test()
        {
            ReportProvider rp = new ReportProvider();
            var data = rp.GetDetailedStocReport(null, null, null, null).ToList();
        }

        [TestMethod]
        public void GetGetGRNDetails_test()
        {
            ReportProvider rp = new ReportProvider();
            var itemid = 1037;
            var data = rp.GetGRNDetails(null,null,null,null,itemid);
        }


        [TestMethod]
        public void GetDetailedStocReport_test()
        {
            ReportProvider rp = new ReportProvider();
            var data = rp.GetDetailedStocReport(null, null, null, null);
        }

        [TestMethod]
        public void GetConsumptionDetails_test()
        {
            ReportProvider rp = new ReportProvider();
            var data = rp.GetConsumptionDetails(null, null, null, null, null);
        }

        [TestMethod]
        public void GetAllowedItenId_test()
        {
            PurchasingManager m = new PurchasingManager();
            var data = m.GetAllowedItemId(1, 1);
        }

        [TestMethod]
        public void GetPendingInfo_test()
        {
            PurchasingManager m = new PurchasingManager();
            var data = m.GetPendingInfo(1, 3, 1025);
        }

        [TestMethod]
        public void Email_test()
        {
            PurchasingManager man = new PurchasingManager();
            man.EmailPR(113);
        }

        [TestMethod]
        public void EmailtoVendor_test()
        {
            PurchasingManager man = new PurchasingManager();
            man.EmailtoVendor(4);
        }

        [TestMethod]
        public void GetItemName_test()
        {
            MasterDataProvier m = new MasterDataProvier();
            var data = m.GetItemName();
        }

        [TestMethod]
        public void GetAllMUs_test()
        {
            MasterDataProvier m = new MasterDataProvier();
            var data = m.GetAllMUs();
        }

        [TestMethod]
        public void CreateNewMU_TEst()
        {
            MasterManager m = new MasterManager();
            m.CreateNewMU("ml");
        }

        [TestMethod]
        public void GetItemCategories_TEst()
        {
            MasterDataProvier m = new MasterDataProvier();
            var data = m.GetItemCategories();
        }

        [TestMethod]
        public void GetMOHeaders_test()
        {
            MODataProvider p = new MODataProvider();
            var data = p.GetMOHeadersNew("dba", null, null, null, 1, 1, true);
        }

        [TestMethod]
        public void GetMOBy_Id_test()
        {
            MODataProvider p = new MODataProvider();
            var data = p.GetMObyId(2658);
        }

        [TestMethod]
        public void CreateNewMO_Test()
        {
            for (int i = 2; i < 3; i++)
            {
                MOManager manager = new MOManager();
                MMO m = new MMO()
                {
                    Requester = "dkm " + i,
                    RequestingSiteID = 1,
                    TransferringSiteID = 2,
                    TransferDate = DateTime.UtcNow.AddHours(5.5)
                };
                var line = m.AddNewLine();
                line.ItemID = 1000 + i;
                line.Quantity = 20 + i;
                manager.CreateNewMO(m);
            }
        }

        [TestMethod]
        public void GetMReceivingforMoRelease_Test()
        {
            MODataProvider m = new MODataProvider();
            var data = m.GetMReceivingforMoRelease(1);
        }

        [TestMethod]
        public void GetMReceivingforMoReceiving_Test()
        {
            MODataProvider m = new MODataProvider();
            var data = m.GetMReceivingforMoReceiving(1);
        }

        [TestMethod]
        public void CreateMORelease_Test()
        {
            MOManager m = new MOManager();
            MReceiving mr = new MReceiving()
            {
                LastUpdaetdOn = DateTime.UtcNow.AddHours(5.5),
                SiteID = 1,
                VendorID = 1,
                Purchaser = "Dinesh Kmr1.",
                MOID = 8
            };
            var line1 = mr.AddNewLine();
            line1.ItemID = 1001;
            line1.Quantity = 4;
            m.CreateMOReceive(mr);
        }

        [TestMethod]
        public void CreateMOReceive_Test()
        {
            MOManager m = new MOManager();
            MReceiving mr = new MReceiving()
            {
                LastUpdaetdOn = DateTime.UtcNow.AddHours(5.5),
                SiteID = 1,
                VendorID = 1,
                Purchaser = "Dinesh Kmr."
            };
            var line1 = mr.AddNewLine();
            line1.ItemID = 1001;
            line1.Quantity = 4;
            m.CreateMORelease(mr);
        }

        [TestMethod]
        public void CloseMO_IFRequired_test()
        {
            db.CloseMO_IFRequired(1);
        }

        [TestMethod]
        public void GetAllUsers_test()
        {
            AuthenticationManager m = new AuthenticationManager();
            var data = m.GetAllUserNames();
        }

        [TestMethod]
        public void GetAllTransporters_test()
        {
            MasterDataProvier m = new MasterDataProvier();
            var data = m.GetAllTransporters();
        }

        [TestMethod]
        public void GetAllowedItemId_test()
        {
            PurchasingManager m = new PurchasingManager();
            var data = m.GetAllowedItemId(154, 1);
        }

        [TestMethod]
        public IQueryable<MReceivingDetailsforMasters> GetAllReceivingsforMasters_test()
        {
            MasterDataProvier m = new MasterDataProvier();
            var data = m.GetAllReceivingsforMasters(154, null);
            return data;
        }

        //[TestMethod]
        //public IEnumrable<MStockReport> GetStockSummary_test()
        //{
        //    MasterDataProvier m = new MasterDataProvier();
        //    var data = m.GetStockSummary(null, null, null, 1012);
        //    return data;
        //}

        [TestMethod]
        public void GetBillbyId_test()
        {
            BillingDataProvider dp = new BillingDataProvider();
            var data = dp.GetBillbyId(7);
        }
    }
}

        