﻿using SIMS.MM.BLL;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using SIMS.MM.MODEL;

namespace SIMS.MM.Testing
{
    
    
    /// <summary>
    ///This is a test class for MRManagerTest and is intended
    ///to contain all MRManagerTest Unit Tests
    ///</summary>
    [TestClass()]
    public class MRManagerTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///A test for CreateNewMR
        ///</summary>
        [TestMethod()]
        public void CreateNewMRTest()
        {
            MRManager target = new MRManager(); // TODO: Initialize to an appropriate value
            MMR _NewMR = new MMR()
            {
                SiteID=1,
                RaisedBy="Dinesh",
            };
            var line = _NewMR.AddNewLine();
            line.LineID = 1;
            line.ItemID = 1002;
            line.ConsumableType = "Consumable";
            line.Quantity = 100;
            //MMR expected = null; // TODO: Initialize to an appropriate value
            MMR actual;
            actual = target.CreateNewMR(_NewMR);
            //Assert.AreEqual(expected, actual);
            //Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for CreateMRIssue
        ///</summary>
        [TestMethod()]
        public void CreateMRIssueTest()
        {
            MRManager target = new MRManager(); // TODO: Initialize to an appropriate value
            MReceiving _mrec = new MReceiving()
            {
                SiteID=10,
                CreatedBy="Dinesh Kr",
                LastUpdatedBy="dk",
                Purchaser="dkm",                
            };
            var line = _mrec.AddNewLine();
            line.ItemID = 1020;
            line.Quantity = 120;

            //int expected = 0; // TODO: Initialize to an appropriate value
            int actual;
            actual = target.CreateMRIssue(_mrec);
            //Assert.AreEqual(expected, actual);
            //Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for CreateMRReturn
        ///</summary>
        [TestMethod()]
        public void CreateMRReturnTest()
        {
            MRManager target = new MRManager(); // TODO: Initialize to an appropriate value
            MReceiving _mrec =new MReceiving()
            {
                SiteID = 10,
                CreatedBy = "Dinesh Kr",
                LastUpdatedBy = "dk",
                Purchaser = "dkm",
            };
            var line = _mrec.AddNewLine();
            line.ItemID = 1020;
            line.Quantity = 120;

            //int expected = 0; // TODO: Initialize to an appropriate value
            int actual;
            actual = target.CreateMRReturn(_mrec);
           // Assert.AreEqual(expected, actual);
            //Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for CloseMR
        ///</summary>
        [TestMethod()]
        public void CloseMRTest()
        {
            MRManager target = new MRManager(); // TODO: Initialize to an appropriate value
            int mrid = 0; // TODO: Initialize to an appropriate value
            target.CloseMR(mrid);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }
    }
}
