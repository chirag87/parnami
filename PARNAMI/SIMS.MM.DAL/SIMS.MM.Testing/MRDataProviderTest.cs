﻿using SIMS.MM.BLL;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using SIMS.MM.MODEL;

namespace SIMS.MM.Testing
{
    
    
    /// <summary>
    ///This is a test class for MRDataProviderTest and is intended
    ///to contain all MRDataProviderTest Unit Tests
    ///</summary>
    [TestClass()]
    public class MRDataProviderTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///A test for GetMRHeaders
        ///</summary>
        [TestMethod()]
        public void GetMRHeadersTest()
        {
            MRDataProvider target = new MRDataProvider(); // TODO: Initialize to an appropriate value
            //Nullable<int> pageindex = new Nullable<int>(); // TODO: Initialize to an appropriate value
           // Nullable<int> pagesize = new Nullable<int>(); // TODO: Initialize to an appropriate value
            //string Key = string.Empty; // TODO: Initialize to an appropriate value
            //Nullable<int> vendorid = new Nullable<int>(); // TODO: Initialize to an appropriate value
            //Nullable<int> sideid = new Nullable<int>(); // TODO: Initialize to an appropriate value
            //IPaginated<MMRHeader> expected = null; // TODO: Initialize to an appropriate value
            IPaginated<MMRHeader> actual;
            actual = target.GetMRHeadersNew(null,null,null,null,null);
            //Assert.AreEqual(expected, actual);
            //Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for GetMRByID
        ///</summary>
        [TestMethod()]
        public void GetMRByIDTest()
        {
            MRDataProvider target = new MRDataProvider(); // TODO: Initialize to an appropriate value
            int MRID = 1; // TODO: Initialize to an appropriate value
           // MMR expected = null; // TODO: Initialize to an appropriate value
            MMR actual;
            actual = target.GetMRByID(MRID);
            //Assert.AreEqual(expected, actual);
            //Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for GetMReceivingforMRIssue
        ///</summary>
        [TestMethod()]
        public void GetMReceivingforMRIssueTest()
        {
            MRDataProvider target = new MRDataProvider(); // TODO: Initialize to an appropriate value
            int mrid = 1; // TODO: Initialize to an appropriate value
            //MReceiving expected = null; // TODO: Initialize to an appropriate value
            MReceiving actual;
            actual = target.GetMReceivingforMRIssue(mrid);
            //Assert.AreEqual(expected, actual);
            //Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for GetMReceivingforMRReturn
        ///</summary>
        [TestMethod()]
        public void GetMReceivingforMRReturnTest()
        {
            MRDataProvider target = new MRDataProvider(); // TODO: Initialize to an appropriate value
            int mrid = 1; // TODO: Initialize to an appropriate value
           // MReceiving expected = null; // TODO: Initialize to an appropriate value
            MReceiving actual;
            actual = target.GetMReceivingforMRReturn(mrid);
            //Assert.AreEqual(expected, actual);
            //Assert.Inconclusive("Verify the correctness of this test method.");
        }
    }
}
