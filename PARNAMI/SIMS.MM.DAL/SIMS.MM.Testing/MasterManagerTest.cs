﻿using SIMS.MM.BLL;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using SIMS.MM.MODEL;

namespace SIMS.MM.Testing
{
    
    
    /// <summary>
    ///This is a test class for MasterManagerTest and is intended
    ///to contain all MasterManagerTest Unit Tests
    ///</summary>
    [TestClass()]
    public class MasterManagerTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///A test for CreateNewPricing
        ///</summary>
        [TestMethod()]
        public void CreateNewPricingTest()
        {
            MasterManager target = new MasterManager(); // TODO: Initialize to an appropriate value
            MPriceMaster _mprice = new MPriceMaster()
            { 


            }; // TODO: Initialize to an appropriate value
            //MPriceMaster expected = null; // TODO: Initialize to an appropriate value
            MPriceMaster actual;
            actual = target.CreateNewPricing(_mprice);
            //.AreEqual(expected, actual);
            //Assert.Inconclusive("Verify the correctness of this test method.");
        }
    }
}
