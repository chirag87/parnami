﻿using SIMS.MM.BLL.Reports;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Linq;
using SIMS.MM.MODEL.Reports;
using System.Collections.Generic;

namespace SIMS.MM.Testing
{
    
    
    /// <summary>
    ///This is a test class for ReportProviderTest and is intended
    ///to contain all ReportProviderTest Unit Tests
    ///</summary>
    [TestClass()]
    public class ReportProviderTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///A test for ReportProvider Constructor
        ///</summary>
        [TestMethod()]
        public void ReportProviderConstructorTest()
        {
            ReportProvider target = new ReportProvider();
            Assert.Inconclusive("TODO: Implement code to verify target");
        }

        /// <summary>
        ///A test for GetPODetailsByLine
        ///</summary>
        [TestMethod()]
        public void GetPODetailsByLineTest()
        {
            ReportProvider target = new ReportProvider(); // TODO: Initialize to an appropriate value
            Nullable<DateTime> startdate = new Nullable<DateTime>(); // TODO: Initialize to an appropriate value
            Nullable<DateTime> enddate = new Nullable<DateTime>(); // TODO: Initialize to an appropriate value
            Nullable<int> siteid = new Nullable<int>(); // TODO: Initialize to an appropriate value
            Nullable<int> vendorid = new Nullable<int>(); // TODO: Initialize to an appropriate value
            Nullable<int> itemid = new Nullable<int>(); // TODO: Initialize to an appropriate value
            Nullable<int> itemcatid = new Nullable<int>(); // TODO: Initialize to an appropriate value
            //IEnumerable<MPOReport> expected = null; // TODO: Initialize to an appropriate value
            IEnumerable<MPOReport> actual;
            actual = target.GetPODetailsByLine(startdate, enddate, siteid, vendorid, itemid, itemcatid);
            Assert.IsTrue(actual.Count()>0);
            //Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for GetSiteWiseReceiving
        ///</summary>
        [TestMethod()]
        //public void GetSiteWiseReceivingTest()
        //{
        //    ReportProvider target = new ReportProvider(); // TODO: Initialize to an appropriate value
        //    Nullable<int> vendorid = new Nullable<int>(); // TODO: Initialize to an appropriate value
        //    Nullable<int> itemid = new Nullable<int>(); // TODO: Initialize to an appropriate value
        //    Nullable<DateTime> date1 = new Nullable<DateTime>(); // TODO: Initialize to an appropriate value
        //    Nullable<DateTime> date2 = new Nullable<DateTime>(); // TODO: Initialize to an appropriate value
        //    IEnumerable<MGRNReport> expected = null; // TODO: Initialize to an appropriate value
        //    IEnumerable<MGRNReport> actual;
        //    actual = target.GetSiteWiseReceiving(vendorid, itemid, date1, date2);
        //    Assert.AreEqual(expected, actual);
        //    Assert.Inconclusive("Verify the correctness of this test method.");
        //}

        /// <summary>
        ///A test for GetVendorWiseReceiving
        ///</summary>
       // [TestMethod()]
        //public void GetVendorWiseReceivingTest()
        //{
        //    ReportProvider target = new ReportProvider(); // TODO: Initialize to an appropriate value
        //    Nullable<int> vendorid = new Nullable<int>(); // TODO: Initialize to an appropriate value
        //    Nullable<int> itemid = new Nullable<int>(); // TODO: Initialize to an appropriate value
        //    Nullable<DateTime> date1 = new Nullable<DateTime>(); // TODO: Initialize to an appropriate value
        //    Nullable<DateTime> date2 = new Nullable<DateTime>(); // TODO: Initialize to an appropriate value
        //    IEnumerable<MDPReport> expected = null; // TODO: Initialize to an appropriate value
        //    IEnumerable<MDPReport> actual;
        //    actual = target.GetVendorWiseReceiving(vendorid, itemid, date1, date2);
        //    Assert.AreEqual(expected, actual);
        //    Assert.Inconclusive("Verify the correctness of this test method.");
        //}

        /// <summary>
        ///A test for InventoryDetailsBySiteID
        ///</summary>
       // [TestMethod()]
        //public void InventoryDetailsBySiteIDTest()
        //{
        //    ReportProvider target = new ReportProvider(); // TODO: Initialize to an appropriate value
        //    Nullable<int> siteid = new Nullable<int>(); // TODO: Initialize to an appropriate value
        //    IEnumerable<MCPReport> expected = null; // TODO: Initialize to an appropriate value
        //    IEnumerable<MCPReport> actual;
        //    actual = target.InventoryDetailsBySiteID(siteid);
        //    Assert.AreEqual(expected, actual);
        //    Assert.Inconclusive("Verify the correctness of this test method.");
        //}

        /// <summary>
        ///A test for InventoryListforAllSites
        ///</summary>
      //  [TestMethod()]
        //public void InventoryListforAllSitesTest()
        //{
        //    ReportProvider target = new ReportProvider(); // TODO: Initialize to an appropriate value
        //    Nullable<DateTime> date1 = new Nullable<DateTime>(); // TODO: Initialize to an appropriate value
        //    Nullable<DateTime> date2 = new Nullable<DateTime>(); // TODO: Initialize to an appropriate value
        //    IEnumerable<MStockReport> expected = null; // TODO: Initialize to an appropriate value
        //    IEnumerable<MStockReport> actual;
        //    actual = target.InventoryListforAllSites(date1, date2);
        //    Assert.AreEqual(expected, actual);
        //    Assert.Inconclusive("Verify the correctness of this test method.");
        //}

        /// <summary>
        ///A test for TotalReceiveByDates
        ///</summary>
       // [TestMethod()]
        public void TotalReceiveByDatesTest()
        {
            ReportProvider target = new ReportProvider(); // TODO: Initialize to an appropriate value
            Nullable<DateTime> date1 = new Nullable<DateTime>(); // TODO: Initialize to an appropriate value
            Nullable<DateTime> date2 = new Nullable<DateTime>(); // TODO: Initialize to an appropriate value
            IEnumerable<MConsumptionReport> expected = null; // TODO: Initialize to an appropriate value
            IEnumerable<MConsumptionReport> actual;
            actual = target.TotalReceiveByDates(date1, date2);
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for TotalReceiveBySiteAndDates
        ///</summary>
        [TestMethod()]
        public void TotalReceiveBySiteAndDatesTest()
        {
            ReportProvider target = new ReportProvider(); // TODO: Initialize to an appropriate value
            Nullable<int> siteid = new Nullable<int>(); // TODO: Initialize to an appropriate value
            Nullable<DateTime> date1 = new Nullable<DateTime>(); // TODO: Initialize to an appropriate value
            Nullable<DateTime> date2 = new Nullable<DateTime>(); // TODO: Initialize to an appropriate value
            IEnumerable<MMTRReport> expected = null; // TODO: Initialize to an appropriate value
            IEnumerable<MMTRReport> actual;
            actual = target.TotalReceiveBySiteAndDates(siteid, date1, date2);
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }
    }
}
